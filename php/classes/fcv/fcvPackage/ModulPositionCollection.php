<?php
namespace fcv\fcvPackage;


use \fcv\fcvPackage\CodeModulCollection;
class ModulPositionCollection
        
        
        implements \Iterator, \ArrayAccess, \Countable {
    private $index = 0;
    /**
     * @var ModulPosition[] $objArray
     */
    protected $objArray = array();


    public function __construct($data = array()){
        $this->objArray = $data;
    }

    /**
     * @param ModulPosition[] $data
     * @return ModulPositionCollection inserted Id
     */
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new ModulPositionCollection($data);
    }
    /**
     * @param ModulPosition $el
     * @return ModulPositionCollection inserted Id
     */
    public function add($el){
        $this->objArray[] = $el;
    }

    public function current(): mixed {
        return $this->objArray[$this->index];
    }

    public function key(): int {
        return $this->index;
    }

    public function next(): void {
        $this->index++;
    }

    public function rewind(): void {
        $this->index = 0;
    }

    public function valid(): bool {
        return isset($this->objArray[$this->index]);
    }
    public function append($el) {
        return $this->add($el);
    }
    public function count(): int {
        return count($this->objArray);
    }

    public function offsetExists($offset): bool {
        return isSet($this->objArray[$offset]);
    }

    public function offsetGet($offset): mixed {
        return $this->objArray[$offset];
    }

    public function offsetSet($offset, $value): void {
        $this->objArray[$offset] = $value;
    }

    public function offsetUnset($offset): void {
        unset($this->objArray[$offset]);
    }

    public function seek($position): void {
        $this->index = $position;
    }

    public function serialize() {
        return serialize($this->objArray);
    }

    public function unserialize($serialized) {
        return unserialize($serialized);
    }
    //UserMethods

    //Setters
    /**
    *
    * @param Integer $positionId
    * @return ModulPosition
    */
    public function setPositionId($positionId){
       foreach($this->objArray as $el){
           $el->setPositionId($positionId);
       }
       return $this;
    }
    /**
    *
    * @param String $name
    * @return ModulPosition
    */
    public function setName($name){
       foreach($this->objArray as $el){
           $el->setName($name);
       }
       return $this;
    }
    /**
    *
    * @param Integer $indent
    * @return ModulPosition
    */
    public function setIndent($indent){
       foreach($this->objArray as $el){
           $el->setIndent($indent);
       }
       return $this;
    }
    public function setModul(CodeModul $modul){
       foreach($this->objArray as $el){
           $el->setModul($modul);
       }
       return $this;
    }
    public function save(){
       foreach($this->objArray as $el){
           $el->save();
       }
       return $this;
    }
    public function treeSave(){
       foreach($this->objArray as $el){
           $el->treeSave();
       }
       return $this;
    }
    public function getHtml($tplName = 'default'){
       $html = '';
       foreach($this->objArray as $el){
           $html .= $el->getHtml($tplName);
       }
       return $html;
    }
    
    public function each($cb){
        foreach($this->objArray as $i => $o){
            $cb($o,$i);
        }
        return $this;
    }
    
    public function addToJs(){
        foreach($this->objArray as $i => $o){
            $o->addToJs();
        }
        return $this;
    }
    //Map Reduce Pattern
    public function map($mapFunction){
        $ret = array();
        foreach($this->objArray as $i => $o){
            $ret[$i] = $mapFunction($o);
        }
        return $ret;
    }
    
    public function reduce($reduceFunction){

        $length = count($this->objArray);
        if($length <= 1) return $this->objArray;

        $array = $this->objArray;
        $out = array_shift($array);
        do {
            $next = array_shift($array);
            $out = $reduceFunction($out, $next);
        } while(!empty($array));

        return $out;
    }
}
