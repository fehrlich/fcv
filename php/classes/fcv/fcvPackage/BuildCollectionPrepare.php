<?php
namespace fcv\fcvPackage;

use \QueryBuilder;

use \fcv\fcvPackage\ProjectCollectionPrepare;
use \fcv\fcvPackage\CodeModulCollectionPrepare;class BuildCollectionPrepare {

    protected $tableName;
    protected $tableNameAlias = '';

    /**
     * @var QueryBuilder
     */
    protected $query;
    protected $objectMap = array();

    protected $primaryKeyIndex = 0;

    public function __construct(){
        $this->tableName = 'Build';
        $this->query = \QueryBuilder::create();
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_NUM);
    }

    /**
     * @param Build[] $data
     * @return BuildCollectionPrepare inserted Id
     */
    public static function create(){
        return new BuildCollectionPrepare();
    }

    /**
     * @return int
     */
    public function getPrimaryKeyIndex(){
        return $this->primaryKeyIndex;
    }

    /**
     * @return array
     */
    public function getObjectMap(){
        return $this->objectMap;
    }

    /**
     * @return QueryBuilder
     */
    public function getQuery(){
        return $this->query;
    }

    /**
     * @return string
     */
    public function getTableName(){
        return $this->tableName;
    }

    /**
     * @return BuildCollectionPrepare
     */
    public function setTableName($tableName){
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableNameAlias(){
        return ($this->tableNameAlias != '')?$this->tableNameAlias:$this->tableName;
    }

    /**
     * @return BuildCollectionPrepare
     */
    public function setTableNameAlias($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias;
        return $this;
    }

    /**
     * @return string
     */
    public function setTableNameAliasPrepend($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias.(($this->tableNameAlias == '')?$this->tableName:$this->tableNameAlias);
        return $this;
    }

    /**
      * Add a basic where clause to the query.
      *
      * @param  string  $column
      * @param  string  $operator
      * @param  mixed   $value
      * @param  string  $boolean
      * @return BuildCollectionPrepare
      *
      * @throws \InvalidArgumentException
      */
    public function where($column, $operator = null, $value = null, $boolean = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->where($spl[1], $operator, $value, $boolean);
        }else{
            $this->query->where($column, $operator, $value, $boolean);
        }
        return $this;
    }
    
    public function orWhere($column, $operator = null, $value = null, $logicConnector = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->orWhere($spl[1], $operator, $value, $logicConnector);
        }else{
            $this->query->orWhere($column, $operator, $value, $logicConnector);
        }
        return $this;
    }

    /**
     * @return BuildCollectionPrepare
     */
    public function whereRaw($where){
        $this->query->whereRaw($where);
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return BuildCollectionPrepare
     */
    public function whereBuildId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('buildId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return BuildCollectionPrepare
     */
    public function orWhereBuildId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('buildId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return BuildCollectionPrepare
     */
    public function selectBuildId(){
        $this->primaryKeyIndex = count($this->query->columns);
        $this->objectMap[] = array(
            'class' => 'Build',
            'field' => 'buildId',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('buildId');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return BuildCollectionPrepare
     */
    public function whereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return BuildCollectionPrepare
     */
    public function orWhereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return BuildCollectionPrepare
     */
    public function selectName(){
        $this->objectMap[] = array(
            'class' => 'Build',
            'field' => 'name',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('name');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return BuildCollectionPrepare
     */
    public function whereFirstBuild($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('firstBuild', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return BuildCollectionPrepare
     */
    public function orWhereFirstBuild($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('firstBuild', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return BuildCollectionPrepare
     */
    public function selectFirstBuild(){
        $this->objectMap[] = array(
            'class' => 'Build',
            'field' => 'firstBuild',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('firstBuild');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return BuildCollectionPrepare
     */
    public function whereBuildPath($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('buildPath', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return BuildCollectionPrepare
     */
    public function orWhereBuildPath($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('buildPath', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return BuildCollectionPrepare
     */
    public function selectBuildPath(){
        $this->objectMap[] = array(
            'class' => 'Build',
            'field' => 'buildPath',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('buildPath');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return BuildCollectionPrepare
     */
    public function whereBuildWorkpackages($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('buildWorkpackages', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return BuildCollectionPrepare
     */
    public function orWhereBuildWorkpackages($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('buildWorkpackages', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return BuildCollectionPrepare
     */
    public function selectBuildWorkpackages(){
        $this->objectMap[] = array(
            'class' => 'Build',
            'field' => 'buildWorkpackages',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('buildWorkpackages');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return BuildCollectionPrepare
     */
    public function whereDisabled($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('disabled', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return BuildCollectionPrepare
     */
    public function orWhereDisabled($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('disabled', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return BuildCollectionPrepare
     */
    public function selectDisabled(){
        $this->objectMap[] = array(
            'class' => 'Build',
            'field' => 'disabled',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('disabled');
        return $this;
    }

    /**
     * @var ProjectCollectionPrepare
     */
    protected $project;
    /**
     * @var ProjectCollectionPrepare
     * @return BuildCollectionPrepare
     */
    public function setProject(ProjectCollectionPrepare $project){
       $this->project = $project;
       $project->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getProject(){
       if(!isset($this->project)) $this->project = ProjectCollectionPrepare::create();
       return $this->project;
    }
    
    
    public function selectProject(){
        $this->objectMap[] = array(
            'class' => 'project',
            'field' => 'project',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('project');
        return $this;
    }




    /**
     * @var CodeModulCollectionPrepare
     */
    protected $usedModules;
    /**
     * @var CodeModulCollectionPrepare
     * @return BuildCollectionPrepare
     */
    public function setUsedModules(CodeModulCollectionPrepare $usedModules){
       $this->usedModules = $usedModules;
       $usedModules->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getUsedModules(){
       if(!isset($this->usedModules)) $this->usedModules = CodeModulCollectionPrepare::create();
       return $this->usedModules;
    }
    
    





    private $selectAll = false;
    public function selectAll(){
        
        $this->selectBuildId();
        $this->selectName();
        $this->selectFirstBuild();
        $this->selectBuildPath();
        $this->selectBuildWorkpackages();
        $this->selectDisabled();
        $this->selectProject();

        return $this;
    }
    public function prepareQuery(){
        $this->prepareJoins();
        $this->query->from = 'Build'.(($this->tableNameAlias == '')?'':' AS '.$this->tableNameAlias);
        QueryBuilder::appendWhereWithTable($this->query, $this->getTableNameAlias());
        return $this;
    }
    /**
     * @return BuildCollection
     */
    public function get(){
        $this->prepareQuery();
        $rows = $this->query->get();
        return $this->parseDbRows($rows, $this->getObjectMap());
    }

    /**
     * parse all return rows from sql
     */
    private function parseDbRows($rows, $map){
        $objsArray = array();
        $objs = BuildCollection::create();
        //build assoc Array with all data
        if(!$this->selectAll){
            foreach($rows as $row){
                $primaryField = $row[$this->primaryKeyIndex];

                //extend object if allready exists

                $initObj = (isset($objsArray[$primaryField]))?$objsArray[$primaryField]:array();
                $objsArray[$primaryField] = $this->parseDbRow($row, $map, $initObj);
            }
        }else $objsArray = $rows;
        //convert array to real objects (subobjects are generated in constructors)
        foreach($objsArray as $dataArray){
            $objs->add(new Build($dataArray));
        }
        return $objs;
    }

    /**
     * parse one row into Object and extend existing ones
     */
    private function parseDbRow($row, $map, $objsData = array(), &$index = 0){
        foreach($map as $m){
            //check if association
            if(isset($m['map'])){
                //check if cardinality is multi
                if($m['multi']){
                    if(!isset($objsData[$m['field']])) $objsData[$m['field']] = array();
                    //get primaryKey ob subobject
                    $id = $row[$index+$m['primaryKeyIndex']];
                    //extend object if allready exists
                    $initObj = (isset($objsData[$m['field']][$id]))?$objsData[$m['field']][$id]:array();
                    //add/extend object with id
                    $objsData[$m['field']][$id] = $this->parseDbRow($row, $m['map'], $initObj, $index);
                }else{
                    $objsData[$m['field']] = $this->parseDbRow($row, $m['map'], array(), $index);
                }
            }
            else{
                $objsData[$m['field']] = $row[$index];
                $index++;
            }
        }
        return $objsData;
    }

    /**
     * create joins depending on associations
     * @return CollectionPrepare
     */
    public function prepareJoins(){
        
        if(!$this->selectAll){
            if(!is_array($this->query->columns) || !in_array('buildId', $this->query->columns)){
                $this->selectbuildId();
            }elseif(!is_array($this->query->columns)){
                $this->selectAll();
            }
        }else{
            $this->query->select('*');
        }
        if(isSet($this->project)){
            $tableName = $this->project->getTableNameAlias();
            //Single Build
             $this->query->join('Project as '.$tableName, $tableName.'.projektId', '=', $this->getTableNameAlias().'.project', 'LEFT');
            $this->project->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->project->getQuery(),$tableName);
            $this->query->mergeQuery($this->project->getQuery());

            $this->objectMap[] = array(
                'class' => 'Project',
                'field' => 'project',
                'multi' => false,
                'primaryKeyIndex' => $this->project->getPrimaryKeyIndex(),
                'map' => $this->project->getObjectMap()
            );
        }


        if(isSet($this->usedModules)){
            $tableName = $this->usedModules->getTableNameAlias();
            //Multi
            $this->query->join('CodeModul as '.$tableName, $tableName.'.usedInBuilds', '=', $this->getTableNameAlias().'.buildId', 'LEFT');
            $this->usedModules->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->usedModules->getQuery(),$tableName);
            $this->query->mergeQuery($this->usedModules->getQuery());

            $this->objectMap[] = array(
                'class' => 'CodeModul',
                'field' => 'usedModules',
                'multi' => true,
                'primaryKeyIndex' => $this->usedModules->getPrimaryKeyIndex(),
                'map' => $this->usedModules->getObjectMap()
            );
        }



        return $this;
    }
}
