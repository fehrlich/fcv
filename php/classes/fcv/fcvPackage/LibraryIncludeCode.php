<?php
namespace fcv\fcvPackage; ///fcv/fcvPackage/LibraryIncludeCode
    
use Exception;
use mys;
use QueryBuilder;
use fcv\libs\TemplateEngine;
use fcv\libs\Debug;
use fcv\FCVJsObj;


use fcv\fcvPackage\CodeLanguage;
;
use fcv\fcvPackage\Library;

class LibraryIncludeCode
        
         
        {
    
    //Attributes
    //const INCLUDECODE = 'IncludeCode';
    //const ESCAPECHAR = 'escapeChar';
    
    //Litearls
    
    //Attributes
    /**
     *
     * @var String
     */
     public $IncludeCode;
    const FIELD_INCLUDECODE = 'IncludeCode';
    /**
     *
     * @var String
     */
     public $escapeChar;
    const FIELD_ESCAPECHAR = 'escapeChar';
    
    //AssociationsEnds
    /**
     * @var CodeLanguage
     */
    private $codeLanguage;
    
    //{/isAssociationClass}}
    /**
     * @var Library
     */
    private $library;
    
    //{/isAssociationClass}}
    //FCV Attributes
    private $apPath = 'FCVAPs/';
    //    public $VIEW = array();
    static $fcvFields = array('IncludeCode','escapeChar');
    //Todo: Outsource
    protected $loadObject = false;
    private $loadedAttributes = array();
    
    //Methods
    //UserOperations
    /**
      *
      * @return String
      */
    public  function getIncludeCode(){
        
            /* @var $this LibraryIncludeCode*/
    
    }
    private $lastModifiedTime = false;
    private $createdTime = false;
    public function __construct($data = array(),$calledFromChild = false){
        if(is_string($data)) $data = json_decode($data,true);
        if(count($data) > 0){
            if(isSet($data['loadObj']) && $data['loadObj']){
                $this->loadObject = true;
            }
            
            if(isSet($data['IncludeCode'])) $this->IncludeCode = $data['IncludeCode'];
            if(isSet($data['escapeChar'])) $this->escapeChar = $data['escapeChar'];
            //associationEnds
            //For AssociationClasses
            if(isSet($data['codeLanguage'])){
                $this->codeLanguage = (is_array($data['codeLanguage']))?CodeLanguage::create($data['codeLanguage'],$this->loadObject):$data['codeLanguage'];
            }
            //values for AssociationClass ... used for ObjekteContent.php Vertriebspartner suche
            if(isSet($data['languages'])){
                if(!is_object($data['languages'])){
                    $newVal = new CodeLanguageCollection();
                    foreach($data['languages'] as $el) $newVal->add(CodeLanguage::create($el,$this->loadObject));
                    $this->languages = $newVal;
                }else $this->languages = $data['languages'];
            }
            //Regular AssociationEnd LibraryIncludeCode (UMLAssociationClass)
            //For AssociationClasses
            if(isSet($data['library'])){
                $this->library = (is_array($data['library']))?Library::create($data['library'],$this->loadObject):$data['library'];
            }
            //values for AssociationClass ... used for ObjekteContent.php Vertriebspartner suche
            if(isSet($data['lib'])){
                if(!is_object($data['lib'])){
                    $newVal = new LibraryCollection();
                    foreach($data['lib'] as $el) $newVal->add(Library::create($el,$this->loadObject));
                    $this->lib = $newVal;
                }else $this->lib = $data['lib'];
            }
            //Regular AssociationEnd LibraryIncludeCode (UMLAssociationClass)
    
            //Helper
            if(isSet($data['lastModified'])) $this->lastModifiedTime = $data['lastModified'];
            if(isSet($data['created'])) $this->createdTime = $data['created'];
        }
    
    
    
    
    }
    
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new LibraryIncludeCode($data);
    }
    
    public static function createLoad(){
        $data = array(
            'loadObj' => true
        );
        return new LibraryIncludeCode($data);
    }
    
    public function getLastModifiedTime(){
        return $this->lastModifiedTime;
    }
    
    public function setLastModifiedTime($time){
        $this->lastModifiedTime = $time;
        return $this;
    }
    
    public function hasPrimaryKey(){
        return false;
    }
    
    
    private $parent;
    private $parentIndex = 0;
    
    public function getParent(){
        return $this->parent;
    }
    public function setParent(&$parent, $parentId = 0){
        $this->parent = $parent;
        $this->parentId = $parentId;
    
        return $this;
    }
    public function getId(){
        if(!isset($this->parent) || empty($this->parent)) return false;
        return ''.$this->parent->getId().'.LibraryIncludeCode.'.$this->parentIndex;
    }
    //    private $pseudoId = false;
    //    public function getId(){
    //        return $this->pseudoId = ($this->pseudoId)?$this->pseudoId:rand();
    //    }
    
    public function addToJs($selectionString = ''){
    //    if(!$calledFromChild && get_class($this) == '' && (!$this->loadObject))
            FCVJsObj::add('', $this, $selectionString);
            return $this;
    }
    public function throwError($msg){
        if(!Debug::getObj()->getThrowErrors()) return "";
        else{
            throw new Exception($msg);
        }
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return CodeLanguage
     * @throws Exception
     */
    public function getLanguages($forceLoad = false){
        if(is_null($this->languages) || empty($this->languages)){
            $this->throwError('class LibraryIncludeCode with id '.$this->getId().' has no Attributes languages');
            //throw new Exception('class LibraryIncludeCode with id '.$this->getId().' has no Attributes languages');
        }
        return $this->languages;
    }
    
    public function hasLanguages($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getLanguages($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param CodeLanguage $codeLanguage
     */
    public function setLanguages(CodeLanguage $languages){
        if(!$this->loadObject) $languages->setLib($this);
        $this->languages = $languages;
        if($this->loadObject) $this->loadedAttributes['languages'] = true;
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return Library
     * @throws Exception
     */
    public function getLib($forceLoad = false){
        if(is_null($this->lib) || empty($this->lib)){
            $this->throwError('class LibraryIncludeCode with id '.$this->getId().' has no Attributes lib');
            //throw new Exception('class LibraryIncludeCode with id '.$this->getId().' has no Attributes lib');
        }
        return $this->lib;
    }
    
    public function hasLib($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getLib($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param Library $library
     */
    public function setLib(Library $lib){
        if(!$this->loadObject) $lib->setLanguages($this);
        $this->lib = $lib;
        if($this->loadObject) $this->loadedAttributes['lib'] = true;
        return $this;
    }
    
    //Getter & Setter
    
    public function get($name){
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            return $this->$name->get($subName);
        }else return $this->$name;
    }
    
    public function has($name){
        if(strpos('.', $name) !== false){
            $spl = explode('.', $name, 2);
            return $this->get($spl[0])->has($spl[1]);
        }
        return (property_exists($this, $name));
    }
    
    public function set($name, $value){
        $possibleFields = array(
        'IncludeCode',
        'escapeChar',
        'languages',
        'lib',
        );
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class LibraryIncludeCode');
            if(!$this->$name){
                if(false);
                elseif($name == 'languages'){
                    $this->languages = CodeLanguage::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['languages'] = true;
                }
                elseif($name == 'lib'){
                    $this->lib = Library::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['lib'] = true;
                }
                else die('Error');
            }else $this->$name->set($subName,$value);
        }else{
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class LibraryIncludeCode');
            $this->$name = $value;
            if($this->loadObject) $this->loadedAttributes[$name] = true;
        }
        return $this;
    }
    
    /**
     *
     * @return String
     * @throws Exception
     */
     public function fcvgetIncludeCode(){
        return $this->IncludeCode;
     }
    
     public function hasIncludeCode(){
        try {
            $this->fcvgetIncludeCode();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $IncludeCode
     * @return LibraryIncludeCode
     */
     public function setIncludeCode($IncludeCode){
        ;
        $this->IncludeCode = $IncludeCode;
        if($this->loadObject) $this->loadedAttributes['IncludeCode'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getEscapeChar(){
        return $this->escapeChar;
     }
    
     public function hasEscapeChar(){
        try {
            $this->getEscapeChar();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $escapeChar
     * @return LibraryIncludeCode
     */
     public function setEscapeChar($escapeChar){
        ;
        $this->escapeChar = $escapeChar;
        if($this->loadObject) $this->loadedAttributes['escapeChar'] = true;
        return $this;
     }
    protected $visitedByToArray = false;
    public function toJSObject($returnArray = false){
        if($this->loadObject) return false;
        if($this->visitedByToArray){
    //            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return '[VALENCODE]jsModel["LibraryIncludeCode.'.$this->getId().'"][VALENCODE]';
                    return array(
                        'class' => 'LibraryIncludeCode',
                        'id' => $this->getId()
                    );
                }
                return false;
    //            }
    //            return array(
    //                'class' => 'LibraryIncludeCode',
    //                'id' => rand()
    //            );
        }
        $retArray = array();
        
        $this->visitedByToArray = true;
        $retArray['id'] = $this->getId();
        if(isSet($this->IncludeCode) && !is_null($this->IncludeCode)) $retArray['IncludeCode'] = (!is_object($this->IncludeCode))?str_replace('"', '\"', $this->IncludeCode):($this->IncludeCode);
        if(isSet($this->escapeChar) && !is_null($this->escapeChar)) $retArray['escapeChar'] = (!is_object($this->escapeChar))?str_replace('"', '\"', $this->escapeChar):($this->escapeChar);
    
        if(isSet($this->languages) && !is_null($this->languages) && $this->languages !== false){
            if(!is_object($this->languages)) $val = $this->languages;
            else{
                $val = $this->languages->toJSObject();
            }
            $retArray['languages'] = $val;
            //$retArray['languages'] = ($simple)?$this->languages->getId():$this->languages->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->lib) && !is_null($this->lib) && $this->lib !== false){
            if(!is_object($this->lib)) $val = $this->lib;
            else{
                $val = $this->lib->toJSObject();
            }
            $retArray['lib'] = $val;
            //$retArray['lib'] = ($simple)?$this->lib->getId():$this->lib->toJSObject($simple, $treeVisit);
    
         }
        return ($returnArray)?$retArray:'[VALENCODE]new LibraryIncludeCode('.json_encode($retArray).')[VALENCODE]';
    }
    
    public function toArray($simple = false, $treeVisit = false){
        if($this->visitedByToArray){
            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return array(
    //                        'class' => 'LibraryIncludeCode',
    //                        'id' => $this->getId()
    //                    );
                    return '[VALENCODE]jsModel["LibraryIncludeCode"]["'.$this->getId().'"][VALENCODE]';
                }
            }
            return false;
        }
        if($treeVisit) $this->visitedByToArray = true;
        $retArray = array();
        
        if(isSet($this->IncludeCode) && !is_null($this->IncludeCode)) $retArray['IncludeCode'] = (is_numeric($this->IncludeCode))?$this->IncludeCode:($this->IncludeCode);
        if(isSet($this->escapeChar) && !is_null($this->escapeChar)) $retArray['escapeChar'] = (is_numeric($this->escapeChar))?$this->escapeChar:($this->escapeChar);
        if(!$simple && isSet($this->languages) && !is_null($this->languages) && $this->languages !== false){
            if(is_numeric($this->languages)) $val = $this->languages;
            else{
                $val = ($simple)?$this->languages->getId():$this->languages->toArray($simple, $treeVisit);
            }
            $retArray['languages'] = $val;
            //$retArray['languages'] = ($simple)?$this->languages->getId():$this->languages->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->lib) && !is_null($this->lib) && $this->lib !== false){
            if(is_numeric($this->lib)) $val = $this->lib;
            else{
                $val = ($simple)?$this->lib->getId():$this->lib->toArray($simple, $treeVisit);
            }
            $retArray['lib'] = $val;
            //$retArray['lib'] = ($simple)?$this->lib->getId():$this->lib->toArray($simple, $treeVisit);
    
         }
        return $retArray;
    }private $tplVars = array();
    public function addTemplateVar($name, $content){
        $this->tplVars[$name] = $content;
    }
    
    private $ignoreErrors = false;
    /**
     * get Html Template
     * @param string $template template name
     */
    public function getHtml($template = "default"){
        $availableTemplates =  array(
            'default'
            ,'default'
        );
        if(!in_array($template, $availableTemplates)){
            throw new Exception('Template <b>'.$template.'</b> doesn\'t exist for <b>LibraryIncludeCode</b>');
        }
        $m = new TemplateEngine;
        
        $templates = array(
            'default' => '' //826
        );
        $tplSrc = $templates[$template];
        
        foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, $this);
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    }
    
    public static function getSubHtml($operationName, $template = 'default'){
    
        
        $availableTemplates =  array(
            'getIncludeCode' => array(
                'default'
                ,'default'
            )
        );
        if(!isset($availableTemplates[$operationName]) || !in_array($template, $availableTemplates[$operationName])){
            throw new Exception('Template OperationTemplate <b>'.$template.'</b> doesn\'t exist for <b>LibraryIncludeCode.'.$operationName.'</b>');
        }
        $m = new TemplateEngine;
        $tplSrc = file_get_contents('FCVAPs/fcv/fcvPackage/LibraryIncludeCode/'.$operationName.'/'.$operationName.'.'.$template.'.html');
        /*foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }*/
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, array());
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    
    }
}
