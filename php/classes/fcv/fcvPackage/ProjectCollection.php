<?php
namespace fcv\fcvPackage;


use \fcv\fcvPackage\BuildCollection;
use \fcv\fcvPackage\WorkPackageCollection;
use \fcv\fcvPackage\LibraryCollection;
use \fcv\fcvPackage\CodeLanguageCollection;
class ProjectCollection
        
        
        implements \Iterator, \ArrayAccess, \Countable {
    private $index = 0;
    /**
     * @var Project[] $objArray
     */
    protected $objArray = array();


    public function __construct($data = array()){
        $this->objArray = $data;
    }

    /**
     * @param Project[] $data
     * @return ProjectCollection inserted Id
     */
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new ProjectCollection($data);
    }
    /**
     * @param Project $el
     * @return ProjectCollection inserted Id
     */
    public function add($el){
        $this->objArray[] = $el;
    }

    public function current() {
        return $this->objArray[$this->index];
    }

    public function key() {
        return $this->index;
    }

    public function next() {
        $this->index++;
    }

    public function rewind() {
        $this->index = 0;
    }

    public function valid() {
        return isset($this->objArray[$this->index]);
    }
    public function append($el) {
        return $this->add($el);
    }
    public function count() {
        return count($this->objArray);
    }

    public function offsetExists($offset) {
        return isSet($this->objArray[$offset]);
    }

    public function offsetGet($offset) {
        return $this->objArray[$offset];
    }

    public function offsetSet($offset, $value) {
        $this->objArray[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->objArray[$offset]);
    }

    public function seek($position) {
        $this->index = $position;
    }

    public function serialize() {
        return serialize($this->objArray);
    }

    public function unserialize($serialized) {
        return unserialize($serialized);
    }
    //UserMethods
    /**
      *
      * @param Build $build
      * @return ProjectCollection this Collection Object
      */
    public function build(Build $build){
        foreach($this->objArray as $el){
           $el->build($build);
       }
       return $this;
    }
    /**
      *
      * @return ProjectCollection this Collection Object
      */
    public function update(){
        foreach($this->objArray as $el){
           $el->update();
       }
       return $this;
    }
    /**
      *
      * @param  $workPackage
      * @return ProjectCollection this Collection Object
      */
    public function buildWorkPackages( $workPackage){
        foreach($this->objArray as $el){
           $el->buildWorkPackages($workPackage);
       }
       return $this;
    }
    /**
      *
      * @return ProjectCollection this Collection Object
      */
    public function buildLibraries(){
        foreach($this->objArray as $el){
           $el->buildLibraries();
       }
       return $this;
    }
    /**
      *
      * @return ProjectCollection this Collection Object
      */
    public function getCurrentBuild(){
        foreach($this->objArray as $el){
           $el->getCurrentBuild();
       }
       return $this;
    }

    //Setters
    /**
    *
    * @param Integer $projektId
    * @return Project
    */
    public function setProjektId($projektId){
       foreach($this->objArray as $el){
           $el->setProjektId($projektId);
       }
       return $this;
    }
    /**
    *
    * @param String $name
    * @return Project
    */
    public function setName($name){
       foreach($this->objArray as $el){
           $el->setName($name);
       }
       return $this;
    }
    /**
    *
    * @param String $umlPath
    * @return Project
    */
    public function setUmlPath($umlPath){
       foreach($this->objArray as $el){
           $el->setUmlPath($umlPath);
       }
       return $this;
    }
    /**
    *
    * @param String $mysqlModel
    * @return Project
    */
    public function setMysqlModel($mysqlModel){
       foreach($this->objArray as $el){
           $el->setMysqlModel($mysqlModel);
       }
       return $this;
    }
    /**
    *
    * @param String $umlModel
    * @return Project
    */
    public function setUmlModel($umlModel){
       foreach($this->objArray as $el){
           $el->setUmlModel($umlModel);
       }
       return $this;
    }
    /**
    *
    * @param String $mysqlDb
    * @return Project
    */
    public function setMysqlDb($mysqlDb){
       foreach($this->objArray as $el){
           $el->setMysqlDb($mysqlDb);
       }
       return $this;
    }
    /**
    *
    * @param String $mysqlUser
    * @return Project
    */
    public function setMysqlUser($mysqlUser){
       foreach($this->objArray as $el){
           $el->setMysqlUser($mysqlUser);
       }
       return $this;
    }
    /**
    *
    * @param String $mysqlPassword
    * @return Project
    */
    public function setMysqlPassword($mysqlPassword){
       foreach($this->objArray as $el){
           $el->setMysqlPassword($mysqlPassword);
       }
       return $this;
    }
    /**
    *
    * @param String $mysqlHost
    * @return Project
    */
    public function setMysqlHost($mysqlHost){
       foreach($this->objArray as $el){
           $el->setMysqlHost($mysqlHost);
       }
       return $this;
    }
    /**
    *
    * @param Integer $mysqlPort
    * @return Project
    */
    public function setMysqlPort($mysqlPort){
       foreach($this->objArray as $el){
           $el->setMysqlPort($mysqlPort);
       }
       return $this;
    }
    /**
    *
    * @param Integer $umlElement
    * @return Project
    */
    public function setUmlElement($umlElement){
       foreach($this->objArray as $el){
           $el->setUmlElement($umlElement);
       }
       return $this;
    }
    /**
    *
    * @param String $buildPath
    * @return Project
    */
    public function setBuildPath($buildPath){
       foreach($this->objArray as $el){
           $el->setBuildPath($buildPath);
       }
       return $this;
    }
    public function setBuilds(Build $builds){
       foreach($this->objArray as $el){
           $el->setBuilds($builds);
       }
       return $this;
    }
    public function setWorkPackages(WorkPackage $workPackages){
       foreach($this->objArray as $el){
           $el->setWorkPackages($workPackages);
       }
       return $this;
    }
    public function setUsedLibs(Library $usedLibs){
       foreach($this->objArray as $el){
           $el->setUsedLibs($usedLibs);
       }
       return $this;
    }
    public function setUsedLanguages(CodeLanguage $usedLanguages){
       foreach($this->objArray as $el){
           $el->setUsedLanguages($usedLanguages);
       }
       return $this;
    }
    public function save(){
       foreach($this->objArray as $el){
           $el->save();
       }
       return $this;
    }
    public function treeSave(){
       foreach($this->objArray as $el){
           $el->treeSave();
       }
       return $this;
    }
    public function getHtml($tplName = 'default'){
       $html = '';
       foreach($this->objArray as $el){
           $html .= $el->getHtml($tplName);
       }
       return $html;
    }
    
    public function each($cb){
        foreach($this->objArray as $i => $o){
            $cb($o,$i);
        }
        return $this;
    }
    
    public function addToJs(){
        foreach($this->objArray as $i => $o){
            $o->addToJs();
        }
        return $this;
    }
    //Map Reduce Pattern
    public function map($mapFunction){
        $ret = array();
        foreach($this->objArray as $i => $o){
            $ret[$i] = $mapFunction($o);
        }
        return $ret;
    }
    
    public function reduce($reduceFunction){

        $length = count($this->objArray);
        if($length <= 1) return $this->objArray;

        $array = $this->objArray;
        $out = array_shift($array);
        do {
            $next = array_shift($array);
            $out = $reduceFunction($out, $next);
        } while(!empty($array));

        return $out;
    }
}
