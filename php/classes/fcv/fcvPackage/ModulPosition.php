<?php
namespace fcv\fcvPackage; ///fcv/fcvPackage/ModulPosition
    
use Exception;
use mys;
use QueryBuilder;
use fcv\libs\TemplateEngine;
use fcv\libs\Debug;
use fcv\FCVJsObj;


use fcv\fcvPackage\CodeModul;
;

class ModulPosition
        
         
        {
    
    //Attributes
    //const POSITIONID = 'positionId';
    //const NAME = 'name';
    //const INDENT = 'indent';
    
    //Litearls
    
    //Attributes
    /**
     *
     * @var Integer
     */
     public $positionId;
    const FIELD_POSITIONID = 'positionId';
    /**
     *
     * @var String
     */
     public $name;
    const FIELD_NAME = 'name';
    /**
     *
     * @var Integer
     */
     public $indent;
    const FIELD_INDENT = 'indent';
    
    //AssociationsEnds
    
    /**
     * @var CodeModul
     */
    private $modul = false;
    const MODUL = 'modul';
    
    //{/isAssociationClass}}
    //FCV Attributes
    private $apPath = 'FCVAPs/';
    //    public $VIEW = array();
    static $fcvFields = array('positionId','name','indent');
    //Todo: Outsource
    protected $loadObject = false;
    private $loadedAttributes = array();
    
    //Methods
    //UserOperations
    private $lastModifiedTime = false;
    private $createdTime = false;
    public function __construct($data = array(),$calledFromChild = false){
        if(is_string($data)) $data = json_decode($data,true);
        if(count($data) > 0){
            if(isSet($data['loadObj']) && $data['loadObj']){
                $this->loadObject = true;
            }
            
        //        if(isset($data['positionId']) && !empty($data['positionId']))
    
            if(isSet($data['positionId'])) $this->positionId = $data['positionId'];
            if(isSet($data['name'])) $this->name = $data['name'];
            if(isSet($data['indent'])) $this->indent = $data['indent'];
            //associationEnds
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['modul'])){
                $this->modul = (is_array($data['modul']))?modul::create($data['modul'],$this->loadObject):$data['modul'];
            }*/
    
            if(isSet($data['modul'])){
                $this->modul = (is_array($data['modul']))?CodeModul::create($data['modul'],$this->loadObject):$data['modul'];
            }
    
            //Helper
            if(isSet($data['lastModified'])) $this->lastModifiedTime = $data['lastModified'];
            if(isSet($data['created'])) $this->createdTime = $data['created'];
        }
    
    
    
    
    }
    
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new ModulPosition($data);
    }
    
    public static function createLoad(){
        $data = array(
            'loadObj' => true
        );
        return new ModulPosition($data);
    }
    
    public function getLastModifiedTime(){
        return $this->lastModifiedTime;
    }
    
    public function setLastModifiedTime($time){
        $this->lastModifiedTime = $time;
        return $this;
    }
    
    public function hasPrimaryKey(){
        return true;
    }
    
    public function getId(){
        return $this->positionId;
    }
    public function setId($id){
    //        ModulPosition::$loadedInstances[$id] = &$this;
        $this->positionId = $id;
        if($this->loadObject) $this->loadedAttributes['positionId'] = true;
        return $this;
    }
    public function getPrimaryKeyName(){
        return "positionId";
    }
    
    public function addToJs($selectionString = ''){
    //    if(!$calledFromChild && get_class($this) == '' && (!$this->loadObject))
            FCVJsObj::add('', $this, $selectionString);
            return $this;
    }
    public function throwError($msg){
        if(!Debug::getObj()->getThrowErrors()) return "";
        else{
            throw new Exception($msg);
        }
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return CodeModul
     * @throws Exception
     */
    public function getModul($forceLoad = false){
        if(is_null($this->modul) || empty($this->modul)){
            $this->throwError('class ModulPosition with id '.$this->getId().' has no Attributes modul');
            //throw new Exception('class ModulPosition with id '.$this->getId().' has no Attributes modul');
        }
        return $this->modul;
    }
    
    public function hasModul($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getModul($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param CodeModul $codeModul
     */
    public function setModul(CodeModul $modul){
        $this->modul = $modul;
        if($this->loadObject) $this->loadedAttributes['modul'] = true;
        return $this;
    }
    
    //Getter & Setter
    
    public function get($name){
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            return $this->$name->get($subName);
        }else return $this->$name;
    }
    
    public function has($name){
        if(strpos('.', $name) !== false){
            $spl = explode('.', $name, 2);
            return $this->get($spl[0])->has($spl[1]);
        }
        return (property_exists($this, $name));
    }
    
    public function set($name, $value){
        $possibleFields = array(
        'positionId',
        'name',
        'indent',
        'modul',
        );
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class ModulPosition');
            if(!$this->$name){
                if(false);
                elseif($name == 'modul'){
                    $this->modul = CodeModul::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['modul'] = true;
                }
                else die('Error');
            }else $this->$name->set($subName,$value);
        }else{
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class ModulPosition');
            $this->$name = $value;
            if($this->loadObject) $this->loadedAttributes[$name] = true;
        }
        return $this;
    }
    
    /**
     *
     * @return Integer
     * @throws Exception
     */
     public function getPositionId(){
        return $this->positionId;
     }
    
     public function hasPositionId(){
        try {
            $this->getPositionId();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Integer $positionId
     * @return ModulPosition
     */
     public function setPositionId($positionId){
        ;
        $this->positionId = $positionId;
        if($this->loadObject) $this->loadedAttributes['positionId'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getName(){
        return $this->name;
     }
    
     public function hasName(){
        try {
            $this->getName();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $name
     * @return ModulPosition
     */
     public function setName($name){
        ;
        $this->name = $name;
        if($this->loadObject) $this->loadedAttributes['name'] = true;
        return $this;
     }
    /**
     *
     * @return Integer
     * @throws Exception
     */
     public function getIndent(){
        return $this->indent;
     }
    
     public function hasIndent(){
        try {
            $this->getIndent();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Integer $indent
     * @return ModulPosition
     */
     public function setIndent($indent){
        ;
        $this->indent = $indent;
        if($this->loadObject) $this->loadedAttributes['indent'] = true;
        return $this;
     }
    protected $visitedByToArray = false;
    public function toJSObject($returnArray = false){
        if($this->loadObject) return false;
        if($this->visitedByToArray){
    //            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return '[VALENCODE]jsModel["ModulPosition.'.$this->getId().'"][VALENCODE]';
                    return array(
                        'class' => 'ModulPosition',
                        'id' => $this->getId()
                    );
                }
                return false;
    //            }
    //            return array(
    //                'class' => 'ModulPosition',
    //                'id' => rand()
    //            );
        }
        $retArray = array();
        
        $this->visitedByToArray = true;
        
        if(isSet($this->positionId) && !is_null($this->positionId)) $retArray['positionId'] = (!is_object($this->positionId))?str_replace('"', '\"', $this->positionId):($this->positionId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (!is_object($this->name))?str_replace('"', '\"', $this->name):($this->name);
        if(isSet($this->indent) && !is_null($this->indent)) $retArray['indent'] = (!is_object($this->indent))?str_replace('"', '\"', $this->indent):($this->indent);
    
        if(isSet($this->modul) && !is_null($this->modul) && $this->modul !== false){
            if(!is_object($this->modul)) $val = $this->modul;
            else{
                $val = $this->modul->toJSObject();
            }
            $retArray['modul'] = $val;
            //$retArray['modul'] = ($simple)?$this->modul->getId():$this->modul->toJSObject($simple, $treeVisit);
    
         }
        return ($returnArray)?$retArray:'[VALENCODE]new ModulPosition('.json_encode($retArray).')[VALENCODE]';
    }
    
    public function toArray($simple = false, $treeVisit = false){
        if($this->visitedByToArray){
            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return array(
    //                        'class' => 'ModulPosition',
    //                        'id' => $this->getId()
    //                    );
                    return '[VALENCODE]jsModel["ModulPosition"]["'.$this->getId().'"][VALENCODE]';
                }
            }
            return false;
        }
        if($treeVisit) $this->visitedByToArray = true;
        $retArray = array();
        
        if(isSet($this->positionId) && !is_null($this->positionId)) $retArray['positionId'] = (is_numeric($this->positionId))?$this->positionId:($this->positionId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (is_numeric($this->name))?$this->name:($this->name);
        if(isSet($this->indent) && !is_null($this->indent)) $retArray['indent'] = (is_numeric($this->indent))?$this->indent:($this->indent);
        if(!$simple && isSet($this->modul) && !is_null($this->modul) && $this->modul !== false){
            if(is_numeric($this->modul)) $val = $this->modul;
            else{
                $val = ($simple)?$this->modul->getId():$this->modul->toArray($simple, $treeVisit);
            }
            $retArray['modul'] = $val;
            //$retArray['modul'] = ($simple)?$this->modul->getId():$this->modul->toArray($simple, $treeVisit);
    
         }
        return $retArray;
    }private $tplVars = array();
    public function addTemplateVar($name, $content){
        $this->tplVars[$name] = $content;
    }
    
    private $ignoreErrors = false;
    /**
     * get Html Template
     * @param string $template template name
     */
    public function getHtml($template = "default"){
        $availableTemplates =  array(
            'default'
            ,'default'
        );
        if(!in_array($template, $availableTemplates)){
            throw new Exception('Template <b>'.$template.'</b> doesn\'t exist for <b>ModulPosition</b>');
        }
        $m = new TemplateEngine;
        
        $templates = array(
            'default' => '' //815
        );
        $tplSrc = $templates[$template];
        
        foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, $this);
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    }
    
    public static function getSubHtml($operationName, $template = 'default'){
    
        
        $availableTemplates =  array(
        );
        if(!isset($availableTemplates[$operationName]) || !in_array($template, $availableTemplates[$operationName])){
            throw new Exception('Template OperationTemplate <b>'.$template.'</b> doesn\'t exist for <b>ModulPosition.'.$operationName.'</b>');
        }
        $m = new TemplateEngine;
        $tplSrc = file_get_contents('FCVAPs/fcv/fcvPackage/ModulPosition/'.$operationName.'/'.$operationName.'.'.$template.'.html');
        /*foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }*/
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, array());
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    
    }
}
