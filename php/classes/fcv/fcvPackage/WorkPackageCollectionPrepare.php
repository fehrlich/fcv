<?php
namespace fcv\fcvPackage;

use \QueryBuilder;

use \fcv\fcvPackage\ViewsCollectionPrepare;
use \fcv\fcvPackage\ProjectCollectionPrepare;
use \fcv\fcvPackage\WorkPackageCollectionPrepare;class WorkPackageCollectionPrepare {

    protected $tableName;
    protected $tableNameAlias = '';

    /**
     * @var QueryBuilder
     */
    protected $query;
    protected $objectMap = array();

    protected $primaryKeyIndex = 0;

    public function __construct(){
        $this->tableName = 'WorkPackage';
        $this->query = \QueryBuilder::create();
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_NUM);
    }

    /**
     * @param WorkPackage[] $data
     * @return WorkPackageCollectionPrepare inserted Id
     */
    public static function create(){
        return new WorkPackageCollectionPrepare();
    }

    /**
     * @return int
     */
    public function getPrimaryKeyIndex(){
        return $this->primaryKeyIndex;
    }

    /**
     * @return array
     */
    public function getObjectMap(){
        return $this->objectMap;
    }

    /**
     * @return QueryBuilder
     */
    public function getQuery(){
        return $this->query;
    }

    /**
     * @return string
     */
    public function getTableName(){
        return $this->tableName;
    }

    /**
     * @return WorkPackageCollectionPrepare
     */
    public function setTableName($tableName){
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableNameAlias(){
        return ($this->tableNameAlias != '')?$this->tableNameAlias:$this->tableName;
    }

    /**
     * @return WorkPackageCollectionPrepare
     */
    public function setTableNameAlias($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias;
        return $this;
    }

    /**
     * @return string
     */
    public function setTableNameAliasPrepend($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias.(($this->tableNameAlias == '')?$this->tableName:$this->tableNameAlias);
        return $this;
    }

    /**
      * Add a basic where clause to the query.
      *
      * @param  string  $column
      * @param  string  $operator
      * @param  mixed   $value
      * @param  string  $boolean
      * @return WorkPackageCollectionPrepare
      *
      * @throws \InvalidArgumentException
      */
    public function where($column, $operator = null, $value = null, $boolean = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->where($spl[1], $operator, $value, $boolean);
        }else{
            $this->query->where($column, $operator, $value, $boolean);
        }
        return $this;
    }
    
    public function orWhere($column, $operator = null, $value = null, $logicConnector = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->orWhere($spl[1], $operator, $value, $logicConnector);
        }else{
            $this->query->orWhere($column, $operator, $value, $logicConnector);
        }
        return $this;
    }

    /**
     * @return WorkPackageCollectionPrepare
     */
    public function whereRaw($where){
        $this->query->whereRaw($where);
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return WorkPackageCollectionPrepare
     */
    public function whereUmlId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('umlId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return WorkPackageCollectionPrepare
     */
    public function orWhereUmlId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('umlId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return WorkPackageCollectionPrepare
     */
    public function selectUmlId(){
        $this->primaryKeyIndex = count($this->query->columns);
        $this->objectMap[] = array(
            'class' => 'WorkPackage',
            'field' => 'umlId',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('umlId');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return WorkPackageCollectionPrepare
     */
    public function whereFunctionContent($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('functionContent', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return WorkPackageCollectionPrepare
     */
    public function orWhereFunctionContent($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('functionContent', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return WorkPackageCollectionPrepare
     */
    public function selectFunctionContent(){
        $this->objectMap[] = array(
            'class' => 'WorkPackage',
            'field' => 'functionContent',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('functionContent');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return WorkPackageCollectionPrepare
     */
    public function whereControlContent($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('controlContent', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return WorkPackageCollectionPrepare
     */
    public function orWhereControlContent($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('controlContent', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return WorkPackageCollectionPrepare
     */
    public function selectControlContent(){
        $this->objectMap[] = array(
            'class' => 'WorkPackage',
            'field' => 'controlContent',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('controlContent');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return WorkPackageCollectionPrepare
     */
    public function whereType($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('type', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return WorkPackageCollectionPrepare
     */
    public function orWhereType($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('type', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return WorkPackageCollectionPrepare
     */
    public function selectType(){
        $this->objectMap[] = array(
            'class' => 'WorkPackage',
            'field' => 'type',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('type');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return WorkPackageCollectionPrepare
     */
    public function whereUmlElement($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('umlElement', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return WorkPackageCollectionPrepare
     */
    public function orWhereUmlElement($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('umlElement', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return WorkPackageCollectionPrepare
     */
    public function selectUmlElement(){
        $this->objectMap[] = array(
            'class' => 'WorkPackage',
            'field' => 'umlElement',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('umlElement');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return WorkPackageCollectionPrepare
     */
    public function whereFunctionIsDefault($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('functionIsDefault', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return WorkPackageCollectionPrepare
     */
    public function orWhereFunctionIsDefault($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('functionIsDefault', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return WorkPackageCollectionPrepare
     */
    public function selectFunctionIsDefault(){
        $this->objectMap[] = array(
            'class' => 'WorkPackage',
            'field' => 'functionIsDefault',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('functionIsDefault');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return WorkPackageCollectionPrepare
     */
    public function whereControlIsDefault($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('controlIsDefault', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return WorkPackageCollectionPrepare
     */
    public function orWhereControlIsDefault($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('controlIsDefault', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return WorkPackageCollectionPrepare
     */
    public function selectControlIsDefault(){
        $this->objectMap[] = array(
            'class' => 'WorkPackage',
            'field' => 'controlIsDefault',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('controlIsDefault');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return WorkPackageCollectionPrepare
     */
    public function whereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return WorkPackageCollectionPrepare
     */
    public function orWhereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return WorkPackageCollectionPrepare
     */
    public function selectName(){
        $this->objectMap[] = array(
            'class' => 'WorkPackage',
            'field' => 'name',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('name');
        return $this;
    }

    /**
     * @var ViewsCollectionPrepare
     */
    protected $views;
    /**
     * @var ViewsCollectionPrepare
     * @return WorkPackageCollectionPrepare
     */
    public function setViews(ViewsCollectionPrepare $views){
       $this->views = $views;
       $views->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getViews(){
       if(!isset($this->views)) $this->views = ViewsCollectionPrepare::create();
       return $this->views;
    }
    
    




    /**
     * @var ProjectCollectionPrepare
     */
    protected $project;
    /**
     * @var ProjectCollectionPrepare
     * @return WorkPackageCollectionPrepare
     */
    public function setProject(ProjectCollectionPrepare $project){
       $this->project = $project;
       $project->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getProject(){
       if(!isset($this->project)) $this->project = ProjectCollectionPrepare::create();
       return $this->project;
    }
    
    
    public function selectProject(){
        $this->objectMap[] = array(
            'class' => 'project',
            'field' => 'project',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('project');
        return $this;
    }




    /**
     * @var WorkPackageCollectionPrepare
     */
    protected $parent;
    /**
     * @var WorkPackageCollectionPrepare
     * @return WorkPackageCollectionPrepare
     */
    public function setParent(WorkPackageCollectionPrepare $parent){
       $this->parent = $parent;
       $parent->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getParent(){
       if(!isset($this->parent)) $this->parent = WorkPackageCollectionPrepare::create();
       return $this->parent;
    }
    
    
    public function selectParent(){
        $this->objectMap[] = array(
            'class' => 'parent',
            'field' => 'parent',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('parent');
        return $this;
    }




    /**
     * @var WorkPackageCollectionPrepare
     */
    protected $childs;
    /**
     * @var WorkPackageCollectionPrepare
     * @return WorkPackageCollectionPrepare
     */
    public function setChilds(WorkPackageCollectionPrepare $childs){
       $this->childs = $childs;
       $childs->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getChilds(){
       if(!isset($this->childs)) $this->childs = WorkPackageCollectionPrepare::create();
       return $this->childs;
    }
    
    





    private $selectAll = false;
    public function selectAll(){
        
        $this->selectUmlId();
        $this->selectFunctionContent();
        $this->selectControlContent();
        $this->selectType();
        $this->selectFunctionIsDefault();
        $this->selectControlIsDefault();
        $this->selectName();
        $this->selectProject();
        $this->selectParent();

        return $this;
    }
    public function prepareQuery(){
        $this->prepareJoins();
        $this->query->from = 'WorkPackage'.(($this->tableNameAlias == '')?'':' AS '.$this->tableNameAlias);
        QueryBuilder::appendWhereWithTable($this->query, $this->getTableNameAlias());
        return $this;
    }
    /**
     * @return WorkPackageCollection
     */
    public function get(){
        $this->prepareQuery();
        $rows = $this->query->get();
        return $this->parseDbRows($rows, $this->getObjectMap());
    }

    /**
     * parse all return rows from sql
     */
    private function parseDbRows($rows, $map){
        $objsArray = array();
        $objs = WorkPackageCollection::create();
        //build assoc Array with all data
        if(!$this->selectAll){
            foreach($rows as $row){
                $primaryField = $row[$this->primaryKeyIndex];

                //extend object if allready exists

                $initObj = (isset($objsArray[$primaryField]))?$objsArray[$primaryField]:array();
                $objsArray[$primaryField] = $this->parseDbRow($row, $map, $initObj);
            }
        }else $objsArray = $rows;
        //convert array to real objects (subobjects are generated in constructors)
        foreach($objsArray as $dataArray){
            $objs->add(new WorkPackage($dataArray));
        }
        return $objs;
    }

    /**
     * parse one row into Object and extend existing ones
     */
    private function parseDbRow($row, $map, $objsData = array(), &$index = 0){
        foreach($map as $m){
            //check if association
            if(isset($m['map'])){
                //check if cardinality is multi
                if($m['multi']){
                    if(!isset($objsData[$m['field']])) $objsData[$m['field']] = array();
                    //get primaryKey ob subobject
                    $id = $row[$index+$m['primaryKeyIndex']];
                    //extend object if allready exists
                    $initObj = (isset($objsData[$m['field']][$id]))?$objsData[$m['field']][$id]:array();
                    //add/extend object with id
                    $objsData[$m['field']][$id] = $this->parseDbRow($row, $m['map'], $initObj, $index);
                }else{
                    $objsData[$m['field']] = $this->parseDbRow($row, $m['map'], array(), $index);
                }
            }
            else{
                $objsData[$m['field']] = $row[$index];
                $index++;
            }
        }
        return $objsData;
    }

    /**
     * create joins depending on associations
     * @return CollectionPrepare
     */
    public function prepareJoins(){
        
        if(!$this->selectAll){
            if(!is_array($this->query->columns) || !in_array('umlId', $this->query->columns)){
                $this->selectumlId();
            }elseif(!is_array($this->query->columns)){
                $this->selectAll();
            }
        }else{
            $this->query->select('*');
        }
        if(isSet($this->views)){
            $tableName = $this->views->getTableNameAlias();
            //Multi
            $this->query->join('Views as '.$tableName, $tableName.'.wp', '=', $this->getTableNameAlias().'.umlId', 'LEFT');
            $this->views->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->views->getQuery(),$tableName);
            $this->query->mergeQuery($this->views->getQuery());

            $this->objectMap[] = array(
                'class' => 'Views',
                'field' => 'views',
                'multi' => true,
                'primaryKeyIndex' => $this->views->getPrimaryKeyIndex(),
                'map' => $this->views->getObjectMap()
            );
        }


        if(isSet($this->project)){
            $tableName = $this->project->getTableNameAlias();
            //Single WorkPackage
             $this->query->join('Project as '.$tableName, $tableName.'.projektId', '=', $this->getTableNameAlias().'.project', 'LEFT');
            $this->project->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->project->getQuery(),$tableName);
            $this->query->mergeQuery($this->project->getQuery());

            $this->objectMap[] = array(
                'class' => 'Project',
                'field' => 'project',
                'multi' => false,
                'primaryKeyIndex' => $this->project->getPrimaryKeyIndex(),
                'map' => $this->project->getObjectMap()
            );
        }


        if(isSet($this->parent)){
            $tableName = $this->parent->getTableNameAlias();
            //Single WorkPackage
             $this->query->join('WorkPackage as '.$tableName, $tableName.'.umlId', '=', $this->getTableNameAlias().'.parent', 'LEFT');
            $this->parent->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->parent->getQuery(),$tableName);
            $this->query->mergeQuery($this->parent->getQuery());

            $this->objectMap[] = array(
                'class' => 'WorkPackage',
                'field' => 'parent',
                'multi' => false,
                'primaryKeyIndex' => $this->parent->getPrimaryKeyIndex(),
                'map' => $this->parent->getObjectMap()
            );
        }


        if(isSet($this->childs)){
            $tableName = $this->childs->getTableNameAlias();
            //Multi
            $this->query->join('WorkPackage as '.$tableName, $tableName.'.parent', '=', $this->getTableNameAlias().'.umlId', 'LEFT');
            $this->childs->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->childs->getQuery(),$tableName);
            $this->query->mergeQuery($this->childs->getQuery());

            $this->objectMap[] = array(
                'class' => 'WorkPackage',
                'field' => 'childs',
                'multi' => true,
                'primaryKeyIndex' => $this->childs->getPrimaryKeyIndex(),
                'map' => $this->childs->getObjectMap()
            );
        }



        return $this;
    }
}
