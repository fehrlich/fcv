<?php
namespace fcv\fcvPackage;

use \QueryBuilder;

use \fcv\fcvPackage\LibraryCollectionPrepare;
use \fcv\fcvPackage\ProjectCollectionPrepare;class CodeLanguageCollectionPrepare {

    protected $tableName;
    protected $tableNameAlias = '';

    /**
     * @var QueryBuilder
     */
    protected $query;
    protected $objectMap = array();

    protected $primaryKeyIndex = 0;

    public function __construct(){
        $this->tableName = 'CodeLanguage';
        $this->query = \QueryBuilder::create();
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_NUM);
    }

    /**
     * @param CodeLanguage[] $data
     * @return CodeLanguageCollectionPrepare inserted Id
     */
    public static function create(){
        return new CodeLanguageCollectionPrepare();
    }

    /**
     * @return int
     */
    public function getPrimaryKeyIndex(){
        return $this->primaryKeyIndex;
    }

    /**
     * @return array
     */
    public function getObjectMap(){
        return $this->objectMap;
    }

    /**
     * @return QueryBuilder
     */
    public function getQuery(){
        return $this->query;
    }

    /**
     * @return string
     */
    public function getTableName(){
        return $this->tableName;
    }

    /**
     * @return CodeLanguageCollectionPrepare
     */
    public function setTableName($tableName){
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableNameAlias(){
        return ($this->tableNameAlias != '')?$this->tableNameAlias:$this->tableName;
    }

    /**
     * @return CodeLanguageCollectionPrepare
     */
    public function setTableNameAlias($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias;
        return $this;
    }

    /**
     * @return string
     */
    public function setTableNameAliasPrepend($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias.(($this->tableNameAlias == '')?$this->tableName:$this->tableNameAlias);
        return $this;
    }

    /**
      * Add a basic where clause to the query.
      *
      * @param  string  $column
      * @param  string  $operator
      * @param  mixed   $value
      * @param  string  $boolean
      * @return CodeLanguageCollectionPrepare
      *
      * @throws \InvalidArgumentException
      */
    public function where($column, $operator = null, $value = null, $boolean = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->where($spl[1], $operator, $value, $boolean);
        }else{
            $this->query->where($column, $operator, $value, $boolean);
        }
        return $this;
    }
    
    public function orWhere($column, $operator = null, $value = null, $logicConnector = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->orWhere($spl[1], $operator, $value, $logicConnector);
        }else{
            $this->query->orWhere($column, $operator, $value, $logicConnector);
        }
        return $this;
    }

    /**
     * @return CodeLanguageCollectionPrepare
     */
    public function whereRaw($where){
        $this->query->whereRaw($where);
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeLanguageCollectionPrepare
     */
    public function whereLangId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('langId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeLanguageCollectionPrepare
     */
    public function orWhereLangId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('langId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeLanguageCollectionPrepare
     */
    public function selectLangId(){
        $this->primaryKeyIndex = count($this->query->columns);
        $this->objectMap[] = array(
            'class' => 'CodeLanguage',
            'field' => 'langId',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('langId');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeLanguageCollectionPrepare
     */
    public function whereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeLanguageCollectionPrepare
     */
    public function orWhereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeLanguageCollectionPrepare
     */
    public function selectName(){
        $this->objectMap[] = array(
            'class' => 'CodeLanguage',
            'field' => 'name',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('name');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeLanguageCollectionPrepare
     */
    public function whereExtension($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('extension', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeLanguageCollectionPrepare
     */
    public function orWhereExtension($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('extension', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeLanguageCollectionPrepare
     */
    public function selectExtension(){
        $this->objectMap[] = array(
            'class' => 'CodeLanguage',
            'field' => 'extension',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('extension');
        return $this;
    }

    /**
     * @var LibraryCollectionPrepare
     */
    protected $lib;
    /**
     * @var LibraryCollectionPrepare
     * @return CodeLanguageCollectionPrepare
     */
    public function setLib(LibraryCollectionPrepare $lib){
       $this->lib = $lib;
       $lib->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getLib(){
       if(!isset($this->lib)) $this->lib = LibraryCollectionPrepare::create();
       return $this->lib;
    }
    
    
    public function selectLib(){
        $this->objectMap[] = array(
            'class' => 'lib',
            'field' => 'lib',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('lib');
        return $this;
    }



    /**
     * @var LibraryIncludeCodeCollectionPrepare
     */
    protected $libraryIncludeCode;
    /**
     * @var LibraryCollectionPrepare
     * @return libCollectionPrepare
     */
    public function setLibraryIncludeCode(LibraryIncludeCodeCollectionPrepare $libraryIncludeCode){
       $this->libraryIncludeCode = $libraryIncludeCode;
       $libraryIncludeCode->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }


    public function getLibraryIncludeCode(){
       if(!isset($this->libraryIncludeCode)) $this->libraryIncludeCode =  LibraryIncludeCodeCollectionPrepare::create();
       return $this->libraryIncludeCode;
    }
        


    /**
     * @var ProjectCollectionPrepare
     */
    protected $inProject;
    /**
     * @var ProjectCollectionPrepare
     * @return CodeLanguageCollectionPrepare
     */
    public function setInProject(ProjectCollectionPrepare $inProject){
       $this->inProject = $inProject;
       $inProject->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getInProject(){
       if(!isset($this->inProject)) $this->inProject = ProjectCollectionPrepare::create();
       return $this->inProject;
    }
    
    





    private $selectAll = false;
    public function selectAll(){
        
        $this->selectLangId();
        $this->selectName();
        $this->selectExtension();
        $this->selectLib();

        return $this;
    }
    public function prepareQuery(){
        $this->prepareJoins();
        $this->query->from = 'CodeLanguage'.(($this->tableNameAlias == '')?'':' AS '.$this->tableNameAlias);
        QueryBuilder::appendWhereWithTable($this->query, $this->getTableNameAlias());
        return $this;
    }
    /**
     * @return CodeLanguageCollection
     */
    public function get(){
        $this->prepareQuery();
        $rows = $this->query->get();
        return $this->parseDbRows($rows, $this->getObjectMap());
    }

    /**
     * parse all return rows from sql
     */
    private function parseDbRows($rows, $map){
        $objsArray = array();
        $objs = CodeLanguageCollection::create();
        //build assoc Array with all data
        if(!$this->selectAll){
            foreach($rows as $row){
                $primaryField = $row[$this->primaryKeyIndex];

                //extend object if allready exists

                $initObj = (isset($objsArray[$primaryField]))?$objsArray[$primaryField]:array();
                $objsArray[$primaryField] = $this->parseDbRow($row, $map, $initObj);
            }
        }else $objsArray = $rows;
        //convert array to real objects (subobjects are generated in constructors)
        foreach($objsArray as $dataArray){
            $objs->add(new CodeLanguage($dataArray));
        }
        return $objs;
    }

    /**
     * parse one row into Object and extend existing ones
     */
    private function parseDbRow($row, $map, $objsData = array(), &$index = 0){
        foreach($map as $m){
            //check if association
            if(isset($m['map'])){
                //check if cardinality is multi
                if($m['multi']){
                    if(!isset($objsData[$m['field']])) $objsData[$m['field']] = array();
                    //get primaryKey ob subobject
                    $id = $row[$index+$m['primaryKeyIndex']];
                    //extend object if allready exists
                    $initObj = (isset($objsData[$m['field']][$id]))?$objsData[$m['field']][$id]:array();
                    //add/extend object with id
                    $objsData[$m['field']][$id] = $this->parseDbRow($row, $m['map'], $initObj, $index);
                }else{
                    $objsData[$m['field']] = $this->parseDbRow($row, $m['map'], array(), $index);
                }
            }
            else{
                $objsData[$m['field']] = $row[$index];
                $index++;
            }
        }
        return $objsData;
    }

    /**
     * create joins depending on associations
     * @return CollectionPrepare
     */
    public function prepareJoins(){
        
        if(!$this->selectAll){
            if(!is_array($this->query->columns) || !in_array('langId', $this->query->columns)){
                $this->selectlangId();
            }elseif(!is_array($this->query->columns)){
                $this->selectAll();
            }
        }else{
            $this->query->select('*');
        }
        if(isSet($this->lib)){
            $tableName = $this->lib->getTableNameAlias();
            //Assoc    CodeLanguage
            $this->query->join('LibraryIncludeCode as LibraryIncludeCode', 'LibraryIncludeCode.codeLanguage', '=', $this->getTableNameAlias().'.langId', 'LEFT');
            $this->query->join('Library as '.$tableName, $tableName.'.libId', '=', 'LibraryIncludeCode.library', 'LEFT');
            $this->lib->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->lib->getQuery(),$tableName);
            $this->query->mergeQuery($this->lib->getQuery());

            $this->objectMap[] = array(
                'class' => 'Library',
                'field' => 'lib',
                'multi' => false,
                'primaryKeyIndex' => $this->lib->getPrimaryKeyIndex(),
                'map' => $this->lib->getObjectMap()
            );
        }

        if(isSet($this->libraryIncludeCode)){
            $tableName = $this->libraryIncludeCode->getTableNameAlias();
            //Assoc    CodeLanguage
            $this->query->join('LibraryIncludeCode as '.$tableName, $tableName.'.codeLanguage', '=', $this->getTableNameAlias().'.langId', 'LEFT');
//            $this->query->join('Library as '.$tableName, $tableName.'.libId', '=', 'LibraryIncludeCode.library', 'LEFT');

            $this->libraryIncludeCode->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->libraryIncludeCode->getQuery(),$tableName);
            $this->query->mergeQuery($this->libraryIncludeCode->getQuery());

            $this->objectMap[] = array(
                'class' => 'lib',
                'field' => 'libraryIncludeCode',
                'multi' => true,
                'primaryKeyIndex' => $this->libraryIncludeCode->getPrimaryKeyIndex(),
                'map' => $this->libraryIncludeCode->getObjectMap()
            );
        }

        if(isSet($this->inProject)){
            $tableName = $this->inProject->getTableNameAlias();
            //Multi
            $this->query->join('Project as '.$tableName, $tableName.'.usedLanguages', '=', $this->getTableNameAlias().'.langId', 'LEFT');
            $this->inProject->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->inProject->getQuery(),$tableName);
            $this->query->mergeQuery($this->inProject->getQuery());

            $this->objectMap[] = array(
                'class' => 'Project',
                'field' => 'inProject',
                'multi' => true,
                'primaryKeyIndex' => $this->inProject->getPrimaryKeyIndex(),
                'map' => $this->inProject->getObjectMap()
            );
        }



        return $this;
    }
}
