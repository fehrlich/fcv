<?php
namespace fcv\fcvPackage;


use \fcv\fcvPackage\ViewsCollection;
use \fcv\fcvPackage\ProjectCollection;
use \fcv\fcvPackage\WorkPackageCollection;
class WorkPackageCollection
        
        
        implements \Iterator, \ArrayAccess, \Countable {
    private $index = 0;
    /**
     * @var WorkPackage[] $objArray
     */
    protected $objArray = array();


    public function __construct($data = array()){
        $this->objArray = $data;
    }

    /**
     * @param WorkPackage[] $data
     * @return WorkPackageCollection inserted Id
     */
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new WorkPackageCollection($data);
    }
    /**
     * @param WorkPackage $el
     * @return WorkPackageCollection inserted Id
     */
    public function add($el){
        $this->objArray[] = $el;
    }

    public function current(): mixed {
        return $this->objArray[$this->index];
    }

    public function key(): int {
        return $this->index;
    }

    public function next(): void {
        $this->index++;
    }

    public function rewind(): void {
        $this->index = 0;
    }

    public function valid(): bool {
        return isset($this->objArray[$this->index]);
    }
    public function append($el): mixed {
        return $this->add($el);
    }
    public function count(): int {
        return count($this->objArray);
    }

    public function offsetExists(mixed $offset): bool {
        return isSet($this->objArray[$offset]);
    }

    public function offsetGet($offset): mixed {
        return $this->objArray[$offset];
    }

    public function offsetSet($offset, $value): void {
        $this->objArray[$offset] = $value;
    }

    public function offsetUnset($offset): void {
        unset($this->objArray[$offset]);
    }

    public function seek($position): void {
        $this->index = $position;
    }

    public function serialize() {
        return serialize($this->objArray);
    }

    public function unserialize($serialized) {
        return unserialize($serialized);
    }
    //UserMethods
    /**
      *
      * @return WorkPackageCollection this Collection Object
      */
    public function build($buildworkpackages = true){
        foreach($this->objArray as $el){
           $el->build($buildworkpackages);
       }
       return $this;
    }
    /**
      *
      * @return WorkPackageCollection this Collection Object
      */
    public function getPath(){
        foreach($this->objArray as $el){
           $el->getPath();
       }
       return $this;
    }
    /**
      *
      * @return WorkPackageCollection this Collection Object
      */
    public function loadContentFromSource(){
        foreach($this->objArray as $el){
           $el->loadContentFromSource();
       }
       return $this;
    }
    /**
      *
      * @return WorkPackageCollection this Collection Object
      */
    public function saveToFiles(){
        foreach($this->objArray as $el){
           $el->saveToFiles();
       }
       return $this;
    }
    /**
      *
      * @return WorkPackageCollection this Collection Object
      */
    public function getUmlElement(){
        foreach($this->objArray as $el){
           $el->getUmlElement();
       }
       return $this;
    }
    /**
      *
      * @return WorkPackageCollection this Collection Object
      */
    public function buildModules($modules = null){
        foreach($this->objArray as $el){
           $el->buildModules($modules);
       }
       return $this;
    }
    /**
      *
      * @param ModulTypes $type
      * @return WorkPackageCollection this Collection Object
      */
    public function getWorkPackagesByType( $type){
        foreach($this->objArray as $el){
           $el->getWorkPackagesByType($type);
       }
       return $this;
    }
    /**
      *
      * @param  $umlElement
      * @return WorkPackageCollection this Collection Object
      */
    public function setUmlElement( $umlElement){
        foreach($this->objArray as $el){
           $el->setUmlElement($umlElement);
       }
       return $this;
    }
    /**
      *
      * @return WorkPackageCollection this Collection Object
      */
    public function getName(){
        foreach($this->objArray as $el){
           $el->getName();
       }
       return $this;
    }

    //Setters
    /**
    *
    * @param String $umlId
    * @return WorkPackage
    */
    public function setUmlId($umlId){
       foreach($this->objArray as $el){
           $el->setUmlId($umlId);
       }
       return $this;
    }
    /**
    *
    * @param String $functionContent
    * @return WorkPackage
    */
    public function setFunctionContent($functionContent){
       foreach($this->objArray as $el){
           $el->setFunctionContent($functionContent);
       }
       return $this;
    }
    /**
    *
    * @param String $controlContent
    * @return WorkPackage
    */
    public function setControlContent($controlContent){
       foreach($this->objArray as $el){
           $el->setControlContent($controlContent);
       }
       return $this;
    }
    /**
    *
    * @param ModulTypes $type
    * @return WorkPackage
    */
    public function setType($type){
       foreach($this->objArray as $el){
           $el->setType($type);
       }
       return $this;
    }
    /**
    *
    * @param  $umlElement
    * @return WorkPackage
    */
    public function fcvsetUmlElement($umlElement){
       foreach($this->objArray as $el){
           $el->fcvsetUmlElement($umlElement);
       }
       return $this;
    }
    /**
    *
    * @param Boolean $functionIsDefault
    * @return WorkPackage
    */
    public function setFunctionIsDefault($functionIsDefault){
       foreach($this->objArray as $el){
           $el->setFunctionIsDefault($functionIsDefault);
       }
       return $this;
    }
    /**
    *
    * @param Boolean $controlIsDefault
    * @return WorkPackage
    */
    public function setControlIsDefault($controlIsDefault){
       foreach($this->objArray as $el){
           $el->setControlIsDefault($controlIsDefault);
       }
       return $this;
    }
    /**
    *
    * @param String $name
    * @return WorkPackage
    */
    public function setName($name){
       foreach($this->objArray as $el){
           $el->setName($name);
       }
       return $this;
    }
    public function setViews(Views $views){
       foreach($this->objArray as $el){
           $el->setViews($views);
       }
       return $this;
    }
    public function setProject(Project $project){
       foreach($this->objArray as $el){
           $el->setProject($project);
       }
       return $this;
    }
    public function setParent(WorkPackage $parent){
       foreach($this->objArray as $el){
           $el->setParent($parent);
       }
       return $this;
    }
    public function setChilds(WorkPackage $childs){
       foreach($this->objArray as $el){
           $el->setChilds($childs);
       }
       return $this;
    }
    public function save(){
       foreach($this->objArray as $el){
           $el->save();
       }
       return $this;
    }
    public function treeSave(){
       foreach($this->objArray as $el){
           $el->treeSave();
       }
       return $this;
    }
    public function getHtml($tplName = 'default'){
       $html = '';
       foreach($this->objArray as $el){
           $html .= $el->getHtml($tplName);
       }
       return $html;
    }
    
    public function each($cb){
        foreach($this->objArray as $i => $o){
            $cb($o,$i);
        }
        return $this;
    }
    
    public function addToJs(){
        foreach($this->objArray as $i => $o){
            $o->addToJs();
        }
        return $this;
    }
    //Map Reduce Pattern
    public function map($mapFunction){
        $ret = array();
        foreach($this->objArray as $i => $o){
            $ret[$i] = $mapFunction($o);
        }
        return $ret;
    }
    
    public function reduce($reduceFunction){

        $length = count($this->objArray);
        if($length <= 1) return $this->objArray;

        $array = $this->objArray;
        $out = array_shift($array);
        do {
            $next = array_shift($array);
            $out = $reduceFunction($out, $next);
        } while(!empty($array));

        return $out;
    }

    public function getArray(){
        return $this->objArray;
    }
}
