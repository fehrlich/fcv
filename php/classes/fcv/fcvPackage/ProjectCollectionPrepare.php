<?php
namespace fcv\fcvPackage;

use \QueryBuilder;

use \fcv\fcvPackage\BuildCollectionPrepare;
use \fcv\fcvPackage\WorkPackageCollectionPrepare;
use \fcv\fcvPackage\LibraryCollectionPrepare;
use \fcv\fcvPackage\CodeLanguageCollectionPrepare;class ProjectCollectionPrepare {

    protected $tableName;
    protected $tableNameAlias = '';

    /**
     * @var QueryBuilder
     */
    protected $query;
    protected $objectMap = array();

    protected $primaryKeyIndex = 0;

    public function __construct(){
        $this->tableName = 'Project';
        $this->query = \QueryBuilder::create();
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_NUM);
    }

    /**
     * @param Project[] $data
     * @return ProjectCollectionPrepare inserted Id
     */
    public static function create(){
        return new ProjectCollectionPrepare();
    }

    /**
     * @return int
     */
    public function getPrimaryKeyIndex(){
        return $this->primaryKeyIndex;
    }

    /**
     * @return array
     */
    public function getObjectMap(){
        return $this->objectMap;
    }

    /**
     * @return QueryBuilder
     */
    public function getQuery(){
        return $this->query;
    }

    /**
     * @return string
     */
    public function getTableName(){
        return $this->tableName;
    }

    /**
     * @return ProjectCollectionPrepare
     */
    public function setTableName($tableName){
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableNameAlias(){
        return ($this->tableNameAlias != '')?$this->tableNameAlias:$this->tableName;
    }

    /**
     * @return ProjectCollectionPrepare
     */
    public function setTableNameAlias($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias;
        return $this;
    }

    /**
     * @return string
     */
    public function setTableNameAliasPrepend($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias.(($this->tableNameAlias == '')?$this->tableName:$this->tableNameAlias);
        return $this;
    }

    /**
      * Add a basic where clause to the query.
      *
      * @param  string  $column
      * @param  string  $operator
      * @param  mixed   $value
      * @param  string  $boolean
      * @return ProjectCollectionPrepare
      *
      * @throws \InvalidArgumentException
      */
    public function where($column, $operator = null, $value = null, $boolean = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->where($spl[1], $operator, $value, $boolean);
        }else{
            $this->query->where($column, $operator, $value, $boolean);
        }
        return $this;
    }
    
    public function orWhere($column, $operator = null, $value = null, $logicConnector = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->orWhere($spl[1], $operator, $value, $logicConnector);
        }else{
            $this->query->orWhere($column, $operator, $value, $logicConnector);
        }
        return $this;
    }

    /**
     * @return ProjectCollectionPrepare
     */
    public function whereRaw($where){
        $this->query->whereRaw($where);
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ProjectCollectionPrepare
     */
    public function whereProjektId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('projektId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ProjectCollectionPrepare
     */
    public function orWhereProjektId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('projektId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ProjectCollectionPrepare
     */
    public function selectProjektId(){
        $this->primaryKeyIndex = count($this->query->columns);
        $this->objectMap[] = array(
            'class' => 'Project',
            'field' => 'projektId',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('projektId');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ProjectCollectionPrepare
     */
    public function whereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ProjectCollectionPrepare
     */
    public function orWhereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ProjectCollectionPrepare
     */
    public function selectName(){
        $this->objectMap[] = array(
            'class' => 'Project',
            'field' => 'name',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('name');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ProjectCollectionPrepare
     */
    public function whereUmlPath($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('umlPath', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ProjectCollectionPrepare
     */
    public function orWhereUmlPath($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('umlPath', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ProjectCollectionPrepare
     */
    public function selectUmlPath(){
        $this->objectMap[] = array(
            'class' => 'Project',
            'field' => 'umlPath',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('umlPath');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ProjectCollectionPrepare
     */
    public function whereMysqlModel($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('mysqlModel', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ProjectCollectionPrepare
     */
    public function orWhereMysqlModel($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('mysqlModel', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ProjectCollectionPrepare
     */
    public function selectMysqlModel(){
        $this->objectMap[] = array(
            'class' => 'Project',
            'field' => 'mysqlModel',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('mysqlModel');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ProjectCollectionPrepare
     */
    public function whereUmlModel($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('umlModel', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ProjectCollectionPrepare
     */
    public function orWhereUmlModel($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('umlModel', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ProjectCollectionPrepare
     */
    public function selectUmlModel(){
        $this->objectMap[] = array(
            'class' => 'Project',
            'field' => 'umlModel',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('umlModel');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ProjectCollectionPrepare
     */
    public function whereMysqlDb($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('mysqlDb', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ProjectCollectionPrepare
     */
    public function orWhereMysqlDb($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('mysqlDb', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ProjectCollectionPrepare
     */
    public function selectMysqlDb(){
        $this->objectMap[] = array(
            'class' => 'Project',
            'field' => 'mysqlDb',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('mysqlDb');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ProjectCollectionPrepare
     */
    public function whereMysqlUser($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('mysqlUser', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ProjectCollectionPrepare
     */
    public function orWhereMysqlUser($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('mysqlUser', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ProjectCollectionPrepare
     */
    public function selectMysqlUser(){
        $this->objectMap[] = array(
            'class' => 'Project',
            'field' => 'mysqlUser',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('mysqlUser');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ProjectCollectionPrepare
     */
    public function whereMysqlPassword($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('mysqlPassword', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ProjectCollectionPrepare
     */
    public function orWhereMysqlPassword($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('mysqlPassword', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ProjectCollectionPrepare
     */
    public function selectMysqlPassword(){
        $this->objectMap[] = array(
            'class' => 'Project',
            'field' => 'mysqlPassword',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('mysqlPassword');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ProjectCollectionPrepare
     */
    public function whereMysqlHost($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('mysqlHost', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ProjectCollectionPrepare
     */
    public function orWhereMysqlHost($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('mysqlHost', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ProjectCollectionPrepare
     */
    public function selectMysqlHost(){
        $this->objectMap[] = array(
            'class' => 'Project',
            'field' => 'mysqlHost',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('mysqlHost');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ProjectCollectionPrepare
     */
    public function whereMysqlPort($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('mysqlPort', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ProjectCollectionPrepare
     */
    public function orWhereMysqlPort($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('mysqlPort', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ProjectCollectionPrepare
     */
    public function selectMysqlPort(){
        $this->objectMap[] = array(
            'class' => 'Project',
            'field' => 'mysqlPort',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('mysqlPort');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ProjectCollectionPrepare
     */
    public function whereUmlElement($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('umlElement', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ProjectCollectionPrepare
     */
    public function orWhereUmlElement($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('umlElement', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ProjectCollectionPrepare
     */
    public function selectUmlElement(){
        $this->objectMap[] = array(
            'class' => 'Project',
            'field' => 'umlElement',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('umlElement');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ProjectCollectionPrepare
     */
    public function whereBuildPath($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('buildPath', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ProjectCollectionPrepare
     */
    public function orWhereBuildPath($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('buildPath', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ProjectCollectionPrepare
     */
    public function selectBuildPath(){
        $this->objectMap[] = array(
            'class' => 'Project',
            'field' => 'buildPath',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('buildPath');
        return $this;
    }

    /**
     * @var BuildCollectionPrepare
     */
    protected $builds;
    /**
     * @var BuildCollectionPrepare
     * @return ProjectCollectionPrepare
     */
    public function setBuilds(BuildCollectionPrepare $builds){
       $this->builds = $builds;
       $builds->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getBuilds(){
       if(!isset($this->builds)) $this->builds = BuildCollectionPrepare::create();
       return $this->builds;
    }
    
    




    /**
     * @var WorkPackageCollectionPrepare
     */
    protected $workPackages;
    /**
     * @var WorkPackageCollectionPrepare
     * @return ProjectCollectionPrepare
     */
    public function setWorkPackages(WorkPackageCollectionPrepare $workPackages){
       $this->workPackages = $workPackages;
       $workPackages->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getWorkPackages(){
       if(!isset($this->workPackages)) $this->workPackages = WorkPackageCollectionPrepare::create();
       return $this->workPackages;
    }
    
    




    /**
     * @var LibraryCollectionPrepare
     */
    protected $usedLibs;
    /**
     * @var LibraryCollectionPrepare
     * @return ProjectCollectionPrepare
     */
    public function setUsedLibs(LibraryCollectionPrepare $usedLibs){
       $this->usedLibs = $usedLibs;
       $usedLibs->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getUsedLibs(){
       if(!isset($this->usedLibs)) $this->usedLibs = LibraryCollectionPrepare::create();
       return $this->usedLibs;
    }
    
    




    /**
     * @var CodeLanguageCollectionPrepare
     */
    protected $usedLanguages;
    /**
     * @var CodeLanguageCollectionPrepare
     * @return ProjectCollectionPrepare
     */
    public function setUsedLanguages(CodeLanguageCollectionPrepare $usedLanguages){
       $this->usedLanguages = $usedLanguages;
       $usedLanguages->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getUsedLanguages(){
       if(!isset($this->usedLanguages)) $this->usedLanguages = CodeLanguageCollectionPrepare::create();
       return $this->usedLanguages;
    }
    
    





    private $selectAll = false;
    public function selectAll(){
        
        $this->selectProjektId();
        $this->selectName();
        $this->selectUmlPath();
        $this->selectMysqlModel();
        $this->selectUmlModel();
        $this->selectMysqlDb();
        $this->selectMysqlUser();
        $this->selectMysqlPassword();
        $this->selectMysqlHost();
        $this->selectMysqlPort();
        $this->selectUmlElement();
        $this->selectBuildPath();

        return $this;
    }
    public function prepareQuery(){
        $this->prepareJoins();
        $this->query->from = 'Project'.(($this->tableNameAlias == '')?'':' AS '.$this->tableNameAlias);
        QueryBuilder::appendWhereWithTable($this->query, $this->getTableNameAlias());
        return $this;
    }
    /**
     * @return ProjectCollection
     */
    public function get(){
        $this->prepareQuery();
        $rows = $this->query->get();
        return $this->parseDbRows($rows, $this->getObjectMap());
    }

    /**
     * parse all return rows from sql
     */
    private function parseDbRows($rows, $map){
        $objsArray = array();
        $objs = ProjectCollection::create();
        //build assoc Array with all data
        if(!$this->selectAll){
            foreach($rows as $row){
                $primaryField = $row[$this->primaryKeyIndex];

                //extend object if allready exists

                $initObj = (isset($objsArray[$primaryField]))?$objsArray[$primaryField]:array();
                $objsArray[$primaryField] = $this->parseDbRow($row, $map, $initObj);
            }
        }else $objsArray = $rows;
        //convert array to real objects (subobjects are generated in constructors)
        foreach($objsArray as $dataArray){
            $objs->add(new Project($dataArray));
        }
        return $objs;
    }

    /**
     * parse one row into Object and extend existing ones
     */
    private function parseDbRow($row, $map, $objsData = array(), &$index = 0){
        foreach($map as $m){
            //check if association
            if(isset($m['map'])){
                //check if cardinality is multi
                if($m['multi']){
                    if(!isset($objsData[$m['field']])) $objsData[$m['field']] = array();
                    //get primaryKey ob subobject
                    $id = $row[$index+$m['primaryKeyIndex']];
                    //extend object if allready exists
                    $initObj = (isset($objsData[$m['field']][$id]))?$objsData[$m['field']][$id]:array();
                    //add/extend object with id
                    $objsData[$m['field']][$id] = $this->parseDbRow($row, $m['map'], $initObj, $index);
                }else{
                    $objsData[$m['field']] = $this->parseDbRow($row, $m['map'], array(), $index);
                }
            }
            else{
                $objsData[$m['field']] = $row[$index];
                $index++;
            }
        }
        return $objsData;
    }

    /**
     * create joins depending on associations
     * @return CollectionPrepare
     */
    public function prepareJoins(){
        
        if(!$this->selectAll){
            if(!is_array($this->query->columns) || !in_array('projektId', $this->query->columns)){
                $this->selectprojektId();
            }elseif(!is_array($this->query->columns)){
                $this->selectAll();
            }
        }else{
            $this->query->select('*');
        }
        if(isSet($this->builds)){
            $tableName = $this->builds->getTableNameAlias();
            //Multi
            $this->query->join('Build as '.$tableName, $tableName.'.project', '=', $this->getTableNameAlias().'.projektId', 'LEFT');
            $this->builds->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->builds->getQuery(),$tableName);
            $this->query->mergeQuery($this->builds->getQuery());

            $this->objectMap[] = array(
                'class' => 'Build',
                'field' => 'builds',
                'multi' => true,
                'primaryKeyIndex' => $this->builds->getPrimaryKeyIndex(),
                'map' => $this->builds->getObjectMap()
            );
        }


        if(isSet($this->workPackages)){
            $tableName = $this->workPackages->getTableNameAlias();
            //Multi
            $this->query->join('WorkPackage as '.$tableName, $tableName.'.project', '=', $this->getTableNameAlias().'.projektId', 'LEFT');
            $this->workPackages->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->workPackages->getQuery(),$tableName);
            $this->query->mergeQuery($this->workPackages->getQuery());

            $this->objectMap[] = array(
                'class' => 'WorkPackage',
                'field' => 'workPackages',
                'multi' => true,
                'primaryKeyIndex' => $this->workPackages->getPrimaryKeyIndex(),
                'map' => $this->workPackages->getObjectMap()
            );
        }


        if(isSet($this->usedLibs)){
            $tableName = $this->usedLibs->getTableNameAlias();
            //Multi
            $this->query->join('Library as '.$tableName, $tableName.'.projects', '=', $this->getTableNameAlias().'.projektId', 'LEFT');
            $this->usedLibs->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->usedLibs->getQuery(),$tableName);
            $this->query->mergeQuery($this->usedLibs->getQuery());

            $this->objectMap[] = array(
                'class' => 'Library',
                'field' => 'usedLibs',
                'multi' => true,
                'primaryKeyIndex' => $this->usedLibs->getPrimaryKeyIndex(),
                'map' => $this->usedLibs->getObjectMap()
            );
        }


        if(isSet($this->usedLanguages)){
            $tableName = $this->usedLanguages->getTableNameAlias();
            //Multi
            $this->query->join('CodeLanguage as '.$tableName, $tableName.'.inProject', '=', $this->getTableNameAlias().'.projektId', 'LEFT');
            $this->usedLanguages->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->usedLanguages->getQuery(),$tableName);
            $this->query->mergeQuery($this->usedLanguages->getQuery());

            $this->objectMap[] = array(
                'class' => 'CodeLanguage',
                'field' => 'usedLanguages',
                'multi' => true,
                'primaryKeyIndex' => $this->usedLanguages->getPrimaryKeyIndex(),
                'map' => $this->usedLanguages->getObjectMap()
            );
        }



        return $this;
    }
}
