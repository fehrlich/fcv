<?php
namespace fcv\fcvPackage;

use \QueryBuilder;

use \fcv\fcvPackage\WorkPackageCollectionPrepare;class ViewsCollectionPrepare {

    protected $tableName;
    protected $tableNameAlias = '';

    /**
     * @var QueryBuilder
     */
    protected $query;
    protected $objectMap = array();

    protected $primaryKeyIndex = 0;

    public function __construct(){
        $this->tableName = 'Views';
        $this->query = \QueryBuilder::create();
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_NUM);
    }

    /**
     * @param Views[] $data
     * @return ViewsCollectionPrepare inserted Id
     */
    public static function create(){
        return new ViewsCollectionPrepare();
    }

    /**
     * @return int
     */
    public function getPrimaryKeyIndex(){
        return $this->primaryKeyIndex;
    }

    /**
     * @return array
     */
    public function getObjectMap(){
        return $this->objectMap;
    }

    /**
     * @return QueryBuilder
     */
    public function getQuery(){
        return $this->query;
    }

    /**
     * @return string
     */
    public function getTableName(){
        return $this->tableName;
    }

    /**
     * @return ViewsCollectionPrepare
     */
    public function setTableName($tableName){
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableNameAlias(){
        return ($this->tableNameAlias != '')?$this->tableNameAlias:$this->tableName;
    }

    /**
     * @return ViewsCollectionPrepare
     */
    public function setTableNameAlias($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias;
        return $this;
    }

    /**
     * @return string
     */
    public function setTableNameAliasPrepend($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias.(($this->tableNameAlias == '')?$this->tableName:$this->tableNameAlias);
        return $this;
    }

    /**
      * Add a basic where clause to the query.
      *
      * @param  string  $column
      * @param  string  $operator
      * @param  mixed   $value
      * @param  string  $boolean
      * @return ViewsCollectionPrepare
      *
      * @throws \InvalidArgumentException
      */
    public function where($column, $operator = null, $value = null, $boolean = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->where($spl[1], $operator, $value, $boolean);
        }else{
            $this->query->where($column, $operator, $value, $boolean);
        }
        return $this;
    }
    
    public function orWhere($column, $operator = null, $value = null, $logicConnector = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->orWhere($spl[1], $operator, $value, $logicConnector);
        }else{
            $this->query->orWhere($column, $operator, $value, $logicConnector);
        }
        return $this;
    }

    /**
     * @return ViewsCollectionPrepare
     */
    public function whereRaw($where){
        $this->query->whereRaw($where);
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ViewsCollectionPrepare
     */
    public function whereViewId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('viewId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ViewsCollectionPrepare
     */
    public function orWhereViewId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('viewId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ViewsCollectionPrepare
     */
    public function selectViewId(){
        $this->primaryKeyIndex = count($this->query->columns);
        $this->objectMap[] = array(
            'class' => 'Views',
            'field' => 'viewId',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('viewId');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ViewsCollectionPrepare
     */
    public function whereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ViewsCollectionPrepare
     */
    public function orWhereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ViewsCollectionPrepare
     */
    public function selectName(){
        $this->objectMap[] = array(
            'class' => 'Views',
            'field' => 'name',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('name');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ViewsCollectionPrepare
     */
    public function whereHtmlContent($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('htmlContent', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ViewsCollectionPrepare
     */
    public function orWhereHtmlContent($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('htmlContent', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ViewsCollectionPrepare
     */
    public function selectHtmlContent(){
        $this->objectMap[] = array(
            'class' => 'Views',
            'field' => 'htmlContent',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('htmlContent');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ViewsCollectionPrepare
     */
    public function whereCssContent($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('cssContent', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ViewsCollectionPrepare
     */
    public function orWhereCssContent($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('cssContent', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ViewsCollectionPrepare
     */
    public function selectCssContent(){
        $this->objectMap[] = array(
            'class' => 'Views',
            'field' => 'cssContent',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('cssContent');
        return $this;
    }

    /**
     * @var WorkPackageCollectionPrepare
     */
    protected $wp;
    /**
     * @var WorkPackageCollectionPrepare
     * @return ViewsCollectionPrepare
     */
    public function setWp(WorkPackageCollectionPrepare $wp){
       $this->wp = $wp;
       $wp->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getWp(){
       if(!isset($this->wp)) $this->wp = WorkPackageCollectionPrepare::create();
       return $this->wp;
    }
    
    
    public function selectWp(){
        $this->objectMap[] = array(
            'class' => 'wp',
            'field' => 'wp',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('wp');
        return $this;
    }





    private $selectAll = false;
    public function selectAll(){
        
        $this->selectViewId();
        $this->selectName();
        $this->selectHtmlContent();
        $this->selectCssContent();
        $this->selectWp();

        return $this;
    }
    public function prepareQuery(){
        $this->prepareJoins();
        $this->query->from = 'Views'.(($this->tableNameAlias == '')?'':' AS '.$this->tableNameAlias);
        QueryBuilder::appendWhereWithTable($this->query, $this->getTableNameAlias());
        return $this;
    }
    /**
     * @return ViewsCollection
     */
    public function get(){
        $this->prepareQuery();
        $rows = $this->query->get();
        return $this->parseDbRows($rows, $this->getObjectMap());
    }

    /**
     * parse all return rows from sql
     */
    private function parseDbRows($rows, $map){
        $objsArray = array();
        $objs = ViewsCollection::create();
        //build assoc Array with all data
        if(!$this->selectAll){
            foreach($rows as $row){
                $primaryField = $row[$this->primaryKeyIndex];

                //extend object if allready exists

                $initObj = (isset($objsArray[$primaryField]))?$objsArray[$primaryField]:array();
                $objsArray[$primaryField] = $this->parseDbRow($row, $map, $initObj);
            }
        }else $objsArray = $rows;
        //convert array to real objects (subobjects are generated in constructors)
        foreach($objsArray as $dataArray){
            $objs->add(new Views($dataArray));
        }
        return $objs;
    }

    /**
     * parse one row into Object and extend existing ones
     */
    private function parseDbRow($row, $map, $objsData = array(), &$index = 0){
        foreach($map as $m){
            //check if association
            if(isset($m['map'])){
                //check if cardinality is multi
                if($m['multi']){
                    if(!isset($objsData[$m['field']])) $objsData[$m['field']] = array();
                    //get primaryKey ob subobject
                    $id = $row[$index+$m['primaryKeyIndex']];
                    //extend object if allready exists
                    $initObj = (isset($objsData[$m['field']][$id]))?$objsData[$m['field']][$id]:array();
                    //add/extend object with id
                    $objsData[$m['field']][$id] = $this->parseDbRow($row, $m['map'], $initObj, $index);
                }else{
                    $objsData[$m['field']] = $this->parseDbRow($row, $m['map'], array(), $index);
                }
            }
            else{
                $objsData[$m['field']] = $row[$index];
                $index++;
            }
        }
        return $objsData;
    }

    /**
     * create joins depending on associations
     * @return CollectionPrepare
     */
    public function prepareJoins(){
        
        if(!$this->selectAll){
            if(!is_array($this->query->columns) || !in_array('viewId', $this->query->columns)){
                $this->selectviewId();
            }elseif(!is_array($this->query->columns)){
                $this->selectAll();
            }
        }else{
            $this->query->select('*');
        }
        if(isSet($this->wp)){
            $tableName = $this->wp->getTableNameAlias();
            //Single Views
             $this->query->join('WorkPackage as '.$tableName, $tableName.'.umlId', '=', $this->getTableNameAlias().'.wp', 'LEFT');
            $this->wp->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->wp->getQuery(),$tableName);
            $this->query->mergeQuery($this->wp->getQuery());

            $this->objectMap[] = array(
                'class' => 'WorkPackage',
                'field' => 'wp',
                'multi' => false,
                'primaryKeyIndex' => $this->wp->getPrimaryKeyIndex(),
                'map' => $this->wp->getObjectMap()
            );
        }



        return $this;
    }
}
