<?php
namespace fcv\fcvPackage; ///fcv/fcvPackage/Views
    
use Exception;
use mys;
use QueryBuilder;
use fcv\libs\TemplateEngine;
use fcv\libs\Debug;
use fcv\FCVJsObj;


use fcv\fcvPackage\WorkPackage;
;

class Views
        
         
        {
    
    //Attributes
    //const VIEWID = 'viewId';
    //const NAME = 'name';
    //const HTMLCONTENT = 'htmlContent';
    //const CSSCONTENT = 'cssContent';
    
    //Litearls
    
    //Attributes
    /**
     *
     * @var Integer
     */
     public $viewId;
    const FIELD_VIEWID = 'viewId';
    /**
     *
     * @var String
     */
     public $name;
    const FIELD_NAME = 'name';
    /**
     *
     * @var String
     */
     public $htmlContent;
    const FIELD_HTMLCONTENT = 'htmlContent';
    /**
     *
     * @var String
     */
     public $cssContent;
    const FIELD_CSSCONTENT = 'cssContent';
    /**
     *
     * @var String
     */
     public $phpContent;
    const FIELD_PHPCONTENT = 'phpContent';
    
    //AssociationsEnds
    
    /**
     * @var WorkPackage
     */
    private $wp = false;
    const WP = 'wp';
    
    //{/isAssociationClass}}
    //FCV Attributes
    private $apPath = 'FCVAPs/';
    //    public $VIEW = array();
    static $fcvFields = array('viewId','name','htmlContent','cssContent', 'phpContent');
    //Todo: Outsource
    protected $loadObject = false;
    private $loadedAttributes = array();
    
    //Methods
    //UserOperations
    private $lastModifiedTime = false;
    private $createdTime = false;
    public function __construct($data = array(),$calledFromChild = false){
        if(is_string($data)) $data = json_decode($data,true);
        if(count($data) > 0){
            if(isSet($data['loadObj']) && $data['loadObj']){
                $this->loadObject = true;
            }
            
        //        if(isset($data['viewId']) && !empty($data['viewId']))
    
            if(isSet($data['viewId'])) $this->viewId = $data['viewId'];
            if(isSet($data['name'])) $this->name = $data['name'];
            if(isSet($data['htmlContent'])) $this->htmlContent = $data['htmlContent'];
            if(isSet($data['cssContent'])) $this->cssContent = $data['cssContent'];
            if(isSet($data['phpContent'])) $this->phpContent = $data['phpContent'];
            //associationEnds
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['wp'])){
                $this->wp = (is_array($data['wp']))?wp::create($data['wp'],$this->loadObject):$data['wp'];
            }*/
    
            if(isSet($data['wp'])){
                $this->wp = (is_array($data['wp']))?WorkPackage::create($data['wp'],$this->loadObject):$data['wp'];
            }
    
            //Helper
            if(isSet($data['lastModified'])) $this->lastModifiedTime = $data['lastModified'];
            if(isSet($data['created'])) $this->createdTime = $data['created'];
        }
    
    
    
    
    }
    
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new Views($data);
    }
    
    public static function createLoad(){
        $data = array(
            'loadObj' => true
        );
        return new Views($data);
    }
    
    public function getLastModifiedTime(){
        return $this->lastModifiedTime;
    }
    
    public function setLastModifiedTime($time){
        $this->lastModifiedTime = $time;
        return $this;
    }
    
    public function hasPrimaryKey(){
        return true;
    }
    
    public function getId(){
        return $this->viewId;
    }
    public function setId($id){
    //        Views::$loadedInstances[$id] = &$this;
        $this->viewId = $id;
        if($this->loadObject) $this->loadedAttributes['viewId'] = true;
        return $this;
    }
    public function getPrimaryKeyName(){
        return "viewId";
    }
    
    public function addToJs($selectionString = ''){
    //    if(!$calledFromChild && get_class($this) == '' && (!$this->loadObject))
            FCVJsObj::add('', $this, $selectionString);
            return $this;
    }
    public function throwError($msg){
        if(!Debug::getObj()->getThrowErrors()) return "";
        else{
            throw new Exception($msg);
        }
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return WorkPackage
     * @throws Exception
     */
    public function getWp($forceLoad = false){
        if(is_null($this->wp) || empty($this->wp)){
            $this->throwError('class Views with id '.$this->getId().' has no Attributes wp');
            //throw new Exception('class Views with id '.$this->getId().' has no Attributes wp');
        }
        return $this->wp;
    }
    
    public function hasWp($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getWp($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param WorkPackage $workPackage
     */
    public function setWp(WorkPackage $wp){
        $this->wp = $wp;
        if($this->loadObject) $this->loadedAttributes['wp'] = true;
        return $this;
    }
    
    //Getter & Setter
    
    public function get($name){
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            return $this->$name->get($subName);
        }else return $this->$name;
    }
    
    public function has($name){
        if(strpos('.', $name) !== false){
            $spl = explode('.', $name, 2);
            return $this->get($spl[0])->has($spl[1]);
        }
        return (property_exists($this, $name));
    }
    
    public function set($name, $value){
        $possibleFields = array(
        'viewId',
        'name',
        'htmlContent',
        'cssContent',
        'phpContent',
        'wp',
        );
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class Views');
            if(!$this->$name){
                if(false);
                elseif($name == 'wp'){
                    $this->wp = WorkPackage::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['wp'] = true;
                }
                else die('Error');
            }else $this->$name->set($subName,$value);
        }else{
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class Views');
            $this->$name = $value;
            if($this->loadObject) $this->loadedAttributes[$name] = true;
        }
        return $this;
    }
    
    /**
     *
     * @return Integer
     * @throws Exception
     */
     public function getViewId(){
        return $this->viewId;
     }
    
     public function hasViewId(){
        try {
            $this->getViewId();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Integer $viewId
     * @return Views
     */
     public function setViewId($viewId){
        ;
        $this->viewId = $viewId;
        if($this->loadObject) $this->loadedAttributes['viewId'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getName(){
        return $this->name;
     }
    
     public function hasName(){
        try {
            $this->getName();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $name
     * @return Views
     */
     public function setName($name){
        ;
        $this->name = $name;
        if($this->loadObject) $this->loadedAttributes['name'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getHtmlContent(){
        return $this->htmlContent;
     }
    
     public function hasHtmlContent(){
        try {
            $this->getHtmlContent();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $htmlContent
     * @return Views
     */
     public function setHtmlContent($htmlContent){
        ;
        $this->htmlContent = $htmlContent;
        if($this->loadObject) $this->loadedAttributes['htmlContent'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getCssContent(){
        return $this->cssContent;
     }
    
     public function hasCssContent(){
        try {
            $this->getCssContent();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    

    /**
     *
     * @param String $cssContent
     * @return Views
     */
    public function setCssContent($cssContent){
        ;
        $this->cssContent = $cssContent;
        if($this->loadObject) $this->loadedAttributes['cssContent'] = true;
        return $this;
     }
    
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getPhpContent(){
        return $this->phpContent;
     }
    
     public function hasPhpContent(){
        try {
            $this->getPhpContent();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    /**
     *
     * @param String $phpContent
     * @return Views
     */
    public function setPhpContent($phpContent){
        ;
        $this->phpContent = $phpContent;
        if($this->loadObject) $this->loadedAttributes['phpContent'] = true;
        return $this;
     }
    
    protected $visitedByToArray = false;
    public function toJSObject($returnArray = false){
        if($this->loadObject) return false;
        if($this->visitedByToArray){
    //            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return '[VALENCODE]jsModel["Views.'.$this->getId().'"][VALENCODE]';
                    return array(
                        'class' => 'Views',
                        'id' => $this->getId()
                    );
                }
                return false;
    //            }
    //            return array(
    //                'class' => 'Views',
    //                'id' => rand()
    //            );
        }
        $retArray = array();
        
        $this->visitedByToArray = true;
        
        if(isSet($this->viewId) && !is_null($this->viewId)) $retArray['viewId'] = (!is_object($this->viewId))?str_replace('"', '\"', $this->viewId):($this->viewId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (!is_object($this->name))?str_replace('"', '\"', $this->name):($this->name);
        if(isSet($this->htmlContent) && !is_null($this->htmlContent)) $retArray['htmlContent'] = (!is_object($this->htmlContent))?str_replace('"', '\"', $this->htmlContent):($this->htmlContent);
        if(isSet($this->cssContent) && !is_null($this->cssContent)) $retArray['cssContent'] = (!is_object($this->cssContent))?str_replace('"', '\"', $this->cssContent):($this->cssContent);
    
        if(isSet($this->wp) && !is_null($this->wp) && $this->wp !== false){
            if(!is_object($this->wp)) $val = $this->wp;
            else{
                $val = $this->wp->toJSObject();
            }
            $retArray['wp'] = $val;
            //$retArray['wp'] = ($simple)?$this->wp->getId():$this->wp->toJSObject($simple, $treeVisit);
    
         }
        return ($returnArray)?$retArray:'[VALENCODE]new Views('.json_encode($retArray).')[VALENCODE]';
    }
    
    public function toArray($simple = false, $treeVisit = false){
        if($this->visitedByToArray){
            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return array(
    //                        'class' => 'Views',
    //                        'id' => $this->getId()
    //                    );
                    return '[VALENCODE]jsModel["Views"]["'.$this->getId().'"][VALENCODE]';
                }
            }
            return false;
        }
        if($treeVisit) $this->visitedByToArray = true;
        $retArray = array();
        
        if(isSet($this->viewId) && !is_null($this->viewId)) $retArray['viewId'] = (is_numeric($this->viewId))?$this->viewId:($this->viewId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (is_numeric($this->name))?$this->name:($this->name);
        if(isSet($this->htmlContent) && !is_null($this->htmlContent)) $retArray['htmlContent'] = (is_numeric($this->htmlContent))?$this->htmlContent:($this->htmlContent);
        if(isSet($this->cssContent) && !is_null($this->cssContent)) $retArray['cssContent'] = (is_numeric($this->cssContent))?$this->cssContent:($this->cssContent);
        if(!$simple && isSet($this->wp) && !is_null($this->wp) && $this->wp !== false){
            if(is_numeric($this->wp)) $val = $this->wp;
            else{
                $val = ($simple)?$this->wp->getId():$this->wp->toArray($simple, $treeVisit);
            }
            $retArray['wp'] = $val;
            //$retArray['wp'] = ($simple)?$this->wp->getId():$this->wp->toArray($simple, $treeVisit);
    
         }
        return $retArray;
    }private $tplVars = array();
    public function addTemplateVar($name, $content){
        $this->tplVars[$name] = $content;
    }
    
    private $ignoreErrors = false;
    /**
     * get Html Template
     * @param string $template template name
     */
    public function getHtml($template = "default"){
        $availableTemplates =  array(
            'default'
            ,'default'
        );
        if(!in_array($template, $availableTemplates)){
            throw new Exception('Template <b>'.$template.'</b> doesn\'t exist for <b>Views</b>');
        }
        $m = new TemplateEngine;
        
        $templates = array(
            'default' => '' //825
        );
        $tplSrc = $templates[$template];
        
        foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, $this);
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    }
    
    public static function getSubHtml($operationName, $template = 'default'){
    
        
        $availableTemplates =  array(
        );
        if(!isset($availableTemplates[$operationName]) || !in_array($template, $availableTemplates[$operationName])){
            throw new Exception('Template OperationTemplate <b>'.$template.'</b> doesn\'t exist for <b>Views.'.$operationName.'</b>');
        }
        $m = new TemplateEngine;
        $tplSrc = file_get_contents('FCVAPs/fcv/fcvPackage/Views/'.$operationName.'/'.$operationName.'.'.$template.'.html');
        /*foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }*/
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, array());
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    
    }
}
