<?php
namespace fcv\fcvPackage; ///fcv/fcvPackage/CodeModul
    
use Exception;
use mys;
use QueryBuilder;
use fcv\libs\TemplateEngine;
use fcv\FCVJsObj;


use fcv\fcvPackage\ModulPosition;
use fcv\fcvPackage\ModulPositionCollection;
use fcv\fcvPackage\Build;
use fcv\fcvPackage\BuildCollection;
use fcv\fcvPackage\CodeModul;
use fcv\fcvPackage\CodeModulCollection;
;
use fcv\fcvPackage\CodeTemplate;
use fcv\fcvPackage\CodeTemplateCollection;
use fcv\fcvPackage\WorkPackage;
use fcv\fcvPackage\Project;
use fcv\libs\Debug;

class CodeModul
        
         
        {
    
    //Attributes
    //const MODULID = 'modulId';
    //const NAME = 'name';
    //const PATH = 'path';
    //const TYPE = 'type';
    //const APPLYTO = 'applyTo';
    //const WORKPACKAGE = 'workPackage';
    //const BUILDONLYONFIRSTBUILD = 'buildOnlyOnFirstBuild';
    //const PROJECT = 'project';
    
    //Litearls
    
    //Attributes
    /**
     *
     * @var Integer
     */
     public $modulId;
    const FIELD_MODULID = 'modulId';
    /**
     *
     * @var String
     */
     public $name;
    const FIELD_NAME = 'name';
    /**
     *
     * @var String
     */
     public $path;
    const FIELD_PATH = 'path';
    /**
     *
     * @var String
     */
     public $type;
    const FIELD_TYPE = 'type';
    /**
     *
     * @var ModulTypes
     */
     public $applyTo;
    const FIELD_APPLYTO = 'applyTo';
    /**
     *
     * @var WorkPackage
     */
     public $workPackage;
    const FIELD_WORKPACKAGE = 'workPackage';
    /**
     *
     * @var Boolean
     */
     public $buildOnlyOnFirstBuild;
    const FIELD_BUILDONLYONFIRSTBUILD = 'buildOnlyOnFirstBuild';
    /**
     *
     * @var Project
     */
     public $project;
    const FIELD_PROJECT = 'project';
    
    //AssociationsEnds
    
    /**
     * @var ModulPositionCollection
     */
    private $positions = false;
    const POSITIONS = 'positions';
    
    //{/isAssociationClass}}
    
    /**
     * @var BuildCollection
     */
    private $usedInBuilds = false;
    const USEDINBUILDS = 'usedInBuilds';
    
    //{/isAssociationClass}}
    
    /**
     * @var CodeModulCollection
     */
    private $childs = false;
    const CHILDS = 'childs';
    
    //{/isAssociationClass}}
    
    /**
     * @var CodeModul
     */
    private $parent = false;
    const PARENT = 'parent';
    
    //{/isAssociationClass}}
    
    /**
     * @var CodeTemplateCollection
     */
    private $codeTemplates = false;
    const CODETEMPLATES = 'codeTemplates';
    
    //{/isAssociationClass}}
    //FCV Attributes
    private $apPath = 'FCVAPs/';
    //    public $VIEW = array();
    static $fcvFields = array('modulId','name','path','type','applyTo','workPackage','buildOnlyOnFirstBuild','project');
    //Todo: Outsource
    protected $loadObject = false;
    private $loadedAttributes = array();
    
    //Methods
    //UserOperations
    /**
      *
      * @return 
      */
    public  function getAbsolutePath(){
        
            /* @var $this CodeModul*/
            
            if(!$this->hasParent()) return '/';
            $path = array();
            $parent = $this->getParent();
            $path[] = $parent->getName();
            while($parent->hasParent()){
                $parent = $parent->getParent();
                $path[] = $parent->getName(); 
            }
             
            return '/'.implode('/',array_reverse($path));
    
    }
    /**
      *
      * @return 
      */
    public  function build(){
        
            /* @var $this CodeModul*/
            //if($this->getPath() != ''){
            //    $file->get
            //}
            //$this->get
            echo 'WUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUT';
    
    }
    /**
      *
      * @return 
      */
    public  function getWorkPackage(){
        
            /* @var $this CodeModul*/
            
            if($this->hasWorkPackage()){
                return $this->workPackage;
            }else{
                if(!$this->hasParent()){
                    d($this);
                    d(1);
                }
                return $this->getParent()->getWorkPackage();
            }
    
    }
    /**
      *
      * @return 
      */
    public  function getProject(){
        
            /* @var $this CodeModul*/
            
            if(!is_null($this->project) && !empty($this->project)){
                return $this->fcvgetProject();
            }elseif(!$this->hasParent()){
                throw Exception('Parent not found');
            }else{
            //    echo $this->getName().'!!!!!!!!!!!!!!!!<br>';
                return $this->getParent()->getProject();
            }
    
    }
    private $lastModifiedTime = false;
    private $createdTime = false;
    public function __construct($data = array(),$calledFromChild = false){
        if(is_string($data)) $data = json_decode($data,true);
        if(count($data) > 0){
            if(isSet($data['loadObj']) && $data['loadObj']){
                $this->loadObject = true;
            }
            
        //        if(isset($data['modulId']) && !empty($data['modulId']))
    
            if(isSet($data['modulId'])) $this->modulId = $data['modulId'];
            if(isSet($data['name'])) $this->name = $data['name'];
            if(isSet($data['path'])) $this->path = $data['path'];
            if(isSet($data['type'])) $this->type = $data['type'];
            if(isSet($data['applyTo'])) $this->applyTo = $data['applyTo'];
            if(isSet($data['workPackage']) && is_array($data['workPackage'])) $this->workPackage = WorkPackage::create($data['workPackage'],$this->loadObject);
            elseif(isSet($data['workPackage'])) $this->workPackage = $data['workPackage'];
            if(isSet($data['buildOnlyOnFirstBuild'])) $this->buildOnlyOnFirstBuild = $data['buildOnlyOnFirstBuild'];
            if(isSet($data['project']) && is_array($data['project'])) $this->project = Project::create($data['project'],$this->loadObject);
            elseif(isSet($data['project'])) $this->project = $data['project'];
            //associationEnds
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['positions'])){
                if(!is_object(current($data['positions']))){
                    $newVal = new ModulPositionCollection();
                    foreach($data['positions'] as $el) $newVal->add(ModulPosition::create($el,$this->loadObject));
                    $this->positions = $newVal;
                }else  $this->positions = $data['positions'];
            }*/
    
            if(isSet($data['positions'])){
                if(!is_object(current($data['positions']))){
                    $newVal = new ModulPositionCollection();
                    foreach($data['positions'] as $el) $newVal->add(ModulPosition::create($el,$this->loadObject));
                    $this->positions = $newVal;
                }else $this->positions = $data['positions'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['usedInBuilds'])){
                if(!is_object(current($data['usedInBuilds']))){
                    $newVal = new BuildCollection();
                    foreach($data['usedInBuilds'] as $el) $newVal->add(Build::create($el,$this->loadObject));
                    $this->usedInBuilds = $newVal;
                }else  $this->usedInBuilds = $data['usedInBuilds'];
            }*/
    
            if(isSet($data['usedInBuilds'])){
                if(!is_object(current($data['usedInBuilds']))){
                    $newVal = new BuildCollection();
                    foreach($data['usedInBuilds'] as $el) $newVal->add(Build::create($el,$this->loadObject));
                    $this->usedInBuilds = $newVal;
                }else $this->usedInBuilds = $data['usedInBuilds'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['childs'])){
                if(!is_object(current($data['childs']))){
                    $newVal = new CodeModulCollection();
                    foreach($data['childs'] as $el) $newVal->add(CodeModul::create($el,$this->loadObject));
                    $this->childs = $newVal;
                }else  $this->childs = $data['childs'];
            }*/
    
            if(isSet($data['childs'])){
                if(!is_object(current($data['childs']))){
                    $newVal = new CodeModulCollection();
                    foreach($data['childs'] as $el) $newVal->add(CodeModul::create($el,$this->loadObject));
                    $this->childs = $newVal;
                }else $this->childs = $data['childs'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['parent'])){
                $this->parent = (is_array($data['parent']))?parent::create($data['parent'],$this->loadObject):$data['parent'];
            }*/
    
            if(isSet($data['parent'])){
                $this->parent = (is_array($data['parent']))?CodeModul::create($data['parent'],$this->loadObject):$data['parent'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['codeTemplates'])){
                if(!is_object(current($data['codeTemplates']))){
                    $newVal = new CodeTemplateCollection();
                    foreach($data['codeTemplates'] as $el) $newVal->add(CodeTemplate::create($el,$this->loadObject));
                    $this->codeTemplates = $newVal;
                }else  $this->codeTemplates = $data['codeTemplates'];
            }*/
    
            if(isSet($data['codeTemplates'])){
                if(!is_object(current($data['codeTemplates']))){
                    $newVal = new CodeTemplateCollection();
                    foreach($data['codeTemplates'] as $el) $newVal->add(CodeTemplate::create($el,$this->loadObject));
                    $this->codeTemplates = $newVal;
                }else $this->codeTemplates = $data['codeTemplates'];
            }
    
            //Helper
            if(isSet($data['lastModified'])) $this->lastModifiedTime = $data['lastModified'];
            if(isSet($data['created'])) $this->createdTime = $data['created'];
        }
    
    
    
    
    }
    
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new CodeModul($data);
    }
    
    public static function createLoad(){
        $data = array(
            'loadObj' => true
        );
        return new CodeModul($data);
    }
    
    public function getLastModifiedTime(){
        return $this->lastModifiedTime;
    }
    
    public function setLastModifiedTime($time){
        $this->lastModifiedTime = $time;
        return $this;
    }
    
    public function hasPrimaryKey(){
        return true;
    }
    
    public function getId(){
        return $this->modulId;
    }
    public function setId($id){
    //        CodeModul::$loadedInstances[$id] = &$this;
        $this->modulId = $id;
        if($this->loadObject) $this->loadedAttributes['modulId'] = true;
        return $this;
    }
    public function getPrimaryKeyName(){
        return "modulId";
    }
    
    public function addToJs($selectionString = ''){
    //    if(!$calledFromChild && get_class($this) == '' && (!$this->loadObject))
            FCVJsObj::add('', $this, $selectionString);
            return $this;
    }
    public function throwError($msg){
        if(!Debug::getObj()->getThrowErrors()) return "";
        else{
            throw new Exception($msg);
        }
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return ModulPositionCollection
     * @throws Exception
     */
    public function getPositions($forceLoad = false){
        if(is_null($this->positions) || empty($this->positions)){
            $this->throwError('class CodeModul with id '.$this->getId().' has no Attributes positions');
            //throw new Exception('class CodeModul with id '.$this->getId().' has no Attributes positions');
        }
        return $this->positions;
    }
    
    public function hasPositions($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getPositions($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param ModulPositionCollection $modulPosition
     */
    public function setPositions(ModulPositionCollection $positions){
        if(!$this->loadObject) $positions->setModul($this);
        $this->positions = $positions;
        if($this->loadObject) $this->loadedAttributes['positions'] = true;
        return $this;
    }
    
    /**
     * @param CodeModul $modulPosition
     */
    public function addModulPosition(ModulPosition $modulPosition){
            if(!$this->loadObject) $modulPosition->setModul($this);
        if(!$modulPosition->hasPrimaryKey()){
            if(!$this->loadObject) $modulPosition->setParent($this, count($this->positions));
        }
        if($this->loadObject){
            $this->loadedAttributes['positions'] = true;
            if(!$this->positions) $this->positions = new ModulPositionCollection();
        }
        elseif(!$this->positions){
            if(!$this->hasPositions()) $this->positions = new ModulPositionCollection();
        }
        $this->positions->add($modulPosition);
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return BuildCollection
     * @throws Exception
     */
    public function getUsedInBuilds($forceLoad = false){
        if(is_null($this->usedInBuilds) || empty($this->usedInBuilds)){
            $this->throwError('class CodeModul with id '.$this->getId().' has no Attributes usedInBuilds');
            //throw new Exception('class CodeModul with id '.$this->getId().' has no Attributes usedInBuilds');
        }
        return $this->usedInBuilds;
    }
    
    public function hasUsedInBuilds($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getUsedInBuilds($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param BuildCollection $build
     */
    public function setUsedInBuilds(BuildCollection $usedInBuilds){
        $this->usedInBuilds = $usedInBuilds;
        if($this->loadObject) $this->loadedAttributes['usedInBuilds'] = true;
        return $this;
    }
    
    /**
     * @param CodeModul $build
     */
    public function addBuild(Build $build){
        if(!$build->hasPrimaryKey()){
            if(!$this->loadObject) $build->setParent($this, count($this->usedInBuilds));
        }
        if($this->loadObject){
            $this->loadedAttributes['usedInBuilds'] = true;
            if(!$this->usedInBuilds) $this->usedInBuilds = new BuildCollection();
        }
        elseif(!$this->usedInBuilds){
            if(!$this->hasUsedInBuilds()) $this->usedInBuilds = new BuildCollection();
        }
        $this->usedInBuilds->add($build);
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return CodeModulCollection
     * @throws Exception
     */
    public function getChilds($forceLoad = false){
        if(is_null($this->childs) || empty($this->childs)){
            $this->throwError('class CodeModul with id '.$this->getId().' has no Attributes childs');
            //throw new Exception('class CodeModul with id '.$this->getId().' has no Attributes childs');
        }
        return $this->childs;
    }
    
    public function hasChilds($forceLoad = false){
        return $this->childs !== false;
    }
    
    /**
     * @param CodeModulCollection $codeModul
     */
    public function setChilds(CodeModulCollection $childs){
        if(!$this->loadObject) $childs->setParent($this);
        $this->childs = $childs;
        if($this->loadObject) $this->loadedAttributes['childs'] = true;
        return $this;
    }
    
    /**
     * @param CodeModul $codeModul
     */
    public function addCodeModul(CodeModul $codeModul){
            if(!$this->loadObject) $codeModul->setParent($this);
        if(!$codeModul->hasPrimaryKey()){
            if(!$this->loadObject) $codeModul->setParent($this, count($this->childs));
        }
        if($this->loadObject){
            $this->loadedAttributes['childs'] = true;
            if(!$this->childs) $this->childs = new CodeModulCollection();
        }
        elseif(!$this->childs){
            if(!$this->hasChilds()) $this->childs = new CodeModulCollection();
        }
        $this->childs->add($codeModul);
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return CodeModul
     * @throws Exception
     */
    public function getParent($forceLoad = false){
        if(is_null($this->parent) || empty($this->parent)){
            $this->throwError('class CodeModul with id '.$this->getId().' has no Attributes parent');
            //throw new Exception('class CodeModul with id '.$this->getId().' has no Attributes parent');
        }
        return $this->parent;
    }
    
    public function hasParent($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getParent($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param CodeModul $codeModul
     */
    public function setParent(CodeModul $parent){
        $this->parent = $parent;
        if($this->loadObject) $this->loadedAttributes['parent'] = true;
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return CodeTemplateCollection
     * @throws Exception
     */
    public function getCodeTemplates($forceLoad = false){
        if(is_null($this->codeTemplates) || empty($this->codeTemplates)){
            $this->throwError('class CodeModul with id '.$this->getId().' has no Attributes codeTemplates');
            //throw new Exception('class CodeModul with id '.$this->getId().' has no Attributes codeTemplates');
        }
        return $this->codeTemplates;
    }
    
    public function hasCodeTemplates($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getCodeTemplates($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param CodeTemplateCollection $codeTemplate
     */
    public function setCodeTemplates(CodeTemplateCollection $codeTemplates){
        if(!$this->loadObject) $codeTemplates->setModul($this);
        $this->codeTemplates = $codeTemplates;
        if($this->loadObject) $this->loadedAttributes['codeTemplates'] = true;
        return $this;
    }
    
    /**
     * @param CodeModul $codeTemplate
     */
    public function addCodeTemplate(CodeTemplate $codeTemplate){
            if(!$this->loadObject) $codeTemplate->setModul($this);
        if(!$codeTemplate->hasPrimaryKey()){
            if(!$this->loadObject) $codeTemplate->setParent($this, count($this->codeTemplates));
        }
        if($this->loadObject){
            $this->loadedAttributes['codeTemplates'] = true;
            if(!$this->codeTemplates) $this->codeTemplates = new CodeTemplateCollection();
        }
        elseif(!$this->codeTemplates){
            if(!$this->hasCodeTemplates()) $this->codeTemplates = new CodeTemplateCollection();
        }
        $this->codeTemplates->add($codeTemplate);
        return $this;
    }
    
    //Getter & Setter
    
    public function get($name){
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            return $this->$name->get($subName);
        }else return $this->$name;
    }
    
    public function has($name){
        if(strpos('.', $name) !== false){
            $spl = explode('.', $name, 2);
            return $this->get($spl[0])->has($spl[1]);
        }
        return (property_exists($this, $name));
    }
    
    public function set($name, $value){
        $possibleFields = array(
        'modulId',
        'name',
        'path',
        'type',
        'applyTo',
        'workPackage',
        'buildOnlyOnFirstBuild',
        'project',
        'positions',
        'usedInBuilds',
        'childs',
        'parent',
        'codeTemplates',
        );
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class CodeModul');
            if(!$this->$name){
                if(false);
                elseif($name == 'workPackage'){
                    $this->workPackage = WorkPackage::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['workPackage'] = true;
                }
                elseif($name == 'project'){
                    $this->project = Project::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['project'] = true;
                }
                elseif($name == 'positions'){
                    $this->positions = ModulPosition::create(array(),$this->loadObject)->set($subName,$value);
                    $this->positions = array($this->positions->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['positions'] = true;
                }
                elseif($name == 'usedInBuilds'){
                    $this->usedInBuilds = Build::create(array(),$this->loadObject)->set($subName,$value);
                    $this->usedInBuilds = array($this->usedInBuilds->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['usedInBuilds'] = true;
                }
                elseif($name == 'childs'){
                    $this->childs = CodeModul::create(array(),$this->loadObject)->set($subName,$value);
                    $this->childs = array($this->childs->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['childs'] = true;
                }
                elseif($name == 'parent'){
                    $this->parent = CodeModul::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['parent'] = true;
                }
                elseif($name == 'codeTemplates'){
                    $this->codeTemplates = CodeTemplate::create(array(),$this->loadObject)->set($subName,$value);
                    $this->codeTemplates = array($this->codeTemplates->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['codeTemplates'] = true;
                }
                else die('Error');
            }else $this->$name->set($subName,$value);
        }else{
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class CodeModul');
            $this->$name = $value;
            if($this->loadObject) $this->loadedAttributes[$name] = true;
        }
        return $this;
    }
    
    /**
     *
     * @return Integer
     * @throws Exception
     */
     public function getModulId(){
        return $this->modulId;
     }
    
     public function hasModulId(){
        try {
            $this->getModulId();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Integer $modulId
     * @return CodeModul
     */
     public function setModulId($modulId){
        ;
        $this->modulId = $modulId;
        if($this->loadObject) $this->loadedAttributes['modulId'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getName(){
        return $this->name;
     }
    
     public function hasName(){
        try {
            $this->getName();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $name
     * @return CodeModul
     */
     public function setName($name){
        ;
        $this->name = $name;
        if($this->loadObject) $this->loadedAttributes['name'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getPath(){
        return $this->path;
     }
    
     public function hasPath(){
        try {
            $this->getPath();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $path
     * @return CodeModul
     */
     public function setPath($path){
        ;
        $this->path = $path;
        if($this->loadObject) $this->loadedAttributes['path'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getType(){
        return $this->type;
     }
    
     public function hasType(){
        try {
            $this->getType();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $type
     * @return CodeModul
     */
     public function setType($type){
        ;
        $this->type = $type;
        if($this->loadObject) $this->loadedAttributes['type'] = true;
        return $this;
     }
    /**
     *
     * @return ModulTypes
     * @throws Exception
     */
     public function getApplyTo(){
        return $this->applyTo;
     }
    
     public function hasApplyTo(){
        try {
            $this->getApplyTo();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param ModulTypes $applyTo
     * @return CodeModul
     */
     public function setApplyTo($applyTo){
        ;
        $this->applyTo = $applyTo;
        if($this->loadObject) $this->loadedAttributes['applyTo'] = true;
        return $this;
     }
    /**
     *
     * @return WorkPackage
     * @throws Exception
     */
     public function fcvgetWorkPackage(){
        if(empty($this->workPackage)){
            $this->throwError('class CodeModul with id '.$this->getId().' has no Attributes workPackage');
            //throw new Exception('class CodeModul with id '.$this->getId().' has no Attributes workPackage');
        }
        return $this->workPackage;
     }
    
     public function hasWorkPackage(){
        try {
            $this->fcvgetWorkPackage();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param WorkPackage $workPackage
     * @return CodeModul
     */
     public function setWorkPackage($workPackage){
        if(is_numeric($workPackage)) $workPackage = WorkPackage::create()->setId($workPackage);
        $this->workPackage = $workPackage;
        if($this->loadObject) $this->loadedAttributes['workPackage'] = true;
        return $this;
     }
    /**
     *
     * @return Boolean
     * @throws Exception
     */
     public function getBuildOnlyOnFirstBuild(){
        return $this->buildOnlyOnFirstBuild;
     }
    
     public function hasBuildOnlyOnFirstBuild(){
        try {
            $this->getBuildOnlyOnFirstBuild();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Boolean $buildOnlyOnFirstBuild
     * @return CodeModul
     */
     public function setBuildOnlyOnFirstBuild($buildOnlyOnFirstBuild){
        ;
        $this->buildOnlyOnFirstBuild = $buildOnlyOnFirstBuild;
        if($this->loadObject) $this->loadedAttributes['buildOnlyOnFirstBuild'] = true;
        return $this;
     }
    /**
     *
     * @return Project
     * @throws Exception
     */
     public function fcvgetProject(){
        if(empty($this->project)){
            $this->throwError('class CodeModul with id '.$this->getId().' has no Attributes project');
            //throw new Exception('class CodeModul with id '.$this->getId().' has no Attributes project');
        }
        return $this->project;
     }
    
     public function hasProject(){
        try {
            $this->fcvgetProject();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Project $project
     * @return CodeModul
     */
     public function setProject($project){
        if(is_numeric($project)) $project = Project::create()->setId($project);
        $this->project = $project;
        if($this->loadObject) $this->loadedAttributes['project'] = true;
        return $this;
     }
    protected $visitedByToArray = false;
    public function toJSObject($returnArray = false){
        if($this->loadObject) return false;
        if($this->visitedByToArray){
    //            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return '[VALENCODE]jsModel["CodeModul.'.$this->getId().'"][VALENCODE]';
                    return array(
                        'class' => 'CodeModul',
                        'id' => $this->getId()
                    );
                }
                return false;
    //            }
    //            return array(
    //                'class' => 'CodeModul',
    //                'id' => rand()
    //            );
        }
        $retArray = array();
        
        $this->visitedByToArray = true;
        
        if(isSet($this->modulId) && !is_null($this->modulId)) $retArray['modulId'] = (!is_object($this->modulId))?str_replace('"', '\"', $this->modulId):($this->modulId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (!is_object($this->name))?str_replace('"', '\"', $this->name):($this->name);
        if(isSet($this->path) && !is_null($this->path)) $retArray['path'] = (!is_object($this->path))?str_replace('"', '\"', $this->path):($this->path);
        if(isSet($this->type) && !is_null($this->type)) $retArray['type'] = (!is_object($this->type))?str_replace('"', '\"', $this->type):($this->type);
        if(isSet($this->applyTo) && !is_null($this->applyTo)) $retArray['applyTo'] = (!is_object($this->applyTo))?str_replace('"', '\"', $this->applyTo):($this->applyTo);
        if(isSet($this->workPackage) && !is_null($this->workPackage)) $retArray['workPackage'] = (!is_object($this->workPackage))?str_replace('"', '\"', $this->workPackage):($this->workPackage->toJSObject());
        if(isSet($this->buildOnlyOnFirstBuild) && !is_null($this->buildOnlyOnFirstBuild)) $retArray['buildOnlyOnFirstBuild'] = (!is_object($this->buildOnlyOnFirstBuild))?str_replace('"', '\"', $this->buildOnlyOnFirstBuild):($this->buildOnlyOnFirstBuild);
        if(isSet($this->project) && !is_null($this->project)) $retArray['project'] = (!is_object($this->project))?str_replace('"', '\"', $this->project):($this->project->toJSObject());
    
        if(isSet($this->positions) && !is_null($this->positions) && $this->positions !== false){
            if(!is_object($this->positions)) $val = $this->positions;
            else{
                $val = array();
                foreach($this->positions as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['positions'] = $val;
            //$retArray['positions'] = ($simple)?$this->positions->getId():$this->positions->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->usedInBuilds) && !is_null($this->usedInBuilds) && $this->usedInBuilds !== false){
            if(!is_object($this->usedInBuilds)) $val = $this->usedInBuilds;
            else{
                $val = array();
                foreach($this->usedInBuilds as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['usedInBuilds'] = $val;
            //$retArray['usedInBuilds'] = ($simple)?$this->usedInBuilds->getId():$this->usedInBuilds->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->childs) && !is_null($this->childs) && $this->childs !== false){
            if(!is_object($this->childs)) $val = $this->childs;
            else{
                $val = array();
                foreach($this->childs as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['childs'] = $val;
            //$retArray['childs'] = ($simple)?$this->childs->getId():$this->childs->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->parent) && !is_null($this->parent) && $this->parent !== false){
            if(!is_object($this->parent)) $val = $this->parent;
            else{
                $val = $this->parent->toJSObject();
            }
            $retArray['parent'] = $val;
            //$retArray['parent'] = ($simple)?$this->parent->getId():$this->parent->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->codeTemplates) && !is_null($this->codeTemplates) && $this->codeTemplates !== false){
            if(!is_object($this->codeTemplates)) $val = $this->codeTemplates;
            else{
                $val = array();
                foreach($this->codeTemplates as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['codeTemplates'] = $val;
            //$retArray['codeTemplates'] = ($simple)?$this->codeTemplates->getId():$this->codeTemplates->toJSObject($simple, $treeVisit);
    
         }
        return ($returnArray)?$retArray:'[VALENCODE]new CodeModul('.json_encode($retArray).')[VALENCODE]';
    }
    
    public function toArray($simple = false, $treeVisit = false){
        if($this->visitedByToArray){
            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return array(
    //                        'class' => 'CodeModul',
    //                        'id' => $this->getId()
    //                    );
                    return '[VALENCODE]jsModel["CodeModul"]["'.$this->getId().'"][VALENCODE]';
                }
            }
            return false;
        }
        if($treeVisit) $this->visitedByToArray = true;
        $retArray = array();
        
        if(isSet($this->modulId) && !is_null($this->modulId)) $retArray['modulId'] = (is_numeric($this->modulId))?$this->modulId:($this->modulId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (is_numeric($this->name))?$this->name:($this->name);
        if(isSet($this->path) && !is_null($this->path)) $retArray['path'] = (is_numeric($this->path))?$this->path:($this->path);
        if(isSet($this->type) && !is_null($this->type)) $retArray['type'] = (is_numeric($this->type))?$this->type:($this->type);
        if(isSet($this->applyTo) && !is_null($this->applyTo)) $retArray['applyTo'] = (is_numeric($this->applyTo))?$this->applyTo:($this->applyTo);
        if(isSet($this->workPackage) && !is_null($this->workPackage)) $retArray['workPackage'] = (is_numeric($this->workPackage))?$this->workPackage:(($simple)?$this->workPackage->getId():$this->workPackage->toArray($simple, $treeVisit));
        if(isSet($this->buildOnlyOnFirstBuild) && !is_null($this->buildOnlyOnFirstBuild)) $retArray['buildOnlyOnFirstBuild'] = (is_numeric($this->buildOnlyOnFirstBuild))?$this->buildOnlyOnFirstBuild:($this->buildOnlyOnFirstBuild);
        if(isSet($this->project) && !is_null($this->project)) $retArray['project'] = (is_numeric($this->project))?$this->project:(($simple)?$this->project->getId():$this->project->toArray($simple, $treeVisit));
        if(!$simple && isSet($this->positions) && !is_null($this->positions) && $this->positions !== false){
            if(is_numeric($this->positions)) $val = $this->positions;
            else{
                $val = array();
                foreach($this->positions as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['positions'] = $val;
            //$retArray['positions'] = ($simple)?$this->positions->getId():$this->positions->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->usedInBuilds) && !is_null($this->usedInBuilds) && $this->usedInBuilds !== false){
            if(is_numeric($this->usedInBuilds)) $val = $this->usedInBuilds;
            else{
                $val = array();
                foreach($this->usedInBuilds as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['usedInBuilds'] = $val;
            //$retArray['usedInBuilds'] = ($simple)?$this->usedInBuilds->getId():$this->usedInBuilds->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->childs) && !is_null($this->childs) && $this->childs !== false){
            if(is_numeric($this->childs)) $val = $this->childs;
            else{
                $val = array();
                foreach($this->childs as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['childs'] = $val;
            //$retArray['childs'] = ($simple)?$this->childs->getId():$this->childs->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->parent) && !is_null($this->parent) && $this->parent !== false){
            if(is_numeric($this->parent)) $val = $this->parent;
            else{
                $val = ($simple)?$this->parent->getId():$this->parent->toArray($simple, $treeVisit);
            }
            $retArray['parent'] = $val;
            //$retArray['parent'] = ($simple)?$this->parent->getId():$this->parent->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->codeTemplates) && !is_null($this->codeTemplates) && $this->codeTemplates !== false){
            if(is_numeric($this->codeTemplates)) $val = $this->codeTemplates;
            else{
                $val = array();
                foreach($this->codeTemplates as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['codeTemplates'] = $val;
            //$retArray['codeTemplates'] = ($simple)?$this->codeTemplates->getId():$this->codeTemplates->toArray($simple, $treeVisit);
    
         }
        return $retArray;
    }private $tplVars = array();
    public function addTemplateVar($name, $content){
        $this->tplVars[$name] = $content;
    }
    
    private $ignoreErrors = false;
    /**
     * get Html Template
     * @param string $template template name
     */
    public function getHtml($template = "default"){
        $availableTemplates =  array(
            'default'
            ,'default'
        );
        if(!in_array($template, $availableTemplates)){
            throw new Exception('Template <b>'.$template.'</b> doesn\'t exist for <b>CodeModul</b>');
        }
        $m = new TemplateEngine;
        
        $templates = array(
            'default' => '' //798
        );
        $tplSrc = $templates[$template];
        
        foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, $this);
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    }
    
    public static function getSubHtml($operationName, $template = 'default'){
    
        
        $availableTemplates =  array(
            'getAbsolutePath' => array(
                'default'
                ,'default'
            ),
            'build' => array(
                'default'
                ,'default'
            ),
            'getWorkPackage' => array(
                'default'
                ,'default'
            ),
            'getProject' => array(
                'default'
                ,'default'
            )
        );
        if(!isset($availableTemplates[$operationName]) || !in_array($template, $availableTemplates[$operationName])){
            throw new Exception('Template OperationTemplate <b>'.$template.'</b> doesn\'t exist for <b>CodeModul.'.$operationName.'</b>');
        }
        $m = new TemplateEngine;
        $tplSrc = file_get_contents('FCVAPs/fcv/fcvPackage/CodeModul/'.$operationName.'/'.$operationName.'.'.$template.'.html');
        /*foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }*/
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, array());
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    
    }
}
