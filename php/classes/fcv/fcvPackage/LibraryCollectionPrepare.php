<?php
namespace fcv\fcvPackage;

use \QueryBuilder;

use \fcv\fcvPackage\CodeLanguageCollectionPrepare;
use \fcv\fcvPackage\ProjectCollectionPrepare;class LibraryCollectionPrepare {

    protected $tableName;
    protected $tableNameAlias = '';

    /**
     * @var QueryBuilder
     */
    protected $query;
    protected $objectMap = array();

    protected $primaryKeyIndex = 0;

    public function __construct(){
        $this->tableName = 'Library';
        $this->query = \QueryBuilder::create();
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_NUM);
    }

    /**
     * @param Library[] $data
     * @return LibraryCollectionPrepare inserted Id
     */
    public static function create(){
        return new LibraryCollectionPrepare();
    }

    /**
     * @return int
     */
    public function getPrimaryKeyIndex(){
        return $this->primaryKeyIndex;
    }

    /**
     * @return array
     */
    public function getObjectMap(){
        return $this->objectMap;
    }

    /**
     * @return QueryBuilder
     */
    public function getQuery(){
        return $this->query;
    }

    /**
     * @return string
     */
    public function getTableName(){
        return $this->tableName;
    }

    /**
     * @return LibraryCollectionPrepare
     */
    public function setTableName($tableName){
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableNameAlias(){
        return ($this->tableNameAlias != '')?$this->tableNameAlias:$this->tableName;
    }

    /**
     * @return LibraryCollectionPrepare
     */
    public function setTableNameAlias($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias;
        return $this;
    }

    /**
     * @return string
     */
    public function setTableNameAliasPrepend($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias.(($this->tableNameAlias == '')?$this->tableName:$this->tableNameAlias);
        return $this;
    }

    /**
      * Add a basic where clause to the query.
      *
      * @param  string  $column
      * @param  string  $operator
      * @param  mixed   $value
      * @param  string  $boolean
      * @return LibraryCollectionPrepare
      *
      * @throws \InvalidArgumentException
      */
    public function where($column, $operator = null, $value = null, $boolean = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->where($spl[1], $operator, $value, $boolean);
        }else{
            $this->query->where($column, $operator, $value, $boolean);
        }
        return $this;
    }
    
    public function orWhere($column, $operator = null, $value = null, $logicConnector = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->orWhere($spl[1], $operator, $value, $logicConnector);
        }else{
            $this->query->orWhere($column, $operator, $value, $logicConnector);
        }
        return $this;
    }

    /**
     * @return LibraryCollectionPrepare
     */
    public function whereRaw($where){
        $this->query->whereRaw($where);
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return LibraryCollectionPrepare
     */
    public function whereLibId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('libId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return LibraryCollectionPrepare
     */
    public function orWhereLibId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('libId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return LibraryCollectionPrepare
     */
    public function selectLibId(){
        $this->primaryKeyIndex = count($this->query->columns);
        $this->objectMap[] = array(
            'class' => 'Library',
            'field' => 'libId',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('libId');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return LibraryCollectionPrepare
     */
    public function whereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return LibraryCollectionPrepare
     */
    public function orWhereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return LibraryCollectionPrepare
     */
    public function selectName(){
        $this->objectMap[] = array(
            'class' => 'Library',
            'field' => 'name',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('name');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return LibraryCollectionPrepare
     */
    public function whereVersion($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('version', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return LibraryCollectionPrepare
     */
    public function orWhereVersion($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('version', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return LibraryCollectionPrepare
     */
    public function selectVersion(){
        $this->objectMap[] = array(
            'class' => 'Library',
            'field' => 'version',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('version');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return LibraryCollectionPrepare
     */
    public function whereVerificationLink($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('verificationLink', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return LibraryCollectionPrepare
     */
    public function orWhereVerificationLink($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('verificationLink', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return LibraryCollectionPrepare
     */
    public function selectVerificationLink(){
        $this->objectMap[] = array(
            'class' => 'Library',
            'field' => 'verificationLink',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('verificationLink');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return LibraryCollectionPrepare
     */
    public function wherePath($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('path', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return LibraryCollectionPrepare
     */
    public function orWherePath($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('path', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return LibraryCollectionPrepare
     */
    public function selectPath(){
        $this->objectMap[] = array(
            'class' => 'Library',
            'field' => 'path',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('path');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return LibraryCollectionPrepare
     */
    public function whereIncludeCode($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('includeCode', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return LibraryCollectionPrepare
     */
    public function orWhereIncludeCode($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('includeCode', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return LibraryCollectionPrepare
     */
    public function selectIncludeCode(){
        $this->objectMap[] = array(
            'class' => 'Library',
            'field' => 'includeCode',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('includeCode');
        return $this;
    }

    /**
     * @var CodeLanguageCollectionPrepare
     */
    protected $languages;
    /**
     * @var CodeLanguageCollectionPrepare
     * @return LibraryCollectionPrepare
     */
    public function setLanguages(CodeLanguageCollectionPrepare $languages){
       $this->languages = $languages;
       $languages->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getLanguages(){
       if(!isset($this->languages)) $this->languages = CodeLanguageCollectionPrepare::create();
       return $this->languages;
    }
    
    
    public function selectLanguages(){
        $this->objectMap[] = array(
            'class' => 'languages',
            'field' => 'languages',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('languages');
        return $this;
    }



    /**
     * @var LibraryIncludeCodeCollectionPrepare
     */
    protected $libraryIncludeCode;
    /**
     * @var CodeLanguageCollectionPrepare
     * @return languagesCollectionPrepare
     */
    public function setLibraryIncludeCode(LibraryIncludeCodeCollectionPrepare $libraryIncludeCode){
       $this->libraryIncludeCode = $libraryIncludeCode;
       $libraryIncludeCode->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }


    public function getLibraryIncludeCode(){
       if(!isset($this->libraryIncludeCode)) $this->libraryIncludeCode =  LibraryIncludeCodeCollectionPrepare::create();
       return $this->libraryIncludeCode;
    }
        


    /**
     * @var ProjectCollectionPrepare
     */
    protected $projects;
    /**
     * @var ProjectCollectionPrepare
     * @return LibraryCollectionPrepare
     */
    public function setProjects(ProjectCollectionPrepare $projects){
       $this->projects = $projects;
       $projects->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getProjects(){
       if(!isset($this->projects)) $this->projects = ProjectCollectionPrepare::create();
       return $this->projects;
    }
    
    





    private $selectAll = false;
    public function selectAll(){
        
        $this->selectLibId();
        $this->selectName();
        $this->selectVersion();
        $this->selectVerificationLink();
        $this->selectPath();
        $this->selectIncludeCode();
        $this->selectLanguages();

        return $this;
    }
    public function prepareQuery(){
        $this->prepareJoins();
        $this->query->from = 'Library'.(($this->tableNameAlias == '')?'':' AS '.$this->tableNameAlias);
        QueryBuilder::appendWhereWithTable($this->query, $this->getTableNameAlias());
        return $this;
    }
    /**
     * @return LibraryCollection
     */
    public function get(){
        $this->prepareQuery();
        $rows = $this->query->get();
        return $this->parseDbRows($rows, $this->getObjectMap());
    }

    /**
     * parse all return rows from sql
     */
    private function parseDbRows($rows, $map){
        $objsArray = array();
        $objs = LibraryCollection::create();
        //build assoc Array with all data
        if(!$this->selectAll){
            foreach($rows as $row){
                $primaryField = $row[$this->primaryKeyIndex];

                //extend object if allready exists

                $initObj = (isset($objsArray[$primaryField]))?$objsArray[$primaryField]:array();
                $objsArray[$primaryField] = $this->parseDbRow($row, $map, $initObj);
            }
        }else $objsArray = $rows;
        //convert array to real objects (subobjects are generated in constructors)
        foreach($objsArray as $dataArray){
            $objs->add(new Library($dataArray));
        }
        return $objs;
    }

    /**
     * parse one row into Object and extend existing ones
     */
    private function parseDbRow($row, $map, $objsData = array(), &$index = 0){
        foreach($map as $m){
            //check if association
            if(isset($m['map'])){
                //check if cardinality is multi
                if($m['multi']){
                    if(!isset($objsData[$m['field']])) $objsData[$m['field']] = array();
                    //get primaryKey ob subobject
                    $id = $row[$index+$m['primaryKeyIndex']];
                    //extend object if allready exists
                    $initObj = (isset($objsData[$m['field']][$id]))?$objsData[$m['field']][$id]:array();
                    //add/extend object with id
                    $objsData[$m['field']][$id] = $this->parseDbRow($row, $m['map'], $initObj, $index);
                }else{
                    $objsData[$m['field']] = $this->parseDbRow($row, $m['map'], array(), $index);
                }
            }
            else{
                $objsData[$m['field']] = $row[$index];
                $index++;
            }
        }
        return $objsData;
    }

    /**
     * create joins depending on associations
     * @return CollectionPrepare
     */
    public function prepareJoins(){
        
        if(!$this->selectAll){
            if(!is_array($this->query->columns) || !in_array('libId', $this->query->columns)){
                $this->selectlibId();
            }elseif(!is_array($this->query->columns)){
                $this->selectAll();
            }
        }else{
            $this->query->select('*');
        }
        if(isSet($this->languages)){
            $tableName = $this->languages->getTableNameAlias();
            //Assoc    Library
            $this->query->join('LibraryIncludeCode as LibraryIncludeCode', 'LibraryIncludeCode.library', '=', $this->getTableNameAlias().'.libId', 'LEFT');
            $this->query->join('CodeLanguage as '.$tableName, $tableName.'.langId', '=', 'LibraryIncludeCode.codeLanguage', 'LEFT');
            $this->languages->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->languages->getQuery(),$tableName);
            $this->query->mergeQuery($this->languages->getQuery());

            $this->objectMap[] = array(
                'class' => 'CodeLanguage',
                'field' => 'languages',
                'multi' => false,
                'primaryKeyIndex' => $this->languages->getPrimaryKeyIndex(),
                'map' => $this->languages->getObjectMap()
            );
        }

        if(isSet($this->libraryIncludeCode)){
            $tableName = $this->libraryIncludeCode->getTableNameAlias();
            //Assoc    Library
            $this->query->join('LibraryIncludeCode as '.$tableName, $tableName.'.library', '=', $this->getTableNameAlias().'.libId', 'LEFT');
//            $this->query->join('CodeLanguage as '.$tableName, $tableName.'.langId', '=', 'LibraryIncludeCode.codeLanguage', 'LEFT');

            $this->libraryIncludeCode->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->libraryIncludeCode->getQuery(),$tableName);
            $this->query->mergeQuery($this->libraryIncludeCode->getQuery());

            $this->objectMap[] = array(
                'class' => 'languages',
                'field' => 'libraryIncludeCode',
                'multi' => true,
                'primaryKeyIndex' => $this->libraryIncludeCode->getPrimaryKeyIndex(),
                'map' => $this->libraryIncludeCode->getObjectMap()
            );
        }

        if(isSet($this->projects)){
            $tableName = $this->projects->getTableNameAlias();
            //Multi
            $this->query->join('Project as '.$tableName, $tableName.'.usedLibs', '=', $this->getTableNameAlias().'.libId', 'LEFT');
            $this->projects->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->projects->getQuery(),$tableName);
            $this->query->mergeQuery($this->projects->getQuery());

            $this->objectMap[] = array(
                'class' => 'Project',
                'field' => 'projects',
                'multi' => true,
                'primaryKeyIndex' => $this->projects->getPrimaryKeyIndex(),
                'map' => $this->projects->getObjectMap()
            );
        }



        return $this;
    }
}
