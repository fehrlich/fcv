<?php
namespace fcv\fcvPackage; ///fcv/fcvPackage/Project
    
use Exception;
use mys;
use QueryBuilder;
use fcv\libs\TemplateEngine;
use fcv\libs\Debug;
use fcv\FCVJsObj;


use fcv\fcvPackage\Build;
use fcv\fcvPackage\BuildCollection;
use fcv\fcvPackage\WorkPackage;
use fcv\fcvPackage\WorkPackageCollection;
use fcv\fcvPackage\Library;
use fcv\fcvPackage\LibraryCollection;
use fcv\fcvPackage\CodeLanguage;
use fcv\fcvPackage\CodeLanguageCollection;
use fcv\fcvPackage\Project;

class Project
        
         
        {
    
    //Attributes
    //const PROJEKTID = 'projektId';
    //const NAME = 'name';
    //const UMLPATH = 'umlPath';
    //const MYSQLMODEL = 'mysqlModel';
    //const UMLMODEL = 'umlModel';
    //const MYSQLDB = 'mysqlDb';
    //const MYSQLUSER = 'mysqlUser';
    //const MYSQLPASSWORD = 'mysqlPassword';
    //const MYSQLHOST = 'mysqlHost';
    //const MYSQLPORT = 'mysqlPort';
    //const UMLELEMENT = 'umlElement';
    //const BUILDPATH = 'buildPath';
    
    //Litearls
    
    //Attributes
    /**
     *
     * @var Integer
     */
     public $projektId;
    const FIELD_PROJEKTID = 'projektId';
    /**
     *
     * @var String
     */
     public $name;
    const FIELD_NAME = 'name';
    /**
     *
     * @var String
     */
     public $umlPath;
    const FIELD_UMLPATH = 'umlPath';
    /**
     *
     * @var String
     */
     public $mysqlModel;
    const FIELD_MYSQLMODEL = 'mysqlModel';
    /**
     *
     * @var String
     */
     public $umlModel;
    const FIELD_UMLMODEL = 'umlModel';
    /**
     *
     * @var String
     */
     public $mysqlDb;
    const FIELD_MYSQLDB = 'mysqlDb';
    /**
     *
     * @var String
     */
     public $mysqlUser;
    const FIELD_MYSQLUSER = 'mysqlUser';
    /**
     *
     * @var String
     */
     public $mysqlPassword;
    const FIELD_MYSQLPASSWORD = 'mysqlPassword';
    /**
     *
     * @var String
     */
     public $mysqlHost;
    const FIELD_MYSQLHOST = 'mysqlHost';
    /**
     *
     * @var Integer
     */
     public $mysqlPort = 3306;
    const FIELD_MYSQLPORT = 'mysqlPort';
    /**
     *
     * @var Integer
     */
     public $umlElement;
    const FIELD_UMLELEMENT = 'umlElement';
    /**
     *
     * @var String
     */
     public $buildPath;
    const FIELD_BUILDPATH = 'buildPath';
    
    //AssociationsEnds
    
    /**
     * @var BuildCollection
     */
    private $builds = false;
    const BUILDS = 'builds';
    
    //{/isAssociationClass}}
    
    /**
     * @var WorkPackageCollection
     */
    private $workPackages = false;
    const WORKPACKAGES = 'workPackages';
    
    //{/isAssociationClass}}
    
    /**
     * @var LibraryCollection
     */
    private $usedLibs = false;
    const USEDLIBS = 'usedLibs';
    
    //{/isAssociationClass}}
    
    /**
     * @var CodeLanguageCollection
     */
    private $usedLanguages = false;
    const USEDLANGUAGES = 'usedLanguages';
    
    //{/isAssociationClass}}
    //FCV Attributes
    private $apPath = 'FCVAPs/';
    //    public $VIEW = array();
    static $fcvFields = array('projektId','name','umlPath','mysqlModel','umlModel','mysqlDb','mysqlUser','mysqlPassword','mysqlHost','mysqlPort','umlElement','buildPath');
    //Todo: Outsource
    protected $loadObject = false;
    private $loadedAttributes = array();
    
    //Methods
    //UserOperations
    /**
      *
      * @param Build $build
      * @return 
      */
    public  function build(Build $build ){
        
            /* @var $this Project*/
    
    }
    /**
      *
      * @return 
      */
    public  function update($modules, $buildworkpackages = true){
        
            
            /* @var $this Project*/
            /* @var $build Build*/
            
            //$this->setCurrentBuild($build);
            Debug::getObj()->startMeasure('update', 'Update Project '.$this->getId());
            
            Debug::getObj()->startMeasure('getPathData', 'Get Raw Data from Path '.$this->getUmlPath());
            $xmlData = file_get_contents($this->getUmlPath());
            Debug::getObj()->stopMeasure('getPathData');
            
            Debug::getObj()->startMeasure('parseUml', 'parse UML');
            $umlModelOld = $this->getUmlModel();
            if(!is_object($umlModelOld) && is_string($umlModelOld)){
                $umlModelOld = unserialize ($umlModelOld);
            }
            else
                $umlModelOld = new \UMLModel($this->getId(), $this->getName());
            
            $parser = new \xmiParserArgo($xmlData, new \UMLModel($this->getId(), $this->getName()));
            $newUmlModel = $parser->parse();
            
            Debug::getObj()->stopMeasure('parseUml');
            Debug::getObj()->setDebugPoint('Merge with old SVN ...');
            Debug::getObj()->startMeasure('mergeUml', 'merg UML with old one');
            $umlMerge = new \UMLMerge($umlModelOld, $newUmlModel);
            Debug::getObj()->stopMeasure('mergeUml');
            $umlMerge->merge();
            
            //if($newUmlModel->hasChanged()) $revision++;
            
            Debug::getObj()->startMeasure('parseSQL', 'parse SQL');
            Debug::getObj()->setDebugPoint('Parse Mysql Model from UML ...');
            $mysqlModel = new \MysqlModel($newUmlModel);
            $mysqlModel->parse();
            Debug::getObj()->stopMeasure('parseSQL');
            
            //d($newUmlModel);
            
            //var_dump($newUmlModel);
            $this->setUmlModel($newUmlModel);
            $this->setMysqlModel($mysqlModel);
            
            Debug::getObj()->startMeasure('projBuild', 'Build WorkPackages');
            Debug::getObj()->setDebugPoint('Build WorkPackages ...');
            $this->buildWorkPackages(false, $modules, $buildworkpackages);
            Debug::getObj()->stopMeasure('projBuild');
                
            Debug::getObj()->setDebugPoint('Update Done');
            
            Debug::getObj()->stopMeasure('update');
            
            return $this;
    
    }
    /**
      *
      * @param  $workPackage
      * @return 
      */
    public  function buildWorkPackages( $workPackage, $modules, $buildworkpackages = true ){
        
            
            /* @var $this Project */
            /* @var $workPackage WorkPackage */
            
            $root = ($workPackage === false);
            if (!$workPackage) {
                $loadedWP = WorkPackage::create();
                $workPackage = $loadedWP
                        ->setId($this->getId())
                        ->setType('Project')
                        ->setUmlElement($this->getUmlModel())
                        ->setName($this->getName())
                        ->setProject($this)
                        ->setChilds(WorkPackageCollection::create());
                $this->getUmlModel()->setWorkPackage($workPackage);
                Debug::getObj()->startMeasure('buildPackage', 'Building Packages From UML');
            }
            $umlPackage = $workPackage->getUmlElement();
            
            $classes = array_merge($umlPackage->getClasses(), $umlPackage->getEnumerations(), $umlPackage->getAssociationClasses());
            foreach ($classes as $class) {
                if (!$class->hasStereoType("Connector")) {
            
                    $loadedWP = WorkPackage::create();
                    $wp = $loadedWP
                        ->setId($class->getId())
                        ->setType('Class')
                        ->setUmlElement($class)
                        ->setName($class->getName())
                        ->setProject($this)
                        ->setChilds(WorkPackageCollection::create());
                    $class->setWorkPackage($wp);
                    $workPackage->addworkPackage($wp);
                    
                    foreach ($class->getOperations() as $operation) {
                        $loadedWP = WorkPackage::create();
                        $w = $loadedWP
                            ->setId($operation->getId())
                            ->setType('Method')
                            ->setUmlElement($operation)
                            ->setName($operation->getName())
                            ->setProject($this)
                            ->setChilds(WorkPackageCollection::create());
                        ;
                        $wp->addworkPackage($w);
                        $operation->setWorkPackage($w);
                    }
                }
            }
            
            foreach ($workPackage->getUmlElement()->getPackages() as $umlPackage) {
                $loadedWP = WorkPackage::create();
                $package = $loadedWP
                    ->setId($umlPackage->getId())
                    ->setType('Package')
                    ->setUmlElement($umlPackage)
                    ->setName($umlPackage->getName())
                    ->setProject($this)
                    ->setChilds(WorkPackageCollection::create());
                $workPackage->addworkPackage($package);
                $umlPackage->setWorkPackage($workPackage);
                $this->buildWorkPackages($package, $modules, $buildworkpackages);
            }
            
            if ($root) {
            
                //Activity Graphs
                $pagesAndFrames = array();
                $activityGraphs = $this->getUmlModel()->getActivityGraphs();
                foreach ($activityGraphs as $graph) {
                    $graph->iterate(function($umlElement) use (&$pagesAndFrames) {
                        if ($umlElement->hasStereoType('page') || $umlElement->hasStereoType('frame'))
                            $pagesAndFrames[] = &$umlElement;
                    });
                }
            
                foreach ($pagesAndFrames as $umlElement) {
                    $loadedWP = WorkPackage::create();
                    $w = $loadedWP
                            ->setId($umlElement->getId())
                            ->setUmlElement($umlElement)
                        ->setName($umlElement->getName())
                            ->setType($umlElement->hasStereoType('page') ? 'Page' : 'Frame')
                            ->setProject($this)
                            ->setChilds(WorkPackageCollection::create());
                    ;
                    $umlElement->setWorkPackage($w);
                    $workPackage->addWorkPackage($w);
                }
            
                $this->addWorkPackage($workPackage);
                Debug::getObj()->stopMeasure('buildPackage', 'Building Packages');
                Debug::getObj()->startMeasure('savePackages', 'save Packages');
                Debug::getObj()->stopMeasure('savePackages');
                Debug::getObj()->startMeasure('buildPackageFS', 'build Packages from FileSystem');
                $workPackage->build($buildworkpackages);
                Debug::getObj()->stopMeasure('buildPackageFS');
                Debug::getObj()->startMeasure('buildModules', 'build Modules');
                $workPackage->buildModules($modules);
                Debug::getObj()->stopMeasure('buildModules');
            }
    
    }
    /**
      *
      * @return 
      */
    public  function buildLibraries(){
        
            /* @var $this Project*/
    
    }
    
    public $currentBuild = null;
    
    public function setCurrentBuild($currentBuild){
        $this->currentBuild = $currentBuild;
    }
    /**
      *
      * @return Build
      */
    public  function getCurrentBuild(){
        if($this->currentBuild){
            return $this->currentBuild;
        }
        throw \Exception('No Build selected');
    }
    private $lastModifiedTime = false;
    private $createdTime = false;
    public function __construct($data = array(),$calledFromChild = false){
        if(is_string($data)) $data = json_decode($data,true);
        if(count($data) > 0){
            if(isSet($data['loadObj']) && $data['loadObj']){
                $this->loadObject = true;
            }
            
        //        if(isset($data['projektId']) && !empty($data['projektId']))
    
            if(isSet($data['projektId'])) $this->projektId = $data['projektId'];
            if(isSet($data['name'])) $this->name = $data['name'];
            if(isSet($data['umlPath'])) $this->umlPath = $data['umlPath'];
            if(isSet($data['mysqlModel'])) $this->mysqlModel = $data['mysqlModel'];
            if(isSet($data['umlModel'])) $this->umlModel = $data['umlModel'];
            if(isSet($data['mysqlDb'])) $this->mysqlDb = $data['mysqlDb'];
            if(isSet($data['mysqlUser'])) $this->mysqlUser = $data['mysqlUser'];
            if(isSet($data['mysqlPassword'])) $this->mysqlPassword = $data['mysqlPassword'];
            if(isSet($data['mysqlHost'])) $this->mysqlHost = $data['mysqlHost'];
            if(isSet($data['mysqlPort'])) $this->mysqlPort = $data['mysqlPort'];
            if(isSet($data['umlElement'])) $this->umlElement = $data['umlElement'];
            if(isSet($data['buildPath'])) $this->buildPath = $data['buildPath'];
            //associationEnds
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['builds'])){
                if(!is_object(current($data['builds']))){
                    $newVal = new BuildCollection();
                    foreach($data['builds'] as $el) $newVal->add(Build::create($el,$this->loadObject));
                    $this->builds = $newVal;
                }else  $this->builds = $data['builds'];
            }*/
    
            if(isSet($data['builds'])){
                if(!is_object(current($data['builds']))){
                    $newVal = new BuildCollection();
                    foreach($data['builds'] as $el) $newVal->add(Build::create($el,$this->loadObject));
                    $this->builds = $newVal;
                }else $this->builds = $data['builds'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['workPackages'])){
                if(!is_object(current($data['workPackages']))){
                    $newVal = new WorkPackageCollection();
                    foreach($data['workPackages'] as $el) $newVal->add(WorkPackage::create($el,$this->loadObject));
                    $this->workPackages = $newVal;
                }else  $this->workPackages = $data['workPackages'];
            }*/
    
            if(isSet($data['workPackages'])){
                if(!is_object(current($data['workPackages']))){
                    $newVal = new WorkPackageCollection();
                    foreach($data['workPackages'] as $el) $newVal->add(WorkPackage::create($el,$this->loadObject));
                    $this->workPackages = $newVal;
                }else $this->workPackages = $data['workPackages'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['usedLibs'])){
                if(!is_object(current($data['usedLibs']))){
                    $newVal = new LibraryCollection();
                    foreach($data['usedLibs'] as $el) $newVal->add(Library::create($el,$this->loadObject));
                    $this->usedLibs = $newVal;
                }else  $this->usedLibs = $data['usedLibs'];
            }*/
    
            if(isSet($data['usedLibs'])){
                if(!is_object(current($data['usedLibs']))){
                    $newVal = new LibraryCollection();
                    foreach($data['usedLibs'] as $el) $newVal->add(Library::create($el,$this->loadObject));
                    $this->usedLibs = $newVal;
                }else $this->usedLibs = $data['usedLibs'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['usedLanguages'])){
                if(!is_object(current($data['usedLanguages']))){
                    $newVal = new CodeLanguageCollection();
                    foreach($data['usedLanguages'] as $el) $newVal->add(CodeLanguage::create($el,$this->loadObject));
                    $this->usedLanguages = $newVal;
                }else  $this->usedLanguages = $data['usedLanguages'];
            }*/
    
            if(isSet($data['usedLanguages'])){
                if(!is_object(current($data['usedLanguages']))){
                    $newVal = new CodeLanguageCollection();
                    foreach($data['usedLanguages'] as $el) $newVal->add(CodeLanguage::create($el,$this->loadObject));
                    $this->usedLanguages = $newVal;
                }else $this->usedLanguages = $data['usedLanguages'];
            }
    
            //Helper
            if(isSet($data['lastModified'])) $this->lastModifiedTime = $data['lastModified'];
            if(isSet($data['created'])) $this->createdTime = $data['created'];
        }
    
    
    
    
    }
    
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new Project($data);
    }
    
    public static function createLoad(){
        $data = array(
            'loadObj' => true
        );
        return new Project($data);
    }
    
    public function getLastModifiedTime(){
        return $this->lastModifiedTime;
    }
    
    public function setLastModifiedTime($time){
        $this->lastModifiedTime = $time;
        return $this;
    }
    
    public function hasPrimaryKey(){
        return true;
    }
    
    public function getId(){
        return $this->projektId;
    }
    public function setId($id){
    //        Project::$loadedInstances[$id] = &$this;
        $this->projektId = $id;
        if($this->loadObject) $this->loadedAttributes['projektId'] = true;
        return $this;
    }
    public function getPrimaryKeyName(){
        return "projektId";
    }
    
    public function addToJs($selectionString = ''){
    //    if(!$calledFromChild && get_class($this) == '' && (!$this->loadObject))
            FCVJsObj::add('', $this, $selectionString);
            return $this;
    }
    public function throwError($msg){
        if(!Debug::getObj()->getThrowErrors()) return "";
        else{
            throw new Exception($msg);
        }
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return BuildCollection
     * @throws Exception
     */
    public function getBuilds($forceLoad = false){
        if(is_null($this->builds) || empty($this->builds)){
            $this->throwError('class Project with id '.$this->getId().' has no Attributes builds');
            //throw new Exception('class Project with id '.$this->getId().' has no Attributes builds');
        }
        return $this->builds;
    }
    
    public function hasBuilds($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getBuilds($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param BuildCollection $build
     */
    public function setBuilds(BuildCollection $builds){
        if(!$this->loadObject) $builds->setProject($this);
        $this->builds = $builds;
        if($this->loadObject) $this->loadedAttributes['builds'] = true;
        return $this;
    }
    
    /**
     * @param Project $build
     */
    public function addBuild(Build $build){
            if(!$this->loadObject) $build->setProject($this);
        if(!$build->hasPrimaryKey()){
            if(!$this->loadObject) $build->setParent($this, count($this->builds));
        }
        if($this->loadObject){
            $this->loadedAttributes['builds'] = true;
            if(!$this->builds) $this->builds = new BuildCollection();
        }
        elseif(!$this->builds){
            if(!$this->hasBuilds()) $this->builds = new BuildCollection();
        }
        $this->builds->add($build);
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return WorkPackageCollection
     * @throws Exception
     */
    public function getWorkPackages($forceLoad = false){
        if(is_null($this->workPackages) || empty($this->workPackages)){
            $this->throwError('class Project with id '.$this->getId().' has no Attributes workPackages');
            //throw new Exception('class Project with id '.$this->getId().' has no Attributes workPackages');
        }
        return $this->workPackages;
    }
    
    public function hasWorkPackages($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getWorkPackages($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param WorkPackageCollection $workPackage
     */
    public function setWorkPackages(WorkPackageCollection $workPackages){
        if(!$this->loadObject) $workPackages->setProject($this);
        $this->workPackages = $workPackages;
        if($this->loadObject) $this->loadedAttributes['workPackages'] = true;
        return $this;
    }
    
    /**
     * @param Project $workPackage
     */
    public function addWorkPackage(WorkPackage $workPackage){
            if(!$this->loadObject) $workPackage->setProject($this);
        if(!$workPackage->hasPrimaryKey()){
            if(!$this->loadObject) $workPackage->setParent($this, count($this->workPackages));
        }
        if($this->loadObject){
            $this->loadedAttributes['workPackages'] = true;
            if(!$this->workPackages) $this->workPackages = new WorkPackageCollection();
        }
        elseif(!$this->workPackages){
            if(!$this->hasWorkPackages()) $this->workPackages = new WorkPackageCollection();
        }
        $this->workPackages->add($workPackage);
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return LibraryCollection
     * @throws Exception
     */
    public function getUsedLibs($forceLoad = false){
        if(is_null($this->usedLibs) || empty($this->usedLibs)){
            $this->throwError('class Project with id '.$this->getId().' has no Attributes usedLibs');
            //throw new Exception('class Project with id '.$this->getId().' has no Attributes usedLibs');
        }
        return $this->usedLibs;
    }
    
    public function hasUsedLibs($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getUsedLibs($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param LibraryCollection $library
     */
    public function setUsedLibs(LibraryCollection $usedLibs){
        $this->usedLibs = $usedLibs;
        if($this->loadObject) $this->loadedAttributes['usedLibs'] = true;
        return $this;
    }
    
    /**
     * @param Project $library
     */
    public function addLibrary(Library $library){
        if(!$library->hasPrimaryKey()){
            if(!$this->loadObject) $library->setParent($this, count($this->usedLibs));
        }
        if($this->loadObject){
            $this->loadedAttributes['usedLibs'] = true;
            if(!$this->usedLibs) $this->usedLibs = new LibraryCollection();
        }
        elseif(!$this->usedLibs){
            if(!$this->hasUsedLibs()) $this->usedLibs = new LibraryCollection();
        }
        $this->usedLibs->add($library);
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return CodeLanguageCollection
     * @throws Exception
     */
    public function getUsedLanguages($forceLoad = false){
        if(is_null($this->usedLanguages) || empty($this->usedLanguages)){
            $this->throwError('class Project with id '.$this->getId().' has no Attributes usedLanguages');
            //throw new Exception('class Project with id '.$this->getId().' has no Attributes usedLanguages');
        }
        return $this->usedLanguages;
    }
    
    public function hasUsedLanguages($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getUsedLanguages($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param CodeLanguageCollection $codeLanguage
     */
    public function setUsedLanguages(CodeLanguageCollection $usedLanguages){
        $this->usedLanguages = $usedLanguages;
        if($this->loadObject) $this->loadedAttributes['usedLanguages'] = true;
        return $this;
    }
    
    /**
     * @param Project $codeLanguage
     */
    public function addCodeLanguage(CodeLanguage $codeLanguage){
        if(!$codeLanguage->hasPrimaryKey()){
            if(!$this->loadObject) $codeLanguage->setParent($this, count($this->usedLanguages));
        }
        if($this->loadObject){
            $this->loadedAttributes['usedLanguages'] = true;
            if(!$this->usedLanguages) $this->usedLanguages = new CodeLanguageCollection();
        }
        elseif(!$this->usedLanguages){
            if(!$this->hasUsedLanguages()) $this->usedLanguages = new CodeLanguageCollection();
        }
        $this->usedLanguages->add($codeLanguage);
        return $this;
    }
    
    //Getter & Setter
    
    public function get($name){
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            return $this->$name->get($subName);
        }else return $this->$name;
    }
    
    public function has($name){
        if(strpos('.', $name) !== false){
            $spl = explode('.', $name, 2);
            return $this->get($spl[0])->has($spl[1]);
        }
        return (property_exists($this, $name));
    }
    
    public function set($name, $value){
        $possibleFields = array(
        'projektId',
        'name',
        'umlPath',
        'mysqlModel',
        'umlModel',
        'mysqlDb',
        'mysqlUser',
        'mysqlPassword',
        'mysqlHost',
        'mysqlPort',
        'umlElement',
        'buildPath',
        'builds',
        'workPackages',
        'usedLibs',
        'usedLanguages',
        );
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class Project');
            if(!$this->$name){
                if(false);
                elseif($name == 'builds'){
                    $this->builds = Build::create(array(),$this->loadObject)->set($subName,$value);
                    $this->builds = array($this->builds->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['builds'] = true;
                }
                elseif($name == 'workPackages'){
                    $this->workPackages = WorkPackage::create(array(),$this->loadObject)->set($subName,$value);
                    $this->workPackages = array($this->workPackages->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['workPackages'] = true;
                }
                elseif($name == 'usedLibs'){
                    $this->usedLibs = Library::create(array(),$this->loadObject)->set($subName,$value);
                    $this->usedLibs = array($this->usedLibs->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['usedLibs'] = true;
                }
                elseif($name == 'usedLanguages'){
                    $this->usedLanguages = CodeLanguage::create(array(),$this->loadObject)->set($subName,$value);
                    $this->usedLanguages = array($this->usedLanguages->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['usedLanguages'] = true;
                }
                else die('Error');
            }else $this->$name->set($subName,$value);
        }else{
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class Project');
            $this->$name = $value;
            if($this->loadObject) $this->loadedAttributes[$name] = true;
        }
        return $this;
    }
    
    /**
     *
     * @return Integer
     * @throws Exception
     */
     public function getProjektId(){
        return $this->projektId;
     }
    
     public function hasProjektId(){
        try {
            $this->getProjektId();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Integer $projektId
     * @return Project
     */
     public function setProjektId($projektId){
        ;
        $this->projektId = $projektId;
        if($this->loadObject) $this->loadedAttributes['projektId'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getName(){
        return $this->name;
     }
    
     public function hasName(){
        try {
            $this->getName();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $name
     * @return Project
     */
     public function setName($name){
        ;
        $this->name = $name;
        if($this->loadObject) $this->loadedAttributes['name'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getUmlPath(){
        return $this->umlPath;
     }
    
     public function hasUmlPath(){
        try {
            $this->getUmlPath();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $umlPath
     * @return Project
     */
     public function setUmlPath($umlPath){
        ;
        $this->umlPath = $umlPath;
        if($this->loadObject) $this->loadedAttributes['umlPath'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getMysqlModel(){
        return $this->mysqlModel;
     }
    
     public function hasMysqlModel(){
        try {
            $this->getMysqlModel();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $mysqlModel
     * @return Project
     */
     public function setMysqlModel($mysqlModel){
        ;
        $this->mysqlModel = $mysqlModel;
        if($this->loadObject) $this->loadedAttributes['mysqlModel'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getUmlModel(){
        return $this->umlModel;
     }
    
     public function hasUmlModel(){
        try {
            $this->getUmlModel();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $umlModel
     * @return Project
     */
     public function setUmlModel($umlModel){
        ;
        $this->umlModel = $umlModel;
        if($this->loadObject) $this->loadedAttributes['umlModel'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getMysqlDb(){
        return $this->mysqlDb;
     }
    
     public function hasMysqlDb(){
        try {
            $this->getMysqlDb();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $mysqlDb
     * @return Project
     */
     public function setMysqlDb($mysqlDb){
        ;
        $this->mysqlDb = $mysqlDb;
        if($this->loadObject) $this->loadedAttributes['mysqlDb'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getMysqlUser(){
        return $this->mysqlUser;
     }
    
     public function hasMysqlUser(){
        try {
            $this->getMysqlUser();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $mysqlUser
     * @return Project
     */
     public function setMysqlUser($mysqlUser){
        ;
        $this->mysqlUser = $mysqlUser;
        if($this->loadObject) $this->loadedAttributes['mysqlUser'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getMysqlPassword(){
        return $this->mysqlPassword;
     }
    
     public function hasMysqlPassword(){
        try {
            $this->getMysqlPassword();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $mysqlPassword
     * @return Project
     */
     public function setMysqlPassword($mysqlPassword){
        ;
        $this->mysqlPassword = $mysqlPassword;
        if($this->loadObject) $this->loadedAttributes['mysqlPassword'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getMysqlHost(){
        return $this->mysqlHost;
     }
    
     public function hasMysqlHost(){
        try {
            $this->getMysqlHost();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $mysqlHost
     * @return Project
     */
     public function setMysqlHost($mysqlHost){
        ;
        $this->mysqlHost = $mysqlHost;
        if($this->loadObject) $this->loadedAttributes['mysqlHost'] = true;
        return $this;
     }
    /**
     *
     * @return Integer
     * @throws Exception
     */
     public function getMysqlPort(){
        return $this->mysqlPort;
     }
    
     public function hasMysqlPort(){
        try {
            $this->getMysqlPort();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Integer $mysqlPort
     * @return Project
     */
     public function setMysqlPort($mysqlPort){
        ;
        $this->mysqlPort = $mysqlPort;
        if($this->loadObject) $this->loadedAttributes['mysqlPort'] = true;
        return $this;
     }
    /**
     *
     * @return Integer
     * @throws Exception
     */
     public function getUmlElement(){
        return $this->umlElement;
     }
    
     public function hasUmlElement(){
        try {
            $this->getUmlElement();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Integer $umlElement
     * @return Project
     */
     public function setUmlElement($umlElement){
        ;
        $this->umlElement = $umlElement;
        if($this->loadObject) $this->loadedAttributes['umlElement'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getBuildPath(){
        return $this->buildPath;
     }
    
     public function hasBuildPath(){
        try {
            $this->getBuildPath();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $buildPath
     * @return Project
     */
     public function setBuildPath($buildPath){
        ;
        $this->buildPath = $buildPath;
        if($this->loadObject) $this->loadedAttributes['buildPath'] = true;
        return $this;
     }
    protected $visitedByToArray = false;
    public function toJSObject($returnArray = false){
        if($this->loadObject) return false;
        if($this->visitedByToArray){
    //            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return '[VALENCODE]jsModel["Project.'.$this->getId().'"][VALENCODE]';
                    return array(
                        'class' => 'Project',
                        'id' => $this->getId()
                    );
                }
                return false;
    //            }
    //            return array(
    //                'class' => 'Project',
    //                'id' => rand()
    //            );
        }
        $retArray = array();
        
        $this->visitedByToArray = true;
        
        if(isSet($this->projektId) && !is_null($this->projektId)) $retArray['projektId'] = (!is_object($this->projektId))?str_replace('"', '\"', $this->projektId):($this->projektId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (!is_object($this->name))?str_replace('"', '\"', $this->name):($this->name);
        if(isSet($this->umlPath) && !is_null($this->umlPath)) $retArray['umlPath'] = (!is_object($this->umlPath))?str_replace('"', '\"', $this->umlPath):($this->umlPath);
        if(isSet($this->mysqlModel) && !is_null($this->mysqlModel)) $retArray['mysqlModel'] = (!is_object($this->mysqlModel))?str_replace('"', '\"', $this->mysqlModel):($this->mysqlModel);
        if(isSet($this->umlModel) && !is_null($this->umlModel)) $retArray['umlModel'] = (!is_object($this->umlModel))?str_replace('"', '\"', $this->umlModel):($this->umlModel);
        if(isSet($this->mysqlDb) && !is_null($this->mysqlDb)) $retArray['mysqlDb'] = (!is_object($this->mysqlDb))?str_replace('"', '\"', $this->mysqlDb):($this->mysqlDb);
        if(isSet($this->mysqlUser) && !is_null($this->mysqlUser)) $retArray['mysqlUser'] = (!is_object($this->mysqlUser))?str_replace('"', '\"', $this->mysqlUser):($this->mysqlUser);
        if(isSet($this->mysqlPassword) && !is_null($this->mysqlPassword)) $retArray['mysqlPassword'] = (!is_object($this->mysqlPassword))?str_replace('"', '\"', $this->mysqlPassword):($this->mysqlPassword);
        if(isSet($this->mysqlHost) && !is_null($this->mysqlHost)) $retArray['mysqlHost'] = (!is_object($this->mysqlHost))?str_replace('"', '\"', $this->mysqlHost):($this->mysqlHost);
        if(isSet($this->mysqlPort) && !is_null($this->mysqlPort)) $retArray['mysqlPort'] = (!is_object($this->mysqlPort))?str_replace('"', '\"', $this->mysqlPort):($this->mysqlPort);
        if(isSet($this->umlElement) && !is_null($this->umlElement)) $retArray['umlElement'] = (!is_object($this->umlElement))?str_replace('"', '\"', $this->umlElement):($this->umlElement);
        if(isSet($this->buildPath) && !is_null($this->buildPath)) $retArray['buildPath'] = (!is_object($this->buildPath))?str_replace('"', '\"', $this->buildPath):($this->buildPath);
    
        if(isSet($this->builds) && !is_null($this->builds) && $this->builds !== false){
            if(!is_object($this->builds)) $val = $this->builds;
            else{
                $val = array();
                foreach($this->builds as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['builds'] = $val;
            //$retArray['builds'] = ($simple)?$this->builds->getId():$this->builds->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->workPackages) && !is_null($this->workPackages) && $this->workPackages !== false){
            if(!is_object($this->workPackages)) $val = $this->workPackages;
            else{
                $val = array();
                foreach($this->workPackages as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['workPackages'] = $val;
            //$retArray['workPackages'] = ($simple)?$this->workPackages->getId():$this->workPackages->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->usedLibs) && !is_null($this->usedLibs) && $this->usedLibs !== false){
            if(!is_object($this->usedLibs)) $val = $this->usedLibs;
            else{
                $val = array();
                foreach($this->usedLibs as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['usedLibs'] = $val;
            //$retArray['usedLibs'] = ($simple)?$this->usedLibs->getId():$this->usedLibs->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->usedLanguages) && !is_null($this->usedLanguages) && $this->usedLanguages !== false){
            if(!is_object($this->usedLanguages)) $val = $this->usedLanguages;
            else{
                $val = array();
                foreach($this->usedLanguages as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['usedLanguages'] = $val;
            //$retArray['usedLanguages'] = ($simple)?$this->usedLanguages->getId():$this->usedLanguages->toJSObject($simple, $treeVisit);
    
         }
        return ($returnArray)?$retArray:'[VALENCODE]new Project('.json_encode($retArray).')[VALENCODE]';
    }
    
    public function toArray($simple = false, $treeVisit = false){
        if($this->visitedByToArray){
            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return array(
    //                        'class' => 'Project',
    //                        'id' => $this->getId()
    //                    );
                    return '[VALENCODE]jsModel["Project"]["'.$this->getId().'"][VALENCODE]';
                }
            }
            return false;
        }
        if($treeVisit) $this->visitedByToArray = true;
        $retArray = array();
        
        if(isSet($this->projektId) && !is_null($this->projektId)) $retArray['projektId'] = (is_numeric($this->projektId))?$this->projektId:($this->projektId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (is_numeric($this->name))?$this->name:($this->name);
        if(isSet($this->umlPath) && !is_null($this->umlPath)) $retArray['umlPath'] = (is_numeric($this->umlPath))?$this->umlPath:($this->umlPath);
        if(isSet($this->mysqlModel) && !is_null($this->mysqlModel)) $retArray['mysqlModel'] = (is_numeric($this->mysqlModel))?$this->mysqlModel:($this->mysqlModel);
        if(isSet($this->umlModel) && !is_null($this->umlModel)) $retArray['umlModel'] = (is_numeric($this->umlModel))?$this->umlModel:($this->umlModel);
        if(isSet($this->mysqlDb) && !is_null($this->mysqlDb)) $retArray['mysqlDb'] = (is_numeric($this->mysqlDb))?$this->mysqlDb:($this->mysqlDb);
        if(isSet($this->mysqlUser) && !is_null($this->mysqlUser)) $retArray['mysqlUser'] = (is_numeric($this->mysqlUser))?$this->mysqlUser:($this->mysqlUser);
        if(isSet($this->mysqlPassword) && !is_null($this->mysqlPassword)) $retArray['mysqlPassword'] = (is_numeric($this->mysqlPassword))?$this->mysqlPassword:($this->mysqlPassword);
        if(isSet($this->mysqlHost) && !is_null($this->mysqlHost)) $retArray['mysqlHost'] = (is_numeric($this->mysqlHost))?$this->mysqlHost:($this->mysqlHost);
        if(isSet($this->mysqlPort) && !is_null($this->mysqlPort)) $retArray['mysqlPort'] = (is_numeric($this->mysqlPort))?$this->mysqlPort:($this->mysqlPort);
        if(isSet($this->umlElement) && !is_null($this->umlElement)) $retArray['umlElement'] = (is_numeric($this->umlElement))?$this->umlElement:($this->umlElement);
        if(isSet($this->buildPath) && !is_null($this->buildPath)) $retArray['buildPath'] = (is_numeric($this->buildPath))?$this->buildPath:($this->buildPath);
        if(!$simple && isSet($this->builds) && !is_null($this->builds) && $this->builds !== false){
            if(is_numeric($this->builds)) $val = $this->builds;
            else{
                $val = array();
                foreach($this->builds as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['builds'] = $val;
            //$retArray['builds'] = ($simple)?$this->builds->getId():$this->builds->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->workPackages) && !is_null($this->workPackages) && $this->workPackages !== false){
            if(is_numeric($this->workPackages)) $val = $this->workPackages;
            else{
                $val = array();
                foreach($this->workPackages as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['workPackages'] = $val;
            //$retArray['workPackages'] = ($simple)?$this->workPackages->getId():$this->workPackages->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->usedLibs) && !is_null($this->usedLibs) && $this->usedLibs !== false){
            if(is_numeric($this->usedLibs)) $val = $this->usedLibs;
            else{
                $val = array();
                foreach($this->usedLibs as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['usedLibs'] = $val;
            //$retArray['usedLibs'] = ($simple)?$this->usedLibs->getId():$this->usedLibs->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->usedLanguages) && !is_null($this->usedLanguages) && $this->usedLanguages !== false){
            if(is_numeric($this->usedLanguages)) $val = $this->usedLanguages;
            else{
                $val = array();
                foreach($this->usedLanguages as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['usedLanguages'] = $val;
            //$retArray['usedLanguages'] = ($simple)?$this->usedLanguages->getId():$this->usedLanguages->toArray($simple, $treeVisit);
    
         }
        return $retArray;
    }private $tplVars = array();
    public function addTemplateVar($name, $content){
        $this->tplVars[$name] = $content;
    }
    
    private $ignoreErrors = false;
    /**
     * get Html Template
     * @param string $template template name
     */
    public function getHtml($template = "default"){
        $availableTemplates =  array(
            'default'
            ,'default'
            ,'BuildView'
        );
        if(!in_array($template, $availableTemplates)){
            throw new Exception('Template <b>'.$template.'</b> doesn\'t exist for <b>Project</b>');
        }
        $m = new TemplateEngine;
        
        $templates = array(
            'default' => '' //809
            ,'BuildView' => '<div class="jumbotron project" data-projectid="{{getId}}" id="project{{getId}}">
        <h2>{{getName}}</h2>
        
        
        <button class="btn btn-success addBuild" data-projectid="{{getId}}">NEW</button>
        
        <h3>Builds</h3>
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Path</th>
                    <th>First</th>
                    <th>Create Workpackages</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{#getBuilds}}
                <tr>
                    <td>{{getName}}</td>
                    <td>{{getBuildPath}}</td>
                    <td>{{getFirstBuild}}</td>
                    <td>{{getBuildWorkpackages}}</td>
                    <td>
                        {{^disabled}}
                        <a class="btn btn-danger" href="?action=update&id={{../getId}}&build={{getId}}">Build</a> 
                        <a class="btn btn-success" href=".{{getBuildPath}}/">Show</a>
                        <a class="btn btn-default" href="?action=updateWP&id={{../getId}}&build={{getId}}">Update Workpackages From Source</a>
                        <a class="btn btn-default editBuild" data-buildid="{{getId}}">Edit</a>
                        {{/disabled}}
                        {{#disabled}}
                        Disabled
                        {{/disabled}}
                    </td>
                </tr>
            {{/getBuilds}}
            </tbody>
        </table>
    </div>' //830
        );
        $tplSrc = $templates[$template];
        
        foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, $this);
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    }
    
    public static function getSubHtml($operationName, $template = 'default'){
    
        
        $availableTemplates =  array(
            'build' => array(
                'default'
                ,'default'
            ),
            'update' => array(
                'default'
                ,'default'
            ),
            'buildWorkPackages' => array(
                'default'
                ,'default'
            ),
            'buildLibraries' => array(
                'default'
                ,'default'
            ),
            'getCurrentBuild' => array(
                'default'
                ,'default'
            )
        );
        if(!isset($availableTemplates[$operationName]) || !in_array($template, $availableTemplates[$operationName])){
            throw new Exception('Template OperationTemplate <b>'.$template.'</b> doesn\'t exist for <b>Project.'.$operationName.'</b>');
        }
        $m = new TemplateEngine;
        $tplSrc = file_get_contents('FCVAPs/fcv/fcvPackage/Project/'.$operationName.'/'.$operationName.'.'.$template.'.html');
        /*foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }*/
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, array());
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    
    }
}
