<?php
namespace fcv\fcvPackage;


use \fcv\fcvPackage\CodeModulCollection;
use \fcv\fcvPackage\ModulPositionCollection;
use \fcv\fcvPackage\CodeLanguageCollection;
class CodeTemplateCollection
        
         extends CodeModulCollection
        implements \Iterator, \ArrayAccess, \Countable {
    private $index = 0;
    /**
     * @var CodeTemplate[] $objArray
     */
    protected $objArray = array();


    public function __construct($data = array()){
        $this->objArray = $data;
    }

    /**
     * @param CodeTemplate[] $data
     * @return CodeTemplateCollection inserted Id
     */
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new CodeTemplateCollection($data);
    }
    /**
     * @param CodeTemplate $el
     * @return CodeTemplateCollection inserted Id
     */
    public function add($el){
        $this->objArray[] = $el;
    }

    public function current(): mixed {
        return $this->objArray[$this->index];
    }

    public function key(): int {
        return $this->index;
    }

    public function next(): void {
        $this->index++;
    }

    public function rewind(): void {
        $this->index = 0;
    }

    public function valid(): bool {
        return isset($this->objArray[$this->index]);
    }
    public function append($el) {
        return $this->add($el);
    }
    public function count(): int {
        return count($this->objArray);
    }

    public function offsetExists($offset): bool {
        return isSet($this->objArray[$offset]);
    }

    public function offsetGet($offset): mixed {
        return $this->objArray[$offset];
    }

    public function offsetSet($offset, $value): void {
        $this->objArray[$offset] = $value;
    }

    public function offsetUnset($offset): void {
        unset($this->objArray[$offset]);
    }

    public function seek($position): void {
        $this->index = $position;
    }

    public function serialize() {
        return serialize($this->objArray);
    }

    public function unserialize($serialized) {
        return unserialize($serialized);
    }
    //UserMethods
    /**
      *
      * @return CodeTemplateCollection this Collection Object
      */
    public function loadTemplateFromFile(){
        foreach($this->objArray as $el){
           $el->loadTemplateFromFile();
       }
       return $this;
    }
    /**
      *
      * @return CodeTemplateCollection this Collection Object
      */
    public function build(){
        foreach($this->objArray as $el){
           $el->build();
       }
       return $this;
    }
    /**
      *
      * @return CodeTemplateCollection this Collection Object
      */
    public function loadExtend(){
        foreach($this->objArray as $el){
           $el->loadExtend();
       }
       return $this;
    }

    //Setters
    /**
    *
    * @param ModulPosition $position
    * @return CodeTemplate
    */
    public function setPosition($position){
       foreach($this->objArray as $el){
           $el->setPosition($position);
       }
       return $this;
    }
    /**
    *
    * @param String $template
    * @return CodeTemplate
    */
    public function setTemplate($template){
       foreach($this->objArray as $el){
           $el->setTemplate($template);
       }
       return $this;
    }
    /**
    *
    * @param CodeLanguage $language
    * @return CodeTemplate
    */
    public function setLanguage($language){
       foreach($this->objArray as $el){
           $el->setLanguage($language);
       }
       return $this;
    }
    /**
    *
    * @param String $stereoType
    * @return CodeTemplate
    */
    public function setStereoType($stereoType){
       foreach($this->objArray as $el){
           $el->setStereoType($stereoType);
       }
       return $this;
    }
    /**
    *
    * @param CodeModul $extend
    * @return CodeTemplate
    */
    public function setExtend($extend){
       foreach($this->objArray as $el){
           $el->setExtend($extend);
       }
       return $this;
    }
    /**
    *
    * @param String $requiereUmlProperties
    * @return CodeTemplate
    */
    public function setRequiereUmlProperties($requiereUmlProperties){
       foreach($this->objArray as $el){
           $el->setRequiereUmlProperties($requiereUmlProperties);
       }
       return $this;
    }
    /**
    *
    * @param Boolean $updated
    * @return CodeTemplate
    */
    public function setUpdated($updated){
       foreach($this->objArray as $el){
           $el->setUpdated($updated);
       }
       return $this;
    }
    public function setModul(CodeModul $modul){
       foreach($this->objArray as $el){
           $el->setModul($modul);
       }
       return $this;
    }
    public function save(){
       foreach($this->objArray as $el){
           $el->save();
       }
       return $this;
    }
    public function treeSave(){
       foreach($this->objArray as $el){
           $el->treeSave();
       }
       return $this;
    }
    public function getHtml($tplName = 'default'){
       $html = '';
       foreach($this->objArray as $el){
           $html .= $el->getHtml($tplName);
       }
       return $html;
    }
    
    public function each($cb){
        foreach($this->objArray as $i => $o){
            $cb($o,$i);
        }
        return $this;
    }
    
    public function addToJs(){
        foreach($this->objArray as $i => $o){
            $o->addToJs();
        }
        return $this;
    }
    //Map Reduce Pattern
    public function map($mapFunction){
        $ret = array();
        foreach($this->objArray as $i => $o){
            $ret[$i] = $mapFunction($o);
        }
        return $ret;
    }
    
    public function reduce($reduceFunction){

        $length = count($this->objArray);
        if($length <= 1) return $this->objArray;

        $array = $this->objArray;
        $out = array_shift($array);
        do {
            $next = array_shift($array);
            $out = $reduceFunction($out, $next);
        } while(!empty($array));

        return $out;
    }
}
