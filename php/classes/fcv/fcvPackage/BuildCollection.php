<?php
namespace fcv\fcvPackage;


use \fcv\fcvPackage\ProjectCollection;
use \fcv\fcvPackage\CodeModulCollection;
class BuildCollection
        
        
        implements \Iterator, \ArrayAccess, \Countable {
    private $index = 0;
    /**
     * @var Build[] $objArray
     */
    protected $objArray = array();


    public function __construct($data = array()){
        $this->objArray = $data;
    }

    /**
     * @param Build[] $data
     * @return BuildCollection inserted Id
     */
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new BuildCollection($data);
    }
    /**
     * @param Build $el
     * @return BuildCollection inserted Id
     */
    public function add($el){
        $this->objArray[] = $el;
    }

    public function current() {
        return $this->objArray[$this->index];
    }

    public function key() {
        return $this->index;
    }

    public function next() {
        $this->index++;
    }

    public function rewind() {
        $this->index = 0;
    }

    public function valid() {
        return isset($this->objArray[$this->index]);
    }
    public function append($el) {
        return $this->add($el);
    }
    public function count() {
        return count($this->objArray);
    }

    public function offsetExists($offset) {
        return isSet($this->objArray[$offset]);
    }

    public function offsetGet($offset) {
        return $this->objArray[$offset];
    }

    public function offsetSet($offset, $value) {
        $this->objArray[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->objArray[$offset]);
    }

    public function seek($position) {
        $this->index = $position;
    }

    public function serialize() {
        return serialize($this->objArray);
    }

    public function unserialize($serialized) {
        return unserialize($serialized);
    }
    //UserMethods
    /**
      *
      * @param  $Build
      * @return BuildCollection this Collection Object
      */

    //Setters
    /**
    *
    * @param Integer $buildId
    * @return Build
    */
    public function setBuildId($buildId){
       foreach($this->objArray as $el){
           $el->setBuildId($buildId);
       }
       return $this;
    }
    /**
    *
    * @param String $name
    * @return Build
    */
    public function setName($name){
       foreach($this->objArray as $el){
           $el->setName($name);
       }
       return $this;
    }
    /**
    *
    * @param Boolean $firstBuild
    * @return Build
    */
    public function setFirstBuild($firstBuild){
       foreach($this->objArray as $el){
           $el->setFirstBuild($firstBuild);
       }
       return $this;
    }
    /**
    *
    * @param String $buildPath
    * @return Build
    */
    public function setBuildPath($buildPath){
       foreach($this->objArray as $el){
           $el->setBuildPath($buildPath);
       }
       return $this;
    }
    /**
    *
    * @param Boolean $buildWorkpackages
    * @return Build
    */
    public function setBuildWorkpackages($buildWorkpackages){
       foreach($this->objArray as $el){
           $el->setBuildWorkpackages($buildWorkpackages);
       }
       return $this;
    }
    /**
    *
    * @param Boolean $disabled
    * @return Build
    */
    public function setDisabled($disabled){
       foreach($this->objArray as $el){
           $el->setDisabled($disabled);
       }
       return $this;
    }
    public function setProject(Project $project){
       foreach($this->objArray as $el){
           $el->setProject($project);
       }
       return $this;
    }
    public function setUsedModules(CodeModul $usedModules){
       foreach($this->objArray as $el){
           $el->setUsedModules($usedModules);
       }
       return $this;
    }
    public function save(){
       foreach($this->objArray as $el){
           $el->save();
       }
       return $this;
    }
    public function treeSave(){
       foreach($this->objArray as $el){
           $el->treeSave();
       }
       return $this;
    }
    public function getHtml($tplName = 'default'){
       $html = '';
       foreach($this->objArray as $el){
           $html .= $el->getHtml($tplName);
       }
       return $html;
    }
    
    public function each($cb){
        foreach($this->objArray as $i => $o){
            $cb($o,$i);
        }
        return $this;
    }
    
    public function addToJs(){
        foreach($this->objArray as $i => $o){
            $o->addToJs();
        }
        return $this;
    }
    //Map Reduce Pattern
    public function map($mapFunction){
        $ret = array();
        foreach($this->objArray as $i => $o){
            $ret[$i] = $mapFunction($o);
        }
        return $ret;
    }
    
    public function reduce($reduceFunction){

        $length = count($this->objArray);
        if($length <= 1) return $this->objArray;

        $array = $this->objArray;
        $out = array_shift($array);
        do {
            $next = array_shift($array);
            $out = $reduceFunction($out, $next);
        } while(!empty($array));

        return $out;
    }
}
