<?php
namespace fcv\fcvPackage;

use \QueryBuilder;

use \fcv\fcvPackage\CodeModulCollectionPrepare;class ModulPositionCollectionPrepare {

    protected $tableName;
    protected $tableNameAlias = '';

    /**
     * @var QueryBuilder
     */
    protected $query;
    protected $objectMap = array();

    protected $primaryKeyIndex = 0;

    public function __construct(){
        $this->tableName = 'ModulPosition';
        $this->query = \QueryBuilder::create();
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_NUM);
    }

    /**
     * @param ModulPosition[] $data
     * @return ModulPositionCollectionPrepare inserted Id
     */
    public static function create(){
        return new ModulPositionCollectionPrepare();
    }

    /**
     * @return int
     */
    public function getPrimaryKeyIndex(){
        return $this->primaryKeyIndex;
    }

    /**
     * @return array
     */
    public function getObjectMap(){
        return $this->objectMap;
    }

    /**
     * @return QueryBuilder
     */
    public function getQuery(){
        return $this->query;
    }

    /**
     * @return string
     */
    public function getTableName(){
        return $this->tableName;
    }

    /**
     * @return ModulPositionCollectionPrepare
     */
    public function setTableName($tableName){
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableNameAlias(){
        return ($this->tableNameAlias != '')?$this->tableNameAlias:$this->tableName;
    }

    /**
     * @return ModulPositionCollectionPrepare
     */
    public function setTableNameAlias($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias;
        return $this;
    }

    /**
     * @return string
     */
    public function setTableNameAliasPrepend($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias.(($this->tableNameAlias == '')?$this->tableName:$this->tableNameAlias);
        return $this;
    }

    /**
      * Add a basic where clause to the query.
      *
      * @param  string  $column
      * @param  string  $operator
      * @param  mixed   $value
      * @param  string  $boolean
      * @return ModulPositionCollectionPrepare
      *
      * @throws \InvalidArgumentException
      */
    public function where($column, $operator = null, $value = null, $boolean = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->where($spl[1], $operator, $value, $boolean);
        }else{
            $this->query->where($column, $operator, $value, $boolean);
        }
        return $this;
    }
    
    public function orWhere($column, $operator = null, $value = null, $logicConnector = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->orWhere($spl[1], $operator, $value, $logicConnector);
        }else{
            $this->query->orWhere($column, $operator, $value, $logicConnector);
        }
        return $this;
    }

    /**
     * @return ModulPositionCollectionPrepare
     */
    public function whereRaw($where){
        $this->query->whereRaw($where);
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ModulPositionCollectionPrepare
     */
    public function wherePositionId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('positionId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ModulPositionCollectionPrepare
     */
    public function orWherePositionId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('positionId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ModulPositionCollectionPrepare
     */
    public function selectPositionId(){
        $this->primaryKeyIndex = count($this->query->columns);
        $this->objectMap[] = array(
            'class' => 'ModulPosition',
            'field' => 'positionId',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('positionId');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ModulPositionCollectionPrepare
     */
    public function whereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ModulPositionCollectionPrepare
     */
    public function orWhereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ModulPositionCollectionPrepare
     */
    public function selectName(){
        $this->objectMap[] = array(
            'class' => 'ModulPosition',
            'field' => 'name',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('name');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return ModulPositionCollectionPrepare
     */
    public function whereIndent($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('indent', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return ModulPositionCollectionPrepare
     */
    public function orWhereIndent($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('indent', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return ModulPositionCollectionPrepare
     */
    public function selectIndent(){
        $this->objectMap[] = array(
            'class' => 'ModulPosition',
            'field' => 'indent',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('indent');
        return $this;
    }

    /**
     * @var CodeModulCollectionPrepare
     */
    protected $modul;
    /**
     * @var CodeModulCollectionPrepare
     * @return ModulPositionCollectionPrepare
     */
    public function setModul(CodeModulCollectionPrepare $modul){
       $this->modul = $modul;
       $modul->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getModul(){
       if(!isset($this->modul)) $this->modul = CodeModulCollectionPrepare::create();
       return $this->modul;
    }
    
    
    public function selectModul(){
        $this->objectMap[] = array(
            'class' => 'modul',
            'field' => 'modul',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('modul');
        return $this;
    }





    private $selectAll = false;
    public function selectAll(){
        
        $this->selectPositionId();
        $this->selectName();
        $this->selectIndent();
        $this->selectModul();

        return $this;
    }
    public function prepareQuery(){
        $this->prepareJoins();
        $this->query->from = 'ModulPosition'.(($this->tableNameAlias == '')?'':' AS '.$this->tableNameAlias);
        QueryBuilder::appendWhereWithTable($this->query, $this->getTableNameAlias());
        return $this;
    }
    /**
     * @return ModulPositionCollection
     */
    public function get(){
        $this->prepareQuery();
        $rows = $this->query->get();
        return $this->parseDbRows($rows, $this->getObjectMap());
    }

    /**
     * parse all return rows from sql
     */
    private function parseDbRows($rows, $map){
        $objsArray = array();
        $objs = ModulPositionCollection::create();
        //build assoc Array with all data
        if(!$this->selectAll){
            foreach($rows as $row){
                $primaryField = $row[$this->primaryKeyIndex];

                //extend object if allready exists

                $initObj = (isset($objsArray[$primaryField]))?$objsArray[$primaryField]:array();
                $objsArray[$primaryField] = $this->parseDbRow($row, $map, $initObj);
            }
        }else $objsArray = $rows;
        //convert array to real objects (subobjects are generated in constructors)
        foreach($objsArray as $dataArray){
            $objs->add(new ModulPosition($dataArray));
        }
        return $objs;
    }

    /**
     * parse one row into Object and extend existing ones
     */
    private function parseDbRow($row, $map, $objsData = array(), &$index = 0){
        foreach($map as $m){
            //check if association
            if(isset($m['map'])){
                //check if cardinality is multi
                if($m['multi']){
                    if(!isset($objsData[$m['field']])) $objsData[$m['field']] = array();
                    //get primaryKey ob subobject
                    $id = $row[$index+$m['primaryKeyIndex']];
                    //extend object if allready exists
                    $initObj = (isset($objsData[$m['field']][$id]))?$objsData[$m['field']][$id]:array();
                    //add/extend object with id
                    $objsData[$m['field']][$id] = $this->parseDbRow($row, $m['map'], $initObj, $index);
                }else{
                    $objsData[$m['field']] = $this->parseDbRow($row, $m['map'], array(), $index);
                }
            }
            else{
                $objsData[$m['field']] = $row[$index];
                $index++;
            }
        }
        return $objsData;
    }

    /**
     * create joins depending on associations
     * @return CollectionPrepare
     */
    public function prepareJoins(){
        
        if(!$this->selectAll){
            if(!is_array($this->query->columns) || !in_array('positionId', $this->query->columns)){
                $this->selectpositionId();
            }elseif(!is_array($this->query->columns)){
                $this->selectAll();
            }
        }else{
            $this->query->select('*');
        }
        if(isSet($this->modul)){
            $tableName = $this->modul->getTableNameAlias();
            //Single ModulPosition
             $this->query->join('CodeModul as '.$tableName, $tableName.'.modulId', '=', $this->getTableNameAlias().'.modul', 'LEFT');
            $this->modul->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->modul->getQuery(),$tableName);
            $this->query->mergeQuery($this->modul->getQuery());

            $this->objectMap[] = array(
                'class' => 'CodeModul',
                'field' => 'modul',
                'multi' => false,
                'primaryKeyIndex' => $this->modul->getPrimaryKeyIndex(),
                'map' => $this->modul->getObjectMap()
            );
        }



        return $this;
    }
}
