<?php
namespace fcv\fcvPackage;


use \fcv\fcvPackage\WorkPackageCollection;
class ViewsCollection
        
        
        implements \Iterator, \ArrayAccess, \Countable {
    private $index = 0;
    /**
     * @var Views[] $objArray
     */
    protected $objArray = array();


    public function __construct($data = array()){
        $this->objArray = $data;
    }

    /**
     * @param Views[] $data
     * @return ViewsCollection inserted Id
     */
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new ViewsCollection($data);
    }
    /**
     * @param Views $el
     * @return ViewsCollection inserted Id
     */
    public function add($el){
        $this->objArray[] = $el;
    }

    public function current(): mixed {
        return $this->objArray[$this->index];
    }

    public function key(): int {
        return $this->index;
    }

    public function next(): void {
        $this->index++;
    }

    public function rewind(): void {
        $this->index = 0;
    }

    public function valid(): bool {
        return isset($this->objArray[$this->index]);
    }
    public function append($el): void {
        $this->add($el);
    }
    public function count(): int {
        return count($this->objArray);
    }

    public function offsetExists(mixed $offset): bool {
        return isSet($this->objArray[$offset]);
    }

    public function offsetGet(mixed $offset): mixed {
        return $this->objArray[$offset];
    }

    public function offsetSet(mixed $offset, $value): void {
        $this->objArray[$offset] = $value;
    }

    public function offsetUnset($offset): void {
        unset($this->objArray[$offset]);
    }

    public function seek($position): void {
        $this->index = $position;
    }

    public function serialize() {
        return serialize($this->objArray);
    }

    public function unserialize($serialized) {
        return unserialize($serialized);
    }
    //UserMethods

    //Setters
    /**
    *
    * @param Integer $viewId
    * @return Views
    */
    public function setViewId($viewId){
       foreach($this->objArray as $el){
           $el->setViewId($viewId);
       }
       return $this;
    }
    /**
    *
    * @param String $name
    * @return Views
    */
    public function setName($name){
       foreach($this->objArray as $el){
           $el->setName($name);
       }
       return $this;
    }
    /**
    *
    * @param String $htmlContent
    * @return Views
    */
    public function setHtmlContent($htmlContent){
       foreach($this->objArray as $el){
           $el->setHtmlContent($htmlContent);
       }
       return $this;
    }
    /**
    *
    * @param String $cssContent
    * @return Views
    */
    public function setCssContent($cssContent){
       foreach($this->objArray as $el){
           $el->setCssContent($cssContent);
       }
       return $this;
    }
    public function setWp(WorkPackage $wp){
       foreach($this->objArray as $el){
           $el->setWp($wp);
       }
       return $this;
    }
    public function save(){
       foreach($this->objArray as $el){
           $el->save();
       }
       return $this;
    }
    public function treeSave(){
       foreach($this->objArray as $el){
           $el->treeSave();
       }
       return $this;
    }
    public function getHtml($tplName = 'default'){
       $html = '';
       foreach($this->objArray as $el){
           $html .= $el->getHtml($tplName);
       }
       return $html;
    }
    
    public function each($cb){
        foreach($this->objArray as $i => $o){
            $cb($o,$i);
        }
        return $this;
    }
    
    public function addToJs(){
        foreach($this->objArray as $i => $o){
            $o->addToJs();
        }
        return $this;
    }
    //Map Reduce Pattern
    public function map($mapFunction){
        $ret = array();
        foreach($this->objArray as $i => $o){
            $ret[$i] = $mapFunction($o);
        }
        return $ret;
    }
    
    public function reduce($reduceFunction){

        $length = count($this->objArray);
        if($length <= 1) return $this->objArray;

        $array = $this->objArray;
        $out = array_shift($array);
        do {
            $next = array_shift($array);
            $out = $reduceFunction($out, $next);
        } while(!empty($array));

        return $out;
    }
}
