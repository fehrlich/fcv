<?php
namespace fcv\fcvPackage; ///fcv/fcvPackage/CodeTemplate
    
use Exception;
use mys;
use QueryBuilder;
use fcv\libs\TemplateEngine;
use fcv\libs\Debug;
use fcv\FCVJsObj;


use fcv\fcvPackage\CodeModul;
;
use fcv\fcvPackage\ModulPosition;
use fcv\fcvPackage\CodeLanguage;
use fcv\fcvPackage\CodeTemplate;
use fcv\fcvPackage\CodeTemplateCollectionPrepare;
use fcv\fcvPackage\ModulPositionCollectionPrepare;

class CodeTemplate
        
         extends CodeModul 
        {
    
    //Attributes
    //const POSITION = 'position';
    //const TEMPLATE = 'template';
    //const LANGUAGE = 'language';
    //const STEREOTYPE = 'stereoType';
    //const EXTEND = 'extend';
    //const REQUIEREUMLPROPERTIES = 'requiereUmlProperties';
    //const UPDATED = 'updated';
    
    //Litearls
    
    //Attributes
    /**
     *
     * @var ModulPosition
     */
     public $position;
    const FIELD_POSITION = 'position';
    /**
     *
     * @var String
     */
     public $template;
    const FIELD_TEMPLATE = 'template';
    /**
     *
     * @var CodeLanguage
     */
     public $language;
    const FIELD_LANGUAGE = 'language';
    /**
     *
     * @var String
     */
     public $stereoType;
    const FIELD_STEREOTYPE = 'stereoType';
    /**
     *
     * @var CodeModul
     */
     public $extend;
    const FIELD_EXTEND = 'extend';
    /**
     *
     * @var String
     */
     public $requiereUmlProperties;
    const FIELD_REQUIEREUMLPROPERTIES = 'requiereUmlProperties';
    /**
     *
     * @var Boolean
     */
     public $updated;
    const FIELD_UPDATED = 'updated';
    
    //AssociationsEnds
    
    /**
     * @var CodeModul
     */
    private $modul = false;
    const MODUL = 'modul';
    
    //{/isAssociationClass}}
    //FCV Attributes
    private $apPath = 'FCVAPs/';
    //    public $VIEW = array();
    static $fcvFields = array('position','template','language','stereoType','extend','requiereUmlProperties','updated');
    //Todo: Outsource
    protected $loadObject = false;
    private $loadedAttributes = array();
    
    //Methods
    //UserOperations
    /**
      *
      * @return 
      */
    public  function loadTemplateFromFile(){
        
            /* @var $this CodeTemplate*/
            $path = 'moduleTemplates'.$this->getAbsolutePath();
            try {
                $filepath = $path . '/' . $this->getName() . '.' . $this->getLanguage()->getName();
            } catch (Exception $exc) {
                d(1);
                d($this->getName());
                exit();
            }
            
            //Todo: check if needed !? only on creation ?
            //Debug::getObj()->info('Check '.$this->getName());
            if(!is_dir($path)) mkdir ($path,0777, true);
            if(!file_exists($filepath)) touch($filepath);
            
            //Todo: Check file modified date before file_get_content
            $content = file_get_contents($filepath);
            $this->setUpdated($this->getTemplate() != $content);
            $this->setUpdated(true);
            $this->template = $content;
            
            if($this->getUpdated()){
            //    Debug::getObj()->info('CodeTemplate '.$this->getName().' updated!');
                $this->save();
            }
            return $this;
    
    }
    
    private $updateTemplateFromFile = true;
    
    function setUpdateTemplateFromFile($updateTemplateFromFile) {
        $this->updateTemplateFromFile = $updateTemplateFromFile;
        return $this;
    }

    /**
      *
      * @return 
      */
    public  function build(){
        
            
            /* @var $this CodeTemplate */
            //Debug::getObj()->info('Build ' . $this->getName());
            if($this->getBuildOnlyOnFirstBuild() && $this->getProject()->getCurrentBuild()->getFirstBuild()){
                return '';
            }
            if ($this->requiereUmlProperties != '') {
                $spl = explode(',', $this->requiereUmlProperties);
                foreach ($spl as $prop) {
                    if (substr($prop, 0, 1) == '^') {
                        $negate = true;
                        $prop = substr($prop, 1);
                    }
                    else{
                        $negate = false;
                    }
                    $args =[];
                    if (substr($prop, 0, 1) == ' ') {
                        $args = explode(' ', $prop);
                        $prop = array_shif($args[0]);
                    }
                    $umlElement = $this->getWorkPackage()->getUmlElement();
                    if (!method_exists($umlElement, $prop) || (\call_user_func_array([$umlElement, $prop], $args) == $negate)) {
                        return '';
                    }
                }
            }
            if($this->updateTemplateFromFile){
                $this->loadTemplateFromFile();
            }
            
            if (!$this->getUpdated() && $this->path != '' && !$this->getWorkPackage()->getUmlElement()->hasChanged() && !$this->getWorkPackage()->getUmlElement()->isNew()) {
                return '';
            }
            
            Debug::getObj()->info('BUILD CodeTpl ' . $this->getName() . ' ('.($this->getUpdated()?'U':'').($this->path == ''?'S':'').') for WP '. $this->getWorkPackage()->getName().' ('.($this->getWorkPackage()->getUmlElement()->isNew()?'N':'').($this->getWorkPackage()->getUmlElement()->hasChanged()?'C':'').')');
            
            $content = $this->getTemplate();
            
            $tplEngine = new TemplateEngine();
            if ($this->hasPositions()) {
                $positions = $this->getPositions();
            
                $debug = false;
                foreach ($positions as $pos) {
                   
                    $allChilds = $this->getChilds();
                    $subTemplates = CodeTemplateCollection::create();
                    $allChilds->each(function($el) use(&$subTemplates, $pos){
                        if($el->getPosition()->getName() == $pos->getName()){
                            $subTemplates->add($el);
                        }
                    });
                    $subContent = '';
                    if($subTemplates->count() > 0){
                        foreach ($subTemplates as $subTpl) {
                            $subTpl->setWorkPackage($this->getWorkPackage());
                            $subTplC = $subTpl->build();

                            $subContent .= $subTplC;
                        }
                    }
                    $space = str_pad("", $pos->getIndent() * 4);
                    $subContent = str_replace(array("\r\n", "\n"), array("\n", "\n" . $space), $subContent);
                    $content = str_replace('{{Position' . $pos->getName() . '}}', $subContent, $content);
                }
            }

            $content = $tplEngine->render($content, $this->getWorkPackage()->getUmlElement());
            
            if ($this->path != '') {
                $path = $this->getWorkPackage()->getProject()->getCurrentBuild()->getBuildPath() . '/' . $this->getPath();
                $path = $tplEngine->render($path, $this->getWorkPackage());
                $path = str_replace('//', '/', $path);
                writeContentToFile($path, $content);
                return '';
            }elseif ($this->hasPosition()) {
                return $content;
            } else {
                echo 'Error: CodeTemplate "' . $this->getName() . '" has no Position or Path';
            }
            
    
    }
    /**
      *
      * @return 
      */
    public  function loadExtend(){
        
            /* @var $this CodeTemplate*/
    
    }
    private $lastModifiedTime = false;
    private $createdTime = false;
    public function __construct($data = array(),$calledFromChild = false){
        if(is_string($data)) $data = json_decode($data,true);
        if(count($data) > 0){
            if(isSet($data['loadObj']) && $data['loadObj']){
                $this->loadObject = true;
            }
             parent::__construct($data,true);
        //        if(isset($data['modulId']) && !empty($data['modulId']))
    
            if(isSet($data['position']) && is_array($data['position'])) $this->position = ModulPosition::create($data['position'],$this->loadObject);
            elseif(isSet($data['position'])) $this->position = $data['position'];
            if(isSet($data['template'])) $this->template = $data['template'];
            if(isSet($data['language']) && is_array($data['language'])) $this->language = CodeLanguage::create($data['language'],$this->loadObject);
            elseif(isSet($data['language'])) $this->language = $data['language'];
            if(isSet($data['stereoType'])) $this->stereoType = $data['stereoType'];
            if(isSet($data['extend']) && is_array($data['extend'])) $this->extend = CodeModul::create($data['extend'],$this->loadObject);
            elseif(isSet($data['extend'])) $this->extend = $data['extend'];
            if(isSet($data['requiereUmlProperties'])) $this->requiereUmlProperties = $data['requiereUmlProperties'];
            if(isSet($data['updated'])) $this->updated = $data['updated'];
            //associationEnds
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['modul'])){
                $this->modul = (is_array($data['modul']))?modul::create($data['modul'],$this->loadObject):$data['modul'];
            }*/
    
            if(isSet($data['modul'])){
                $this->modul = (is_array($data['modul']))?CodeModul::create($data['modul'],$this->loadObject):$data['modul'];
            }
    
            //Helper
            if(isSet($data['lastModified'])) $this->lastModifiedTime = $data['lastModified'];
            if(isSet($data['created'])) $this->createdTime = $data['created'];
        }
    
    
    
    
    }
    
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new CodeTemplate($data);
    }
    
    public static function createLoad(){
        $data = array(
            'loadObj' => true
        );
        return new CodeTemplate($data);
    }
    
    public function getLastModifiedTime(){
        return $this->lastModifiedTime;
    }
    
    public function setLastModifiedTime($time){
        $this->lastModifiedTime = $time;
        return $this;
    }
    
    public function hasPrimaryKey(){
        return true;
    }
    
    public function getId(){
        return $this->modulId;
    }
    public function setId($id){
    //        CodeModul::$loadedInstances[$id] = &$this;
        $this->modulId = $id;
        if($this->loadObject) $this->loadedAttributes['modulId'] = true;
        return $this;
    }
    public function getPrimaryKeyName(){
        return "modulId";
    }
    
    public function addToJs($selectionString = ''){
    //    if(!$calledFromChild && get_class($this) == '' && (!$this->loadObject))
            FCVJsObj::add('', $this, $selectionString);
            return $this;
    }
    public function throwError($msg){
        if(!Debug::getObj()->getThrowErrors()) return "";
        else{
            throw new Exception($msg);
        }
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return CodeModul
     * @throws Exception
     */
    public function getModul($forceLoad = false){
        if(is_null($this->modul) || empty($this->modul)){
            $this->throwError('class CodeTemplate with id '.$this->getId().' has no Attributes modul');
            //throw new Exception('class CodeTemplate with id '.$this->getId().' has no Attributes modul');
        }
        return $this->modul;
    }
    
    public function hasModul($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getModul($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param CodeModul $codeModul
     */
    public function setModul(CodeModul $modul){
        $this->modul = $modul;
        if($this->loadObject) $this->loadedAttributes['modul'] = true;
        return $this;
    }
    
    //Getter & Setter
    
    public function get($name){
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            return $this->$name->get($subName);
        }else return $this->$name;
    }
    
    public function has($name){
        if(strpos('.', $name) !== false){
            $spl = explode('.', $name, 2);
            return $this->get($spl[0])->has($spl[1]);
        }
        return (property_exists($this, $name));
    }
    
    public function set($name, $value){
        $possibleFields = array(
        'position',
        'template',
        'language',
        'stereoType',
        'extend',
        'requiereUmlProperties',
        'updated',
        'modul',
        );
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            if(!in_array($name, $possibleFields)) return parent::set($name.'.'.$subName,$value);
            if(!$this->$name){
                if(false);
                elseif($name == 'position'){
                    $this->position = ModulPosition::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['position'] = true;
                }
                elseif($name == 'language'){
                    $this->language = CodeLanguage::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['language'] = true;
                }
                elseif($name == 'extend'){
                    $this->extend = CodeModul::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['extend'] = true;
                }
                elseif($name == 'modul'){
                    $this->modul = CodeModul::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['modul'] = true;
                }
                else die('Error');
            }else $this->$name->set($subName,$value);
        }else{
            if(!in_array($name, $possibleFields)) return parent::set($name,$value);
            $this->$name = $value;
            if($this->loadObject) $this->loadedAttributes[$name] = true;
        }
        return $this;
    }
    
    /**
     *
     * @return ModulPosition
     * @throws Exception
     */
     public function getPosition(){
        if(empty($this->position)){
            $this->throwError('class CodeTemplate with id '.$this->getId().' has no Attributes position');
            //throw new Exception('class CodeTemplate with id '.$this->getId().' has no Attributes position');
        }
        return $this->position;
     }
    
     public function hasPosition(){
        try {
            $this->getPosition();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param ModulPosition $position
     * @return CodeTemplate
     */
     public function setPosition($position){
        if(is_numeric($position)) $position = ModulPosition::create()->setId($position);
        $this->position = $position;
        if($this->loadObject) $this->loadedAttributes['position'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getTemplate(){
        return $this->template;
     }
    
     public function hasTemplate(){
        try {
            $this->getTemplate();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $template
     * @return CodeTemplate
     */
     public function setTemplate($template){
        ;
        $this->template = $template;
        if($this->loadObject) $this->loadedAttributes['template'] = true;
        return $this;
     }
    /**
     *
     * @return CodeLanguage
     * @throws Exception
     */
     public function getLanguage(){
        if(empty($this->language)){
            $this->throwError('class CodeTemplate with id '.$this->getId().' has no Attributes language');
            //throw new Exception('class CodeTemplate with id '.$this->getId().' has no Attributes language');
        }
        return $this->language;
     }
    
     public function hasLanguage(){
        try {
            $this->getLanguage();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param CodeLanguage $language
     * @return CodeTemplate
     */
     public function setLanguage($language){
        if(is_numeric($language)) $language = CodeLanguage::create()->setId($language);
        $this->language = $language;
        if($this->loadObject) $this->loadedAttributes['language'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getStereoType(){
        return $this->stereoType;
     }
    
     public function hasStereoType(){
        try {
            $this->getStereoType();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $stereoType
     * @return CodeTemplate
     */
     public function setStereoType($stereoType){
        ;
        $this->stereoType = $stereoType;
        if($this->loadObject) $this->loadedAttributes['stereoType'] = true;
        return $this;
     }
    /**
     *
     * @return CodeModul
     * @throws Exception
     */
     public function getExtend(){
        if(empty($this->extend)){
            $this->throwError('class CodeTemplate with id '.$this->getId().' has no Attributes extend');
            //throw new Exception('class CodeTemplate with id '.$this->getId().' has no Attributes extend');
        }
        return $this->extend;
     }
    
     public function hasExtend(){
        try {
            $this->getExtend();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param CodeModul $extend
     * @return CodeTemplate
     */
     public function setExtend($extend){
        if(is_numeric($extend)) $extend = CodeModul::create()->setId($extend);
        $this->extend = $extend;
        if($this->loadObject) $this->loadedAttributes['extend'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getRequiereUmlProperties(){
        return $this->requiereUmlProperties;
     }
    
     public function hasRequiereUmlProperties(){
        try {
            $this->getRequiereUmlProperties();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $requiereUmlProperties
     * @return CodeTemplate
     */
     public function setRequiereUmlProperties($requiereUmlProperties){
        ;
        $this->requiereUmlProperties = $requiereUmlProperties;
        if($this->loadObject) $this->loadedAttributes['requiereUmlProperties'] = true;
        return $this;
     }
    /**
     *
     * @return Boolean
     * @throws Exception
     */
     public function getUpdated(){
        return $this->updated;
     }
    
     public function hasUpdated(){
        try {
            $this->getUpdated();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Boolean $updated
     * @return CodeTemplate
     */
     public function setUpdated($updated){
        ;
        $this->updated = $updated;
        if($this->loadObject) $this->loadedAttributes['updated'] = true;
        return $this;
     }
    protected $visitedByToArray = false;
    public function toJSObject($returnArray = false){
        if($this->loadObject) return false;
        if($this->visitedByToArray){
    //            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return '[VALENCODE]jsModel["CodeTemplate.'.$this->getId().'"][VALENCODE]';
                    return array(
                        'class' => 'CodeTemplate',
                        'id' => $this->getId()
                    );
                }
                return false;
    //            }
    //            return array(
    //                'class' => 'CodeTemplate',
    //                'id' => rand()
    //            );
        }
        $retArray = array();
         $retArray = parent::toJSObject(true);
        $this->visitedByToArray = true;
        
        if(isSet($this->position) && !is_null($this->position)) $retArray['position'] = (!is_object($this->position))?str_replace('"', '\"', $this->position):($this->position->toJSObject());
        if(isSet($this->template) && !is_null($this->template)) $retArray['template'] = (!is_object($this->template))?str_replace('"', '\"', $this->template):($this->template);
        if(isSet($this->language) && !is_null($this->language)) $retArray['language'] = (!is_object($this->language))?str_replace('"', '\"', $this->language):($this->language->toJSObject());
        if(isSet($this->stereoType) && !is_null($this->stereoType)) $retArray['stereoType'] = (!is_object($this->stereoType))?str_replace('"', '\"', $this->stereoType):($this->stereoType);
        if(isSet($this->extend) && !is_null($this->extend)) $retArray['extend'] = (!is_object($this->extend))?str_replace('"', '\"', $this->extend):($this->extend->toJSObject());
        if(isSet($this->requiereUmlProperties) && !is_null($this->requiereUmlProperties)) $retArray['requiereUmlProperties'] = (!is_object($this->requiereUmlProperties))?str_replace('"', '\"', $this->requiereUmlProperties):($this->requiereUmlProperties);
        if(isSet($this->updated) && !is_null($this->updated)) $retArray['updated'] = (!is_object($this->updated))?str_replace('"', '\"', $this->updated):($this->updated);
    
        if(isSet($this->modul) && !is_null($this->modul) && $this->modul !== false){
            if(!is_object($this->modul)) $val = $this->modul;
            else{
                $val = $this->modul->toJSObject();
            }
            $retArray['modul'] = $val;
            //$retArray['modul'] = ($simple)?$this->modul->getId():$this->modul->toJSObject($simple, $treeVisit);
    
         }
        return ($returnArray)?$retArray:'[VALENCODE]new CodeTemplate('.json_encode($retArray).')[VALENCODE]';
    }
    
    public function toArray($simple = false, $treeVisit = false){
        if($this->visitedByToArray){
            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return array(
    //                        'class' => 'CodeTemplate',
    //                        'id' => $this->getId()
    //                    );
                    return '[VALENCODE]jsModel["CodeTemplate"]["'.$this->getId().'"][VALENCODE]';
                }
            }
            return false;
        }
        if($treeVisit) $this->visitedByToArray = true;
        $retArray = array();
         $retArray = parent::toArray($simple, $treeVisit);
        if(isSet($this->position) && !is_null($this->position)) $retArray['position'] = (is_numeric($this->position))?$this->position:(($simple)?$this->position->getId():$this->position->toArray($simple, $treeVisit));
        if(isSet($this->template) && !is_null($this->template)) $retArray['template'] = (is_numeric($this->template))?$this->template:($this->template);
        if(isSet($this->language) && !is_null($this->language)) $retArray['language'] = (is_numeric($this->language))?$this->language:(($simple)?$this->language->getId():$this->language->toArray($simple, $treeVisit));
        if(isSet($this->stereoType) && !is_null($this->stereoType)) $retArray['stereoType'] = (is_numeric($this->stereoType))?$this->stereoType:($this->stereoType);
        if(isSet($this->extend) && !is_null($this->extend)) $retArray['extend'] = (is_numeric($this->extend))?$this->extend:(($simple)?$this->extend->getId():$this->extend->toArray($simple, $treeVisit));
        if(isSet($this->requiereUmlProperties) && !is_null($this->requiereUmlProperties)) $retArray['requiereUmlProperties'] = (is_numeric($this->requiereUmlProperties))?$this->requiereUmlProperties:($this->requiereUmlProperties);
        if(isSet($this->updated) && !is_null($this->updated)) $retArray['updated'] = (is_numeric($this->updated))?$this->updated:($this->updated);
        if(!$simple && isSet($this->modul) && !is_null($this->modul) && $this->modul !== false){
            if(is_numeric($this->modul)) $val = $this->modul;
            else{
                $val = ($simple)?$this->modul->getId():$this->modul->toArray($simple, $treeVisit);
            }
            $retArray['modul'] = $val;
            //$retArray['modul'] = ($simple)?$this->modul->getId():$this->modul->toArray($simple, $treeVisit);
    
         }
        return $retArray;
    }private $tplVars = array();
    public function addTemplateVar($name, $content){
        $this->tplVars[$name] = $content;
    }
    
    private $ignoreErrors = false;
    /**
     * get Html Template
     * @param string $template template name
     */
    public function getHtml($template = "default"){
        $availableTemplates =  array(
            'default'
            ,'default'
        );
        if(!in_array($template, $availableTemplates)){
            return parent::getHtml($template);
        }
        $m = new TemplateEngine;
        
        $templates = array(
            'default' => '' //805
        );
        $tplSrc = $templates[$template];
        
        foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, $this);
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    }
    
    public static function getSubHtml($operationName, $template = 'default'){
    
        
        $availableTemplates =  array(
            'loadTemplateFromFile' => array(
                'default'
                ,'default'
            ),
            'build' => array(
                'default'
                ,'default'
            ),
            'loadExtend' => array(
                'default'
                ,'default'
            )
        );
        if(!isset($availableTemplates[$operationName]) || !in_array($template, $availableTemplates[$operationName])){
            return parent::getHtml($template);
        }
        $m = new TemplateEngine;
        $tplSrc = file_get_contents('FCVAPs/fcv/fcvPackage/CodeTemplate/'.$operationName.'/'.$operationName.'.'.$template.'.html');
        /*foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }*/
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, array());
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    
    }
}
