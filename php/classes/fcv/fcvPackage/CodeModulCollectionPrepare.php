<?php
namespace fcv\fcvPackage;

use \QueryBuilder;

use \fcv\fcvPackage\ModulPositionCollectionPrepare;
use \fcv\fcvPackage\BuildCollectionPrepare;
use \fcv\fcvPackage\CodeModulCollectionPrepare;
use \fcv\fcvPackage\CodeTemplateCollectionPrepare;
use \fcv\fcvPackage\WorkPackageCollectionPrepare;
use \fcv\fcvPackage\ProjectCollectionPrepare;class CodeModulCollectionPrepare {

    protected $tableName;
    protected $tableNameAlias = '';

    /**
     * @var QueryBuilder
     */
    protected $query;
    protected $objectMap = array();

    protected $primaryKeyIndex = 0;

    public function __construct(){
        $this->tableName = 'CodeModul';
        $this->query = \QueryBuilder::create();
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_NUM);
    }

    /**
     * @param CodeModul[] $data
     * @return CodeModulCollectionPrepare inserted Id
     */
    public static function create(){
        return new CodeModulCollectionPrepare();
    }

    /**
     * @return int
     */
    public function getPrimaryKeyIndex(){
        return $this->primaryKeyIndex;
    }

    /**
     * @return array
     */
    public function getObjectMap(){
        return $this->objectMap;
    }

    /**
     * @return QueryBuilder
     */
    public function getQuery(){
        return $this->query;
    }

    /**
     * @return string
     */
    public function getTableName(){
        return $this->tableName;
    }

    /**
     * @return CodeModulCollectionPrepare
     */
    public function setTableName($tableName){
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableNameAlias(){
        return ($this->tableNameAlias != '')?$this->tableNameAlias:$this->tableName;
    }

    /**
     * @return CodeModulCollectionPrepare
     */
    public function setTableNameAlias($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias;
        return $this;
    }

    /**
     * @return string
     */
    public function setTableNameAliasPrepend($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias.(($this->tableNameAlias == '')?$this->tableName:$this->tableNameAlias);
        return $this;
    }

    /**
      * Add a basic where clause to the query.
      *
      * @param  string  $column
      * @param  string  $operator
      * @param  mixed   $value
      * @param  string  $boolean
      * @return CodeModulCollectionPrepare
      *
      * @throws \InvalidArgumentException
      */
    public function where($column, $operator = null, $value = null, $boolean = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->where($spl[1], $operator, $value, $boolean);
        }else{
            $this->query->where($column, $operator, $value, $boolean);
        }
        return $this;
    }
    
    public function orWhere($column, $operator = null, $value = null, $logicConnector = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->orWhere($spl[1], $operator, $value, $logicConnector);
        }else{
            $this->query->orWhere($column, $operator, $value, $logicConnector);
        }
        return $this;
    }

    /**
     * @return CodeModulCollectionPrepare
     */
    public function whereRaw($where){
        $this->query->whereRaw($where);
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeModulCollectionPrepare
     */
    public function whereModulId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('modulId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeModulCollectionPrepare
     */
    public function orWhereModulId($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('modulId', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeModulCollectionPrepare
     */
    public function selectModulId(){
        $this->primaryKeyIndex = count($this->query->columns);
        $this->objectMap[] = array(
            'class' => 'CodeModul',
            'field' => 'modulId',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('modulId');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeModulCollectionPrepare
     */
    public function whereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeModulCollectionPrepare
     */
    public function orWhereName($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('name', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeModulCollectionPrepare
     */
    public function selectName(){
        $this->objectMap[] = array(
            'class' => 'CodeModul',
            'field' => 'name',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('name');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeModulCollectionPrepare
     */
    public function wherePath($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('path', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeModulCollectionPrepare
     */
    public function orWherePath($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('path', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeModulCollectionPrepare
     */
    public function selectPath(){
        $this->objectMap[] = array(
            'class' => 'CodeModul',
            'field' => 'path',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('path');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeModulCollectionPrepare
     */
    public function whereType($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('type', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeModulCollectionPrepare
     */
    public function orWhereType($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('type', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeModulCollectionPrepare
     */
    public function selectType(){
        $this->objectMap[] = array(
            'class' => 'CodeModul',
            'field' => 'type',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('type');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeModulCollectionPrepare
     */
    public function whereApplyTo($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('applyTo', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeModulCollectionPrepare
     */
    public function orWhereApplyTo($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('applyTo', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeModulCollectionPrepare
     */
    public function selectApplyTo(){
        $this->objectMap[] = array(
            'class' => 'CodeModul',
            'field' => 'applyTo',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('applyTo');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeModulCollectionPrepare
     */
    public function whereWorkPackage($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('workPackage', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeModulCollectionPrepare
     */
    public function orWhereWorkPackage($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('workPackage', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeModulCollectionPrepare
     */
    public function selectWorkPackage(){
        $this->objectMap[] = array(
            'class' => 'CodeModul',
            'field' => 'workPackage',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('workPackage');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeModulCollectionPrepare
     */
    public function whereBuildOnlyOnFirstBuild($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('buildOnlyOnFirstBuild', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeModulCollectionPrepare
     */
    public function orWhereBuildOnlyOnFirstBuild($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('buildOnlyOnFirstBuild', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeModulCollectionPrepare
     */
    public function selectBuildOnlyOnFirstBuild(){
        $this->objectMap[] = array(
            'class' => 'CodeModul',
            'field' => 'buildOnlyOnFirstBuild',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('buildOnlyOnFirstBuild');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeModulCollectionPrepare
     */
    public function whereProject($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('project', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeModulCollectionPrepare
     */
    public function orWhereProject($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('project', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeModulCollectionPrepare
     */
    public function selectProject(){
        $this->objectMap[] = array(
            'class' => 'CodeModul',
            'field' => 'project',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('project');
        return $this;
    }

    /**
     * @var ModulPositionCollectionPrepare
     */
    protected $positions;
    /**
     * @var ModulPositionCollectionPrepare
     * @return CodeModulCollectionPrepare
     */
    public function setPositions(ModulPositionCollectionPrepare $positions){
       $this->positions = $positions;
       $positions->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getPositions(){
       if(!isset($this->positions)) $this->positions = ModulPositionCollectionPrepare::create();
       return $this->positions;
    }
    
    




    /**
     * @var BuildCollectionPrepare
     */
    protected $usedInBuilds;
    /**
     * @var BuildCollectionPrepare
     * @return CodeModulCollectionPrepare
     */
    public function setUsedInBuilds(BuildCollectionPrepare $usedInBuilds){
       $this->usedInBuilds = $usedInBuilds;
       $usedInBuilds->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getUsedInBuilds(){
       if(!isset($this->usedInBuilds)) $this->usedInBuilds = BuildCollectionPrepare::create();
       return $this->usedInBuilds;
    }
    
    




    /**
     * @var CodeModulCollectionPrepare
     */
    protected $childs;
    /**
     * @var CodeModulCollectionPrepare
     * @return CodeModulCollectionPrepare
     */
    public function setChilds(CodeModulCollectionPrepare $childs){
       $this->childs = $childs;
       $childs->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getChilds(){
       if(!isset($this->childs)) $this->childs = CodeModulCollectionPrepare::create();
       return $this->childs;
    }
    
    




    /**
     * @var CodeModulCollectionPrepare
     */
    protected $parent;
    /**
     * @var CodeModulCollectionPrepare
     * @return CodeModulCollectionPrepare
     */
    public function setParent(CodeModulCollectionPrepare $parent){
       $this->parent = $parent;
       $parent->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getParent(){
       if(!isset($this->parent)) $this->parent = CodeModulCollectionPrepare::create();
       return $this->parent;
    }
    
    
    public function selectParent(){
        $this->objectMap[] = array(
            'class' => 'parent',
            'field' => 'parent',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('parent');
        return $this;
    }




    /**
     * @var CodeTemplateCollectionPrepare
     */
    protected $codeTemplates;
    /**
     * @var CodeTemplateCollectionPrepare
     * @return CodeModulCollectionPrepare
     */
    public function setCodeTemplates(CodeTemplateCollectionPrepare $codeTemplates){
       $this->codeTemplates = $codeTemplates;
       $codeTemplates->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getCodeTemplates(){
       if(!isset($this->codeTemplates)) $this->codeTemplates = CodeTemplateCollectionPrepare::create();
       return $this->codeTemplates;
    }
    
    





    /**
     * @var WorkPackageCollectionPrepare
     */
    protected $workPackage;
    /**
     *
     * @param WorkPackageCollectionPrepare $workPackage
     * @return workPackageCollectionPrepare
     */
    public function setWorkPackage(WorkPackageCollectionPrepare $workPackage){
       $this->workPackage = $workPackage;
       $workPackage->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    /**
     * @var ProjectCollectionPrepare
     */
    protected $project;
    /**
     *
     * @param ProjectCollectionPrepare $project
     * @return projectCollectionPrepare
     */
    public function setProject(ProjectCollectionPrepare $project){
       $this->project = $project;
       $project->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    private $selectAll = false;
    public function selectAll(){
        
        $this->selectModulId();
        $this->selectName();
        $this->selectPath();
        $this->selectType();
        $this->selectApplyTo();
        $this->selectBuildOnlyOnFirstBuild();
        $this->selectParent();

        return $this;
    }
    public function prepareQuery(){
        $this->prepareJoins();
        $this->query->from = 'CodeModul'.(($this->tableNameAlias == '')?'':' AS '.$this->tableNameAlias);
        QueryBuilder::appendWhereWithTable($this->query, $this->getTableNameAlias());
        return $this;
    }
    /**
     * @return CodeModulCollection
     */
    public function get(){
        $this->prepareQuery();
        $rows = $this->query->get();
        return $this->parseDbRows($rows, $this->getObjectMap());
    }

    /**
     * parse all return rows from sql
     */
    private function parseDbRows($rows, $map){
        $objsArray = array();
        $objs = CodeModulCollection::create();
        //build assoc Array with all data
        if(!$this->selectAll){
            foreach($rows as $row){
                $primaryField = $row[$this->primaryKeyIndex];

                //extend object if allready exists

                $initObj = (isset($objsArray[$primaryField]))?$objsArray[$primaryField]:array();
                $objsArray[$primaryField] = $this->parseDbRow($row, $map, $initObj);
            }
        }else $objsArray = $rows;
        //convert array to real objects (subobjects are generated in constructors)
        foreach($objsArray as $dataArray){
            $objs->add(new CodeModul($dataArray));
        }
        return $objs;
    }

    /**
     * parse one row into Object and extend existing ones
     */
    private function parseDbRow($row, $map, $objsData = array(), &$index = 0){
        foreach($map as $m){
            //check if association
            if(isset($m['map'])){
                //check if cardinality is multi
                if($m['multi']){
                    if(!isset($objsData[$m['field']])) $objsData[$m['field']] = array();
                    //get primaryKey ob subobject
                    $id = $row[$index+$m['primaryKeyIndex']];
                    //extend object if allready exists
                    $initObj = (isset($objsData[$m['field']][$id]))?$objsData[$m['field']][$id]:array();
                    //add/extend object with id
                    $objsData[$m['field']][$id] = $this->parseDbRow($row, $m['map'], $initObj, $index);
                }else{
                    $objsData[$m['field']] = $this->parseDbRow($row, $m['map'], array(), $index);
                }
            }
            else{
                $objsData[$m['field']] = $row[$index];
                $index++;
            }
        }
        return $objsData;
    }

    /**
     * create joins depending on associations
     * @return CollectionPrepare
     */
    public function prepareJoins(){
        
        if(!$this->selectAll){
            if(!is_array($this->query->columns) || !in_array('modulId', $this->query->columns)){
                $this->selectmodulId();
            }elseif(!is_array($this->query->columns)){
                $this->selectAll();
            }
        }else{
            $this->query->select('*');
        }
        if(isSet($this->positions)){
            $tableName = $this->positions->getTableNameAlias();
            //Multi
            $this->query->join('ModulPosition as '.$tableName, $tableName.'.modul', '=', $this->getTableNameAlias().'.modulId', 'LEFT');
            $this->positions->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->positions->getQuery(),$tableName);
            $this->query->mergeQuery($this->positions->getQuery());

            $this->objectMap[] = array(
                'class' => 'ModulPosition',
                'field' => 'positions',
                'multi' => true,
                'primaryKeyIndex' => $this->positions->getPrimaryKeyIndex(),
                'map' => $this->positions->getObjectMap()
            );
        }


        if(isSet($this->usedInBuilds)){
            $tableName = $this->usedInBuilds->getTableNameAlias();
            //Multi
            $this->query->join('Build as '.$tableName, $tableName.'.usedModules', '=', $this->getTableNameAlias().'.modulId', 'LEFT');
            $this->usedInBuilds->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->usedInBuilds->getQuery(),$tableName);
            $this->query->mergeQuery($this->usedInBuilds->getQuery());

            $this->objectMap[] = array(
                'class' => 'Build',
                'field' => 'usedInBuilds',
                'multi' => true,
                'primaryKeyIndex' => $this->usedInBuilds->getPrimaryKeyIndex(),
                'map' => $this->usedInBuilds->getObjectMap()
            );
        }


        if(isSet($this->childs)){
            $tableName = $this->childs->getTableNameAlias();
            //Multi
            $this->query->join('CodeModul as '.$tableName, $tableName.'.parent', '=', $this->getTableNameAlias().'.modulId', 'LEFT');
            $this->childs->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->childs->getQuery(),$tableName);
            $this->query->mergeQuery($this->childs->getQuery());

            $this->objectMap[] = array(
                'class' => 'CodeModul',
                'field' => 'childs',
                'multi' => true,
                'primaryKeyIndex' => $this->childs->getPrimaryKeyIndex(),
                'map' => $this->childs->getObjectMap()
            );
        }


        if(isSet($this->parent)){
            $tableName = $this->parent->getTableNameAlias();
            //Single CodeModul
             $this->query->join('CodeModul as '.$tableName, $tableName.'.modulId', '=', $this->getTableNameAlias().'.parent', 'LEFT');
            $this->parent->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->parent->getQuery(),$tableName);
            $this->query->mergeQuery($this->parent->getQuery());

            $this->objectMap[] = array(
                'class' => 'CodeModul',
                'field' => 'parent',
                'multi' => false,
                'primaryKeyIndex' => $this->parent->getPrimaryKeyIndex(),
                'map' => $this->parent->getObjectMap()
            );
        }


        if(isSet($this->codeTemplates)){
            $tableName = $this->codeTemplates->getTableNameAlias();
            //Multi
            $this->query->join('CodeTemplate as '.$tableName, $tableName.'.modul', '=', $this->getTableNameAlias().'.modulId', 'LEFT');
            $this->codeTemplates->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->codeTemplates->getQuery(),$tableName);
            $this->query->mergeQuery($this->codeTemplates->getQuery());

            $this->objectMap[] = array(
                'class' => 'CodeTemplate',
                'field' => 'codeTemplates',
                'multi' => true,
                'primaryKeyIndex' => $this->codeTemplates->getPrimaryKeyIndex(),
                'map' => $this->codeTemplates->getObjectMap()
            );
        }



        if(isSet($this->workPackage)){
            $tableName = $this->workPackage->getTableNameAlias();
            //Single CodeModul
            $this->query->join('WorkPackage AS '.$tableName, $tableName.'.umlId', '=', $this->getTableNameAlias().'.workPackage', 'LEFT');

            $this->workPackage->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->workPackage->getQuery(),$tableName);
            $this->query->mergeQuery($this->workPackage->getQuery());

            $this->objectMap[] = array(
                'class' => 'WorkPackage',
                'field' => 'workPackage',
                'multi' => false,
                'primaryKeyIndex' => $this->workPackage->getPrimaryKeyIndex(),
                'map' => $this->workPackage->getObjectMap()
            );
        }
        if(isSet($this->project)){
            $tableName = $this->project->getTableNameAlias();
            //Single CodeModul
            $this->query->join('Project AS '.$tableName, $tableName.'.projektId', '=', $this->getTableNameAlias().'.project', 'LEFT');

            $this->project->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->project->getQuery(),$tableName);
            $this->query->mergeQuery($this->project->getQuery());

            $this->objectMap[] = array(
                'class' => 'Project',
                'field' => 'project',
                'multi' => false,
                'primaryKeyIndex' => $this->project->getPrimaryKeyIndex(),
                'map' => $this->project->getObjectMap()
            );
        }
        return $this;
    }
}
