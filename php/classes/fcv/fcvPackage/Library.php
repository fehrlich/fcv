<?php
namespace fcv\fcvPackage; ///fcv/fcvPackage/Library
    
use Exception;
use mys;
use QueryBuilder;
use fcv\libs\TemplateEngine;
use fcv\libs\Debug;
use fcv\FCVJsObj;


use fcv\fcvPackage\CodeLanguage;
;
use fcv\fcvPackage\Project;
use fcv\fcvPackage\ProjectCollection;

class Library
        
         
        {
    
    //Attributes
    //const LIBID = 'libId';
    //const NAME = 'name';
    //const VERSION = 'version';
    //const VERIFICATIONLINK = 'verificationLink';
    //const PATH = 'path';
    //const INCLUDECODE = 'includeCode';
    
    //Litearls
    
    //Attributes
    /**
     *
     * @var Integer
     */
     public $libId;
    const FIELD_LIBID = 'libId';
    /**
     *
     * @var String
     */
     public $name;
    const FIELD_NAME = 'name';
    /**
     *
     * @var String
     */
     public $version;
    const FIELD_VERSION = 'version';
    /**
     *
     * @var String
     */
     public $verificationLink;
    const FIELD_VERIFICATIONLINK = 'verificationLink';
    /**
     *
     * @var String
     */
     public $path;
    const FIELD_PATH = 'path';
    /**
     *
     * @var String
     */
     public $includeCode;
    const FIELD_INCLUDECODE = 'includeCode';
    
    //AssociationsEnds
    
    /**
     * @var CodeLanguage
     */
    private $languages = false;
    const LANGUAGES = 'languages';
    
    /**
     * @var LibraryIncludeCode
     */
    public $libraryIncludeCode = false;
    const FIELD_LIBRARYINCLUDECODE = 'libraryIncludeCode';
    //{/isAssociationClass}}
    
    /**
     * @var ProjectCollection
     */
    private $projects = false;
    const PROJECTS = 'projects';
    
    //{/isAssociationClass}}
    //FCV Attributes
    private $apPath = 'FCVAPs/';
    //    public $VIEW = array();
    static $fcvFields = array('libId','name','version','verificationLink','path','includeCode');
    //Todo: Outsource
    protected $loadObject = false;
    private $loadedAttributes = array();
    
    //Methods
    //UserOperations
    private $lastModifiedTime = false;
    private $createdTime = false;
    public function __construct($data = array(),$calledFromChild = false){
        if(is_string($data)) $data = json_decode($data,true);
        if(count($data) > 0){
            if(isSet($data['loadObj']) && $data['loadObj']){
                $this->loadObject = true;
            }
            
        //        if(isset($data['libId']) && !empty($data['libId']))
    
            if(isSet($data['libId'])) $this->libId = $data['libId'];
            if(isSet($data['name'])) $this->name = $data['name'];
            if(isSet($data['version'])) $this->version = $data['version'];
            if(isSet($data['verificationLink'])) $this->verificationLink = $data['verificationLink'];
            if(isSet($data['path'])) $this->path = $data['path'];
            if(isSet($data['includeCode'])) $this->includeCode = $data['includeCode'];
            //associationEnds
            //Regular AssociationEnd LibraryIncludeCode (UMLAssociationClass)
            //AssociationEnd is from AssociationClass
            if(isSet($data['languages'])){
                $this->languages = (is_array($data['languages']))?CodeLanguage::create($data['languages'],$this->loadObject):$data['languages'];
            }
    
    
            //value from AssociationClass
            if(isSet($data['libraryIncludeCode'])){
                if(!is_object(current($data['libraryIncludeCode']))){
                    $newVal = new LibraryIncludeCodeCollection();
                    foreach($data['libraryIncludeCode'] as $el) $newVal->add(LibraryIncludeCode::create($el,$this->loadObject));
                    $this->libraryIncludeCode = $newVal;
                }else $this->libraryIncludeCode = $data['libraryIncludeCode'];
            }
            if(isSet($data['languages'])){
                $this->languages = (is_array($data['languages']))?CodeLanguage::create($data['languages'],$this->loadObject):$data['languages'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['projects'])){
                if(!is_object(current($data['projects']))){
                    $newVal = new ProjectCollection();
                    foreach($data['projects'] as $el) $newVal->add(Project::create($el,$this->loadObject));
                    $this->projects = $newVal;
                }else  $this->projects = $data['projects'];
            }*/
    
            if(isSet($data['projects'])){
                if(!is_object(current($data['projects']))){
                    $newVal = new ProjectCollection();
                    foreach($data['projects'] as $el) $newVal->add(Project::create($el,$this->loadObject));
                    $this->projects = $newVal;
                }else $this->projects = $data['projects'];
            }
    
            //Helper
            if(isSet($data['lastModified'])) $this->lastModifiedTime = $data['lastModified'];
            if(isSet($data['created'])) $this->createdTime = $data['created'];
        }
    
    
    
    
    }
    
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new Library($data);
    }
    
    public static function createLoad(){
        $data = array(
            'loadObj' => true
        );
        return new Library($data);
    }
    
    public function getLastModifiedTime(){
        return $this->lastModifiedTime;
    }
    
    public function setLastModifiedTime($time){
        $this->lastModifiedTime = $time;
        return $this;
    }
    
    public function hasPrimaryKey(){
        return true;
    }
    
    public function getId(){
        return $this->libId;
    }
    public function setId($id){
    //        Library::$loadedInstances[$id] = &$this;
        $this->libId = $id;
        if($this->loadObject) $this->loadedAttributes['libId'] = true;
        return $this;
    }
    public function getPrimaryKeyName(){
        return "libId";
    }
    
    public function addToJs($selectionString = ''){
    //    if(!$calledFromChild && get_class($this) == '' && (!$this->loadObject))
            FCVJsObj::add('', $this, $selectionString);
            return $this;
    }
    public function throwError($msg){
        if(!Debug::getObj()->getThrowErrors()) return "";
        else{
            throw new Exception($msg);
        }
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return CodeLanguage
     * @throws Exception
     */
    public function getLanguages($forceLoad = false){
        if(is_null($this->languages) || empty($this->languages)){
            $this->throwError('class Library with id '.$this->getId().' has no Attributes languages');
            //throw new Exception('class Library with id '.$this->getId().' has no Attributes languages');
        }
        return $this->languages;
    }
    
    public function hasLanguages($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getLanguages($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param CodeLanguage $codeLanguage
     */
    public function setLanguages(CodeLanguage $languages){
        if(!$this->loadObject) $languages->setLib($this);
        $this->languages = $languages;
        if($this->loadObject) $this->loadedAttributes['languages'] = true;
        return $this;
    }
    /**
     * @return LibraryIncludeCode
     * @throws Exception
     */
    public function getLibraryIncludeCode($forceLoad = false){
    
        if(empty($this->libraryIncludeCode)){
            $this->throwError('Attribute languages not Found in class languages');
            //throw new Exception('Attribute languages not Found in class languages');
        }
        return $this->libraryIncludeCode;
    }
    
    /**
     * Check if the Association LibraryIncludeCode exists
     */
    public function hasLibraryIncludeCode($forceLoad = false){
        try {
            $this->getLibraryIncludeCode($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    /** a
     * @param LibraryIncludeCode $languages
     * @return Library
     */
    public function setLibraryIncludeCode($languages){
        $this->languages = $languages;
        if($this->loadObject) $this->loadedAttributes['languages'] = true;
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return ProjectCollection
     * @throws Exception
     */
    public function getProjects($forceLoad = false){
        if(is_null($this->projects) || empty($this->projects)){
            $this->throwError('class Library with id '.$this->getId().' has no Attributes projects');
            //throw new Exception('class Library with id '.$this->getId().' has no Attributes projects');
        }
        return $this->projects;
    }
    
    public function hasProjects($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getProjects($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param ProjectCollection $project
     */
    public function setProjects(ProjectCollection $projects){
        $this->projects = $projects;
        if($this->loadObject) $this->loadedAttributes['projects'] = true;
        return $this;
    }
    
    /**
     * @param Library $project
     */
    public function addProject(Project $project){
        if(!$project->hasPrimaryKey()){
            if(!$this->loadObject) $project->setParent($this, count($this->projects));
        }
        if($this->loadObject){
            $this->loadedAttributes['projects'] = true;
            if(!$this->projects) $this->projects = new ProjectCollection();
        }
        elseif(!$this->projects){
            if(!$this->hasProjects()) $this->projects = new ProjectCollection();
        }
        $this->projects->add($project);
        return $this;
    }
    
    //Getter & Setter
    
    public function get($name){
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            return $this->$name->get($subName);
        }else return $this->$name;
    }
    
    public function has($name){
        if(strpos('.', $name) !== false){
            $spl = explode('.', $name, 2);
            return $this->get($spl[0])->has($spl[1]);
        }
        return (property_exists($this, $name));
    }
    
    public function set($name, $value){
        $possibleFields = array(
        'libId',
        'name',
        'version',
        'verificationLink',
        'path',
        'includeCode',
        'languages',
        'libraryIncludeCode',
        'projects',
        );
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class Library');
            if(!$this->$name){
                if(false);
                elseif($name == 'languages'){
                    $this->languages = CodeLanguage::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['languages'] = true;
                }
                elseif($name == 'libraryIncludeCode'){
                    $this->libraryIncludeCode = LibraryIncludeCode::create(array(),$this->loadObject)->set($subName,$value);
                    if($this->loadObject) $this->loadedAttributes['libraryIncludeCode'] = true;
                }
                elseif($name == 'projects'){
                    $this->projects = Project::create(array(),$this->loadObject)->set($subName,$value);
                    $this->projects = array($this->projects->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['projects'] = true;
                }
                else die('Error');
            }else $this->$name->set($subName,$value);
        }else{
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class Library');
            $this->$name = $value;
            if($this->loadObject) $this->loadedAttributes[$name] = true;
        }
        return $this;
    }
    
    /**
     *
     * @return Integer
     * @throws Exception
     */
     public function getLibId(){
        return $this->libId;
     }
    
     public function hasLibId(){
        try {
            $this->getLibId();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Integer $libId
     * @return Library
     */
     public function setLibId($libId){
        ;
        $this->libId = $libId;
        if($this->loadObject) $this->loadedAttributes['libId'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getName(){
        return $this->name;
     }
    
     public function hasName(){
        try {
            $this->getName();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $name
     * @return Library
     */
     public function setName($name){
        ;
        $this->name = $name;
        if($this->loadObject) $this->loadedAttributes['name'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getVersion(){
        return $this->version;
     }
    
     public function hasVersion(){
        try {
            $this->getVersion();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $version
     * @return Library
     */
     public function setVersion($version){
        ;
        $this->version = $version;
        if($this->loadObject) $this->loadedAttributes['version'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getVerificationLink(){
        return $this->verificationLink;
     }
    
     public function hasVerificationLink(){
        try {
            $this->getVerificationLink();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $verificationLink
     * @return Library
     */
     public function setVerificationLink($verificationLink){
        ;
        $this->verificationLink = $verificationLink;
        if($this->loadObject) $this->loadedAttributes['verificationLink'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getPath(){
        return $this->path;
     }
    
     public function hasPath(){
        try {
            $this->getPath();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $path
     * @return Library
     */
     public function setPath($path){
        ;
        $this->path = $path;
        if($this->loadObject) $this->loadedAttributes['path'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getIncludeCode(){
        return $this->includeCode;
     }
    
     public function hasIncludeCode(){
        try {
            $this->getIncludeCode();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $includeCode
     * @return Library
     */
     public function setIncludeCode($includeCode){
        ;
        $this->includeCode = $includeCode;
        if($this->loadObject) $this->loadedAttributes['includeCode'] = true;
        return $this;
     }
    protected $visitedByToArray = false;
    public function toJSObject($returnArray = false){
        if($this->loadObject) return false;
        if($this->visitedByToArray){
    //            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return '[VALENCODE]jsModel["Library.'.$this->getId().'"][VALENCODE]';
                    return array(
                        'class' => 'Library',
                        'id' => $this->getId()
                    );
                }
                return false;
    //            }
    //            return array(
    //                'class' => 'Library',
    //                'id' => rand()
    //            );
        }
        $retArray = array();
        
        $this->visitedByToArray = true;
        
        if(isSet($this->libId) && !is_null($this->libId)) $retArray['libId'] = (!is_object($this->libId))?str_replace('"', '\"', $this->libId):($this->libId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (!is_object($this->name))?str_replace('"', '\"', $this->name):($this->name);
        if(isSet($this->version) && !is_null($this->version)) $retArray['version'] = (!is_object($this->version))?str_replace('"', '\"', $this->version):($this->version);
        if(isSet($this->verificationLink) && !is_null($this->verificationLink)) $retArray['verificationLink'] = (!is_object($this->verificationLink))?str_replace('"', '\"', $this->verificationLink):($this->verificationLink);
        if(isSet($this->path) && !is_null($this->path)) $retArray['path'] = (!is_object($this->path))?str_replace('"', '\"', $this->path):($this->path);
        if(isSet($this->includeCode) && !is_null($this->includeCode)) $retArray['includeCode'] = (!is_object($this->includeCode))?str_replace('"', '\"', $this->includeCode):($this->includeCode);
    
        if(isSet($this->languages) && !is_null($this->languages) && $this->languages !== false){
            if(!is_object($this->languages)) $val = $this->languages;
            else{
                $val = $this->languages->toJSObject();
            }
            $retArray['languages'] = $val;
            //$retArray['languages'] = ($simple)?$this->languages->getId():$this->languages->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->projects) && !is_null($this->projects) && $this->projects !== false){
            if(!is_object($this->projects)) $val = $this->projects;
            else{
                $val = array();
                foreach($this->projects as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['projects'] = $val;
            //$retArray['projects'] = ($simple)?$this->projects->getId():$this->projects->toJSObject($simple, $treeVisit);
    
         }
        return ($returnArray)?$retArray:'[VALENCODE]new Library('.json_encode($retArray).')[VALENCODE]';
    }
    
    public function toArray($simple = false, $treeVisit = false){
        if($this->visitedByToArray){
            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return array(
    //                        'class' => 'Library',
    //                        'id' => $this->getId()
    //                    );
                    return '[VALENCODE]jsModel["Library"]["'.$this->getId().'"][VALENCODE]';
                }
            }
            return false;
        }
        if($treeVisit) $this->visitedByToArray = true;
        $retArray = array();
        
        if(isSet($this->libId) && !is_null($this->libId)) $retArray['libId'] = (is_numeric($this->libId))?$this->libId:($this->libId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (is_numeric($this->name))?$this->name:($this->name);
        if(isSet($this->version) && !is_null($this->version)) $retArray['version'] = (is_numeric($this->version))?$this->version:($this->version);
        if(isSet($this->verificationLink) && !is_null($this->verificationLink)) $retArray['verificationLink'] = (is_numeric($this->verificationLink))?$this->verificationLink:($this->verificationLink);
        if(isSet($this->path) && !is_null($this->path)) $retArray['path'] = (is_numeric($this->path))?$this->path:($this->path);
        if(isSet($this->includeCode) && !is_null($this->includeCode)) $retArray['includeCode'] = (is_numeric($this->includeCode))?$this->includeCode:($this->includeCode);
        if(!$simple && isSet($this->languages) && !is_null($this->languages) && $this->languages !== false){
            if(is_numeric($this->languages)) $val = $this->languages;
            else{
                $val = ($simple)?$this->languages->getId():$this->languages->toArray($simple, $treeVisit);
            }
            $retArray['languages'] = $val;
            //$retArray['languages'] = ($simple)?$this->languages->getId():$this->languages->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->projects) && !is_null($this->projects) && $this->projects !== false){
            if(is_numeric($this->projects)) $val = $this->projects;
            else{
                $val = array();
                foreach($this->projects as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['projects'] = $val;
            //$retArray['projects'] = ($simple)?$this->projects->getId():$this->projects->toArray($simple, $treeVisit);
    
         }
        return $retArray;
    }private $tplVars = array();
    public function addTemplateVar($name, $content){
        $this->tplVars[$name] = $content;
    }
    
    private $ignoreErrors = false;
    /**
     * get Html Template
     * @param string $template template name
     */
    public function getHtml($template = "default"){
        $availableTemplates =  array(
            'default'
            ,'default'
        );
        if(!in_array($template, $availableTemplates)){
            throw new Exception('Template <b>'.$template.'</b> doesn\'t exist for <b>Library</b>');
        }
        $m = new TemplateEngine;
        
        $templates = array(
            'default' => '' //804
        );
        $tplSrc = $templates[$template];
        
        foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, $this);
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    }
    
    public static function getSubHtml($operationName, $template = 'default'){
    
        
        $availableTemplates =  array(
        );
        if(!isset($availableTemplates[$operationName]) || !in_array($template, $availableTemplates[$operationName])){
            throw new Exception('Template OperationTemplate <b>'.$template.'</b> doesn\'t exist for <b>Library.'.$operationName.'</b>');
        }
        $m = new TemplateEngine;
        $tplSrc = file_get_contents('FCVAPs/fcv/fcvPackage/Library/'.$operationName.'/'.$operationName.'.'.$template.'.html');
        /*foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }*/
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, array());
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    
    }
}
