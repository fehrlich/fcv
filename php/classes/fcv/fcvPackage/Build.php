<?php
namespace fcv\fcvPackage; ///fcv/fcvPackage/Build
    
use Exception;
use mys;
use QueryBuilder;
use fcv\libs\TemplateEngine;
use fcv\libs\Debug;
use fcv\FCVJsObj;


use fcv\fcvPackage\Project;
;
use fcv\fcvPackage\CodeModul;
use fcv\fcvPackage\CodeModulCollection;

class Build
        
         
        {
    
    //Attributes
    //const BUILDID = 'buildId';
    //const NAME = 'name';
    //const FIRSTBUILD = 'firstBuild';
    //const BUILDPATH = 'buildPath';
    //const BUILDWORKPACKAGES = 'buildWorkpackages';
    //const DISABLED = 'disabled';
    
    //Litearls
    
    //Attributes
    /**
     *
     * @var Integer
     */
     public $buildId;
    const FIELD_BUILDID = 'buildId';
    /**
     *
     * @var String
     */
     public $name;
    const FIELD_NAME = 'name';
    /**
     *
     * @var Boolean
     */
     public $firstBuild = true;
    const FIELD_FIRSTBUILD = 'firstBuild';
    /**
     *
     * @var String
     */
     public $buildPath;
    const FIELD_BUILDPATH = 'buildPath';
    /**
     *
     * @var String
     */
    public static $defaultContentPath;
    /**
     *
     * @var Boolean
     */
     public $buildWorkpackages;
    const FIELD_BUILDWORKPACKAGES = 'buildWorkpackages';
    /**
     *
     * @var Boolean
     */
     public $disabled;
    const FIELD_DISABLED = 'disabled';
    
    //AssociationsEnds
    
    /**
     * @var Project
     */
    private $project = false;
    const PROJECT = 'project';
    
    //{/isAssociationClass}}
    
    /**
     * @var CodeModulCollection
     */
    private $usedModules = false;
    const USEDMODULES = 'usedModules';
    
    //{/isAssociationClass}}
    //FCV Attributes
    private $apPath = 'FCVAPs/';
    //    public $VIEW = array();
    static $fcvFields = array('buildId','name','firstBuild','buildPath','buildWorkpackages','disabled');
    //Todo: Outsource
    protected $loadObject = false;
    private $loadedAttributes = array();
    
    //Methods
    //UserOperations
    /**
      *
      * @param  $Build
      * @return 
      */
    public  static function addBuild( $Build ){
        
    
    }
    private $lastModifiedTime = false;
    private $createdTime = false;
    public function __construct($data = array(),$calledFromChild = false){
        if(is_string($data)) $data = json_decode($data,true);
        if(count($data) > 0){
            if(isSet($data['loadObj']) && $data['loadObj']){
                $this->loadObject = true;
            }
            
        //        if(isset($data['buildId']) && !empty($data['buildId']))
    
            if(isSet($data['buildId'])) $this->buildId = $data['buildId'];
            if(isSet($data['name'])) $this->name = $data['name'];
            if(isSet($data['firstBuild'])) $this->firstBuild = $data['firstBuild'];
            if(isSet($data['buildPath'])) $this->buildPath = $data['buildPath'];
            if(isSet($data['buildWorkpackages'])) $this->buildWorkpackages = $data['buildWorkpackages'];
            if(isSet($data['disabled'])) $this->disabled = $data['disabled'];
            //associationEnds
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['project'])){
                $this->project = (is_array($data['project']))?project::create($data['project'],$this->loadObject):$data['project'];
            }*/
    
            if(isSet($data['project'])){
                $this->project = (is_array($data['project']))?Project::create($data['project'],$this->loadObject):$data['project'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['usedModules'])){
                if(!is_object(current($data['usedModules']))){
                    $newVal = new CodeModulCollection();
                    foreach($data['usedModules'] as $el) $newVal->add(CodeModul::create($el,$this->loadObject));
                    $this->usedModules = $newVal;
                }else  $this->usedModules = $data['usedModules'];
            }*/
    
            if(isSet($data['usedModules'])){
                if(!is_object(current($data['usedModules']))){
                    $newVal = new CodeModulCollection();
                    foreach($data['usedModules'] as $el) $newVal->add(CodeModul::create($el,$this->loadObject));
                    $this->usedModules = $newVal;
                }else $this->usedModules = $data['usedModules'];
            }
    
            //Helper
            if(isSet($data['lastModified'])) $this->lastModifiedTime = $data['lastModified'];
            if(isSet($data['created'])) $this->createdTime = $data['created'];
        }
    
    
    
    
    }
    
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new Build($data);
    }
    
    public static function createLoad(){
        $data = array(
            'loadObj' => true
        );
        return new Build($data);
    }
    
    public function getLastModifiedTime(){
        return $this->lastModifiedTime;
    }
    
    public function setLastModifiedTime($time){
        $this->lastModifiedTime = $time;
        return $this;
    }
    
    public function hasPrimaryKey(){
        return true;
    }
    
    public function getId(){
        return $this->buildId;
    }
    public function setId($id){
    //        Build::$loadedInstances[$id] = &$this;
        $this->buildId = $id;
        if($this->loadObject) $this->loadedAttributes['buildId'] = true;
        return $this;
    }
    public function getPrimaryKeyName(){
        return "buildId";
    }
    
    public function addToJs($selectionString = ''){
    //    if(!$calledFromChild && get_class($this) == '' && (!$this->loadObject))
            FCVJsObj::add('', $this, $selectionString);
            return $this;
    }
    public function throwError($msg){
        if(!Debug::getObj()->getThrowErrors()) return "";
        else{
            throw new Exception($msg);
        }
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return Project
     * @throws Exception
     */
    public function getProject($forceLoad = false){
        if(is_null($this->project) || empty($this->project)){
            $this->throwError('class Build with id '.$this->getId().' has no Attributes project');
            //throw new Exception('class Build with id '.$this->getId().' has no Attributes project');
        }
        return $this->project;
    }
    
    public function hasProject($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getProject($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param Project $project
     */
    public function setProject(Project $project){
        $this->project = $project;
        if($this->loadObject) $this->loadedAttributes['project'] = true;
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return CodeModulCollection
     * @throws Exception
     */
    public function getUsedModules($forceLoad = false){
        if(is_null($this->usedModules) || empty($this->usedModules)){
            $this->throwError('class Build with id '.$this->getId().' has no Attributes usedModules');
            //throw new Exception('class Build with id '.$this->getId().' has no Attributes usedModules');
        }
        return $this->usedModules;
    }
    
    public function hasUsedModules($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getUsedModules($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param CodeModulCollection $codeModul
     */
    public function setUsedModules(CodeModulCollection $usedModules){
        $this->usedModules = $usedModules;
        if($this->loadObject) $this->loadedAttributes['usedModules'] = true;
        return $this;
    }
    
    /**
     * @param Build $codeModul
     */
    public function addCodeModul(CodeModul $codeModul){
        if(!$codeModul->hasPrimaryKey()){
            if(!$this->loadObject) $codeModul->setParent($this, count($this->usedModules));
        }
        if($this->loadObject){
            $this->loadedAttributes['usedModules'] = true;
            if(!$this->usedModules) $this->usedModules = new CodeModulCollection();
        }
        elseif(!$this->usedModules){
            if(!$this->hasUsedModules()) $this->usedModules = new CodeModulCollection();
        }
        $this->usedModules->add($codeModul);
        return $this;
    }
    
    //Getter & Setter
    
    public function get($name){
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            return $this->$name->get($subName);
        }else return $this->$name;
    }
    
    public function has($name){
        if(strpos('.', $name) !== false){
            $spl = explode('.', $name, 2);
            return $this->get($spl[0])->has($spl[1]);
        }
        return (property_exists($this, $name));
    }
    
    public function set($name, $value){
        $possibleFields = array(
        'buildId',
        'name',
        'firstBuild',
        'buildPath',
        'buildWorkpackages',
        'disabled',
        'project',
        'usedModules',
        );
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class Build');
            if(!$this->$name){
                if(false);
                elseif($name == 'project'){
                    $this->project = Project::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['project'] = true;
                }
                elseif($name == 'usedModules'){
                    $this->usedModules = CodeModul::create(array(),$this->loadObject)->set($subName,$value);
                    $this->usedModules = array($this->usedModules->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['usedModules'] = true;
                }
                else die('Error');
            }else $this->$name->set($subName,$value);
        }else{
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class Build');
            $this->$name = $value;
            if($this->loadObject) $this->loadedAttributes[$name] = true;
        }
        return $this;
    }
    
    /**
     *
     * @return Integer
     * @throws Exception
     */
     public function getBuildId(){
        return $this->buildId;
     }
    
     public function hasBuildId(){
        try {
            $this->getBuildId();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Integer $buildId
     * @return Build
     */
     public function setBuildId($buildId){
        ;
        $this->buildId = $buildId;
        if($this->loadObject) $this->loadedAttributes['buildId'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getName(){
        return $this->name;
     }
    
     public function hasName(){
        try {
            $this->getName();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $name
     * @return Build
     */
     public function setName($name){
        ;
        $this->name = $name;
        if($this->loadObject) $this->loadedAttributes['name'] = true;
        return $this;
     }
    /**
     *
     * @return Boolean
     * @throws Exception
     */
     public function getFirstBuild(){
        return $this->firstBuild;
     }
    
     public function hasFirstBuild(){
        try {
            $this->getFirstBuild();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Boolean $firstBuild
     * @return Build
     */
     public function setFirstBuild($firstBuild){
        ;
        $this->firstBuild = $firstBuild;
        if($this->loadObject) $this->loadedAttributes['firstBuild'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getBuildPath(){
        return $this->buildPath;
     }

     public $workpackagePath = null;

     public function setWorkPackagePath($workpackagePath){
        $this->workpackagePath = $workpackagePath;
        return $this;
     }

     public function getWorkPackagePath(){
        if(!$this->workpackagePath){
            return $this->getWorkPackagePath().'/FCVAPs';
        }
        return $this->workpackagePath;
     }
    
     public function hasBuildPath(){
        try {
            $this->getBuildPath();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $buildPath
     * @return Build
     */
     public function setBuildPath($buildPath){
        ;
        $this->buildPath = $buildPath;
        if($this->loadObject) $this->loadedAttributes['buildPath'] = true;
        return $this;
     }
    /**
     *
     * @return Boolean
     * @throws Exception
     */
     public function getBuildWorkpackages(){
        return $this->buildWorkpackages;
     }
    
     public function hasBuildWorkpackages(){
        try {
            $this->getBuildWorkpackages();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Boolean $buildWorkpackages
     * @return Build
     */
     public function setBuildWorkpackages($buildWorkpackages){
        ;
        $this->buildWorkpackages = $buildWorkpackages;
        if($this->loadObject) $this->loadedAttributes['buildWorkpackages'] = true;
        return $this;
     }
    /**
     *
     * @return Boolean
     * @throws Exception
     */
     public function getDisabled(){
        return $this->disabled;
     }
    
     public function hasDisabled(){
        try {
            $this->getDisabled();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Boolean $disabled
     * @return Build
     */
     public function setDisabled($disabled){
        ;
        $this->disabled = $disabled;
        if($this->loadObject) $this->loadedAttributes['disabled'] = true;
        return $this;
     }
    protected $visitedByToArray = false;
    public function toJSObject($returnArray = false){
        if($this->loadObject) return false;
        if($this->visitedByToArray){
    //            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return '[VALENCODE]jsModel["Build.'.$this->getId().'"][VALENCODE]';
                    return array(
                        'class' => 'Build',
                        'id' => $this->getId()
                    );
                }
                return false;
    //            }
    //            return array(
    //                'class' => 'Build',
    //                'id' => rand()
    //            );
        }
        $retArray = array();
        
        $this->visitedByToArray = true;
        
        if(isSet($this->buildId) && !is_null($this->buildId)) $retArray['buildId'] = (!is_object($this->buildId))?str_replace('"', '\"', $this->buildId):($this->buildId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (!is_object($this->name))?str_replace('"', '\"', $this->name):($this->name);
        if(isSet($this->firstBuild) && !is_null($this->firstBuild)) $retArray['firstBuild'] = (!is_object($this->firstBuild))?str_replace('"', '\"', $this->firstBuild):($this->firstBuild);
        if(isSet($this->buildPath) && !is_null($this->buildPath)) $retArray['buildPath'] = (!is_object($this->buildPath))?str_replace('"', '\"', $this->buildPath):($this->buildPath);
        if(isSet($this->buildWorkpackages) && !is_null($this->buildWorkpackages)) $retArray['buildWorkpackages'] = (!is_object($this->buildWorkpackages))?str_replace('"', '\"', $this->buildWorkpackages):($this->buildWorkpackages);
        if(isSet($this->disabled) && !is_null($this->disabled)) $retArray['disabled'] = (!is_object($this->disabled))?str_replace('"', '\"', $this->disabled):($this->disabled);
    
        if(isSet($this->project) && !is_null($this->project) && $this->project !== false){
            if(!is_object($this->project)) $val = $this->project;
            else{
                $val = $this->project->toJSObject();
            }
            $retArray['project'] = $val;
            //$retArray['project'] = ($simple)?$this->project->getId():$this->project->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->usedModules) && !is_null($this->usedModules) && $this->usedModules !== false){
            if(!is_object($this->usedModules)) $val = $this->usedModules;
            else{
                $val = array();
                foreach($this->usedModules as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['usedModules'] = $val;
            //$retArray['usedModules'] = ($simple)?$this->usedModules->getId():$this->usedModules->toJSObject($simple, $treeVisit);
    
         }
        return ($returnArray)?$retArray:'[VALENCODE]new Build('.json_encode($retArray).')[VALENCODE]';
    }
    
    public function toArray($simple = false, $treeVisit = false){
        if($this->visitedByToArray){
            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return array(
    //                        'class' => 'Build',
    //                        'id' => $this->getId()
    //                    );
                    return '[VALENCODE]jsModel["Build"]["'.$this->getId().'"][VALENCODE]';
                }
            }
            return false;
        }
        if($treeVisit) $this->visitedByToArray = true;
        $retArray = array();
        
        if(isSet($this->buildId) && !is_null($this->buildId)) $retArray['buildId'] = (is_numeric($this->buildId))?$this->buildId:($this->buildId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (is_numeric($this->name))?$this->name:($this->name);
        if(isSet($this->firstBuild) && !is_null($this->firstBuild)) $retArray['firstBuild'] = (is_numeric($this->firstBuild))?$this->firstBuild:($this->firstBuild);
        if(isSet($this->buildPath) && !is_null($this->buildPath)) $retArray['buildPath'] = (is_numeric($this->buildPath))?$this->buildPath:($this->buildPath);
        if(isSet($this->buildWorkpackages) && !is_null($this->buildWorkpackages)) $retArray['buildWorkpackages'] = (is_numeric($this->buildWorkpackages))?$this->buildWorkpackages:($this->buildWorkpackages);
        if(isSet($this->disabled) && !is_null($this->disabled)) $retArray['disabled'] = (is_numeric($this->disabled))?$this->disabled:($this->disabled);
        if(!$simple && isSet($this->project) && !is_null($this->project) && $this->project !== false){
            if(is_numeric($this->project)) $val = $this->project;
            else{
                $val = ($simple)?$this->project->getId():$this->project->toArray($simple, $treeVisit);
            }
            $retArray['project'] = $val;
            //$retArray['project'] = ($simple)?$this->project->getId():$this->project->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->usedModules) && !is_null($this->usedModules) && $this->usedModules !== false){
            if(is_numeric($this->usedModules)) $val = $this->usedModules;
            else{
                $val = array();
                foreach($this->usedModules as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['usedModules'] = $val;
            //$retArray['usedModules'] = ($simple)?$this->usedModules->getId():$this->usedModules->toArray($simple, $treeVisit);
    
         }
        return $retArray;
    }private $tplVars = array();
    public function addTemplateVar($name, $content){
        $this->tplVars[$name] = $content;
    }
    
    private $ignoreErrors = false;
    /**
     * get Html Template
     * @param string $template template name
     */
    public function getHtml($template = "default"){
        $availableTemplates =  array(
            'default'
            ,'default'
            ,'form'
        );
        if(!in_array($template, $availableTemplates)){
            throw new Exception('Template <b>'.$template.'</b> doesn\'t exist for <b>Build</b>');
        }
        $m = new TemplateEngine;
        
        $templates = array(
            'default' => '' //797
            ,'form' => '<form action="" method="post">
        <h1>{{getBuildWorkpackages}}</h1>
        <label for="BuildName">Name</label>
        <input type="text" id="BuildName" name="name" value="{{getName}}" />
        
        <label for="BuildName">Path</label>
        <input type="text" id="builPath" name="buildPath" value="{{getBuildPath}}" />
        
        <label for="buildWorkpackages">buildWorkpackages</label>
        <input type="checkbox" id="buildWorkpackages" name="buildWorkpackages"{{#getBuildWorkpackages}} checked="checked"{{/getBuildWorkpackages}} />
    </form>' //832
        );
        $tplSrc = $templates[$template];
        
        foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, $this);
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    }
    
    public static function getSubHtml($operationName, $template = 'default'){
    
        
        $availableTemplates =  array(
            'addBuild' => array(
                'default'
                ,'default'
            )
        );
        if(!isset($availableTemplates[$operationName]) || !in_array($template, $availableTemplates[$operationName])){
            throw new Exception('Template OperationTemplate <b>'.$template.'</b> doesn\'t exist for <b>Build.'.$operationName.'</b>');
        }
        $m = new TemplateEngine;
        $tplSrc = file_get_contents('FCVAPs/fcv/fcvPackage/Build/'.$operationName.'/'.$operationName.'.'.$template.'.html');
        /*foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }*/
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, array());
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    
    }
}
