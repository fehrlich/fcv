<?php
namespace fcv\fcvPackage;

use \QueryBuilder;

use \fcv\fcvPackage\CodeModulCollectionPrepare;
use \fcv\fcvPackage\ModulPositionCollectionPrepare;
use \fcv\fcvPackage\CodeLanguageCollectionPrepare;class CodeTemplateCollectionPrepare  extends CodeModulCollectionPrepare{

    protected $tableName;
    protected $tableNameAlias = '';

    /**
     * @var QueryBuilder
     */
    protected $query;
    protected $objectMap = array();

    protected $primaryKeyIndex = 0;

    public function __construct(){
        $this->tableName = 'CodeTemplate';
        $this->query = \QueryBuilder::create();
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_NUM);
    }

    /**
     * @param CodeTemplate[] $data
     * @return CodeTemplateCollectionPrepare inserted Id
     */
    public static function create(){
        return new CodeTemplateCollectionPrepare();
    }

    /**
     * @return int
     */
    public function getPrimaryKeyIndex(){
        return $this->primaryKeyIndex;
    }

    /**
     * @return array
     */
    public function getObjectMap(){
        return $this->objectMap;
    }

    /**
     * @return QueryBuilder
     */
    public function getQuery(){
        return $this->query;
    }

    /**
     * @return string
     */
    public function getTableName(){
        return $this->tableName;
    }

    /**
     * @return CodeTemplateCollectionPrepare
     */
    public function setTableName($tableName){
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableNameAlias(){
        return ($this->tableNameAlias != '')?$this->tableNameAlias:$this->tableName;
    }

    /**
     * @return CodeTemplateCollectionPrepare
     */
    public function setTableNameAlias($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias;
        return $this;
    }

    /**
     * @return string
     */
    public function setTableNameAliasPrepend($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias.(($this->tableNameAlias == '')?$this->tableName:$this->tableNameAlias);
        return $this;
    }

    /**
      * Add a basic where clause to the query.
      *
      * @param  string  $column
      * @param  string  $operator
      * @param  mixed   $value
      * @param  string  $boolean
      * @return CodeTemplateCollectionPrepare
      *
      * @throws \InvalidArgumentException
      */
    public function where($column, $operator = null, $value = null, $boolean = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->where($spl[1], $operator, $value, $boolean);
        }else{
            $this->query->where($column, $operator, $value, $boolean);
        }
        return $this;
    }
    
    public function orWhere($column, $operator = null, $value = null, $logicConnector = 'and'){
        if(strpos($column, '.') !== false){
            $spl = explode('.', $column, 2);
            $func = 'get'.ucfirst($spl[0]);
            $sub = $this->$func();
            $sub->orWhere($spl[1], $operator, $value, $logicConnector);
        }else{
            $this->query->orWhere($column, $operator, $value, $logicConnector);
        }
        return $this;
    }

    /**
     * @return CodeTemplateCollectionPrepare
     */
    public function whereRaw($where){
        $this->query->whereRaw($where);
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeTemplateCollectionPrepare
     */
    public function wherePosition($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('position', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeTemplateCollectionPrepare
     */
    public function orWherePosition($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('position', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeTemplateCollectionPrepare
     */
    public function selectPosition(){
        $this->objectMap[] = array(
            'class' => 'CodeTemplate',
            'field' => 'position',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('position');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeTemplateCollectionPrepare
     */
    public function whereTemplate($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('template', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeTemplateCollectionPrepare
     */
    public function orWhereTemplate($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('template', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeTemplateCollectionPrepare
     */
    public function selectTemplate(){
        $this->objectMap[] = array(
            'class' => 'CodeTemplate',
            'field' => 'template',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('template');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeTemplateCollectionPrepare
     */
    public function whereLanguage($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('language', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeTemplateCollectionPrepare
     */
    public function orWhereLanguage($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('language', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeTemplateCollectionPrepare
     */
    public function selectLanguage(){
        $this->objectMap[] = array(
            'class' => 'CodeTemplate',
            'field' => 'language',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('language');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeTemplateCollectionPrepare
     */
    public function whereStereoType($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('stereoType', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeTemplateCollectionPrepare
     */
    public function orWhereStereoType($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('stereoType', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeTemplateCollectionPrepare
     */
    public function selectStereoType(){
        $this->objectMap[] = array(
            'class' => 'CodeTemplate',
            'field' => 'stereoType',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('stereoType');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeTemplateCollectionPrepare
     */
    public function whereExtend($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('extend', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeTemplateCollectionPrepare
     */
    public function orWhereExtend($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('extend', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeTemplateCollectionPrepare
     */
    public function selectExtend(){
        $this->objectMap[] = array(
            'class' => 'CodeTemplate',
            'field' => 'extend',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('extend');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeTemplateCollectionPrepare
     */
    public function whereRequiereUmlProperties($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('requiereUmlProperties', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeTemplateCollectionPrepare
     */
    public function orWhereRequiereUmlProperties($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('requiereUmlProperties', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeTemplateCollectionPrepare
     */
    public function selectRequiereUmlProperties(){
        $this->objectMap[] = array(
            'class' => 'CodeTemplate',
            'field' => 'requiereUmlProperties',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('requiereUmlProperties');
        return $this;
    }

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return CodeTemplateCollectionPrepare
     */
    public function whereUpdated($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->where($operation);
        }else{
            $this->query->where('updated', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return CodeTemplateCollectionPrepare
     */
    public function orWhereUpdated($operation, $value = false){
        if(is_callable($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('updated', $operation, $value);
        }
        return $this;
    }

    /**
     *
     * @return CodeTemplateCollectionPrepare
     */
    public function selectUpdated(){
        $this->objectMap[] = array(
            'class' => 'CodeTemplate',
            'field' => 'updated',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('updated');
        return $this;
    }

    /**
     * @var CodeModulCollectionPrepare
     */
    protected $modul;
    /**
     * @var CodeModulCollectionPrepare
     * @return CodeTemplateCollectionPrepare
     */
    public function setModul(CodeModulCollectionPrepare $modul){
       $this->modul = $modul;
       $modul->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    public function getModul(){
       if(!isset($this->modul)) $this->modul = CodeModulCollectionPrepare::create();
       return $this->modul;
    }
    
    
    public function selectModul(){
        $this->objectMap[] = array(
            'class' => 'modul',
            'field' => 'modul',
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->addSelect('modul');
        return $this;
    }





    /**
     * @var ModulPositionCollectionPrepare
     */
    protected $position;
    /**
     *
     * @param ModulPositionCollectionPrepare $position
     * @return positionCollectionPrepare
     */
    public function setPosition(ModulPositionCollectionPrepare $position){
       $this->position = $position;
       $position->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    /**
     * @var CodeLanguageCollectionPrepare
     */
    protected $language;
    /**
     *
     * @param CodeLanguageCollectionPrepare $language
     * @return languageCollectionPrepare
     */
    public function setLanguage(CodeLanguageCollectionPrepare $language){
       $this->language = $language;
       $language->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    /**
     * @var CodeModulCollectionPrepare
     */
    protected $extend;
    /**
     *
     * @param CodeModulCollectionPrepare $extend
     * @return extendCollectionPrepare
     */
    public function setExtend(CodeModulCollectionPrepare $extend){
       $this->extend = $extend;
       $extend->setTableNameAliasPrepend($this->getTableNameAlias());
       return $this;
    }

    private $selectAll = false;
    public function selectAll(){
        parent::selectAll();
        $this->selectPosition();
        $this->selectTemplate();
        $this->selectLanguage();
        $this->selectStereoType();
        $this->selectExtend();
        $this->selectRequiereUmlProperties();
        $this->selectModul();

        return $this;
    }
    public function prepareQuery(){
        $this->prepareJoins();
        $this->query->from = 'CodeTemplate'.(($this->tableNameAlias == '')?'':' AS '.$this->tableNameAlias);
        QueryBuilder::appendWhereWithTable($this->query, $this->getTableNameAlias());
        return $this;
    }
    /**
     * @return CodeTemplateCollection
     */
    public function get(){
        $this->prepareQuery();
        $rows = $this->query->get();
        return $this->parseDbRows($rows, $this->getObjectMap());
    }

    /**
     * parse all return rows from sql
     */
    private function parseDbRows($rows, $map){
        $objsArray = array();
        $objs = CodeTemplateCollection::create();
        //build assoc Array with all data
        if(!$this->selectAll){
            foreach($rows as $row){
                $primaryField = $row[$this->primaryKeyIndex];

                //extend object if allready exists

                $initObj = (isset($objsArray[$primaryField]))?$objsArray[$primaryField]:array();
                $objsArray[$primaryField] = $this->parseDbRow($row, $map, $initObj);
            }
        }else $objsArray = $rows;
        //convert array to real objects (subobjects are generated in constructors)
        foreach($objsArray as $dataArray){
            $objs->add(new CodeTemplate($dataArray));
        }
        return $objs;
    }

    /**
     * parse one row into Object and extend existing ones
     */
    private function parseDbRow($row, $map, $objsData = array(), &$index = 0){
        foreach($map as $m){
            //check if association
            if(isset($m['map'])){
                //check if cardinality is multi
                if($m['multi']){
                    if(!isset($objsData[$m['field']])) $objsData[$m['field']] = array();
                    //get primaryKey ob subobject
                    $id = $row[$index+$m['primaryKeyIndex']];
                    //extend object if allready exists
                    $initObj = (isset($objsData[$m['field']][$id]))?$objsData[$m['field']][$id]:array();
                    //add/extend object with id
                    $objsData[$m['field']][$id] = $this->parseDbRow($row, $m['map'], $initObj, $index);
                }else{
                    $objsData[$m['field']] = $this->parseDbRow($row, $m['map'], array(), $index);
                }
            }
            else{
                $objsData[$m['field']] = $row[$index];
                $index++;
            }
        }
        return $objsData;
    }

    /**
     * create joins depending on associations
     * @return CollectionPrepare
     */
    public function prepareJoins(){
        parent::prepareJoins();
        if(!$this->selectAll){
            if(!is_array($this->query->columns) || !in_array('modulId', $this->query->columns)){
                $this->selectmodulId();
            }elseif(!is_array($this->query->columns)){
                $this->selectAll();
            }
        }else{
            $this->query->select('*');
        }
        if(isSet($this->modul)){
            $tableName = $this->modul->getTableNameAlias();
            //Single CodeTemplate
             $this->query->join('CodeModul as '.$tableName, $tableName.'.modulId', '=', $this->getTableNameAlias().'.modul', 'LEFT');
            $this->modul->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->modul->getQuery(),$tableName);
            $this->query->mergeQuery($this->modul->getQuery());

            $this->objectMap[] = array(
                'class' => 'CodeModul',
                'field' => 'modul',
                'multi' => false,
                'primaryKeyIndex' => $this->modul->getPrimaryKeyIndex(),
                'map' => $this->modul->getObjectMap()
            );
        }



        if(isSet($this->position)){
            $tableName = $this->position->getTableNameAlias();
            //Single CodeTemplate
            $this->query->join('ModulPosition AS '.$tableName, $tableName.'.positionId', '=', $this->getTableNameAlias().'.position', 'LEFT');

            $this->position->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->position->getQuery(),$tableName);
            $this->query->mergeQuery($this->position->getQuery());

            $this->objectMap[] = array(
                'class' => 'ModulPosition',
                'field' => 'position',
                'multi' => false,
                'primaryKeyIndex' => $this->position->getPrimaryKeyIndex(),
                'map' => $this->position->getObjectMap()
            );
        }
        if(isSet($this->language)){
            $tableName = $this->language->getTableNameAlias();
            //Single CodeTemplate
            $this->query->join('CodeLanguage AS '.$tableName, $tableName.'.langId', '=', $this->getTableNameAlias().'.language', 'LEFT');

            $this->language->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->language->getQuery(),$tableName);
            $this->query->mergeQuery($this->language->getQuery());

            $this->objectMap[] = array(
                'class' => 'CodeLanguage',
                'field' => 'language',
                'multi' => false,
                'primaryKeyIndex' => $this->language->getPrimaryKeyIndex(),
                'map' => $this->language->getObjectMap()
            );
        }
        if(isSet($this->extend)){
            $tableName = $this->extend->getTableNameAlias();
            //Single CodeTemplate
            $this->query->join('CodeModul AS '.$tableName, $tableName.'.modulId', '=', $this->getTableNameAlias().'.extend', 'LEFT');

            $this->extend->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->extend->getQuery(),$tableName);
            $this->query->mergeQuery($this->extend->getQuery());

            $this->objectMap[] = array(
                'class' => 'CodeModul',
                'field' => 'extend',
                'multi' => false,
                'primaryKeyIndex' => $this->extend->getPrimaryKeyIndex(),
                'map' => $this->extend->getObjectMap()
            );
        }
        return $this;
    }
}
