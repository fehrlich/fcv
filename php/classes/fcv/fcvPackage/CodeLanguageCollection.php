<?php
namespace fcv\fcvPackage;


use \fcv\fcvPackage\LibraryCollection;
use \fcv\fcvPackage\ProjectCollection;
class CodeLanguageCollection
        
        
        implements \Iterator, \ArrayAccess, \Countable {
    private $index = 0;
    /**
     * @var CodeLanguage[] $objArray
     */
    protected $objArray = array();


    public function __construct($data = array()){
        $this->objArray = $data;
    }

    /**
     * @param CodeLanguage[] $data
     * @return CodeLanguageCollection inserted Id
     */
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new CodeLanguageCollection($data);
    }
    /**
     * @param CodeLanguage $el
     * @return CodeLanguageCollection inserted Id
     */
    public function add($el){
        $this->objArray[] = $el;
    }

    public function current() {
        return $this->objArray[$this->index];
    }

    public function key() {
        return $this->index;
    }

    public function next() {
        $this->index++;
    }

    public function rewind() {
        $this->index = 0;
    }

    public function valid() {
        return isset($this->objArray[$this->index]);
    }
    public function append($el) {
        return $this->add($el);
    }
    public function count() {
        return count($this->objArray);
    }

    public function offsetExists($offset) {
        return isSet($this->objArray[$offset]);
    }

    public function offsetGet($offset) {
        return $this->objArray[$offset];
    }

    public function offsetSet($offset, $value) {
        $this->objArray[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->objArray[$offset]);
    }

    public function seek($position) {
        $this->index = $position;
    }

    public function serialize() {
        return serialize($this->objArray);
    }

    public function unserialize($serialized) {
        return unserialize($serialized);
    }
    //UserMethods

    //Setters
    /**
    *
    * @param Integer $langId
    * @return CodeLanguage
    */
    public function setLangId($langId){
       foreach($this->objArray as $el){
           $el->setLangId($langId);
       }
       return $this;
    }
    /**
    *
    * @param String $name
    * @return CodeLanguage
    */
    public function setName($name){
       foreach($this->objArray as $el){
           $el->setName($name);
       }
       return $this;
    }
    /**
    *
    * @param String $extension
    * @return CodeLanguage
    */
    public function setExtension($extension){
       foreach($this->objArray as $el){
           $el->setExtension($extension);
       }
       return $this;
    }
    public function setLib(Library $lib){
       foreach($this->objArray as $el){
           $el->setLib($lib);
       }
       return $this;
    }
    public function setInProject(Project $inProject){
       foreach($this->objArray as $el){
           $el->setInProject($inProject);
       }
       return $this;
    }
    public function save(){
       foreach($this->objArray as $el){
           $el->save();
       }
       return $this;
    }
    public function treeSave(){
       foreach($this->objArray as $el){
           $el->treeSave();
       }
       return $this;
    }
    public function getHtml($tplName = 'default'){
       $html = '';
       foreach($this->objArray as $el){
           $html .= $el->getHtml($tplName);
       }
       return $html;
    }
    
    public function each($cb){
        foreach($this->objArray as $i => $o){
            $cb($o,$i);
        }
        return $this;
    }
    
    public function addToJs(){
        foreach($this->objArray as $i => $o){
            $o->addToJs();
        }
        return $this;
    }
    //Map Reduce Pattern
    public function map($mapFunction){
        $ret = array();
        foreach($this->objArray as $i => $o){
            $ret[$i] = $mapFunction($o);
        }
        return $ret;
    }
    
    public function reduce($reduceFunction){

        $length = count($this->objArray);
        if($length <= 1) return $this->objArray;

        $array = $this->objArray;
        $out = array_shift($array);
        do {
            $next = array_shift($array);
            $out = $reduceFunction($out, $next);
        } while(!empty($array));

        return $out;
    }
}
