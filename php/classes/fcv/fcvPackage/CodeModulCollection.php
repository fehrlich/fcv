<?php
namespace fcv\fcvPackage;


use \fcv\fcvPackage\ModulPositionCollection;
use \fcv\fcvPackage\BuildCollection;
use \fcv\fcvPackage\CodeModulCollection;
use \fcv\fcvPackage\CodeTemplateCollection;
use \fcv\fcvPackage\WorkPackageCollection;
use \fcv\fcvPackage\ProjectCollection;
class CodeModulCollection
        
        
        implements \Iterator, \ArrayAccess, \Countable {
    private $index = 0;
    /**
     * @var CodeModul[] $objArray
     */
    protected $objArray = array();


    public function __construct($data = array()){
        $this->objArray = $data;
    }

    /**
     * @param CodeModul[] $data
     * @return CodeModulCollection inserted Id
     */
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new CodeModulCollection($data);
    }
    /**
     * @param CodeModul $el
     * @return CodeModulCollection inserted Id
     */
    public function add($el){
        $this->objArray[] = $el;
    }

    public function current(): mixed {
        return $this->objArray[$this->index];
    }

    public function key(): int {
        return $this->index;
    }

    public function next(): void {
        $this->index++;
    }

    public function rewind(): void {
        $this->index = 0;
    }

    public function valid(): bool {
        return isset($this->objArray[$this->index]);
    }
    public function append($el) {
        return $this->add($el);
    }
    public function count(): int {
        return count($this->objArray);
    }

    public function offsetExists($offset): bool {
        return isSet($this->objArray[$offset]);
    }

    public function offsetGet($offset): mixed {
        return $this->objArray[$offset];
    }

    public function offsetSet($offset, $value): void {
        $this->objArray[$offset] = $value;
    }

    public function offsetUnset($offset): void {
        unset($this->objArray[$offset]);
    }

    public function seek($position): void {
        $this->index = $position;
    }

    public function serialize() {
        return serialize($this->objArray);
    }

    public function unserialize($serialized) {
        return unserialize($serialized);
    }
    //UserMethods
    /**
      *
      * @return CodeModulCollection this Collection Object
      */
    public function getAbsolutePath(){
        foreach($this->objArray as $el){
           $el->getAbsolutePath();
       }
       return $this;
    }
    /**
      *
      * @return CodeModulCollection this Collection Object
      */
    public function build(){
        foreach($this->objArray as $el){
           $el->build();
       }
       return $this;
    }
    /**
      *
      * @return CodeModulCollection this Collection Object
      */
    public function getWorkPackage(){
        foreach($this->objArray as $el){
           $el->getWorkPackage();
       }
       return $this;
    }
    /**
      *
      * @return CodeModulCollection this Collection Object
      */
    public function getProject(){
        foreach($this->objArray as $el){
           $el->getProject();
       }
       return $this;
    }

    //Setters
    /**
    *
    * @param Integer $modulId
    * @return CodeModul
    */
    public function setModulId($modulId){
       foreach($this->objArray as $el){
           $el->setModulId($modulId);
       }
       return $this;
    }
    /**
    *
    * @param String $name
    * @return CodeModul
    */
    public function setName($name){
       foreach($this->objArray as $el){
           $el->setName($name);
       }
       return $this;
    }
    /**
    *
    * @param String $path
    * @return CodeModul
    */
    public function setPath($path){
       foreach($this->objArray as $el){
           $el->setPath($path);
       }
       return $this;
    }
    /**
    *
    * @param String $type
    * @return CodeModul
    */
    public function setType($type){
       foreach($this->objArray as $el){
           $el->setType($type);
       }
       return $this;
    }
    /**
    *
    * @param ModulTypes $applyTo
    * @return CodeModul
    */
    public function setApplyTo($applyTo){
       foreach($this->objArray as $el){
           $el->setApplyTo($applyTo);
       }
       return $this;
    }
    /**
    *
    * @param WorkPackage $workPackage
    * @return CodeModul
    */
    public function setWorkPackage($workPackage){
       foreach($this->objArray as $el){
           $el->setWorkPackage($workPackage);
       }
       return $this;
    }
    /**
    *
    * @param Boolean $buildOnlyOnFirstBuild
    * @return CodeModul
    */
    public function setBuildOnlyOnFirstBuild($buildOnlyOnFirstBuild){
       foreach($this->objArray as $el){
           $el->setBuildOnlyOnFirstBuild($buildOnlyOnFirstBuild);
       }
       return $this;
    }
    /**
    *
    * @param Project $project
    * @return CodeModul
    */
    public function setProject($project){
       foreach($this->objArray as $el){
           $el->setProject($project);
       }
       return $this;
    }
    public function setPositions(ModulPosition $positions){
       foreach($this->objArray as $el){
           $el->setPositions($positions);
       }
       return $this;
    }
    public function setUsedInBuilds(Build $usedInBuilds){
       foreach($this->objArray as $el){
           $el->setUsedInBuilds($usedInBuilds);
       }
       return $this;
    }
    public function setChilds(CodeModul $childs){
       foreach($this->objArray as $el){
           $el->setChilds($childs);
       }
       return $this;
    }
    public function setParent(CodeModul $parent){
       foreach($this->objArray as $el){
           $el->setParent($parent);
       }
       return $this;
    }
    public function setCodeTemplates(CodeTemplate $codeTemplates){
       foreach($this->objArray as $el){
           $el->setCodeTemplates($codeTemplates);
       }
       return $this;
    }
    public function save(){
       foreach($this->objArray as $el){
           $el->save();
       }
       return $this;
    }
    public function treeSave(){
       foreach($this->objArray as $el){
           $el->treeSave();
       }
       return $this;
    }
    public function getHtml($tplName = 'default'){
       $html = '';
       foreach($this->objArray as $el){
           $html .= $el->getHtml($tplName);
       }
       return $html;
    }
    
    public function each($cb){
        foreach($this->objArray as $i => $o){
            $cb($o,$i);
        }
        return $this;
    }
    
    public function addToJs(){
        foreach($this->objArray as $i => $o){
            $o->addToJs();
        }
        return $this;
    }
    //Map Reduce Pattern
    public function map($mapFunction){
        $ret = array();
        foreach($this->objArray as $i => $o){
            $ret[$i] = $mapFunction($o);
        }
        return $ret;
    }
    
    public function reduce($reduceFunction){

        $length = count($this->objArray);
        if($length <= 1) return $this->objArray;

        $array = $this->objArray;
        $out = array_shift($array);
        do {
            $next = array_shift($array);
            $out = $reduceFunction($out, $next);
        } while(!empty($array));

        return $out;
    }
}
