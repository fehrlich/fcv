<?php
namespace fcv\fcvPackage; ///fcv/fcvPackage/WorkPackage
    
use Exception;
use mys;
use QueryBuilder;
use fcv\libs\TemplateEngine;
use fcv\libs\Debug;
use fcv\FCVJsObj;


use fcv\fcvPackage\Views;
use fcv\fcvPackage\ViewsCollection;
use fcv\fcvPackage\Project;
;
use fcv\fcvPackage\WorkPackage;
use fcv\fcvPackage\WorkPackageCollection;
use fcv\fcvPackage\CodeTemplate;

class WorkPackage
        
         
        {
    
    //Attributes
    //const UMLID = 'umlId';
    //const FUNCTIONCONTENT = 'functionContent';
    //const CONTROLCONTENT = 'controlContent';
    //const TYPE = 'type';
    //const UMLELEMENT = 'umlElement';
    //const FUNCTIONISDEFAULT = 'functionIsDefault';
    //const CONTROLISDEFAULT = 'controlIsDefault';
    //const NAME = 'name';
    
    //Litearls
    
    //Attributes
    /**
     *
     * @var String
     */
     public $umlId;
    const FIELD_UMLID = 'umlId';
    /**
     *
     * @var String
     */
     public $functionContent;
    const FIELD_FUNCTIONCONTENT = 'functionContent';
    /**
     *
     * @var String
     */
     public $controlContent;
    const FIELD_CONTROLCONTENT = 'controlContent';
    /**
     *
     * @var ModulTypes
     */
     public $type;
    const FIELD_TYPE = 'type';
    /**
     *
     * @var 
     */
     public $umlElement;
    const FIELD_UMLELEMENT = 'umlElement';
    /**
     *
     * @var Boolean
     */
     public $functionIsDefault = true;
    const FIELD_FUNCTIONISDEFAULT = 'functionIsDefault';
    /**
     *
     * @var Boolean
     */
     public $controlIsDefault = true;
    const FIELD_CONTROLISDEFAULT = 'controlIsDefault';
    /**
     *
     * @var String
     */
     public $name;
    const FIELD_NAME = 'name';
    
    //AssociationsEnds
    
    /**
     * @var ViewsCollection
     */
    private $views = false;
    const VIEWS = 'views';
    
    //{/isAssociationClass}}
    
    /**
     * @var Project
     */
    private $project = false;
    const PROJECT = 'project';
    
    //{/isAssociationClass}}
    
    /**
     * @var WorkPackage
     */
    private $parent = false;
    const PARENT = 'parent';
    
    //{/isAssociationClass}}
    
    /**
     * @var WorkPackageCollection
     */
    private $childs = false;
    const CHILDS = 'childs';
    
    //{/isAssociationClass}}
    //FCV Attributes
    private $apPath = 'FCVAPs/';
    //    public $VIEW = array();
    static $fcvFields = array('umlId','functionContent','controlContent','type','umlElement','functionIsDefault','controlIsDefault','name');
    //Todo: Outsource
    protected $loadObject = false;
    private $loadedAttributes = array();
    
    //Methods
    //UserOperations
    /**
      *
      * @return 
      */
    public  function build($buildworkpackages = false){
    
        /* @var $this WorkPackage*/
        $this->loadContentFromSource();
    
        if($this->hasChilds()){
            $this->getChilds()->build($buildworkpackages);
        }
        if($buildworkpackages){
            $this->saveToFiles();
        }
            
            
    
    }
    /**
      *
      * @return 
      */
    public  function getPath(){
        
            /* @var $this WorkPackage*/
            
            if($this->type == 'Frame' || $this->type == "Page") return '/'.$this->getProject()->getName().'/FCVNavigation/'.$this->getType ().'/'.$this->getName();
            if (!$this->hasParent()) {
                return '/';
            }
            if($this->getName() == ''){
                throw new Exception("The Name Of the Uml Element is empty (ID: ".$this->getUmlElement()->getId().")");
                exit();
            }
            if (!$this->hasParent() || $this->getParent()->getType() == 'Project') {
                return '/'.$this->getName();
            }
            
            $path = array();
            $parent = $this->getParent();
            //new dBug($parent);
            $path[] = $parent->getName();
            while($parent->hasParent() && $parent->getParent()->getType() != 'Project'){
                $parent = $parent->getParent();
            //    d($parent);
                $path[] = $parent->getName();
            }
            $path[] = $this->getProject()->getName();
            //d($this);
            //dd('/'.implode('/',array_reverse($path)).'/'.$this->getName());
            
            return '/'.implode('/',array_reverse($path)).'/'.$this->getName();
    
    }

    public function writeWorkPackage($extension, $contentname, $stripEmptyFunction, $defaultContentField){
        
        $apPath = $this->getProject()->getCurrentBuild()->getWorkPackagePath();
        $path = $apPath.$this->getPath();
        if(substr($path, 0, 1) == '/') $path = substr ($path, 1);

        $type = strtolower($this->getType());
        if(!isset(WorkPackage::$defaultContents[$type.'.'.$extension])){
            WorkPackage::$defaultContents[$type.'.'.$extension] = file_get_contents(Build::$defaultContentPath.'/'.strtolower($this->getType()).'.'.$extension);
        }
        $defaultFunctionTpl = WorkPackage::$defaultContents[$type.'.'.$extension];
        $tplEngine = new TemplateEngine();
        $defaultFunction = $tplEngine->render($defaultFunctionTpl, $this);
        
        $functionContent = @file_get_contents($path.'/'.$this->getName().'.'.$extension);
        $functionContentExists = $functionContent !== false;

        $functionContent = $functionContentExists ? $functionContent : $this->$contentname;
        
        if($this->$defaultContentField) $this->$defaultContentField = $functionContent == $defaultFunction || $stripEmptyFunction($functionContent);
        $this->$contentname = ($this->$defaultContentField)?$defaultFunction:$functionContent;
    }

    /**
      *
      * @return 
      */
    static $defaultContents = [];
    public  function loadContentFromSource(){
        /* @var $this WorkPackage*/
        
        if($this->getType() == 'Attribute') return;
        if($this->getType() == 'Model') return;
        if($this->getType() == 'Project') return;
        if($this->getType() == 'Package') return;
        
        $projectBuild = $this->getProject();
        $apPath = $this->getProject()->getCurrentBuild()->getWorkPackagePath();
        $path = $apPath.$this->getPath();
        if(substr($path, 0, 1) == '/') $path = substr ($path, 1);

        //Todo: Outsourcen
        $this->writeWorkPackage('php', 'functionContent', function($code){
            return in_array(trim(stripCommentsPHP($code)), ['', '<?php']);
        }, 'functionIsDefault');

        $name = $this->name;
        $this->writeWorkPackage('js', 'controlContent', function($code) use ($name){
            $stripped = stripCommentsJS($code, true);
            if(is_null($stripped)){
                return false;
            }
            return empty(trim($stripped));
        }, 'controlIsDefault');

        $viewContentFields = array();
        
        foreach(Views::$fcvFields as $field){
            if(substr($field, -7) == 'Content'){
                $viewContentFields[] = substr($field, 0, -7);
            }
        }
        
        if((!$this->hasViews() /*|| $this->getViews()->count() == 0*/) && in_array($this->type, ['Frame', 'Page'])){
            $view = Views::create()
                ->setName('default')
                ->setHtmlContent('')
                ->setCssContent('');

            $this->addViews($view);
        }elseif($this->hasViews()){
            $views = $this->hasViews() ? $this->getViews() : [];
        
            foreach($views as $v){
                foreach(Views::$fcvFields as $field){
                    if(substr($field, -7) == 'Content'){
                        $type = substr($field, 0, -7);
                        
                        $viewConent = @file_get_contents($path.'/'.$this->getName().'.'.$v->getName().'.'.$type);
        
                        if($viewConent !== false){
                            $v->set($field, $viewConent);
                        }
                    }
                }
            }
        }
        
        //load new views
        $files = glob($path."/".$this->getName().'.*.'.$viewContentFields[0]);
        if($files){
            foreach ($files as $filename) {
                $viewNameAndExt = substr(basename($filename),  strlen($this->getName())+1);
                $spl = explode('.', $viewNameAndExt);
                $viewName = $spl[0];
                $exists = false;
                if($this->hasViews()){
                    foreach($this->getViews() as $v){
                        if($v->getName() == $viewName) $exists = true;
                    }
                }
                if(!$exists){
                    $newView = Views::create()
                        ->setName($viewName);
        
                    foreach($viewContentFields as $viewField){
                        $f = $path.'/'.$this->getName ().'.'.$viewName.'.'.$viewField;
                        $vContent = @file_get_contents($f);
                        $newView->set($viewField.'Content', $vContent);
                    }
                    $this->addViews($newView);
                }
            }
        }
    
    }
    /**
      *
      * @return 
      */
    public  function saveToFiles(){
        
            /* @var $this WorkPackage*/
            if($this->getType() == 'Attribute') return;
            if($this->getType() == 'Project') return;
            if($this->getType() == 'Model') return;
            if($this->getType() == 'Package') return;
            
            $apPath = $this->getProject()->getCurrentBuild()->getWorkPackagePath();
            $path = $apPath.$this->getPath();
            
            // if(!is_dir($path)) mkdir ($path,0777, true);
            $umlElement = $this->getUmlElement();

            if (!$umlElement->isJs()) {
                writeContentToFile($path.'/'.$this->getName().'.php', $this->functionContent);
            } elseif(file_exists($path.'/'.$this->getName().'.php')) {
                echo 'delete ' . $path.'/'.$this->getName().'.php' . "\n";
            }

            if ($umlElement->isClient()) {
                writeContentToFile($path.'/'.$this->getName().'.js', $this->controlContent);
            } elseif(file_exists($path.'/'.$this->getName().'.js')) {
                echo 'delete ' . $path.'/'.$this->getName().'.js' . "\n";
            }
            
            $updateViews = ViewsCollection::create();
            if($this->hasViews()){
                foreach($this->getViews() as $view){
                    $forceWrite = $view->getName() == 'default' && in_array($this->type, ['Frame', 'Page']);
                    if(!empty($view->getHtmlContent()) || !empty($view->getCssContent()) || $forceWrite){
                        writeContentToFile($path.'/'.$this->getName().'.'.$view->getName().'.html', $view->getHtmlContent());
                        writeContentToFile($path.'/'.$this->getName().'.'.$view->getName().'.css', $view->getCssContent());
                        $updateViews->add($view);
                    }
                    if(!empty($view->getPhpContent())){
                        writeContentToFile($path.'/'.$this->getName().'.'.$view->getName().'.php', $view->getPhpContent());
                    }
                }
            }
            $this->setViews($updateViews);
            
            if($this->hasChilds()){
                $this->getChilds()->saveToFiles();
            }
    
    }
    /**
      *
      * @return 
      */
    public  function getUmlElement(){
        
            /* @var $this WorkPackage*/
            if(!isSet($GLOBALS['countThisShit'])) $GLOBALS['countThisShit'] = 0;
            $GLOBALS['countThisShit']++;
            //if($GLOBALS['countThisShit'] == 100){
            //    d(1);
            //    exit();
            //}
            return is_object($this->umlElement)?$this->umlElement:unserialize($this->umlElement);
    
    }
    /**
      *
      * @return 
      */
    public  function buildModules($modules = null){
        
            /* @var $this WorkPackage*/
            //Todo: Fix multiple applyTo
            $overWriteType = ($this->getType() == 'Page' || $this->getType() == 'Frame')?'Class':$this->getType();
            $searchFor = CodeTemplate::create(array(), true)->setApplyTo($overWriteType);
            
            $codeTpls = null;
            if(!$modules){
                $codeTpls = \fcv\fcvPackage\CodeTemplateCollectionPrepare::create()
                        ->whereApplyTo('=', $overWriteType)
                        ->selectAll()
                        ->get();
            } else {
                $codeTpls = CodeTemplateCollection::create();
                foreach($modules as $module){
                    if($module->getType() == $overWriteType){
                        $codeTpls->add($module);
                    }
                }
            }
            
            $codeTpls->setWorkPackage($this);
            $codeTpls->setProject($this->getProject());
            $codeTpls->build();
            
            if($this->hasChilds()){
                $this->getChilds()->buildModules($modules);
            }
    
    }
    /**
      *
      * @param ModulTypes $type
      * @return 
      */
    public  function getWorkPackagesByType( $type ){
        
            /* @var $this WorkPackage*/
            $arr = array();
            if($this->getType() == $type){
                $arr[] = $this;
            }
            if($this->hasChilds()){
                foreach($this->getChilds() as $child){
                    $arr = array_merge($arr, $child->getWorkPackagesByType($type));
                }
            }
            return $arr;
    
    }
    /**
      *
      * @param  $umlElement
      * @return 
      */
    public  function setUmlElement( $umlElement ){
        
            /* method setUmlElement from Class WorkPackage */
            /* @var $this WorkPackage */
            // Parameters:
            /* @var $umlElement  */
            
            $this->umlElement = $umlElement;
            return $this;
    
    }
    /**
      *
      * @return 
      */
    public  function getName(){
        
            /* @var $this fcv\fcvPackage\WorkPackage*/
            //d($this);
            if(!$this->getUmlElement()){
                if($this->fcvgetName() != '') return $this->fcvgetName ();
                d($this->getUmlElement());
                //Kint::($this->getUmlElement());
                d(1);
            }
            return $this->getUmlElement()->getName();
    
    }
    private $lastModifiedTime = false;
    private $createdTime = false;
    public function __construct($data = array(),$calledFromChild = false){
        if(is_string($data)) $data = json_decode($data,true);
        if(count($data) > 0){
            if(isSet($data['loadObj']) && $data['loadObj']){
                $this->loadObject = true;
            }
            
        //        if(isset($data['umlId']) && !empty($data['umlId']))
    
            if(isSet($data['umlId'])) $this->umlId = $data['umlId'];
            if(isSet($data['functionContent'])) $this->functionContent = $data['functionContent'];
            if(isSet($data['controlContent'])) $this->controlContent = $data['controlContent'];
            if(isSet($data['type'])) $this->type = $data['type'];
            if(isSet($data['umlElement'])) $this->umlElement = $data['umlElement'];
            if(isSet($data['functionIsDefault'])) $this->functionIsDefault = $data['functionIsDefault'];
            if(isSet($data['controlIsDefault'])) $this->controlIsDefault = $data['controlIsDefault'];
            if(isSet($data['name'])) $this->name = $data['name'];
            //associationEnds
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['views'])){
                if(!is_object(current($data['views']))){
                    $newVal = new ViewsCollection();
                    foreach($data['views'] as $el) $newVal->add(Views::create($el,$this->loadObject));
                    $this->views = $newVal;
                }else  $this->views = $data['views'];
            }*/
    
            if(isSet($data['views'])){
                if(!is_object(current($data['views']))){
                    $newVal = new ViewsCollection();
                    foreach($data['views'] as $el) $newVal->add(Views::create($el,$this->loadObject));
                    $this->views = $newVal;
                }else $this->views = $data['views'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['project'])){
                $this->project = (is_array($data['project']))?project::create($data['project'],$this->loadObject):$data['project'];
            }*/
    
            if(isSet($data['project'])){
                $this->project = (is_array($data['project']))?Project::create($data['project'],$this->loadObject):$data['project'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['parent'])){
                $this->parent = (is_array($data['parent']))?parent::create($data['parent'],$this->loadObject):$data['parent'];
            }*/
    
            if(isSet($data['parent'])){
                $this->parent = (is_array($data['parent']))?WorkPackage::create($data['parent'],$this->loadObject):$data['parent'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['childs'])){
                if(!is_object(current($data['childs']))){
                    $newVal = new WorkPackageCollection();
                    foreach($data['childs'] as $el) $newVal->add(WorkPackage::create($el,$this->loadObject));
                    $this->childs = $newVal;
                }else  $this->childs = $data['childs'];
            }*/
    
            if(isSet($data['childs'])){
                if(!is_object(current($data['childs']))){
                    $newVal = new WorkPackageCollection();
                    foreach($data['childs'] as $el) $newVal->add(WorkPackage::create($el,$this->loadObject));
                    $this->childs = $newVal;
                }else $this->childs = $data['childs'];
            }
    
            //Helper
            if(isSet($data['lastModified'])) $this->lastModifiedTime = $data['lastModified'];
            if(isSet($data['created'])) $this->createdTime = $data['created'];
        }
    
    
    
    
    }
    
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new WorkPackage($data);
    }
    
    public static function createLoad(){
        $data = array(
            'loadObj' => true
        );
        return new WorkPackage($data);
    }
    
    public function getLastModifiedTime(){
        return $this->lastModifiedTime;
    }
    
    public function setLastModifiedTime($time){
        $this->lastModifiedTime = $time;
        return $this;
    }
    
    public function hasPrimaryKey(){
        return true;
    }
    
    public function getId(){
        return $this->umlId;
    }
    public function setId($id){
    //        WorkPackage::$loadedInstances[$id] = &$this;
        $this->umlId = $id;
        if($this->loadObject) $this->loadedAttributes['umlId'] = true;
        return $this;
    }
    public function getPrimaryKeyName(){
        return "umlId";
    }
    
    public function addToJs($selectionString = ''){
    //    if(!$calledFromChild && get_class($this) == '' && (!$this->loadObject))
            FCVJsObj::add('', $this, $selectionString);
            return $this;
    }
    public function throwError($msg){
        if(!Debug::getObj()->getThrowErrors()) return "";
        else{
            throw new Exception($msg);
        }
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return ViewsCollection
     * @throws Exception
     */
    public function getViews($forceLoad = false){
        if(is_null($this->views) || empty($this->views)){
            return new ViewsCollection();
//            $this->throwError('class WorkPackage with id '.$this->getId().' has no Attributes views');
            //throw new Exception('class WorkPackage with id '.$this->getId().' has no Attributes views');
        }
        return $this->views;
    }
    
    public function hasViews($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getViews($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param ViewsCollection $views
     */
    public function setViews(ViewsCollection $views){
        if(!$this->loadObject) $views->setWp($this);
        $this->views = $views;
        if($this->loadObject) $this->loadedAttributes['views'] = true;
        return $this;
    }
    
    /**
     * @param WorkPackage $views
     */
    public function addViews(Views $views){
            if(!$this->loadObject) $views->setWp($this);
        if(!$views->hasPrimaryKey()){
            if(!$this->loadObject) $views->setParent($this, count($this->views));
        }
        if($this->loadObject){
            $this->loadedAttributes['views'] = true;
            if(!$this->views) $this->views = new ViewsCollection();
        }
        elseif(!$this->views){
//            if(!$this->hasViews()) 
                $this->views = new ViewsCollection();
        }
        $this->views->add($views);
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return Project
     * @throws Exception
     */
    public function getProject($forceLoad = false){
        if(is_null($this->project) || empty($this->project)){
            $this->throwError('class WorkPackage with id '.$this->getId().' has no Attributes project');
            //throw new Exception('class WorkPackage with id '.$this->getId().' has no Attributes project');
        }
        return $this->project;
    }
    
    public function hasProject($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getProject($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param Project $project
     */
    public function setProject(Project $project){
        $this->project = $project;
        if($this->loadObject) $this->loadedAttributes['project'] = true;
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return WorkPackage
     * @throws Exception
     */
    public function getParent($forceLoad = false){
        if(is_null($this->parent) || empty($this->parent)){
            $this->throwError('class WorkPackage with id '.$this->getId().' has no Attributes parent');
            //throw new Exception('class WorkPackage with id '.$this->getId().' has no Attributes parent');
        }
        return $this->parent;
    }
    
    public function hasParent($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getParent($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param WorkPackage $workPackage
     */
    public function setParent(WorkPackage $parent){
        $this->parent = $parent;
        if($this->loadObject) $this->loadedAttributes['parent'] = true;
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return WorkPackageCollection
     * @throws Exception
     */
    public function getChilds($forceLoad = false){
        if(is_null($this->childs) || empty($this->childs)){
            $this->throwError('class WorkPackage with id '.$this->getId().' has no Attributes childs');
            //throw new Exception('class WorkPackage with id '.$this->getId().' has no Attributes childs');
        }
        return $this->childs;
    }
    
    public function hasChilds($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getChilds($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param WorkPackageCollection $workPackage
     */
    public function setChilds(WorkPackageCollection $childs){
        if(!$this->loadObject) $childs->setParent($this);
        $this->childs = $childs;
        if($this->loadObject) $this->loadedAttributes['childs'] = true;
        return $this;
    }
    
    /**
     * @param WorkPackage $workPackage
     */
    public function addWorkPackage(WorkPackage $workPackage){
            if(!$this->loadObject) $workPackage->setParent($this);
        if(!$workPackage->hasPrimaryKey()){
            if(!$this->loadObject) $workPackage->setParent($this, count($this->childs));
        }
        if($this->loadObject){
            $this->loadedAttributes['childs'] = true;
            if(!$this->childs) $this->childs = new WorkPackageCollection();
        }
        elseif(!$this->childs){
            if(!$this->hasChilds()) $this->childs = new WorkPackageCollection();
        }
        $this->childs->add($workPackage);
        return $this;
    }
    
    //Getter & Setter
    
    public function get($name){
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            return $this->$name->get($subName);
        }else return $this->$name;
    }
    
    public function has($name){
        if(strpos('.', $name) !== false){
            $spl = explode('.', $name, 2);
            return $this->get($spl[0])->has($spl[1]);
        }
        return (property_exists($this, $name));
    }
    
    public function set($name, $value){
        $possibleFields = array(
        'umlId',
        'functionContent',
        'controlContent',
        'type',
        'umlElement',
        'functionIsDefault',
        'controlIsDefault',
        'name',
        'views',
        'project',
        'parent',
        'childs',
        );
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class WorkPackage');
            if(!$this->$name){
                if(false);
                elseif($name == 'views'){
                    $this->views = Views::create(array(),$this->loadObject)->set($subName,$value);
                    $this->views = array($this->views->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['views'] = true;
                }
                elseif($name == 'project'){
                    $this->project = Project::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['project'] = true;
                }
                elseif($name == 'parent'){
                    $this->parent = WorkPackage::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['parent'] = true;
                }
                elseif($name == 'childs'){
                    $this->childs = WorkPackage::create(array(),$this->loadObject)->set($subName,$value);
                    $this->childs = array($this->childs->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['childs'] = true;
                }
                else die('Error');
            }else $this->$name->set($subName,$value);
        }else{
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class WorkPackage');
            $this->$name = $value;
            if($this->loadObject) $this->loadedAttributes[$name] = true;
        }
        return $this;
    }
    
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getUmlId(){
        return $this->umlId;
     }
    
     public function hasUmlId(){
        try {
            $this->getUmlId();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $umlId
     * @return WorkPackage
     */
     public function setUmlId($umlId){
        ;
        $this->umlId = $umlId;
        if($this->loadObject) $this->loadedAttributes['umlId'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getFunctionContent(){
        return $this->functionContent;
     }
    
     public function hasFunctionContent(){
        try {
            $this->getFunctionContent();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $functionContent
     * @return WorkPackage
     */
     public function setFunctionContent($functionContent){
        ;
        $this->functionContent = $functionContent;
        if($this->loadObject) $this->loadedAttributes['functionContent'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getControlContent(){
        return $this->controlContent;
     }
    
     public function hasControlContent(){
        try {
            $this->getControlContent();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $controlContent
     * @return WorkPackage
     */
     public function setControlContent($controlContent){
        ;
        $this->controlContent = $controlContent;
        if($this->loadObject) $this->loadedAttributes['controlContent'] = true;
        return $this;
     }
    /**
     *
     * @return ModulTypes
     * @throws Exception
     */
     public function getType(){
        return $this->type;
     }
    
     public function hasType(){
        try {
            $this->getType();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param ModulTypes $type
     * @return WorkPackage
     */
     public function setType($type){
        ;
        $this->type = $type;
        if($this->loadObject) $this->loadedAttributes['type'] = true;
        return $this;
     }
    /**
     *
     * @return 
     * @throws Exception
     */
     public function fcvgetUmlElement(){
        return $this->umlElement;
     }
    
     public function hasUmlElement(){
        try {
            $this->fcvgetUmlElement();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param  $umlElement
     * @return WorkPackage
     */
     public function fcvsetUmlElement($umlElement){
        ;
        $this->umlElement = $umlElement;
        if($this->loadObject) $this->loadedAttributes['umlElement'] = true;
        return $this;
     }
    /**
     *
     * @return Boolean
     * @throws Exception
     */
     public function getFunctionIsDefault(){
        return $this->functionIsDefault;
     }
    
     public function hasFunctionIsDefault(){
        try {
            $this->getFunctionIsDefault();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Boolean $functionIsDefault
     * @return WorkPackage
     */
     public function setFunctionIsDefault($functionIsDefault){
        ;
        $this->functionIsDefault = $functionIsDefault;
        if($this->loadObject) $this->loadedAttributes['functionIsDefault'] = true;
        return $this;
     }
    /**
     *
     * @return Boolean
     * @throws Exception
     */
     public function getControlIsDefault(){
        return $this->controlIsDefault;
     }
    
     public function hasControlIsDefault(){
        try {
            $this->getControlIsDefault();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Boolean $controlIsDefault
     * @return WorkPackage
     */
     public function setControlIsDefault($controlIsDefault){
        ;
        $this->controlIsDefault = $controlIsDefault;
        if($this->loadObject) $this->loadedAttributes['controlIsDefault'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function fcvgetName(){
        return $this->name;
     }
    
     public function hasName(){
        try {
            $this->fcvgetName();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $name
     * @return WorkPackage
     */
     public function setName($name){
        ;
        $this->name = $name;
        if($this->loadObject) $this->loadedAttributes['name'] = true;
        return $this;
     }
    protected $visitedByToArray = false;
    public function toJSObject($returnArray = false){
        if($this->loadObject) return false;
        if($this->visitedByToArray){
    //            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return '[VALENCODE]jsModel["WorkPackage.'.$this->getId().'"][VALENCODE]';
                    return array(
                        'class' => 'WorkPackage',
                        'id' => $this->getId()
                    );
                }
                return false;
    //            }
    //            return array(
    //                'class' => 'WorkPackage',
    //                'id' => rand()
    //            );
        }
        $retArray = array();
        
        $this->visitedByToArray = true;
        
        if(isSet($this->umlId) && !is_null($this->umlId)) $retArray['umlId'] = (!is_object($this->umlId))?str_replace('"', '\"', $this->umlId):($this->umlId);
        if(isSet($this->functionContent) && !is_null($this->functionContent)) $retArray['functionContent'] = (!is_object($this->functionContent))?str_replace('"', '\"', $this->functionContent):($this->functionContent);
        if(isSet($this->controlContent) && !is_null($this->controlContent)) $retArray['controlContent'] = (!is_object($this->controlContent))?str_replace('"', '\"', $this->controlContent):($this->controlContent);
        if(isSet($this->type) && !is_null($this->type)) $retArray['type'] = (!is_object($this->type))?str_replace('"', '\"', $this->type):($this->type);
        if(isSet($this->umlElement) && !is_null($this->umlElement)) $retArray['umlElement'] = (!is_object($this->umlElement))?str_replace('"', '\"', $this->umlElement):($this->umlElement);
        if(isSet($this->functionIsDefault) && !is_null($this->functionIsDefault)) $retArray['functionIsDefault'] = (!is_object($this->functionIsDefault))?str_replace('"', '\"', $this->functionIsDefault):($this->functionIsDefault);
        if(isSet($this->controlIsDefault) && !is_null($this->controlIsDefault)) $retArray['controlIsDefault'] = (!is_object($this->controlIsDefault))?str_replace('"', '\"', $this->controlIsDefault):($this->controlIsDefault);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (!is_object($this->name))?str_replace('"', '\"', $this->name):($this->name);
    
        if(isSet($this->views) && !is_null($this->views) && $this->views !== false){
            if(!is_object($this->views)) $val = $this->views;
            else{
                $val = array();
                foreach($this->views as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['views'] = $val;
            //$retArray['views'] = ($simple)?$this->views->getId():$this->views->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->project) && !is_null($this->project) && $this->project !== false){
            if(!is_object($this->project)) $val = $this->project;
            else{
                $val = $this->project->toJSObject();
            }
            $retArray['project'] = $val;
            //$retArray['project'] = ($simple)?$this->project->getId():$this->project->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->parent) && !is_null($this->parent) && $this->parent !== false){
            if(!is_object($this->parent)) $val = $this->parent;
            else{
                $val = $this->parent->toJSObject();
            }
            $retArray['parent'] = $val;
            //$retArray['parent'] = ($simple)?$this->parent->getId():$this->parent->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->childs) && !is_null($this->childs) && $this->childs !== false){
            if(!is_object($this->childs)) $val = $this->childs;
            else{
                $val = array();
                foreach($this->childs as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['childs'] = $val;
            //$retArray['childs'] = ($simple)?$this->childs->getId():$this->childs->toJSObject($simple, $treeVisit);
    
         }
        return ($returnArray)?$retArray:'[VALENCODE]new WorkPackage('.json_encode($retArray).')[VALENCODE]';
    }
    
    public function toArray($simple = false, $treeVisit = false){
        if($this->visitedByToArray){
            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return array(
    //                        'class' => 'WorkPackage',
    //                        'id' => $this->getId()
    //                    );
                    return '[VALENCODE]jsModel["WorkPackage"]["'.$this->getId().'"][VALENCODE]';
                }
            }
            return false;
        }
        if($treeVisit) $this->visitedByToArray = true;
        $retArray = array();
        
        if(isSet($this->umlId) && !is_null($this->umlId)) $retArray['umlId'] = (is_numeric($this->umlId))?$this->umlId:($this->umlId);
        if(isSet($this->functionContent) && !is_null($this->functionContent)) $retArray['functionContent'] = (is_numeric($this->functionContent))?$this->functionContent:($this->functionContent);
        if(isSet($this->controlContent) && !is_null($this->controlContent)) $retArray['controlContent'] = (is_numeric($this->controlContent))?$this->controlContent:($this->controlContent);
        if(isSet($this->type) && !is_null($this->type)) $retArray['type'] = (is_numeric($this->type))?$this->type:($this->type);
        if(isSet($this->umlElement) && !is_null($this->umlElement)) $retArray['umlElement'] = (is_numeric($this->umlElement))?$this->umlElement:($this->umlElement);
        if(isSet($this->functionIsDefault) && !is_null($this->functionIsDefault)) $retArray['functionIsDefault'] = (is_numeric($this->functionIsDefault))?$this->functionIsDefault:($this->functionIsDefault);
        if(isSet($this->controlIsDefault) && !is_null($this->controlIsDefault)) $retArray['controlIsDefault'] = (is_numeric($this->controlIsDefault))?$this->controlIsDefault:($this->controlIsDefault);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (is_numeric($this->name))?$this->name:($this->name);
        if(!$simple && isSet($this->views) && !is_null($this->views) && $this->views !== false){
            if(is_numeric($this->views)) $val = $this->views;
            else{
                $val = array();
                foreach($this->views as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['views'] = $val;
            //$retArray['views'] = ($simple)?$this->views->getId():$this->views->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->project) && !is_null($this->project) && $this->project !== false){
            if(is_numeric($this->project)) $val = $this->project;
            else{
                $val = ($simple)?$this->project->getId():$this->project->toArray($simple, $treeVisit);
            }
            $retArray['project'] = $val;
            //$retArray['project'] = ($simple)?$this->project->getId():$this->project->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->parent) && !is_null($this->parent) && $this->parent !== false){
            if(is_numeric($this->parent)) $val = $this->parent;
            else{
                $val = ($simple)?$this->parent->getId():$this->parent->toArray($simple, $treeVisit);
            }
            $retArray['parent'] = $val;
            //$retArray['parent'] = ($simple)?$this->parent->getId():$this->parent->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->childs) && !is_null($this->childs) && $this->childs !== false){
            if(is_numeric($this->childs)) $val = $this->childs;
            else{
                $val = array();
                foreach($this->childs as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['childs'] = $val;
            //$retArray['childs'] = ($simple)?$this->childs->getId():$this->childs->toArray($simple, $treeVisit);
    
         }
        return $retArray;
    }private $tplVars = array();
    public function addTemplateVar($name, $content){
        $this->tplVars[$name] = $content;
    }
    
    private $ignoreErrors = false;
    /**
     * get Html Template
     * @param string $template template name
     */
    public function getHtml($template = "default"){
        $availableTemplates =  array(
            'default'
            ,'default'
        );
        if(!in_array($template, $availableTemplates)){
            throw new Exception('Template <b>'.$template.'</b> doesn\'t exist for <b>WorkPackage</b>');
        }
        $m = new TemplateEngine;
        
        $templates = array(
            'default' => '' //816
        );
        $tplSrc = $templates[$template];
        
        foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, $this);
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    }
    
    public static function getSubHtml($operationName, $template = 'default'){
    
        
        $availableTemplates =  array(
            'build' => array(
                'default'
                ,'default'
            ),
            'getPath' => array(
                'default'
                ,'default'
            ),
            'loadContentFromSource' => array(
                'default'
                ,'default'
            ),
            'saveToFiles' => array(
                'default'
                ,'default'
            ),
            'getUmlElement' => array(
                'default'
                ,'default'
            ),
            'buildModules' => array(
                'default'
                ,'default'
            ),
            'getWorkPackagesByType' => array(
                'default'
                ,'default'
            ),
            'setUmlElement' => array(
                'default'
                ,'default'
            ),
            'getName' => array(
                'default'
                ,'default'
            )
        );
        if(!isset($availableTemplates[$operationName]) || !in_array($template, $availableTemplates[$operationName])){
            throw new Exception('Template OperationTemplate <b>'.$template.'</b> doesn\'t exist for <b>WorkPackage.'.$operationName.'</b>');
        }
        $m = new TemplateEngine;
        $tplSrc = file_get_contents('FCVAPs/fcv/fcvPackage/WorkPackage/'.$operationName.'/'.$operationName.'.'.$template.'.html');
        /*foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }*/
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, array());
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    
    }
}
