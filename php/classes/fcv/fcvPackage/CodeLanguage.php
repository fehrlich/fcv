<?php
namespace fcv\fcvPackage; ///fcv/fcvPackage/CodeLanguage
    
use Exception;
use mys;
use QueryBuilder;
use fcv\libs\TemplateEngine;
use fcv\libs\Debug;
use fcv\FCVJsObj;


use fcv\fcvPackage\Library;
;
use fcv\fcvPackage\Project;
use fcv\fcvPackage\ProjectCollection;

class CodeLanguage
        
         
        {
    
    //Attributes
    //const LANGID = 'langId';
    //const NAME = 'name';
    //const EXTENSION = 'extension';
    
    //Litearls
    
    //Attributes
    /**
     *
     * @var Integer
     */
     public $langId;
    const FIELD_LANGID = 'langId';
    /**
     *
     * @var String
     */
     public $name;
    const FIELD_NAME = 'name';
    /**
     *
     * @var String
     */
     public $extension;
    const FIELD_EXTENSION = 'extension';
    
    //AssociationsEnds
    
    /**
     * @var Library
     */
    private $lib = false;
    const LIB = 'lib';
    
    /**
     * @var LibraryIncludeCode
     */
    public $libraryIncludeCode = false;
    const FIELD_LIBRARYINCLUDECODE = 'libraryIncludeCode';
    //{/isAssociationClass}}
    
    /**
     * @var ProjectCollection
     */
    private $inProject = false;
    const INPROJECT = 'inProject';
    
    //{/isAssociationClass}}
    //FCV Attributes
    private $apPath = 'FCVAPs/';
    //    public $VIEW = array();
    static $fcvFields = array('langId','name','extension');
    //Todo: Outsource
    protected $loadObject = false;
    private $loadedAttributes = array();
    
    //Methods
    //UserOperations
    private $lastModifiedTime = false;
    private $createdTime = false;
    public function __construct($data = array(),$calledFromChild = false){
        if(is_string($data)) $data = json_decode($data,true);
        if(count($data) > 0){
            if(isSet($data['loadObj']) && $data['loadObj']){
                $this->loadObject = true;
            }
            
        //        if(isset($data['langId']) && !empty($data['langId']))
    
            if(isSet($data['langId'])) $this->langId = $data['langId'];
            if(isSet($data['name'])) $this->name = $data['name'];
            if(isSet($data['extension'])) $this->extension = $data['extension'];
            //associationEnds
            //Regular AssociationEnd LibraryIncludeCode (UMLAssociationClass)
            //AssociationEnd is from AssociationClass
            if(isSet($data['lib'])){
                $this->lib = (is_array($data['lib']))?Library::create($data['lib'],$this->loadObject):$data['lib'];
            }
    
    
            //value from AssociationClass
            if(isSet($data['libraryIncludeCode'])){
                if(!is_object(current($data['libraryIncludeCode']))){
                    $newVal = new LibraryIncludeCodeCollection();
                    foreach($data['libraryIncludeCode'] as $el) $newVal->add(LibraryIncludeCode::create($el,$this->loadObject));
                    $this->libraryIncludeCode = $newVal;
                }else $this->libraryIncludeCode = $data['libraryIncludeCode'];
            }
            if(isSet($data['lib'])){
                $this->lib = (is_array($data['lib']))?Library::create($data['lib'],$this->loadObject):$data['lib'];
            }
            //Regular AssociationEnd  (UMLAssociation)
            /*if(isSet($data['inProject'])){
                if(!is_object(current($data['inProject']))){
                    $newVal = new ProjectCollection();
                    foreach($data['inProject'] as $el) $newVal->add(Project::create($el,$this->loadObject));
                    $this->inProject = $newVal;
                }else  $this->inProject = $data['inProject'];
            }*/
    
            if(isSet($data['inProject'])){
                if(!is_object(current($data['inProject']))){
                    $newVal = new ProjectCollection();
                    foreach($data['inProject'] as $el) $newVal->add(Project::create($el,$this->loadObject));
                    $this->inProject = $newVal;
                }else $this->inProject = $data['inProject'];
            }
    
            //Helper
            if(isSet($data['lastModified'])) $this->lastModifiedTime = $data['lastModified'];
            if(isSet($data['created'])) $this->createdTime = $data['created'];
        }
    
    
    
    
    }
    
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new CodeLanguage($data);
    }
    
    public static function createLoad(){
        $data = array(
            'loadObj' => true
        );
        return new CodeLanguage($data);
    }
    
    public function getLastModifiedTime(){
        return $this->lastModifiedTime;
    }
    
    public function setLastModifiedTime($time){
        $this->lastModifiedTime = $time;
        return $this;
    }
    
    public function hasPrimaryKey(){
        return true;
    }
    
    public function getId(){
        return $this->langId;
    }
    public function setId($id){
    //        CodeLanguage::$loadedInstances[$id] = &$this;
        $this->langId = $id;
        if($this->loadObject) $this->loadedAttributes['langId'] = true;
        return $this;
    }
    public function getPrimaryKeyName(){
        return "langId";
    }
    
    public function addToJs($selectionString = ''){
    //    if(!$calledFromChild && get_class($this) == '' && (!$this->loadObject))
            FCVJsObj::add('', $this, $selectionString);
            return $this;
    }
    public function throwError($msg){
        if(!Debug::getObj()->getThrowErrors()) return "";
        else{
            throw new Exception($msg);
        }
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return Library
     * @throws Exception
     */
    public function getLib($forceLoad = false){
        if(is_null($this->lib) || empty($this->lib)){
            $this->throwError('class CodeLanguage with id '.$this->getId().' has no Attributes lib');
            //throw new Exception('class CodeLanguage with id '.$this->getId().' has no Attributes lib');
        }
        return $this->lib;
    }
    
    public function hasLib($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getLib($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param Library $library
     */
    public function setLib(Library $lib){
        if(!$this->loadObject) $lib->setLanguages($this);
        $this->lib = $lib;
        if($this->loadObject) $this->loadedAttributes['lib'] = true;
        return $this;
    }
    /**
     * @return LibraryIncludeCode
     * @throws Exception
     */
    public function getLibraryIncludeCode($forceLoad = false){
    
        if(empty($this->libraryIncludeCode)){
            $this->throwError('Attribute lib not Found in class lib');
            //throw new Exception('Attribute lib not Found in class lib');
        }
        return $this->libraryIncludeCode;
    }
    
    /**
     * Check if the Association LibraryIncludeCode exists
     */
    public function hasLibraryIncludeCode($forceLoad = false){
        try {
            $this->getLibraryIncludeCode($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    /** a
     * @param LibraryIncludeCode $lib
     * @return CodeLanguage
     */
    public function setLibraryIncludeCode($lib){
        $this->lib = $lib;
        if($this->loadObject) $this->loadedAttributes['lib'] = true;
        return $this;
    }
    /**
     * @param boolean $forceLoad set to true if you want to reload the data
     * @return ProjectCollection
     * @throws Exception
     */
    public function getInProject($forceLoad = false){
        if(is_null($this->inProject) || empty($this->inProject)){
            $this->throwError('class CodeLanguage with id '.$this->getId().' has no Attributes inProject');
            //throw new Exception('class CodeLanguage with id '.$this->getId().' has no Attributes inProject');
        }
        return $this->inProject;
    }
    
    public function hasInProject($forceLoad = false){
    
        $throwErrors = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(true);
        try {
            $this->getInProject($forceLoad);
        } catch (Exception $exc) {
            return false;
        }
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }
    
    /**
     * @param ProjectCollection $project
     */
    public function setInProject(ProjectCollection $inProject){
        $this->inProject = $inProject;
        if($this->loadObject) $this->loadedAttributes['inProject'] = true;
        return $this;
    }
    
    /**
     * @param CodeLanguage $project
     */
    public function addProject(Project $project){
        if(!$project->hasPrimaryKey()){
            if(!$this->loadObject) $project->setParent($this, count($this->inProject));
        }
        if($this->loadObject){
            $this->loadedAttributes['inProject'] = true;
            if(!$this->inProject) $this->inProject = new ProjectCollection();
        }
        elseif(!$this->inProject){
            if(!$this->hasInProject()) $this->inProject = new ProjectCollection();
        }
        $this->inProject->add($project);
        return $this;
    }
    
    //Getter & Setter
    
    public function get($name){
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            return $this->$name->get($subName);
        }else return $this->$name;
    }
    
    public function has($name){
        if(strpos('.', $name) !== false){
            $spl = explode('.', $name, 2);
            return $this->get($spl[0])->has($spl[1]);
        }
        return (property_exists($this, $name));
    }
    
    public function set($name, $value){
        $possibleFields = array(
        'langId',
        'name',
        'extension',
        'lib',
        'libraryIncludeCode',
        'inProject',
        );
        if(strpos($name, '.')){
            $spl = explode('.', $name, 2);
            $name = $spl[0];
            $subName = $spl[1];
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class CodeLanguage');
            if(!$this->$name){
                if(false);
                elseif($name == 'lib'){
                    $this->lib = Library::create(array(),$this->loadObject)->set($subName,$value);
                    
                    if($this->loadObject) $this->loadedAttributes['lib'] = true;
                }
                elseif($name == 'libraryIncludeCode'){
                    $this->libraryIncludeCode = LibraryIncludeCode::create(array(),$this->loadObject)->set($subName,$value);
                    if($this->loadObject) $this->loadedAttributes['libraryIncludeCode'] = true;
                }
                elseif($name == 'inProject'){
                    $this->inProject = Project::create(array(),$this->loadObject)->set($subName,$value);
                    $this->inProject = array($this->inProject->set($subName,$value));
                    if($this->loadObject) $this->loadedAttributes['inProject'] = true;
                }
                else die('Error');
            }else $this->$name->set($subName,$value);
        }else{
            if(!in_array($name, $possibleFields)) die('The Field "'. $name.'" doesnt exists in the class CodeLanguage');
            $this->$name = $value;
            if($this->loadObject) $this->loadedAttributes[$name] = true;
        }
        return $this;
    }
    
    /**
     *
     * @return Integer
     * @throws Exception
     */
     public function getLangId(){
        return $this->langId;
     }
    
     public function hasLangId(){
        try {
            $this->getLangId();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param Integer $langId
     * @return CodeLanguage
     */
     public function setLangId($langId){
        ;
        $this->langId = $langId;
        if($this->loadObject) $this->loadedAttributes['langId'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getName(){
        return $this->name;
     }
    
     public function hasName(){
        try {
            $this->getName();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $name
     * @return CodeLanguage
     */
     public function setName($name){
        ;
        $this->name = $name;
        if($this->loadObject) $this->loadedAttributes['name'] = true;
        return $this;
     }
    /**
     *
     * @return String
     * @throws Exception
     */
     public function getExtension(){
        return $this->extension;
     }
    
     public function hasExtension(){
        try {
            $this->getExtension();
        } catch (Exception $exc) {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     * @param String $extension
     * @return CodeLanguage
     */
     public function setExtension($extension){
        ;
        $this->extension = $extension;
        if($this->loadObject) $this->loadedAttributes['extension'] = true;
        return $this;
     }
    protected $visitedByToArray = false;
    public function toJSObject($returnArray = false){
        if($this->loadObject) return false;
        if($this->visitedByToArray){
    //            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return '[VALENCODE]jsModel["CodeLanguage.'.$this->getId().'"][VALENCODE]';
                    return array(
                        'class' => 'CodeLanguage',
                        'id' => $this->getId()
                    );
                }
                return false;
    //            }
    //            return array(
    //                'class' => 'CodeLanguage',
    //                'id' => rand()
    //            );
        }
        $retArray = array();
        
        $this->visitedByToArray = true;
        
        if(isSet($this->langId) && !is_null($this->langId)) $retArray['langId'] = (!is_object($this->langId))?str_replace('"', '\"', $this->langId):($this->langId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (!is_object($this->name))?str_replace('"', '\"', $this->name):($this->name);
        if(isSet($this->extension) && !is_null($this->extension)) $retArray['extension'] = (!is_object($this->extension))?str_replace('"', '\"', $this->extension):($this->extension);
    
        if(isSet($this->lib) && !is_null($this->lib) && $this->lib !== false){
            if(!is_object($this->lib)) $val = $this->lib;
            else{
                $val = $this->lib->toJSObject();
            }
            $retArray['lib'] = $val;
            //$retArray['lib'] = ($simple)?$this->lib->getId():$this->lib->toJSObject($simple, $treeVisit);
    
         }
        if(isSet($this->inProject) && !is_null($this->inProject) && $this->inProject !== false){
            if(!is_object($this->inProject)) $val = $this->inProject;
            else{
                $val = array();
                foreach($this->inProject as $o){
                    $val[] = $o->toJSObject();
                }            
            }
            $retArray['inProject'] = $val;
            //$retArray['inProject'] = ($simple)?$this->inProject->getId():$this->inProject->toJSObject($simple, $treeVisit);
    
         }
        return ($returnArray)?$retArray:'[VALENCODE]new CodeLanguage('.json_encode($retArray).')[VALENCODE]';
    }
    
    public function toArray($simple = false, $treeVisit = false){
        if($this->visitedByToArray){
            if($this->hasPrimaryKey()){
                $id = $this->getId();
                if(!empty($id)){
    //                    return array(
    //                        'class' => 'CodeLanguage',
    //                        'id' => $this->getId()
    //                    );
                    return '[VALENCODE]jsModel["CodeLanguage"]["'.$this->getId().'"][VALENCODE]';
                }
            }
            return false;
        }
        if($treeVisit) $this->visitedByToArray = true;
        $retArray = array();
        
        if(isSet($this->langId) && !is_null($this->langId)) $retArray['langId'] = (is_numeric($this->langId))?$this->langId:($this->langId);
        if(isSet($this->name) && !is_null($this->name)) $retArray['name'] = (is_numeric($this->name))?$this->name:($this->name);
        if(isSet($this->extension) && !is_null($this->extension)) $retArray['extension'] = (is_numeric($this->extension))?$this->extension:($this->extension);
        if(!$simple && isSet($this->lib) && !is_null($this->lib) && $this->lib !== false){
            if(is_numeric($this->lib)) $val = $this->lib;
            else{
                $val = ($simple)?$this->lib->getId():$this->lib->toArray($simple, $treeVisit);
            }
            $retArray['lib'] = $val;
            //$retArray['lib'] = ($simple)?$this->lib->getId():$this->lib->toArray($simple, $treeVisit);
    
         }
        if(!$simple && isSet($this->inProject) && !is_null($this->inProject) && $this->inProject !== false){
            if(is_numeric($this->inProject)) $val = $this->inProject;
            else{
                $val = array();
                foreach($this->inProject as $o){
                    $val[] = ($simple)?$o->getId():$o->toArray($simple, $treeVisit);
                }            
            }
            $retArray['inProject'] = $val;
            //$retArray['inProject'] = ($simple)?$this->inProject->getId():$this->inProject->toArray($simple, $treeVisit);
    
         }
        return $retArray;
    }private $tplVars = array();
    public function addTemplateVar($name, $content){
        $this->tplVars[$name] = $content;
    }
    
    private $ignoreErrors = false;
    /**
     * get Html Template
     * @param string $template template name
     */
    public function getHtml($template = "default"){
        $availableTemplates =  array(
            'default'
            ,'default'
        );
        if(!in_array($template, $availableTemplates)){
            throw new Exception('Template <b>'.$template.'</b> doesn\'t exist for <b>CodeLanguage</b>');
        }
        $m = new TemplateEngine;
        
        $templates = array(
            'default' => '' //803
        );
        $tplSrc = $templates[$template];
        
        foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, $this);
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    }
    
    public static function getSubHtml($operationName, $template = 'default'){
    
        
        $availableTemplates =  array(
        );
        if(!isset($availableTemplates[$operationName]) || !in_array($template, $availableTemplates[$operationName])){
            throw new Exception('Template OperationTemplate <b>'.$template.'</b> doesn\'t exist for <b>CodeLanguage.'.$operationName.'</b>');
        }
        $m = new TemplateEngine;
        $tplSrc = file_get_contents('FCVAPs/fcv/fcvPackage/CodeLanguage/'.$operationName.'/'.$operationName.'.'.$template.'.html');
        /*foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }*/
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = $m->render($tplSrc, array());
        Debug::getObj()->setThrowErrors($throwBefore);
        
        return $htmlSrc; 
    
    }
}
