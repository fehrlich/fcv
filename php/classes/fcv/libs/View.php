<?php
class View {
    private $page = false;
    private $frames = array();
    
    static public function create(){
        return new View();
    }
    
    public function getPage() {
        return $this->page;
    }

    public function setPage($page) {
        $this->page = Page::create()->setName($page);
        return $this;
    }
    public function addFrame($frameName, $frame){
        $this->frames[] = Frame::create()->setVarName($frameName)->setFrameName($frame);
        return $this;
    }
    
    public function getRoute(){
        $frameRoute = '';
        foreach($this->frames as $frame){
            $frameRoute .= '&';
//            $frameRoute .= ($frameRoute == '')?'?':'&amp;';
            $frameRoute .= $frame->getVarName().'='.$frame->getFrameName();
        }
        return 'index.php?page='.$this->page->getName().$frameRoute; 
   }
   static function parseQueryString(){
//       $str = $_SERVER['R']
       $view = View::create();
       $view->setPage('Wrapper'); //Todo get Default
       foreach($_GET as $name => $value){
           if($name == 'page') $view->setPage($value);
           else{
               $view->addFrame($name, $value);
           }
       }
       return $view;
   }
   
   static public function getCurrent(){
       return View::parseQueryString();
   }
   
   private $vars = array();
   public function addVars($name, $content){
       $this->vars[$name] = $content;
   }
   
    public function getHtml(){
//        if(!$this->page) $view = $this->parseQueryString();
//        else $view = $this;
        $className = $this->page->getName();
        $class = new $className();
//        $class = $className::create();
        foreach($this->frames as $frame){
            $varName = $frame->getVarName();
            $valClassName = $frame->getFrameName();
            if(class_exists($valClassName)){
                $tmp = new $valClassName();
                $class->$varName = $tmp->getHtml();
            }
//            if(class_exists($valClassName)) $class->$varName = $valClassName::create()->getHtml();
        }
        foreach($this->vars as $name => $content){
            $class->$name = $content;            
        }
        return $class->getHtml();
    }
}