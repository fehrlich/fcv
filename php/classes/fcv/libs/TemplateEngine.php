<?php
namespace fcv\libs; 
class TemplateEngine extends \Mustache_Engine {
    static $tplCount = 0;
    public function __construct(array $options = array()) {
        $options['escape'] = function($value){
            if(is_array($value)){
                d($value);
                d($this);
                
                d($this->templates);
                d($this->getTemplateClassName($source));
            //    d('ERROR');
            //    exit();
            }
            return is_null($value) ? '' :htmlspecialchars($value, ENT_COMPAT, 'UTF-8');
        };
//        $options['cache'] = '.fcv/tpl.compiled';
//        require($_SERVER['DOCUMENT_ROOT'].'/FCV/php/libs/Mustache_2.3.1/Mustache/Logger/StreamLogger.php');
        $options['logger'] = new \Mustache_Logger_StreamLogger('php://stderr');
        parent::__construct($options);
//        $this->addHelper('test', function($template, $context, $args, $source){
//           return "ASDF: ".print_r($args,1);
//        });
//        $this->set
        $this->addHelper('list', function($content, $template, $args){
            $arrayVar = $args[0];
            $array = $template->getContext()->find($arrayVar);
            $newArray = array();
            $lastId = is_array($array) ? count($array)-1 : 0;
            if($array){
                foreach($array as $i => $value){
                    $newArray[] = array(
                        'index' => $i,
                        'even' => ($i%2==0),
                        'first' => ($i==0),
                        'last' => ($i==$lastId),
                        'value' => $value
                    );
                }
            }
//            $template->getContext()->pop();
            $template->getContext()->push(array(
                $arrayVar => $newArray
            ));
            return $template->render('{{#'.$arrayVar.'}}'.$content.'{{/'.$arrayVar.'}}');
        });
        
        $this->addHelper('indent', function($content, $template, $args){
            $insertNumberOfTabs = $args[0];
            $space = str_pad(" ", $insertNumberOfTabs*4);
            
//            if($insertNumberOfTabs == 2){
//                d($content);
//            }
            $bContent = $content;
            $content = $template->render($content);
//            d($bContent);
//            if($bContent == '{{&getFunctionContent}}'){
////            if($template->getContext()->last()->getId() == '-64--88-2-107-6776b680:142eb8486fb:-8000:0000000000000FED'){
//                d($bContent);
//                d($content);
////                d($template);
//                d($template->getContext()->last());
//                d(1);
//                exit();
//            }//elseif($insertNumberOfTabs == 2) d($template->getContext()->last());
            $content = str_replace(array("\r\n", "\n"), array("\n", "\n".$space), $content);
            return $content;
        }); 
        
        $this->addHelper('replace', function($content, $template, $args){
            $content = $template->render($content);
            $content = str_replace($args[0], $args[1], $content);
            return $content;
        });
        $this->addHelper('pathToNamespace', function($content, $template, $args){
            $arg = (isset($args[0]))?$args[0]:false;
            $content = $template->render($content);
            $path = explode('/', substr($content,1));
            if($arg == 2){
//                d("YEAH");
                $replaceWith = '\\\\';
            }else{
                $replaceWith = '\\';
            }
            array_pop($path);
            $content = implode($replaceWith, $path);
            return $content;
        });
        $this->addHelper('substr', function($content, $template, $args){
            $from = $args[0];
            $length = (isset($args[1]))?$args[1]:null;
//            d($args);
            $content = $template->render($content);
//            d(array(
//                'str' => $content,
//                'from' => $from,
//                'length' => $length,
//                '=' => substr($content, $from)
//            ));
            $content = substr($content, $from);
//            d($content);
            return $content;
        });
        $this->addHelper('includeTemplate', function($content, $template, $args){
            $templateDepth = $args[0];
            $context = $template->getContext();
            $obj = $context->last();
            $func = str_replace(array('{{', '}}', '&'), '', $content);
            $code = $obj->$func();
            $tpl = str_replace("'", "\'", $code);
            $pad = str_pad('', $templateDepth, "\\");
            $tpl = str_replace('{{', "{".$pad."{", $tpl);
            return $tpl;
        });
        $this->addHelper('jsMultilineString', function($content, $template, $args){
            $content = $template->render($content);
            $content = str_replace("\r", "", $content);
            $content = str_replace("\n", "\\\n", $content);
            return $content;
        });
        
        $this->addHelper('addSimpleSlashes', function($content, $template, $args){
            $content = $template->render($content);
            $content = str_replace("'", "\'", $content);
            return $content;
        });
        $this->addHelper('addDoubleSlashes', function($content, $template, $args){
            $content = $template->render($content);
            $content = str_replace('"', '\"', $content);
            return $content;
        });
//        $this->addHelper('dontParse', function($content){
//            d($content);
//            return $content;
//        });
        
        $this->addHelper('uniqueLines', function($content, $template, $args){
            $content = $template->render($content);
            $lines = explode("\n", substr($content,1));
            $lines = array_map('trim', $lines);
            $lines = array_unique($lines);
            $content = implode("\n", $lines);
            return $content;
        });
        
        $this->addHelper('eval', function($content, $template, $args){
            $content = $template->render($content);
            return eval($content);
        });
        
        
        $this->addHelper('singleParse', function($content, $template, $args){
            $func = str_replace(array('{{','}}', '&'), '', $content);
            $context = $template->getContext();
            $obj = $context->last();
            $content = $obj->$func();
            return str_replace('{{', "{\\\\{", $content);
        });
        
        $this->addHelper('', function($content, $template, $args){
            $content = $template->render($content);
            $matches = array();
            if(substr($content, 0, 5) == '<?php'){
                $content = substr($content, 5);
            }
            elseif(substr($content, 0, 2) == '<?'){
                $content = substr($content, 2);
            }
            
            //TODO: RANZIGE NAMESPACES
            preg_match_all('/(\s*use [A-z0-9\\\\]+\s*;)/m', $content, $matches);
            /*$content = str_replace(array('<?php','?>','<?'), '', $content);*/
            $addUses = array();
            foreach($matches[1] as $useMatch){
                $content = str_replace($useMatch, '', $content);
                $addUses[] = trim($useMatch);
            }            
            
            if(strpos($content, '//EndOfCodeGeneration') !== false){
                $spl = explode('//EndOfCodeGeneration', $content);
                return $spl[1];                
            }else{
                //Deprecated
//                $content = $template->render($content);
                return $content;
            }
        });
        
        $this->addHelper('includeCode', function($content, $template, $args){
            $content = $template->render($content);
            if(substr($content, 0, 5) == '<?php'){
                $content = substr($content, 5);
            }
            elseif(substr($content, 0, 2) == '<?'){
                $content = substr($content, 2);
            }

            $matches = array();
            //TODO: RANZIGE NAMESPACES
            preg_match_all('/(\s*use [A-z0-9\\\\]+\s*;)/m', $content, $matches);
            $addUses = array();
            foreach($matches[1] as $useMatch){
                $content = str_replace($useMatch, '', $content);
                $addUses[] = trim($useMatch);
            }            
            
            if(strpos($content, '//EndOfCodeGeneration') !== false){
                $spl = explode('//EndOfCodeGeneration', $content);
                return $spl[1];
            }else{
                //Deprecated
//                $content = $template->render($content);
                return $content;
            }
        });
        
        $this->addHelper('removeSelfNamespace', function($content, $template, $args){
            $append = isset($args[0]) ? $args[0] : '';
            $useString = $template->render($content);
            $lines = explode("\n", $useString);
            $lines = array_map('trim', $lines);
            $umlClass = $template->getContext()->last();
            $filterdLines = [];
            $path = substr(str_replace('/', '\\', $umlClass->getNamePath()), 1) . $append;
            foreach($lines as $line){
                if($line != 'use ' . $path . ';') {
                    $filterdLines[] = $line;
                }
            }
            return implode("\n", $filterdLines);
        });
        $this->addHelper('addUses', function($content, $template, $args){
            //TODO: RANZIGE NAMESPACES
            $dontIncludeUses = array(
                'fcv\libs\Debug',
                'Exception',
                'mys',
                'QueryBuilder',
                'fcv\view\TemplateEngine',
                'fcv\libs\Debug',
                'fcv\FCVJsObj'
            );
            $addUses = [];
            $wpId = trim($template->render($content));
            $class = $template->getContext()->last();
            $wp = $class->getWorkPackage();
            $wps = $wp->getChilds()->getArray();

            $wps[] = $wp;

            foreach($wps as $wp){
                $matches = array();
                $content = $wp->getFunctionContent();
                preg_match_all('/\s*use ([A-z0-9\\\\]+)\s*;/m', $content, $matches);
                foreach($matches[1] as $useMatch){
                    if(!in_array($useMatch, $dontIncludeUses)) $addUses[] = 'use '.$useMatch.';';
                }
                
                foreach($wp->getViews() as $view){
                    
                    $matches = array();
                    $content = $view->getPhpContent();
                    preg_match_all('/\s*use ([A-z0-9\\\\]+)\s*;/m', $content, $matches);
                    foreach($matches[1] as $useMatch){
                        if(!in_array($useMatch, $dontIncludeUses)) $addUses[] = 'use '.$useMatch.';';
                    }
                }
            }
            
            return implode("\n", $addUses);
        });
        
        $this->addHelper('tpl', function($content, $template, $args){
            
            return 'dsaf';
            $content = $template->render($content);
            $spl = explode('//EndOfCodeGeneration');
            
            return $spl[1];
        });
    }
    
    
    public function render($template, $data = array()) {
        TemplateEngine::$tplCount++;
        $ret = parent::render($template, $data);
        $ret = str_replace("{\\{", '{{', $ret);
        $ret = str_replace("{\\\\{", '{\\{', $ret);
        return $ret; 
    }

    static function load(){
    }
}