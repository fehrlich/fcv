<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Debug
 *
 * @author XX
 */
class Debug {
    /**
     *
     * @var Debug
     */
    private static $handle = false;
    private $enabled = true;
    /**
     *
     * @return Debug
     */
    static function getObj(){
        Debug::$handle = (Debug::$handle !== false)?Debug::$handle:new Debug();
        Debug::$handle->enabled = Debug::$handle->enabled && (isset($_GET['debug']) && $_GET['debug'] = 1);
        return  Debug::$handle;
    }

    private $entries = array();
    private $lastTime = false;
    public function setDebugPoint($name){
        if(!$this->enabled) return;
        $element = new DebugElement($name);
        if($this->lastTime){
            $element->timeDiff = $element->time-$this->lastTime;
        }
        $this->lastTime = $element->time;
        $this->entries[] = $element;
    }

    private $debugBar = null;

    public function getDebugBar() {
        return $this->debugBar;
    }

    public function setDebugBar($debugBar) {
        $this->debugBar = $debugBar;
    }

    public function startMeasure($name, $desc){
        echo $name;
//        $this->debugBar['time']->startMeasure($name, $desc);
    }
    public function stopMeasure($name){
//        $this->debugBar['time']->stopMeasure($name);
    }
    public function info($info){
//        $this->debugBar["messages"]->addMessage($info);
    }



    public function dump(){
        if(!$this->enabled) return;
//        var_dump($this->entries);
        new dBug($this->entries);
    }
}

class DebugElement{
    public $name;
    public $time;
    public $trace;
    public $timeDiff;

    function __construct($name) {
        $this->name = $name;
        $this->time = microtime(true);
//        $this->trace = print_r(debug_backtrace(), 1);
//        $this->trace = print_r(debug_backtrace(), 1);
    }

}

?>
