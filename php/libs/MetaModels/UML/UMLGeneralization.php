<?php
class UMLGeneralization extends UMLProperties {
//    private $pseudoClass = false;
    /**
     *
     * @var UMLClass
     */
    protected $parentClass;
    /**
     *
     * @var UMLClass
     */
    protected $childClass;
    
    private $nextClassIsChild = true;
    
    
    function getLinkedElements(){
        $addLinkedElements = array('parentClass','childClass');
        return array_merge(parent::getLinkedElements(),$addLinkedElements);
    }
    
    /**
     * 
     * @param UMLClass $class
     */
    function addClass($class){
        if($this->nextClassIsChild){
            $this->childClass = $class;
//            $class->addGeneralizes($class);
        }else{
            $this->parentClass = $class;
//            $class->addSpecializes($class);
        }
        $this->updateClasses();
    }
    
    function addEnumeration($class){
        $this->addClass($class);
    }
    
    public function setNextClassIsChild($nextClassIsChild) {
        $this->nextClassIsChild = $nextClassIsChild;
    }
    
    function toArray() {
        $arr = parent::toArray();
        if(!$this->pseudoClass){
            if($this->parentClass) $arr['parentClass'] = $this->parentClass->getName();
            if($this->childClass) $arr['childClass'] = $this->childClass->getName();
        }
        if($this->pseudoClass) $arr['pseudo'] = 'TRUE';
        return $arr;
    }
    public function getParentClass() {
        return $this->parentClass;
    }
    public function getChildClass() {
        return $this->childClass;
    }
    public function setParentClass($parentClass) {
        $this->parentClass = $parentClass;
        $this->updateClasses();
    }
    

    public function setChildClass($childClass) {
        $this->childClass = $childClass;
        $this->updateClasses();
    }
    public function updateClasses(){        
        if($this->childClass && !$this->childClass->isPseudoClass() && $this->parentClass && !$this->parentClass->isPseudoClass()){
            $this->parentClass->addGeneralizes($this->childClass);
            $this->childClass->addSpecializes($this->parentClass);
        }else{
//            if($this->childClass) new dBug($this->childClass->toArray());
//            else 'No child';
//            if($this->parentClass) new dBug($this->parentClass->toArray());
//            else 'No Parent';
//            echo '<br>';
            
        }
    }
    
    public function afterLinking() {
        parent::afterLinking();
        $this->updateClasses();
    }


//    public function linkMyObjects(&$realObjects) {
//        parent::linkMyObjects($realObjects);
//        $c = $this->parentClass;        
//        if($c->isPseudoClass()){
//            if(isSet($realObjects[$c->getId ()])) $this->setParentClass ($realObjects[$c->getId ()]);
////            else echo "LinkingError: ".$c->getId ().'<br>';
//        }
//        $c = $this->childClass;
//        if($c->isPseudoClass()){
//            if(isSet($realObjects[$c->getId ()])) $this->setChildClass($realObjects[$c->getId ()]);
////            else echo "LinkingError: ".$c->getId ().'<br>';
//        }
//    }
}

?>
