<?php

class UMLClass extends UMLProperties {

    /**
     *
     * @var UMLOperation
     */
    protected $operations = array();
    /**
     *
     * @var UMLDataType
     */
    protected $attributes = array();


    /**
     *
     * @var UMLClass[]
     */
    protected $generalizes = array();
    /**
     *
     * @var UMLClass[]
     */
    protected $specializes = array();
    /**
     *
     * @var UMLClass[]
     */
    protected $realizes = array();
    /**
     *
     * @var UMLClass[]
     */
    protected $implements = array();

    /**
     *
     * @var UMLAssociationEnd[]
     */
    protected $associationEnds = array();

    /**
     *
     * @var UMLComments
     */
//    protected $comments = array();

    protected $constructor = false;


//    private $attrCount = 0;

    function getLinkedElements(){
        $addLinkedElements = array('operations','attributes','generalizes','specializes','realizes','implements','associationEnds','comments');
        return array_merge(parent::getLinkedElements(),$addLinkedElements);
    }
    function getRealElements(){
        $addLinkedElements = array('operations','attributes');
        return array_merge(parent::getRealElements(),$addLinkedElements);
    }


    function addAssociationEnd($associationEnd){
//        $associationEnd->first = ($this->attrCount == 0);
        $this->associationEnds[] = $associationEnd;
//        $this->attrCount++;
    }

    public function __construct($id, $name, $parent) {
//        if ($name == false)
//            $this->pseudoClass = true;
        parent::__construct($id, $name, $parent);
    }

    function addAttribute($attribute) {
//        $attribute->first = ($this->attrCount == 0);
        $this->attributes[] = $attribute;
//        $this->attrCount++;
    }

    function addOperation($operation) {
        if($operation->getName() == $this->getName()){
            $operation->setConstructor = true;
            $this->constructor = $operation;
        }else{
            $this->operations[] = $operation;
        }
    }

    public function getConstructor() {
        return $this->constructor;
    }

    public function addGeneralizes($class) {
        $this->generalizes[] = $class;
    }

    public function addSpecializes($class) {
        $this->specializes[] = $class;
    }

    public function addRealizes($class) {
        $this->realizes[] = $class;
    }
    public function addImplements($class) {
        $this->implements[] = $class;
//        new dBug($this->toArray());
    }
    public function addComment($c){
        $this->comments[] = $c;
    }

    public function addGeneralization($class) {
//        WORKAROUND
    }
    public function addAbstraction($class) {
//        WORKAROUND
    }

    public function toArray() {
        $array = parent::toArray();
        $array['isDBTable'] = $this->isTable();
        $array['attributes'] = array();
        $array['opterations'] = array();
        $array['generalizes'] = array();
        $array['specializes'] = array();
        $array['realizes'] = array();
        $array['implements'] = array();
        if(count($this->associationEnds) > 0) $array['associationEnds'] = array();
        foreach ($this->attributes as $attr) {
            $array['attributes'][] = $attr->toArray();
        }
        foreach ($this->operations as $op) {
            $array['opterations'][] = $op->toArray();
        }
        foreach ($this->generalizes as $op) {
            $array['generalizes'][] = $op->getName();
        }
        foreach ($this->specializes as $op) {
            $array['specializes'][] = $op->getName();
        }
        foreach ($this->realizes as $op) {
            $array['realizes'][] = $op->getName();
        }
        foreach ($this->implements as $op) {
            $array['implements'][] = $op->getName();
        }
        foreach ($this->associationEnds as $op) {
            $array['associationEnds'][] = $op->toArray();
        }
        if($this->pseudoClass) $array['pseudo'] = 'TRUE';
        $array['usesTypeParser'] = $this->usesTypeParser();
//        $array['PrimaryKey'] = $this->getPrimaryKeyAttribute()->getName();
        return $array;
    }

    public function hasOperation($opName){
        foreach($this->operations as $op){
            if($opName == $op->getName()) return true;
        }
        return false;
    }
    
    public function hasAttribute($opName){
        foreach($this->attributes as $op){
            if($opName == $op->getName()) return true;
        }
        return false;
    }

    public function getSaveOperation(){
        $defaultName = 'save';
        if($this->hasOperation($defaultName)) $defaultName = 'FCV'.$defaultName;
        return $defaultName;
    }

//    public function getLinkObjects() {
//        $objs = parent::getLinkObjects();
//        foreach($this->associationEnds as $end){
//            $objs = array_merge($objs, $end->getLinkObjects());
//        }
//        return $objs;
//    }
//
//    public function linkMyObjects($realObjects) {
//        parent::linkMyObjects($realObjects);
//        foreach($this->associationEnds as $end){
//            if($end->linkMyObjects($realObjects));
//        }
//        foreach($this->attributes as $attr){
//            if($attr->linkMyObjects($realObjects));
//        }
//        foreach($this->operations as $attr){
//            if($attr->linkMyObjects($realObjects));
//        }
//    }

    public function getOperations() {
        return $this->operations;
    }

    public function hasConstructor(){
        if($this->isPageOrFrame()) return true;
//        else{
//            foreach($this->operations as $op){
//                if($op->isConstructor()) return true;
//            }
//        }
        return ($this->constructor !== false);
    }

    public function getAttributes() {
        if(isSet($this->attributes[0])) $this->attributes[0]->first = true;
        return $this->attributes;
    }

    public function getAllConnectedDataTypes(){
        return array_merge($this->attributes,$this->associationEnds);
    }

    public function getGeneralizes($withStereoType = false){
        if(count($this->generalizes) == 0) return $this->generalizes;
        $ret = array();
        
        foreach($this->generalizes as $s){
            $hasStereoType = !$withStereoType || $s->hasStereoType($withStereoType);
            if($hasStereoType && $s->isClient()) $ret[] = $s;
            else $ret = array_merge ($ret, $s->getGeneralizes($withStereoType));
        }
        
        return $ret;
    }
    
    public function hasSpecializes(){
        return count($this->getSpecializes()) > 0;
    }
    /**
     *
     * @return UMLClass[]
     */
    public function getSpecializes($withStereoType = false) {
        if(!$withStereoType ||  count($this->specializes) == 0) return $this->specializes;
        $ret = array();
        
        foreach($this->specializes as $s){
            if($s->hasStereoType($withStereoType)) $ret[] = $s;
            else $ret = array_merge ($ret, $s->getSpecializes($withStereoType));
        }
        
        return $ret;
    }
    
    public function getClientSpecializes($withStereoType = false){
        if(count($this->specializes) == 0) return $this->specializes;
        $ret = array();
        
        foreach($this->specializes as $s){
            $hasStereoType = !$withStereoType || $s->hasStereoType($withStereoType);
            if($hasStereoType && $s->isClient()) $ret[] = $s;
            else $ret = array_merge ($ret, $s->getClientSpecializes($withStereoType));
        }
        
        return $ret;
    }
    
    public function getRealizes($x = null) {
        return $this->realizes;
    }

    public function getImplements() {
        return $this->implements;
    }
    public function getAssociationEnds() {
        return $this->associationEnds;
    }
    
    public function getSpecializesRecursive(){
        $ret = [];
        foreach($this->specializes as $class){
            $ret = array_merge($ret, $class->getSpecializesRecursive());
        }
        $ret = array_merge($ret, $this->specializes);
        return $ret;
    }
    
    /**
     *
     * @return UMLDataType
     */
    public function getPrimaryKeyAttribute(){
        foreach($this->attributes as $attr){
            if($attr->isPrimaryKey()) return $attr;
        }
        foreach($this->specializes as $class){
            $ret = $class->getPrimaryKeyAttribute();
            if($ret) return $ret;
        }
        return false;
    }
    
    public function getPrimaryKeyAttributes(){
        $ret = array();
        foreach($this->attributes as $attr){
            if($attr->isPrimaryKey()) $ret[] = $attr;
        }
        if(count($ret) != 0) return $ret;
        
        foreach($this->specializes as $class){
            $ret = $class->getPrimaryKeyAttributes();
            if($ret) return $ret;
        }            
        
        if($this->isAssociationClass()){
            foreach($this->getAssociationEnds() as $end){
                $ret[] = $end->getType();
            }
        }   
        
        return (count($ret) != 0)?$ret:[];
    }
    
    
    public function hasPrimaryKeyAttribute(){
        return ($this->getPrimaryKeyAttribute() !== false);
    }
    public function hasPrimaryKeyAttributes(){
        return !empty($this->getPrimaryKeyAttributes());
    }
    public function hasMultiplePrimaryKeyAttributes(){
        return (count($this->getPrimaryKeyAttributes()) > 1);
    }
    public function countMultiplePrimaryKeyAttributes(){
        return count($this->getPrimaryKeyAttributes());
    }
    
    


    public function hasForeignKeyAttributes(){
        return (count($this->getForeignKeyAttributes()) > 0);
    }

    /**
     *
     * @return UMLDataType[]
     */
    public function getForeignKeyAttributes(){
        $ret = array();
        foreach($this->attributes as $attr){
            if($attr->isForeignKey()) $ret[] = $attr;
        }
        foreach($this->associationEnds as $assocEnd){
//            if(!$assocEnd->isCardinalityZero() && (Settings::getObj()->tradeAllAssociationEndsAsForeignKeys || $assocEnd->isForeignKey()))
            if(!$assocEnd->isCardinalityZero() && ($assocEnd->isForeignKey()))
                $ret[] = $assocEnd;
        }
        return $ret;
    }

    public function isInterface(){
        return (count($this->realizes) >  0);
    }
    public function implementsClasses(){
        return (count($this->implements) >  0);
    }
    public function specializeClasses(){
        return (count($this->specializes) >  0);
    }
    
    public function usesTypeParser($firstLevel = true){
        if($this->hasStereoType('typeParser') && !$firstLevel) return true;
        foreach($this->implements as $impl){
            if($impl->usesTypeParser(false)) return true;
        }
        foreach($this->specializes as $impl){
            if($impl->usesTypeParser(false)) return true;
        }
        return false;
    }
    public function isEnum(){
        return ($this->debugType == 'UMLEnumeration');
    }
    public function isTable(){
        return $this->hasStereoType('TABLE');
    }
    
    public function isTableOrParent(){
        return $this->isTable() || count($this->getSpecializes('TABLE')) > 0;
    }
    
    public function getFirstTableClass(){
        if($this->isTable()) return $this;
        $spec = $this->getSpecializes('TABLE');
        return array_shift($spec);
    }
    
    public function getLiterals (){
        return false;
    }
    public function isAssociationClass  (){
        return false;
    }
    //Todo: does not belong here because of the use of collections
    public function getRequired(){
        $ret = array();
//        foreach($this->getSpecializes() as $r){
//            $ret[] = array(
//                'multi' => false,
//                'req' => $r
//            );
//        }
        foreach($this->getImplements() as $r){
            
            if($r->hasPath()) $ret[] = array(
                'multi' => false,
                'req' => $r
            );
        }
        foreach($this->getAssociationEnds() as $r){
            if($r->getConnectionClass()->hasPath()) $ret[] = array(
                'multi' => !$r->isCardinalitySingle(),
                'req' => $r->getConnectionClass()
            );
            if($r->getParent()->isAssociationClass()){
                if($r->getParent()->hasPath()) $ret[] = array(
                    'multi' => false,
                    'req' => $r->getParent()
                );
            }
            
        }
        foreach($this->getAttributes() as $r){
            if($r->getType()->isClass()){
                if($r->getType()->hasPath()) $ret[] = array(
                    'multi' => !$r->isCardinalitySingle(),
                    'req' => $r->getType()
                );
            }
            
        }
        return $ret;
    }
    
    function getAllClassProperties() {
        
    }
    
    function hasInheritedProperty($hasProperty){
        if($this->hasProperty($hasProperty)) return true;
        foreach($this->implements as $impl){
            if($impl->hasInheritedProperty($hasProperty)) return true;
        }
        foreach($this->specializes as $impl){
            if($impl->hasInheritedProperty($hasProperty)) return true;
        }
        
        return false;
    }
    
    function getInheritedProperty($property){
        $value = $this->getProperty($property);
        
        if(!$value){
            foreach($this->implements as $impl){
                $value = $impl->getInheritedProperty($property);
                if($value){
                    return $value;
                }
            }
            foreach($this->specializes as $impl){
                $value = $impl->getInheritedProperty($property);
                if($value){
                    return $value;
                }
            }
        }
        return $value;
    }
    
    function isSingleton() {
        return $this->hasStereoType('singleton');
    }
    
    
    public function genericGetOperation(){
        $opName = 'get';
        if($this->hasOperation($opName)) $opName = 'fcv'.$opName;
        return $opName;
    }    
    public function genericSetOperation(){
        $opName = 'set';
        if($this->hasOperation($opName)) $opName = 'fcv'.$opName;
        return $opName;
    }    
    public function genericHasOperation(){
        $opName = 'has';
        if($this->hasOperation($opName)) $opName = 'fcv'.$opName;
        return $opName;
    }    
    public function getIdFunctionName(){
        $opName = 'getId';
        if($this->hasOperation($opName) || $this->hasAttribute('id')) $opName = 'getFcvId';
        return $opName;
    }    
    public function setIdFunctionName(){
        $opName = 'setId';
        if($this->hasOperation($opName) || $this->hasAttribute('id')) $opName = 'setFcvId';
        return $opName;
    }    
    
    public function hasInheritedStereoType($stereotype){
        foreach($this->specializes as $spec){
            if($spec->hasStereoType($stereotype)) return true;
        }
        return false;
    }
    
    public function hasPartialParse() {
        $ret = $this->hasInheritedStereoType('partialParse');
        return $ret;
    }
    
}

?>
