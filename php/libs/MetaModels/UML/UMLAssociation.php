<?php
class UMLAssociation extends UMLProperties{
    /**
     *
     * @var UMLAssociationEnd[]
     */
    protected $associationEnds = array();
        
//    private $associationClass;
    
    
    function getLinkedElements(){
        $addLinkedElements = array('associationEnds');
        return array_merge(parent::getLinkedElements(),$addLinkedElements);
    }
    function getRealElements(){
        $addLinkedElements = array('associationEnds');
        return array_merge(parent::getRealElements(),$addLinkedElements);
    }
    
    function addAssociationEnd($associationEnd){
        $this->associationEnds[] = $associationEnd;
    }
    public function getAssociationEnds() {
        return $this->associationEnds;
    }

        /**
     * 
     * @param type $otherId
     * @return UMLAssociationEnd
     */
    function getOppositeAssociationEnd($otherId){
        foreach($this->associationEnds as $end){
            if($end->getId() != $otherId) return $end;
        }
        return current($this->associationEnds);
    }
    
    function toArray() {
        $arr = parent::toArray();
        foreach($this->associationEnds as $assocEnd){
            $arr['associationEnds'][] = $assocEnd->toArray();
        }
        $arr['isRelationNM'] = $this->isRelationNM();
        return $arr;
    }

//    public function getLinkObjects() {
//        $objs = parent::getLinkObjects();
//        foreach($this->associationEnds as $end){
//            $objs = array_merge($objs, $end->getLinkObjects());
//        }
//        return $objs;
//    }
//
//    public function linkMyObjects(&$realObjects) {
//        parent::linkMyObjects($realObjects);
//        foreach($this->associationEnds as $end){
//            $end->linkMyObjects($realObjects);
//        }        
//    }
    public function isRelationNM(){
        if(!isSet($this->associationEnds[0]) || !isSet($this->associationEnds[1])) return "unknown";
        return (!$this->associationEnds[0]->isCardinalitySingle() && !$this->associationEnds[1]->isCardinalitySingle());
    }
    
    public function hasForeignKeyAttributes(){
        return (count($this->getForeignKeyAttributes()) > 0);
    }
    
    /**
     * 
     * @return UMLDataType[]
     */
    public function getForeignKeyAttributes(){
        $ret = array();
        foreach($this->associationEnds as $assocEnd){
            if(!$assocEnd->isCardinalityZero() && ($assocEnd->isForeignKey()))
//            if(!$assocEnd->isCardinalityZero() && (Settings::getObj()->tradeAllAssociationEndsAsForeignKeys || $assocEnd->isForeignKey()))
                $ret[] = $assocEnd;
        }
        return $ret;
    }
    public function isAssociationClass(){
        return false;
    }
}
?>