<?php

class UMLAssociationClass extends UMLClass {
    /**
     * 
     * @param type $otherId
     * @return UMLAssociationEnd
     */
    function getOppositeAssociationEnd($otherId){
        foreach($this->associationEnds as $end){
            if($end->getId() != $otherId) return $end;
        }
        return current($this->associationEnds);
    }
    
    public function isRelationNM(){
        if(!isSet($this->associationEnds[0]) || !isSet($this->associationEnds[1])) return "unknown";
        return (!$this->associationEnds[0]->isCardinalitySingle() && !$this->associationEnds[1]->isCardinalitySingle());
    }
    public function isAssociationClass(){
        return true;
    }
    public function toArray() {
        $arr = parent::toArray();
        $arr['isTable'] = $this->isTable();
        return $arr;
    }
}

?>
