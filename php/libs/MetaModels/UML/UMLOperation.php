<?php
class UMLOperation extends UMLProperties{
    //SubElements
    /**
     *
     * @var UMLDataType[]
     */
    protected $parameters = array();
    /**
     *
     * @var UMLDataType[]
     */
    protected $returnParameter = array();

    protected $abstract;
    protected $static;
    protected $constructor = false;
    public $setConstructor = false;

    function isConstructor(){
        return $this->constructor;
    }

    public function setConstructor($constructor) {
        $this->constructor = $constructor;
    }

    function getLinkedElements(){
        $addLinkedElements = array('parameters','returnParameter');
        return array_merge(parent::getLinkedElements(),$addLinkedElements);
    }
    function getRealElements(){
        $addLinkedElements = array('parameters','returnParameter');
        return array_merge(parent::getRealElements(),$addLinkedElements);
    }

    function addParameter($parameter){
        $this->parameters[] = $parameter;
    }

    public function addReturnParameter($returnParameter) {
        $this->returnParameter[] = $returnParameter;
    }


    function toArray() {
        $array = parent::toArray();
        $array['parameters'] = array();
        foreach($this->parameters as $para){
            $array['parameters'][] = $para->toArray();
        }
        if(isSet($this->returnParameter))
            $array['returnParameter'] = $this->returnParameter->toArray();
        return $array;
    }

    public function getParameters() {
        if(isSet($this->parameters[0])) $this->parameters[0]->first = true;
        return $this->parameters;
    }

    public function getReturnParameter() {
        return !empty($this->returnParameter) ? $this->returnParameter[0] : null;
    }
    public function isAbstract() {
        return $this->abstract;
    }

    public function setAbstract($abstract) {
        $this->abstract = $abstract;
    }

    public function isStatic() {
        return $this->static;
    }

    public function setStatic($static) {
        $this->static = $static;
    }
}
?>
