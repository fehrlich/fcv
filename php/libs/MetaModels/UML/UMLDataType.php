<?php

class UMLDataType extends UMLProperties {
    public $first = false;
    
    protected $type;
    private $cardinality = array(
        'lower' => '0',
        'upper' => '1'
    );
    private $default = false;
    private $changeability;
    private $defaultType = true;
    private $static = false;

    public function __construct($id, $name, $parent) {
        parent::__construct($id, $name, $parent);
        $this->type = new UMLDefaultType(0, '', $this);
    }

    function getLinkedElements() {
        $addLinkedElements = array('type');
        return array_merge(parent::getLinkedElements(), $addLinkedElements);
    }

    /**
     * 
     * @param UMLClass $class
     */
    function addClass($class) {
        $this->type = $class;
        $this->defaultType = false;
    }
    
    /**
     * 
     * @param UMLAssociationClass $class
     */
    function addAssociationClass($class) {
        $this->type = $class;
        $this->defaultType = false;
    }

    /**
     * 
     * @param UMLClass $class
     */
    function addEnumeration($class) {
        $this->type = $class;
        $this->defaultType = true;
    }
    
    public function setDefaultType($defaultType) {
        $this->defaultType = $defaultType;
    }

    function addMultiplicityRange($lower, $upper) {
        $this->cardinality['lower'] = $lower;
        $this->cardinality['upper'] = $upper;
    }

    public function setCardinality($cardinality) {
        $this->cardinality = $cardinality;
    }

    public function toArray() {
        $arr = parent::toArray();
        $arr['cardinality'] = $this->cardinality;
        $arr['defaultType'] = $this->isDefaultType();
        $arr['isCardinalityZero'] = $this->isCardinalityZero();
//        new dBug($this->type);
        if ($this->type)
            $arr['type'] = $this->type->getName();
        return $arr;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getType() {
        return $this->type;
    }

    public function getCardinality() {
        return $this->cardinality;
    }


    public function isCardinalityZero() {
        return ($this->cardinality['upper'] == '0');
    }
    public function isCardinalitySingle() {
        return ($this->cardinality['upper'] == '1' || $this->cardinality['upper'] == '0');
    }
    public function isCardinalityMulti() {
        return !$this->isCardinalitySingle();
    }

    public function isCardinalityMandatory() {
        return ($this->cardinality['lower'] == '1');
    }

    public function isDefaultType() {
        return $this->defaultType;
    }
    
    public function isClass() {
        return !$this->defaultType && !in_array($this->type->getName(), array('', 'Boolean', 'String', 'Integer'));
    }
    
    public function isBoolean(){
        return in_array($this->type->getName(), array('Boolean'));
    }

    public function isInteger(){
        return in_array($this->type->getName(), array('Integer'));
    }
    
    public function afterLinking() {
//        parent::afterLinking();
//        if($this->isClass()) new dBug($this->type->toArray());
//        if($this->isClass() && $this->type->usesTypeParser()){
//            $this->defaultType = true;
//        }
    }
    public function getDefault() {
        return $this->default;
    }
    public function hasDefault() {
        return $this->defaultIsSet;
    }

    
    private $defaultIsSet = false;
    public function setDefault($default) {
        $this->default = $default;
        $this->defaultIsSet = true;
    }
    
    public function getConnectionClass(){
        return $this->type;
    }
    
    
    public function setterFunction(){
        $default = 'set'.ucfirst($this->name);
        if($this->parent->hasOperation($default)) $default = 'fcv'.$default;
        return $default;
    }
    public function getterFunction(){
        $default = 'get'.ucfirst($this->name);
        if($this->parent->hasOperation($default)) $default = 'fcv'.$default;
        return $default;
    }
    public function hasGetterFunction(){
        $default = 'has'.ucfirst($this->name);
        if($this->parent->hasOperation($default)) $default = 'fcv'.$default;
        return $default;
    }
    
//    public function getParsedValue(){
//        if(!$this->isCardinalitySingle()){
//            
//        }
//    }
    
    public function isStatic() {
        return $this->static;
    }

    public function setStatic($static) {
        $this->static = $static;
    }
}

?>
