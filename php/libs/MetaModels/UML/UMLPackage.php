<?php

class UMLPackage extends UMLProperties{
    //SubElements
    protected $packages = array();
    /**
     *
     * @var UMLClass[] 
     */
    protected $classes = array();
    /**
     *
     * @var UMLGeneralization[]
     */
    protected $generalizations = array();
    protected $abstractions = array();
    /**
     *
     * @var UMLAssociation[]
     */
    protected $associations = array();
    /**
     *
     * @var UMLAssociationClass[]
     */
    protected $associationClasses = array();
    /**
     *
     * @var UMLEnumeration
     */
    protected $enumerations = array();
    
    protected $dataTypes = array();
    
    /**
     *
     * @var UMLComment[]
     */
//    protected $comments = array();
    
    function getLinkedElements(){
        $addLinkedElements = array('packages','classes','generalizations','associations','abstractions', 'associationClasses', 'enumerations', 'dataTypes');
        return array_merge(parent::getLinkedElements(),$addLinkedElements);
    }
    function getRealElements(){
        $addLinkedElements = array('packages','classes','generalizations','associations','abstractions','associationClasses', 'enumerations', 'comments', 'dataTypes');
        return array_merge(parent::getRealElements(),$addLinkedElements);
    }
        
    function addPackage($package){
        $this->packages[] = $package;
    }
    function addClass($class){
        $this->classes[] = $class;
    }
    function setType($type){
        $this->dataTypes[] = $type;
    }
    function addAssociationClass($class){
        $this->associationClasses[] = $class;
    }
    
    function addGeneralization($generalization){
        $this->generalizations[] = $generalization;
    }
    function addAbstraction($abstraction){
        $this->abstractions[] = $abstraction;
    }
    function addAssociation($association){
        $this->associations[] = $association;
    }
    function addEnumeration($enum){
        $this->enumerations[] = $enum;
    }
    
    function toArray(){
        $arr = parent::toArray();
        foreach($this->packages as $package){
            $arr['packages'][] = $package->toArray();
        }
        foreach($this->classes as $class){
            $arr['classes'][] = $class->toArray();
        }
        foreach($this->generalizations as $gen){
            $arr['generalizations'][] = $gen->toArray();
        }
        foreach($this->associations as $assoc){
            $arr['associations'][] = $assoc->toArray();
        }
        foreach($this->associationClasses as $assocClass){
            $arr['associationClasses'][] = $assocClass->toArray();
        }
        foreach($this->abstractions as $assocClass){
            $arr['abstractions'][] = $assocClass->toArray();
        }
        foreach($this->enumerations as $assocClass){
            $arr['enumerations'][] = $assocClass->toArray();
        }
        foreach($this->dataTypes as $assocClass){
            $arr['dataTypes'][] = $assocClass->toArray();
        }
        return $arr;
    }
    
//    function getLinkObjects($pseudo = false){
//        $linkObj = parent::getLinkObjects();
//        foreach($this->packages as &$package){
////            $linkObj[] = $package->getLinkObjects();
//            $packageObjs = $package->getLinkObjects($pseudo);
//            foreach($packageObjs as $id => &$obj) $linkObj[$id] = &$obj;
//        }
//        foreach($this->classes as &$class){
//            if($class->isPseudoClass() === $pseudo) $linkObj[$class->getId()] = &$class;
//            $linkObj = array_merge($linkObj, $class->getLinkObjects());
//        }
//        foreach($this->associationClasses as &$assocClass){
//            if($assocClass->isPseudoClass() === $pseudo) $linkObj[$assocClass->getId()] = &$assocClass;
//            $linkObj = array_merge($linkObj, $assocClass->getLinkObjects());
//        }
//        foreach($this->generalizations as &$gen){
//            if($gen->isPseudoClass() === $pseudo) $linkObj[$gen->getId()] = &$gen;
//            else{
//                if($pseudo){
//                    //FIX: OUTSOURCEN
//                    $c = &$gen->getParentClass();
//                    if($c->isPseudoClass()) $linkObj[$c->getId()] = &$c;
//                    $c = &$gen->getChildClass();
//                    if($c->isPseudoClass()) $linkObj[$c->getId()] = &$c;
//                }
//            }
//        }
//        foreach($this->associations as $assoc){
//            if($pseudo){
//                $linkObj = array_merge($linkObj. $assoc->getLinkObjects());
//            }
//        }
//        return $linkObj;
//    }
//    
//    function linkMyObjects(&$realObjects){
//        parent::linkMyObjects($realObjects);
//        $linkObj = array();
//        foreach($this->packages as $package){
//            $package->linkMyObjects($realObjects);
//        }
//        foreach($this->classes as $i => $class){
//            $class->linkMyObjects($realObjects);
//            if($class->isPseudoClass()){
//                if(isSet($realObjects[$class->getId()])) unset ($this->classes[$i]);
////                else echo "LinkingError: ".$class->getId().'<br>';
//            }
////            $class->linkMyObjects();
//        }
//        foreach($this->associationClasses as $i => $assocClass){
//            $assocClass->linkMyObjects($realObjects);
//            if($assocClass->isPseudoClass()){
//                if(isSet($realObjects[$assocClass->getId()])) unset ($this->associationClasses[$i]);
////                else echo "LinkingError: ".$class->getId().'<br>';
//            }
//        }
//        foreach($this->generalizations as $i => $gen){
//            if($gen->isPseudoClass()){
//                $this->generalizations[$i] = $realObjects[$gen->getId()];
//            }
//            $gen->linkMyObjects($realObjects);
//        }
//        
//        foreach($this->associations as $i => $assoc){
//            $assoc->linkMyObjects($realObjects);
//        }
//    }
    
    
//    public function iterate($cb){
//        $ret = $cb($this);
//        if($ret) return $ret;
//    }
    
    public function getPackages() {
        return $this->packages;
    }
    /**
     * 
     * @return UMLClass[]
     */
    public function getClasses($rekursiv = false) {
        $ret = $this->classes;
        if($rekursiv){
            foreach($this->packages as $p){
                $ret = array_merge($ret, $p->getClasses(TRUE));
            }
        }
        return $ret;
    }
    
    public function getEnumerations($rekursiv = false){
        $ret = $this->enumerations;
        if($rekursiv){
            foreach($this->packages as $p){
                $ret = array_merge($ret, $p->getEnumerations(TRUE));
            }
        }
        return $ret;        
    }

    public function getGeneralizations() {
        return $this->generalizations;
    }
    
    public function getAbstractions() {
        return $this->abstractions;
    }
    
    public function getAssociations() {
        return $this->associations;
    }

    public function getAssociationClasses() {
        return $this->associationClasses;
    }
    public function addComment($c){
        $this->comments[] = $c;
    }
}
?>