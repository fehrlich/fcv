<?php
class UMLProperty extends UMLProperties{
    
    private $value;
//    private $tagName;
    public $propertyDefinition;
        
    
    function getLinkedElements(){
        $addLinkedElements = array('propertyDefinition');
        return array_merge(parent::getLinkedElements(),$addLinkedElements);
    }
//    function getRealElements(){
//        $addLinkedElements = array('propertyDefinition');
//        return array_merge(parent::getRealElements(),$addLinkedElements);
//    }
    
//    function getLinkedElements(){
//        return array();
//    }
    
    function getValue() {
        return $this->value;
    }

    function setValue($value) {
        $this->value = $value;
    }
    
    function getTagName() {
        return $this->propertyDefinition->getName();
//        return $this->tagName;
    }
    
    function getName(){
        return $this->getTagName();
    }

//    function setTagName($tagName) {
//        $this->tagName = $tagName;
//    }

    function addPropertyDefinition($def){
        $this->propertyDefinition = $def;
//        d($this);
//        d($def);
//        exit();
//        $this->setTagName($def->getName());
    }
    
}