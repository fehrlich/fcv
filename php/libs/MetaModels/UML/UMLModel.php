<?php

include_once 'UMLProperties.php';
include_once 'UMLDataType.php';

include_once 'UMLComment.php';
include_once 'UMLAssociation.php';
include_once 'UMLAssociationEnd.php';
include_once 'UMLClass.php';
include_once 'UMLEnumeration.php';
include_once 'UMLEnumerationLiteral.php';
include_once 'UMLDefaultType.php';
include_once 'UMLOperation.php';
include_once 'UMLPackage.php';
include_once 'UMLGeneralization.php';
include_once 'UMLAssociationClass.php';
include_once 'UMLAbstraction.php';

include_once 'UMLStereotype.php';
include_once 'activitygraph/UMLActivityGraph.php';
include_once 'UMLProperty.php';
include_once 'UMLPropertyDefinition.php';

class UMLModel extends UMLPackage {
    protected $activityGraphs = array();
    protected $signalEvents= array();
    protected $propertyDefinition= array();
    public $oldUmlElement = null;

    public function __construct($id, $name) {
        parent::__construct($id, $name, null);
    }

    function getLinkedElements() {
        $addLinkedElements = array('stereotypes','activityGraphs','signalEvents','propertyDefinition');
        return array_merge(parent::getLinkedElements(), $addLinkedElements);
    }

    function getRealElements() {
        $addLinkedElements = array('stereotypes','activityGraphs','signalEvents', 'propertyDefinition');
        return array_merge(parent::getRealElements(), $addLinkedElements);
    }
    
    public function addPropertyDefinition($p){
        $this->propertyDefinition[] = $p;
    }
    
    public function getPropertyDefinition(){
        return $this->propertyDefinition;
    }

    public function linkObjects() {
        $objs = $this->getLinkObjects(false);
        $this->linkMyObjects($objs);
//        $this->iterate(function($umlElement){
//            $umlElement->afterLinking();
//        });
        $this->checkForTypeParser ();
//        if(Settings::getObj()->tradeAllClassAttributesAsAssociation) 
            $this->rewriteClassAttributesWithAssociation ();
    }

    public function getElementFromId($id) {
        return $this->iterate(function($umlObj) use ($id) {
            $ret = $umlObj->getId() == $id;
            return ($ret) ? $umlObj : false;
        });
    }

    public function rewriteClassAttributesWithAssociation() {
        /**
         * @var $attributes UMLDataType[]
         */
        $attributes = $this->getAll('UMLDataType');
        foreach ($attributes as $attr) {
            if ($attr->getParent()->debugType == 'UMLClass' || $attr->getParent()->debugType == 'UMLAssociationClass') {
//                if($attr->isClass() && $attr->getType()->usesTypeParser()) new dBug($attr->toArray());
//                if($attr->isClass()){
//                    new dBug($attr->getType()->toArray());
//                    new dBug($attr->getType()->usesTypeParser());
//                }
                if ($attr->isClass() && !$attr->getType()->usesTypeParser()) {
                    $package = $attr->getPackage();
//                    new dBug($attr->toArray());
                    $assoc = new UMLAssociation($attr->getId() . 'ASSOC', $attr->getName() . '_' . $attr->getType()->getName() . 'Assoc', $package);
                    $end1 = new UMLAssociationEnd($attr->getId() . 'ASSOC1', $attr->getName(), $assoc);
                    $end2 = new UMLAssociationEnd($attr->getId() . 'ASSOC2', $attr->getType()->getName(), $assoc);
                    $end1->addClass($attr->getParent());
                    $end1->setCardinality($attr->getCardinality());
                    
                    foreach($attr->getStereotypes() as $s){
                        $end1->addStereotype($s);
//                        $end2->addStereotype($s);                 
                    }
//                    $attr->get
                    
                    $end2->addClass($attr->getType());
                    $end2->addMultiplicityRange(0, 0);

                    $assoc->addAssociationEnd($end1);
                    $assoc->addAssociationEnd($end2);
                    $package->addAssociation($assoc);
//                    $end1->afterLinking();
//                    $end2->afterLinking();
//                    $assoc->afterLinking();
                }
            }
        }
    }

    public function getAll($elementName) {
        $ret = array();
        $this->iterate(function($umlObj) use (&$ret, $elementName) {
//                d($umlObj);
//            echo $umlObj->debugType.'<br>';
                    if ($umlObj->debugType == $elementName && !$umlObj->hasStereoType('Connector'))
                        $ret[] = $umlObj;
                    return false;
                });
        return $ret;
    }
    
    /**
     * 
     * @return UMLAssociationClass[]
     */
    public function getAllAssociationClasses() {
        return $this->getAll('UMLAssociationClass');
    }
    /**
     * 
     * @return UMLAssociationClass[]
     */
    public function getAllAssociationClassesAndClasses() {
        $ret = $this->getAll('UMLClass');
        $ret = array_merge($ret, $this->getAll('UMLEnumeration'));
        $ret = array_merge($ret, $this->getAll('UMLAssociationClass'));
        return $ret;
    }

    public function getAllAssociations() {
        return $this->getAll('UMLAssociation');
    }
    
    
    /**
     * 
     * @return UMLEnumeration[]
     */
    public function getAllEnumerations() {
//        echo 'CHECK';
        $ret = $this->getAll('UMLEnumeration');
        return $ret;
    }
    
    /**
     * 
     * @return UMLClass[]
     */
    public function getAllClasses() {
//        echo 'CHECK';
        $ret = $this->getAll('UMLClass');
        $ret = array_merge($ret, $this->getAll('UMLEnumeration'));
//        d($ret);
//        foreach ($ret as $r){
//            echo $r->getName().':'.$r->debugType;
//        }
//        echo 'WUT'.count($ret);
//        dd($this->getRealElements());
//        $this->iterate($callBackFunction)
        return $ret;
    }
    
    
    /**
     * 
     * @return UMLAssociationClass[]
     */
    public function getAllClassesOrdered() {
        $classes = $this->getAll('UMLClass');
        $classes = array_merge($classes, $this->getAll('UMLEnumeration'));
        $classes = array_merge($classes, $this->getAll('UMLAssociationClass'));
        $classes = array_merge($classes, $this->getAll('UMLActionState'));
        
        $ordered = [];
        $existed = [];
        
        foreach($classes as $class){
            foreach($class->getSpecializesRecursive() as $spec){
                $path = $spec->getNamePath();
                if(!isset($existed[$path])){
//                    echo 'Sub:'.$path.'<br>';
                    $existed[$path] = $spec;
                    $ordered[] = $spec;
                }
            }
            $path = $class->getNamePath();
            if(!isset($existed[$path])){
//                echo ''.$path.'<br>';
//                echo '=====================================<br>';
                $existed[$path] = $class;
                $ordered[] = $class;
//                echo $class->getName().'<br>';
//                $ordered[] = $class;
            }
//            foreach($class->getSpecializes() as $spec){
//                if(!in_array($spec, $ordered)){
//                    echo $spec->getName().'<br>';
//                    $ordered[] = $spec;
//                }
//            }
//            if(!in_array($class, $ordered)){
//                echo $class->getName().'<br>';
//                $ordered[] = $class;
//            }
        }
        return $ordered;
    }

    public function checkForTypeParser() {
        $attributes = $this->getAll('UMLDataType');
        foreach ($attributes as $attr) {
            if ($attr->getParent()->debugType == 'UMLClass' || $attr->getParent()->debugType == 'UMLAssociationClass') {
//                if($attr->isClass() && $attr->getType()->usesTypeParser()) new dBug($attr->toArray());
//                 new dBug($attr->getName());
//                 new dBug($attr->getType()->toArray());
//                if($attr->isClass()){
//                    new dBug($attr->getType()->toArray());
//                    new dBug($attr->getType()->usesTypeParser());
//                }
                if ($attr->isClass() && $attr->getType()->usesTypeParser()) {
//                    new dBug($attr->toArray());
                    $attr->setDefaultType(true);
                }
            }
        }        
    }
    
    public function addActivityGraph($graph){
        $this->activityGraphs[] = $graph;
    }
    public function addSignalEvent($event){
        $this->signalEvents[] = $event;
    }
    
    public function toArray() {
        $arr = parent::toArray();
        $arr['ActivityGraphs'] = array();
        $arr['SignalEvents'] = array();
        foreach($this->activityGraphs as $g)
            $arr['ActivityGraphs'] = $g->toArray();
        foreach($this->activityGraphs as $g)
            $arr['SignalEvents'] = $g->toArray();
        return $arr;
    }
    
    public function getActivityGraphs() {
        return $this->activityGraphs;
    }

    public function isInterface(){return false;}
    function isEnum(){return false;}
    function getSpecializes (){return false;}
    function implementsClasses (){return false;}
    function getImplements  (){return false;}
    function getAttributes  (){return false;}
    function getLiterals   (){return false;}
    function getAssociationEnds    (){return false;}
    function getParameters(){return false;}
    function getPrimaryKeyAttribute(){return false;}
//    public function iterate($cb){
//        $ret = $cb($this);
//        return $ret;
//    }
}

?>
