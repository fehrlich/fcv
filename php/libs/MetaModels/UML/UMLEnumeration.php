<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UMLEnumeration
 *
 * @author XX
 */
class UMLEnumeration extends UMLClass {
    protected $literals = array();
    
    
    function getLinkedElements(){
        $addLinkedElements = array('literals');
        return array_merge(parent::getLinkedElements(),$addLinkedElements);
    }
    function getRealElements(){
        $addLinkedElements = array('literals');
        return array_merge(parent::getRealElements(),$addLinkedElements);
    }
    
    function addEnumerationLiteral($literal){
        $this->literals[] = $literal;
    }
    
    function toArray() {
        $array = parent::toArray();
        unset($array['attributes']);        
        $array['literals'] = array();
        foreach($this->literals as $lit){
            $array['literals'] = $lit;
        }
        
        return $array;
    }
    public function getLiterals($rekursiv = false) {
        $literals = $this->literals;
        if($rekursiv){
            foreach($this->getGeneralizes() as $g){
                $literals = array_merge($literals, $g->getLiterals(true));
            }
        }
        return $literals;
    }
}

?>
