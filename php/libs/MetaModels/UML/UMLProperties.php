<?php


/**
 * Description of UMLProperties
 *
 * @author XX
 */
class UMLProperties {
    public $first = false;
    public $last = false;
    static $colPWhitList = 'php/libs/MetaModels/Mappings/ColPWhiteListImpro.php';
    static $colPWhitListVar = 'whitelisteCollectionPrepares';
    static $types = array();

    protected $pseudoClass = false;

    protected $id;
    protected $name;
    protected $comments = array();
    /**
     *
     * @var UMLProperties
     */
    protected $parent;

    
    public function addComment($c){
        $this->comments[] = $c;
    }
    /**
     *
     * @var UMLStereotype[]
     */
    protected $stereotypes = array();
    private $properties = array();
    private $visibility = 'public';
    private $linkChecked = false;

    protected $linkeElements = array('stereotypes', 'comments', 'properties'); //stereoTypes
    protected $realElements = array('comments', 'properties'); //stereoTypes

    public $debugType = '';
    public $updateStatus = '';

    function getLinkedElements(){
        return $this->linkeElements;
    }
    public function getRealElements() {
        return $this->realElements;
    }

    function __construct($id, $name, $parent) {
        $this->id = $id;
        if(!isSet($name)) $name = '';
        $this->name = $name;
        $this->parent = $parent;
        $this->debugType = get_class($this);
    }
    function toArray(){
        $arr = array(
            'id' => $this->id,
            'name' => $this->name,
            'UMLType' => get_class($this),
//            'parent' => $this->parent->getName().'['.$this->parent->getId.']'
        );
        if($this->visibility) $arr['visibility'] = $this->visibility;
        if($this->parent) $arr['parent'] = $this->parent->getName().'['.$this->parent->debugType.']';
        if($this->pseudoClass) $arr['PSEUDOCLASS'] = $this->pseudoClass;
//        echo $this->name.'!';
        if(count($this->stereotypes) > 0){
            $arr['stereotypes'] = array();
            foreach($this->stereotypes as $stereotype){
                $arr['stereotypes'][] = $stereotype->getName();
            }
        }
        if($this->linkChecked) $arr['linkedCheck'] = 'TRUE';

        return $arr;
    }

    public function dump(){
        new dBug($this->toArray());
    }

    
    public function isPrimaryKey() {
        return $this->hasStereoType('PK');
    }
    
    public function isForeignKey() {
        $ret = $this->hasStereoType('FK');
        if($ret) return $ret;
//        new dBug($this->parent->debugType);
        return ($this->debugType == 'UMLAssociationEnd' &&
                ($this->parent->debugType == 'UMLAssociation' || $this->parent->debugType == 'UMLAssociationClass'));
//        return (Settings::getObj()->tradeAllAssociationEndsAsForeignKeys && $this->debugType == 'UMLAssociationEnd' &&
//                ($this->parent->debugType == 'UMLAssociation' || $this->parent->debugType == 'UMLAssociationClass'));
       
    }
    
    public function getName() {
//        if($this->name == '') return '('.$this->getId().')';
        return $this->name;
    }
    public function getNamePath() {
        $path = array($this->getName());
        $parent = $this;
        while($parent->getParent()){
            $parent = $parent->getParent();
            $path[] = $parent->getName();
        }
        $path = array_reverse($path);
        if($this->hasStereoType('Connector')){
            array_shift($path);
        }
        return '\\'.implode('\\', $path);
    }
    public function getNameUCFirst(){
        return ucfirst($this->getName());
    }
    public function getNameLCFirst(){
        return lcfirst($this->getName());
    }
    public function getNameUppdercase(){
        return strtoupper($this->getName());
    }
    public function getNameUppercase(){
        return strtoupper($this->getName());
    }
    public function getId() {
        return $this->id;
    }
    public function setName($name) {
        $this->name = $name;
    }
    public function setVisibility($visibility) {
        $this->visibility = $visibility;
    }

    public function addStereotype($stereotype){
        $this->stereotypes[] = $stereotype;
    }

    public function addProperty($property){
        $this->properties[] = $property;
    }

    public function setPseudoClass($pseudoClass) {
        $this->pseudoClass = $pseudoClass;
    }

    public function isPseudoClass() {
        return $this->pseudoClass;
    }

    static function getFromId($id, $parent) {
        $className = get_called_class();
        $class = new $className($id, '', $parent);
        $class->setPseudoClass(true);
        return $class;
    }


    public function getLinkObjects($pseudo = true) {
        $objs = array();

        foreach($this->getLinkedElements() as $linkedKey){
            if(is_array($this->$linkedKey)){
                foreach($this->$linkedKey as $i => $umlObj){
                    $objs = array_merge($objs, $umlObj->getLinkObjects($pseudo));
                }
            }else{
                if(!method_exists($this->$linkedKey, "isPseudoClass")){
                    d($this);
                    d($this->$linkedKey);
                    d(1);
                }
                
                if($this->$linkedKey->isPseudoClass() == $pseudo){
                    $objs[$this->$linkedKey->getId()] = $this->$linkedKey;
                }
            }
        }
        if($this->isPseudoClass() == $pseudo) $objs[$this->getId()] = $this;
        return $objs;
    }

    function linkMyObjects(&$realObjects){
        
        if($this->linkChecked) return;
        $this->linkChecked = true;
        foreach($this->getLinkedElements() as $linkedKey){
            $linkedKeyArray = &$this->$linkedKey;
            
            if(is_array($linkedKeyArray)){
                foreach($linkedKeyArray as $i => $umlObj){
                    if($umlObj->isPseudoClass()){
                        $linkedKeyArray[$i] = &$realObjects[$umlObj->getId()];
                    }
                    $umlObj->linkMyObjects($realObjects);
                }
            }else{
                $umlObj = $this->$linkedKey;
                if($this->$linkedKey->isPseudoClass()){
                    if(isSet($realObjects[$this->$linkedKey->getId()])){
                        $this->$linkedKey = &$realObjects[$this->$linkedKey->getId()];
                    }else{
                        echo 'Link Error: '. $this->$linkedKey->getId().'<br>';
                        d($linkedKey);
                        exit();
                    }
                }
            }
        }
        $this->afterLinking();
    }

    function iterate($callBackFunction){
        $ret = $callBackFunction($this);
        if($ret) return $ret;
        foreach($this->getRealElements() as $linkedKey){
            $linkedKeyArray = &$this->$linkedKey;
            if(is_array($linkedKeyArray)){
                foreach($linkedKeyArray as $i => $umlObj){
                    $ret = $umlObj->iterate($callBackFunction);
                    if($ret) return $ret;
                }
            }else{
                $ret = $this->$linkedKey->iterate($callBackFunction);
                if($ret) return $ret;
            }
        }
        return false;
    }

    public function getParent() {
        return $this->parent;
    }

    public function getStereotypes() {
        return $this->stereotypes;
    }
    
    function getProperties() {
        return $this->properties;
    }

    public function getVisibility() {
        return $this->visibility;
    }

    public function afterLinking() {

    }

    
    public function hasOrParentHasStereoType($stereotype){
        $parent = $this;
        while($parent && !($parent instanceof UMLModel)){
            if($parent->hasStereoType($stereotype)) return true;
            $parent = $parent->getParent();
        }
        return false;
    }
    
    public function hasStereoType($stereotype){
        foreach($this->stereotypes as $stereo){
            if($stereo && strtoupper($stereo->getName()) == strtoupper($stereotype)) return true;
        }
        return false;
    }
    
    
    
    public function hasInheritedProperty($hasProperty){
        return ($this->hasProperty($hasProperty));
    }
    
    public function getInheritedProperty($property){
        return $this->getProperty($property);
    }
        
    public function hasProperty($hasProperty){
        foreach($this->properties as $property){
            if(strtoupper($property->getTagName()) == strtoupper($hasProperty)){
                return true;
            }
        }
        return false;
    }
    
    public function getProperty($hasProperty){
        foreach($this->properties as $property){
            if(strtoupper($property->getTagName()) == strtoupper($hasProperty)){
                return $property->getValue();
            }
        }
        return false;
    }
    
    public function getDocumentation(){
        $dok = "";
        if($this->hasProperty('documentation')){
            $dok = $this->getProperty('documentation');
        }
        return $dok;
    }
    
    /**
     *
     * @return UMLPackage
     */
    public function getPackage(){
        $parent = $this->parent;
        while($parent->debugType != 'UMLPackage' && $parent->debugType != 'UMLModel'){
            $parent = $parent->getParent();
        }
        return $parent;
    }

    public function isClass(){
        return ($this->debugType == 'UMLClass' || $this->debugType == 'UMLAssociationClass' || $this->debugType == 'UMLEnumeration');
    }
    
    public function isOperation(){
        return ($this->debugType == 'UMLOperation');
    }
    public function getExistsOperation(){
        $defaultName = 'exists';
        if($this->hasOperation($defaultName)) $defaultName = 'FCV'.$defaultName;
        return $defaultName;
    }
    public function isProject(){
        return ($this->debugType == 'UMLModel');
    }

    public function isNew(){
        return ($this->updateStatus == 'NEW');
    }
    public function hasChanged(){
        return ($this->updateStatus == 'CHANGE');
    }
    public function getOldUmlElement() {
        return $this->oldUmlElement;
    }

    public function setOldUmlElement($oldUmlElement) {
        $this->oldUmlElement = $oldUmlElement;
    }

    public function isPageOrFrame(){
        return ($this->hasStereoType('page') || $this->hasStereoType('frame'));
    }
    public function hasHistory(){
        return $this->hasStereoType('history');
    }
    public function hasActive(){
        return $this->hasStereoType('active');
    }
    public function parentsHaveActive(){
        if($this->hasStereoType('active')){
            return true;
        }
        if($this->parent){
            return $this->parent->parentsHaveActive();
        }
        return false;
    }
    private $workPackage;
    public function getWorkPackage() {
        return $this->workPackage;
    }

    public function setWorkPackage(&$workPackage) {
        $this->workPackage = $workPackage;
    }
    function hasPath(){
        return $this->getWorkPackage() && $this->getWorkPackage()->getPath() != '';
    }
    
    function getComments() {
        return $this->comments;
    }
    
    function isJs(){
        return $this->hasOrParentHasStereoType('JS');
    }
    function noJs(){
        return $this->hasOrParentHasStereoType('nojs') || $this->hasOrParentHasStereoType('noClient');
    }
    
    function isNotClient(){
        return !$this->isClient();
    }
    
    function isClient(){
        
        if($this->noJs()){
            return false;
        }
        
        $c = $this->isPageOrFrame() || 
            $this->hasOrParentHasStereoType('JS') || 
            $this->hasStereoType('Client') || 
            $this->hasStereoType('ClientConnect') || 
            $this->hasStereoType('typeParser');
        
        
        if(!$c){            
            if($this instanceof UMLClass){
                if($this->usesTypeParser()){
                    return true;
                }
                return false;
                $ret = $this->checkClient(str_replace('/', '\\', $this->getNamePath()));
                return $ret;
            }
            elseif($this instanceof UMLOperation){
                if($this->getParent()->usesTypeParser()){
                    return true;
                }
                return false;
                return $this->checkClient(str_replace('/', '\\', $this->getNamePath()), $this->getName());
            }
        }
        return $c;
    }
    
    function canCallFromClient(){ 
        if($this instanceof UMLOperation){
            
            $class = substr(str_replace('/', '\\', $this->getParent()->getNamePath()), 1);
            $operation = $this->name;
            
            $jsDispatchMap = $this->getWhiteListeJs();            
            return isset($jsDispatchMap[$class]) && isset($jsDispatchMap[$class][$operation]);
        }
        throw new Exception(get_class($this).' not supported for server calls');
    }
    
    function checkClientColP($operation, $parent = null){
        if($this->noJs()){
            return false;
        }
        
        if($this instanceof UMLClass && $operation == 'isNot'){
            $classPath = $this->getNamePath();        
            $class = substr(str_replace('/', '\\', $classPath), 1);
            return $this->checkClientCP($class.'CollectionPrepare', $operation, lcfirst($this->getName()));
        }
        if($this instanceof UMLAssociationEnd){
            $parent = $this->getParent();
            
            $attr = $this->getName();
            if($parent instanceof UMLAssociationClass){
                $classPath = $this->getParent()->getNamePath();
                $attr = lcfirst($this->getClass()->getName());
            } else {
                $classPath = $this->getClass()->getNamePath();
            }
            
            
            $class = substr(str_replace('/', '\\', $classPath), 1);
            return $this->checkClientCP($class.'CollectionPrepare', $operation, $attr);
        }
        if($this instanceof UMLDataType){
            $class = substr(str_replace('/', '\\', $this->getParent()->getNamePath()), 1);
            return $this->checkClientCP($class.'CollectionPrepare', $operation, $this->getName());
        }
        return false;        
    }
    
    function checkClientCP($class, $operation = null, $attribute = null){
        $whitelisteCollectionPrepares = $this->getWhiteListeColP();
        $ret = 
            $attribute && 
            isset($whitelisteCollectionPrepares[$class]) && 
            isset($whitelisteCollectionPrepares[$class][$attribute]) && 
            isset($whitelisteCollectionPrepares[$class][$attribute]['operations']) && 
            in_array($operation, $whitelisteCollectionPrepares[$class][$attribute]['operations']);
        
        return $ret;
    }
    
    function getWhiteListeColP(){
        include static::$colPWhitList;
        $varName = static::$colPWhitListVar;
        return $$varName;
    }
    
    function getWhiteListeJs(){
        include 'php/libs/MetaModels/Mappings/JsWhiteListImpro.php';
        return $jsDispatchMap;
    }
}

?>
