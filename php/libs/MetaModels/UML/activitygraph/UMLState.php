<?php
class UMLState extends UMLProperties{
    protected $sourceTransitions = array();
    protected $targetTransitions = array();
    
    protected $type = 'Integer';
    protected $initValue = 0;
    
    function addTransition(){
        
    }
    function addSourceTransition(&$t){
        $this->sourceTransitions[] = $t;
    }
    function addTargetTransition(&$t){
        $this->targetTransitions[] = $t;
    }
    
    function toArray() {
        $arr = parent::toArray();
        $arr['sourceTransitions'] = array();
        $arr['targetTransitions'] = array();
        foreach($this->sourceTransitions as $t) $arr['sourceTransitions'][] = $t->getName();
        foreach($this->targetTransitions as $t) $arr['targetTransitions'][] = $t->getName();
        return $arr;
    }
    
    function isInterface(){return false;}
    function isEnum(){return false;}
    function getSpecializes (){return false;}
    function getClientSpecializes (){return false;}
    function implementsClasses (){return false;}
    function getImplements  (){return false;}
    function getAttributes  (){return false;}
    function getLiterals   (){return false;}
    function getAssociationEnds    (){return false;}
    function getPrimaryKeyAttribute    (){return false;}
    function isDefault    (){
        return $this->hasStereoType('default');
        
    }
    
    public function hasOperation($opName){
//        foreach($this->operations as $op){
//            if($opName == $op->getName()) return true;
//        }
        return false;
    }
    
    public function genericGetOperation(){
        $opName = 'get';
        if($this->hasOperation($opName)) $opName = 'fcv'.$opName;
        return $opName;
    }    
    public function genericSetOperation(){
        $opName = 'set';
        if($this->hasOperation($opName)) $opName = 'fcv'.$opName;
        return $opName;
    }    
    public function genericHasOperation(){
        $opName = 'has';
        if($this->hasOperation($opName)) $opName = 'fcv'.$opName;
        return $opName;
    }
       
    public function getIdFunctionName(){
        return 'getId';
    }    
    public function setIdFunctionName(){
        return 'setId';
    }
    public function getSpecializesRecursive(){
        return [];
    }
}