<?php
/**
 * Description of UMLActivityGraph
 *
 * @author XX
 */
include 'UMLState.php';
include 'UMLActionState.php';
include 'UMLCompositeState.php';
include 'UMLFinalState.php';
include 'UMLPseudostate.php';
include 'UMLSignalEvent.php';
include 'UMLTransition.php';
class UMLActivityGraph extends UMLProperties{
    protected $actionStates = array();
    protected $pseudoStates = array();
    protected $finalStates = array();
    
    protected $transitions = array();
    
    
    function getLinkedElements(){
        $addLinkedElements = array('actionStates','finalStates','pseudoStates','transitions');
        return array_merge(parent::getLinkedElements(),$addLinkedElements);
    }
    function getRealElements(){
        $addLinkedElements = array('transitions','actionStates','finalStates','pseudoStates');
        return array_merge(parent::getRealElements(),$addLinkedElements);
    }
            
    function addActionState($state){
        $this->actionStates[] = $state;
    }
    function addPseudoState($state){
        $this->pseudoStates[] = $state;
    }
    function addFinalState($state){
        $this->finalStates[] = $state;
    }
    function addTransition($trans){
        $this->transitions[] = $trans;
    }
    
    function toArray() {
        $arr = parent::toArray();
        $arr['States'] = array();
        $arr['Transitions'] = array();
        $states = array_merge($this->actionStates, $this->pseudoStates, $this->finalStates);
        foreach($states as $s) $arr['States'][] = $s->toArray();
        foreach($this->transitions as $t) $arr['Transitions'][] = $t->toArray();
        
        return $arr;
    }
}

?>
