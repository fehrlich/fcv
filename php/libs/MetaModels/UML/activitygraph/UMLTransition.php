<?php
/**
 * Description of UMLActivityGraph
 *
 * @author XX
 */
class UMLTransition extends UMLProperties{
    /**
     *
     * @var UMLState
     */
    protected $sourceState = false;
    protected $useVar = '';
    protected $condition;
    
    /**
     *
     * @var UMLState
     */
    protected $targetState = false;
    protected $triggerSignalEvents = array();
    
    protected $nextIsTarget = false;
    
    function getLinkedElements(){
        $addLinkedElements = array('sourceState','targetState','triggerSignalEvents');
        return array_merge(parent::getLinkedElements(),$addLinkedElements);
    }
            
    function addPseudostate($state){
        if($this->nextIsTarget) $this->targetState = $state;
        else $this->sourceState = $state;
    }
    function addActionState($state){
        if($this->nextIsTarget) $this->targetState = $state;
        else $this->sourceState = $state;
    }
    function addFinalState($state){
        if($this->nextIsTarget) $this->targetState = $state;
        else $this->sourceState = $state;
    }
    
    function addSignalEvent($event){
        $this->triggerSignalEvents[] = $event;
    }
    
    function setNextStateIsTarget($t){
        $this->nextIsTarget = $t;
    }
    
    function toArray() {
        $arr = parent::toArray();
        $arr['sourceState'] = $this->sourceState->getName();
        $arr['targetState'] = $this->targetState->getName();
        $arr['events'] = array();
        foreach($this->triggerSignalEvents as $e){
            $arr['events'] = $e->toArray();
        }
        return $arr;
    }
    
    public function afterLinking() {
        $this->sourceState->addSourceTransition($this);
        $this->targetState->addTargetTransition($this);
    }
    
}

?>
