<?php
class UMLAssociationEnd extends UMLDataType{    

    public function getClass() {
        return $this->type;
    }

    /**
     * 
     * @return UMLClass
     */
    function getConnectionClass(){
//        new dBug($this->parent->toArray());
//        echo $this->parent->getId().'<br>';
        return $this->getOppositeAssociationEnd()->getClass();
    }
    
    function getOppositeAssociationEnd(){
        return $this->parent->getOppositeAssociationEnd($this->id);
    }
    
    
    function toArray() {
        $array = parent::toArray();
        $array['isCardinalitySingle'] = $this->isCardinalitySingle();
//        $array['connectionClass'] = $this->getConnectionClass()->getName();
        
        return $array;
    }
    
    public function afterLinking() {
        $this->type->addAssociationEnd($this);
    }
    
    public function addAssociationFunction(){
        $opName = 'add'.$this->getConnectionClass()->getName();
        if($this->getClass()->hasOperation($opName)) $opName = 'fcv'.$opName;
        return $opName;
    }
    public function setAssociationFunction(){
        $opName = 'set'.ucfirst($this->getName());
        if($this->getClass()->hasOperation($opName)) $opName = 'fcv'.$opName;
        return $opName;
    }
    public function getAssociationFunction(){
        $opName = 'get'.ucfirst($this->getName());
        if($this->getClass()->hasOperation($opName)) $opName = 'fcv'.$opName;
        return $opName;
    }
    public function getType(){
        return $this->getConnectionClass();
    }
//    public function isClass(){
//        return true;
//    }
}

?>
