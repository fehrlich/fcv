<?php

class xmiParserArgo extends xmiParser {

    /**
     *
     * @var UMLProperties 
     */
    private $curElementStack = array();
    private $parser;
    
    private $dataTypeParser;
    
    private $debug = false;
    private $debugDeath = 0;

    public function __construct($xmlData, $umlModel) {
        parent::__construct($xmlData, $umlModel);
//        d($xmlData);
        $this->parser = new xmlObj2ParserArgo();
        $this->dataTypeParser = new dataTypeParserArgo();
//        $this->curElementStack[] = &$this->umlModel;
        $this->setNewParent($this->umlModel);
    }

    public function getStack() {
        return $this->curElementStack[count($this->curElementStack) - 1];
    }

    public function progressXMIObj($obj, $parentKey, $realDataObject = true, $completeObj = array()) {
        if($realDataObject){
            switch ($parentKey) {
                case 'UML:Package':
                    $package = $this->parser->parsePackage($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartPackage';
                        new dBug($package->toArray());
                        echo '</div>';       
                        $this->debugDeath++;
                        print_r($this->debugDeath);
                    }
                    $this->getStack()->addPackage($package);
                    $this->setNewParent($package);
                    break;
                case 'UML:Class':
                    // $class = $this->parser->parseClass($obj);
                    // if(!isset($obj['xmi.idref'])){
                        $class = $this->parser->parseClass($obj);
                    // }else{
                    //     $class = new UMLClass($obj['xmi.idref'], '', $this->getStack());
                    //     $class->setPseudoClass(true);                            
                    // }
                    
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartClass';
                        new dBug($class->toArray());
                        echo '</div>';       
                        $this->debugDeath++;
                    }

                    $this->goUpToFunction('addClass');
                    $this->getStack()->addClass($class);
                    $this->setNewParent($class);
                    break;
                case 'UML:Interface':
                    $class = $this->parser->parseClass($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartClass';
                        new dBug($class->toArray());
                        echo '</div>';       
                        $this->debugDeath++;
                    }
                    $stereoType = new UMLStereotype($class->getId().'-interface', 'inteface', $class);
//                    $class
                    $class->addStereoType($stereoType);
                    $this->goUpToFunction('addClass');
                    $this->getStack()->addClass($class);
                    $this->setNewParent($class);
                    break;
                case 'UML:Comment':
                    $comment = $this->parser->parseComment($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartComment';
                        new dBug($comment->toArray());
                        echo '</div>';       
                        $this->debugDeath++;
                    }
                    $this->goUpToFunction('addComment');
                    $this->getStack()->addComment($comment);
                    $this->setNewParent($comment);
                    break;
                case 'UML:Enumeration':
                    
                    $type = 'NotSupported';
                    if(isSet($obj['href'])){
                        $retArr = $this->dataTypeParser->parseDatatype ($obj['href']);
                        $type = new UMLDefaultType($retArr['xmi.id'], $retArr['name'], $this->getStack());
                        $this->getStack()->setType($type);
                        break;
                    }
                    
                    $enum = $this->parser->parseEnumeration($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartEnumeration';
                        new dBug($enum->toArray());
                        echo '</div>';       
                        $this->debugDeath++;
                    }
                    $this->goUpToFunction('addEnumeration');
                    $this->getStack()->addEnumeration($enum);
                    $this->setNewParent($enum);
                    break;
                case 'UML:EnumerationLiteral':
                    $enumLit = $this->parser->parseEnumerationLiteral($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartEnumerationLiteral';
                        new dBug($enumLit->toArray());
                        echo '</div>';       
                        $this->debugDeath++;
                    }
                    $this->goUpToFunction('addEnumerationLiteral');
                    $this->getStack()->addEnumerationLiteral($enumLit);
//                    d($this->getStack());
                    $this->setNewParent($enumLit);
                    break;
                case 'UML:AssociationClass':
                    $associationClass = $this->parser->parseAssociationClass($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'startAssociationClass';
                        new dBug($associationClass->toArray());
                        echo '</div>';       
                        $this->debugDeath++;
                    }
                    $this->goUpToFunction('addAssociationClass');
                    $this->getStack()->addAssociationClass($associationClass);
                    $this->setNewParent($associationClass);
                    break;
                case 'UML:Attribute':
                    $attribute = $this->parser->parseDataType($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartAttribute';
                        new dBug($attribute->toArray());
                        echo '</div>';       
                        $this->debugDeath++;
                    }
                    $this->goUpToFunction('addAttribute');
                    $this->getStack()->addAttribute($attribute);
                    $this->setNewParent($attribute);
                    break;
                case 'UML:Operation':
                    $operation = $this->parser->parseOperation($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartOperation';
                        new dBug($operation->toArray());
                        echo '</div>';       
                        $this->debugDeath++;
                    }
                    $this->goUpToFunction('addOperation', $operation);
                    $this->getStack()->addOperation($operation);
                    $this->setNewParent($operation);
                    break;
                case 'UML:Parameter':
                    $parameter = $this->parser->parseDataType($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartParameter';
                        new dBug($parameter->toArray()); 
                        echo '</div>';        
                        $this->debugDeath++;                  
                    }
                    $this->goUpToFunction('addParameter');
                    if($obj['kind'] == 'return'){
                        $this->getStack()->addReturnParameter($parameter);
                    }
                    else $this->getStack()->addParameter($parameter);
                    $this->setNewParent($parameter);
                    break;
                case 'UML:Generalization':
                    $generalization = $this->parser->parseGeneralization($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartGeneralization';
                        new dBug($generalization->toArray());   
                        echo '</div>';            
                        $this->debugDeath++;
                    }
                    $this->goUpToFunction('addGeneralization');
                    $this->getStack()->addGeneralization($generalization);
                    $this->setNewParent($generalization);
                    break;
                case 'UML:Dependency':
                case 'UML:Abstraction':
                    $abstraction = $this->parser->parseAbstraction($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartAbstraction';
                        new dBug($abstraction->toArray());   
                        echo '</div>';            
                        $this->debugDeath++;
                    }
                    $this->goUpToFunction('addAbstraction');
                    $this->getStack()->addAbstraction($abstraction);
                    $this->setNewParent($abstraction);
                    break;
                case 'UML:Association':
                    $association = $this->parser->parseAssociation($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartAssociation';
                        new dBug($association->toArray());   
                        echo '</div>';            
                        $this->debugDeath++;
                    }
                    $this->goUpToFunction('addAssociation');
                    $this->getStack()->addAssociation($association);
                    $this->setNewParent($association);
                    break;
                case 'UML:AssociationEnd':
                    $associationEnd = $this->parser->parseAssociationEnd($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartAssociationEnd';
                        new dBug($associationEnd->toArray());   
                        echo '</div>';            
                        $this->debugDeath++;
                    }
                    $this->goUpToFunction('addAssociationEnd');
                    $this->getStack()->addAssociationEnd($associationEnd);
                    $this->setNewParent($associationEnd);
                    break;
                case 'UML:MultiplicityRange':
                    if($this->getStack()->debugType != 'UMLModel')
                        $this->getStack()->addMultiplicityRange($obj['lower'], $obj['upper']);
                    break;
                case 'UML:DataType':
                    $type = 'NotSupported';
                    if(isSet($obj['href'])){
                        $retArr = $this->dataTypeParser->parseDatatype ($obj['href']);
                        $type = new UMLDefaultType($retArr['xmi.id'], $retArr['name'], $this->getStack());
                    }else{
                        
                        if(!isset($obj['xmi.idref'])){
                            $type = $this->parser->parseDataType($obj);
                        }else{
                            $type = new UMLClass($obj['xmi.idref'], '', $this->getStack());
                            $type->setPseudoClass(true);                            
                        }
                    }
                    $this->getStack()->setType($type);                    
                    break;
                case 'UML:Stereotype':
                    $stereotype = $this->parser->parseStereotype($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartStereotype';
                        new dBug($stereotype->toArray());   
                        echo '</div>';            
                        $this->debugDeath++;
                    }
                    $this->goUpToFunction('addStereotype');
                    $this->getStack()->addStereotype($stereotype);
                    $this->setNewParent($stereotype);
                    break;
                case 'UML:TagDefinition':
                    $propertyDef = $this->parser->parsePropertyDefinition($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartProperty';
                        new dBug($property->toArray());   
                        echo '</div>';            
                        $this->debugDeath++;
                    }
//                    if($propertyDef->getName() != ''){
//                        d($propertyDef);
//                        d($obj);
//                        d($completeObj);
//                        d("PROPERTY HAS A NAME");
////                        exit();
//                    }
                    $this->goUpToFunction('addPropertyDefinition');
                    $this->getStack()->addPropertyDefinition($propertyDef);
                    $this->setNewParent($propertyDef);
                    break;
                case 'UML:TaggedValue':
                    $property = $this->parser->parseProperty($obj);
                    if($this->debug){
                        echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                        echo 'StartProperty';
                        new dBug($property->toArray());   
                        echo '</div>';            
                        $this->debugDeath++;
                    }
//                    if($property->getId() == '-64--88-2-55-2490541e:15ca5ad70b1:-8000:0000000000002088'){ // && 
////                        if(!isset($completeObj['UML:TaggedValue.dataValue']['value']) && $property->getId() == '-64--88-2-55-2490541e:15ca5ad70b1:-8000:0000000000002088'){ // && 
//                        echo 'ERROR NO VALUE';
//                        d($obj);
//                        d($property);
//                        d($completeObj);
//                        d($completeObj['UML:TaggedValue.dataValue']);
//                        exit();
//                    }
                    if(isset($completeObj['UML:TaggedValue.dataValue'])){
                        if(!isset($completeObj['UML:TaggedValue.dataValue'])){
//                            d($completeObj['UML:TaggedValue.dataValue']);
                        }
//                        elseif($property->getId() == '-64--88-2-55-2490541e:15ca5ad70b1:-8000:0000000000002088'){ // && 
                        elseif(!isset($completeObj['UML:TaggedValue.dataValue']['value'])){ // && 
//                            d($obj);
//                            d($property);
//                            d($completeObj);
//                            d($completeObj['UML:TaggedValue.dataValue']);
                        }
                        else{
                            $property->setvalue($completeObj['UML:TaggedValue.dataValue']['value']);
                        }
                    }
//                    d($property);
                    $this->goUpToFunction('addProperty');
                    $this->getStack()->addProperty($property);
//                    d($this->getStack());
                    $this->setNewParent($property);
                    break;
//                case 'UML:TaggedValue.dataValue':
//                    $this->getStack()->setvalue($completeObj['value']);
//                    d($this->getStack());
//                    break;
                case 'UML:Expression':
                    $this->getStack()->setDefault($obj['body']);
                    break;
                case 'UML:Model':
                    break;
    //            case '':
    //                $this->curPackage = $obj['attr'];
    //                break;
                default:
    //                echo $parentKey.'!';
                    $elemName = str_replace('UML:', '', $parentKey);
                    if(class_exists('UML'.$elemName)){
                        $element = $this->parser->parseElement($elemName, $obj);
                        if($this->debug){
                            echo '<div style="padding-left: '.(40*$this->debugDeath).'px">';
                            echo 'Start'.$elemName;
                            new dBug($element->toArray());   
                            echo '</div>';            
                            $this->debugDeath++;
                        }
                        $funcName = 'add'.$elemName;
                        $this->goUpToFunction($funcName);
                        $this->getStack()->$funcName($element);
                        $this->setNewParent($element);
                    }
                    break;
            }
        }else{
            switch ($parentKey) {
                case 'UML:Generalization.child':
                    $this->getStack()->setNextClassIsChild(true);
                    break;
//                case 'UML:TaggedValue.dataValue':
////                    d($obj);
////                    d($parentKey);
//                    $this->getStack()->setvalue($completeObj['value']);
//                    break;
                case 'UML:Generalization.parent':
                    $this->getStack()->setNextClassIsChild(false);
                    break;     
                case 'UML:Dependency.client':
//                    if($this->getStack()->debugType == 'UMLAbstraction'){
                        $this->getStack()->setNextClassIsChild(true);
//                    }
                    break;           
                case 'UML:Dependency.supplier':
//                    if($this->getStack()->debugType == 'UMLAbstraction')
                        $this->getStack()->setNextClassIsChild(false);
                    break;    
                case 'UML:Transition.source':
                    $this->getStack()->setNextStateIsTarget(false);
                    break;       
                case 'UML:Transition.target':
                    $this->getStack()->setNextStateIsTarget(true);
                    break;       
            }
        }
    }

    function goUpToFunction($funcName, $debugObj = array()) {
        $check = $this->curElementStack;
        if (!method_exists($this->getStack(), $funcName)) {
            echo 'LEFT('.$funcName.')'.$this->getStack()->getName();
//            new dBug($check);
            
            foreach ($check as $c) d($c->toArray());
//            d(1);
//            d($this->getStack();
            d($debugObj);
            die("ERROR@" . $funcName.'('.$this->getStack()->getName().')');
//            array_pop($this->curElementStack);
//            if (empty($this->curElementStack)) {
//                new dBug($check);
//                foreach($check as $c) new dBug($c->toArray());
//                die("ERROR@" . $funcName);
//            }
        }
    }
    function setNewParent($obj){
//        echo 'START';
        $this->curElementStack[] = $obj;
        $this->parser->setParent($this->getStack());
        $this->depthBreathArray[$this->depth][$this->breadth] = true;
    }
    
    function endElementLoop(){
        if(!isSet($this->depthBreathArray[$this->depth][$this->breadth])) echo $this->depth.'x'.$this->breadth;
        if($this->depthBreathArray[$this->depth][$this->breadth]){
//        if($this->depthArray[$this->debugDeath]){
            array_pop($this->curElementStack);
            if(count($this->curElementStack) > 0)
                $this->parser->setParent($this->getStack());
            if($this->debug){
                echo 'END';       
                $this->debugDeath--;
            }
        }
    }

}

class xmlObj2ParserArgo extends xmlObj2Parser {
    
    public function parseGeneral($uml, $obj){
        if(isSet($obj['visibility'])) $uml->setVisibility($obj['visibility']);
        
        
        return $uml;
    }

    public function parseClass($obj) {
        if(!isSet($obj['name'])) $obj['name'] = '';
        if (!isSet($obj['xmi.id']) && isSet($obj['xmi.idref'])){
            return UMLClass::getFromId($obj['xmi.idref'],$this->parent);
        }
//            new dBug($obj);
        $uml = new UMLClass($obj['xmi.id'], $obj['name'], $this->parent);
        return $this->parseGeneral($uml, $obj);
    }

    public function parseDataType($obj) {
        if(!isSet($obj['name'])) $obj['name'] = '';
        $uml = new UMLDataType($obj['xmi.id'], $obj['name'], $this->parent);
        $uml->setStatic(isSet($obj['ownerScope']) && $obj['ownerScope'] == 'classifier');
//        new dBug($uml->toArray());
        return $this->parseGeneral($uml, $obj);
    }

    public function parseOperation($obj) {
        if(!isSet($obj['name'])) $obj['name'] = '';
        $uml = new UMLOperation($obj['xmi.id'], $obj['name'], $this->parent);
//        var_dump($obj['isAbstract']);
        $uml->setAbstract(isSet($obj['isAbstract']) && $obj['isAbstract'] == 'true');
        $uml->setStatic(isSet($obj['ownerScope']) && $obj['ownerScope'] == 'classifier');
        return $this->parseGeneral($uml, $obj);
    }

    public function parsePackage($obj) {
        if(!isSet($obj['name'])) $obj['name'] = '';
        $uml = new UMLPackage($obj['xmi.id'], $obj['name'], $this->parent);
        return $this->parseGeneral($uml, $obj);
    }

    public function parseGeneralization($obj) {
        if (!isSet($obj['xmi.id']) && isSet($obj['xmi.idref'])){
            return UMLGeneralization::getFromId($obj['xmi.idref'],$this->parent);
        }
        if(!isSet($obj['name'])) $obj['name'] = '';
        $uml = new UMLGeneralization($obj['xmi.id'], $obj['name'], $this->parent);  
        return $this->parseGeneral($uml, $obj); 
    }

    public function parseAssociation($obj) {
        $uml = new UMLAssociation($obj['xmi.id'], $obj['name'], $this->parent);  
        return $this->parseGeneral($uml, $obj);         
    }
    public function parseAssociationEnd($obj) {
        if(!isSet($obj['name'])) $obj['name'] = '';
        $uml = new UMLAssociationEnd($obj['xmi.id'], $obj['name'], $this->parent);  
        return $this->parseGeneral($uml, $obj);         
    }

    
    public function parseAssociationClass($obj) {
        if(!isSet($obj['name'])) $obj['name'] = '';
        if (!isSet($obj['xmi.id']) && isSet($obj['xmi.idref'])){
            return UMLAssociationClass::getFromId($obj['xmi.idref'],$this->parent);
        }
//            new dBug($obj);
        $uml = new UMLAssociationClass($obj['xmi.id'], $obj['name'], $this->parent);
        return $this->parseGeneral($uml, $obj);
    }
    
    public function parseStereotype($obj) {
        if (!isSet($obj['xmi.id']) && isSet($obj['xmi.idref'])){
            return UMLStereotype::getFromId($obj['xmi.idref'],$this->parent);
        }
        if(!isSet($obj['name'])) $obj['name'] = '';
        $uml = new UMLStereotype($obj['xmi.id'], $obj['name'], $this->parent);  
        return $this->parseGeneral($uml, $obj); 
        
    }
    
    public function parseProperty($obj) {
        if (!isSet($obj['xmi.id']) && isSet($obj['xmi.idref'])){
            return UMLProperty::getFromId($obj['xmi.idref'],$this->parent);
        }
        if(!isSet($obj['name'])) $obj['name'] = '';
        $uml = new UMLProperty($obj['xmi.id'], $obj['name'], $this->parent);
//        $uml->setValue($obj['']);
        return $this->parseGeneral($uml, $obj); 
        
    }
    
    public function parsePropertyDefinition($obj) {
        if (!isSet($obj['xmi.id']) && isSet($obj['xmi.idref'])){
            return UMLPropertyDefinition::getFromId($obj['xmi.idref'],$this->parent);
        }
        if(!isSet($obj['name'])) $obj['name'] = '';
        $uml = new UMLPropertyDefinition($obj['xmi.id'], $obj['name'], $this->parent);
        return $this->parseGeneral($uml, $obj); 
        
    }

    public function parseAbstraction($obj) {
        if (!isSet($obj['xmi.id']) && isSet($obj['xmi.idref'])){
            return UMLAbstraction::getFromId($obj['xmi.idref'],$this->parent);
        }
        if(!isSet($obj['name'])) $obj['name'] = '';
        $uml = new UMLAbstraction($obj['xmi.id'], $obj['name'], $this->parent);  
        return $this->parseGeneral($uml, $obj);         
    }

    public function parseEnumeration($obj) {
        if (!isSet($obj['xmi.id']) && isSet($obj['xmi.idref'])){
            return UMLEnumeration::getFromId($obj['xmi.idref'],$this->parent);
        }
        if(!isSet($obj['name'])) $obj['name'] = '';
        $uml = new UMLEnumeration($obj['xmi.id'], $obj['name'], $this->parent);  
        return $this->parseGeneral($uml, $obj);           
    }
    
    public function parseEnumerationLiteral($obj) {
        if (!isSet($obj['xmi.id']) && isSet($obj['xmi.idref'])){
            return UMLEnumerationLiteral::getFromId($obj['xmi.idref'],$this->parent);
        }
        if(!isSet($obj['name'])) $obj['name'] = '';
        $uml = new UMLEnumerationLiteral($obj['xmi.id'], $obj['name'], $this->parent);  
        return $this->parseGeneral($uml, $obj);              
    }

    public function parseComment($obj) {
        if (!isSet($obj['xmi.id']) && isSet($obj['xmi.idref'])){
            return UMLComment::getFromId($obj['xmi.idref'],$this->parent);
        }
        if(!isSet($obj['name'])) $obj['name'] = '';
        $uml = new UMLComment($obj['xmi.id'], $obj['name'], $this->parent);  
        return $this->parseGeneral($uml, $obj);          
    }
    
    public function parseElement($elemName, $obj) {
        $className = 'UML'.$elemName;
        if(!isSet($obj['name'])) $obj['name'] = '';
        if (!isSet($obj['xmi.id']) && isSet($obj['xmi.idref'])){
            return $className::getFromId($obj['xmi.idref'],$this->parent);
        }
        $uml = new $className($obj['xmi.id'], $obj['name'], $this->parent);
        return $this->parseGeneral($uml, $obj);
    }
}

class dataTypeParserArgo{
    private $parsedData = array();
    private $keys = array();
    static $dataTypeCache = [
        '120b548df95dae740b5f3c62132dfceb' => 'a:6:{s:6:"xmi.id";s:56:"-84-17--56-5-43645a83:11466542d86:-8000:000000000000087C";s:4:"name";s:7:"Integer";s:15:"isSpecification";s:5:"false";s:6:"isRoot";s:5:"false";s:6:"isLeaf";s:5:"false";s:10:"isAbstract";s:5:"false";}',
        '522a0f801f8d9e123ff12b63bce2ec27' => 'a:6:{s:6:"xmi.id";s:56:"-84-17--56-5-43645a83:11466542d86:-8000:000000000000087E";s:4:"name";s:6:"String";s:15:"isSpecification";s:5:"false";s:6:"isRoot";s:5:"false";s:6:"isLeaf";s:5:"false";s:10:"isAbstract";s:5:"false";}',
        '8395b5a341165fa33da68ca6f69e8b52' => 'a:6:{s:6:"xmi.id";s:56:"-84-17--56-5-43645a83:11466542d86:-8000:0000000000000880";s:4:"name";s:7:"Boolean";s:15:"isSpecification";s:5:"false";s:6:"isRoot";s:5:"false";s:6:"isLeaf";s:5:"false";s:10:"isAbstract";s:5:"false";}',
    ];
        
    function newRet($return, $url){
        if(is_array($return)){
            $this->keys[md5($url)] = $return;
            dataTypeParserArgo::$dataTypeCache[md5($url)] = serialize($return);
        }
        return $return;
    }
    
    function parseDatatype($url){
        $spl = explode('#', $url);
        $id = $spl[1];
        $dataUrl = $spl[0];
        if(isSet($this->keys[md5($url)])) return $this->keys[md5($url)];
        if(isSet($this->parsedData[$dataUrl])) return $this->newRet($this->findXmlKey ('xmi.id', $this->parsedData[$dataUrl], $id), $url);
        
        if(isset(dataTypeParserArgo::$dataTypeCache[md5($url)])){
            $row = [
                'url' => md5($url),
                'value' => dataTypeParserArgo::$dataTypeCache[md5($url)],
            ];
        } else {
            $row = null;
            ddd("pls add: ".md5($url)." manually:".$url, dataTypeParserArgo::$dataTypeCache);
        }
        if($row && isSet($row['url'])){
//            $row = mysql_fetch_assoc($res);
//            return unserialize($row);
            $data = unserialize($row['value']);
            $this->keys[$row['url']] = $data;
            return $data;
        }
        d($dataUrl);
        $xml = file_get_contents($dataUrl);
        $array =  xml2array($xml,1);
        $this->parsedData[$dataUrl] = $array;
//        d($array);
        $res = $this->findXmlKey('xmi.id', $array,$id);
//        d($res);
        $this->keys[md5($url)] = $res;
                
        return $this->newRet($res, $url);
    }

    function findXmlKey($attrName,$dataSet, $id){
        $ret = false;
//        if(isSet($dataSet[$attrName])){
//            echo 'CHECK: '.$attrName.':'.$dataSet[$attrName].'=='.$id;
////            new dBug($dataSet);
//        }
//        new dBug($dataSet);
        if(isSet($dataSet[$attrName]) && $dataSet[$attrName] == $id) return $dataSet;
        elseif(is_array($dataSet)){
            foreach ($dataSet as $set) {
                $ret = $this->findXmlKey($attrName, $set,$id);
                if($ret) return $ret;
            }
        }
        return false;
    }

}

?>
