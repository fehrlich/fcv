<?php
include('MysqlProperties.php');
include('MysqlTable.php');
include('MysqlField.php');
include('MysqlView.php');
class MysqlModel {
    /**
     *
     * @var UMLModel
     */
    private $umlModel;
    
    /**
     *
     * @var MysqlTable[]
     */
    private $tables = array();
    /**
     *
     * @var MysqlView[]
     */
    private $views = array();
    
    public $charset = 'utf8_general_ci';
    public $engine = 'INNODB';
    
    public $nmTables = array();
    
    static $defaultPK = 'INT UNSIGNED';
    static $defaultInt = 'INT';
    static $defaultString = 'VARCHAR(120)';
    static $defaultDecimal = 'DECIMAL(12,4)';
    
    function toArray(){
        $arr = array();
        $arr['charset'] = $this->charset;
        $arr['engine'] = $this->engine;
        $arr['tables'] = array();
        $arr['views'] = array();
        foreach($this->tables as $tables){
            $arr['tables'][] = $tables->toArray();
        }
        foreach($this->views as $tables){
            $arr['views'][] = $tables->toArray();
        }
        return $arr;
    }
    
    function __construct($umlModel) {
        $this->umlModel = $umlModel;
    }
    function parse(){
        foreach($this->umlModel->getAllClasses() as $class){
            if($class->isTable() && !$class->isInterface() && !$class->isEnum() && !$class->usesTypeParser() && !$class->hasStereoType("Connector")){
                
                $mysqlTable = new MysqlTable($class->getName());
                $mysqlTable->setUmlElement($class);
                
                foreach($class->getAttributes() as $attr){
                    if(!$attr->hasStereoType('noDBField') && !$attr->isStatic()){
                        $field = new MysqlField($attr->getName());
                        $field->setType(MysqlModel::parseType($attr));
                        $field->setIsPrimaryKey($attr->isPrimaryKey());
                        $fc = $attr->getType();
                        $field->setIsForeignKey($attr->isForeignKey(),$fc);
                        $field->setUmlElement($attr);
                        $mysqlTable->addField($field);
                    }
                }
                
                //AssoziantonsEnden für normale Klassen und NM Beziehungen
                foreach($class->getAssociationEnds() as $assocEnd){
                    if(!$assocEnd->isCardinalityZero() && $assocEnd->isCardinalitySingle() && $assocEnd->getParent()->debugType != 'UMLAssociationClass' && !$assocEnd->isStatic()){
                        $field = new MysqlField($assocEnd->getName());
//                        $field->setType(MysqlModel::parseType($assocEnd->getConnectionClass()->getPrimaryKeyAttribute(), true));
                        $field->setType(MysqlModel::parseType($assocEnd));
                        $field->setIsPrimaryKey($assocEnd->isPrimaryKey());
                        $field->setIsForeignKey($assocEnd->isForeignKey(),$assocEnd->getConnectionClass());
                        $field->setUmlElement($assocEnd);
                        $mysqlTable->addField($field);
                    }elseif(
                        $assocEnd->getParent()->debugType == 'UMLAssociation' && 
                        !$assocEnd->isCardinalitySingle() && !$assocEnd->getOppositeAssociationEnd()->isCardinalitySingle() && 
                        !in_array($assocEnd->getParent()->getName(), $this->nmTables)){
                        $this->nmTables[] = $assocEnd->getParent()->getName();
                        
                        
                        $mysqlTableNM = new MysqlTable($assocEnd->getParent()->getName());
                        $mysqlTableNM->setUmlElement($assocEnd->getParent());
                        
                        $field = new MysqlField(lcfirst($assocEnd->getClass()->getName()));
                        $field->setType(MysqlModel::parseType($assocEnd->getClass()->getPrimaryKeyAttribute(), true, true));
                        $field->setIsPrimaryKey(true);
                        $field->setIsForeignKey(true, $assocEnd->getClass(),'CASCADE', 'CASCADE');
                        $field->setUmlElement($assocEnd);
                        
                        $mysqlTableNM->addField($field);
                        
                        $field = new MysqlField(lcfirst($assocEnd->getOppositeAssociationEnd()->getClass()->getName()));
                        $field->setType(MysqlModel::parseType($assocEnd->getConnectionClass()->getPrimaryKeyAttribute(), true, true));
                        $field->setIsPrimaryKey(true);
                        $field->setIsForeignKey(true, $assocEnd->getConnectionClass(),'CASCADE', 'CASCADE');
                        $field->setUmlElement($assocEnd->getOppositeAssociationEnd());
                        $mysqlTableNM->addField($field);
                        $this->tables[] = $mysqlTableNM;
                    }
                }
//                
//                //Generalisierung
                if($class->specializeClasses()){
                    $mysqlTable->setName($class->getName().'_sub');
                    if(!$mysqlTable->getPrimaryFields()){
                        $gen = $class->getSpecializes("Table");
                        $gen = $gen[0];
                        $attr = $gen->getPrimaryKeyAttribute();
                        $pk = new MysqlField($attr->getName());
                        $pk->setIsPrimaryKey(true);
                        $pk->setIsForeignKey(TRUE, $gen, 'CASCADE', 'CASCADE');
                        $pk->setType(MysqlModel::parseType($attr));
                        $mysqlTable->addField($pk,true);
                    }
                    $mysqlView = new MysqlView($class->getName(),$mysqlTable);
                    $mysqlView->setUmlElement($class);
                    $mysqlView->setPrimaryKey($class->getPrimaryKeyAttribute()->getName());
                    $mysqlView->setConnectedTable($mysqlTable);
                    $this->views[] = $mysqlView;
                }else{
                    if($mysqlTable->hasFields()){
                        $lastMod = new MysqlField('lastModified');
                        $lastMod->setType('DATETIME');
                        $lastMod->setUmlElement(new UMLDataType('0','test', $class));
                        $mysqlTable->addField($lastMod);
                        
                        $created = new MysqlField('created');
                        $created->setType('DATETIME');
                        $created->setUmlElement(new UMLDataType('0','test', $class));
                        $mysqlTable->addField($created);
                        
                        $createdBy = new MysqlField('createdBy');
                        $createdBy->setType(static::$defaultPK);
                        $createdBy->setUmlElement(new UMLDataType('0','test', $class));
                        $mysqlTable->addField($createdBy);
                    }
                }
                
                if($class->hasStereoType('active')){
                    $activeField = new MysqlField('active');
                    $activeField->setUmlElement(new UMLDataType('0','test', $class));
                    $activeField->setType('TINYINT(1)');
                    $activeField->setDefault('1');
                    $mysqlTable->addField($activeField);
                }
                
                if($class->hasStereoType('History')){
                    $version = new MysqlField('version');
                    $version->setType(static::$defaultPK);
                    $version->setIsPrimaryKey(true);
                    $version->setUmlElement(new UMLDataType('0','test', $class));
                    
                    $historyTable = clone $mysqlTable;
                    
                    $historyTable->addField($version);
                    
                    $historyTable->setUmlElement($class);
                    $historyTable->setName($historyTable->getName().'History');
                    $this->tables[] = $historyTable;
                }
                
                $this->tables[] = $mysqlTable;
            }
        }
        
        //Assoziationsklassen
        foreach($this->umlModel->getAllAssociationClasses() as $class){
            
            $mysqlTable = new MysqlTable($class->getName());
            $mysqlTable->setUmlElement($class);

            foreach($class->getAttributes() as $attr){       
                if(!$attr->hasStereoType('noDBField') && !$attr->isStatic()){
                    $field = new MysqlField($attr->getName());
                    $field->setType(MysqlModel::parseType($attr));
                    $field->setIsPrimaryKey($attr->isPrimaryKey());
                    $field->setIsForeignKey($attr->isForeignKey(), $attr->getType());
                    $field->setUmlElement($attr);

                    $mysqlTable->addField($field);
                }
            }

            //AssoziantonsEnden
            foreach($class->getAssociationEnds() as $assocEnd){
                if(!$assocEnd->hasStereoType('noDBField')){
                    $field = new MysqlField(lcfirst($assocEnd->getClass()->getName()));
                    $field->setType(MysqlModel::parseType($assocEnd));
                    $assocClassHasPrimaryKey = $class->hasPrimaryKeyAttribute();
                    $field->setIsPrimaryKey(!$assocClassHasPrimaryKey);
                    $field->setIsForeignKey(true, $assocEnd->getClass(),'CASCADE', $assocClassHasPrimaryKey ? 'SET NULL' : 'CASCADE');
                    $field->setUmlElement($assocEnd);

                    $mysqlTable->addField($field);
                }
            }
            if($mysqlTable->hasFields()){
                $lastMod = new MysqlField('lastModified');
                $lastMod->setType('DATETIME');
                $lastMod->setUmlElement(new UMLDataType('0','test', $class));
                $mysqlTable->addField($lastMod);
                $created = new MysqlField('created');
                $created->setType('DATETIME');
                $created->setUmlElement(new UMLDataType('0','test', $class));
                $mysqlTable->addField($created);

                $createdBy = new MysqlField('createdBy');
                $createdBy->setType(static::$defaultPK);
                $createdBy->setUmlElement(new UMLDataType('0','test', $class));
                $mysqlTable->addField($createdBy);
            }
            $this->tables[] = $mysqlTable;
        }
    }
    /**
     * 
     * @param UMLDataType $type
     */
    static function parseType($type, $isForeignKey = false, $multiPrimaryKeys = false){
        if(!method_exists($type, 'isPrimaryKey')) d(1);
        $prim = $type->isPrimaryKey() && !$isForeignKey;
        
        if($type->isForeignKey()){
            $primaryKey = $type->getType()->getPrimaryKeyAttribute();
            if($primaryKey === false){
                echo 'Error: '.$type->getType()->getName().' has no Primary Key for Foreign Key in '.$type->getName();
                return Settings::getObj()->defaultMysqlPrimaryKeayType();
            }
            return MysqlModel::parseType($primaryKey, true);
        }
        if($type->hasInheritedProperty('DbType')){            
            $propertyValue = $type->getInheritedProperty('DbType');
            return $propertyValue;
        }
        if($type->getType()->hasInheritedProperty('DbType')){            
            $propertyValue = $type->getType()->getInheritedProperty('DbType');
            return $propertyValue;
        }
        
        if($type->getType()->debugType == 'UMLEnumeration'){
            $literals = array();
            foreach($type->getType()->getLiterals(true) as $lit){
                $literals[] = $lit->getName();
            }
            return "ENUM('".implode("','", $literals)."')";
        }
        
        if($type->isDefaultType()){
            $typeName = $type->getType()->getName();
            if($typeName == 'Boolean') return 'TINYINT(1)';
            elseif($typeName == '' && $type->isPrimaryKey()) return static::$defaultPK.(($prim && !$multiPrimaryKeys)?' AUTO_INCREMENT':'');
            elseif($typeName == '' && $isForeignKey){
                return static::$defaultPK;
            }
            elseif($typeName == 'Integer'){
                if($prim && !$multiPrimaryKeys){
                    return static::$defaultPK.' AUTO_INCREMENT';
                }
                if($multiPrimaryKeys || $isForeignKey){
                    return static::$defaultPK;
                }
                return static::$defaultInt;
            }elseif($type->getType()->getName() == 'Date') return 'DATE';
            elseif($type->getType()->getName() == 'Time') return 'TIME';
            elseif($type->getType()->getName() == 'DateTime') return 'DATETIME';
            elseif($type->getType()->getName() == 'Float' || $type->getType()->getName() == 'float'){
                return static::$defaultDecimal;
            }
            elseif($type->getType()->getName() == 'Text' || $type->getType()->getName() == 'text'){
                return 'TEXT';
            }
            return static::$defaultString;
        }elseif($type->isClass()){
            if($type->getType()->getName() == 'Date') return 'DATE';
            elseif($type->getType()->getName() == 'Time') return 'TIME';
            elseif($type->getType()->getName() == 'DateTime') return 'DATETIME';
            elseif($type->getType() == 'Boolean') return 'TINYINT(1)';
            elseif($type->getType() == 'Integer'){
                return (($prim && !$multiPrimaryKeys)?static::$defaultPK.' AUTO_INCREMENT':static::$defaultInt);
            }else{
                $primaryKey = $type->getType()->getPrimaryKeyAttribute();
                if($primaryKey === false){
                    d($type);
                    return static::$defaultString;
                }
                return MysqlModel::parseType($primaryKey, true);
            }    
        }
        
        return static::$defaultString;
    }
    
    public function getTables() {
        return $this->tables;
    }
    
    public function getForeignKeyTables(){
        $tables = array();
        foreach($this->tables as $table){
            $fields = $table->getForeignFields();
            if($fields) $tables[] = $table;
        }
//        foreach($tables as $t) new dBug($t->getForeignKeys());
        return $tables;
    }
    public function getViews() {
        return $this->views;
    }
}
