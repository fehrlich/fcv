<?php

class MysqlField extends MysqlProperties{
    public $first = false;
    
     private $type;
     private $isPrimaryKey = false;
     private $isForeignKey = false;
     private $foreignClass = false;
     private $table;
     
     private $default = '';
     
     private $onUpdate = '';
     private $onDelete = '';
    
     

     public function setType($type) {
         $this->type = $type;
     }

     public function setIsPrimaryKey($isPrimaryKey) {
         $this->isPrimaryKey = $isPrimaryKey;
     }

     public function setIsForeignKey($isForeignKey, $foreignClass, $onUpdate = 'CASCADE', $onDelete = 'SET NULL') {
         $this->isForeignKey = $isForeignKey;
         $this->foreignClass = ($foreignClass instanceof UMLClass) ? $foreignClass->getFirstTableClass() : $foreignClass;
         $this->onUpdate = $onUpdate;
         $this->onDelete = $onDelete;
     }
     
     public function isPrimaryKey() {
         return $this->isPrimaryKey;
     }

     public function isForeignKey() {
         return $this->isForeignKey;
     }
     
     public function getType() {
         return $this->type;
     }
     
     public function getTable() {
         return $this->table;
     }

     public function setTable($table) {
         $this->table = $table;
     }
     public function getForeignClass() {
         return $this->foreignClass;
     }
     
     public function getDefault() {
         return $this->default;
     }

     public function setDefault($default) {
         $this->default = $default;
     }
     
     function getOnUpdate() {
         return $this->onUpdate;
     }

     function getOnDelete() {
         return $this->onDelete;
     }
     
    public function toArray() {
        $arr = array();
        $arr['name'] = $this->name;
        $arr['type'] = $this->type;
        $arr['foreign'] = $this->isForeignKey();
        $arr['primary'] = $this->isPrimaryKey();
        return $arr;
    }
}

?>
