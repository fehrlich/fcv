<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MysqlProperties
 *
 * @author XzenTorXz
 */
class MysqlProperties {
    protected $name = '';
    protected $umlElement = '';
//    protected $new = false;
//    protected $changed = false;
    
    function __construct($name) {
        $this->name = $name;
    }
    
    function toArray(){
        $arr = array();
        $arr['name'] = $this->name;
        return $arr;
    }
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }
    public function getUmlElement() {
        return $this->umlElement;
    }

    public function setUmlElement($umlElemnt) {
        $this->umlElement = $umlElemnt;
    }
    public function isNew(){
        return $this->new;
    }    
}

?>
