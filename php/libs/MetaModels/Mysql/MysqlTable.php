<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MysqlTable
 *
 * @author XX
 */
class MysqlTable extends MysqlProperties{
    
    /**
     *
     * @var MysqlField[]
     */
    private $fields = array();
    
    function addField($field, $atBeginning = false){
        $field->first = (count($this->fields) == 0);
        $field->setTable($this);
        if($atBeginning && !$field->first){
            $field->first = true;
            $this->fields[0]->first = false;
            array_unshift($this->fields, $field);
        }
        else $this->fields[] = $field;
    }

    public function getFields() {
        $ret = array();
        $first = true;
        foreach($this->fields as $f){
            $f->first = $first;
            $ret[] = $f;
            $first = false;
        }
        return $this->fields;
    }
    
    public function getPrimaryFields(){
        $keys = array();
        foreach($this->fields as $f){
            if($f->isPrimaryKey()) $keys[] = $f;
        }
        if(count($keys) == 0) return false;
        return $keys;
    }
    public function getPrimaryFieldsList(){
        $keys = array();
        $index = 0;
        foreach($this->fields as $f){
            if($f->isPrimaryKey()){ 
                $keys[] = array(
                    'index' => $index,
                    'even' => $index%2,
                    'first' => $index==0,
                    'value' => $f
                );
                $index++;
            }
        }
        if(count($keys) == 0) return false;
        
        return $keys;
    }
    public function hasPrimaryField(){
        $keys = $this->getPrimaryFields();
        return is_array($keys);
    }
    public function getForeignFields(){
        $ret = array();
        foreach($this->fields as $f){
            if($f->isForeignKey()) $ret[] = $f;
        }
        if(isset($ret[0])) $ret[0]->first = true;
//        new dBug($ret);
        return (count($ret) == 0)?false:$ret;
    }

    public function toArray() {
        $arr = array();
        $arr['name'] = $this->name;
        $arr['fields'] =array();
        foreach($this->fields as $f){
            $arr['fields'][] = $f->toArray();
        }
        return $arr;        
    }
    public function hasFields(){
        return (count($this->fields) > 0);
    }
}

?>
