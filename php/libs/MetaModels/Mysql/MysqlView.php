<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MysqlView
 *
 * @author XX
 */
class MysqlView extends MysqlTable{
    private $primaryKey = "";
    private $table;
    private $connectedTable = false;
    
    function __construct($name, $table) {
        $this->table = $table;
        parent::__construct($name);
    }    
    
    public function getPrimaryKey() {
        return $this->primaryKey;
    }

    public function setPrimaryKey($primaryKey) {
        $this->primaryKey = $primaryKey;
    }
    public function getPrimaryFields() {
        new dBug($this->table->toArray());
        new dBug($this->table->getPrimaryFields()->toArray());
        $ret = parent::getPrimaryFields();
        if($ret) return $ret;
        return $this->table->getPrimaryFields();
    }
    public function getConnectedTable() {
        return $this->connectedTable;
    }
    
    public function getTable() {
        return $this->table;
    }
    
    public function setConnectedTable(&$connectedTable) {
        $this->connectedTable = $connectedTable;
    }
    
    public function getAllGeneralizations($umlElement = false, $index = 0){
        $ret = array();
        $parentIndex = ($index == 0)?'':$index;
        if(!$umlElement) $umlElement = $this->getUmlElement ();
        foreach($umlElement->getSpecializes() as $specClass){
            if($specClass->isTable()){
                $isView = (count($specClass->getSpecializes()) > 0);
                $add = array(
                    'name' => $specClass->getName().(($isView)?'_sub':''),
                    'primaryKey' => $specClass->getPrimaryKeyAttribute()->getName(),
                    'primaryKeyParent' => $umlElement->getPrimaryKeyAttribute()->getName(),
                    'index' => $index+1,
                    'indexParent' => $parentIndex,
                    'first' => ($index == 0),
                    'fields' => array()
                );
                $firstAttr = true;
                foreach($specClass->getAttributes() as $attr){
                    if(!$attr->isPrimaryKey() && !$attr->hasStereoType('noDBField') && !$attr->isStatic()) $add['fields'][] = array(
                        'firstField' => $firstAttr,
                        'name' => $attr->getName()
                    );
                    $firstAttr = false;
                }
                if($specClass->hasActive()){
                    $add['fields'][] = array(
                        'firstField' => $firstAttr,
                        'name' => 'active'
                    );
                    $firstAttr = false;
                }
                foreach($specClass->getAssociationEnds() as $assocEnd){
                    if(!$assocEnd->isCardinalityZero() && $assocEnd->isCardinalitySingle() && $assocEnd->getParent()->debugType != 'UMLAssociationClass'){
                        $add['fields'][] = array(
                            'firstField' => $firstAttr,
                            'name' => $assocEnd->getName()
                        );
                        $firstAttr = false;
                    }
                }
                $ret[] = $add;
                $index++;
            }
            $ret = array_merge($ret, $this->getAllGeneralizations($specClass, $index));
        }
        return $ret;
    }
}

?>
