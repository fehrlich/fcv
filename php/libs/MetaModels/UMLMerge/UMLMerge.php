<?php

use fcv\libs\Debug;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UMLMerge
 *
 * @author XX
 */
class UMLMerge {
    /**
     *
     * @var UMLModel
     */
    private $oldModel;
    /**
     *
     * @var UMLModel
     */
    private $newModel;
    
    private $newElements;
    
    function __construct($oldModel, $newModel) {
        $this->oldModel = $oldModel;
        $this->newModel = $newModel;
    }
    
    function merge(){
//        $umlModelNew = &$this->newModel;
//        $umlModelNew = &$this->oldModel;
        $self = &$this;
        $this->newModel->iterate(
            function($umlElement) use(&$self){
            
            
            $id = $umlElement->getId();
            if($oldEl = $self->getOldModel()->getElementFromId($id)){
//                $umlElement->updateStatus = 'NEW';
                //Todo: All Properties (fcvFields)
                if($oldEl->getName() != $umlElement->getName()){
                    Debug::getObj()->info($umlElement->getName().' CHANGED FROM '.$oldEl->getName());
                    $umlElement->updateStatus = 'CHANGE';
                }
            }else{
                Debug::getObj()->info($umlElement->getName().' IS NEW');
                $umlElement->updateStatus = 'NEW';
                $parent = $umlElement->getParent();
                while($parent){
                    if($parent->updateStatus == ''){
                        $parent->updateStatus = 'CHANGE';
                        $parent->setOldUmlElement($self->getOldModel()->getElementFromId($parent->getId()));
                    }
                    $parent = $parent->getParent();
                }
            }
        });
    }
    public function getOldModel() {
        return $this->oldModel;
    }

    public function getNewModel() {
        return $this->newModel;
    }
}

?>
