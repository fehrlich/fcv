<?php

use impro\kontakte\AuftraggeberCollectionPrepare;
use impro\kontakte\FirmaCollectionPrepare;
use impro\kontakte\ImproBueroCollectionPrepare;
use impro\kontakte\ImproMitarbeiterCollectionPrepare;
use impro\kontakte\KontaktBetreuungCollectionPrepare;
use impro\kontakte\KontaktCollectionPrepare;
use impro\kontakte\PartnerMaklerCollectionPrepare;
use impro\kontakte\StandortCollectionPrepare;
use impro\kontakte\StandortKontaktDatenCollectionPrepare;
use impro\kontakte\VertriebsPartnerCollectionPrepare;
use impro\login\LoginUserCollectionPrepare;
use impro\objekte\AuftragBearbeitungCollectionPrepare;
use impro\objekte\AuftragCollectionPrepare;
use impro\objekte\ObjektCollectionPrepare;
use impro\objekte\ObjektDateiTypCollectionPrepare;
use impro\objekte\VeroeffentlichungCollectionPrepare;
use impro\rechnungen\RechnungCollectionPrepare;
use impro\typen\AdresseCollectionPrepare;
use impro\typen\KategorieCollectionPrepare;
use impro\webapps\stundenabrechnung\StundenAbrechnungsEintragCollectionPrepare;
use impro\monatsangebot\MailingObjektCollectionPrepare;

//<editor-fold defaultstate="collapsed" desc="CollectionPrepares">

$firmaRights = [
    'firmenId' => ['operations' => ['in', 'select', '=', 'where']],
    'firmenName' => [
        'operations' => ['select', 'like', 'regexp', 'where']
    ],
    'standorte' => [
        'operations' => ['set', 'select']
    ],
    'kurzform' => [
        'operations' => ['select']
    ],
    'auftraggeber' => [
        'operations' => ['select', 'isNot']
    ],
    'partnerMakler' => [
        'operations' => ['select', 'isNot']
    ],
];

$standortRights = [
    'standortId' => [
        'operations' => ['=', 'select', 'where']
    ],
    'firma' => [
        'operations' => ['=', 'is', 'set', 'select', 'where']
    ],
    'standortKontaktDaten' => [
        'operations' => ['set']
    ],
    'hauptSitz' => [
        'operations' => ['=', 'where', 'select']
    ],
    'adresse' => [
        'operations' => ['set', 'is', 'where']
    ],
    'telefon' => [
        'operations' => ['select']
    ],
    'fax' => [
        'operations' => ['select']
    ],
    'email' => [
        'operations' => ['select']
    ],
    'active' => [
        'operations' => ['=', 'where']
    ]
];

//TODO: remove selectAll in JS
$whitelisteCollectionPrepares = [
     LoginUserCollectionPrepare::class => [
        'kontaktId' => [
            'operations' => ['select']
        ],
        'kuerzel' => [
            'operations' => ['select', 'is', 'where']
        ]
    ],
    FirmaCollectionPrepare::class => $firmaRights,
    AdresseCollectionPrepare::class => [
//        'selectAll',
        'adressId' => ['operations' => ['select', '=', 'where']],
//        'land' => ['operations' => ['select']],
        'ort' => [
            'operations' => ['select', 'is', '=', '!=', 'like', 'regexp', 'where']
        ],
        'geoLat' => [
            'operations' => ['select', 'is', '<>', 'where']
        ],
        'geoLon' => [
            'operations' => ['select', 'is', '<>', 'where']
        ],
        'plz' => [
            'operations' => ['select']
        ],
        'strasse' => [
            'operations' => ['select']
        ],
        'hausNummer' => [
            'operations' => ['select']
        ],
        'ortsTeil' => [
            'operations' => ['select']
        ],
        'land' => [
            'operations' => ['select']
        ],
    ],
    ObjektDateiTypCollectionPrepare::class => [
        'ObjektDateiTypId' => ['operations' => ['select', '=', 'where']],
        'name' => [
            'operations' => ['select']
        ]
    ],
    ObjektCollectionPrepare::class => [
        'objektId' => ['operations' => ['select', '=', 'where']],
        'verfahren' => [
            'operations' => ['select']
        ],
        'archiviert' => [
            'operations' => ['select', 'is', 'where']
        ],
        'eigenschaften' => [
            'operations' => ['select', '~&', '&', 'where']
        ],
        'objektArt' => [
            'operations' => ['set']
        ],
        'veroeffentlichung' => [
            'operations' => ['set']
        ],
        'adresse' => [
            'operations' => ['set']
        ],
        'auftragBearbeitung' => [
            'operations' => ['set']
        ],
        'objektNummer' => [
            'operations' => ['select']
        ],
        'auftrag' => [
            'operations' => ['set']
        ],
        'mailingObjekte' => [
            'operations' => ['set']
        ],
    ],
    AuftragCollectionPrepare::class => [
        'firmenId' => [
            'operations' => ['where']
        ],
        'auftragsId' => [
            'operations' => ['select']
        ],
        'auftraggeber' => [
            'operations' => ['in', 'set']
        ]
    ],
    KategorieCollectionPrepare::class => [
        'kategorieId' => ['operations' => ['select', '=', 'where']],
        'name' => ['operations' => ['select', '=', '!=', 'where']],
        'parent' => ['operations' => ['set', 'where']]
    ],
    VeroeffentlichungCollectionPrepare::class => [
        'veroeffentlichungsId' => ['operations' => ['select', '=', 'where']],
        'veroeffentlicht' => ['operations' => ['select']],
    ],
    AuftragBearbeitungCollectionPrepare::class => [
        'objekt' => ['operations' => ['select', '=', 'set', 'where']],
        'improBuero' => ['operations' => ['select', '=', 'set', 'where']],
        'improBueroBearbeitung' => [
            'operations' => ['set']
        ],
        'sachbearbeiter' => [
            'operations' => ['set']
        ]
    ],
    KontaktCollectionPrepare::class => [
        'loginUser' => [
            'operations' => ['set']
        ],
        'kontaktId' => ['operations' => ['select', '=', 'where']],
        'nachname' => [
            'operations' => ['select', 'like', '=', 'where']
        ],
        'vorname' => [
            'operations' => ['select', 'like','=', 'where']
        ],
        'titel' => [
            'operations' => ['select']
        ],
        'geschlecht' => [
            'operations' => ['select']
        ],
        'standortKontaktDaten' => [
            'operations' => ['set']
        ],
        'standorte' => [
            'operations' => ['set']
        ],
        'eigenschaften' => [
            'operations' => ['&', 'where']
        ],
        'privatAdresse' => [
            'operations' => ['set', 'is', 'where']
        ],
        'typ' => [
            'operations' => ['=', 'like', 'where']
        ],
        'kontaktBetreuung' => [
            'operations' => ['set']
        ],
        'geburtstag' => [
            'operations' => ['select']
        ],
        'created' => [
            'operations' => ['select']
        ],
        'active' => [
            'operations' => ['=', 'where']
        ],
        'email' => [
            'operations' => ['select']
        ],
        'telefon' => [
            'operations' => ['select']
        ],
    ],
     ImproMitarbeiterCollectionPrepare::class => [
        'kontakt' => [
            'operations' => ['set']
        ],
        'kontaktId' => [
            'operations' => ['select']
        ],
     ],
    AuftraggeberCollectionPrepare::class => $firmaRights + [
        'typ' => [
            'operations' => ['!=', '=', 'select', 'where']
        ],
        'potentiell' => [
            'operations' => ['!=', '=', 'where']
        ],
    ],
    KontaktBetreuungCollectionPrepare::class => [
        'betreuungsId' => [
            'operations' => ['select']
        ],
        'improBuero' => [
            'operations' => ['set', 'select']
        ],
        'kontakt' => [
            'operations' => ['set', 'select']
        ],
        'improSachbearbeiter' => [
            'operations' => ['set']
        ],
    ],
    StandortKontaktDatenCollectionPrepare::class => [
        
        'standortKontaktId' => [
            'operations' => ['set', '=', 'select', '>', 'where']
        ],
        
        'standort' => [
            'operations' => ['set', '=', 'select', '>', 'where']
        ],
        'standorte' => [
            'operations' => ['set', '=', 'select', '>', 'where']
        ],
        'kontakt' => [
            'operations' => ['=', 'set', 'select', 'where']
        ],
        'insolvenzverwalter' => [
            'operations' => ['=', 'select', 'where']
        ],
        'restrukturierer' => [
            'operations' => ['=', 'select', 'where']
        ],
        'sachbearbeiter' => [
            'operations' => ['=', 'or', 'select', 'where']
        ],
        'vertriebsPartner' => [
            'operations' => ['=', 'where']
        ],
        'telefon' => [
            'operations' => ['select']
        ],
        'fax' => [
            'operations' => ['select']
        ],
        'email' => [
            'operations' => ['select']
        ],
        'mobil' => [
            'operations' => ['select']
        ],
        'nested' => true
    ],
    RechnungCollectionPrepare::class => [
        'rechnungsId' => ['operations' => ['select', '=', 'where']],
        'betrag' => [
            'operations' => ['select']
        ],
        'rechnungsNummer' => [
            'operations' => ['select', 'like', 'where']
        ],
    ],
    PartnerMaklerCollectionPrepare::class => $firmaRights + [
        'sonstige' => [
            'operations' => ['=', 'where']
        ],
        'kuerzel' => [
            'operations' => ['select']
        ],
        'kooperationsPartner' => [
            'operations' => ['select']
        ],
    ],
    ImproBueroCollectionPrepare::class => $standortRights+ [
        'regional' => [
            'operations' => ['=', 'select', 'where']
        ],
        'virtuell' => [
            'operations' => ['select']
        ],
        'aufKarteZeigen' => [
            'operations' => ['=', 'where']
        ],
        'bezeichnung' => [
            'operations' => ['select']
        ],
    ],
    StandortCollectionPrepare::class => $standortRights,
    
    StundenAbrechnungsEintragCollectionPrepare::class => [
        'eintragsId' => [
            'operations' => ['=', 'select', 'where']
        ],
        'createdBy' => [
            'operations' => ['select']
        ],
        'lastModified' => [
            'operations' => ['select']
        ],
        'created' => [
            'operations' => ['select']
        ],
        'projekt' => [
            'operations' => ['=', 'select', 'where']
        ],
        'user' => [
            'operations' => ['in','=', 'set', 'select', 'where']
        ],
        'von' => [
            'operations' => ['=', '<=', '>=', 'select', 'where']
        ],
        'bis' => [
            'operations' => ['=', '<=', '>=', 'select', 'where']
        ],
        'was' => [
            'operations' => ['=', 'select', 'where']
        ],
        'wo' => [
            'operations' => ['select']
        ]
    ],
    VertriebsPartnerCollectionPrepare::class => [
        'vertriebsPartnerId' => [
            'operations' => ['=', 'select', 'where']
        ],
        'main' => [
            'operations' => ['=', 'where']
        ],
        'firma' => [
            'operations' => ['set']
        ],
        'standortKontaktDaten' => [
            'operations' => ['set', 'or']
        ],
    ],
    MailingObjektCollectionPrepare::class => [
        'angebotId' => [
            'operations' => ['=', 'select', 'where']
        ],
        'type' => [
            'operations' => ['=', 'where', 'select']
        ],
        'versendet' => [
            'operations' => ['=', 'where', '!=', 'select']
        ],
    ],
];
