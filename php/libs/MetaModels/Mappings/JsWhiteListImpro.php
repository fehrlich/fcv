<?php

use fcv\App;
use fcv\CollectionPrepare;
use fcv\exceptions\NotLoggedInException;
use fcv\exceptions\SecurityException;
use impro\dateien\FormularVorlage;
use impro\filesystem\FileSystemNode;
use impro\kontakte\Auftraggeber;
use impro\kontakte\AuftraggeberCollectionPrepare;
use impro\kontakte\Firma;
use impro\kontakte\FirmaCollectionPrepare;
use impro\kontakte\ImproBuero;
use impro\kontakte\ImproBueroCollectionPrepare;
use impro\kontakte\ImproMitarbeiterCollectionPrepare;
use impro\kontakte\Kontakt;
use impro\kontakte\KontaktBetreuung;
use impro\kontakte\KontaktBetreuungCollectionPrepare;
use impro\kontakte\KontaktCollectionPrepare;
use impro\kontakte\KontaktDatei;
use impro\kontakte\PartnerMakler;
use impro\kontakte\PartnerMaklerCollectionPrepare;
use impro\kontakte\Standort;
use impro\kontakte\StandortBetreuung;
use impro\kontakte\StandortCollectionPrepare;
use impro\kontakte\StandortKontaktDaten;
use impro\kontakte\StandortKontaktDatenCollectionPrepare;
use impro\kontakte\SuchAuftrag;
use impro\kontakte\VertriebsPartner;
use impro\kontakte\VertriebsPartnerCollectionPrepare;
use impro\kontakte\VertriebsPartnerObjekte;
use impro\login\LoginUser;
use impro\login\LoginUserCollectionPrepare;
use impro\messages\Nachricht;
use impro\modules\FormMailer;
use impro\monatsangebot\MailingObjekt;
use impro\monatsangebot\Monatsangebot;
use impro\objekte\Auftrag;
use impro\objekte\AuftragBearbeitung;
use impro\objekte\AuftragBearbeitungCollectionPrepare;
use impro\objekte\AuftragCollectionPrepare;
use impro\objekte\BewertungsEintrag;
use impro\objekte\Expose;
use impro\objekte\Gebot;
use impro\objekte\GrundbuchEintrag;
use impro\objekte\GrundbuchLast;
use impro\objekte\GrundpfandEintrag;
use impro\objekte\InfoMaterial;
use impro\objekte\MietePachtEintrag;
use impro\objekte\Objekt;
use impro\objekte\ObjektCollectionPrepare;
use impro\objekte\ObjektDatei;
use impro\objekte\ObjektDateiTypCollectionPrepare;
use impro\objekte\Veroeffentlichung;
use impro\objekte\VeroeffentlichungCollectionPrepare;
use impro\rechnungen\Rechnung;
use impro\rechnungen\RechnungCollectionPrepare;
use impro\rechnungen\RechnungenStatistik;
use impro\rechnungen\RechnungsPosten;
use impro\rechnungen\ReferenzEmpfaenger;
use impro\rechnungen\Teilzahlung;
use impro\tools\InstancePdf;
use impro\typen\Adresse;
use impro\typen\AdresseCollectionPrepare;
use impro\typen\KategorieCollectionPrepare;
use impro\typen\Notiz;
use impro\ui\DataTable;
use impro\ui\SimpleDataTable;
use impro\webapps\fotoalbum\AlbumBild;
use impro\webapps\fotoalbum\Fotoalbum;
use impro\webapps\stundenabrechnung\StundenAbrechnungsEintrag;
use impro\webapps\stundenabrechnung\StundenAbrechnungsEintragCollectionPrepare;
use impro\webapps\stundenabrechnung\StundenProjekt;
use impro\typen\Bankverbindung;

// <editor-fold defaultstate="collapsed" desc="js method dispatch">

$jsDispatchMap = [
    Veroeffentlichung::class => [
        'veroeffentlichen' => [''],
        'save' => [],
        'setNull' => ['fields']
    ],
    InfoMaterial::class => [
        'save' => [],
        'setNull' => ['fields']
    ],
    Gebot::class => [
        'save' => [],
        'setNull' => ['fields']
    ],
    InstancePdf::class => [
        'action' => []
    ],
    DataTable::class => [
        'toPdf' => [],
        'toExcel' => []
    ],
    Fotoalbum::class => [
        'save' => [],
        'delete' => [],
        'setNull' => ['fields']
    ],
    ReferenzEmpfaenger::class => [
        'save' => [],
        'delete' => [],
        'setNull' => ['fields']
    ],
    AlbumBild::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => [],
        'handleUpload' => [],
    ],
    Nachricht::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => [],
        'handleUpload' => []
    ],
    StundenProjekt::class => [
        'updateStundenProjekte' => ['user', 'projectIds'],
        'save' => [],
        'setNull' => ['fields'],
        'delete' => []
    ],
    StundenAbrechnungsEintrag::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => []
    ],
    FileSystemNode::class => [
        'deleteFile' => [],
        'delete' => []
    ],
    KontaktDatei::class => [
        'deleteFile' => [],
        'save' => [],
        'setNull' => ['fields'],
        'delete' => [],
        'handleUpload' => []
    ],
    ObjektDatei::class => [
        'deleteFile' => [],
        'save' => [],
        'setNull' => ['fields'],
        'delete' => [],
        'handleUpload' => []
    ],
    Objekt::class => [
        'createPrototype' => [],
        'kopieren' => ['cb'],
        'save' => [],
        'setNull' => ['fields'],
        'saveFlaechenByName' => ['array']
    ],
    Firma::class => [
        'createPrototype' => [],
        'softDelete' => [],
        'save' => [],
        'addPm' => [],
        'addAg' => [],
    ],
    Auftraggeber::class => [
        'save' => [],
        'setNull' => ['fields'],
    ],
    Kontakt::class => [
        'createPrototype' => ['name', 'vorname'],
        'softDelete' => [],
        'save' => [],
    ],
    KontaktBetreuung::class => [
        'save' => [],
    ],
    FormMailer::class => [
        'send' => []
    ],
    MailingObjekt::class => [
        'newOrder' => ['int']
    ],
    StandortBetreuung::class => [
        'save' => [],
        'saveNew' => [],
        'setNull' => ['fields'],
        'delete' => []
    ],
    FormularVorlage::class => [
        'save' => [],
        'setNull' => ['fields'],
        'softDelete' => []
    ],
    Expose::class => [
        'save' => [],
        'setNull' => ['fields'],
    ],
    Notiz::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => []
    ],
    Auftrag::class => [
        'save' => [],
//        'delete' => [],
        'setNull' => ['fields']
    ],
    AuftragBearbeitung::class => [
        'save' => [],
        'setNull' => ['fields'],
//        'delete' => []
    ],
    VertriebsPartnerObjekte::class => [
        'save' => [],
        'setNull' => ['fields'],
//        'delete' => []
    ],
    Standort::class => [
        'save' => [],
        'setNull' => ['fields'],
        'softDelete' => []
    ],
    StandortKontaktDaten::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => []
    ],
    Adresse::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => [],
        'searchOrt' => ['plz'],
    ],
    GrundbuchEintrag::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => []
    ],
    GrundbuchLast::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => []
    ],
    GrundpfandEintrag::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => []
    ],
    MietePachtEintrag::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => []
    ],
    BewertungsEintrag::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => []
    ],
    RechnungenStatistik::class => [
        'save' => [],
        'setNull' => ['fields'],
    ],
    Rechnung::class => [
        'save' => [],
        'setNull' => ['fields'],
        'createFromObjekt' => ["static", ''],
        'createPrototype' => ['improBuero'],
        'filterRechnungen' => ['typ', 'ort', 'sb', 'jahr', 'monat', 'intern', 'includeHolding', 'ag']
    ],
    RechnungsPosten::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => []
    ],
    Teilzahlung::class => [
        'save' => [],
        'setNull' => ['fields'],
        'delete' => []
    ],
    PartnerMakler::class => [
        'save' => [],
        'setNull' => ['fields'],
    ],
    VertriebsPartner::class => [
        'save' => [],
        'setNull' => ['fields'],
    ],
     SimpleDataTable::class => [
         'toPdf' => []
     ],
     LoginUser::class => [
        'checkIfLoggedIn' => [],
         'forceLoginAs' => ['loginUser'] //rights are checked within the method
     ],
     SuchAuftrag::class => [
         'save' => [],
        'setNull' => ['fields'],
         'delete' => [],
     ],
//    PdfTemplate::class => [
//        'save' => []
//    ],
];
$jsDispatchMap[LoginUser::class]['save'] = [];
$jsDispatchMap[MailingObjekt::class]['save'] = [];
$jsDispatchMap[MailingObjekt::class]['setOrderNr'] = ['nr'];
$jsDispatchMap[MailingObjekt::class]['delete'] = [];
$jsDispatchMap[Kontakt::class]['overwriteMonatsmail'] = [];

$jsDispatchMap[ImproBuero::class] = [
    'save' => [],
    'softDelete' => []
];
$jsDispatchMap[BankVerbindung::class] = [
    'save' => [],
    'softDelete' => []
];
$jsDispatchMap[Monatsangebot::class]['send'] = ['onlyTest'];
$jsDispatchMap[Monatsangebot::class]['sendTest'] = [];
$jsDispatchMap[Monatsangebot::class]['getStatus'] = [];

// </editor-fold>
