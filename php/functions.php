<?php

function stripCommentsPHP($str)
{
    $newStr = '';

    if(is_null($str)) {
        return '';
    }

    $commentTokens = array(T_COMMENT);

    if (defined('T_DOC_COMMENT')) {
        $commentTokens[] = T_DOC_COMMENT;
    }
    // PHP 5
    if (defined('T_ML_COMMENT')) {
        $commentTokens[] = T_ML_COMMENT;
    }
    // PHP 4

    $tokens = token_get_all($str);

    foreach ($tokens as $i => $token) {
        if (is_array($token)) {
            if (in_array($token[0], $commentTokens)) {
                continue;
            }

            $token = $token[1];
        }

        $newStr .= $token;
    }

    return $newStr;
}

function stripCommentsJS($str)
{
    if (is_null($str)) {
        return '';
    }
    // remove comments
    return preg_replace('/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\|\')\/\/.*))/', '', $str); //Yancharuk's code/regex
}

function writeContentToFile($path, $content)
{
    $dir = dirname($path);
    $check = true;
    $writeFile = true;
    if (!is_dir($dir)) {
        @mkdir($dir, 0777, true);
        $check = false;
    }
    if ($check) {
        if (file_exists($path)) {
            $md5File = hash_file('md5', $path);
            $md5Content = md5($content);
            if ($md5File == $md5Content) {
                $writeFile = false;
            }
        }
    }
    if ($writeFile) {
        file_put_contents($path, $content);
    }
}
