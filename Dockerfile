FROM php:8.1-cli as build

WORKDIR /project

RUN apt-get update \
    && apt-get -y upgrade \
    && apt-get install -y git

COPY *.php /project/
COPY moduleTemplates /project/moduleTemplates/
COPY php /project/php/
COPY bin /project/
RUN ./install-composer.sh
COPY config/php.ini /usr/local/etc/php/
COPY composer.json /project/

RUN php composer.phar --no-dev install

FROM php:8.1-cli as fcv
WORKDIR /project
EXPOSE 22

# RUN apt-get -y update \
#     && apt-get install -y openssh-server

COPY --from=build /project /project
COPY config/php.ini /usr/local/etc/php/
# COPY config/ssh/* /root/.ssh/

# RUN chmod 600 /root/.ssh/* && \
#     mkdir -p /var/run/sshd

RUN mkdir ./dist && \
    chmod 755 dist

CMD echo "creating phar" && rm -f ./dist/*.phar && php createPhar.php