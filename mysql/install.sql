
SET autocommit=0;
SET unique_checks=0;
SET foreign_key_checks=0;
#DROP FOR DEBUG
#DROP TABLE IF EXISTS `Build`,`CodeModul`,`CodeLanguage`,`Library`,`CodeTemplate_sub`,`Project`,`ModulPosition`,`WorkPackage`,`Views`,`LibraryIncludeCode`,`t`;
DROP DATABASE `fcv`;
CREATE DATABASE `fcv`;
USE `fcv`;

#Classes
CREATE TABLE `Build` (
     `buildId` INT AUTO_INCREMENT
    , `name` VARCHAR(120)
    , `firstBuild` TINYINT(1)
    , `buildPath` VARCHAR(120)
    , `buildWorkpackages` TINYINT(1)
    , `disabled` TINYINT(1)
    , `project` INT
    , `lastModified` DATETIME
    , `created` DATETIME
    , PRIMARY KEY (`buildId`)
)
COLLATE='utf8_general_ci'
ENGINE=INNODB;
CREATE TABLE `CodeModul` (
     `modulId` INT AUTO_INCREMENT
    , `name` VARCHAR(120)
    , `path` VARCHAR(120)
    , `type` VARCHAR(120)
    , `applyTo` VARCHAR(120)
    , `buildOnlyOnFirstBuild` TINYINT(1)
    , `parent` INT
    , `lastModified` DATETIME
    , `created` DATETIME
    , PRIMARY KEY (`modulId`)
)
COLLATE='utf8_general_ci'
ENGINE=INNODB;
CREATE TABLE `CodeLanguage` (
     `langId` INT AUTO_INCREMENT
    , `name` VARCHAR(120)
    , `extension` VARCHAR(120)
    , `lastModified` DATETIME
    , `created` DATETIME
    , PRIMARY KEY (`langId`)
)
COLLATE='utf8_general_ci'
ENGINE=INNODB;
CREATE TABLE `Library` (
     `libId` INT AUTO_INCREMENT
    , `name` VARCHAR(120)
    , `version` VARCHAR(120)
    , `verificationLink` VARCHAR(120)
    , `path` VARCHAR(120)
    , `includeCode` VARCHAR(120)
    , `lastModified` DATETIME
    , `created` DATETIME
    , PRIMARY KEY (`libId`)
)
COLLATE='utf8_general_ci'
ENGINE=INNODB;
CREATE TABLE `CodeTemplate_sub` (
     `modulId` INT AUTO_INCREMENT
    , `position` VARCHAR(120)
    , `template` VARCHAR(120)
    , `language` VARCHAR(120)
    , `stereoType` VARCHAR(120)
    , `extend` VARCHAR(120)
    , `requiereUmlProperties` VARCHAR(120)
    , `modul` INT
    , PRIMARY KEY (`modulId`)
)
COLLATE='utf8_general_ci'
ENGINE=INNODB;
CREATE TABLE `Project` (
     `projektId` INT AUTO_INCREMENT
    , `name` VARCHAR(120)
    , `umlPath` VARCHAR(120)
    , `mysqlModel` VARCHAR(120)
    , `umlModel` VARCHAR(120)
    , `mysqlDb` VARCHAR(120)
    , `mysqlUser` VARCHAR(120)
    , `mysqlPassword` VARCHAR(120)
    , `mysqlHost` VARCHAR(120)
    , `mysqlPort` INT
    , `umlElement` INT
    , `buildPath` VARCHAR(120)
    , `lastModified` DATETIME
    , `created` DATETIME
    , PRIMARY KEY (`projektId`)
)
COLLATE='utf8_general_ci'
ENGINE=INNODB;
CREATE TABLE `ModulPosition` (
     `positionId` INT AUTO_INCREMENT
    , `name` VARCHAR(120)
    , `indent` INT
    , `modul` INT
    , `lastModified` DATETIME
    , `created` DATETIME
    , PRIMARY KEY (`positionId`)
)
COLLATE='utf8_general_ci'
ENGINE=INNODB;
CREATE TABLE `WorkPackage` (
     `umlId` VARCHAR(120)
    , `functionContent` VARCHAR(120)
    , `controlContent` VARCHAR(120)
    , `type` VARCHAR(120)
    , `functionIsDefault` TINYINT(1)
    , `controlIsDefault` TINYINT(1)
    , `name` VARCHAR(120)
    , `project` INT
    , `parent` VARCHAR(120)
    , `lastModified` DATETIME
    , `created` DATETIME
    , PRIMARY KEY (`umlId`)
)
COLLATE='utf8_general_ci'
ENGINE=INNODB;
CREATE TABLE `Views` (
     `viewId` INT AUTO_INCREMENT
    , `name` VARCHAR(120)
    , `htmlContent` VARCHAR(120)
    , `cssContent` VARCHAR(120)
    , `wp` VARCHAR(120)
    , `lastModified` DATETIME
    , `created` DATETIME
    , PRIMARY KEY (`viewId`)
)
COLLATE='utf8_general_ci'
ENGINE=INNODB;
CREATE TABLE `LibraryIncludeCode` (
     `IncludeCode` VARCHAR(120)
    , `escapeChar` VARCHAR(120)
    , `library` INT
    , `codeLanguage` INT
    , `lastModified` DATETIME
    , `created` DATETIME
    
)
COLLATE='utf8_general_ci'
ENGINE=INNODB;

#SetForeignKeys
ALTER TABLE `Build`
	ADD FOREIGN KEY (`project`) REFERENCES `Project`(`projektId`) ON UPDATE CASCADE
;
ALTER TABLE `CodeModul`
	ADD FOREIGN KEY (`parent`) REFERENCES `CodeModul`(`modulId`) ON UPDATE CASCADE
;
ALTER TABLE `CodeTemplate_sub`
	ADD FOREIGN KEY (`modulId`) REFERENCES `CodeModul`(`modulId`) ON UPDATE CASCADE
	, ADD FOREIGN KEY (`modul`) REFERENCES `CodeModul`(`modulId`) ON UPDATE CASCADE
;
ALTER TABLE `ModulPosition`
	ADD FOREIGN KEY (`modul`) REFERENCES `CodeModul`(`modulId`) ON UPDATE CASCADE
;
ALTER TABLE `WorkPackage`
	ADD FOREIGN KEY (`project`) REFERENCES `Project`(`projektId`) ON UPDATE CASCADE
	, ADD FOREIGN KEY (`parent`) REFERENCES `WorkPackage`(`umlId`) ON UPDATE CASCADE
;
ALTER TABLE `Views`
	ADD FOREIGN KEY (`wp`) REFERENCES `WorkPackage`(`umlId`) ON UPDATE CASCADE
;
ALTER TABLE `LibraryIncludeCode`
	ADD FOREIGN KEY (`library`) REFERENCES `Library`(`libId`) ON UPDATE CASCADE
	, ADD FOREIGN KEY (`codeLanguage`) REFERENCES `CodeLanguage`(`langId`) ON UPDATE CASCADE
;
COMMIT;
#setViews
CREATE VIEW `CodeTemplate`
    AS SELECT 
            `t2`.*
            
            ,t.`position`
            ,t.`template`
            ,t.`language`
            ,t.`stereoType`
            ,t.`extend`
            ,t.`requiereUmlProperties`
            ,t.`modul`
        FROM `CodeTemplate_sub` AS t
        INNER JOIN `CodeModul` AS t2 ON t.`modulId` = t2.`modulId`
#Todo: Disjunkt
    WITH CHECK OPTION;
COMMIT;

SET autocommit=1;
SET unique_checks=1;
SET foreign_key_checks=1;
