
#Classes











#SetForeignKeys
ALTER TABLE `Build`
	ADD FOREIGN KEY (`project`) REFERENCES `Project`(`projektId`) ON UPDATE CASCADE
;
ALTER TABLE `CodeModul`
	ADD FOREIGN KEY (`project`) REFERENCES `Project`(`projektId`) ON UPDATE CASCADE
	, ADD FOREIGN KEY (`parent`) REFERENCES `CodeModul`(`modulId`) ON UPDATE CASCADE
;
ALTER TABLE `CodeTemplate_sub`
	ADD FOREIGN KEY (`modulId`) REFERENCES `CodeModul`(`modulId`) ON UPDATE CASCADE
	, ADD FOREIGN KEY (`modul`) REFERENCES `CodeModul`(`modulId`) ON UPDATE CASCADE
;
ALTER TABLE `ModulPosition`
	ADD FOREIGN KEY (`modul`) REFERENCES `CodeModul`(`modulId`) ON UPDATE CASCADE
;
ALTER TABLE `WorkPackage`
	ADD FOREIGN KEY (`project`) REFERENCES `Project`(`projektId`) ON UPDATE CASCADE
	, ADD FOREIGN KEY (`parent`) REFERENCES `WorkPackage`(`umlId`) ON UPDATE CASCADE
;
ALTER TABLE `Views`
	ADD FOREIGN KEY (`wp`) REFERENCES `WorkPackage`(`umlId`) ON UPDATE CASCADE
;
ALTER TABLE `LibraryIncludeCode`
	ADD FOREIGN KEY (`library`) REFERENCES `Library`(`libId`) ON UPDATE CASCADE
	, ADD FOREIGN KEY (`codeLanguage`) REFERENCES `CodeLanguage`(`langId`) ON UPDATE CASCADE
;

#setViews
