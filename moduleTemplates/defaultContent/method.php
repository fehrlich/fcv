<?php
{{#getUmlElement}}
/* method {{getName}} from Class {{getParent.getName}} */
/* @var $this {{#substr 1}}{{getParent.getNamePath}}{{/substr 1}} */
// Parameters:
{{#getParameters}}      
/* @var ${{getName}} {{getType.getName}} */
{{/getParameters}}
{{/getUmlElement}}

//EndOfCodeGeneration