<?php
namespace fcv;

class Enviroment {

    /**
     *
     * @var Enviroment[]
     */
    static public $enviroments;
    public $load = null;
    public $detectWhen = null;
    
    public $currentEnviroment;

    public function __construct($cb) {
        $this->load = $cb;
    }

    public function detectWhen($hostOrCB) {
        self::$enviroments[] = $this;
        $this->setDetectWhen($hostOrCB);
    }

    public static function detect() {
        foreach (self::$enviroments as $env) {
            $detect = $env->getDetectWhen();
            if (is_callable($detect)) {
                if ($detect()) {
                    $env->load();
                }
            } elseif (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == $detect) {
                $env->load();
            }
        }
    }

    function getDetectWhen() {
        return $this->detectWhen;
    }

    function setDetectWhen($detectWhen) {
        $this->detectWhen = $detectWhen;
        return $this;
    }
    
    static function create($cb){
        return new Enviroment($cb);
    }
    
    function load(){
        $load = $this->load;
        $load();
        return $this;
    }
    
    function getCurrentEnviroment() {
        return $this->currentEnviroment;
    }
}
