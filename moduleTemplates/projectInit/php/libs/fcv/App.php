<?php

namespace fcv;

use \fcv\traits\Singleton;
use \fcv\traits\EventAction;
use \fcv\Enviroment; 

class App {

    use Singleton;
    use EventAction;

    /**
     *
     * @var \fcv\Viewable 
     */
    private $currentSite = null;
    
    private $ajax = false;

    function load() {
        $this
            ->loadConfig()
            ->loadEnviroments()
            ->loadRouting()
            ->loadTemplateHelper();
        
        Enviroment::detect();
        
        $this->loadDebug();
        $this->loadSecurity();
        $this->trigger("configLoaded");
        
        $this->startRouting();        
        $this->trigger("routingLoaded");
        
        return $this;
    }

    function loadConfig() {
        include('config/settings.php');
        return $this;
    }

    function loadEnviroments() {
        include('config/enviroments.php');
        return $this;
    }

    function loadRouting() {
        include('config/routes.php');
        return $this;
    }

    function loadDebug() {
        include('config/debug.php');
        return $this;
    }
    
    function loadSecurity() {
        include('config/security.php');
        return $this;
    }
    
    function loadTemplateHelper() {
        include('config/viewHelper.php');
        return $this;
    }
    
    function startRouting(){
        
    }

    function getCurrentSite() {
        return $this->currentSite;
    }
    
    function setCurrentSite($currentSite) {
        $this->currentSite = $currentSite;
        $this->trigger("initSite");
        return $this;
    }

    /**
     * 
     * @return App
     */
    static function get() {
        return self::getObj();
    }
    
    function getAjax() {
        return $this->ajax;
    }

    function setAjax($ajax) {
        $this->ajax = $ajax;
        return $this;
    }
    
    function onAjaxRequest($cb){
        $this->on('ajaxRequest', $cb);
    }
    
    function triggerAjaxRequest($class, $action, $param){
        $this->trigger('ajaxRequest', $class, $action, $param);
    }
}
