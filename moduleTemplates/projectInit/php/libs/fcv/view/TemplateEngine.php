<?php
namespace fcv\view;

use fcv\traits\Singleton;
 
class TemplateEngine extends \Mustache_Engine  {
    use Singleton;
    
    private $templateEngine = null;
    
    /**
     * 
     * @return TemplateEngine
     */
    
    static function get(){
        return self::getObj();
    }
    
    public function __construct(array $options = array(), $templateEngine = false) {
        if(!$templateEngine){
            parent::__construct($options);
            $templateEngine = $this;
        }
        $this->templateEngine = $templateEngine;
//        $options['escape'] = function($value){
////            return htmlspecialchars($value);
//            if(!is_string($value)){
//                echo "Template Error";
//                d($value);
//                d(1);
//                exit();
//            }
//            return htmlspecialchars($value, ENT_COMPAT, 'UTF-8');
//        };
//        parent::__construct($options);
    }

    public function render($template, $data = array()) {
        $ret = parent::render($template, $data);
        return $ret;
    }
    
    function getTemplateEngine() {
        return $this->templateEngine;
    }

    static function setTemplateEngine($templateEngine) {
        self::$handle = $templateEngine;
//        $this->templateEngine = $templateEngine;
    }
    
//    function addHelper($name, $helper) {
//        $this->templateEngine->addHelper($name, $helper);
//    }
}
