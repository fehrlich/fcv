<?php
namespace fcv\view;

use DebugBar\DataCollector\AssetProvider;
use DebugBar\DataCollector\DataCollector;
use DebugBar\DataCollector\Renderable;

/**
 * Description of MustacheDataCollector
 *
 * @author fehrlich
 */
class MustacheDataCollector extends DataCollector implements Renderable, AssetProvider{
//    protected $timeCollector;
//    /**
//     *
//     * @var TemplateEngineTraceable 
//     */
//    protected $templateEngine = '';
//
//
//    protected $lines = array(
//        'test' => 'hallo'
//    );
//    
//    public function __construct($templateEngine = false, TimeDataCollector $timeCollector = null) {
//        $this->templateEngine = $templateEngine;
//        $this->timeCollector = $timeCollector;
//    }
//    
//    public function getWidgets()
//    {
//        return array(
//            array(
//                'mustache' => array(
//                    'icon' => 'leaf',
//                    'widget' => 'PhpDebugBar.Widgets.TemplatesWidget',
//                    'map' => 'twig',
//                    'default' => '[]'
//                ),
//                'mustache:badge' => array(
//                    'map' => 'mustache.nb_templates',
//                    'default' => 0
//                )
//            );
//    }
////    public function getWidgets()
////    {
////        return array(
////            
////            "template" => array(
////                'icon' => 'list-alt',
////                "widget" => "PhpDebugBar.Widgets.MessagesWidget",
////                "map" => "template.lines",
////                "default" => "[]"
////            )
////        );
////    }
//
//    public function collect() {
////        $data = array(
////            'nb_templates' => count($this->templateEngine->getTplExecutions()),
////            'durations' => $this->templateEngine->getDuration(),
////            'templates' => $this->templateEngine->getTplExecutions()
////        );
//        $data = array(
//            'nb_templates' => 5,
//            'durations' => 20,
//            'templates' => array(
//                array(
//                    'template' => "5",
//                    'duration' => 20,
//                )
//            )
//        );
//
////        foreach ($this->templateEngine->getTplExecutions() as $name => $pdo) {
////            $pdodata = $this->collectPDO($pdo, $this->timeCollector);
////            $data['nb_statements'] += $pdodata['nb_statements'];
////            $data['nb_failed_statements'] += $pdodata['nb_failed_statements'];
////            $data['accumulated_duration'] += $pdodata['accumulated_duration'];
////            $data['memory_usage'] += $pdodata['memory_usage'];
////            $data['peak_memory_usage'] = max($data['peak_memory_usage'], $pdodata['peak_memory_usage']);
////            $data['statements'] = array_merge($data['statements'],
////                array_map(function($s) use ($name) { $s['connection'] = $name; return $s; }, $pdodata['statements']));
////        }
//
////        $data['accumulated_duration_str'] = $this->getDataFormatter()->formatDuration($data['accumulated_duration']);
////        $data['memory_usage_str'] = $this->getDataFormatter()->formatBytes($data['memory_usage']);
////        $data['peak_memory_usage_str'] = $this->getDataFormatter()->formatBytes($data['peak_memory_usage']);
//
//        return $data;
//        
//    }
//
//    public function getName() {
//        return "HASFD";
//    }

    private $mustache;
    
    public function __construct(TemplateEngineTraceable $te)
    {
        $this->mustache = $te;
    }

    public function collect()
    {
        $templates = array();
        $accuRenderTime = 0;
        foreach ($this->mustache->getRenderedTemplates() as $tpl) {
            $accuRenderTime += $tpl['render_time'];
            $templates[] = array(
                'name' => $tpl['name'],
                'render_time' => $tpl['render_time'],
                'render_time_str' => $this->formatDuration($tpl['render_time'])
            );
        }
        
//        d($templates);

        return array(
            'nb_templates' => count($templates),
            'templates' => $templates,
            'accumulated_render_time' => $accuRenderTime,
            'accumulated_render_time_str' => $this->formatDuration($accuRenderTime)
        );
    }

    public function getName()
    {
        return 'mustache';
    }

    public function getWidgets()
    {
        return array(
            'mustache' => array(
                'icon' => 'leaf',
                'widget' => 'PhpDebugBar.Widgets.TemplatesWidget',
                'map' => 'mustache',
                'default' => '[]'
            ),
            'mustache:badge' => array(
                'map' => 'mustache.nb_templates',
                'default' => 0
            )
        );
    }

    public function getAssets()
    {
        return array(
            'css' => 'widgets/templates/widget.css',
            'js' => 'widgets/templates/widget.js'
        );
    }
}
