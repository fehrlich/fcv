<?php

namespace fcv\view;

use fcv\traits\Singleton;

class TemplateEngineTraceable extends \Mustache_Engine {

    use Singleton;

    protected $renderedTemplates = array();
    protected $timeDataCollector;

    public function __construct($options = array(),$timeDataCollector = null) {
        $this->timeDataCollector = $timeDataCollector;
        parent::__construct($options);
    }

    public function getRenderedTemplates() {
        return $this->renderedTemplates;
    }

    public function addRenderedTemplate(array $info) {
        $this->renderedTemplates[] = $info;
    }

    public function getTimeDataCollector() {
        return $this->timeDataCollector;
    }

    public function render($template, $data = array()) {
        $before = microtime(true);
        $ret = parent::render($template, $data);
        $timeDiff = microtime(true) - $before;
//        $this->renderedTemplates[] = array(
//            'name' => 'undefined',
//            'render_time' => $timeDiff
//        );
        return $ret;
    }

}
