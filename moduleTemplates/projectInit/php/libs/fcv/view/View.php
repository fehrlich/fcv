<?php
namespace fcv\view;

use fcv\Config;
use fcv\Routing;
use fcv\traits\FactoryMethod;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RequestContext;

class View{
    use FactoryMethod;
    private $page = false;
    private $frames = array();
    private $pageInstance = null;

    public function setPage($page) {
        $this->page = $page;
        return $this;
    }
    
    function getPage() {
        return $this->page;
    }
    
    function getFrames() {
        return $this->frames;
    }
    
    /**
     * 
     * @param type $frameName
     * @param type $frame
     * @return View
     */
    public function addFrame($frameName, $frame){
        $this->frames[$frameName] = $frame;
        return $this;
    }

    public function getUrl(){
        $context = new RequestContext();
        $generator = new UrlGenerator(Routing::getObj(), $context);
        $name = $this->page;
        $url = $generator->generate($name, $this->frames);
        return Config::get()->getDefaultPath().$url;
    }
    
    public function getPageInstance(){
        if($this->pageInstance) return $this->pageInstance;
        if(empty($this->getPage())){
            return '';
        }
        
        $pageName = '\\{{getName}}\\FCVNavigation\\Page\\'.$this->getPage();
        $page = new $pageName();
        
        
        foreach($this->frames as $varname => $value){
            $frameName = '\\{{getName}}\\FCVNavigation\\Frame\\'.$value;

            $options = array();
            if(strpos($frameName, '?') !== false){
                $spl = explode('?', $frameName);
                $frameName = $spl[0];
                parse_str($spl[1], $options);
            }

            Frame::$loaded[$value] = true;
            if(class_exists($frameName)){
                $frame =  $frameName::create();        
                foreach($options as $name => $val){
                    //Todo Security
                    $frame->$name = $val;
                }
                $frame->loadContent();
                $page->$varname = $frame->getHtml();            
            }else{
                $page->$varname = $value;
            }
        }
        
        $this->pageInstance = $page;
        
        return $page;
    }
}
