<?php
namespace fcv\traits;


trait Singleton {
    
    private static $handle = false;
    
    private function __construct(){
        
    }
    
    /**
     * 
     * @return static
     */
    static function getObj(){
        $class = get_called_class();
        self::$handle = (self::$handle !== false)?self::$handle:new $class();
        return  self::$handle;
    }
}
