<?php
namespace fcv\traits;

use fcv\Config;

trait EventAction {
    
    private $on = [];
    static private $staticOn = [];
    
    
    public function on($name, $cb){
        if(!Config::get()->import){
            if(!isset($this->on[$name])) $this->on[$name] = array();
            $this->on[$name][] = $cb;
        }
    }
    
    public function trigger($name, ...$parameter){
        
        if(Config::get()->import){
            return;   
        }
        if (!isset($this->on[$name])) {
            return;
        }
        foreach($this->on[$name] as $cb){
            call_user_func_array($cb, $parameter);            
        }
//        static::triggerS($name,$parameter);
    }
    
//    static function staticOn($name, $cb){
//        if(!Config::get()->import){
//            if(!is_array($name)) $name = array($name);
//            foreach($name as $n){
//                if(!isset(static::$staticOn[$n])) static::$staticOn[$n] = array();
//                static::$staticOn[$n][] = $cb;                
//            }
//        }
//    }
    
    
//    static function triggerS($name, $parameter = null){
//        $ret = true;
//        if (!isset(static::$staticOn[$name])) {
//            return $ret;
//        }
//        foreach(static::$staticOn[$name] as $cb){
//            $ret = $ret && call_user_func($cb, $parameter);            
//        }
//        return $ret;
//    }
}
