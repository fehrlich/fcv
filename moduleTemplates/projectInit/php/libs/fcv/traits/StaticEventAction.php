<?php
namespace fcv\traits;

use fcv\Config;

trait StaticEventAction {
    
    static private $staticOn = [];
    
    static function on($name, $cb){
        if(!Config::get()->import){
            if(!is_array($name)) $name = array($name);
            foreach($name as $n){
                if(!isset(static::$staticOn[$n])) static::$staticOn[$n] = array();
                static::$staticOn[$n][] = $cb;                      
            }
        }
    }
    
    static function trigger($name, ...$parameter){
        $ret = true;
        if($name != 'event'){
            static::trigger('event', $name, $parameter);
        }
        if (!isset(static::$staticOn[$name])) {
            return $ret;
        }
        foreach(static::$staticOn[$name] as $cb){
            $ret = call_user_func_array($cb, $parameter) && $ret;            
        }
        return $ret;
    }
}
