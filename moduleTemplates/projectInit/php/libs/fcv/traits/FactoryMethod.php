<?php
namespace fcv\traits;


trait FactoryMethod {
    
    /**
     * 
     * @return static
     */
    static function create(){
        $class = get_called_class();
        return new $class();
    }
}
