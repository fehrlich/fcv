<?php

namespace fcv\traits;

/**
 * Description of Record
 *
 * @author xzen
 */
trait Recordable {

    private $record = false;
    private $records = array(
        'methods' => array()
    );

    public function __call($name, $arguments) {
        d($name);
        d("method in class '".get_class($this)."' not found:".$name);
        exit();
        if ($this->record) {
            $this->records['methods'][] = array($name, $arguments);
        }
    }

    public function startRecord(){
        $this->record = true;
    }
    public function stopRecord(){
        $this->record = false;
    }
    
    function getRecords() {
        return $this->records;
    }
    function setRecords($records) {
        $this->records = $records;
        return $this;
    }
    
    function play($from = 0, $to = false){
        if(!isset($this->records['methods'])) return;
        $to = $to ? $to : count($this->records['methods']);
//        d($to);
        for($i=$from; $i < $to; $i++){
            $method = $this->records['methods'][$i];
            $this->callMethod($method);
        }
    }
    
    function playRecords($records){
        if(!isset($records['methods'])) return;
        foreach($records['methods'] as $method){
//            d($method);
            $this->callMethod($method);
        }
    }
    
    function filterMethods($cb){
        if(!isset($this->records['methods'])) return array();
        $ret = array();
        foreach($this->records['methods'] as $method){
            $val = $cb($method);
            if($val){
                $ret[] = $method;
            }
        }
        return $ret;
    }
    
    private function callMethod($method){
        call_user_method_array ($method[0], $this , $method[1]);
        
    }
    
}

class RecorderStub{
    use Recordable;
}
