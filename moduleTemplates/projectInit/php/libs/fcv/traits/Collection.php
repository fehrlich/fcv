<?php
namespace fcv\traits;

use fcv\libs\Debug;
use fcv\exceptions\CollectionIsEmpty;
use Object;

trait Collection{
    
    private $index = 0;
    
    protected $objArray = array();


    public function __construct($data = array()){
        $this->objArray = $data;
    }

    /**
     * @param $data
     * @return static
     */
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new static($data);
    }
    
    /**
     * @param Object $el
     * @return static
     */
    public function add($el){
        if($el instanceof static){
            foreach($el as $e){
                $this->objArray[] = $e;
            }
        }else{
            $this->objArray[] = $el;            
        }
        return $this;
    }
    
    function removeIndex($index){
        
        array_splice ($this->objArray, $index, 1);
            
        return $this;
    }
    
    function removeIndexes($indexes){
        
        foreach($indexes as $index){
            $this->removeIndex($index);
        }          
        
        return $this;
    }
    
    public function first() {
        if($this->isEmpty()){
            if(Debug::getObj()->getThrowErrors()){
                throw new CollectionIsEmpty('Collection is empty, cant get first element');
            }
            return null;
        }
        return $this->objArray[0];
    }
    
    public function hasFirst() {
        
        return !$this->isEmpty();
    }
    public function getFirst() {
        
        return $this->first();
    }
    
    
    public function last() {
        if($this->isEmpty()){
            if(Debug::getObj()->getThrowErrors()){
                throw new CollectionIsEmpty('Collection is empty, cant get current element');
            }
            return null;
        }
        $this->index = count($this->objArray)-1;
        return $this->current();
    }
    
    public function current() {
        if($this->isEmpty()){
            throw new CollectionIsEmpty('Collection is empty, cant get current element');
        }
        return $this->objArray[$this->index];
    }

    public function key() {
        return $this->index;
    }

    public function next() {
        $this->index++;
        return $this;
    }

    public function rewind() {
        $this->index = 0;
        return $this;
    }

    public function valid() {
        return isset($this->objArray[$this->index]);
    }
    public function append($el) {
        return $this->add($el);
    }
    public function count() {
        return count($this->objArray);
    }

    public function offsetExists($offset) {
        return isSet($this->objArray[$offset]);
    }

    public function offsetGet($offset) {
        return $this->objArray[$offset];
    }

    public function offsetSet($offset, $value) {
        $this->objArray[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->objArray[$offset]);
    }

    public function seek($position) {
        $this->index = $position;
    }
    
    public function serialize() {
        return serialize($this->objArray);
    }
    
    public function pop() {
        return array_pop($this->objArray);
    }
    
    public function isEmpty() {
        return empty($this->objArray);
    }
    
    public function chunk($size){
        return array_chunk($this->objArray, $size);        
    }

    public function unserialize($serialized) {
        return unserialize($serialized);
    }
    
    public function getArray(){
        return $this->objArray;
    }
    
    public function reverse(){
        $this->objArray = array_reverse($this->objArray);
        return $this;
    }
    
    public function each($cb){
        foreach($this->objArray as $i => $o){
            $cb($o,$i);
        }
        return $this;
    }
    
    /*
     * Iteration Helpers
     */    
    
    function isFirst(){
        return $this->index == 0;
    }
    function isLast(){
        return $this->index == count($this->objArray) - 1;
    }
    
    function isEven(){
        return $this->index%2 == 0;
    }
    
    function isOdd(){
        return $this->index%2 == 1;
    }
    function getIndex(){
        return $this->index;
    }
    
    function onNextChange($id){
        if($this->isLast()) return true;
        return ($this->objArray[$this->index+1]->get($id) != $this->objArray[$this->index]->get($id));
    }
    
    function mod($x, $shift = 0){
        return $this->index % $x == $shift;
    }
    
    function max($field){
        $max = false;
        foreach($this->objArray as $o){
            if($max === false || $o->get($field) > $max){
                $max = $o->get($field);
            }
        }
        return $max;
    }
    
    function min($field){
        $min = false;
        foreach($this->objArray as $o){
            if($min === false || $o->get($field) < $min){
                $min = $o->get($field);
            }
        }
        return $min;
    }
    
    function onChange($id){
        if($this->isFirst()) return true;
        return ($this->objArray[$this->index-1]->get($id) != $this->objArray[$this->index]->get($id));
    }
    
    function getList(){
        return $this->map(function($el, $i){
            return [
                'index' => $i,
                'value' => $el,
                'even' => ($i % 2 == 0),
                'first' => ($i == 0),
                'last' => ($i == $this->getIndex()),
                'mod' => function ($content,$tpl,$args) use ($i){
                    $mod = $args[0];
                    $shift = isset($args[1]) ? $args[1] : 0;
                    return $i % $mod == $shift  ? $content : '';
                },
                'nmod' => function ($content,$tpl,$args) use ($i){
                    $mod = $args[0];
                    $shift = isset($args[1]) ? $args[1] : 0;
                    return $i % $mod != $shift  ? $content : '';
                }
            ];
        });
    }
    
    //Map Reduce Pattern
    public function map($mapFunction){
        $ret = array();
        foreach($this->objArray as $i => $o){
            $ret[$i] = $mapFunction($o, $i);
        }
        return $ret;
    }
    
    public function reduce($reduceFunction){

        $length = count($this->objArray);
        if($length <= 1) return $this->objArray;

        $array = $this->objArray;
        $out = array_shift($array);
        do {
            $next = array_shift($array);
            $out = $reduceFunction($out, $next);
        } while(!empty($array));

        return $out;
    }
    
    
    public function filter($filterFunction){
        
        $ret = array();        
        foreach($this->objArray as $obj){
            if($filterFunction($obj)){
                $ret[] = $obj;
            }
        }

        return new static($ret);
    }
    
    
    private function compareByFieldName($fieldName, $a, $b, $desc = false){
            //TODO: php bug https://bugs.php.net/bug.php?id=50688
//            $aValue = $a->has($by) ? $a->get($by) : null;
//            $bValue = $b->has($by) ? $b->get($by) : null;
        
            $aValue = $a->get($fieldName);
            $bValue = $b->get($fieldName);
            
            if($aValue == $bValue){
                return 0;
            }
            
            if($desc){
                return ($aValue < $bValue) ? 1 : -1;
            }
            
            return ($aValue < $bValue) ? -1 : 1;        
    }
    
    public function sortBy($by, $desc = false){
        if(!is_array($by)) $by = [$by];
        usort($this->objArray, function($a, $b) use($by,$desc) {     
            $index = 0;
            $length = count($by);
            
            $check = false;
            
            do {
                $ret = $this->compareByFieldName($by[$index], $a, $b, $desc);
                if($ret === 0) $check = true;
                $index++;
            } while ($check && $index < $length);
            
            return $ret;
        });
        
        return $this;
    }
    
    public function groupBy($field){
        
        $groupedArray = array();        
        
        foreach($this->objArray as $obj){
            $fieldValue = !is_string($field) ? $field($obj) : $obj->get($field);
            if(!isset($groupedArray[$fieldValue])){
                $groupedArray[$fieldValue] = static::create();
            }
            $groupedArray[$fieldValue]->add($obj);
        }
        
        return $groupedArray;
    }
    
    public function getRange($startIndex, $endIndex = -1){
        $col = static::create();
        $maxIndex = $this->count()-1;
        
        if($startIndex > $maxIndex){
            return $col;
        }
        
        if($endIndex == -1) $endIndex = $startIndex;
        
        $endIndex = min($endIndex, $maxIndex);
                
        for($i = $startIndex; $i <= $endIndex; $i++){
            $col->add($this->objArray[$i]);            
        }
        
        return $col;
    }
    
//    public function sortBy($by, $desc = false){
//        
//        $sortedArray = $this->objArray;
//        
//        usort($sortedArray, function($a, $b) use($by,$desc) {
//            
//            //TODO: php bug https://bugs.php.net/bug.php?id=50688
////            $aValue = $a->has($by) ? $a->get($by) : null;
////            $bValue = $b->has($by) ? $b->get($by) : null;
//            
//            $aValue = $a->get($by);
//            $bValue = $b->get($by);
//            
//            if($aValue == $bValue){
//                return 0;
//            }
//            
//            if($desc){
//                return ($aValue < $bValue) ? 1 : -1;
//            }
//            
//            return ($aValue < $bValue) ? -1 : 1;
//        });
//        
//        ddd($sortedArray);
//        
//        return static::create($sortedArray);
//    }
    
}