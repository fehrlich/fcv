<?php

namespace fcv;

class RememberObject{
    public $obj;
    public $tailorArray = [];
    public $selectors = [];
    
    public $data = [];
    private $generatedId = false;
    
    function __construct(&$obj, $selectors, $tailorArray) {
        $this->obj = &$obj;
        $this->tailorArray = $tailorArray ? $tailorArray : [];
        $this->selectors = $selectors ? $selectors : [];
    }
    
    private function getGeneratedId(){
        if(!$this->generatedId){
            $this->generatedId = rand(0, 20000000);
        }
        return $this->generatedId;
    }
    
    function getId(){
        if($this->obj->getId()) return $this->obj->getId();
        if(isset($this->selectors[0])) return $this->selectors[0];
        
//        d($this->obj);
//        exit();
//        throw new \Exception('JsObject has no ID');
        
        return false;
//        return $this->getGeneratedId();
    }
    
    function getJSId(){
        return get_class($this->obj).'.'.$this->getId();
    }
    
    function loadData($hash){
        $this->data = $this->obj->toJSObject(true, $hash, true, $this->tailorArray, $this->getId());
        return $this->data;
    }
    
    function loadObject($hash){      
        $this->obj->loadJsObject($hash, $this->tailorArray);
//        d("load");
//        d($this->tailorArray);
    }
    
    /**
     * 
     * @param RememberObject $rObj
     */
    function merge($rObj){
        $this->tailorArray = array_merge_rekursive_simple($this->tailorArray, $rObj->tailorArray);
        $this->selectors = array_merge($this->selectors, $rObj->selectors);
        $this->selectors = array_unique($this->selectors);
    }
    
    /**
     * 
     * @param RememberObject $rObj
     */
    function getSelectors($rObj){
    }
}

/**
 * Steps:
 *  1. Load all needed Classes and Objects with their tailored Attributes
 *  2. Merge all needed Objects
 *  3. load Data for the Objects
 */
class FCVJsObj {

    static $jsObj = array();
    static $jsObjReq = array();
    static $jsObjSel = array();
    static $jsObjTailor = array();
    static $jsClasses = array();
    static private $loadedElements;
    
    static $jsObjIdList = array();
    
    //refactor:
    static $jsRememberedObjects = [];
    static $jsRememberedObjectsById = [];
    static $loadingElements = false;
    
    static $remKeyMap = [];
    
    static function add(&$val, $selectionString = '', $tailorArray = [], $overwrite = true) {
        $sel = $selectionString != '' ? [$selectionString] : false;
        
        if($overwrite || !isset(static::$remKeyMap[$selectionString])){
            static::$jsRememberedObjects[] = new RememberObject($val, $sel, $tailorArray);
        }
        
        if($selectionString){
            static::$remKeyMap[$selectionString] = new RememberObject($val, $sel, $tailorArray);
        }
    }
    
    
    static function addRememberedObjectWithId($rObj){
        $id = $rObj->getJSId();
        
        if(isset(static::$jsRememberedObjectsById[$id])){
            static::$jsRememberedObjectsById[$id]->merge($rObj);
            
        }else{
            static::$jsRememberedObjectsById[$id] = $rObj;            
        }
        
    }
    
    static private function preLoad(){
        foreach(static::$jsRememberedObjects as $rObj){
            static::addRememberedObjectWithId($rObj);
        }        
    }
    
    static function loadCurrentElements($hash){
        foreach(static::$jsRememberedObjects as $rObj){
            $rObj->loadObject($hash);
        }
    }
        
    
    static private function loadElements(){
        static::$loadingElements = true;   
        static::$loadedElements = [];
        $hash = rand(0, 100000);
        $hash2 = $hash+1;
                
        static::loadCurrentElements($hash);
        static::preLoad();
                
        foreach(static::$jsRememberedObjectsById as $rObj){
            $data = $rObj->loadData($hash2);
            $data['selector'] = $rObj->selectors;
            static::$loadedElements[$rObj->getJSId()] = $data;
        }
        
//        d(static::$loadedElements);

        return static::$loadedElements;
    }
    
    static function addRequire($req) {
        FCVJsObj::$jsObjReq[substr($req, 1)] = FCVJsObj::toJSNameSpace($req);
    }
    
    static function getRequires(){
        return FCVJsObj::$jsObjReq;
    }

    static function addClass($class){
        FCVJsObj::$jsClasses[$class] = true;
    }
    
//    static function add(&$val, $selectionString = '', $tailorArray = false) {
//        $class = get_class($val);
//        
//        $id = $val->getId();
//        
//        if(!$id && $selectionString != ''){
//            $id = $selectionString;
//        }
//                
//        if(isset(FCVJsObj::$jsObjIdList[$class.'.'.$id]) &&
//            ($selectionString == '' || in_array($selectionString, FCVJsObj::$jsObjSel))){
//            if($tailorArray){
//                $before = FCVJsObj::$jsObjTailor[$class.'.'.$id];
//                if($before){
//                    FCVJsObj::$jsObjTailor[$class.'.'.$id] = array_merge($before, $tailorArray);
//                }
//            }
//            return;
//        }
//        
//        $val->on('idUpdated', function($obj,$oldId) use($class, $id){
//            FCVJsObj::$jsObjIdList[$class.'.'.$obj->getId()] = true;
//            FCVJsObj::$jsObjTailor[$class.'.'.$obj->getId()] = FCVJsObj::$jsObjTailor[$class.'.'.$oldId];
//            
//            unset(FCVJsObj::$jsObjIdList[$class.'.'.$oldId]);            
//            unset(FCVJsObj::$jsObjTailor[$class.'.'.$oldId]);       
//        });
//        
//        FCVJsObj::$jsObjIdList[$class.'.'.$id] = true;
//        FCVJsObj::$jsObjTailor[$class.'.'.$id] = $tailorArray;
//        FCVJsObj::$jsObj[] = $val;
//        FCVJsObj::$jsObjSel[] = $selectionString;
//        
//        if(!in_array($class, FCVJsObj::$jsObjReq)){
//            FCVJsObj::$jsObjReq[] = $class;
//        }
//    }
    
    static function toJSNameSpace($objectName){
        $spl = explode('/', $objectName);
        $objectName = $spl[count($spl)-1];
        $spl = explode('\\', $objectName);
        $objectName = $spl[count($spl)-1];
        return $spl[count($spl)-1];
    }
    
//    static private function loadElements(){
//        $loadedElements = array();
//        $hash = rand(0, 100000);
//        for ($i = 0; $i < count(FCVJsObj::$jsObj); $i++) {
//            $el = FCVJsObj::$jsObj[$i];
//            $id = $el->getId();
//            if(!$id && isset(FCVJsObj::$jsObjSel[$i])){
//                $id = FCVJsObj::$jsObjSel[$i];
//            }
//            if ((!empty($id) && !is_null($id))) {
//                $tailorArray = FCVJsObj::$jsObjTailor[get_class($el) . '.' . $id];
//                                
//                $jsEl = $el->toJSObject(true, $hash, true, $tailorArray, $id);
////                ddd($jsEl);
////                $x = FCVJsObj::$jsObj;
////                echo count($x);
//                
//                $jsEl['selector'] = FCVJsObj::$jsObjSel[$i];
////                echo $jsEl['id'];
////                d($jsEl);
////                d(get_class($el) . '.' . $id);
//                if(isset($jsEl['data'])) $loadedElements[get_class($el) . '.' . $id] = $jsEl; 
//            }
//        }
//        static::$loadedElements = $loadedElements;
//        d(static::$loadedElements );
//    }
//    
    static function getJsModel(){
        static::loadElements();
        return static::$loadedElements;
    }
    
    static function get() { 
//        $loadedElements = array();
//        $hash = rand(0, 100000);
//        foreach (FCVJsObj::$jsObj as $i => $el) {
//        for ($i = 0; $i < count(FCVJsObj::$jsObj); $i++) {
//            $el = FCVJsObj::$jsObj[$i];
//            $id = $el->getId();
//            if (!empty($id) && !is_null($id)) {
//                $jsEl = $el->toJSObject(true, $hash);
//                
//                $jsEl['selector'] = FCVJsObj::$jsObjSel[$i];
//                if(isset($jsEl['data'])) $loadedElements[get_class($el) . '.' . $id] = $jsEl; 
//            }
//        }
        
        $loadedElements = static::getJsModel();
        
        $jsInit = "";
//        $jsInit .= "var jsModel = {};\n";
//        $jsInit .= "requirejs(['" . implode("','", array_keys(FCVJsObj::$jsObjReq)) . "'], function(".implode(',', FCVJsObj::$jsObjReq)."){\n\t";

        foreach ($loadedElements as $name => $jsObj) {
            $name = FCVJsObj::toJSNameSpace($name);
            $jsInit .= "\tjsModel['" . $name . "'] = new " . $jsObj['class'] . " (" . json_encode($jsObj['data']) . ");\n";
            foreach($jsObj['selector'] as $selector){
                $jsInit .= "\tjsModel['" . $selector . "'] = jsModel['" . $name . "'];";                
            }
//            $jsInit .= (!empty($jsObj['selector'])) ? "\tjsModel['" . $jsObj['selector'] . "'] = jsModel['" . $name . "'];" : "";
//            $jsInit .= (!empty($jsObj['selector'])) ? "\tjsModel['" . $jsObj['selector'] . "'] = jsModel['" . $name . "'];" : "";
            
        }

        $jsInit .= "for(var id in jsModel){
            if(jsModel[id].class && jsModel[id].id) jsModel[id] = jsModel[jsModel[id].class+'.'+jsModel[id].id];
        }\n";
//        $jsInit .= "});\n";
        
//        d($jsInit);

        return $jsInit;
    }
    
    function ajaxRequestWrapper(){
        
    }
}
