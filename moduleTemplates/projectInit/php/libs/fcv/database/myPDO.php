<?php

namespace fcv\database;

use PDO;
use fcv\Config;

class myPDO extends PDO {

    public function __construct($host, $user, $password, $database, $port = 3306, $socket = NULL, $reconnect = false) {
        parent::__construct(Config::get()->getDbDriver() . ':host=' . $host . ';dbname=' . $database . ';port=' . $port . ';charset=utf8', $user, $password);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $this->connected = true;
    }

}
