<?php
namespace fcv\database;

use DebugBar\DataCollector\PDO\TraceablePDO;
use PDO;


class myPDOTraceable extends TraceablePDO{
    
    public function __construct($host, $user, $password, $database, $port = 3306, $socket = NULL) {
        parent::__construct(new myPDO($host,$user,$password,$database,$port,$socket));
         $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $this->connected = true;
    }
}

