<?php
namespace fcv\database;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\MySqlGrammar;
use Illuminate\Database\Query\Processors\MySqlProcessor;
use Illuminate\Database\Connection;


class QueryBuilder extends Builder{
    static $handle = false;
    
    static $handleCount = 0;
        
    public function __construct() {
        $connection = new Connection(DB::getObj());
        parent::__construct($connection, new MySqlGrammar(), new MySqlProcessor());
        $this->setFetchModeAssoc();
    }

    /**
     * @return QueryBuilder
     */
    static function getObj(){
        QueryBuilder::$handle = (QueryBuilder::$handle !== false) ? QueryBuilder::$handle : new QueryBuilder();
        return  QueryBuilder::$handle;
    }
    
    function setFetchModeAssoc(){
        parent::getConnection()->setFetchMode(\PDO::FETCH_ASSOC); //setFetchMode is no longer available in laravel > 5.0
        return $this;
    }
    function setFetchModeNum(){
        parent::getConnection()->setFetchMode(\PDO::FETCH_NUM);
        return $this;
    }
    
    static function create(){
        return new QueryBuilder();
    }    
    
    public function mergeQuery($query, $logic = "and"){
        $logic = strtolower($logic);
        
        if(isSet($query->wheres)){
            $this->addNestedWhereQuery($query, $logic);
        }
        if(isSet($query->joins)){
            foreach($query->joins as $join){
                $this->joins[] = $join;
            }
        }
        if(isSet($query->columns)){
            foreach($query->columns as $cols){
                $this->columns[] = $cols;
            }
        }
    }
    
    public function getArray($columns = array()) {
        $arrayOrCollection = parent::get($columns);
        if($arrayOrCollection instanceof \Illuminate\Support\Collection){
            return $arrayOrCollection->toArray();
        }
        return $arrayOrCollection;
    }
        
    /**
     * add additional Operators to work with where methods in preparing collections
     * @param QueryBuilder $query
     * @param string $column
     * @param string $operator
     * @param string $value
     * @param string $boolean
     * @return boolean
     * @throws type
     */
    public static function customWhereOperator($query, $column, $operator, $value, $boolean){
        //var [TABLEALIAS] is overwritten with final table name alias in raw statements
        $useOr = strtolower($boolean) == 'or';
        $operator = strtoupper($operator);
        $escapedCol = '`'.$column.'`';
        switch ($operator){
            case 'IS':
                $value = strtoupper($value);
                if($value == 'NULL'){
                    $query->whereNull($column, $boolean);
                }
                elseif($value == 'NOT NULL'){
                    $query->whereNotNull($column, $boolean);
                }else{
                    throw new \Exception("Mysql Operator 'IS' needs to be 'NULL' OR 'NOT NULL' (or is not supported yet)");
                }
                break;
            case 'IN':
                $query->whereIn($column, $value, $boolean);
                break;
            case 'REGEXP':
                if(!is_array($value)){
                    return false;
                }
                $query->where($column, 'REGEXP', implode('|', $value), $boolean);
                break;
            case '~&':
                $query->whereRaw("~[TABLEALIAS].".$escapedCol." & ?", array($value), $boolean);
                break;
            case 'NOT IN':
                $query->whereNotIn($column, $value, $boolean);
                break;
            default:
                return false;
        }
        return true;
    }
    
    public static function appendWhereWithTable($query,$tableName){
        if(isSet($query->wheres)){
            foreach($query->wheres as $index => $w){
//                if(isset($query->wheres[$index]['column']) && !is_string($query->wheres[$index]['column'])){
//                    ddd($query->wheres[$index]['column']);
//                }
                if(isset($query->wheres[$index]['query'])){
                    QueryBuilder::appendWhereWithTable($query->wheres[$index]['query'],$tableName);
                }elseif(isset($query->wheres[$index]['type']) && $query->wheres[$index]['type'] == 'raw' && isset($query->wheres[$index]['sql'])){
                    $query->wheres[$index]['sql'] = str_replace('[TABLEALIAS]', $tableName, $query->wheres[$index]['sql']);
                }elseif(isset($query->wheres[$index]['column']) && strpos($query->wheres[$index]['column'], '.') === false){
                    $query->wheres[$index]['column'] = $tableName.'.'.$query->wheres[$index]['column'];
                }
            }
        }
        if(isSet($query->columns)){
            foreach($query->columns as $index => $w){
                if(strpos($query->columns[$index], '.') === false){
                    $query->columns[$index] = $tableName.'.'.$query->columns[$index];
                }
            }
        }        
    }
}
