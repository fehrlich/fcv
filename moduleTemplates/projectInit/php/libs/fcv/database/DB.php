<?php

namespace fcv\database;

use fcv\Config;
use impro\login\LoginUser;

/**
 * Description of DB
 *
 * @author xzen
 */
class DB {

    static $handle = false;

    /**
     * 
     * @return myPDO
     */
    static function getObj($reconnect = false) {
        
        if(self::$handle !== false && $reconnect === false){
            return self::$handle;
        }
        
        $className = "fcv\\database\\".Config::get()->getDbClassName();
        
        self::$handle = new $className(
            Config::get()->getDbHost(),  
            Config::get()->getDbUser(), 
            Config::get()->getDbPassword(), 
            Config::get()->getDbDatabaseName(), 
            Config::get()->getDbPort(), 
            Config::get()->getDbSocket()
        );
        
        return self::$handle;
    }
    
    static function reconnect() {
        return self::getObj(true);
    }
    
    static function getCreatedBy(){
        return LoginUser::getCurrentUser()->getId();
    }
    
    static function getCreatedByText($id){
        if($id > 0){
            return LoginUser::load($id);
        }
        return '';
    }
}
