<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace fcv\database;

/**
 * Description of CollectionPrepare
 *
 * @author xzen
 */
class CollectionPrepare {

    //put your code here

    static function create() {
        
    }
    
    static function parseWhere($w){
        $column = isset($w[0]) ? $w[0] : $w['column'];
        $operator = isset($w[1]) ? $w[1] : $w['operator'];
        $value = isset($w[2]) ? $w[2] : $w['value'];
        $boolean = isset($w[3]) ? $w[3] : $w['boolean'];        
        
        return [$column, $operator, $value, $boolean];
    }

    static function createFromObj($objData) {
//        ddd($objData);
        $colPrepareObject = new $objData['class']();
        $colPrepareObject->setTriggerGenericEvents(true);

//    $class = new $objData['class']();
        if (isset($objData['wheres'])) {
            foreach ($objData['wheres'] as $w) {
                $w = CollectionPrepare::parseWhere($w);
                
                if (is_array($w[0])) {
                    $logic = $w[3];
                    $wheres = $w[0]['wheres'];
                    if ($logic == 'or') {
                        $colPrepareObject->orWhere(function($query) use($wheres) {
                            foreach ($wheres as $w) {
                                $w = CollectionPrepare::parseWhere($w);
                                $query->orWhere($w[0], $w[1], $w[2], $w[3]);
                            }
                        });
                    } else {
                        $colPrepareObject->where(function($query) use($wheres) {
                            foreach ($wheres as $w) {
                                $w = CollectionPrepare::parseWhere($w);
                                $query->where($w[0], $w[1], $w[2], $w[3]);
                            }
                        });
                    }
                } else {
                    $colPrepareObject->where($w[0], $w[1], $w[2], $w[3]);
                }
            }
        }
        
        if (isset($objData['columns'])) {
            foreach ($objData['columns'] as $w) {
                if ($w == '*') {
                    $colPrepareObject->selectAll();
                } else {
                    $func = 'select' . ucfirst($w);
                    $colPrepareObject->$func();
                }
            }
        }
        
        if (isset($objData['joins'])) {
            foreach ($objData['joins'] as $attrName => $w) {
                $func = "set" . $attrName;
                $colPrepareObject->$func(CollectionPrepare::createFromObj($w));
            }
        }
        
        if (isset($objData['orderBy']) && $objData['orderBy'] != '') {
            $colPrepareObject->orderBy($objData['orderBy']);
        }
        
        if (isset($objData['limit']) && $objData['limit'] != '') {
            $colPrepareObject->limit($objData['limit']);
        }
        
        if (isset($objData['connectionLogic']) && $objData['connectionLogic'] != '') {
//        echo $objData['connectionLogic'].'!!!';
            $colPrepareObject->setConnectionLogic($objData['connectionLogic']);
        }
//    print_r($objData);
        return $colPrepareObject;
    }

}
