<?php

namespace fcv;

use fcv\traits\Singleton;

class Config {

    use Singleton;
    
    //User Settings
    
    private $organizationAdminEmail = '';
    private $publicSellingEmail = '';
    private $publicSellingName = '';

    //Debug
    private $debug = true;
    
    //Filesystem
    
    private $fileSystemRoot = '';
    
    //DB
    private $dbClassName = 'myPDO';
    private $dbDriver = 'mysql';
    private $dbHost = 'localhost';
    private $dbDatabaseName = '';
    private $dbPort = 3306;
    private $dbSocket = null;
    private $dbUser = '';
    private $dbPassword = '';
    
    //Mail
    private $mailHost = 'localhost';
    private $mailSmtpPort = 25;
    private $mailSmtpUser = '';
    private $mailSmtpPassword = '';
    
    private $mailFromEmail = '';
    private $mailFromName = '';
    
    //Routing
    private $defaultPath = '';
    
    //View
    private $templateEngineClass = 'TemplateEngine';
    private $fixedTime = false;
    private $pageTitle = '';
    private $bodyCssClass = '';
    private $defaultFrame = '';
    
    private $publicFileUrl = '';
    
    //Apache
    
    private $maxHeaderSize = 8*1000;
    
    //Logging
    
    private $loggingPath = false;
    
    //bypass stuff
    public $import = false;
    private $serverHash = 'Xj}:TxS)xKv5aP~YbEQ3T3=Bhz[?XLIxLÖC_';
    
    /**
     * 
     * @return Config
     */
    static function get() {
        return self::getObj();
    }

    //Getter/Setter
    function getDebug() {
        return $this->debug;
    }

    function getDbDriver() {
        return $this->dbDriver;
    }

    function getDbHost() {
        return $this->dbHost;
    }

    function getDbDatabaseName() {
        return $this->dbDatabaseName;
    }

    function getDbPort() {
        return $this->dbPort;
    }

    function getDbUser() {
        return $this->dbUser;
    }

    function getDbPassword() {
        return $this->dbPassword;
    }

    function getDbClassName() {
        return $this->dbClassName;
    }

    function getDefaultPath() {
        return $this->defaultPath;
    }

    function setDebug($debug) {
        $this->debug = $debug;
        return $this;
    }

    function setDbDriver($dbDriver) {
        $this->dbDriver = $dbDriver;
        return $this;
    }

    function setDbHost($dbHost) {
        $this->dbHost = $dbHost;
        return $this;
    }

    function setDbDatabaseName($dbDatabaseName) {
        $this->dbDatabaseName = $dbDatabaseName;
        return $this;
    }

    function setDbPort($dbPort) {
        $this->dbPort = $dbPort;
        return $this;
    }

    function setDbUser($dbUser) {
        $this->dbUser = $dbUser;
        return $this;
    }

    function setDbPassword($dbPassword) {
        $this->dbPassword = $dbPassword;
        return $this;
    }

    function setDbClassName($dbClassName) {
        $this->dbClassName = $dbClassName;
        return $this;
    }

    function setDefaultPath($defaultPath) {
        $this->defaultPath = $defaultPath;
        return $this;
    }

    function getDbSocket() {
        return $this->dbSocket;
    }

    function setDbSocket($dbSocket) {
        $this->dbSocket = $dbSocket;
        return $this;
    }
    function getMailHost() {
        return $this->mailHost;
    }

    function getMailSmtpPort() {
        return $this->mailSmtpPort;
    }

    function getMailSmtpUser() {
        return $this->mailSmtpUser;
    }

    function getMailSmtpPassword() {
        return $this->mailSmtpPassword;
    }

    function getMailFromEmail() {
        return $this->mailFromEmail;
    }

    function getMailFromName() {
        return $this->mailFromName;
    }

    function setMailHost($mailHost) {
        $this->mailHost = $mailHost;
        return $this;
    }

    function setMailSmtpPort($mailSmtpPort) {
        $this->mailSmtpPort = $mailSmtpPort;
        return $this;
    }

    function setMailSmtpUser($mailSmtpUser) {
        $this->mailSmtpUser = $mailSmtpUser;
        return $this;
    }

    function setMailSmtpPassword($mailSmtpPassword) {
        $this->mailSmtpPassword = $mailSmtpPassword;
        return $this;
    }

    function setMailFromEmail($mailFromEmail) {
        $this->mailFromEmail = $mailFromEmail;
        return $this;
    }

    function setMailFromName($mailFromName) {
        $this->mailFromName = $mailFromName;
        return $this;
    }
    
    function getFileSystemRoot() {
        return $this->fileSystemRoot;
    }

    function setFileSystemRoot($fileSystemRoot) {
        $this->fileSystemRoot = $fileSystemRoot;
        return $this;
    }
    
    function getMaxHeaderSize() {
        return $this->maxHeaderSize;
    }

    function setMaxHeaderSize($maxHeaderSize) {
        $this->maxHeaderSize = $maxHeaderSize;
        return $this;
    }
    
    function getFixedTime() {
        return $this->fixedTime;
    }

    function setFixedTime($fixedTime) {
        $this->fixedTime = $fixedTime;
        return $this;
    }
    
    function getLoggingPath() {
        return $this->loggingPath;
    }

    function setLoggingPath($loggingPath) {
        $this->loggingPath = $loggingPath;
        return $this;
    }
    
    function getPageTitle() {
        return $this->pageTitle;
    }

    function setPageTitle($pageTitle) {
        $this->pageTitle = $pageTitle;
        return $this;
    }
    
    function appendPageTitle($addToPageTitle, $seperator = ' - '){
        $this->pageTitle .= $seperator.$addToPageTitle;
        return $this;
    }
    
    function prependPageTitle($addToPageTitle, $seperator = ' - '){
        $this->pageTitle = $addToPageTitle.$seperator.$this->pageTitle;
        return $this;
    }
    
    function getServerHash() {
        return $this->serverHash;
    }

    function setServerHash($serverHash) {
        $this->serverHash = $serverHash;
        return $this;
    }
    
    function getOrganizationAdminEmail() {
        return $this->organizationAdminEmail;
    }

    function setOrganizationAdminEmail($organizationAdminEmail) {
        $this->organizationAdminEmail = $organizationAdminEmail;
        return $this;
    }
    
    function getAbsoluteDomain(){
        return 'http://'.$_SERVER['HTTP_HOST'];
    }
    
    function getBodyCssClass() {
        return $this->bodyCssClass;
    }

    function setBodyCssClass($bodyCssClass) {
        $this->bodyCssClass = $bodyCssClass;
        return $this;
    }
    
    function getDefaultFrame() {
        return $this->defaultFrame;
    }

    function setDefaultFrame($defaultFrame) {
        $this->defaultFrame = $defaultFrame;
        return $this;
    }
    
    function getPublicFileUrl() {
        return $this->publicFileUrl;
    }

    function setPublicFileUrl($publicFileUrl) {
        $this->publicFileUrl = $publicFileUrl;
        return $this;
    }
    
    function getPublicSellingEmail() {
        return $this->publicSellingEmail;
    }

    function setPublicSellingEmail($publicSellingEmail) {
        $this->publicSellingEmail = $publicSellingEmail;
        return $this;
    }
    
    function getPublicSellingName() {
        return $this->publicSellingName;
    }

    function setPublicSellingName($publicSellingName) {
        $this->publicSellingName = $publicSellingName;
        return $this;
    }
}
