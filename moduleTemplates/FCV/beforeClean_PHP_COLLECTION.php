<?php
namespace {{#pathToNamespace}}{{getWorkPackage.getPath}}{{/pathToNamespace}};

use fcv\view\TemplateEngine;
use fcv\libs\Debug;

{{#uniqueLines}}
{{#getAssociationEnds}}
use {{#substr 1}}{{getConnectionClass.getNamePath}}Collection{{/substr 1}};
{{^getOppositeAssociationEnd.isCardinalityZero}}
use {{#substr 1}}{{getOppositeAssociationEnd.getClass.getNamePath}}{{/substr 1}};
{{/getOppositeAssociationEnd.isCardinalityZero}}
{{/getAssociationEnds}}
{{#getAttributes}}
{{#getType.isClass}}
use {{#substr 1}}{{getType.getNamePath}}Collection{{/substr 1}};
{{/getType.isClass}}
{{/getAttributes}}

{{#getImplements}}
use {{#substr 1}}{{getNamePath}}{{/substr 1}};
{{/getImplements}}

{{#getSpecializes}}
use {{#substr 1}}{{getNamePath}}Collection{{/substr 1}};

{{/getSpecializes}} 

{{/uniqueLines}}
{{^isEnum}}


{{#isInterface}}//Realizes:{{#getRealizes}} {{getName}}{{/getRealizes}}{{/isInterface}}
{{#isInterface}}interface{{/isInterface}}{{^isInterface}}class{{/isInterface}} {{getName}}Collection
        {{#isEnum}}{{^specializeClasses}} extends SplEnum{{/specializeClasses}}{{/isEnum}}
        {{#getSpecializes}} extends {{getName}}Collection{{/getSpecializes}}
        implements {{#getImplements}}
            {{getName}},
        {{/getImplements}}\Iterator, \ArrayAccess, \Countable {
            
    private $index = 0;
    /**
     * @var {{getName}}[] $objArray
     */
    protected $objArray = array();


    public function __construct($data = array()){
        $this->objArray = $data;
    }

    /**
     * @param {{getName}}[] $data
     * @return {{getName}}Collection inserted Id
     */
    public static function create($data = array(),$loadObj = false){
        if($loadObj) $data['loadObj'] = true;
        return new {{getName}}Collection($data);
    }
    /**
     * @param {{getName}} $el
     * @return {{getName}}Collection inserted Id
     */
    public function add($el){
        if($el instanceof {{getName}}Collection){
            foreach($el as $e){
                $this->objArray[] = $e;
            }
        }else{
            $this->objArray[] = $el;            
        }
    }

    public function first() {
        if($this->isEmpty()){
            throw new \fcv\exceptions\CollectionIsEmpty('Collection is empty, cant get first element');
        }
        return $this->objArray[0];
    }
    
    
    public function last() {
        if($this->isEmpty()){
            throw new \fcv\exceptions\CollectionIsEmpty('Collection is empty, cant get current element');
        }
        $this->index = count($this->objArray)-1;
        return $this->current();
    }
    
    public function current() {
        if($this->isEmpty()){
            throw new \fcv\exceptions\CollectionIsEmpty('Collection is empty, cant get current element');
        }
        return $this->objArray[$this->index];
    }

    public function key() {
        return $this->index;
    }

    public function next() {
        $this->index++;
        return $this;
    }

    public function rewind() {
        $this->index = 0;
        return $this;
    }

    public function valid() {
        return isset($this->objArray[$this->index]);
    }
    public function append($el) {
        return $this->add($el);
    }
    public function count() {
        return count($this->objArray);
    }

    public function offsetExists($offset) {
        return isSet($this->objArray[$offset]);
    }

    public function offsetGet($offset) {
        return $this->objArray[$offset];
    }

    public function offsetSet($offset, $value) {
        $this->objArray[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->objArray[$offset]);
    }

    public function seek($position) {
        $this->index = $position;
    }
    
    public function serialize() {
        return serialize($this->objArray);
    }
    
    public function pop() {
        return array_pop($this->objArray);
    }
    
    public function isEmpty() {
        return empty($this->objArray);
    }
    
    public function chunk($size){
        return array_chunk($this->objArray, $size);        
    }

    public function unserialize($serialized) {
        return unserialize($serialized);
    }
    
    public function toArray($simple = false, $treevisit = true){
       $arr = array();
       foreach($this->objArray as $el){
           $arr[] = $el->toArray($simple, $treevisit);
       }
       return $arr;
    }
    
    //UserMethods
    {{#getOperations}}
    /**
      *
{{#getParameters}}
      * @param {{getType.getName}} ${{getName}}
{{/getParameters}}
      * @return {{getParent.getName}}Collection this Collection Object
      */
    {{^isAbstract}}{{^isStatic}}{{^isInterface}}
    public function {{getName}}({{#getParameters}}{{^first}}, {{/first}}{{^isDefaultType}}{{getType.getName}}{{/isDefaultType}} ${{getName}}{{/getParameters}}){
        foreach($this->objArray as $el){
           $el->{{getName}}({{#getParameters}}{{^first}}, {{/first}}${{getName}}{{/getParameters}});
       }
       return $this;
    }
    {{/isInterface}}{{/isStatic}}{{/isAbstract}}
    {{/getOperations}}

    //Setters
    {{#getAttributes}}
    /**
    *
    * @param {{getType.getName}}{{^isCardinalitySingle}}[]{{/isCardinalitySingle}} ${{getName}}
    * @return {{getParent.getName}}
    */
    {{getVisibility}} function {{setterFunction}}(${{getName}}){
       foreach($this->objArray as $el){
           $el->{{setterFunction}}(${{getName}});
       }
       return $this;
    }
    {{/getAttributes}}
    {{#getAssociationEnds}}
    {{^isCardinalityZero}}
    public function {{setAssociationFunction}}({{getConnectionClass.getName}} ${{getNameLCFirst}}){
       foreach($this->objArray as $el){
           $el->{{setAssociationFunction}}(${{getNameLCFirst}});
       }
       return $this;
    }
    {{/isCardinalityZero}}
    {{/getAssociationEnds}}
    public function {{getSaveOperation}}(){
       foreach($this->objArray as $el){
           $el->save();
       }
       return $this;
    }
    public function treeSave(){
       foreach($this->objArray as $el){
           $el->treeSave();
       }
       return $this;
    }
    public function getHtml($template = 'default'){
       
       $html = '';
       if(substr($template, 0, 11) == 'collection_'){
            
        $availableTemplates =  array(
        'default' => '.default'
        {{#getWorkPackage.getViews}}
            ,'{{getName}}' => '{{../getName}}.{{getName}}'
            {{#getWorkPackage.getChilds}}
            {{#getViews}}
            ,'{{../getName}}/{{getName}}' => '/{{../getName}}/{{../getName}}.{{getName}}'
            {{/getViews}}
            {{/getWorkPackage.getChilds}}
        {{/getWorkPackage.getViews}}
        );
        {{#getWorkPackage.getChilds}}
        {{#getViews}}
        if($template == '{{../getName}}/{{getName}}') $this->{{../getName}}();
        {{/getViews}}
        {{/getWorkPackage.getChilds}}

        //if(!in_array($template, $availableTemplates)){
        if(!isset($availableTemplates[$template])){
            {{^getSpecializes}}
            throw new \Exception('Template <b>'.$template.'</b> doesn\'t exist for <b>{{getName}}</b>');
            {{/getSpecializes}} 
            {{#getSpecializes}}
            return parent::getHtml($template);
            {{/getSpecializes}} 
        }
        $template = $availableTemplates[$template];

        {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        $templates = array(
            {{#getWorkPackage}}
            {{#list getViews}}
            {{^first}},{{/first}}'{{../getName}}.{{value.getName}}' => '{{#value}}{{#includeTemplate 1}}{{&getHtmlContent}}{{/includeTemplate 1}}{{/value}}' //{{value.getId}}
            {{/list getViews}}
            {{/getWorkPackage}}
        );
        $tplSrc = $templates[$template];
        {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        {{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        $tplSrc = file_get_contents('FCVAPs{{#getWorkPackage}}/{{getPath}}/{{/getWorkPackage}}'.$template.'.html');
        {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}

//        foreach($this->tplVars as $name => $content){
//            $this->$name = $content;
//        }
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = TemplateEngine::get()->render($tplSrc, $this, '{{getName}}Collection'.$template);
        Debug::getObj()->setThrowErrors($throwBefore);

        return $htmlSrc; 
           
       }
       foreach($this->objArray as $el){
           $html .= $el->getHtml($template);
       }
       return $html;
    }
    
    public function each($cb){
        foreach($this->objArray as $i => $o){
            $cb($o,$i);
        }
        return $this;
    }
    
    public function addToJs(){
        foreach($this->objArray as $i => $o){
            $o->addToJs();
        }
        return $this;
    }
    {{^hasPrimaryKeyAttributes}}
    public function setParent($parent, $index = 0){
        foreach($this->objArray as $i => $o){
            $o->setParent($parent, $index = 0);
        }
        return $this;
    }
    
    {{/hasPrimaryKeyAttributes}}
    //Map Reduce Pattern
    public function map($mapFunction){
        $ret = array();
        foreach($this->objArray as $i => $o){
            $ret[$i] = $mapFunction($o);
        }
        return $ret;
    }
    
    public function reduce($reduceFunction){

        $length = count($this->objArray);
        if($length <= 1) return $this->objArray;

        $array = $this->objArray;
        $out = array_shift($array);
        do {
            $next = array_shift($array);
            $out = $reduceFunction($out, $next);
        } while(!empty($array));

        return $out;
    }
    
    public function filter($filterFunction){
        
        $ret = array();
        
        foreach($this->objArray as $obj){
            if($filterFunction($obj)){
                $ret[] = $obj;
            }
        }

        return new {{getName}}Collection($ret);
    }
    
    public function sortBy($by, $desc = false){
        usort($this->objArray, function($a, $b) use($by,$desc) {
            $aValue = $a->get($by);
            $bValue = $b->get($by);
            
            if($aValue == $bValue){
                return 0;
            }
            
            if($desc){
                return ($aValue < $bValue) ? 1 : -1;
            }
            
            return ($aValue < $bValue) ? -1 : 1;
        });
        
        return $this;
    }
    
    public function getId($id){
        foreach($this->objArray as $i => $o){
            if($o->getId() == $id) return $o;
        }
    }
    public function hasId($id){
        foreach($this->objArray as $i => $o){
            if($o->getId() == $id) return true;
        }
        return false;
    }
    public function toJSObject($returnArray = false, $hash = true, $addObjectAndId = true){
        $arr = array();
        foreach($this->objArray as $i => $o){
            $arr[] = $o->toJSObject($returnArray, $hash, $addObjectAndId);
        }
        return $arr;
    }
    
    function sum($field){
        $ret = 0;
        foreach($this->objArray as $i => $o){
            $val =  $o->get($field);
            if($val instanceof \impro\typen\DefaultTypeParser) $val = $val->toString();
            $ret += $val;
        }
        return $ret;        
    }
    function times($field){
        $ret = 0;
        foreach($this->objArray as $i => $o){
            $val =  $o->get($field);
            if($val instanceof \impro\typen\DefaultTypeParser) $val = $val->toString();
            $ret *= $val;
        }
        return $ret;        
    }
    function sub($field){
        $ret = 0;
        foreach($this->objArray as $i => $o){
            $val = $o->get($field);
            if($val instanceof \impro\typen\DefaultTypeParser) $val = $val->toString();
            $ret -= $val;
        }
        return $ret;
    }
    function has($name, $id = 0){
        if($this->count() == 0) return false;
        
        return $this->current()->has($name, $id);
    }
    
    function get($name){
        $ret = array();
        foreach($this->objArray as $o){
            $ret[] = $o->get($name);
        }
        return $ret;
    }
    
    
    function exists($name){
        foreach($this->objArray as $o){
            $v = $o->has($name);
            if($v === false) return false;
        }
        return true;
    }
    
    function removeIndex($index){
        array_splice ($this->objArray, $index, 1);
            
        return $this;
    }
    function removeById($id){
        foreach($this->objArray as $i => $o){
            if($o->getId() == $id){
                return $this->removeIndex($i);
            }
        }
        return $this;
    }
}
{{/isEnum}}