<?php
use fcv\libs\Debug;
class mys extends mysqli{
    static $handle = false;
    
    private $connected = false;
    
    public function connect($host = '{{&getWorkPackage.getProject.getMysqlHost}}', $user = '{{&getWorkPackage.getProject.getMysqlUser}}', $password = '{{&getWorkPackage.getProject.getMysqlPassword}}', $database = '{{&getWorkPackage.getProject.getMysqlDb}}', $port = {{&getWorkPackage.getProject.getMysqlPort}}, $socket = NULL) {
        parent::connect($host, $user, $password, $database, $port, $socket);
        $this->connected = true;
    }
    
    public function query($query){
        Debug::getObj()->setDebugPoint('SQLQuery: ', $query);
        return parent::query($query);
    }
    
    /**
     * 
     * @return mys
     */
    static function getObj(){
        mys::$handle = (mys::$handle !== false)?mys::$handle:new mys();
        if(! mys::$handle->connected)  mys::$handle->connect();
        return  mys::$handle;
    }
    
    
    
    function insertArray($table,$array){
        $fields = array_keys($array);

        $sql = 'INSERT INTO `'.$table.'` 
                    (`'.implode('`,`',$fields).'`)
                    VALUES (\''.implode("','",$array).'\')';
        mys::getObj()->query($sql);
        echo mys::getObj()->error;
        return mys::getObj()->insert_id;    
    } 
}

class myPDO extends PDO{
    static $handle = false;
    
    public function __construct($host = '{{&getWorkPackage.getProject.getMysqlHost}}', $user = '{{&getWorkPackage.getProject.getMysqlUser}}', $password = '{{&getWorkPackage.getProject.getMysqlPassword}}', $database = '{{&getWorkPackage.getProject.getMysqlDb}}', $port = {{&getWorkPackage.getProject.getMysqlPort}}, $socket = NULL) {
        parent::__construct('mysql:host='.$host.';dbname='.$database.';port='.$port, $user, $password);
        $this->connected = true;
    }
    
    static function getObj(){
        myPDO::$handle = (myPDO::$handle !== false)?myPDO::$handle:new myPDO();
        if(! myPDO::$handle->connected)  myPDO::$handle->connect();
        return  myPDO::$handle;
    }
}


use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\MySqlGrammar;
use Illuminate\Database\Query\Processors\MySqlProcessor;
use Illuminate\Database\Connection;

class QueryBuilder extends Builder{
    static $handle = false;
        
    public function __construct() {
        $connection = new Connection(myPDO::getObj());
        parent::__construct($connection, new MySqlGrammar(), new MySqlProcessor());
    }
    
    static function getObj(){
        QueryBuilder::$handle = (QueryBuilder::$handle !== false)?QueryBuilder::$handle:new QueryBuilder();
        return  QueryBuilder::$handle;
    }   
    
    static function create(){
        return new QueryBuilder();
    }    
    
    public function mergeQuery(&$query, $logic = "and"){
        $logic = strtolower($logic);
        if(isSet($query->wheres)){
            $this->addNestedWhereQuery($query, $logic);
        }
        if(isSet($query->joins)){
            foreach($query->joins as $join){
                $this->joins[] = $join;
            }
        }
        if(isSet($query->columns)){
            foreach($query->columns as $cols){
                $this->columns[] = $cols;
            }
        }
    }
    
    
    public static function appendWhereWithTable(&$query,$tableName){
        if(isSet($query->wheres)){
            foreach($query->wheres as $index => $w){
                if(isset($query->wheres[$index]['query'])){
                    QueryBuilder::appendWhereWithTable($query->wheres[$index]['query'],$tableName);
                }elseif(isset($query->wheres[$index]['column']) && strpos($query->wheres[$index]['column'], '.') === false){
                    $query->wheres[$index]['column'] = $tableName.'.'.$query->wheres[$index]['column'];
                }
            }
        }
        if(isSet($query->columns)){
            foreach($query->columns as $index => $w){
                if(strpos($query->columns[$index], '.') === false){
                    $query->columns[$index] = $tableName.'.'.$query->columns[$index];
                }
            }
        }        
    }
}
