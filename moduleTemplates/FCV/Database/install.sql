{{#getWorkPackage.getProject.getMysqlModel}}

SET autocommit=0;
SET unique_checks=0;
SET foreign_key_checks=0;
#DROP FOR DEBUG
#DROP DATABASE `{{getWorkPackage.getProject.getMysqlDb}}`;
#CREATE DATABASE `{{getWorkPackage.getProject.getMysqlDb}}`;
#USE `{{getWorkPackage.getProject.getMysqlDb}}`;

#Classes
{{#getTables}}
{{#hasFields}}
CREATE TABLE `{{getName}}` (
    {{#getFields}}
    {{^first}},{{/first}} `{{getName}}` {{getType}}
    {{/getFields}}
    {{#hasPrimaryField}}, PRIMARY KEY ({{#getPrimaryFieldsList}}{{^first}},{{/first}}`{{value.getName}}`{{/getPrimaryFieldsList}}){{/hasPrimaryField}}
)
COLLATE='{{charset}}'
ENGINE={{engine}};
{{/hasFields}}
{{/getTables}}

#SetForeignKeys
{{#getForeignKeyTables}}
ALTER TABLE `{{getName}}`
{{#getForeignFields}}
	{{^first}}, {{/first}}ADD FOREIGN KEY (`{{getName}}`) REFERENCES `{{#getForeignClass}}{{getName}}{{#specializeClasses}}_sub{{/specializeClasses}}`(`{{getPrimaryKeyAttribute.getName}}`){{/getForeignClass}} ON UPDATE {{getOnUpdate}} ON DELETE {{getOnDelete}}

{{/getForeignFields}}
;
{{/getForeignKeyTables}}
COMMIT;
#setViews
{{#getViews}}
CREATE VIEW `{{getName}}`
    AS SELECT 
            t.{{getUmlElement.getPrimaryKeyAttribute.getName}},
            t1.createdBy,
            t1.created,
            t1.lastModified
            {{#getAllGeneralizations}}
            {{#fields}}
            ,`t{{index}}`.{{name}}
            {{/fields}}
            {{/getAllGeneralizations}}
            {{#getTable}}
            {{#getFields}}
            {{^isPrimaryKey}},t.`{{getName}}`{{/isPrimaryKey}}
            {{/getFields}}
            {{/getTable}}
        FROM `{{getName}}_sub` AS t
        {{#getAllGeneralizations}}
        INNER JOIN `{{name}}` AS t{{index}} ON t{{index}}.`{{primaryKey}}` = t{{indexParent}}.`{{primaryKeyParent}}`
        {{/getAllGeneralizations}}
        {{#getUmlElement.getSpecializes}}
        #INNER JOIN `{{getName}}` AS t2 ON t.`{{getPrimaryKeyAttribute.getName}}` = t2.`{{getUmlElement.getPrimaryKeyAttribute.getName}}`
        {{/getUmlElement.getSpecializes}}
#Todo: Disjunkt
    WITH CHECK OPTION;
{{/getViews}}
COMMIT;

SET autocommit=1;
SET unique_checks=1;
SET foreign_key_checks=1;
{{/getWorkPackage.getProject.getMysqlModel}}