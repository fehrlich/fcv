{{#getWorkPackage.getProject.getMysqlModel}}

#Classes
{{#getTables}}
{{#hasFields}}

{{#getUmlElement.isNew}}CREATE TABLE `{{getName}}` (
    {{#getFields}}
    {{^first}},{{/first}} `{{getName}}` {{getType}}
    {{/getFields}}
    {{#hasPrimaryField}}, PRIMARY KEY ({{#getPrimaryFieldsList}}{{^first}},{{/first}}`{{getName}}`{{/getPrimaryFieldsList}}){{/hasPrimaryField}}
)
COLLATE='{{charset}}'
ENGINE={{engine}};
{{/getUmlElement.isNew}}
{{#getUmlElement.hasChanged}}
RENAME TABLE `{{getUmlElement.getOldUmlElement.getName}}` TO `{{getName}}`;
ALTER TABLE `{{getName}}` (
    {{#getFields}}
    {{^first}},{{/first}}
    {{#getUmlElement.isNew}}ADD COLUMN{{/getUmlElement.isNew}}
    {{#getUmlElement.hasChanged}}CHANGE COLUMN `{{getUmlElement.getOldUmlElement.getName}}`{{/getUmlElement.hasChanged}}
        `{{getName}}` {{getType}}
    {{/getFields}}
);

{{/getUmlElement.hasChanged}}
{{/hasFields}}
{{/getTables}}

#SetForeignKeys
{{#getForeignKeyTables}}
{{#getUmlElement.hasChanged}}
ALTER TABLE `{{getName}}`
{{#getForeignFields}}
    {{#getUmlElement.hasChanged}}
        DROP INDEX `{{getUmlElement.getOldUmlElement.getName}}`,
        CHANGE COLUMN `{{getUmlElement.getOldUmlElement.getName}}`{{/getUmlElement.hasChanged}}
        `{{getName}}` {{getType}}
	#{{^first}}, {{/first}}ADD FOREIGN KEY (`{{getName}}`) REFERENCES `{{#getForeignClass}}{{getName}}{{#specializeClasses}}_sub{{/specializeClasses}}`(`{{getPrimaryKeyAttribute.getName}}`){{/getForeignClass}} ON UPDATE CASCADE
        #{{^first}}, {{/first}}ADD FOREIGN KEY (`{{getName}}`) REFERENCES `{{#getForeignClass}}{{getName}}{{#specializeClasses}}_sub{{/specializeClasses}}`(`{{getPrimaryKeyAttribute.getName}}`){{/getForeignClass}} ON UPDATE CASCADE
{{/getForeignFields}}
;
{{/getUmlElement.hasChanged}}
{{/getForeignKeyTables}}

#setViews
{{#getViews}}
{{#getUmlElement.isNew}}
CREATE VIEW `{{getName}}`
    AS SELECT
            `t2`.*
            {{#getTable}}
            {{#getFields}}
            {{^isPrimaryKey}},t.`{{getName}}`{{/isPrimaryKey}}
            {{/getFields}}
            {{/getTable}}
        FROM `{{getName}}_sub` AS t
        {{#getUmlElement.getSpecializes}}
        INNER JOIN `{{getName}}` AS t2 ON t.`{{getPrimaryKeyAttribute.getName}}` = t2.`{{getUmlElement.getPrimaryKeyAttribute.getName}}`
        {{/getUmlElement.getSpecializes}}
#Todo: Disjunkt
    WITH CHECK OPTION;
{{/getUmlElement.isNew}}
{{#getUmlElement.hasChanged}}
RENAME TABLE `{{getUmlElement.getOldUmlElement.getName}}` TO `{{getName}}`;
ALTER VIEW `{{getName}}`
    AS SELECT
            `t2`.*
            {{#getTable}}
            {{#getFields}}
            {{^isPrimaryKey}},t.`{{getName}}`{{/isPrimaryKey}}
            {{/getFields}}
            {{/getTable}}
        FROM `{{getName}}_sub` AS t
        {{#getUmlElement.getSpecializes}}
        INNER JOIN `{{getName}}` AS t2 ON t.`{{getPrimaryKeyAttribute.getName}}` = t2.`{{getUmlElement.getPrimaryKeyAttribute.getName}}`
        {{/getUmlElement.getSpecializes}}
#Todo: Disjunkt
    WITH CHECK OPTION;
{{/getUmlElement.hasChanged}}
{{/getViews}}
{{/getWorkPackage.getProject.getMysqlModel}}