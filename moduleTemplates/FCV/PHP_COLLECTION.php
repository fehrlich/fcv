<?php
namespace {{#pathToNamespace}}{{getWorkPackage.getPath}}{{/pathToNamespace}};

use fcv\view\TemplateEngine;
use fcv\libs\Debug;
use fcv\traits\StaticEventAction;
use fcv\metaclass\Collection;

{{#uniqueLines}}
{{#getAssociationEnds}}
use {{#substr 1}}{{getConnectionClass.getNamePath}}Collection{{/substr 1}};
{{^getOppositeAssociationEnd.isCardinalityZero}}
use {{#substr 1}}{{getOppositeAssociationEnd.getClass.getNamePath}}{{/substr 1}};
{{/getOppositeAssociationEnd.isCardinalityZero}}
{{/getAssociationEnds}}
{{#getAttributes}}
{{#getType.isClass}}
use {{#substr 1}}{{getType.getNamePath}}Collection{{/substr 1}};
{{/getType.isClass}}
{{/getAttributes}}

{{#getImplements}}
use {{#substr 1}}{{getNamePath}}{{/substr 1}};
{{/getImplements}}

{{#getSpecializes}}
use {{#substr 1}}{{getNamePath}}Collection{{/substr 1}};

{{/getSpecializes}} 

{{/uniqueLines}}
{{^isEnum}}


{{#isInterface}}//Realizes:{{#getRealizes}} {{getName}}{{/getRealizes}}{{/isInterface}}
{{#isInterface}}interface{{/isInterface}}{{^isInterface}}class{{/isInterface}} {{getName}}Collection
        {{#isEnum}}{{^specializeClasses}} extends SplEnum{{/specializeClasses}}{{/isEnum}}
        {{#getSpecializes}} extends {{getName}}Collection{{/getSpecializes}}
        {{^getSpecializes}} extends Collection{{/getSpecializes}}
        implements {{#getImplements}}
            {{getName}},
        {{/getImplements}}\Iterator, \ArrayAccess, \Countable {
    {{^specializeClasses}}
    use \fcv\traits\Collection;
    {{/specializeClasses}}        
            
    use StaticEventAction;
        
    //UserMethods
    {{#getOperations}}
    /**
      *
{{#getParameters}}
      * @param {{getType.getName}} ${{getName}}
{{/getParameters}}
      * @return {{getParent.getName}}Collection this Collection Object
      */
    {{^isAbstract}}{{^isStatic}}{{^isInterface}}
    public function {{getName}}({{#getParameters}}{{^first}}, {{/first}}{{^isDefaultType}}{{getType.getName}}{{/isDefaultType}} ${{getName}}{{/getParameters}}){
        foreach($this->objArray as $el){
           $el->{{getName}}({{#getParameters}}{{^first}}, {{/first}}${{getName}}{{/getParameters}});
       }
       return $this;
    }
    {{/isInterface}}{{/isStatic}}{{/isAbstract}}
    {{/getOperations}}

    //Setters
    {{#getAttributes}}
    /**
    *
    * @param {{getType.getName}}{{^isCardinalitySingle}}[]{{/isCardinalitySingle}} ${{getName}}
    * @return {{getParent.getName}}
    */
    {{getVisibility}} function {{setterFunction}}(${{getName}}){
       foreach($this->objArray as $el){
           $el->{{setterFunction}}(${{getName}});
       }
       return $this;
    }
    {{/getAttributes}}
    {{#getAssociationEnds}}
    {{^isCardinalityZero}}
    public function {{setAssociationFunction}}({{getConnectionClass.getName}} ${{getNameLCFirst}}){
       foreach($this->objArray as $el){
           $el->{{setAssociationFunction}}(${{getNameLCFirst}});
       }
       return $this;
    }
    {{/isCardinalityZero}}
    {{/getAssociationEnds}}
    
    
        
    public function toArray(){
       $arr = array();
       foreach($this->objArray as $el){
           $arr[] = $el->toArray();
       }
       return $arr;
    }
    
    /**
     * @return {{getName}}Collection
     */
    public function {{getSaveOperation}}(){
       foreach($this->objArray as $el){
           $el->save();
       }
       return $this;
    }
    /**
     * 
     * @return {{getName}}Collection
     */
    public function treeSave(){
       foreach($this->objArray as $el){
           $el->treeSave();
       }
       return $this;
    }
    
    public function getHtml($template = 'default'){
       
       $html = '';
       if(substr($template, 0, 11) == 'collection_'){
            
        $availableTemplates =  array(
        'default' => '.default'
        {{#getWorkPackage.getViews}}
            ,'{{getName}}' => '{{../getName}}.{{getName}}'
            {{#getWorkPackage.getChilds}}
            {{#getViews}}
            ,'{{../getName}}/{{getName}}' => '/{{../getName}}/{{../getName}}.{{getName}}'
            {{/getViews}}
            {{/getWorkPackage.getChilds}}
        {{/getWorkPackage.getViews}}
        );
        {{#getWorkPackage.getChilds}}
        {{#getViews}}
        if($template == '{{../getName}}/{{getName}}') $this->{{../getName}}();
        {{/getViews}}
        {{/getWorkPackage.getChilds}}

        //if(!in_array($template, $availableTemplates)){
        if(!isset($availableTemplates[$template])){
            {{^getSpecializes}}
            throw new \Exception('Template <b>'.$template.'</b> doesn\'t exist for <b>{{getName}}</b>');
            {{/getSpecializes}} 
            {{#getSpecializes}}
            return parent::getHtml($template);
            {{/getSpecializes}} 
        }
        $template = $availableTemplates[$template];

        {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        $templates = array(
            {{#getWorkPackage}}
            {{#list getViews}}
            {{^first}},{{/first}}'{{../getName}}.{{value.getName}}' => '{{#value}}{{#includeTemplate 1}}{{&getHtmlContent}}{{/includeTemplate 1}}{{/value}}' //{{value.getId}}
            {{/list getViews}}
            {{/getWorkPackage}}
        );
        $tplSrc = $templates[$template];
        {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        {{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        $tplSrc = file_get_contents('FCVAPs{{#getWorkPackage}}/{{getPath}}/{{/getWorkPackage}}'.$template.'.html');
        {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}

        foreach($this->tplVars as $name => $content){
            $this->$name = $content;
        }
        
        $throwBefore = Debug::getObj()->getThrowErrors();
        Debug::getObj()->setThrowErrors(false);
        $htmlSrc = TemplateEngine::get()->render($tplSrc, $this, '{{getName}}Collection'.$template);
        Debug::getObj()->setThrowErrors($throwBefore);

        return $htmlSrc; 
           
       }
       foreach($this->objArray as $el){
           $html .= $el->getHtml($template);
       }
       return $html;
    }
        
    public function addToJs($selectionString = '', $tailorArray = false){
        
        foreach($this->objArray as $i => $o){
            $o->addToJs($selectionString != '' ? $selectionString.'.'.$i : '', $tailorArray);
        }
        return $this;
    }
    
    public function getId($id){
        foreach($this->objArray as $i => $o){
            if($o->getId() == $id) return $o;
        }
    }
    public function hasId($id){
        foreach($this->objArray as $i => $o){
            if($o->getId() == $id) return true;
        }
        return false;
    }
    
    public function getCollectionPrepare(){
        return {{getName}}CollectionPrepare::create();
    }
    
    public function toJSObject($returnArray = false, $hash = true, $addObjectAndId = true, $subTailor = false){
        $arr = array();
        foreach($this->objArray as $i => $o){
            $arr[] = $o->toJSObject($returnArray, $hash, $addObjectAndId, $subTailor);
        }
        return $arr;
    }
    
    public function addJSVariable($name, $value = null){
        foreach($this->objArray as $i => $o){
           $o->addJSVariable($name, $value);
        }
        return $this;
    }
    
    public function loadJsObject($hash = false, $subTailor = array()){
        $arr = array();
        foreach($this->objArray as $i => $o){
            $arr[] = $o->loadJsObject($hash, $subTailor);
        }
        return $arr;
    }
    
    function sum($field){
        $ret = 0;
        foreach($this->objArray as $i => $o){
            try{
                $val =  $o->get($field);
                if($val instanceof \impro\typen\DefaultTypeParser) $val = $val->toString();
                $ret += $val;
            }
            catch (\fcv\exceptions\EmptyChainException $exc) {}
        }
        return $ret;        
    }
    
    function times($field){
        $ret = 0;
        foreach($this->objArray as $i => $o){
            try{
                $val =  $o->get($field);
                if($val instanceof \impro\typen\DefaultTypeParser) $val = $val->toString();
                $ret *= $val;
            }
            catch (\fcv\exceptions\EmptyChainException $exc) {}
        }
        return $ret;        
    }
    
    function sub($field){
        $ret = 0;
        foreach($this->objArray as $i => $o){
            try{
                $val = $o->get($field);
                if($val instanceof \impro\typen\DefaultTypeParser) $val = $val->toString();
                $ret -= $val;
            }
            catch (\fcv\exceptions\EmptyChainException $exc) {}
        }
        return $ret;
    }
    
    function has($name, $id = 0){
        if($this->count() == 0) return false;
        
        if(substr($name,0,6) == 'first.'){
            $subName = substr($name,6);
            return $this->first()->has($subName, $id);
        }
        foreach($this->objArray as $o){
            $ret = $o->has($name);
            if(!$ret) return false;
        }
        return true;
    }
    
    function get($name){
        $ret = array();
        
        if(substr($name,0,6) == 'first.'){
            $subName = substr($name,6);
            return $this->first()->get($subName);
        }
        foreach($this->objArray as $o){
            $ret[] = $o->get($name);
        }
        return $ret;
    }
    
    
    function exists($name){
        if(substr($name,0,6) == 'first.'){
            $subName = substr($name,6);
            return $this->first()->exists($subName);
        }
        foreach($this->objArray as $o){
            $v = $o->exists($name);
            if($v === false) return false;
        }
        return true;
    }
    
    function set($name, $value){
        if(substr($name,0,6) == 'first.'){
            $subName = substr($name,6);
            $this->first()->set($subName, $value);
        }else{
            foreach($this->objArray as $o){
                $o->set($name, $value);
            }
        }
        return $this;
    }
    
    function removeById($id){
        foreach($this->objArray as $i => $o){
            if($o->getId() == $id){
                return $this->removeIndex($i);
            }
        }
        return $this;
    }
}
{{/isEnum}}