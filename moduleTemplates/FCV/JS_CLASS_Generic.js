//requirejs([{{#getSpecializes}}"FCVAPs{{getWorkPackage.getPath}}/{{getName}}.js"{{/getSpecializes}}], function(){
//var {{getName}};
define(['require'{{#getSpecializes}},'{{#substr 1}}{{getWorkPackage.getPath}}{{/substr 1}}'{{/getSpecializes}}], //{{#multi}},'js/classes{{req.getWorkPackage.getPath}}Collection.js'{{/multi}}
function (require{{#getSpecializes}},{{getName}}F{{/getSpecializes}}) { //{{#multi}},{{req.getName}}CollectionF{{/multi}}
    "use strict";
    {{#isPageOrFrame}}
    {{#getWorkPackage}}
    function {{getName}}(obj,fromInherit){}
    {{getName}}.prototype.loadPage = function(){
        
        {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        {{#includeCode}}{{#indent 2}}{{#singleParse}}{{&getControlContent}}{{/singleParse}}{{/indent 2}}{{/includeCode}}
        {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    }
    {{getName}}.create = function(){
        return new {{getName}}();
    };
    {{/getWorkPackage}}
    {{/isPageOrFrame}}
    {{^isPageOrFrame}}
    function {{getName}}(obj,fromInherit){
        {{#getPrimaryKeyAttribute}}var id = obj['{{getPrimaryKeyAttribute.getName}}'];{{/getPrimaryKeyAttribute}}
        {{^getPrimaryKeyAttribute}}var id = obj['id'] || Math.random();{{/getPrimaryKeyAttribute}}
        {{^getSpecializes}}FcvMetaClass.call(this, obj,true, id);{{/getSpecializes}}
        {{#getSpecializes}}{{getName}}.call(this, obj,true);{{/getSpecializes}}
    };
    
    {{getName}}.on = function(name,cb){
        if(!{{getName}}.onCallbacks[name]) {{getName}}.onCallbacks[name] = [];
        {{getName}}.onCallbacks[name].push(cb);
    };
        
    
    {{getName}}.trigger = function(name, arg){
        if({{getName}}.onCallbacks[name]){
            for(var index in {{getName}}.onCallbacks[name]){
                {{getName}}.onCallbacks[name][index].call({{getName}}, arg);
            }
        }
    };
    
    {{getName}}.triggerStatic = function(name,arg){
        {{getName}}.trigger(name,arg);
    };
    
    //Helper
    {{^isSingleton}}
    {{getName}}.create = function(obj){
        obj = obj || {};
        return new {{getName}}(obj);
    };
    {{/isSingleton}}
    {{#isSingleton}}
    {{getName}}.instance = null;
    {{getName}}.get = function(){
        if({{getName}}.instance === null) {{getName}}.instance = new {{getName}}();
        return {{getName}}.instance;
    };
    {{/isSingleton}}
    
    //MetaData
    {{getName}}.fcvFields = [{{#getAttributes}}'{{getName}}',{{/getAttributes}}];
                
    {{getName}}.getClassName = function(){
        return '{{getName}}';
    };
    
    {{getName}}.prototype.getClassName = function(){
        return '{{getName}}';
    };
    {{#usesTypeParser}}
    
    {{getName}}.null = function(){
        var n = new {{getName}}();
        n.isNull = true;
        return n;
    };    
    {{/usesTypeParser}}
            
    {{#getSpecializes}}
    
    var _parent = {{getName}}.prototype;
    {{/getSpecializes}}
    
    //Attributes Getter Setter
    {{#getAttributes}}
    {{../getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getName}}{{#getDefault}} = {{&.}}{{/getDefault}};

    if(!{{../getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getterFunction}}) {{../getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getterFunction}} = function(){
        return this.returnGetter('{{getName}}', {{#isClass}}true{{/isClass}}{{^isClass}}false{{/isClass}});
    };

    if(!{{../getName}}{{^isStatic}}.prototype{{/isStatic}}.{{setterFunction}}) {{../getName}}{{^isStatic}}.prototype{{/isStatic}}.{{setterFunction}} = function({{getName}}, fromUser){
        this.returnSetter();
        fromUser = fromUser || false;
        {{^isDefaultType}}
        if({{getName}} == null) this.{{getName}} = null;
        else if({{getName}} == '[null]') this.{{getName}} = null;


        {{^isCardinalityMulti}}
        {{#isClass}}
        else if(typeof {{getName}} == 'object' && {{getName}} instanceof {{getType.getName}}) this.{{getName}} = {{getName}};
//            else if(typeof {{getName}} == 'object' && {{getName}}.class && {{getName}}.id) this.{{getName}} = {{getName}};
        else if(typeof {{getName}} == 'object') this.{{getName}} = new {{getType.getName}}({{getName}});
        {{/isClass}}
        {{#getType.getPrimaryKeyAttribute}}else if({{../getName}} > 0  || (typeof {{../getName}} == 'string' && {{../getName}} != '')) this.{{../getName}} = {{../getType.getName}}.create({ {{getName}}: {{../getName}}, partialLoaded: true});{{/getType.getPrimaryKeyAttribute}}
        {{/isCardinalityMulti}}{{#isCardinalityMulti}}
//            else if({{getName}} instanceof {{getType.getName}}Collection) this.{{getName}} = {{getName}};
        else if(Object.prototype.toString.call( {{getName}} ) === '[object Array]'){
            this.{{getName}} = [];
            for(var index in {{getName}}){
                var element = {{getName}}[index];
                if(typeof element == 'object' && element instanceof {{getType.getName}}) this.{{getName}}.push(element);
                else if(typeof element == 'object') this.{{getName}}.push({{getType.getName}}.create(element));
                {{#getType.getPrimaryKeyAttribute}}else if(element > 0 || (typeof element == 'string' && element != '')) this.{{../getName}}.push({{../getType.getName}}.create({ {{getName}}: element, partialLoaded: true}));{{/getType.getPrimaryKeyAttribute}}
//                    {{#getType.hasPrimaryKeyAttribute}}else if(element > 0) this.{{getName}}.push({{getType.getName}}.create({ {{getType.getPrimaryKeyAttribute.getName}}: element, partialLoaded: true}));{{/getType.hasPrimaryKeyAttribute}}
                else{
                    this.{{getName}} = null;
                    console.log("TYPEERROR: ",{{getName}});
                }
            }
        }

        {{/isCardinalityMulti}}
        else{
            this.{{getName}} = null;
            console.log("TYPEERROR: ",{{getName}});
        }
        {{/isDefaultType}}
        {{#isDefaultType}}
        {{#getType.usesTypeParser}}
        if({{getName}} != null){
            if(typeof {{getName}} == 'object' && {{getName}} instanceof {{getType.getName}}){
                this.{{getName}} = {{getName}};
            }
            else if(fromUser && {{getType.getName}}.parseFromUser){
                this.{{getName}} = {{getType.getName}}.parseFromUser({{getName}});
            }else{
                this.{{getName}} = {{getType.getName}}.parse({{getName}});
            }
        }else{
            this.{{getName}} = {{getType.getName}}.null();
        }
        {{/getType.usesTypeParser}}{{^getType.usesTypeParser}}
        this.{{getName}} = {{#isBoolean}}!{{getName}} || {{getName}} == "0" || {{getName}} == "false" ? false : true{{/isBoolean}}{{^isBoolean}}{{getName}}{{/isBoolean}};
        {{/getType.usesTypeParser}}
        {{/isDefaultType}}

        return this;
    };

    {{/getAttributes}}

    //AssociationEnds
    {{#getAssociationEnds}}
    {{^isCardinalityZero}}
    {{../getName}}.prototype.{{getName}};


     {{../getName}}.prototype.get{{getNameUCFirst}} = function(){
        {{#isCardinalityMulti}}
        if(this.{{getName}}){
            for(var index in this.{{getName}}){
                var val = this.{{getName}}[index];
                if(val.class && val.id) this.{{getName}}[index] = jsModel[val.class+'.'+val.id];
            }
        }
        {{/isCardinalityMulti}}
        {{^isCardinalityMulti}}
        if(this.{{getName}} && this.{{getName}}.class && this.{{getName}}.id) this.{{getName}} = jsModel[this.{{getName}}.class+'.'+this.{{getName}}.id];
        {{/isCardinalityMulti}}
        return this.{{getName}};
    };

    {{../getName}}.prototype.set{{getNameUCFirst}} = function({{getName}}){
        {{^isDefaultType}}
        if({{getName}} == null) this.{{getName}} = null;
        else if({{getName}} == '[null]') this.{{getName}} = null;

        {{^isCardinalityMulti}}
        else if(typeof {{getName}} == 'object' && {{getName}} instanceof {{getType.getName}}) this.{{getName}} = {{getName}};
        else if(typeof {{getName}} == 'object') this.{{getName}} = new {{getType.getName}}({{getName}});
//            else if({{getName}} > 0) this.{{getName}} = {{getType.getName}}.create({ {{getType.getPrimaryKeyAttribute.getName}}: {{getName}}, partialLoaded: true});
        {{#getType.getPrimaryKeyAttribute}}else if({{../getName}} > 0  || (typeof {{../getName}} == 'string' && {{../getName}} != '')) this.{{../getName}} = {{../getType.getName}}.create({ {{getName}}: {{../getName}}, partialLoaded: true});{{/getType.getPrimaryKeyAttribute}}
        {{/isCardinalityMulti}}{{#isCardinalityMulti}}
//            else if({{getName}} instanceof {{getType.getName}}Collection) this.{{getName}} = {{getName}};
        else if(Object.prototype.toString.call( {{getName}} ) === '[object Array]'){
            this.{{getName}} = [];
            for(var index in {{getName}}){
                var element = {{getName}}[index];
                if(typeof element == 'object') this.{{getName}}.push({{getType.getName}}.create(element));
                else if(typeof element == 'object' && element instanceof {{getType.getName}}) this.{{getName}}.push(element);
                {{#getType.getPrimaryKeyAttribute}}else if(element > 0 || (typeof element == 'string' && element != '')) this.{{../getName}}.push({{../getType.getName}}.create({ {{getName}}: element, partialLoaded: true}));{{/getType.getPrimaryKeyAttribute}}
//                    {{#getType.hasPrimaryKeyAttribute}}else if(element > 0) this.{{getName}}.push({{getType.getName}}.create({ {{getType.getPrimaryKeyAttribute.getName}}: element, partialLoaded: true}));{{/getType.hasPrimaryKeyAttribute}}
                else{
                    this.{{getName}} = null;
                    console.log("TYPEERROR: ",{{getName}});
                }
            }
        }

        {{/isCardinalityMulti}}
        else{
            this.{{getName}} = null;
            console.log("TYPEERROR: ",{{getName}});
        }
        {{/isDefaultType}}
        {{#isDefaultType}}this.{{getName}} = {{getName}};{{/isDefaultType}}
        {{^getOppositeAssociationEnd.isCardinalityZero}}

        {{#getOppositeAssociationEnd.isCardinalitySingle}}
        {{#isCardinalityMulti}}
        for(var index in {{getNameLCFirst}}){
            if(typeof {{getNameLCFirst}}[index] == 'object' && {{getNameLCFirst}}[index] instanceof {{getType.getName}} && {{getNameLCFirst}}[index].get{{getOppositeAssociationEnd.getNameUCFirst}}().getId() != this.getId()) {{getNameLCFirst}}[index].set{{getOppositeAssociationEnd.getNameUCFirst}}(this);
        }
        {{/isCardinalityMulti}}
        {{^isCardinalityMulti}}
        if(typeof {{getNameLCFirst}} == 'object' && {{getNameLCFirst}} instanceof {{getType.getName}} && {{getNameLCFirst}}.get{{getOppositeAssociationEnd.getNameUCFirst}}() && {{getNameLCFirst}}.get{{getOppositeAssociationEnd.getNameUCFirst}}().getId() != this.getId()) {{getNameLCFirst}}.set{{getOppositeAssociationEnd.getNameUCFirst}}(this);
        {{/isCardinalityMulti}}
        {{/getOppositeAssociationEnd.isCardinalitySingle}}
        {{/getOppositeAssociationEnd.isCardinalityZero}}

        return this;
    };


    {{#isCardinalityMulti}}
    {{../../getName}}.prototype.add{{getNameUCFirst}} = function({{getName}}){

        if(!this.{{getName}}) this.{{getName}} = [];
        this.{{getName}}.push({{getName}});
        return this;
    };
    {{/isCardinalityMulti}}

    {{#isAssociationClass}}
    //AssocClass
    {{../../getName}}.prototype.{{getType.getNameLCFirst}};

    {{../../getName}}.prototype.get{{getType.getName}} = function(){
        if(this.{{getType.getNameLCFirst}} && this.{{getType.getNameLCFirst}}.class && this.{{getType.getNameLCFirst}}.id) this.{{getType.getNameLCFirst}} = jsModel[this.{{getType.getNameLCFirst}}.class+'.'+this.{{getType.getNameLCFirst}}.id];
        return this.{{getType.getNameLCFirst}};
    };

    {{../../getName}}.prototype.set{{getType.getName}} = function({{getType.getNameLCFirst}}){

        if({{getType.getNameLCFirst}} == null) this.{{getType.getNameLCFirst}} = null;

        else if(typeof {{getType.getNameLCFirst}} == 'object' && {{getType.getNameLCFirst}} instanceof {{getType.getName}}) this.{{getType.getNameLCFirst}} = {{getType.getNameLCFirst}};
        else if(typeof {{getType.getNameLCFirst}} == 'object') this.{{getType.getNameLCFirst}} = new {{getType.getName}}({{getType.getNameLCFirst}});
        {{#getType.getPrimaryKeyAttribute}}else if({{../getType.getNameLCFirst}} > 0) this.{{../getType.getNameLCFirst}} = {{../getType.getName}}.create({ {{getName}}: {{../getType.getNameLCFirst}}, partialLoaded: true});{{/getType.getPrimaryKeyAttribute}}
        else{
            this.{{getType.getNameLCFirst}} = null;
            console.log("ERROR: ",{{getType.getNameLCFirst}});
        }

        //{{^getOppositeAssociationEnd.isCardinalityZero}}if({{getType.getNameLCFirst}}.get{{getOppositeAssociationEnd.getNameUCFirst}}() != this.getId()) {{getType.getNameLCFirst}}.set{{getOppositeAssociationEnd.getNameUCFirst}}(this);{{/getOppositeAssociationEnd.isCardinalityZero}}

        return this;
    };


    {{/isAssociationClass}}

    {{#getParent.isAssociationClass}}
    {{^isAssociationClass}}

    //end belongs to AssocClass
    {{../../getName}}.prototype.{{getParent.getNameLCFirst}};

    {{../../getName}}.prototype.set{{getParent.getName}} = function({{getParent.getNameLCFirst}}){
        if({{getParent.getNameLCFirst}} == null) this.{{getParent.getNameLCFirst}} = null;
        {{^isCardinalitySingle}}
        else if(typeof {{getParent.getNameLCFirst}} == 'object' && {{getParent.getNameLCFirst}} instanceof Object){
            this.{{getParent.getNameLCFirst}} = [];
            for(var index in {{getParent.getNameLCFirst}}){
                var val = {{getParent.getNameLCFirst}}[index];
                if(!(typeof val == 'object' && val instanceof {{getParent.getName}})) val = new {{getParent.getName}}(val);
                this.{{getParent.getNameLCFirst}}.push(val);
            }
        }
        {{/isCardinalitySingle}}
        {{#isCardinalitySingle}}
        else if(typeof {{getParent.getNameLCFirst}} == 'object' &&  {{getParent.getNameLCFirst}} instanceof {{getParent.getName}}) this.{{getParent.getNameLCFirst}} = {{getParent.getNameLCFirst}};
        else if(typeof {{getParent.getNameLCFirst}} == 'object') this.{{getParent.getNameLCFirst}} = new {{getParent.getName}}({{getParent.getNameLCFirst}});
        {{/isCardinalitySingle}}
        else{
            this.{{getParent.getNameLCFirst}} = null;
            console.log("ERROR: ",{{getParent.getNameLCFirst}});
        }

        {{#isCardinalityMulti}}
        for(var index in {{getParent.getNameLCFirst}}){
            if(typeof {{getParent.getNameLCFirst}}[index]== 'object' && {{getParent.getNameLCFirst}}[index] instanceof {{getParent.getName}} && (!{{getParent.getNameLCFirst}}[index].get{{../../../getName}}() || {{getParent.getNameLCFirst}}[index].get{{../../../getName}}().getId() != this.getId())) {{getParent.getNameLCFirst}}[index].set{{../../../getName}}(this);
        }
        {{/isCardinalityMulti}}
        {{^isCardinalityMulti}}
        if(typeof {{getParent.getNameLCFirst}}== 'object' &&  {{getParent.getNameLCFirst}} instanceof {{getParent.getName}} && (!{{getParent.getNameLCFirst}}.get{{../../getName}}() || {{getParent.getNameLCFirst}}.get{{../../getName}}().getId() != this.getId())) {{getParent.getNameLCFirst}}.set{{../../getName}}(this);
        {{/isCardinalityMulti}}

        return this;
    };

    {{../../getName}}.prototype.get{{getParent.getName}} = function(){
        {{#isCardinalityMulti}}
        if(this.{{getParent.getNameLCFirst}}){
            for(var index in this.{{getParent.getNameLCFirst}}){
                var val = this.{{getParent.getNameLCFirst}}[index];
                if(val.class && val.id) this.{{getParent.getNameLCFirst}}[index] = jsModel[val.class+'.'+val.id];
            }
        }
        {{/isCardinalityMulti}}
        {{^isCardinalityMulti}}
        if(this.{{getParent.getNameLCFirst}} && this.{{getParent.getNameLCFirst}}.class && this.{{getParent.getNameLCFirst}}.id) this.{{getParent.getNameLCFirst}} = jsModel[this.{{getParent.getNameLCFirst}}.class+'.'+this.{{getParent.getNameLCFirst}}.id];
        {{/isCardinalityMulti}}
        return this.{{getParent.getNameLCFirst}};
    };

    {{/isAssociationClass}}
    {{/getParent.isAssociationClass}}
    {{/isCardinalityZero}}
    {{/getAssociationEnds}}

    //PrimaryAttribute
    {{^getPrimaryKeyAttribute}}
     {{#isAssociationClass}}

    {{../getName}}.prototype.getId = function(){
        var ids = [];
        {{#getAssociationEnds}}
        ids.push(this.{{getType.getNameLCFirst}});
        {{/getAssociationEnds}}
        return ids;
    };
    {{../getName}}.prototype.setId =  function(ids){
        if(typeof ids == "string") ids = ids.split('_');
        {{#getAssociationEnds}}
        this.{{getType.getNameLCFirst}} = ids.shift();
        {{/getAssociationEnds}}
        return this;
    };
     {{/isAssociationClass}}
    {{/getPrimaryKeyAttribute}}

    {{#getPrimaryKeyAttribute}}
    {{../getName}}.prototype.getId = function(){
        return this.{{getName}};
    };
    {{../getName}}.prototype.setId =  function(id){
        this.{{getName}} = id;
        return this;
    };

    {{../getName}}.getPrimaryKeyName = function(){
        return "{{getName}}";
    };
    {{/getPrimaryKeyAttribute}}

    {{getName}}.hasPrimaryKey = function(){
        return {{#getPrimaryKeyAttribute}}true{{/getPrimaryKeyAttribute}}{{^getPrimaryKeyAttribute}}false{{/getPrimaryKeyAttribute}};
    };

    
    {{getName}}.prototype.toObjForTransfer = function(recursive, recursiveQueue){
        var obj = this.toObj(recursive, recursiveQueue);
        
        //change boolean values, so they are not interpretet as string
        for(var index in obj){
            var val = obj[index];
            if(val === true || val === false){
                obj[index] = val ? 1 : 0;
            }
        }
        
        return obj;
    };
    
    {{getName}}.prototype.convertetToObj = false;
    {{getName}}.prototype.toObj = function(recursive, recursiveQueue){
        recursive = recursive || false;
//            first = first || false;
        if(recursive && !recursiveQueue){
            console.error("toObj needs a recursiveQueeue if called recursive");
        }
        if(recursive){
            if(this.convertetToObj == recursiveQueue){
                return this.getId();
            }
            this.convertetToObj = recursiveQueue;
        }
        //TODO: Cardinality Multi + Class ids
        var obj = {};
        {{#getSpecializes}}obj = _parent.toObj.call(this);{{/getSpecializes}}
                
        {{^getSpecializes}}
        if(this.created !== null && typeof this.created != 'undefined'){
            obj.created = (typeof this.created == 'object') ? this.created.val() : this.created;
        }
        if(this.lastModifiedTime !== null && typeof this.lastModifiedTime != 'undefined'){
            obj.lastModifiedTime = (typeof this.lastModifiedTime == 'object') ? this.lastModifiedTime.val() : this.lastModifiedTime;
        }
        if(this.createdBy !== null && typeof this.createdBy != 'undefined') obj.createdBy = this.createdBy;
        {{/getSpecializes}}
        obj.class = '{{getName}}';
        obj.nsClass = '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}';

        //From Attributes
        {{#getAttributes}}
        {{#isDefaultType}}   
        {{#getType.usesTypeParser}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = this.get{{getNameUCFirst}}().val();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';        
        {{/getType.usesTypeParser}}{{^getType.usesTypeParser}} 
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = this.get{{getNameUCFirst}}();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/getType.usesTypeParser}} 
        {{/isDefaultType}}    
        {{^isDefaultType}}          
        {{#isClass}}
        {{#getType.hasPrimaryKeyAttribute}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = (recursive)?this.get{{getNameUCFirst}}().toObj(true,recursiveQueue):this.get{{getNameUCFirst}}().getId();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}{{^getType.hasPrimaryKeyAttribute}}
        if(this.{{getName}} !== null && recursive) obj.{{getName}} = this.get{{getNameUCFirst}}().toObj(true,recursiveQueue);   
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';                 
        {{/getType.hasPrimaryKeyAttribute}}
        {{/isClass}}{{^isClass}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = (typeof this.{{getName}} === 'object')?null:this.get{{getNameUCFirst}}();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/isClass}}
        {{/isDefaultType}}
        {{/getAttributes}}

        //From AssociationsEnds
        {{#getAssociationEnds}}
        {{^isCardinalityZero}}

        {{^isCardinalityMulti}}
        //{{isCardinalityMulti}}
        {{#getType.hasPrimaryKeyAttribute}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = (recursive)? this.get{{getNameUCFirst}}().toObj(true, recursiveQueue):this.get{{getNameUCFirst}}().getId();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}{{^getType.hasPrimaryKeyAttribute}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined' && recursive) obj.{{getName}} = this.get{{getNameUCFirst}}().toObj(true, recursiveQueue);
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}

        {{^isAssociationClass}}
        {{#getParent.isAssociationClass}}
        //From AssocClassConnections
        {{#getType.hasPrimaryKeyAttribute}}
        if(this.{{getNameLCFirst}} !== null && typeof this.{{getNameLCFirst}} != 'undefined') obj.{{getNameLCFirst}} = (recursive)?this.get{{getNameUCFirst}}().toObj(true, recursiveQueue):this.get{{getNameUCFirst}}().getId();
//        else if(this.{{getNameLCFirst}} == null) obj.{{getNameLCFirst}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}{{^getType.hasPrimaryKeyAttribute}}
        if(this.{{getNameLCFirst}} !== null && typeof this.{{getNameLCFirst}} != 'undefined' && recursive) obj.{{getNameLCFirst}} = this.get{{getNameUCFirst}}().toObj(true, recursiveQueue);
//        else if(this.{{getNameLCFirst}} == null) obj.{{getNameLCFirst}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}
        {{/getParent.isAssociationClass}}
        {{/isAssociationClass}} 
        {{/isCardinalityMulti}}

        {{#isAssociationClass}}
        //From AssocClass
        {{#getType.hasPrimaryKeyAttribute}}
        if(this.{{getConnectionClass.getNameLCFirst}} !== null && typeof this.{{getConnectionClass.getNameLCFirst}} != 'undefined') obj.{{getConnectionClass.getNameLCFirst}} = (recursive)?this.get{{getConnectionClass.getNameUCFirst}}().toObj(true, recursiveQueue):this.{{getConnectionClass.getNameLCFirst}}.getId();
//        else if(this.{{getConnectionClass.getNameLCFirst}} == null) obj.{{getConnectionClass.getNameLCFirst}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}{{^getType.hasPrimaryKeyAttribute}}
        if(this.{{getConnectionClass.getNameLCFirst}} !== null && typeof this.{{getConnectionClass.getNameLCFirst}} != 'undefined' && recursive) obj.{{getConnectionClass.getNameLCFirst}} = this.get{{getConnectionClass.getNameUCFirst}}().toObj(true, recursiveQueue);
//        else if(this.{{getConnectionClass.getNameLCFirst}} == null) obj.{{getConnectionClass.getNameLCFirst}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}
        {{/isAssociationClass}}
        {{/isCardinalityZero}}
        {{/getAssociationEnds}}

        return obj;
    };

    {{getName}}.prototype.updateFromObj = function(obj, fromUser){
        fromUser = fromUser || false;
        {{#getSpecializes}}
        _parent.updateFromObj.call(this, obj);
        {{/getSpecializes}}
        {{^getSpecializes}}
        if(obj['created'] != null) this.created = obj['created'];
        if(obj['createdBy'] != null) this.createdBy = obj['createdBy'];
        if(obj['lastModifiedTime'] != null) this.lastModifiedTime = obj['lastModifiedTime'];
        {{/getSpecializes}}
        
        {{#getSpecializes}}
        {{/getSpecializes}}
            //From Attributes
            {{#getAttributes}}
            if(obj.{{getName}} != null) this.set{{getNameUCFirst}}(obj.{{getName}},fromUser);
            {{/getAttributes}}

            //From AssociationsEnds
            {{#getAssociationEnds}}
            {{^isCardinalityZero}}
            if(obj.{{getName}} != null) this.set{{getNameUCFirst}}(obj.{{getName}},fromUser);

            {{^isAssociationClass}}
            {{#getParent}}{{#isAssociationClass}}
            //From AssocClassConnection
            if(obj.{{getNameLCFirst}} != null) this.set{{getNameUCFirst}}(obj.{{getNameLCFirst}},fromUser);
            {{/isAssociationClass}}{{/getParent}}
            {{/isAssociationClass}}

            {{#isAssociationClass}}
            //From AssocClass
            if(obj.{{getConnectionClass.getNameLCFirst}} != null) this.set{{getConnectionClass.getName}}(obj.{{getConnectionClass.getNameLCFirst}},fromUser);
            {{/isAssociationClass}}
            {{/isCardinalityZero}}        
            {{/getAssociationEnds}}
        {{#getSpecializes}}
        {{/getSpecializes}}
        return this;
    };

    {{getName}}.prototype.getPostOptions = function(){
        return {
            'class': '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}',
            'action': '{{getName}}',
            'objParam': {{^isStatic}}this.toObjForTransfer(){{/isStatic}}{{#isStatic}}'static'{{/isStatic}},
            'param': {}
        };
    };
    
    //Userdefined Operation default php mapping
    {{#getOperations}}
    
    /**
     * 
    {{#getParameters}}
      * @param {{{{getType.getName}}}} {{getName}}
    {{/getParameters}}
      * @return {{{{getReturnParameter.getName}}}}
     */
    {{getParent.getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getName}}PHP = function({{#getParameters}}{{^first}}, {{/first}}{{getName}}{{/getParameters}}){
        {{#getParameters}}{{#getDefault}}{{getName}} = typeof {{getName}} === 'undefined' ? {{&.}} : {{getName}};{{/getDefault}}{{/getParameters}}
        var dfd = $.Deferred();
        $.post('ajax.php',{
            'class': '{{#pathToNamespace 2}}{{getParent.getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getParent.getName}}',
            'action': '{{getName}}',
            'objParam': {{^isStatic}}this.toObjForTransfer(){{/isStatic}}{{#isStatic}}'static'{{/isStatic}},
            'param': {
                {{#getParameters}}
                '{{getName}}': JsModel.transferParameter({{getName}}),
//                '{{getName}}': ({{getName}} != null && typeof {{getName}} == 'object' && Array !== {{getName}}.constructor) ? {{getName}}.toObjForTransfer() : {{getName}},
                {{/getParameters}}
            }
        },function(r){
                if(r.error) alert(r.error);
                dfd.resolve(r);
                //return handle (sync if return != void)
        }{{#getReturnParameter.isClass}},'json'{{/getReturnParameter.isClass}}).fail(function(r){
            dfd.reject(r);
        });
        return dfd.promise();
    };

    {{/getOperations}}

    //generic getter/setter
    {{getName}}.prototype.get = function(selector){
        var dotIndex = selector.indexOf('.');
        if(dotIndex > 0){
            return this.get(selector.substr(0,dotIndex)).get(selector.substr(dotIndex+1));
        }
        var first = selector.charAt(0).toUpperCase();
        selector = first + selector.substr(1);
        return this['get'+selector]();
    };
    
    {{getName}}.prototype.set = function(selector, value, fromUser){
        fromUser = fromUser || false;
        var dotIndex = selector.indexOf('.');
        if(dotIndex > 0){
            var newScope = this.get(selector.substr(0,dotIndex));
            if(!newScope){
                console.log(this);
                throw ("Element {{getName}} has no attribute "+selector.substr(0,dotIndex)+" please declare it before");
            }
            return newScope.set(selector.substr(dotIndex+1), value);
        }
        var first = selector.charAt(0).toUpperCase();
        selector = first + selector.substr(1);

        if(typeof this['set'+selector] == 'undefined'){
            throw ("Element {{getName}} has no setter for "+selector);
        }

        return this['set'+selector](value, fromUser);
    };

    //CRUD
    {{#isTable}}    
    {{getName}}.prototype.save = function(cb){
        var self = this;
        var dfd = $.Deferred();

        $.post('ajax.php',{
            'class': '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}',
            'action': 'save',
            'objParam': this.toObjForTransfer() /*{
                '{{getPrimaryKeyAttribute.getName}}': id,
            }*/,
            'param': {}
        },function(r){
                if(r.error) alert(r.error);

                {{#getPrimaryKeyAttribute}}
                if(!self.{{getName}}){
                    self.{{getName}} = r.{{getName}};
                }
                {{/getPrimaryKeyAttribute}}

                dfd.resolve(r);
                if(cb) cb.call(self,r);
                self.trigger("save");
                //return handle (sync if return != void)
        },'json').fail(function(r){
            dfd.reject(r);
        });
        return dfd.promise();
    };
    
    {{getName}}.prototype.treeSave = function(){
        var self = this;
        var dfd = $.Deferred();
        var recursiveQueue = Math.random();
        $.post('ajax.php',{
            'class': '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}',
            'action': 'treeSave',
            'objParam': this.toObjForTransfer(true, recursiveQueue) /*{
                '{{getPrimaryKeyAttribute.getName}}': id,
            }*/,
            'param': {}
        },function(r){
                if(r.error) alert(r.error);


                {{#getPrimaryKeyAttribute}}
                if(!self.{{getName}}){
                    self.{{getName}} = r.{{getName}};
                }
                {{/getPrimaryKeyAttribute}}

                dfd.resolve(r);

                self.trigger("save");
                //return handle (sync if return != void)
        },'json').fail(function(r){
            dfd.reject(r);
        });
        return dfd.promise();
    };
    
    
    {{getName}}.prototype.setNull = function(name){
        var dfd = $.Deferred();
        $.post('ajax.php',{
            'class': '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}',
            'action': 'setNull',
            'objParam': {
                '{{getPrimaryKeyAttribute.getName}}': this.getId()
            },
            'param': {
                'fields': name 
            }
        },function(r){
                if(r.error) alert(r.error);
                dfd.resolve(r);
        },'json').fail(function(r){
            dfd.reject(r);
        });
        return dfd;
    };

    {{getName}}.load = function(id){
        if(jsModel['{{getName}}.'+id]) return jsModel['{{getName}}.'+id];
        else{
            var ret = $.ajax({
                'type': "POST",
                'url': 'ajax.php',
                'async': false,
                'data': {
                    'class': '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}',
                    'action': 'load',
                    'param': {
                        id: id
                    }              
                }
            }).responseText;
            console.log(ret);
            return ret;
        }
    };

    {{getName}}.prototype.delete = function(){
        var self = this;
        var dfd = $.Deferred();
        $.post('ajax.php',{
            'class': '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}',
            'action': 'delete',
            'objParam': {
                {{#hasPrimaryKeyAttribute}}
                {{#getPrimaryKeyAttribute}}
                {{getName}}: self.get{{getNameUCFirst}}()
                {{/getPrimaryKeyAttribute}}
                {{/hasPrimaryKeyAttribute}}
                {{^hasPrimaryKeyAttribute}}
                {{#isAssociationClass}}
                {{#getAssociationEnds}}
                {{getType.getNameLCFirst}}: self.get{{getType.getNameUCFirst}}().getId(),
                {{/getAssociationEnds}}
                {{/isAssociationClass}}
                {{/hasPrimaryKeyAttribute}}
            },
            'param': {}
        },function(r){
                if(r.error) alert(r.error);

                dfd.resolve(r);
                self.trigger("delete");
        }).fail(function(r){
            dfd.reject(r);
        });;
        
        return dfd;
    };
    
    {{#hasActive}}
    {{getName}}.prototype.softDelete = function(){
        var self = this;
        var dfd = $.Deferred();
        $.post('ajax.php',{
            'class': '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}',
            'action': 'softDelete',
            'objParam': {
                {{#hasPrimaryKeyAttribute}}
                {{#getPrimaryKeyAttribute}}
                {{getName}}: self.get{{getNameUCFirst}}()
                {{/getPrimaryKeyAttribute}}
                {{/hasPrimaryKeyAttribute}}
                {{^hasPrimaryKeyAttribute}}
                {{#isAssociationClass}}
                {{#getAssociationEnds}}
                {{getType.getNameLCFirst}}: self.get{{getType.getNameUCFirst}}().getId(),
                {{/getAssociationEnds}}
                {{/isAssociationClass}}
                {{/hasPrimaryKeyAttribute}}
            },
            'param': {}
        },function(r){
                if(r.error) alert(r.error);
                
                dfd.resolve(r);
                
                self.trigger("delete");
                self.trigger("softDelete");
        },'json').fail(function(r){
            dfd.reject(r);
        });
        
        return dfd;
    };
    {{/hasActive}}
    {{/isTable}}

    //Views and Templates

    {{getName}}.prototype.getHtmlPHP = function(templateName, cb){
        var self = this;
        templateName = templateName || 'default';
        var dfd = $.Deferred();

        $.post('ajax.php',{
            'class': '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}',
            'action': 'getHtml',
            'objParam': self.toObjForTransfer(),
            'param': {
                'template': templateName
            }
        },function(r){
                if(r.error) alert(r.error);
                dfd.resolve(r);
                if(cb) cb(r);
                //return handle (sync if return != void)
        }).fail(function(r){
            dfd.reject(r);
        });
        return dfd.promise();
    };
    
    {{getName}}.prototype.getHtml = function(templateName, cb){
        var self = this;
        var dfd = $.Deferred();
        var tplSrc = {{getName}}.getTemplate(templateName);
        var template = Handlebars.compile(tplSrc);
        var html = template(self);
        if(cb){
            cb(html);
        }
        dfd.resolve(html);
        return html;
    };
    
    {{getName}}.getTemplate = function(templateName){
        if(typeof templateName !== "string") templateName = "";
        var availableTemplates = {
            '': '{{getName}}.default'
            {{#getWorkPackage.getViews}}
            ,'{{getName}}': '{{../getName}}.{{getName}}'
            {{#getWorkPackage.getChilds}}
            {{#getViews}}
            ,'{{../getName}}/{{getName}}': '/{{../getName}}/{{../getName}}.{{getName}}'
            {{/getViews}}
            {{/getWorkPackage.getChilds}}
            {{/getWorkPackage.getViews}}
        };
        if(!availableTemplates[templateName]){
            {{#getSpecializes}}
            return {{getName}}.getTemplate(templateName);
            {{/getSpecializes}}{{^getSpecializes}}
            throw Error("template '"+templateName+"' doesnt exists");
            {{/getSpecializes}}
        }
        templateName = templateName || 'default';
        
        {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        var templateSrcs = {
            {{#getWorkPackage}}
            {{#list getViews}}
            {{^first}},{{/first}}'{{../getName}}.{{value.getName}}': '{{#value}}{{#jsMultilineString}}{{#includeTemplate 1}}{{&getHtmlContent}}{{/includeTemplate 1}}{{/jsMultilineString}}{{/value}}' //{{value.getId}}
            {{/list getViews}}
            {{/getWorkPackage}}
        };
        var tplSrc = templateSrcs[availableTemplates[templateName]];
        return tplSrc;
        {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        {{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        
        var templateFile = "FCVAPs{{getWorkPackage.getPath}}/"+availableTemplates[templateName]+".html";
       
        var template = $.ajax({
            'type': "GET",
            'url': templateFile,
            'async': false
        }).responseText;
        return template;
        {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    };
    
    {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    
    
    {{getName}}.prototype.init = function(){
        {{#includeCode}}{{#indent 2}}{{#getWorkPackage}}{{#singleParse}}{{&getControlContent}}{{/singleParse}}{{/getWorkPackage}}{{/indent 2}}{{/includeCode}}
    }
    
    //userMethods
    {{#getOperations}}
    {{^isInterface}}
    /**
      *
    {{#getParameters}}
      * @param {{getType.getName}} {{getName}}
    {{/getParameters}}
      * @return {{getReturnParameter.getType.getName}}
      */
    {{getParent.getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getName}} = function({{#getParameters}}{{^first}}, {{/first}}{{getName}}{{/getParameters}}){
        {{#getParameters}}{{#getDefault}}{{getName}} = typeof {{getName}} === 'undefined' ? {{&.}} : {{getName}};{{/getDefault}}{{/getParameters}}
        {{#includeCode}}{{#indent 2}}{{#getWorkPackage}}{{#singleParse}}{{&getControlContent}}{{/singleParse}}{{/getWorkPackage}}{{/indent 2}}{{/includeCode}}
    };
    
    {{/isInterface}}
    {{/getOperations}}
    
    {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    {{#getSpecializes}}
    extend({{../getName}}, {{getName}});
    {{/getSpecializes}}
    {{/isPageOrFrame}}

    globalScope.{{getName}} = {{getName}};
    JsModel.getObj().loadedClass('{{getName}}');
    
    return {{getName}};
});