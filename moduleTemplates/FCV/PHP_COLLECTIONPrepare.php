<?php
namespace {{#pathToNamespace}}{{getWorkPackage.getPath}}{{/pathToNamespace}};

{{#uniqueLines}}
use fcv\database\QueryBuilder;
use fcv\traits\StaticEventAction;
use fcv\CollectionPrepare;
use fcv\FCVJsObj;
use fcv\classTraits\CollectionPrepare as collectionPrepareTrait;
use fcv\classTraits\CollectionPrepareActive;
//From AssocEnds
{{#getAssociationEnds}}
use {{#substr 1}}{{getConnectionClass.getNamePath}}CollectionPrepare{{/substr 1}};
{{#isAssociationClass}}
use {{#substr 1}}{{getType.getNamePath}}CollectionPrepare{{/substr 1}};
{{/isAssociationClass}}
{{#getParent.isAssociationClass}}
{{^isAssociationClass}}
use {{#substr 1}}{{getParent.getNamePath}}CollectionPrepare{{/substr 1}};
{{/isAssociationClass}}
{{/getParent.isAssociationClass}}
{{/getAssociationEnds}}

//From Attributes
{{#getAttributes}}
{{#getType.isClass}}
use {{#substr 1}}{{getType.getNamePath}}CollectionPrepare{{/substr 1}};
{{/getType.isClass}}
{{/getAttributes}}

//From Implementations
{{#getImplements}}
use {{#substr 1}}{{getNamePath}}CollectionPrepare{{/substr 1}};

{{/getImplements}}

//From Specialisations
{{#getSpecializes table}}
use {{#substr 1}}{{getNamePath}}CollectionPrepare{{/substr 1}};

{{/getSpecializes table}} 

{{/uniqueLines}}


class {{getName}}CollectionPrepare {{#getSpecializes table}} extends {{getName}}CollectionPrepare{{/getSpecializes table}}{
        
    {{#hasSpecializes}}
    use StaticEventAction;
    {{/hasSpecializes}}
    {{#getSpecializes}}
    {{/getSpecializes}}
    {{^getSpecializes}}
    use collectionPrepareTrait;
    {{/getSpecializes}}
    {{#hasActive}}
    use CollectionPrepareActive;
    {{/hasActive}}
    
    protected $mysqlTable = '{{getName}}';
    protected $tableName = '{{getName}}';
    protected $className = '{{getName}}';

    {{^getSpecializes}}
    
    /**
     * selects alle primary keys and updates the primaryKeyIndex, that is used
     * to group the data later in parse section
     * @return $this
     */
    public function selectId(){
        if(!$this->getQuery()->distinct && empty($this->primaryKeyIndex)){
            $primKeyIndex = 0;
            {{#getPrimaryKeyAttributes}}
            $this->primaryKeyIndex[] = $primKeyIndex++;
            $this->select{{getNameUCFirst}}();
            {{/getPrimaryKeyAttributes}}
        }
        return $this;
    }
    
   
    {{^hasActive}}
    /**
     * @return static
     */
    public function whereSubsActive(){
        return $this;
    }
    {{/hasActive}}

    {{/getSpecializes}}
    
    {{#getAttributes}}
    {{^isStatic}}

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function where{{getNameUCFirst}}($operation, $value = false, $logicConnector = 'and'){
        $this->where('{{getName}}', $operation, $value, $logicConnector);
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function orWhere{{getNameUCFirst}}($operation, $value = false){
        $this->orWhere('{{getName}}', $operation, $value);
        return $this;
    }

    /**
     *
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function select{{getNameUCFirst}}(){
        return $this->select('{{getName}}', {{^isCardinalitySingle}}true{{/isCardinalitySingle}}{{#isCardinalitySingle}}false{{/isCardinalitySingle}}, '{{../getName}}');
    }
    {{#isClass}}

    /**
     * @var {{getType.getName}}CollectionPrepare
     * @return {{getType.getName}}CollectionPrepare
     */
    public function  get{{getNameUCFirst}}(){
       if(!isset($this->{{getName}})){
           $colPrep = {{getType.getName}}CollectionPrepare::create();
           $colPrep->setTriggerGenericEvents($this->triggerGenericEvents);
           $this->set{{getNameUCFirst}}($colPrep);
       }
       return $this->{{getName}};
    }
    {{/isClass}}
    
    {{/isStatic}}
    {{/getAttributes}}
    {{#getAssociationEnds}}
    {{^isStatic}}
    {{^isCardinalityZero}}
    /**
     * @var {{getType.getName}}CollectionPrepare
     */
    protected ${{getNameLCFirst}};
    
    /**
     * AssociationEnd setter for all
     * @var {{getConnectionClass.getName}}CollectionPrepare
     * @return {{#isAssociationClass}}{{getParent.getName}}{{/isAssociationClass}}{{^isAssociationClass}}{{getClass.getName}}{{/isAssociationClass}}CollectionPrepare
     */
    public function {{setAssociationFunction}}({{getConnectionClass.getName}}CollectionPrepare ${{getNameLCFirst}}, $logicConnector = false){
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('set', get_class($this), '{{getNameLCFirst}}');
        }
        $this->{{getNameLCFirst}} = ${{getNameLCFirst}};
        $this->{{getNameLCFirst}}->setTableNameAlias($this->getTableNameAlias().'{{getNameUCFirst}}');
        if($logicConnector) ${{getNameLCFirst}}->setConnectionLogic($logicConnector);
        return $this;
    }

    /**
     * @var {{getConnectionClass.getName}}CollectionPrepare
     * @return {{getConnectionClass.getName}}CollectionPrepare
     */
    public function {{getAssociationFunction}}(){
       if(!isset($this->{{getNameLCFirst}})){
           $colPrep = {{getConnectionClass.getName}}CollectionPrepare::create();
           $colPrep->setTriggerGenericEvents($this->triggerGenericEvents);
           
           $this->{{setAssociationFunction}}($colPrep);
       }
       return $this->{{getNameLCFirst}};
    }
    
    
    {{#isCardinalitySingle}}
        
    
    /**
     * AssociationEnd selector for single data
     * @return {{getClass.getName}}CollectionPrepare
     */
    {{getVisibility}} function select{{getNameUCFirst}}(){
        return $this->select('{{getName}}', {{^isCardinalitySingle}}true{{/isCardinalitySingle}}{{#isCardinalitySingle}}false{{/isCardinalitySingle}}, '{{../getName}}');
    }
    
    /**
     * AssociationEnd where for single data
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function where{{getNameUCFirst}}($operation, $value = false, $logicConnector = 'and'){
        $this->where('{{getName}}', $operation, $value, $logicConnector);
        return $this;
    }

    /**
     * 
     * @param mixed operation string or nested function
     * @param string $value
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function orWhere{{getNameUCFirst}}($operation, $value = false){
        $this->orWhere('{{getName}}', $operation, $value);
        return $this;
    }
    {{/isCardinalitySingle}}
    
    
//    {{!DUPLICATE FOR Assoc Classes with multiCaridnality}}
    {{#isAssociationClass}}
        
    protected ${{getType.getNameLCFirst}};
    /**
     * setter for AssociationEnd for Associationclasses
     * @var {{getType.getName}}CollectionPrepare
     * @return {{getParent.getName}}CollectionPrepare
     */
    public function set{{getType.getNameUCFirst}}({{getType.getName}}CollectionPrepare ${{getType.getNameLCFirst}}, $logicConnector = false){
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('set', get_class($this), '{{getType.getNameLCFirst}}');
        }
        $this->{{getType.getNameLCFirst}} = ${{getType.getNameLCFirst}};
        $this->{{getType.getNameLCFirst}}->set{{getParent.getNameUCFirst}}($this);
        $this->{{getType.getNameLCFirst}}->setTableNameAlias($this->getTableNameAlias().'{{getType.getNameUCFirst}}');
        if($logicConnector) ${{getType.getNameLCFirst}}->setConnectionLogic($logicConnector);
        return $this;
    }
    
    /**
     * getter AssociationEnd for Associationclasses
     * @var {{getType.getName}}CollectionPrepare
     * @return {{getParent.getName}}CollectionPrepare
     */
    public function get{{getType.getName}}(){        
        if(!isset($this->{{getType.getNameLCFirst}})){
            $colPrep = {{getType.getName}}CollectionPrepare::create();
            $colPrep->setTriggerGenericEvents($this->triggerGenericEvents);
            $this->set{{getType.getNameUCFirst}}($colPrep);
        }
        return $this->{{getType.getNameLCFirst}};
    }
    {{/isAssociationClass}}

    {{!END DUPLICATE FOR Assoc Classes}}

    {{#getParent.isAssociationClass}}
    {{^isAssociationClass}}

    /**
     * AssociationsEnds setter for an external AssociationClass
     * @var {{getParent.getName}}CollectionPrepare
     */
    protected ${{getParent.getNameLCFirst}};
    /**
     * @var {{getConnectionClass.getName}}CollectionPrepare
     * @return {{getClass.getName}}CollectionPrepare
     */
    public function set{{getParent.getName}}({{getParent.getName}}CollectionPrepare ${{getParent.getNameLCFirst}}, $logicConnector = false){
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('set', get_class($this), '{{getParent.getNameLCFirst}}');
        }
        $this->{{getParent.getNameLCFirst}} = ${{getParent.getNameLCFirst}};
        $this->{{getParent.getNameLCFirst}}->setTableNameAlias($this->getTableNameAlias().'{{getParent.getNameUCFirst}}');
        if($logicConnector) ${{getParent.getNameLCFirst}}->setConnectionLogic($logicConnector);
        return $this;
    }
    
    /**
     * @return {{getParent.getName}}CollectionPrepare
     */
    public function get{{getParent.getName}}(){
        if(!isset($this->{{getParent.getNameLCFirst}})){
            $colPrep = {{getParent.getName}}CollectionPrepare::create();
            $colPrep->setTriggerGenericEvents($this->triggerGenericEvents);
            $this->set{{getParent.getNameUCFirst}}($colPrep);
        }
        return $this->{{getParent.getNameLCFirst}};
    }
        
    {{/isAssociationClass}}
    {{#isAssociationClass}}
        
    //is AssocClass
    
    /**
     *  AssociationsEnds selector for an this AssociationClass
     * @return {{getParent.getName}}CollectionPrepare
     */
    public function select{{getType.getName}}(){
        return $this->select('{{getType.getNameLCFirst}}', false, '{{../getName}}');
    }
    
    public function where{{getType.getName}}($operation, $value = false, $logicConnector = 'and'){
        $this->where('{{getType.getNameLCFirst}}', $operation, $value, $logicConnector);
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function orWhere{{getType.getName}}($operation, $value = false){
        $this->orWhere('{{getType.getNameLCFirst}}', $operation, $value);
        return $this;
    }   
    
    {{/isAssociationClass}}
    {{/getParent.isAssociationClass}}
    
    {{/isCardinalityZero}}
    {{/isStatic}}
    {{/getAssociationEnds}}

    {{#getAttributes}}
    {{^isStatic}}
    {{#getType.isClass}}

    /**
     * @var {{getType.getName}}CollectionPrepare
     */
    protected ${{getName}};
    /**
     *
     * @param {{getType.getName}}CollectionPrepare ${{getName}}
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function {{setterFunction}}({{getType.getName}}CollectionPrepare ${{getName}}, $logicConnector = false){
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('set', get_class($this), '{{getName}}');
        }
        $this->{{getName}} = ${{getName}};
        $this->{{getName}}->setTableNameAlias($this->getTableNameAlias().'{{getNameUCFirst}}');
        if($logicConnector) ${{getName}}->setConnectionLogic($logicConnector);
        return $this;
    }
    {{/getType.isClass}}
    {{/isStatic}}
    {{/getAttributes}}
    
    /**
     * selects all defined fields from the table
     * @return static
     */
    public function selectAll(){
        
        $this->selectId();
        {{#getSpecializes}}parent::selectAll();{{/getSpecializes}}
        
        //default
        {{^getSpecializes}}
        $this->selectCreated();
        $this->selectCreatedBy();
        $this->selectLastModified();
        {{#hasActive}}
        {{^isPrimaryKey}}
        $this->selectActive();
        {{/isPrimaryKey}}
        {{/hasActive}}
        {{/getSpecializes}}
        
        //attributes
        {{#getAttributes}}
        {{^isStatic}}
        {{^hasStereoType NoDBField}}
        {{^isPrimaryKey}}
        $this->select{{getNameUCFirst}}();
        {{/isPrimaryKey}}
        {{/hasStereoType NoDBField}}
        {{/isStatic}}
        {{/getAttributes}}
        
        //assocEnds
        {{#getAssociationEnds}}
        {{^isStatic}}
        {{^isCardinalityZero}}
        {{^getParent.isAssociationClass}}
        {{#isCardinalitySingle}}
        {{^hasStereoType NoDBField}}
        {{^isPrimaryKey}}
        $this->select{{getNameUCFirst}}();
        {{/isPrimaryKey}}
        {{/hasStereoType NoDBField}}
        {{/isCardinalitySingle}}
        {{/getParent.isAssociationClass}}
        {{#isAssociationClass}}
        {{^isPrimaryKey}}
        $this->select{{getType.getName}}(); //from assocClass
        {{/isPrimaryKey}}
        {{/isAssociationClass}}
        {{/isCardinalityZero}}
        {{/isStatic}}
        {{/getAssociationEnds}}
        return $this;
    }
            
    
    {{^getSpecializes}}
    /**
     * gets all non active results (if active if specified else its alias for get)
     * @return {{getName}}Collection
     */
    public function getRegular(){
        {{#hasActive}}
        //only select Active Elements if its not specific set otherwise
        if(!$this->activeSpecified){ //  && count($this->query->wheres) > 0
            $this->whereActive('=', 1);
        }
        {{/hasActive}}
        return $this->get();
    }
    
    
    /**
     * @return int 
     */
    public function count($columns = '*'){
        {{#hasActive}}
        //only select Active Elements if its not specific set otherwise
        if(!$this->activeSpecified){
            $this->whereActive('=', 1);
        }
        {{/hasActive}}
        
        static::trigger("get", $this);
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('get', get_class($this));
        }
        static::trigger("count", $this);
        
        $this->prepareQuery();
        
        $this->query->setFetchModeAssoc();
        $count = $this->query->count($columns);
        $this->query->setFetchModeNum();
        return $count;
    }
    {{/getSpecializes}}
        
    /**
     * delete all elements with the WHERE clausel
     * @return type
     */
    public function delete(){
        if(static::trigger("delete", $this)){
            {{#hasSpecializes}}
            $this->mysqlTable = "{{getName}}_sub";
            $this->tableName = "{{getName}}_sub";
            {{/hasSpecializes}}
            
            $this->prepareQuery({{#hasSpecializes}}false{{/hasSpecializes}});
            
            return $this->query->delete();
        }
    }
    
    /**
     * parse all return rows from sql
     */
    protected function parseDbRows($rows, $map){
        $objsArray = array();
        $objs = {{getName}}Collection::create();
        $hasPrimaryKeyIndex = count($this->primaryKeyIndex) > 0;
        //build assoc Array with all data
        foreach($rows as $row){
            
            if(!$hasPrimaryKeyIndex){
                $objsArray[] = $this->parseDbRow($row, $map, array());                
            }else{
                //generate unique identifier with primary keys
                $primaryFields = array();
                foreach($this->primaryKeyIndex as $keyIndex){
                    $primaryFields[] = $row[$keyIndex];                
                }
                $primaryField = implode(".", $primaryFields);

                //extend object if allready exists
                $initObj = (isset($objsArray[$primaryField]))?$objsArray[$primaryField]:array();
                $objsArray[$primaryField] = $this->parseDbRow($row, $map, $initObj);
            }
        }
        
        //convert array to real objects (subobjects are generated in constructors)
        foreach($objsArray as $dataArray){
            $objs->add({{getName}}::createFromArray($dataArray));
        }
        
        {{getName}}Collection::trigger('afterInit', $objs);
        
        return $objs;
    }
    
    
    
    protected $prepared = 0;
    private $preparedAttributes = [];
    
    public function isPrepared(){
        return $this->prepared == 2;
    }
    
    public function isNotPrepared(){
        return $this->prepared == 0;
    }

    /**
     * prepares the query for execution
     * @return {{getName}}Collection
     */
    public function prepareQuery({{#hasSpecializes}}$prepareParent = true{{/hasSpecializes}}){
        $this->prepareJoins({{#hasSpecializes}}$prepareParent{{/hasSpecializes}});
        $this->query->from = $this->mysqlTable.(($this->tableNameAlias == '')?'':' AS '.$this->tableNameAlias);
        QueryBuilder::appendWhereWithTable($this->query, $this->getTableNameAlias());
        $this->prepareNestedWheres();
        $this->orderQuery();
        return $this;
    }

    /**
     * gets all objects from a select query and use all where data to perform a
     * rating of the results
     * @param int $minCount
     * @return {{getName}}Collection
     */
    public function searchQueryGet($minWordCount = 0){
        {{#hasActive}}
        //only select Active Elements if its not specific set otherwise
        if(!$this->activeSpecified  && is_array($this->query->wheres) && count($this->query->wheres) > 0){
            $this->whereActive('=', 1);
        }
        {{/hasActive}}
        
        $this->prepareQuery();
        $checkBools = array();
        $checkBoolsValues = array();
        
        $flattenWheres = $this->getWheres();
        
        foreach($flattenWheres as $where){
            $checkBools[] = $where['column'].' '.$where['operator'].' ?';
            $checkBoolsValues[] = $where['value'];
        }
        $whereStatement = 'COALESCE(('.implode('), 0) + COALESCE((', $checkBools).'), 0) AS searchValue';
        $this->query->selectRaw($whereStatement, $checkBoolsValues);
        if($minWordCount > 0){
            $this->query->having('searchValue', '>=', $minWordCount);
        }
        
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('get', get_class($this));
        }
        
        $rows = $this->query->get();
        return $this->parseDbRows($rows, $this->getObjectMap());
    }

    /**
     * create joins depending on associations
     * @return {{../getName}}CollectionPrepare
     */
    public function prepareJoins({{#getSpecializes}}$prepareParentsJoins = true{{/getSpecializes}}){
        
        {{#getSpecializes}}
        if($prepareParentsJoins){
            parent::prepareJoins();
        }
        {{/getSpecializes}}
        {{#getPrimaryKeyAttributes}}
        if($this->isNotPrepared()){
            if(!$this->getQuery()->distinct && (!is_array($this->query->columns) || !in_array('{{getName}}', $this->query->columns))){
                if(is_array($this->query->columns) && !in_array('{{getName}}', $this->query->columns)){
                    $this->select{{getNameUCFirst}}();
                }
            }elseif(!$this->getQuery()->distinct && !is_array($this->query->columns)){
                $this->selectAll();
            }
        }
        {{/getPrimaryKeyAttributes}}
        
        $this->prepared = 1;
        
        {{#getAssociationEnds}}
        {{^isCardinalityZero}}
        if(isSet($this->{{getNameLCFirst}}) && !in_array('{{getNameLCFirst}}', $this->preparedAttributes) && $this->{{getNameLCFirst}}->isNotPrepared()){
            if($this->{{getNameLCFirst}}->hasSelects() && !$this->hasSelects()){
                $this->selectId();
            }
            $this->preparedAttributes[] = '{{getNameLCFirst}}';
            $realTable = $this->{{getNameLCFirst}}->getTableName();
//            $this->{{getNameLCFirst}}->setTableNameAlias($this->getTableNameAlias().'{{getNameUCFirst}}');
            $tableName = $this->{{getNameLCFirst}}->getTableNameAlias();
            {{#getParent}}{{#isAssociationClass}}
            //Assoc    {{../../../getName}}
            {{^../../isAssociationClass}}
            $this->query->join('{{getName}} as {{getName}}', '{{getName}}.{{getClass.getNameLCFirst}}', '=', $this->getTableNameAlias().'.{{../../../getPrimaryKeyAttribute.getName}}', 'LEFT');
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', '{{getName}}.{{getConnectionClass.getNameLCFirst}}', 'LEFT');
            {{/../../isAssociationClass}}
            {{#../../isAssociationClass}}
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', $this->getTableNameAlias().'.{{getConnectionClass.getNameLCFirst}}', 'LEFT');
            {{/../../isAssociationClass}}
            {{/isAssociationClass}}{{/getParent}}
            {{^getParent.isAssociationClass}}
            {{#isCardinalitySingle}}
            //Single {{../../getName}}
             $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', $this->getTableNameAlias().'.{{getName}}', 'LEFT');
            {{/isCardinalitySingle}}
            {{^isCardinalitySingle}}
            //Multi
            {{^getParent.isRelationNM}}
            //no n-m relation
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getOppositeAssociationEnd.getName}}', '=', $this->getTableNameAlias().'.{{getPrimaryKeyAttribute.getName}}', 'LEFT');
            {{/getParent.isRelationNM}}
            {{#getParent.isRelationNM}}
            //is n-m relation
            $this->query->join('{{getParent.getName}} as {{getParent.getName}}', '{{getParent.getName}}.{{getClass.getNameLCFirst}}', '=', $this->getTableNameAlias().'.{{getPrimaryKeyAttribute.getName}}', 'LEFT');
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', '{{getParent.getName}}.{{getConnectionClass.getNameLCFirst}}', 'LEFT');
            {{/getParent.isRelationNM}}
            {{/isCardinalitySingle}}
            {{/getParent.isAssociationClass}}
            $this->{{getNameLCFirst}}->prepareQuery();
//            QueryBuilder::appendWhereWithTable($this->{{getNameLCFirst}}->getQuery(),$tableName);
            $this->query->mergeQuery($this->{{getNameLCFirst}}->getQuery(), $this->{{getNameLCFirst}}->getConnectionLogic());

            if(!is_null($this->{{getNameLCFirst}}->getQuery()->columns)){
                $this->objectMap[] = array(
                    'class' => '{{getConnectionClass.getName}}',
                    'field' => '{{getNameLCFirst}}',
                    'multi' => {{#isCardinalitySingle}}false{{/isCardinalitySingle}}{{^isCardinalitySingle}}true{{/isCardinalitySingle}},
                    'primaryKeyIndex' => $this->{{getNameLCFirst}}->getPrimaryKeyIndex(),
                    'map' => $this->{{getNameLCFirst}}->getObjectMap()
                );
            }
            $this->joins['{{getNameLCFirst}}'] = $this->{{getNameLCFirst}};
        }

        {{#getParent.isAssociationClass}}
        {{^isAssociationClass}}
        if(isSet($this->{{getParent.getNameLCFirst}}) && !in_array('{{getParent.getNameLCFirst}}', $this->preparedAttributes) && $this->{{getParent.getNameLCFirst}}->isNotPrepared()){
            if($this->{{getParent.getNameLCFirst}}->hasSelects() && !$this->hasSelects()){
                $this->selectId();
            }
            $this->preparedAttributes[] = '{{getParent.getNameLCFirst}}';
            $realTableName = $this->{{getParent.getNameLCFirst}}->getTableName();
//            $this->{{getParent.getNameLCFirst}}->setTableNameAlias($this->getTableNameAlias().'{{getParent.getNameUCFirst}}');
            $tableName = $this->{{getParent.getNameLCFirst}}->getTableNameAlias();
            {{#getParent}}
            //Assoc    {{../../../getName}}
            $this->query->join($realTableName.' as '.$tableName, $tableName.'.{{../getClass.getNameLCFirst}}', '=', $this->getTableNameAlias().'.{{../../../getPrimaryKeyAttribute.getName}}', 'LEFT');
//            $this->query->join('{{getConnectionClass.getName}} as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', '{{getName}}.{{getConnectionClass.getNameLCFirst}}', 'LEFT');
            {{/getParent}}

            $this->{{getParent.getNameLCFirst}}->prepareQuery();
//            QueryBuilder::appendWhereWithTable($this->{{getParent.getNameLCFirst}}->getQuery(),$tableName);
            $this->query->mergeQuery($this->{{getParent.getNameLCFirst}}->getQuery(), $this->{{getParent.getNameLCFirst}}->getConnectionLogic());

            if(!is_null($this->{{getParent.getNameLCFirst}}->getQuery()->columns)){
                $this->objectMap[] = array(
                    'class' => '{{getParent.getName}}',
                    'field' => '{{getParent.getNameLCFirst}}',
                    'multi' => {{#isCardinalitySingle}}false{{/isCardinalitySingle}}{{^isCardinalitySingle}}true{{/isCardinalitySingle}},
                    'primaryKeyIndex' => $this->{{getParent.getNameLCFirst}}->getPrimaryKeyIndex(),
                    'map' => $this->{{getParent.getNameLCFirst}}->getObjectMap()
                );
            }
            $this->joins['{{getParent.getNameLCFirst}}'] = $this->{{getParent.getNameLCFirst}};
        }
        {{/isAssociationClass}}
        {{/getParent.isAssociationClass}}
        
        {{#isAssociationClass}}
        if(isSet($this->{{getType.getNameLCFirst}}) && !in_array('{{getType.getNameLCFirst}}', $this->preparedAttributes) && $this->{{getType.getNameLCFirst}}->isNotPrepared()){
            if($this->{{getType.getNameLCFirst}}->hasSelects() && !$this->hasSelects()){
                $this->selectId();
            }
            $this->preparedAttributes[] = '{{getType.getNameLCFirst}}';
            $realTable = $this->{{getType.getNameLCFirst}}->getTableName();
//            $this->{{getType.getNameLCFirst}}->setTableNameAlias($this->getTableNameAlias().'{{getType.getNameUCFirst}}');
            $tableName = $this->{{getType.getNameLCFirst}}->getTableNameAlias();
            {{#getParent}}{{#isAssociationClass}}
            //Assoc    {{../../../getName}}
            {{^../../isAssociationClass}}
            $this->query->join('{{getName}} as {{getName}}', '{{getName}}.{{getClass.getNameLCFirst}}', '=', $this->getTableNameAlias().'.{{../../../getPrimaryKeyAttribute.getName}}', 'LEFT');
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', '{{getName}}.{{getConnectionClass.getNameLCFirst}}', 'LEFT');
            {{/../../isAssociationClass}}
            {{#../../isAssociationClass}}
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', $this->getTableNameAlias().'.{{getConnectionClass.getNameLCFirst}}', 'LEFT');
            {{/../../isAssociationClass}}
            {{/isAssociationClass}}{{/getParent}}
            {{^getParent.isAssociationClass}}
            
            //Single {{../../getName}}
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', $this->getTableNameAlias().'.{{getName}}', 'LEFT');
            
            {{/getParent.isAssociationClass}}
            $this->{{getType.getNameLCFirst}}->prepareQuery();
//            QueryBuilder::appendWhereWithTable($this->{{getType.getNameLCFirst}}->getQuery(),$tableName);
            $this->query->mergeQuery($this->{{getType.getNameLCFirst}}->getQuery(), $this->{{getType.getNameLCFirst}}->getConnectionLogic());

            if(!is_null($this->{{getType.getNameLCFirst}}->getQuery()->columns)){
                $this->objectMap[] = array(
                    'class' => '{{getConnectionClass.getName}}',
                    'field' => '{{getType.getNameLCFirst}}',
                    'multi' => false,
                    'primaryKeyIndex' => $this->{{getType.getNameLCFirst}}->getPrimaryKeyIndex(),
                    'map' => $this->{{getType.getNameLCFirst}}->getObjectMap()
                );
            }
            $this->joins['{{getType.getNameLCFirst}}'] = $this->{{getType.getNameLCFirst}};
        }
        {{/isAssociationClass}}

        {{/isCardinalityZero}}
        {{/getAssociationEnds}}

        {{#getAttributes}}
        {{#getType.isClass}}
        if(isSet($this->{{getNameLCFirst}}) && !in_array('{{getType.getNameLCFirst}}', $this->preparedAttributes) && $this->{{getNameLCFirst}}->isNotPrepared()){
            if($this->{{getNameLCFirst}}->hasSelects() && !$this->hasSelects()){
                $this->selectId();
            }
            $this->preparedAttributes[] = '{{getType.getNameLCFirst}}';
//            $this->{{getNameLCFirst}}->setTableNameAlias($this->getTableNameAlias().'{{getNameUCFirst}}');
            $tableName = $this->{{getName}}->getTableNameAlias();
            {{#isCardinalitySingle}}
            //Single {{../../../getName}}
            $this->query->join('{{getConnectionClass.getName}} AS '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', $this->getTableNameAlias().'.{{getName}}', 'LEFT');
            {{/isCardinalitySingle}}
            {{^isCardinalitySingle}}
            //Multi
            $this->query->join('{{getConnectionClass.getName}} AS '.$tableName, $tableName.'.{{getOppositeAssociationEnd.getName}}', '=', $this->getTableNameAlias().'.{{getPrimaryKeyAttribute.getName}}', 'LEFT');
            {{/isCardinalitySingle}}

            $this->{{getName}}->prepareQuery();
//            QueryBuilder::appendWhereWithTable($this->{{getName}}->getQuery(),$tableName);
            $this->query->mergeQuery($this->{{getName}}->getQuery(), $this->{{getName}}->getConnectionLogic());

            if(!is_null($this->{{getNameLCFirst}}->getQuery()->columns)){
                $this->objectMap[] = array(
                    'class' => '{{getConnectionClass.getName}}',
                    'field' => '{{getNameLCFirst}}',
                    'multi' => {{#isCardinalitySingle}}false{{/isCardinalitySingle}}{{^isCardinalitySingle}}true{{/isCardinalitySingle}},
                    'primaryKeyIndex' => $this->{{getNameLCFirst}}->getPrimaryKeyIndex(),
                    'map' => $this->{{getNameLCFirst}}->getObjectMap()
                );
            }
            $this->joins['{{getNameLCFirst}}'] = $this->{{getNameLCFirst}};
        }
        {{/getType.isClass}}
        {{/getAttributes}}
        $this->prepared = 2;        
        return $this;
    }
    
    public function latest(){
        $this->limit(1);
        {{#getPrimaryKeyAttributes}}
        $this->orderBy('{{getName}} DESC');
        {{/getPrimaryKeyAttributes}}
        $this->prepareQuery();
        return $this->get()->first();
    }

    public function toJsObject($returnArray = false, $hash = true, $addObjectAndId = true, $subTailor = false){
        $wheres = $this->getWheres();
        $select = array();
        $joins = array();
        
        FCVJsObj::addClass(static::class);
        
        foreach($this->objectMap as $map){
            $selectStr = !empty($map['class']) ? $map['class'].'.' : '';
            $selectStr .= $map['field'];
            $select[] = $selectStr;            
        }
        foreach($this->joins as $index => $j){
            $joins[$index] = $j->toJsObject();
        }
        
        
        return array(
            'class' => '{{getName}}CollectionPrepare',
            'id' => $this->getId(),
//            'class' => '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}CollectionPrepare',
            'data' => array(
                'columns' => $this->columns,
                'wheres' => $this->wheres,
                'order' => $this->orderBy,
                'limit' => $this->limitStr,
                'joins' => $joins,
//                'type' => '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}CollectionPrepare'
            )
        );
    }

    {{^getSpecializes}}
    /**
     * @return static
     */
    public function whereId($operation, $value = false, $logicConnector = 'and'){
        {{#getPrimaryKeyAttributes}}
        $this->where{{getNameUCFirst}}($operation, $value, $logicConnector);
        {{/getPrimaryKeyAttributes}}
        return $this;
    }
    {{/getSpecializes}}
    
    function getPrimaryKeyNames(){
        return {{getName}}::$fcvPrimaryKeyAttributes;
    }
}
