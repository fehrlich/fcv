//define(['require'{{#getRequired}},'js/classes/{{req.getWorkPackage.getPath}}.js'{{#multi}},'js/classes/{{req.getWorkPackage.getPath}}Collection.js'{{/multi}}{{/getRequired}}],
//function (require{{#getRequired}},{{req.getName}}{{#multi}},{{req.getName}}Collection{{/multi}}{{/getRequired}}) {
var {{getName}}Collection = function(obj,calledByChild){
    calledByChild = calledByChild || false;
    if(!obj) return;
    {{#getSpecializes}}{{getName}}Collection.call(this, obj,true);{{/getSpecializes}}
    
    var self = this;    
}

{{getName}}.prototype.init = function(){};

{{#getOperations}}
{{getParent.getName}}.prototype.{{getName}} = function({{#getParameters}}{{^first}}, {{/first}}{{getName}}{{/getParameters}}){
    var self = this;
    $.post('ajax.php',{
        'class': '{{getParent.getName}}',
        'action': '{{getName}}',
        'objParam': self.toObj(),
        'param': {
            {{#getParameters}}
            '{{getName}}': {{getName}}.toObj() || {{getName}},
            {{/getParameters}}
        }
    },function(r){
            if(r.error) alert(r.error);
            //return handle (sync if return != void)
    },'json');
}
{{/getOperations}}

{{#getAssociationEnds}}
{{../getName}}.prototype.get{{getNameUCFirst}} = function(){
    if(this.{{getName}} && this.{{getName}}.class && this.{{getName}}.id) this.{{getName}} = jsModel[this.{{getName}}.class+'.'+this.{{getName}}.id];
    return this.{{getName}};
}
{{/getAssociationEnds}}


{{getName}}.create = function(){
    return new {{getName}}();
};
    
{{getName}}.prototype.save = function(){
    var self = this;
    $.post('ajax.php',{
        'class': '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}',
        'action': 'save',
        'objParam': {
            '{{getPrimaryKeyAttribute.getName}}': id,
        },
        'param': {}
    },function(r){
            if(r.error) alert(r.error);
            //return handle (sync if return != void)
    },'json');
}
{{getName}}.load = function(){
    var self = this;
    if(jsModel['{{getName}}.'+id]) return jsModel['{{getName}}.'+id];
    else{
        var self = this;
        var ret = $.ajax({
            type: "POST",
            url: remote_url,
            async: false,
            data: {
                'class': '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}',
                'action': 'load',
                'objParam': self.toObj(),
                'param': {}                
            }
        }).responseText;
        console.log(ret);
        return ret;
    }
}
{{getName}}.prototype.getHtml = function(templateName, cb){
    var self = this;
    templateName = templateName || 'default';
    
    $.post('ajax.php',{
        'class': '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}',
        'action': 'getHtml',
        'objParam': self.toObj(),
        'param': {
            'template': templateName
        }
    },function(r){
            if(r.error) alert(r.error);
            cb(r);
            //return handle (sync if return != void)
    });
};

{{#getSpecializes}}
{{../getName}}.prototype = new {{getName}}; 
{{../getName}}.prototype.constructor = {{../getName}};
{{/getSpecializes}}
        
//});