{{#getSpecializes}}
var {{getName}} = require('./{{getName}}'); 
{{/getSpecializes}}
{{#isTable}}
var mysql = require ('mysql');
{{/isTable}}
/**
 * 
 * @param {type} obj object with initial values
 * @returns {{getName}}
 */
//var {{getName}} = function(obj){
function {{getName}}(obj){
    {{#getSpecializes}}{{getName}}.call(this, obj,true);{{/getSpecializes}}
    var self = this;
    var obj = obj || {};
    /**
     * 
     * @type Number
     */
    {{#getPrimaryKeyAttribute}}var id = obj['{{getPrimaryKeyAttribute.getName}}'];{{/getPrimaryKeyAttribute}}
    {{^getPrimaryKeyAttribute}}var id = obj['id'] || 0;{{/getPrimaryKeyAttribute}}
    {{#getAttributes}}
    /**
     * 
     * @type {{getType.getName}}
     */
    self.{{getName}} = obj['{{getName}}'] || null;
    {{/getAttributes}}
    {{#getAssociationEnds}}
    /**
     * 
     * @type {{getType.getName}}
     */
    self.{{getName}} =  obj['{{getName}}'] || null;
    {{/getAssociationEnds}}
            
    {{#getPrimaryKeyAttribute}}
    self.getId = function(){return self.{{getName}};};
    self.setId =  function(id){self.{{getName}} = id;};
    {{/getPrimaryKeyAttribute}}    
    {{#getAttributes}}
    self.get{{getNameUCFirst}} = function(){return self.{{getName}};};
    self.set{{getNameUCFirst}} = function({{getName}}){self.{{getName}} = {{getName}};return self;};
    {{/getAttributes}}
    {{#getAssociationEnds}}
    self.get{{getNameUCFirst}} = function(){return self.{{getName}};};
    self.set{{getNameUCFirst}} = function({{getName}}){return self.{{getName}} = {{getName}};return self;};
    {{/getAssociationEnds}}
    
    self.toObj = function(){
        var obj = {};
        {{#getAttributes}}
        obj.{{getName}} = self.{{getName}};
        {{/getAttributes}}
        {{#getAssociationEnds}}
        obj.{{getName}} = self.{{getName}}.getId();
        {{/getAssociationEnds}}
        return obj;
    };
    {{#isTable}}
    /**
     * saves Object {{getName}} into database
     * @returns {{getName}}
     */
    self.save = function(){
        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'bank'
        });
        if(self.getId() > 0){
            connection.query('UPDATE {{getName}} SET ? WHERE id = "'+self.getId()+'"',  self.toObj(), function(err, result) {
                console.log(err);
            });
        }else{
            connection.query('INSERT INTO {{getName}} SET ?',  self.toObj(), function(err, result) {
                console.log(err);          
                self.setId(result.insertId)
            });            
        }
        return self;
    };
    {{/isTable}}
}

{{getName}}.create = function(){
    return new {{getName}}();
};
{{getName}}.prototype.init = function(){};

{{#getOperations}}
/**
 * 
{{#getParameters}}
 * @param {{getType.getName}} {{getName}}
{{/getParameters}}
 * @returns {undefined}
 */
{{getParent.getName}}.prototype.{{getName}} = function({{#getParameters}}{{^first}}, {{/first}}{{getName}}{{/getParameters}}){
    var self = this;
}
{{/getOperations}}

{{#getSpecializes}}
{{../getName}}.prototype = new {{getName}}; 
{{../getName}}.prototype.constructor = {{../getName}};
{{/getSpecializes}}

module.exports = {{getName}};