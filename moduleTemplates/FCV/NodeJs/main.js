var http = require('http');
var cluster = require('cluster');
var http = require('http');
var Parallel = require('paralleljs');
var mysql = require('mysql');
var async = require('async');

var longQuerySize = 10;
var useMysql = true;
var useCluster = true;
var useParallel = false;

//getClasses
{{#getAllClasses}}
var {{getName}} = require("./classes{{getWorkPackage.getPath}}.js");
{{/getAllClasses}}

function initAccounts(count) {
    count = count || 10;
            for (var i = 0; i < count; i++) {
    var acc = Account.create()
        .setBalance(Math.floor((Math.random() * 100000) + 1))
        .plays(
            CheckingAccount.create()
                .setLimit((Math.random() * 5) * 1000)
        ).plays(
            SavingsAccount.create()
                .setTransactionFee((Math.random() * 10))
        );
        if(useMysql) acc.save();
    }
}

if (cluster.isMaster && useCluster) {
    initAccounts();
    var numCPUs = require('os').cpus().length;
    console.log(numCPUs + " cpus found");
    // Fork workers.
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
        console.log('Fork ' + (i + 1) + '');
    }

    cluster.on('exit', function(worker, code, signal) {
    console.log('worker ' + worker.process.pid + ' died');
        console.log('Restarting ' + worker.process.pid + '');
        cluster.fork();
    });
} else {
    // Workers can share any TCP connection
    // In this case its a HTTP server
    http.createServer(function(req, res) {
        
    }).listen(80);
}
