define(['require','{{#substr 1}}{{getWorkPackage.getPath}}{{/substr 1}}'{{#getClientSpecializes table}},'{{#substr 1}}{{getWorkPackage.getPath}}CollectionPrepare{{/substr 1}}'{{/getClientSpecializes table}}], //{{#multi}},'js/classes/{{req.getWorkPackage.getPath}}Collection.js'{{/multi}}
function (require) { //,{{getName}}{{#getClientSpecializes table}},{{getName}}CollectionPrepare{{/getClientSpecializes table}} //{{#multi}},{{req.getName}}Collection{{/multi}}

var {{getName}}CollectionPrepare = function(obj,calledByChild){
    calledByChild = calledByChild || false;
    
    {{#getClientSpecializes table}}{{getName}}CollectionPrepare.call(this, obj,true);{{/getClientSpecializes table}}
    
    var self = this;    
    
    self.wheres = [];
    self.columns = [];
    
    self.joins = {};
    self.order = '';
    self.connectionLogic = 'and';
    self.limitTxt = null;
    
    
    self.updateFromObj = function(obj, fromUser){
        if(obj){
            if(obj.columns) self.columns = obj.columns;
            if(obj.wheres) self.wheres = obj.wheres;
            if(obj.order) self.order = obj.order;
            if(obj.connectionLogic) self.connectionLogic = obj.connectionLogic;
            if(obj.limit) self.limit = obj.limit;

            if(obj.joins){
                for(var index in obj.joins){
                    var colP = obj.joins[index];
                    if(colP['class']){
                        colP = new globalScope[colP['class']](colP['data']);
                    }
                    self.joins[index] = colP;
                }
            }
        }
        
    };
    
    self.updateFromObj(obj);
    
    self.limit = function(limit){
        self.limitTxt = limit;
        return self;
    }
    
    self.orderBy = function(order){
        self.order = order;
        return self;
    }
    
    self.where = function(column,operator,value,logic){
        operator = operator || '=';
        value = value || '';
        logic = logic || 'and';
        if(typeof column === 'function'){
            var tmpPrepare = new {{getName}}CollectionPrepare();
            column(tmpPrepare);
            this.wheres.push([tmpPrepare.toObj(),'','',logic]);
        }else{
            this.wheres.push([column,operator,value,logic]);
        }
        return this;
    }
    self.orWhere = function(column,operator,value){
        this.wheres.push([column,operator,value,'or']);
        return this;
    }
    
    self.selectAll = function(){
        this.columns = ['*'];
        return this;
    }
    self.getConnectionLogic = function(){
        return this.connectionLogic;
    }
    self.setConnectionLogic = function(connectionLogic){
        this.connectionLogic = connectionLogic;
        return this;
    }
    
    {{#getAttributes}}
    {{#checkClientColP where}}
    self.where{{getNameUCFirst}} = function(operation, value){
        this.where('{{getName}}', operation, value);
        return this;
    }
    self.orWhere{{getNameUCFirst}} = function(operation, value){
        this.orWhere('{{getName}}', operation, value);
        return this;
    }
    {{/checkClientColP where}}
    {{#checkClientColP select}}
    self.select{{getNameUCFirst}} = function(){
        this.columns.push('{{getName}}');
        return this;
    }
    {{/checkClientColP select}}
    {{#isClass}}
    {{#checkClientColP set}}
    self.set{{getNameUCFirst}} = function(collectionsPrepare, logic){
        logic = logic || "and";
        this.joins['{{getName}}'] = collectionsPrepare;
        collectionsPrepare.setConnectionLogic(logic);
        return this;
    }
    {{/checkClientColP set}}
    {{/isClass}}
    {{/getAttributes}}
    {{#getAssociationEnds}}
    {{^isCardinalityZero}}
    {{#checkClientColP select}}
    self.select{{getNameUCFirst}} = function(){
        this.columns.push('{{getName}}');
        return this;
    }
    {{/checkClientColP select}}
    {{#checkClientColP set}}
    self.{{setAssociationFunction}} = function(collectionsPrepare, logic){
        logic = logic || "and";
        this.joins['{{getName}}'] = collectionsPrepare;
        collectionsPrepare.setConnectionLogic(logic);
        return this;
    }
    {{/checkClientColP set}}
    {{#isCardinalitySingle}}
    {{#checkClientColP where}}
    self.where{{getNameUCFirst}} = function(operation, value){
        this.where('{{getName}}', operation, value);
        return this;
    }
    self.orWhere{{getNameUCFirst}} = function(operation, value){
        this.orWhere('{{getName}}', operation, value);
        return this;
    }
    {{/checkClientColP where}}
    {{/isCardinalitySingle}}
    {{^isCardinalitySingle}}
    {{#isAssociationClass}}
    //assocClass 
//    self.where{{getNameUCFirst}} = function(operation, value){
//        this.where('{{getName}}', operation, value);
//        return this;
//    }
//    self.orWhere{{getNameUCFirst}} = function(operation, value){
//        this.orWhere('{{getName}}', operation, value);
//        return this;
//    }
    {{/isAssociationClass}}
    {{/isCardinalitySingle}}
    {{#getParent.isAssociationClass}}
    {{^isAssociationClass}}
    {{#checkClientColP set}}
    self.set{{getParent.getName}} = function(collectionsPrepare, logic){
        logic = logic || "and";
        this.joins['{{getParent.getName}}'] = collectionsPrepare;
        collectionsPrepare.setConnectionLogic(logic);
        return this;
    }
    {{/checkClientColP set}}
    {{/isAssociationClass}}
    {{/getParent.isAssociationClass}}
            
    {{#isAssociationClass}}
    {{#checkClientColP where}}
    self.where{{getClass.getName}} = function(operation, value){
        this.where('{{getClass.getNameLCFirst}}', operation, value);
        return this;
    }
    {{/checkClientColP where}}
    {{#checkClientColP select}}
    self.select{{getClass.getName}} = function(operation, value){
        this.where('{{getClass.getNameLCFirst}}', operation, value);
        return this;
    }
    self.orWhere{{getClass.getName}} = function(operation, value){
        this.orWhere('{{getClass.getNameLCFirst}}', operation, value);
        return this;
    }
    {{/checkClientColP select}}
    
    {{#checkClientColP set}}
    self.set{{getClass.getName}} = function(collectionsPrepare, logic){
        logic = logic || "and";
        this.joins['{{getClass.getName}}'] = collectionsPrepare;
        collectionsPrepare.setConnectionLogic(logic);
        return this;
    }
    {{/checkClientColP set}}
    {{/isAssociationClass}}
    {{/isCardinalityZero}}
    {{/getAssociationEnds}}
            
    self.toObj = function(){
        var obj = {};
        obj.class = '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}CollectionPrepare';
        obj.wheres = this.wheres;
        obj.columns = this.columns;
        obj.joins = {};
        obj.orderBy = this.order;
        obj.limit = this.limitTxt;
        obj.connectionLogic = this.connectionLogic;
        for(var index in this.joins){
            obj.joins[index] = this.joins[index].toObj();
        }
        return obj;
    }
            
    self.get = function(cb, cache){
        cache = cache || false;
        var dfd = $.Deferred();
        $.post('ajax.php',{
            'class': '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}CollectionPrepare',
            'action': 'get',
            'objParam': self.toObj(),
            'param': {}
        },function(r){
                if(r.error){
                    alert(r.error);
                    dfd.reject();
                }
                else{
                    var data = [];
                    for(var index in r){
                        data.push(new {{getName}}(r[index]));
                    }
                    dfd.resolve(data);
                    if(cb) cb(data);
                }
        },'json');
        return dfd.promise();
    }
}

{{getName}}CollectionPrepare.prototype.init = function(){};


{{#getAssociationEnds}}
{{^isCardinalityZero}}
{{../getName}}CollectionPrepare.prototype.get{{getNameUCFirst}} = function(){
    if(this.{{getName}} && this.{{getName}}.class && this.{{getName}}.id) this.{{getName}} = jsModel[this.{{getName}}.class+'.'+this.{{getName}}.id];
    return this.{{getName}};
};
{{/isCardinalityZero}}
{{/getAssociationEnds}}

{{getName}}CollectionPrepare.create = function(){
    return new {{getName}}CollectionPrepare();
};

{{#getClientSpecializes table}}
FCVClass.extend({{../getName}}CollectionPrepare, {{getName}}CollectionPrepare);
{{/getClientSpecializes table}}
        
        globalScope.{{getName}}CollectionPrepare = {{getName}}CollectionPrepare;
        
        return {{getName}}CollectionPrepare;        
});