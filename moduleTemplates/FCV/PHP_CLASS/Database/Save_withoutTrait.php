
{{#getAssociationEnds}}
{{#getParent.isRelationNM}}
{{#getOppositeAssociationEnd.getClass.isTable}}
public function save{{getNameUCFirst}}(){
    if($this->{{getName}} && is_object($this->{{getName}})){
        $q = new \fcv\database\QueryBuilder();
        $q->from('{{getParent.getName}}');
        $q->where('{{getOppositeAssociationEnd.getConnectionClass.getNameLCFirst}}', '=', $this->getId());
        $q->delete();

        $q = new \fcv\database\QueryBuilder();
        $q->from('{{getParent.getName}}');
        foreach($this->{{getName}} as ${{getName}}){
            $q->insert(array(
                '{{getConnectionClass.getNameLCFirst}}' => ${{getName}}->getId(),
                '{{getOppositeAssociationEnd.getConnectionClass.getNameLCFirst}}' => $this->getId()
            ));
        }
    }
}
{{/getOppositeAssociationEnd.getClass.isTable}}
{{/getParent.isRelationNM}}
{{/getAssociationEnds}}

{{^getSpecializes}}

{{#hasActive}}

public function isActive(){
    return $this->active;
}

public function activate(){
    $this->active = true;
    return $this;
}
public function deactivate(){
    $this->active = false;
    return $this;
}
{{#isTable}}
public function softDelete(){
    $this->deactivate();
    $col = $this->getCurrentCollectionPrepare();
    $col->update([
        '{{getPrimaryKeyAttribute.getName}}' => $this->getId(), //needed for security checks
        'active' => 0
    ], $this);
    $this->trigger('softDelete');
    return $this;
}
public function softRecover(){
    $this->activate();
    $col = $this->getCurrentCollectionPrepare();
    $col->update([
        '{{getPrimaryKeyAttribute.getName}}' => $this->getId(), //needed for security checks
        'active' => 1
    ], $this);
    $this->trigger('softRecover');
    return $this;
}
{{/isTable}}
{{/hasActive}}
{{/hasActive}}

function setNull($fields){
    if(!is_array($fields)) $fields = [$fields];
    
    $nullFields = array();
    
    foreach($fields as $f){
        $nullFields[$f] = null;
    }
    
    if($this->getId()){
        $col = $this->getCurrentCollectionPrepare();
        $col->update($nullFields, $this);
        $this->trigger("update", $this);
    }
    
    return $this;
}

private function toStringObject(){
    $insertObj = array();
    
    {{^getSpecializes}}
    {{#isAssociationClass}}
    {{^hasPrimaryKeyAttribute}}
    {{#getAssociationEnds}}
    $insertObj['{{getType.getNameLCFirst}}'] = ((is_object($this->{{getType.getNameLCFirst}}))?$this->{{getType.getNameLCFirst}}->getId():$this->{{getType.getNameLCFirst}});
    if(is_null($insertObj['{{getType.getNameLCFirst}}'])) $insertObj['{{getType.getNameLCFirst}}'] = null;
    {{/getAssociationEnds}}
    {{/hasPrimaryKeyAttribute}}
    {{#hasPrimaryKeyAttribute}}
    $insertObj['{{getPrimaryKeyAttribute.getName}}'] = $this->getId()?$this->getId():'';
    {{/hasPrimaryKeyAttribute}}
    {{/isAssociationClass}}
    {{^isAssociationClass}}
    $insertObj['{{getPrimaryKeyAttribute.getName}}'] = $this->getId()?$this->getId():'';
    {{/isAssociationClass}}
    {{/getSpecializes}}
    
    {{#getAttributes}}
    {{^isStatic}}
    {{^hasStereoType noDBField}}
    {{^isClass}}if(isSet($this->{{getName}})) $insertObj['{{getName}}'] = $this->{{getName}}{{#getType.usesTypeParser}}->val(){{/getType.usesTypeParser}}{{#isClass}}->getId(){{/isClass}};{{/isClass}}
    {{#isClass}}if(isSet($this->{{getName}})){
        $insertObj['{{getName}}'] =  (!is_object($this->{{getName}}))?$this->{{getName}}:$this->{{getName}}->getId();
    }{{/isClass}}
    {{/hasStereoType noDBField}}
    {{/isStatic}}
    {{/getAttributes}} 

    {{#isAssociationClass}}
    {{#getAssociationEnds}} 
    {{^isStatic}}
    {{^isCardinalityZero}}
    {{^hasStereoType noDBField}}
    if($this->{{getConnectionClass.getNameLCFirst}}){
        if(is_numeric($this->{{getConnectionClass.getNameLCFirst}})) $insertObj['{{getConnectionClass.getNameLCFirst}}'] = $this->{{getConnectionClass.getNameLCFirst}};
        else $insertObj['{{getConnectionClass.getNameLCFirst}}'] = $this->{{getConnectionClass.getNameLCFirst}}->getId();
    }
    {{/hasStereoType noDBField}}
    {{/isCardinalityZero}}
    {{/isStatic}}
    {{/getAssociationEnds}}
    {{/isAssociationClass}}
    {{^isAssociationClass}}
    {{#getAssociationEnds}} 
    {{^isStatic}}
    {{^isCardinalityZero}}
    {{^getParent.isAssociationClass}}
    {{#isCardinalitySingle}}
    if($this->{{getName}}){
        if(!is_object($this->{{getName}})) $insertObj['{{getName}}'] = $this->{{getName}};
        else $insertObj['{{getName}}'] = $this->{{getName}}->getId();
    }
    {{/isCardinalitySingle}}
    {{/getParent.isAssociationClass}}
    {{/isCardinalityZero}}
    {{/isStatic}}
    {{/getAssociationEnds}}
    {{/isAssociationClass}}
    
    return $insertObj;
}

public static function getCollectionPrepare($id = null){

    {{#hasMultiplePrimaryKeyAttributes}}
    $id = is_string($id) ? explode('_', $id) : $id;
    {{/hasMultiplePrimaryKeyAttributes}}
    
    $col = {{getName}}CollectionPrepare::create();
    if(!is_null($id)){
        {{^hasPrimaryKeyAttribute}}
        {{#isAssociationClass}}
        $index = 0;
        {{#getAssociationEnds}}
        $col->where('{{getType.getNameLCFirst}}', '=', $id[$index++]);
        {{/getAssociationEnds}}
        {{/isAssociationClass}}
        {{/hasPrimaryKeyAttribute}}

        {{#hasPrimaryKeyAttribute}}
        {{#getPrimaryKeyAttributes}}
        $col->where('{{getName}}', '=', $id);
        {{/getPrimaryKeyAttributes}}
        {{/hasPrimaryKeyAttribute}}
    }
    return $col;
}

private function getCurrentCollectionPrepare(){
    
    $col = {{getName}}::getCollectionPrepare($this->getId());
    
    return $col;
}

{{#hasHistory}}
public function saveCurrentVersion(){
    if($this->getId()){        
        $mysql = DB::getObj();
        $sql = 'INSERT INTO `{{getName}}History` '
            . 'SELECT *,COALESCE(('
            . 'SELECT version FROM `{{getName}}History` WHERE {{getPrimaryKeyAttribute.getName}} = '.$this->getId().' ORDER BY version DESC  LIMIT 1'
            . '),0)+1 as version '
            . 'FROM `{{getName}}` WHERE {{getPrimaryKeyAttribute.getName}} = '.$this->getId().';';
        $mysql->query($sql);
    }
}
{{/hasHistory}}

/**
 * Saves the Object Data
 * @return {{getName}} this Object
 */
public function {{getSaveOperation}}($useHistory = true, $updateCreated = false, $isNewWithId = false, $updateParent = true){
    $this->trigger('beforeSave');
    
    $isNew = $isNewWithId || empty($this->getId());
    
    {{#getSpecializes}}
    if($updateParent){
        parent::save($useHistory, $updateCreated, $isNewWithId);
    }
    $insertObj = $this->toStringObject();
    $insertObj['{{getPrimaryKeyAttribute.getName}}'] = $this->getId();
    {{/getSpecializes}}
    
    {{^getSpecializes}}
    $insertObj = $this->toStringObject();
    $created = ($this->created)?$this->created->format('Y-m-d H:i:s'):\fcv\database\QueryBuilder::getObj()->raw('Now()');
    //$lastModified = ($this->lastModified)?$this->lastModified->format('Y-m-d H:i:s'):\fcv\database\QueryBuilder::getObj()->raw('Now()');
    $lastModified = \fcv\database\QueryBuilder::getObj()->raw('Now()');
    $insertObj['lastModified'] = $lastModified;
    
    {{#hasActive}}
    $insertObj['active'] = $this->active;
    {{/hasActive}}
    {{/getSpecializes}}
    
    {{#hasHistory}}
    if($useHistory){        
        $this->saveCurrentVersion();
    }
    {{/hasHistory}}
        
    $this->trigger("save", $insertObj);
    
    {{#hasMultiplePrimaryKeyAttributes}}
    $isNew = $this->getCurrentCollectionPrepare()->get()->isEmpty();
    {{/hasMultiplePrimaryKeyAttributes}}
    if(!$isNew){
        {{^getSpecializes}}
        if($updateCreated && $this->created){
            $insertObj['created'] = $created;
        }
        {{/getSpecializes}}
        $col = $this->getCurrentCollectionPrepare();
        $col->update($insertObj, $this);
        $this->trigger("update", $this);
    }else{
        {{^getSpecializes}}
        $createdBy = $this->createdBy ? $this->createdBy:\fcv\database\DB::getCreatedBy();
        if($createdBy){
            $this->createdBy = $createdBy;
            $insertObj['createdBy'] = $createdBy;    
        }
        $insertObj['created'] = $created;
        {{/getSpecializes}}
        $col = {{getName}}CollectionPrepare::create();
        $id = $col->insertGetId($insertObj, $this);    
        
        $this->trigger("insert", $this);    
        
        if ($this->hasPrimaryKey()){
            $this->setId($id);
        }
    }
        
    $this->trigger("afterSave", $insertObj);
    return $this;
}

/**
 * Saves the Object Data
 * @return {{getName}} this Object
 */
public function {{getSaveOperation}}OldAndImport($useHistory = true, $updateCreated = false){
    $this->trigger('beforeSave');
    $insertObj = array();
    {{#getSpecializes}}
    $insertObj['{{getPrimaryKeyAttribute.getName}}'] = parent::saveOldAndImport()->getId();
    {{/getSpecializes}}
    {{^getSpecializes}}
    {{#isAssociationClass}}
    {{#getAssociationEnds}}
    $insertObj['{{getType.getNameLCFirst}}'] = ((is_object($this->{{getType.getNameLCFirst}}))?$this->{{getType.getNameLCFirst}}->getId():$this->{{getType.getNameLCFirst}});
    if(is_null($insertObj['{{getType.getNameLCFirst}}'])) $insertObj['{{getType.getNameLCFirst}}'] = 0;
    {{/getAssociationEnds}}
    {{/isAssociationClass}}{{^isAssociationClass}}
    $insertObj['{{getPrimaryKeyAttribute.getName}}'] = $this->getId()?$this->getId():'';
    {{/isAssociationClass}}
    {{/getSpecializes}}
    //New {{getName}}
    $mysql = DB::getObj();
    
    {{^getSpecializes}}
    $created = ($this->created)?"'".$this->created->format('Y-m-d H:i:s')."'":'NOW()';
    $lastModified = ($this->lastModified)?"'".$this->lastModified->format('Y-m-d H:i:s')."'":'NOW()';
    $createdBy = ($this->createdBy && !is_array($this->createdBy))?$this->createdBy:"NULL";
    {{#hasActive}}
    $insertObj['active'] = $this->active;
    {{/hasActive}}
    {{/getSpecializes}}
    
    {{#hasHistory}}
    if($this->getId() && $useHistory){        
        $sql = 'INSERT INTO `{{getName}}History` '
            . 'SELECT *,COALESCE(('
            . 'SELECT version FROM `{{getName}}History` WHERE {{getPrimaryKeyAttribute.getName}} = '.$this->getId().' ORDER BY version DESC  LIMIT 1'
            . '),0)+1 as version '
            . 'FROM `{{getName}}` WHERE {{getPrimaryKeyAttribute.getName}} = '.$this->getId().';';
        $mysql->query($sql);
    }
    {{/hasHistory}}
    {{#getAttributes}}
    {{^isStatic}}
        {{^hasStereoType noDBField}}
        {{^isClass}}if(isSet($this->{{getName}})) $insertObj['{{getName}}'] = $this->{{getName}}{{#getType.usesTypeParser}}->val(){{/getType.usesTypeParser}}{{#isClass}}->getId(){{/isClass}};{{/isClass}}
        {{#isClass}}if(isSet($this->{{getName}})){
            $insertObj['{{getName}}'] =  (!is_object($this->{{getName}}))?$this->{{getName}}:$this->{{getName}}->getId();
        }{{/isClass}}
        {{/hasStereoType noDBField}}
    {{/isStatic}}
    {{/getAttributes}} 

    {{#isAssociationClass}}
     {{#getAssociationEnds}} 
     {{^isStatic}}
        {{^isCardinalityZero}}
        {{^hasStereoType noDBField}}
    if($this->{{getConnectionClass.getNameLCFirst}}){
        if(is_numeric($this->{{getConnectionClass.getNameLCFirst}})) $insertObj['{{getConnectionClass.getNameLCFirst}}'] = $this->{{getConnectionClass.getNameLCFirst}};
        else $insertObj['{{getConnectionClass.getNameLCFirst}}'] = $this->{{getConnectionClass.getNameLCFirst}}->getId();
    }
        {{/hasStereoType noDBField}}
        {{/isCardinalityZero}}
     {{/isStatic}}
    {{/getAssociationEnds}}
    {{/isAssociationClass}}
    {{^isAssociationClass}}
    {{#getAssociationEnds}} 
    {{^isStatic}}
    {{^isCardinalityZero}}
    {{#getParent.isAssociationClass}}
//        if($this->{{getNameLCFirst}}){
//            if(is_numeric($this->{{getNameLCFirst}})) $insertObj['{{getNameLCFirst}}'] = $this->{{getNameLCFirst}};
//            else $insertObj['{{getNameLCFirst}}'] = $this->{{getNameLCFirst}}->getId();
//        }
    {{/getParent.isAssociationClass}}
    {{^getParent.isAssociationClass}}
    {{#isCardinalitySingle}}
        if($this->{{getName}}){
            if(!is_object($this->{{getName}})) $insertObj['{{getName}}'] = $this->{{getName}};
            else $insertObj['{{getName}}'] = $this->{{getName}}->getId();
        }
    {{/isCardinalitySingle}}
    {{/getParent.isAssociationClass}}
    {{/isCardinalityZero}}
    {{/isStatic}}
    {{/getAssociationEnds}}
    {{/isAssociationClass}}
    
    $fields = array_keys($insertObj);
    $prepareValues = array();
    $sql = "INSERT INTO `{{getName}}{{#getSpecializes}}_sub{{/getSpecializes}}` (`" . implode('`,`', $fields) . "`{{^getSpecializes}},`lastModified`,`created`,`createdBy`{{/getSpecializes}}) VALUES (" .
            implode(",", array_map(function($key) {
                        return ":" . $key;
                    }, $fields)) . "{{^getSpecializes}}, ".$lastModified.", ".$created.", ".$createdBy."{{/getSpecializes}})";
    $sql .= " ON DUPLICATE KEY UPDATE " .
            implode(",", array_map(function($key) {
                        return "`" . $key . "` = :" . $key . "";
                    }, $fields)) {{^getSpecializes}}. ", `lastModified` = ".$lastModified.(($updateCreated && $this->created)?", `created` = ".$created:'').(($createdBy)?", `createdBy` = ".$createdBy:''){{/getSpecializes}};
      

    $sth = $mysql->prepare($sql);

    foreach ($insertObj as $id => $obj) {
        $prepareValues[':' . $id] = $obj;
    }
    $this->trigger("save", $insertObj);
    
    $sth->execute($prepareValues);
    
    if ($this->hasPrimaryKey() && !$this->getId()){
            $this->setId($mysql->lastInsertId());
    }
    $this->trigger("afterSave", $insertObj);
    return $this;
}

/**
 * Loads {{getName}} object from Id
 * @return {{getName}} object
 * @throws ObjectDoesNotExist
 */
public static function load($id){
    
    $colP = {{getName}}::getCollectionPrepare($id);
    $colP->selectAll();
    $col = $colP->get();
    
    if($col->isEmpty()){
        if(!Debug::getObj()->getThrowErrors()) return null;
        else{
            throw new ObjectDoesNotExist('Object {{getName}} with Id '.$id.' not found.');
        }
    }
    
    return $col->current();
}

/**
 * Reload {{getName}} from Database
 * @return {{getName}} object
 * @throws ObjectDoesNotExist
 */
public function reLoad(){
    $id = $this->getId();
    $colP = {{getName}}::getCollectionPrepare($id);
    $colP->selectAll();
    $data = $colP->getSimpleData();
    
    if(empty($data)){
        if(!Debug::getObj()->getThrowErrors()) return null;
        else{
            throw new ObjectDoesNotExist('Object {{getName}} with Id '.$id.' not found.');
        }
    }
    $this->loadFromArray($data[0]);
    
    return $this;
}

/**
 * Deletes current {{getName}} from Database
 */
public function delete({{#getSpecializes}}$deleteMain = true{{/getSpecializes}}){
    
    $this->trigger("beforeDelete");
    
    $col = $this->getCurrentCollectionPrepare();
    
    {{#getSpecializes}}
    if($deleteMain) parent::delete();
    $col->getQuery()->from = "{{getName}}_sub";
    {{/getSpecializes}}
    
    $col->delete();
    
    $this->trigger("delete");
    
    return true;
}
