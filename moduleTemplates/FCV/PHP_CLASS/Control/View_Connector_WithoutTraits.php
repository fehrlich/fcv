{{^getSpecializes}}
protected $tplVars = array();

public function addTemplateVar($name, $content){
    $this->tplVars[$name] = $content;
    return $this;
}

public function appendTemplateVar($name, $content){
    $this->tplVars[$name] = (!isset($this->tplVars[$name]))?$content:$this->tplVars[$name].$content;
    return $this;
}

public function getTemplateVars(){
    return $this->tplVars;
}

public function setTemplateVars($tplVars){
    $this->tplVars = $tplVars;
    return $this;
}

{{/getSpecializes}}

private $ignoreErrors = false;
/**
 * get Html Template
 * @param string $template template name
 */
public function getHtml($template = "default", $addView = false){
    $availableTemplates =  array(
        'default' => '.default'
    {{#getWorkPackage.getViews}}
        ,'{{getName}}' => '{{../getName}}.{{getName}}'
        {{#getWorkPackage.getChilds}}
        {{#getViews}}
        ,'{{../getName}}/{{getName}}' => '/{{../getName}}/{{../getName}}.{{getName}}'
        {{/getViews}}
        {{/getWorkPackage.getChilds}}
    {{/getWorkPackage.getViews}}
    );
    {{#getWorkPackage.getChilds}}
    {{#getViews}}
    if($template == '{{../getName}}/{{getName}}') $this->{{../getName}}();
    {{/getViews}}
    {{/getWorkPackage.getChilds}}
    
    //if(!in_array($template, $availableTemplates)){
    if(!isset($availableTemplates[$template])){
        {{^getSpecializes}}
        //throw new Exception('Template <b>'.$template.'</b> doesn\'t exist for <b>{{getName}}</b>');
        {{/getSpecializes}} 
        {{#getSpecializes}}
        return parent::getHtml($template);
        {{/getSpecializes}} 
    }
    
    $parsedTemplate = $availableTemplates[$template];
{{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    $templates = array(
{{#getWorkPackage}}
{{#list getViews}}
        {{^first}},{{/first}}'{{getName}}.{{value.getName}}' => '{{#value}}{{#includeTemplate 2}}{{&getHtmlContent}}{{/includeTemplate 2}}{{/value}}' //{{value.getId}}
        {{#value.getWorkPackage.getChilds}}
        {{#list getViews}}
        ,'/{{getName}}/{{getName}}.{{value.getName}}' => '{{#value}}{{#includeTemplate 2}}{{&getHtmlContent}}{{/includeTemplate 2}}{{/value}}' //{{value.getId}}
{{/list getViews}}
        {{/value.getWorkPackage.getChilds}}
        
        {{/list getViews}}
{{/getWorkPackage}}
        
        {{#getWorkPackage.getChilds}}
        {{#list getViews}}
        ,'/{{getName}}/{{getName}}.{{value.getName}}' => '{{#value}}{{#includeTemplate 2}}{{&getHtmlContent}}{{/includeTemplate 2}}{{/value}}' //{{value.getId}}
        {{/list getViews}}
        {{/getWorkPackage.getChilds}}
    );
    $tplSrc = $templates[$parsedTemplate];
    
    //load viewControlers
    
    switch($parsedTemplate){
        {{#getWorkPackage}}
        {{#list getViews}}
        {{#value.getWorkPackage.getFunctionContent}}
        case '{{getName}}.{{value.getName}}':
            {{#value}}
            {{#includeCode}}{{#indent 4}}{{#getWorkPackage}}{{#singleParse}}{{&getFunctionContent}}{{/singleParse}}{{/getWorkPackage}}{{/indent 4}}{{/includeCode}}
            {{/value}}
            break;
        {{/value.getWorkPackage.getFunctionContent}}
        {{/list getViews}}
        {{/getWorkPackage}}
        
        {{#value.getWorkPackage.getChilds}}
        {{#list getViews}}
        {{#value.getWorkPackage.getFunctionContent}}
        case '/{{getName}}/{{getName}}.{{value.getName}}':
            {{#value}}
            {{#includeCode}}{{#indent 4}}{{#getWorkPackage}}{{#singleParse}}{{&getFunctionContent}}{{/singleParse}}{{/getWorkPackage}}{{/indent 4}}{{/includeCode}}
            {{/value}}
            break;
        {{/value.getWorkPackage.getFunctionContent}}
        {{/list getViews}}
        {{/value.getWorkPackage.getChilds}}
    }
{{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    {{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    $tplSrc = file_get_contents('FCVAPs{{#getWorkPackage}}/{{getPath}}/{{/getWorkPackage}}'.$parsedTemplate.'.html');
    
    //load ViewControler if exists
    if(file_exists('FCVAPs{{#getWorkPackage}}/{{getPath}}/{{/getWorkPackage}}'.$parsedTemplate.'.php')){
        include('FCVAPs{{#getWorkPackage}}/{{getPath}}/{{/getWorkPackage}}'.$parsedTemplate.'.php');
    }
    {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    
    foreach($this->tplVars as $name => $content){
        $this->$name = $content;
    }
    $throwBefore = Debug::getObj()->getThrowErrors();
    Debug::getObj()->setThrowErrors(false);
    try{
        $htmlSrc = TemplateEngine::get()->render($tplSrc, $this); //, '{{getName}}.'.$template
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
        echo 'Error in {{getName}}.php thrown in Template "'.$template.'"';
        throw $exc;
    }
    Debug::getObj()->setThrowErrors($throwBefore);
    if($addView){
        $htmlSrc = '<div class="view" data-object="{{getName}}.'.$this->getId().'" data-template="'.$template.'">'."\n"
            .$htmlSrc."\n"
            .'</div>'."\n";
    }
    return $htmlSrc; 
}

public function view($template = "default"){
    return $this->getHtml($template, true);
}

public function getHtmlFromTemplate($template){

    $throwBefore = Debug::getObj()->getThrowErrors();
    Debug::getObj()->setThrowErrors(false);
    try{
        $html = TemplateEngine::get()->render($template, $this);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
        echo 'Error in custom template for {{getName}}';
        throw $exc;
    }
    Debug::getObj()->setThrowErrors($throwBefore);
    
    return $html;
}

public static function getSubHtml($operationName, $template = 'default', $addView = false){

    {{#getAPChilds}}
//        $this->VIEW['{{getName}}'] = array(
//        {{#getViews}}
//            {{^default}},{{/default}}'{{name}}' => TemplateEngine::getObj()->render(file_get_contents('FCVAPs/{{#getWorkPackage}}/{{getPath}}/{{getName}}.{{../getName}}{{/getWorkPackage}}.html'), $this) //, '{{getName}}.sub.'.$operationName.'.'.$template
//        {{/getViews}}
//        );
    {{/getAPChilds}}
    
    $availableTemplates =  array(
    {{#list getOperations}}
        '{{value.getName}}' => array(
            'default'
            {{#value.getWorkPackage.getViews}}
            ,'{{getName}}'
            {{/value.getWorkPackage.getViews}}
        ){{^last}},{{/last}}
    {{/list getOperations}}
    );
    if(!isset($availableTemplates[$operationName]) || !in_array($template, $availableTemplates[$operationName])){
        {{^getSpecializes}}
        throw new Exception('Template OperationTemplate <b>'.$template.'</b> doesn\'t exist for <b>{{getName}}.'.$operationName.'</b>');
        {{/getSpecializes}} 
    {{#getSpecializes}}
        return parent::getHtml($template);
    {{/getSpecializes}} 
}
    $tplSrc = file_get_contents('FCVAPs{{getWorkPackage.getPath}}/'.$operationName.'/'.$operationName.'.'.$template.'.html');
    /*foreach($this->tplVars as $name => $content){
        $this->$name = $content;
    }*/
    $throwBefore = Debug::getObj()->getThrowErrors();
    Debug::getObj()->setThrowErrors(false);
    $htmlSrc = TemplateEngine::get()->render($tplSrc, array()); //, '{{getName}}.sub.'.$operationName.'.'.$template
    Debug::getObj()->setThrowErrors($throwBefore);
    
    if($addView){
        $htmlSrc = '<div class="view" data-object="{{getName}}.'.$this->getId().'" data-template="'.$template.'">'."\n"
            .$htmlSrc."\n"
            .'</div>'."\n";
    }
    
    return $htmlSrc; 

}
