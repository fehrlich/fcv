private $lastModified = false;
private $created = false;
public function __construct($data = array(),$calledFromChild = false){
    if(is_string($data)) $data = json_decode($data,true);
    {{#getSpecializes}} parent::__construct($data,true);{{/getSpecializes}}
    if(count($data) > 0){
        if(isSet($data['loadObj']) && $data['loadObj']){
            $this->loadObject = true;
        }
        {{#getPrimaryKeyAttribute}}
    //        if(isset($data['{{getPrimaryKeyAttribute.getName}}']) && !empty($data['{{getPrimaryKeyAttribute.getName}}']))

        {{/getPrimaryKeyAttribute}}
        {{#getAttributes}}
        {{^isDefaultType}}if(isSet($data['{{getName}}']) && is_array($data['{{getName}}'])) $this->{{getName}} = {{getType.getName}}::create($data['{{getName}}'],$this->loadObject);
        else{{/isDefaultType}}if(isSet($data['{{getName}}'])) $this->{{getName}} = {{#getType.usesTypeParser}}{{getType.getName}}::parse($data['{{getName}}']){{/getType.usesTypeParser}}{{^getType.usesTypeParser}}$data['{{getName}}']{{/getType.usesTypeParser}};
        {{/getAttributes}}
        //associationEnds
        {{#getAssociationEnds}}
        {{^isCardinalityZero}}
        {{#isAssociationClass}}
        //For AssociationClasses {{getName}}
        if(isSet($data['{{getConnectionClass.getNameLCFirst}}'])){
            $this->{{getConnectionClass.getNameLCFirst}} = (is_array($data['{{getConnectionClass.getNameLCFirst}}']))?{{getConnectionClass.getName}}::create($data['{{getConnectionClass.getNameLCFirst}}'],$this->loadObject):$data['{{getConnectionClass.getNameLCFirst}}'];
            
            if($this->{{getConnectionClass.getNameLCFirst}}  instanceof {{getConnectionClass.getName}}){
                $this->{{getConnectionClass.getNameLCFirst}}->set{{getName}}($this);
            }
        }
        //values for AssociationClass ... used for ObjekteContent.php Vertriebspartner suche
        if(isSet($data['{{getNameLCFirst}}'])){
            if(!is_object($data['{{getNameLCFirst}}'])){
                {{^isCardinalitySingle}}
                $newVal = new {{getConnectionClass.getName}}Collection();
                foreach($data['{{getNameLCFirst}}'] as $el) $newVal->add({{getConnectionClass.getName}}::create($el,$this->loadObject));
                $this->{{getNameLCFirst}} = $newVal;
                {{/isCardinalitySingle}}
                {{#isCardinalitySingle}}
                $this->{{getNameLCFirst}} = new {{getConnectionClass.getName}}($data['{{getNameLCFirst}}']);
                {{/isCardinalitySingle}}
                {{^getOppositeAssociationEnd.isCardinalitySingle}}
                $this->{{getNameLCFirst}}->set{{getOppositeAssociationEnd.getName}}($this);
                {{/getOppositeAssociationEnd.isCardinalitySingle}}
            }else $this->{{getNameLCFirst}} = $data['{{getNameLCFirst}}'];
        }
        {{/isAssociationClass}}
        //Regular AssociationEnd {{getParent.getName}} ({{getParent.debugType}})
        {{^isAssociationClass}}
        {{^getParent.isAssociationClass}}
        /*if(isSet($data['{{getName}}'])){
            {{#isCardinalitySingle}}
            $this->{{getName}} = (is_array($data['{{getName}}']))?{{../getName}}::create($data['{{getName}}'],$this->loadObject):$data['{{getName}}'];
            {{/isCardinalitySingle}}
            {{^isCardinalitySingle}}
            if(!is_object(current($data['{{getName}}']))){
                $newVal = new {{getConnectionClass.getName}}Collection();
                foreach($data['{{getName}}'] as $el) $newVal->add({{getConnectionClass.getName}}::create($el,$this->loadObject));
                $this->{{getName}} = $newVal;
            }else  $this->{{getName}} = $data['{{getName}}'];
            {{/isCardinalitySingle}}
        }*/
        {{/getParent.isAssociationClass}}
        {{#getParent.isAssociationClass}}
        //AssociationEnd is from AssociationClass
        if(isSet($data['{{getNameLCFirst}}'])){
            {{#isCardinalitySingle}}
            $this->{{getNameLCFirst}} = (is_array($data['{{getNameLCFirst}}']))?{{getConnectionClass.getName}}::create($data['{{getNameLCFirst}}'],$this->loadObject):$data['{{getNameLCFirst}}'];
            {{/isCardinalitySingle}}
            {{^isCardinalitySingle}}
            if(!is_object(current($data['{{getNameLCFirst}}']))){
                $newVal = new {{getConnectionClass.getName}}Collection();
                foreach($data['{{getNameLCFirst}}'] as $el) $newVal->add({{getConnectionClass.getName}}::create($el,$this->loadObject));
                $this->{{getNameLCFirst}} = $newVal;
            }else $this->{{getNameLCFirst}} = $data['{{getNameLCFirst}}'];
            {{/isCardinalitySingle}}

            {{#getOppositeAssociationEnd.isCardinalitySingle}}
            if($this->{{getNameLCFirst}} instanceof {{getConnectionClass.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}}){
                $this->{{getNameLCFirst}}->set{{getConnectionClass.getName}}($this);
            }
            {{/getOppositeAssociationEnd.isCardinalitySingle}}
        }
        {{/getParent.isAssociationClass}}

        {{#getParent.isAssociationClass}}
        {{^isAssociationClass}}

        //value from AssociationClass
        if(isSet($data['{{getParent.getNameLCFirst}}'])){
            if(!is_object(current($data['{{getParent.getNameLCFirst}}']))){
                {{#isCardinalitySingle}}
                $this->{{getParent.getNameLCFirst}} = (is_array($data['{{getParent.getNameLCFirst}}']))?{{getParent.getName}}::create($data['{{getParent.getNameLCFirst}}']):$data['{{getParent.getNameLCFirst}}'];
                {{/isCardinalitySingle}}
                {{^isCardinalitySingle}}
                $newVal = new {{getParent.getName}}Collection();
                foreach($data['{{getParent.getNameLCFirst}}'] as $el) $newVal->add({{getParent.getName}}::create($el,$this->loadObject));
                $this->{{getParent.getNameLCFirst}} = $newVal;
                {{/isCardinalitySingle}}
                
            }else $this->{{getParent.getNameLCFirst}} = $data['{{getParent.getNameLCFirst}}'];
        }
        {{/isAssociationClass}}
        {{/getParent.isAssociationClass}}
        if(isSet($data['{{getNameLCFirst}}'])){
            {{#isCardinalitySingle}}
            $this->{{getNameLCFirst}} = (is_array($data['{{getNameLCFirst}}']))?{{getConnectionClass.getName}}::create($data['{{getNameLCFirst}}'],$this->loadObject):$data['{{getNameLCFirst}}'];
            {{/isCardinalitySingle}}
            {{^isCardinalitySingle}}
            if(!is_object(current($data['{{getNameLCFirst}}']))){
                $newVal = new {{getConnectionClass.getName}}Collection();
                foreach($data['{{getNameLCFirst}}'] as $el) $newVal->add({{getConnectionClass.getName}}::create($el,$this->loadObject));
                $this->{{getNameLCFirst}} = $newVal;
                
                {{#getOppositeAssociationEnd.isCardinalitySingle}}
                if($this->{{getNameLCFirst}} instanceof {{getConnectionClass.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}}){
                    $this->{{getNameLCFirst}}->set{{getOppositeAssociationEnd.getName}}($this);
                }
                {{/getOppositeAssociationEnd.isCardinalitySingle}}
            }else $this->{{getNameLCFirst}} = $data['{{getNameLCFirst}}'];
            {{/isCardinalitySingle}}
        }
        {{/isAssociationClass}}
        {{/isCardinalityZero}}
        {{/getAssociationEnds}}

        //Helper
        if(isSet($data['lastModified'])) $this->setLastModified($data['lastModified']);
        if(isSet($data['created'])) $this->setCreated($data['created']);
    }

    {{#getAPChilds}}
//        $this->VIEW['{{getName}}'] = array(
//        {{#getViews}}
//            {{^default}},{{/default}}'{{name}}' => $m->render(file_get_contents('FCVAPs/{{#getWorkPackage}}/{{getPath}}/{{getName}}.{{../getName}}{{/getWorkPackage}}.html'), $this)
//        {{/getViews}}
//        );
    {{/getAPChilds}}

    {{#isPageOrFrame}}
    {{#getWorkPackage}}
    {{#getProject.getCurrentBuild.getBuildWorkpackages}}
    //include("FCVAPs{{getPath}}/{{getName}}.php");
    {{/getProject.getCurrentBuild.getBuildWorkpackages}}
    {{^getProject.getCurrentBuild.getBuildWorkpackages}}
     //CodeInclude getFunctionContent
    //{{#includeCode}}{{#indent 2}}{{#singleParse}}{{&getFunctionContent}}{{/singleParse}}{{/indent 2}}{{/includeCode}}
    {{/getProject.getCurrentBuild.getBuildWorkpackages}}
    {{/getWorkPackage}}
    {{/isPageOrFrame}}
    {{#hasConstructor}}
    {{#getWorkPackage}}
    {{#getProject.getCurrentBuild.getBuildWorkpackages}}
    include("FCVAPs{{getPath}}/{{getName}}.php");
    {{/getProject.getCurrentBuild.getBuildWorkpackages}}
    {{^getProject.getCurrentBuild.getBuildWorkpackages}}
     //CodeInclude
    {{#includeCode}}{{#indent 2}}{{#singleParse}}{{&getFunctionContent}}{{/singleParse}}{{/indent 2}}{{/includeCode}}
    {{/getProject.getCurrentBuild.getBuildWorkpackages}}
    {{/getWorkPackage}}
    {{/hasConstructor}}


    {{#isProject}}
//    $m = new TemplateEngine;
//    $self = &$this;
    {{#getAllClasses}}
//        $this->{{getName}} = array(
//            'create' => function($arguments) use($self){
////                $self->{{getName}}Created = {{getName}}::create($arguments);
////                $self->{{getName}}Created = {{getName}}::create($arguments);
//                return {{getName}}::create($arguments);
//            }
//        );
    {{/getAllClasses}}
    {{/isProject}}
}
{{#isPageOrFrame}}
function loadContent(){
    {{#getWorkPackage}}
    {{#getProject.getCurrentBuild.getBuildWorkpackages}}
    include("FCVAPs{{getPath}}/{{getName}}.php");
    {{/getProject.getCurrentBuild.getBuildWorkpackages}}
    {{^getProject.getCurrentBuild.getBuildWorkpackages}}
     //CodeInclude getFunctionContent
    //{{#includeCode}}{{#indent 2}}{{#singleParse}}{{&getFunctionContent}}{{/singleParse}}{{/indent 2}}{{/includeCode}}
    {{/getProject.getCurrentBuild.getBuildWorkpackages}}
    {{/getWorkPackage}}
}
{{/isPageOrFrame}}

public static function create($data = array(),$loadObj = false){
    if($loadObj) $data['loadObj'] = true;
    return new {{getName}}($data);
}

public static function createLoad(){
    $data = array(
        'loadObj' => true
    );
    return new {{getName}}($data);
}

public function getLastModifiedTime(){
    return $this->lastModifiedTime;
}

public function setLastModifiedTime($time){
    $this->lastModifiedTime = $time;
    return $this;
}

public function hasPrimaryKey(){
    return {{#hasPrimaryKeyAttributes}}true{{/hasPrimaryKeyAttributes}}{{^hasPrimaryKeyAttributes}}false{{/hasPrimaryKeyAttributes}};
}

{{#hasPrimaryKeyAttributes}}
{{#hasMultiplePrimaryKeyAttributes}}
public function getId(){
    $ret = array();
    {{#getPrimaryKeyAttributes}}
    $ret[] = (is_object($this->{{getNameLCFirst}}))?$this->{{getNameLCFirst}}->getId():$this->{{getNameLCFirst}};
    {{/getPrimaryKeyAttributes}}
    return implode('_', $ret);
}
public function setId($id){
//        {{getParent.getName}}::$loadedInstances[$id] = &$this;
//    $this->{{getName}} = $id;
//   if($this->loadObject) $this->loadedAttributes['{{getName}}'] = true;
/*    d("Error cant set Id for multiple primary KEy attributes jet");
    d($id);
    exit();*/
    return $this;
}
public function getPrimaryKeyName(){
    return "{{getName}}";
}
{{/hasMultiplePrimaryKeyAttributes}}
{{^hasMultiplePrimaryKeyAttributes}}
{{#getPrimaryKeyAttribute}}
public function getId(){
    return (is_object($this->{{getName}}))?$this->{{getName}}->getId():$this->{{getName}};
}
public function setId($id){
    $this->{{getName}} = $id;
    return $this;
}
public function getPrimaryKeyName(){
    return "{{getName}}";
}
{{/getPrimaryKeyAttribute}}
{{/hasMultiplePrimaryKeyAttributes}}
{{/hasPrimaryKeyAttributes}}
{{^hasPrimaryKeyAttributes}}

private $parent;
private $parentIndex = 0;

public function getParent(){
    return $this->parent;
}
public function setParent(&$parent, $parentIndex = 0){
    $this->parent = $parent;
    $this->parentIndex = $parentIndex;

    return $this;
}
public function getId(){
    if(!isset($this->parent) || empty($this->parent)) return false;
    return ''.$this->parent->getId().'.{{getName}}.'.$this->parentIndex;
}
//    private $pseudoId = false;
//    public function getId(){
//        return $this->pseudoId = ($this->pseudoId)?$this->pseudoId:rand();
//    }
{{/hasPrimaryKeyAttributes}}

public function addToJs($selectionString = ''){
//    if(!$calledFromChild && get_class($this) == '{{../getName}}' && (!$this->loadObject))
        FCVJsObj::add($this, $selectionString);
        //FCVJsObj::addRequire('{{getWorkPackage.getPath}}');
        return $this;
}
{{#isDefaultType}}
public function __toString(){
    return $this->toString();
}
{{/isDefaultType}}