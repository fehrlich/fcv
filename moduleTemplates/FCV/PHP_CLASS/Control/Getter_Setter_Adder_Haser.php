
// <editor-fold defaultstate="collapsed" desc="getter/setter/adder">

public function throwError($msg){
    if(!Debug::getObj()->getThrowErrors()) return "";
    else{
        throw new AttributeDoesNotExist($msg);
    }
}

{{#getAssociationEnds}}
{{^isCardinalityZero}}
{{^isAssociationClass}}
/**
 * @param {{getConnectionClass.getFirstTableClass.getName}}CollectionPrepare $collectionPrepare
 * @return {{getConnectionClass.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}}
 * @throws Exception
 */
public function {{getAssociationFunction}}({{getConnectionClass.getFirstTableClass.getName}}CollectionPrepare $collectionPrepare = null, $onlyRegular = {{#isCardinalitySingle}}false{{/isCardinalitySingle}}{{^isCardinalitySingle}}true{{/isCardinalitySingle}}){
    {{#getOppositeAssociationEnd.getClass.isTableOrParent}}
    if({{#isCardinalitySingle}}{{^getParent.isAssociationClass}}!empty($this->{{getName}}) && {{/getParent.isAssociationClass}}{{/isCardinalitySingle}} 
        {{^isCardinalitySingle}}!empty($this->getId()) && {{/isCardinalitySingle}} 
        (!is_object($this->{{getName}}) || !is_null($collectionPrepare) )) {
        if(is_null($collectionPrepare)) $collectionPrepare = {{getConnectionClass.getFirstTableClass.getName}}CollectionPrepare::create()->selectAll();
        
        $collectionPrepare = $collectionPrepare
            {{#getParent.isAssociationClass}}
            {{#isAssociationClass}}
            //is assoc Class
            {{/isAssociationClass}}
            {{^isAssociationClass}}
            //with assoc Class
            ->set{{getParent.getNameUCFirst}}(
                {{getParent.getNameUCFirst}}CollectionPrepare::create()
                    ->where{{getOppositeAssociationEnd.getConnectionClass.getNameUCFirst}}('=', $this->getId())
            )
            {{/isAssociationClass}}
            {{/getParent.isAssociationClass}}
            {{^getParent.isAssociationClass}}
            //without assoc Class
            {{#getParent.isRelationNM}}
            //is n-m relation
            ->set{{getOppositeAssociationEnd.getNameUCFirst}}(
                {{getOppositeAssociationEnd.getConnectionClass.getFirstTableClass.getNameUCFirst}}CollectionPrepare::create()
                ->where{{getOppositeAssociationEnd.getConnectionClass.getPrimaryKeyAttribute.getName}}('=', $this->getId())
            )
            
            {{/getParent.isRelationNM}}
            {{^getParent.isRelationNM}}
            //no n-m relation
            {{#isCardinalitySingle}}
            //single
            ->where{{getConnectionClass.getPrimaryKeyAttribute.getNameUCFirst}}('=', $this->{{getName}})            
            {{/isCardinalitySingle}}
            {{^isCardinalitySingle}}
            //multi
            ->where{{getOppositeAssociationEnd.getNameUCFirst}}('=', $this->getId())  
            //->where{{getOppositeAssociationEnd.getConnectionClass.getNameUCFirst}}('=', $this->getId())        
            {{/isCardinalitySingle}}
            {{/getParent.isRelationNM}}
            {{/getParent.isAssociationClass}}    
            {{#isCardinalitySingle}}->limit(1){{/isCardinalitySingle}}
        ;
        
        $this->{{getName}} = $onlyRegular ? $collectionPrepare->getRegular() : $collectionPrepare->get();
        
        {{#isCardinalitySingle}}
        if(count($this->{{getName}}) == 1){
            $this->{{getName}} = $this->{{getName}}->current();
        }
        else $this->{{getName}} = null;
        {{/isCardinalitySingle}}
        
        if($this->{{getName}}){
            {{^getOppositeAssociationEnd.isCardinalityZero}}
            {{#getOppositeAssociationEnd.isCardinalitySingle}}
            $this->{{getName}}->set{{getOppositeAssociationEnd.getNameUCFirst}}($this);
            {{/getOppositeAssociationEnd.isCardinalitySingle}}
            {{/getOppositeAssociationEnd.isCardinalityZero}}
        }
        
        {{^getOppositeAssociationEnd.isCardinalityZero}}
        {{/getOppositeAssociationEnd.isCardinalityZero}}
    }elseif(!is_object($this->{{getName}})) $this->{{getName}} = null;
    {{/getOppositeAssociationEnd.getClass.isTableOrParent}}
    if(is_null($this->{{getName}}) || empty($this->{{getName}})){
        $this->throwError('class {{../getName}} with id '.$this->getId().' has no Attributes {{getName}}');
    }
    return $this->{{getName}};
}

public function has{{getNameUCFirst}}({{getConnectionClass.getFirstTableClass.getName}}CollectionPrepare $collectionPrepare = null){

    $throwErrors = Debug::getObj()->getThrowErrors();
    Debug::getObj()->setThrowErrors(true);
    try {
        $this->get{{getNameUCFirst}}($collectionPrepare);
    } catch (EmptyChainException $exc) {
        Debug::getObj()->setThrowErrors($throwErrors);
        return false;
    }
    Debug::getObj()->setThrowErrors($throwErrors);
    return true;
}

/**
 * @param {{getConnectionClass.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}} ${{getConnectionClass.getNameLCFirst}}
 */
public function {{setAssociationFunction}}({{getConnectionClass.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}} ${{getNameLCFirst}} = null){
    
    $this->{{getNameLCFirst}} = ${{getNameLCFirst}};
    {{#isCardinalitySingle}}
    {{#getOppositeAssociationEnd.isCardinalitySingle}}
    {{^getOppositeAssociationEnd.isCardinalityZero}}
    if(!is_null(${{getNameLCFirst}}) && (!$this->{{getNameLCFirst}}->has{{getOppositeAssociationEnd.getNameUCFirst}}() || $this->{{getNameLCFirst}}->get{{getOppositeAssociationEnd.getNameUCFirst}}()->getId() != $this->getId())) $this->{{getNameLCFirst}}->set{{getOppositeAssociationEnd.getNameUCFirst}}($this);
    {{/getOppositeAssociationEnd.isCardinalityZero}}
    {{/getOppositeAssociationEnd.isCardinalitySingle}}
    {{/isCardinalitySingle}}
    return $this;
}
{{/isAssociationClass}}
{{^isCardinalitySingle}}

/**
 * @param {{../getName}} ${{getConnectionClass.getNameLCFirst}}
 */
public function {{addAssociationFunction}}({{getConnectionClass.getName}} ${{getConnectionClass.getNameLCFirst}}){
    {{#getOppositeAssociationEnd.isCardinalitySingle}}
    {{^getOppositeAssociationEnd.isCardinalityZero}}
    if(!is_null(${{getConnectionClass.getNameLCFirst}})){
        ${{getConnectionClass.getNameLCFirst}}->set{{getOppositeAssociationEnd.getNameUCFirst}}($this);
    }
    {{/getOppositeAssociationEnd.isCardinalityZero}}
    {{/getOppositeAssociationEnd.isCardinalitySingle}}
    
    if(!$this->has{{getNameUCFirst}}()){
        $this->{{getName}} = new {{getConnectionClass.getName}}Collection();
    }
    
    $this->{{getName}}->add(${{getConnectionClass.getNameLCFirst}});
    return $this;
}
{{/isCardinalitySingle}}
{{^isAssociationClass}}
{{#getParent.isAssociationClass}}

/**
 * @param {{getParent.getName}}CollectionPrepare $collectionPrepare
 * @return {{getParent.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}}
 * @throws Exception
 */
public function get{{getParent.getName}}({{getParent.getName}}CollectionPrepare $collectionPrepare = null){
    {{#getParent.isTable}}
    if(!empty($this->getId()) && (!is_object($this->{{getParent.getNameLCFirst}}) || !is_null($collectionPrepare))) {
    
        if(is_null($collectionPrepare)) $collectionPrepare = {{getParent.getName}}CollectionPrepare::create()->selectAll();
        
        $this->{{getParent.getNameLCFirst}} = $collectionPrepare
            ->where{{../../../getNameUCFirst}}('=', $this->getId())
            {{#isCardinalitySingle}}->limit(1){{/isCardinalitySingle}}
            ->get()            
        ;
        
        {{^isCardinalitySingle}}       
        foreach($this->{{getParent.getNameLCFirst}} as $con){
            $con->set{{getOppositeAssociationEnd.getConnectionClass.getNameUCFirst}}($this);
        }
        {{/isCardinalitySingle}}       
        
        {{#isCardinalitySingle}}        
        if($this->{{getParent.getNameLCFirst}}->count() > 0){
            $this->{{getParent.getNameLCFirst}} = $this->{{getParent.getNameLCFirst}}->current();
            $this->{{getParent.getNameLCFirst}}->set{{getOppositeAssociationEnd.getConnectionClass.getNameUCFirst}}($this);
        }
        else{
            $this->{{getParent.getNameLCFirst}} = null;
        }
        {{/isCardinalitySingle}}
    }elseif(!is_object($this->{{getParent.getNameLCFirst}})){
        $this->{{getParent.getNameLCFirst}} = null;
    }
    {{/getParent.isTable}}

    if(is_null($this->{{getParent.getNameLCFirst}})){
        $this->throwError('Attribute {{../getNameLCFirst}} not Found in class {{../getName}}');
        //throw new Exception('Attribute {{../getNameLCFirst}} not Found in class {{../getName}}');
    }
    return $this->{{getParent.getNameLCFirst}};
}

/**
 * Check if the Association {{getParent.getName}} exists
 */
public function has{{getParent.getName}}({{getParent.getName}}CollectionPrepare $collectionPrepare = null){
    $throwErrors = Debug::getObj()->getThrowErrors();
    Debug::getObj()->setThrowErrors(true);
    try {
        $this->get{{getParent.getName}}($collectionPrepare);
    } catch (EmptyChainException $exc) {
        Debug::getObj()->setThrowErrors($throwErrors);
        return false;
    }
    Debug::getObj()->setThrowErrors($throwErrors);
    return true;
}

/**
 * Setter for AssociationClass {{getParent.getName}}
 * @param {{getParent.getName}} ${{getNameLCFirst}}
 * @return {{getClass.getName}}
 */
public function set{{getParent.getName}}({{getParent.getNameUCFirst}} ${{getParent.getNameLCFirst}} = null){
    $this->{{getParent.getNameLCFirst}} = ${{getParent.getNameLCFirst}};    
    {{#isCardinalitySingle}}
    if(!$this->{{getParent.getNameLCFirst}}->has{{getOppositeAssociationEnd.getConnectionClass.getNameUCFirst}}() || !$this->{{getParent.getNameLCFirst}}->get{{getOppositeAssociationEnd.getConnectionClass.getNameUCFirst}}()->getId() != $this->getId()){
        $this->{{getParent.getNameLCFirst}}->set{{getOppositeAssociationEnd.getConnectionClass.getNameUCFirst}}($this);
    }
    {{/isCardinalitySingle}}
    
    return $this;
}
{{/getParent.isAssociationClass}}
{{/isAssociationClass}}
{{#isAssociationClass}}

/**
 * @return {{getType.getName}}
 */
public function get{{getType.getName}}(){
    if(is_null($this->{{getType.getNameLCFirst}}) || empty($this->{{getType.getNameLCFirst}})){
        
        $this->throwError('class {{getParent.getName}} with id has no primary attribute Attributes {{getType.getName}}');
    }
    {{#getType.isTableOrParent}}
    if(!is_object($this->{{getType.getNameLCFirst}}) && !empty($this->{{getType.getNameLCFirst}})){
        $this->{{getType.getNameLCFirst}} = {{getType.getName}}::load($this->{{getType.getNameLCFirst}});
    }
    {{/getType.isTableOrParent}}
    return $this->{{getType.getNameLCFirst}};
}

/**
 * @param {{getType.getNameLCFirst}} ${{getType.getNameLCFirst}}
 * @return {{getParent.getName}}
 */
public function set{{getType.getName}}({{getType.getNameLCFirst}} ${{getType.getNameLCFirst}} = null){
    $this->{{getType.getNameLCFirst}} = ${{getType.getNameLCFirst}};    
    return $this;
}


/**
 * Check if the Association {{getType.getName}} exists
 */
public function has{{getType.getName}}($forceLoad = false){
    $throwErrors = Debug::getObj()->getThrowErrors();
    Debug::getObj()->setThrowErrors(true);
    try {
        $this->get{{getType.getName}}($forceLoad);
    } catch (EmptyChainException $exc) {
        Debug::getObj()->setThrowErrors($throwErrors);
        return false;
    }
    Debug::getObj()->setThrowErrors($throwErrors);
    return true;
}
{{/isAssociationClass}}
{{/isCardinalityZero}}
{{/getAssociationEnds}}

//Getter & Setter

public function {{genericGetOperation}}($name, $id = 0){
    if($name == 'id') $name = '{{getPrimaryKeyAttribute.getName}}';
    //dot notation ... house.address
    $name = str_replace('_dot_', '.', $name);
    if(strpos($name, '.')){
        $spl = explode('.', $name, 2);
        $name = $spl[0];
        $subName = $spl[1];
        $firstSplit = $this->get($name);
        return $firstSplit->get($subName);
    }
    //specific id notation ... house.adress[3].id
    if(strpos($name, '[')){
        $spl = explode('[', substr($name,0,-1), 2);
        $name = $spl[0];
        $id = $spl[1];
        $firstSplit = $this->get($name);
        return $firstSplit->getId($id);
    }    
    
    $func = 'get'.ucfirst($name);
    return $id > 0 ? $this->$func($id) : $this->$func();
    
}

public function {{genericHasOperation}}($name, $id = 0){
    if($name == 'id') $name = '{{getPrimaryKeyAttribute.getName}}';
    $throwErrors = Debug::getObj()->getThrowErrors();
    Debug::getObj()->setThrowErrors(true);
    //dot notation ... house.address
    try{
        if(strpos($name, '.') !== false){
            $spl = explode('.', $name, 2);
            Debug::getObj()->setThrowErrors($throwErrors);
            $hasValue = $this->has($spl[0]);
            if($hasValue){
                $firstSplit = $this->get($spl[0]);
                return $firstSplit->has($spl[1]);
            } else{
                return false;
            }
        }
        //specific id notation ... house.adress[3]
        if(strpos($name, '[') !== false){
            $spl = explode('[', substr($name,0,-1), 2);
            $attr = $spl[0];
            $id = $spl[1];
            Debug::getObj()->setThrowErrors($throwErrors);
            return $this->get($attr)->hasId($id);
        }
    } catch (EmptyChainException $ex) {
        Debug::getObj()->setThrowErrors($throwErrors);
        return false;
    }
    
    $func = 'has'.ucfirst($name);
    Debug::getObj()->setThrowErrors($throwErrors);
    return method_exists($this, $func) && $this->$func();
}

public function {{getExistsOperation}}($name, $id = 0){
    if($name == 'id') $name = '{{getPrimaryKeyAttribute.getName}}';
    $throwErrors = Debug::getObj()->getThrowErrors();
    Debug::getObj()->setThrowErrors(true);
    //dot notation ... house.address
    try{
        if(strpos($name, '.') !== false){
            $spl = explode('.', $name, 2);
            Debug::getObj()->setThrowErrors($throwErrors);
            $firstSplitExists = $this->exists($spl[0]) && $this->has($spl[0]);
            return  $firstSplitExists && $this->get($spl[0])->exists($spl[1]);
        }
        //specific id notation ... house.adress[3]
        if(strpos($name, '[') !== false){
            $spl = explode('[', substr($name,0,-1), 2);
            $attr = $spl[0];
            $id = $spl[1];
            Debug::getObj()->setThrowErrors($throwErrors);
            return $this->get($attr)->hasId($id);
        }
    } catch (EmptyChainException $ex) {
        Debug::getObj()->setThrowErrors($throwErrors);
        return false;
    }
    
    if(in_array($name, array('created', 'lastModified', 'createdBy'))){
        Debug::getObj()->setThrowErrors($throwErrors);
        return true;
    }

    $func = 'has'.ucfirst($name);
    Debug::getObj()->setThrowErrors($throwErrors);
    return  method_exists($this, $func);
}


/**
 * @param $name Attribute name
 * @param $value Attribute value
 * @return {{getName}}
 */
public function {{genericSetOperation}}($name, $value){

    if($name == 'id') $name = '{{getPrimaryKeyAttribute.getName}}';
    
    $possibleFields = array(
        'created',
        'createdBy',
        'lastModified',
        {{#getAllConnectedDataTypes}}
        {{^isCardinalityZero}}
        '{{getName}}',
        {{#getParent.isAssociationClass}}
        {{^isAssociationClass}}
        '{{getParent.getNameLCFirst}}',
        {{/isAssociationClass}}
        {{/getParent.isAssociationClass}}
        {{/isCardinalityZero}}
        {{/getAllConnectedDataTypes}}
        {{#isAssociationClass}}
        {{#getAssociationEnds}}
        '{{getClass.getNameLCFirst}}',
        {{/getAssociationEnds}}
        {{/isAssociationClass}}
    );
    if(strpos($name, '.')){
        $spl = explode('.', $name, 2);
        return $this->get($spl[0])->set($spl[1], $value);
    }
    if(strpos($name, '[')){
        return $this->get($name)->set($name, $value);
    }
    
    if(!in_array($name, $possibleFields)) {{#getSpecializes}}return parent::set($name,$value);{{/getSpecializes}}{{^getSpecializes}} throw new \Exception('The Field "'. $name.'" doesnt exists in the class {{getName}}');{{/getSpecializes}}
    $func = 'set'.ucfirst($name);
    $this->$func($value);
    return $this;
}

{{^getSpecializes}}
public function getCreated(){
    return $this->created;
}
public function setCreated($created){
    if(is_string($created)) $created = \fcv\DateTime::create($created);
    $this->created = $created;    
    return $this;
}  

public function getLastModified(){
    return $this->lastModified;
}

public function setLastModified($lastModified){
    if(is_string($lastModified)) $lastModified = \fcv\DateTime::create($lastModified);
    $this->lastModified = $lastModified;
    
    return $this;
}
{{/getSpecializes}}

{{#getAttributes}}
/**
 *
 * @return {{getType.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}}
 * @throws Exception
 */
 {{getVisibility}} function {{getterFunction}}(){
    {{#getType.isClass}}
    {{^getType.isEnum}}
    {{^getType.usesTypeParser}}
    {{#getType.isTableOrParent}}
    if(!is_null( $this->{{getName}}) && !is_object($this->{{getName}})){
        $this->{{getName}} = {{getType.getName}}::load($this->{{getName}});
    }
    {{/getType.isTableOrParent}}
    {{/getType.usesTypeParser}}
    {{#getType.usesTypeParser}}
    if(!is_null( $this->{{getName}}) && !is_object($this->{{getName}})){
        $this->{{getName}} = {{getType.getName}}::parse($this->{{getName}});
    }
    {{/getType.usesTypeParser}}
    if(is_null($this->{{getName}}){{#getType.usesTypeParser}} || ($this->{{getName}} instanceof {{getType.getName}} && $this->{{getName}}->isNull()){{/getType.usesTypeParser}}){
        $this->throwError('class {{getParent.getName}} with id '.$this->getId().' has no Attributes {{getName}}');
    }
    {{/getType.isEnum}}
    {{/getType.isClass}}
    {{^getType.isClass}}
    if(is_null($this->{{getName}})){
        $this->throwError('class {{getParent.getName}} with id '.$this->getId().' has no Attributes {{getName}}');
    }
    {{/getType.isClass}}
    
    return {{#isBoolean}}(boolean) {{/isBoolean}}$this->{{getName}};
 }
 
{{#isClass}}
/**
 *
 * @return int
 */
 {{getVisibility}} function {{getterFunction}}ID(){    
    return !is_null($this->{{getName}}) && is_object($this->{{getName}}) ? $this->{{getName}}->getId() : $this->{{getName}};
 }
 {{/isClass}}

 public function {{hasGetterFunction}}(){
    $throwErrors = Debug::getObj()->getThrowErrors();
    Debug::getObj()->setThrowErrors(true);
    try {
        $this->{{getterFunction}}();
    } catch (EmptyChainException $exc) {
        Debug::getObj()->setThrowErrors($throwErrors);
        return false;
    }
    Debug::getObj()->setThrowErrors($throwErrors);
    return true;
}


/**
 *
 * @param {{getType.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}} ${{getName}}
 * @return {{getParent.getName}}
 */
 {{getVisibility}} function {{setterFunction}}({{#getType}}{{#isClass}}{{^isEnum}}{{getType.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}}{{/isEnum}}{{/isClass}}{{/getType}} ${{getName}}{{#getType}}{{#isClass}}{{^isEnum}} = null{{/isEnum}}{{/isClass}}{{/getType}}){
 
    $this->{{getName}} = ${{getName}};
    {{#getType.usesTypeParser}}
    if(is_null(${{getName}})) $this->{{getName}} = {{getType.getName}}::null();
    {{/getType.usesTypeParser}}

    return $this;
 }
 {{^isCardinalitySingle}}
 /**
  *
  * @param {{getType.getName}} ${{getType.getName}}
  * @return {{getParent.getName}}
  */
  {{getVisibility}} function add{{getNameUCFirst}}{{getType.getName}}(${{getName}}{{getType.getName}}){
     if(!$this->{{getName}}) $this->get{{getNameUCFirst}}();
     $this->{{getName}}->add(${{getName}}{{getType.getName}});
     return $this;
  }

 {{/isCardinalitySingle}}
{{/getAttributes}}

// </editor-fold>
