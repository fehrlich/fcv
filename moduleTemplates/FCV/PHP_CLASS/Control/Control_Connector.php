

protected static $fcvClientFields = [
    {{#getAttributes}}
    {{^isStatic}}
    {{^hasStereoType noClientField}}
    {{^hasStereoType nojs}}
    '{{getName}}' => true,
    {{/hasStereoType nojs}}
    {{/hasStereoType noClientField}}
    {{/isStatic}}
    {{/getAttributes}} 
    {{#isAssociationClass}}
    {{#getAssociationEnds}} 
    {{^isStatic}}
    {{^isCardinalityZero}}
    {{^hasStereoType noClientField}}
    {{#getConnectionClass.isClient}}
    '{{getConnectionClass.getNameLCFirst}}' => true,
    {{/getConnectionClass.isClient}}
    {{/hasStereoType noClientField}}
    {{/isCardinalityZero}}
    {{/isStatic}}
    {{/getAssociationEnds}}
    {{/isAssociationClass}}
    {{^isAssociationClass}}
    {{#getAssociationEnds}} 
    {{^isStatic}}
    {{^isCardinalityZero}}
    {{^getParent.isAssociationClass}}
    {{#isCardinalitySingle}}
    {{#getConnectionClass.isClient}}
    '{{getName}}' => true,
    {{/getConnectionClass.isClient}}
    {{/isCardinalitySingle}}
    {{/getParent.isAssociationClass}}
    {{/isCardinalityZero}}
    {{/isStatic}}
    {{/getAssociationEnds}}
    {{/isAssociationClass}}
];