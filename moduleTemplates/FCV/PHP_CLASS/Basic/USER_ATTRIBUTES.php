
// <editor-fold defaultstate="collapsed" desc="Attribute-Definitions">

{{#isEnum}}//Litearls{{/isEnum}}
{{#list getLiterals}}
const {{value.getNameUppdercase}} = '{{value.getName}}';
{{/list getLiterals}}

{{#getAttributes}}
/**
 * {{&getDocumentation}}
 * @var {{getType.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}}
 */
{{getVisibility}}{{#isStatic}} static{{/isStatic}} ${{getName}}{{#getDefault}} = {{&.}}{{/getDefault}};
const FIELD_{{getNameUppdercase}} = '{{getName}}';
{{/getAttributes}}

//AssociationsEnds
{{#getAssociationEnds}}
{{^isCardinalityZero}}
{{#isAssociationClass}}
/**
 * {{&getDocumentation}}
 * @var {{getConnectionClass.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}}
 */
protected ${{getConnectionClass.getNameLCFirst}};
{{/isAssociationClass}}
{{^isAssociationClass}}

/**
 * {{&getDocumentation}}
 * @var {{getConnectionClass.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}}
 */
protected ${{getName}} = null;
const {{getNameUppdercase}} = '{{getName}}';
{{/isAssociationClass}}
{{#isAssociationClass}}
{{^isCardinalitySingle}}
/**
 * {{&getDocumentation}}
 * @var {{getConnectionClass.getName}}Collection
 */
protected ${{getName}} = null;
const FIELD_{{getNameUppdercase}} = '{{getName}}';
{{/isCardinalitySingle}}
{{/isAssociationClass}}

{{#getParent.isAssociationClass}}
{{^isAssociationClass}}
/**
 * {{&getDocumentation}}
 * @var {{getParent.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}}
 */
{{getVisibility}} ${{getParent.getNameLCFirst}} = null;
const FIELD_{{getParent.getNameUppdercase}} = '{{getParent.getNameLCFirst}}';
{{/isAssociationClass}}
{{/getParent.isAssociationClass}}
{{/isCardinalityZero}}
{{/getAssociationEnds}}

// </editor-fold>
