<?php
class Tree{
    /**
     *
     * @var UMLClass
     */
    private $umlElement;
    /**
     * Tree Structure
     */
    private $treeLft;
    private $treeRgt;
    private $parentAttribute;
    private $hasOwnParentAttribute;
    
    function init(){
        
    }
    
    function on(){
        
    }
    
    function extendUML(){
        $hasOwnParentAttribute = false;
        $addAttributes = array();
        
        $addAttributes[] = new UMLDataType($this->umlElement->getId().'-TreeLft', $this->umlElement->getName().'TreeLft', $this->umlElement);
        $addAttributes[] = new UMLDataType($this->umlElement->getId().'-TreeRgt', $this->umlElement->getName().'TreeRgt', $this->umlElement);
        
        
        if(!$this->hasOwnParentAttribute()){
            $addAttributes[] = $this->getOwnParentAttribute();
        }
        
        foreach($addAttributes as $attribute){
            $this->umlElement->addAttribute($attribute);
        }
    }
    
    function getOwnParentAttribute(){
        if($this->parentAttribute != null) return $this->parentAttribute;
        $dataTypes = array_merge($this->umlElement->getAttributes(),$this->umlElement->getAssociationEnds());
        foreach($dataTypes as $attribute){
//        foreach($this->umlElement->getAttributes() as $attribute){
            if($attribute->hasStereoType('parent')){
                $this->parentAttribute = $attribute;
                $this->hasOwnParentAttribute = true;
                return $attribute;
            }
        }
        $this->hasOwnParentAttribute = false;
        return new UMLDataType($this->umlElement->getId().'-TreeParent', $this->umlElement->getName().'TreeParent', $this->umlElement);            ;
    }
    
    function hasOwnParentAttribute(){
        if($this->hasOwnParentAttribute == null){
            $this->getOwnParentAttribute();
        }
        
        return $this->hasOwnParentAttribute;
    }
    
    //onSave
    function onSave($fields){
        $fields['parent'] = '';
    }
}