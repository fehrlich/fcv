<?php
namespace {{#pathToNamespace}}{{getWorkPackage.getPath}}{{/pathToNamespace}};

{{#uniqueLines}}
use fcv\database\QueryBuilder;
use fcv\traits\StaticEventAction;
use fcv\CollectionPrepare;
use fcv\FCVJsObj;
use fcv\classTraits\CollectionPrepare as collectionPrepareTrait;
//From AssocEnds
{{#getAssociationEnds}}
use {{#substr 1}}{{getConnectionClass.getNamePath}}CollectionPrepare{{/substr 1}};
{{#isAssociationClass}}
use {{#substr 1}}{{getType.getNamePath}}CollectionPrepare{{/substr 1}};
{{/isAssociationClass}}
{{#getParent.isAssociationClass}}
{{^isAssociationClass}}
use {{#substr 1}}{{getParent.getNamePath}}CollectionPrepare{{/substr 1}};
{{/isAssociationClass}}
{{/getParent.isAssociationClass}}
{{/getAssociationEnds}}


//From Attributes
{{#getAttributes}}
{{#getType.isClass}}
use {{#substr 1}}{{getType.getNamePath}}CollectionPrepare{{/substr 1}};
{{/getType.isClass}}
{{/getAttributes}}

//From Implementations
{{#getImplements}}
use {{#substr 1}}{{getNamePath}}CollectionPrepare{{/substr 1}};

{{/getImplements}}

//From Specialisations
{{#getSpecializes table}}
use {{#substr 1}}{{getNamePath}}CollectionPrepare{{/substr 1}};

{{/getSpecializes table}} 

{{/uniqueLines}}


class {{getName}}CollectionPrepare {{#getSpecializes table}} extends {{getName}}CollectionPrepare{{/getSpecializes table}}{

    use collectionPrepareTrait;
    
    protected $tableName;
    protected $mysqlTable = '{{getName}}';
    protected $mysqlEditTable = '{{getName}}';
    protected $tableNameAlias = '';
    protected $connectionLogic = 'and';

    /**
     * @var QueryBuilder
     */
    protected $query;
    protected $objectMap = array();

    protected $primaryKeyIndex = array();
    
    protected $triggerGenericEvents = false;
    
    protected $wheres = [];
    protected $orderBy = '';
    protected $limitStr = '';
    protected $joins = [];
    protected $columns = [];
    
    function getPrimaryKeyNames(){
        return {{getName}}::$fcvPrimaryKeyAttributes;
    }

    public function __construct(){
        $this->tableName = '{{getName}}';
        $this->query = QueryBuilder::create();
        $this->query->setFetchModeNum();
    }

    /**
     * @return {{getName}}CollectionPrepare
     */
    public static function create(){
        return new {{getName}}CollectionPrepare();
    }
    
    public function setTriggerGenericEvents($triggerGenericEvents){
        $this->triggerGenericEvents = $triggerGenericEvents;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrimaryKeyIndex(){
        return $this->primaryKeyIndex;
    }

    /**
     * @return array
     */
    public function getObjectMap(){
        return $this->objectMap;
    }

    /**
     * @return QueryBuilder
     */
    public function getQuery(){
        return $this->query;
    }

    /**
     * @return string
     */
    public function getConnectionLogic(){
        return $this->connectionLogic;
    }

    /**
     * @return {{getName}}CollectionPrepare
     */
    public function setConnectionLogic($connectionLogic){
        $this->connectionLogic = $connectionLogic;
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getTableName(){
        return $this->tableName;
    }

    /**
     * @return {{getName}}CollectionPrepare
     */
    public function setTableName($tableName){
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableNameAlias(){
        return ($this->tableNameAlias != '')?$this->tableNameAlias:$this->tableName;
    }
    
    /**
     * 
     * @return string
     */
    public function getSelector(){
        return $this->getTableNameAlias();
    }

    /**
     * @return {{getName}}CollectionPrepare
     */
    public function setTableNameAlias($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias;
        return $this;
    }

    /**
     * @return string
     */
    public function setTableNameAliasPrepend($tableNameAlias){
        $this->tableNameAlias = $tableNameAlias.(($this->tableNameAlias == '')?$this->tableName:$this->tableNameAlias);
        return $this;
    }
    
    /**
      * Add a basic where clause to the query.
      *
      * @param  string  $column
      * @param  string  $operator
      * @param  mixed   $value
      * @param  string  $boolean
      * @return {{getName}}CollectionPrepare
      *
      */
    public function where($column, $operator = null, $value = null, $boolean = 'and'){
        
        if($this->prepared){
            throw new \Exception("Can't modify an allready prepared query!");
        }
        
        $triggerEvent = $this->triggerGenericEvents;
        
        $this->wheres[] = array($column, $operator, $value, $boolean);
        
        if(!is_string($column)){
            $this->query->where($column);
        }elseif(!is_string($operator)){
            $this->query->where($operator);
        }else{
            if(strpos($column, '.') !== false){
                $spl = explode('.', $column, 2);
                $func = 'get'.ucfirst($spl[0]);
                $sub = $this->$func();
                $triggerEvent = false;
                $sub->where($spl[1], $operator, $value, $boolean);
            }else{
                $isCustomOp = QueryBuilder::customWhereOperator($this->query,$column,$operator, $value, $boolean);
                if(!$isCustomOp){
                    $this->query->where($column, $operator, $value, $boolean);                      
                }
            }
        }
        
        if($triggerEvent){
            CollectionPrepare::trigger('where', {{getName}}CollectionPrepare::class, $column, $operator, $value, $boolean);
        }
        
        return $this;
    }
    
    /**
      * Add a basic where clause to the query.
      *
      * @param  string  $column
      * @param  string  $operator
      * @param  mixed   $value
      * @param  string  $boolean
      * @return {{getName}}CollectionPrepare
      *
      */
    public function orWhere($column, $operator = null, $value = null, $logicConnector = 'or'){
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('where', {{getName}}CollectionPrepare::class, $column, $operator, $value, $logicConnector);
        }
        
        $this->wheres[] = array($column, $operator, $value, $logicConnector);
        
        if(!is_string($column)){
            $this->query->orWhere($column);
        }else{
            if(strpos($column, '.') !== false){
                $spl = explode('.', $column, 2);
                $func = 'get'.ucfirst($spl[0]);
                $sub = $this->$func();
                $sub->orWhere($spl[1], $operator, $value, $logicConnector);
            }else{
                $isCustomOp = QueryBuilder::customWhereOperator($this->query,$column,$operator, $value, $logicConnector);
                if(!$isCustomOp){
                    $this->query->orWhere($column, $operator, $value, $logicConnector);                      
                }
            }
        }
        return $this;
    }

    /**
     * @return {{getName}}CollectionPrepare
     */
    public function whereRaw($where, $binding = array(), $boolean = 'and'){
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('whereraw', {{getName}}CollectionPrepare::class);
        }
        $this->query->whereRaw($where, $binding, $boolean);
        return $this;
    }
    {{^getSpecializes}}
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function whereCreated($operation, $value = false, $logicConnector = 'and'){
        if(!is_string($operation) && $value === false){
            $this->where($operation);
        }else{
            $this->where('created', $operation, $value, $logicConnector);
        }
        return $this;
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function whereId($operation, $value = false, $logicConnector = 'and'){
        {{#getPrimaryKeyAttributes}}
        $this->where{{getNameUCFirst}}($operation, $value, $logicConnector);
        {{/getPrimaryKeyAttributes}}
        return $this;
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function orWhereCreated($operation, $value = false){
        if(!is_string($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('created', $operation, $value);
        }
        return $this;
    }
    
    /**
     * selects alle primary keys and updates the primaryKeyIndex, that is used
     * to group the data later in parse section
     * @return $this
     */
    public function selectId(){
        if(empty($this->primaryKeyIndex)){
            $primKeyIndex = 0;
            {{#getPrimaryKeyAttributes}}
            $this->primaryKeyIndex[] = $primKeyIndex++;
            $this->select{{getNameUCFirst}}();
            {{/getPrimaryKeyAttributes}}
        }
        return $this;
    }
    
    /**
     * 
     * @return boolean true if the query has more then 0 columns selected
     */
    function hasSelects(){
        return isset($this->query->columns) && count($this->query->columns) > 0;
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function select($column, $multi = false, $class = ''){
        if($this->prepared){
            throw new \Exception("Can't modify an allready prepared query!");
        }
        if(!in_array($column, $this->columns)){
            if($this->triggerGenericEvents){
                CollectionPrepare::trigger('select', {{getName}}CollectionPrepare::class, $column);
            }

            if(!isSet($this->query->columns)){
                $this->query->columns = array();
                $this->selectId();
            }
            $this->objectMap[] = array(
                'class' => $class,
                'field' => $column,
                'multi' => $multi
            );
            $this->query->addSelect($column);
            $this->columns[] = $column;
        }
        return $this;
    }
    /**
     * you can map fields to the resulting objects directly with a given select
     * there is no need for "as fieldname" because the mapping is index based
     * @return {{getName}}CollectionPrepare
     */
    public function customSelect($field, $rawSelect){
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('select', {{getName}}CollectionPrepare::class, $field);
        }
                
        $this->objectMap[] = array(
            'class' => '',
            'field' => $field,
            'multi' => false
        );
        if(!isSet($this->query->columns)) $this->query->columns = array();
        $this->query->selectRaw($rawSelect);
        return $this;
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function distinct(){
        $this->query->distinct();
        return $this;
    }
    
    public static function getDistinctValues($fields, $query = false){       
        $q = $query ? $query : QueryBuilder::create();
        $q
            ->from("{{getName}}")
            ->select($fields)
            ->distinct();
        
        return $q->get();
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function selectCreated(){
        return $this->select('created');
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function whereLastModifiedTime($operation, $value = false, $logicConnector = 'and'){
        return $this->whereLastModified($operation, $value, $logicConnector);
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function orWhereLastModifiedTime($operation, $value = false){
        return $this->orWhereLastModified($operation, $value);
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function whereLastModified($operation, $value = false, $logicConnector = 'and'){
        if(!is_string($operation) && $value === false){
            $this->where($operation);
        }else{
            $this->where('lastModified', $operation, $value, $logicConnector);
        }
        return $this;
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function orWhereLastModified($operation, $value = false){
        if(!is_string($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('lastModified', $operation, $value);
        }
        return $this;
    }
    
    public function selectLastModified(){
        return $this->select('lastModified');
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function selectLastModifiedTime(){
        return $this->selectLastModified();
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function whereCreatedBy($operation, $value = false, $logicConnector = 'and'){
        if(!is_string($operation) && $value === false){
            $this->where($operation);
        }else{
            $this->where('createdBy', $operation, $value, $logicConnector);
        }
        return $this;
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function orWhereCreatedBy($operation, $value = false){
        if(!is_string($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('createdBy', $operation, $value);
        }
        return $this;
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function selectCreatedBy(){
        return $this->select('createdBy');
    }
   
    {{^hasActive}}
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function whereSubsActive(){
        return $this;
    }
    {{/hasActive}}
    
    {{#hasActive}}
    private $activeSpecified = false;
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function whereActive($operation, $value = false, $logicConnector = 'and'){
        $this->activeSpecified = true;
        if(!is_string($operation) && $value === false){
            $this->where($operation);
        }else{
            $this->where('active', $operation, $value, $logicConnector);
        }
        return $this;
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function whereSubsActive(){
        return $this->where(function($q){
            $q->whereNull('active');
            $q->orWhere('active', '=', '1');
        });
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function orWhereActive($operation, $value = false){
        $this->activeSpecified = true;
        if(!is_string($operation) && $value === false){
            $this->query->orWhere($operation);
        }else{
            $this->query->orWhere('active', $operation, $value);
        }
        return $this;
    }
    
    /**
     * @return {{getName}}CollectionPrepare
     */
    public function selectActive(){
        return $this->select('active');
    }
    {{/hasActive}}
    {{/getSpecializes}}
    
    {{#getAttributes}}
    {{^isStatic}}

    /**
     *
     * @param mixed $operation Operationstring or Function for nested where
     * @param string $value valuestring
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function where{{getNameUCFirst}}($operation, $value = false, $logicConnector = 'and'){
        $this->where('{{getName}}', $operation, $value, $logicConnector);
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function orWhere{{getNameUCFirst}}($operation, $value = false){
        $this->orWhere('{{getName}}', $operation, $value);
        return $this;
    }

    /**
     *
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function select{{getNameUCFirst}}(){
        return $this->select('{{getName}}', {{^isCardinalitySingle}}true{{/isCardinalitySingle}}{{#isCardinalitySingle}}false{{/isCardinalitySingle}}, '{{../getName}}');
    }
    {{#isClass}}
    
//    private ${{getName}};
    /**
     * @var {{getType.getName}}CollectionPrepare
     * @return {{../getName}}CollectionPrepare
     */
//    public function set{{getNameUCFirst}}({{getType.getName}}CollectionPrepare ${{getName}}){
//       $this->{{getName}} = ${{getName}};
//       ${{getName}}->setTableNameAliasPrepend($this->getTableNameAlias());
//       return $this;
//    }

    /**
     * @var {{getType.getName}}CollectionPrepare
     * @return {{getType.getName}}CollectionPrepare
     */
    public function  get{{getNameUCFirst}}(){
       if(!isset($this->{{getName}})){
           $colPrep = {{getType.getName}}CollectionPrepare::create();
           $colPrep->setTriggerGenericEvents($this->triggerGenericEvents);
           $this->set{{getNameUCFirst}}($colPrep);
       }
       return $this->{{getName}};
    }
    {{/isClass}}
    
    {{/isStatic}}
    {{/getAttributes}}
    {{#getAssociationEnds}}
    {{^isStatic}}
    {{^isCardinalityZero}}
    /**
     * @var {{getType.getName}}CollectionPrepare
     */
    protected ${{getNameLCFirst}};
    
    /**
     * AssociationEnd setter for all
     * @var {{getConnectionClass.getName}}CollectionPrepare
     * @return {{#isAssociationClass}}{{getParent.getName}}{{/isAssociationClass}}{{^isAssociationClass}}{{getClass.getName}}{{/isAssociationClass}}CollectionPrepare
     */
    public function {{setAssociationFunction}}({{getConnectionClass.getName}}CollectionPrepare ${{getNameLCFirst}}, $logicConnector = false){
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('set', {{#isAssociationClass}}{{getParent.getName}}{{/isAssociationClass}}{{^isAssociationClass}}{{getClass.getName}}{{/isAssociationClass}}CollectionPrepare::class, '{{getNameLCFirst}}');
        }
        $this->{{getNameLCFirst}} = ${{getNameLCFirst}};
        ${{getNameLCFirst}}->setTableNameAliasPrepend($this->getTableNameAlias().'{{getNameUCFirst}}');
        if($logicConnector) ${{getNameLCFirst}}->setConnectionLogic($logicConnector);
        return $this;
    }

    /**
     * @var {{getConnectionClass.getName}}CollectionPrepare
     * @return {{getConnectionClass.getName}}CollectionPrepare
     */
    public function {{getAssociationFunction}}(){
       if(!isset($this->{{getNameLCFirst}})){
           $colPrep = {{getConnectionClass.getName}}CollectionPrepare::create();
           $colPrep->setTriggerGenericEvents($this->triggerGenericEvents);
           
           $this->{{getNameLCFirst}} = $colPrep;
           $this->{{getNameLCFirst}}->setTableNameAliasPrepend($this->getTableNameAlias().'{{getNameUCFirst}}');
       }
       return $this->{{getNameLCFirst}};
    }
    
    
    {{#isCardinalitySingle}}
        
    
    /**
     * AssociationEnd selector for single data
     * @return {{getClass.getName}}CollectionPrepare
     */
    {{getVisibility}} function select{{getNameUCFirst}}(){
        return $this->select('{{getName}}', {{^isCardinalitySingle}}true{{/isCardinalitySingle}}{{#isCardinalitySingle}}false{{/isCardinalitySingle}}, '{{../getName}}');
    }
    
    /**
     * AssociationEnd where for single data
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function where{{getNameUCFirst}}($operation, $value = false, $logicConnector = 'and'){
        $this->where('{{getName}}', $operation, $value, $logicConnector);
        return $this;
    }

    /**
     * 
     * @param mixed operation string or nested function
     * @param string $value
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function orWhere{{getNameUCFirst}}($operation, $value = false){
        $this->orWhere('{{getName}}', $operation, $value);
        return $this;
    }
    {{/isCardinalitySingle}}
    
    
//    {{!DUPLICATE FOR Assoc Classes with multiCaridnality}}
    {{#isAssociationClass}}
        
    
    /**
     * AssociationEnd for Associationclasses
     * @var {{getType.getName}}CollectionPrepare
     * @return {{getParent.getName}}CollectionPrepare
     */
    public function set{{getType.getName}}({{getType.getName}}CollectionPrepare ${{getType.getNameLCFirst}}, $logicConnector = false){
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('set', {{getParent.getName}}CollectionPrepare::class, '{{getType.getNameLCFirst}}');
        }
        $this->{{getType.getNameLCFirst}} = ${{getType.getNameLCFirst}};
        ${{getType.getNameLCFirst}}->setTableNameAliasPrepend($this->getTableNameAlias().'{{getType.getName}}');
        if($logicConnector) ${{getType.getNameLCFirst}}->setConnectionLogic($logicConnector);
        return $this;
    }
    
    /**
     * AssociationEnd for Associationclasses
     * @var {{getType.getName}}CollectionPrepare
     * @return {{getParent.getName}}CollectionPrepare
     */
    public function get{{getType.getName}}(){        
        if(!isset($this->{{getType.getNameLCFirst}})){
            $colPrep = {{getType.getName}}CollectionPrepare::create();
            $colPrep->setTriggerGenericEvents($this->triggerGenericEvents);
            $this->set{{getType.getNameUCFirst}}($colPrep);
        }
        return $this->{{getType.getNameLCFirst}};
    }
    {{/isAssociationClass}}

    {{!END DUPLICATE FOR Assoc Classes}}

    {{#getParent.isAssociationClass}}
    {{^isAssociationClass}}

    /**
     * AssociationsEnds setter for an external AssociationClass
     * @var {{getParent.getName}}CollectionPrepare
     */
    protected ${{getParent.getNameLCFirst}};
    /**
     * @var {{getConnectionClass.getName}}CollectionPrepare
     * @return {{getClass.getName}}CollectionPrepare
     */
    public function set{{getParent.getName}}({{getParent.getName}}CollectionPrepare ${{getParent.getNameLCFirst}}, $logicConnector = false){
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('set', {{getClass.getName}}CollectionPrepare::class, '{{getParent.getNameLCFirst}}');
        }
        $this->{{getParent.getNameLCFirst}} = ${{getParent.getNameLCFirst}};
        ${{getParent.getNameLCFirst}}->setTableNameAliasPrepend($this->getTableNameAlias().'{{getParent.getName}}');
        if($logicConnector) ${{getParent.getNameLCFirst}}->setConnectionLogic($logicConnector);
        return $this;
    }
    
    /**
     * @return {{getParent.getName}}CollectionPrepare
     */
    public function get{{getParent.getName}}(){
        if(!isset($this->{{getParent.getNameLCFirst}})){
            $colPrep = {{getParent.getName}}CollectionPrepare::create();
            $colPrep->setTriggerGenericEvents($this->triggerGenericEvents);
            $this->set{{getParent.getNameUCFirst}}($colPrep);
        }
        return $this->{{getParent.getNameLCFirst}};
    }
        
    {{/isAssociationClass}}
    {{#isAssociationClass}}
        
    //is AssocClass
    
    /**
     *  AssociationsEnds selector for an this AssociationClass
     * @return {{getParent.getName}}CollectionPrepare
     */
    public function select{{getType.getName}}(){
        return $this->select('{{getType.getNameLCFirst}}', false, '{{../getName}}');
    }
    
    public function where{{getType.getName}}($operation, $value = false, $logicConnector = 'and'){
        $this->where('{{getType.getNameLCFirst}}', $operation, $value, $logicConnector);
        return $this;
    }

    /**
     *
     * @param mixed operation string or nested function
     * @param string $value
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function orWhere{{getType.getName}}($operation, $value = false){
        $this->orWhere('{{getType.getNameLCFirst}}', $operation, $value);
        return $this;
    }   
    
    {{/isAssociationClass}}
    {{/getParent.isAssociationClass}}
    

//    {{getVisibility}} function select{{getNameUCFirst}}(){
//        $this->objectMap[] = array(
//            'class' => '{{../getName}}',
//            'field' => '{{getName}}',
//            'multi' => true
//        );
//        if(!isSet($this->query->columns)) $this->query->columns = array();
//        $this->query->addSelect('getName');
//        return $this;
//    }
//    {{getVisibility}} function where{{getNameUCFirst}}($operation, $value = false){
//        if(is_callable($operation) && $value === false){
//            $this->query->where($operation);
//        }else{
//            $this->query->where('{{getName}}', $operation, $value);
//        }
//        return $this;
//    }
    {{/isCardinalityZero}}
    {{/isStatic}}
    {{/getAssociationEnds}}

    {{#getAttributes}}
    {{^isStatic}}
    {{#getType.isClass}}

    /**
     * @var {{getType.getName}}CollectionPrepare
     */
    protected ${{getName}};
    /**
     *
     * @param {{getType.getName}}CollectionPrepare ${{getName}}
     * @return {{getParent.getName}}CollectionPrepare
     */
    {{getVisibility}} function {{setterFunction}}({{getType.getName}}CollectionPrepare ${{getName}}, $logicConnector = false){
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('set', {{getParent.getName}}CollectionPrepare::class, '{{getName}}');
        }
        $this->{{getName}} = ${{getName}};
        ${{getName}}->setTableNameAliasPrepend($this->getTableNameAlias().'{{getNameUCFirst}}');
        if($logicConnector) ${{getName}}->setConnectionLogic($logicConnector);
        return $this;
    }
    {{/getType.isClass}}
    {{/isStatic}}
    {{/getAttributes}}
    
    /**
     * selects all defined fields from the table
     * @return {{getName}}CollectionPrepare
     */
    public function selectAll(){
        
        $this->selectId();
        {{#getSpecializes}}parent::selectAll();{{/getSpecializes}}
        
        //default
        {{^getSpecializes}}
        $this->selectCreated();
        $this->selectCreatedBy();
        $this->selectLastModified();
        {{#hasActive}}
        {{^isPrimaryKey}}
        $this->selectActive();
        {{/isPrimaryKey}}
        {{/hasActive}}
        {{/getSpecializes}}
        
        //attributes
        {{#getAttributes}}
        {{^isStatic}}
        {{^hasStereoType NoDBField}}
        {{^isPrimaryKey}}
        $this->select{{getNameUCFirst}}();
        {{/isPrimaryKey}}
        {{/hasStereoType NoDBField}}
        {{/isStatic}}
        {{/getAttributes}}
        
        //assocEnds
        {{#getAssociationEnds}}
        {{^isStatic}}
        {{^isCardinalityZero}}
        {{^getParent.isAssociationClass}}
        {{#isCardinalitySingle}}
        {{^hasStereoType NoDBField}}
        {{^isPrimaryKey}}
        $this->select{{getNameUCFirst}}();
        {{/isPrimaryKey}}
        {{/hasStereoType NoDBField}}
        {{/isCardinalitySingle}}
        {{/getParent.isAssociationClass}}
        {{#isAssociationClass}}
        {{^isPrimaryKey}}
        $this->select{{getType.getName}}(); //from assocClass
        {{/isPrimaryKey}}
        {{/isAssociationClass}}
        {{/isCardinalityZero}}
        {{/isStatic}}
        {{/getAssociationEnds}}
        return $this;
    }
    
    /**
     * adds a raw order by statement
     * @return {{getName}}CollectionPrepare
     */
    public function orderBy($column){
        $this->orderBy = $column;
        $this->query->orderByRaw($column);
        return $this;
    }
    
    /**
     * adds a raw order by statement
     * @return {{getName}}CollectionPrepare
     */
    public function orderByRaw($orderBy){
        $this->orderBy = $orderBy;
        $this->query->orderByRaw($orderBy);
        return $this;
    }
    
    /**
     * adds a limit statement
     * @return {{getName}}CollectionPrepare
     */
    public function limit($limit){
        $this->limitStr = $limit;
        $this->query->limit($limit);
        return $this;
    }
    
    /**
     * prepares the query for execution
     * @return {{getName}}CollectionPrepare
     */
    public function prepareQuery({{#hasSpecializes}}$prepareParent = true{{/hasSpecializes}}){
        $this->prepareJoins({{#hasSpecializes}}$prepareParent{{/hasSpecializes}});
        $this->query->from = $this->mysqlTable.(($this->tableNameAlias == '')?'':' AS '.$this->tableNameAlias);
        QueryBuilder::appendWhereWithTable($this->query, $this->getTableNameAlias());
        return $this;
    }
    
    /**
     * returns the sql statement
     * @return string
     */
    public function toSql(){
        $this->prepareQuery();
        return fillSqlWithBindings($this->query->toSql(), $this->getQuery()->getBindings()); //TODO: include dependency
    }
    
    private function getWheres($rawWheres = false){
        if(!$rawWheres){
            $rawWheres = $this->query->wheres;
        }
        
        $wheres = array();
        
        if(is_array($rawWheres)){
            foreach($rawWheres as $where){
                if($where['type'] == 'Nested'){
                    $wheres = array_merge($wheres, $this->getWheres($where['query']->wheres));
                }else{
                    $wheres[] = $where;
                }
            }
        }
        return $wheres;
    }
    
    public function having($column, $operator = null, $value = null, $boolean = 'and'){
        $this->query->having($column, $operator, $value, $boolean);
        return $this;
    }
    
    public function havingRaw($sql, array $bindings = array(), $boolean = 'and'){
        $this->query->havingRaw($sql, $bindings, $boolean);
        return $this;
    }
    
    public function selectRaw($expression, array $bindings = array(), $field = ''){
        $this->query->selectRaw($expression, $bindings);
        
        $this->objectMap[] = array(
            'class' => '{{../getName}}',
            'field' => $field,
            'multi' => false
        );
        
        return $this;
    }
    
    /**
     * gets all objects from a select query and use all where data to perform a
     * rating of the results
     * @param int $minCount
     * @return ObjektCollection
     */
    public function searchQueryGet($minWordCount = 0){
        {{#hasActive}}
        //only select Active Elements if its not specific set otherwise
        if(!$this->activeSpecified  && count($this->query->wheres) > 0){
            $this->whereActive('=', 1);
        }
        {{/hasActive}}
        
        $this->prepareQuery();
        $checkBools = array();
        $checkBoolsValues = array();
        
        $flattenWheres = $this->getWheres();
        
        foreach($flattenWheres as $where){
            $checkBools[] = $where['column'].' '.$where['operator'].' ?';
            $checkBoolsValues[] = $where['value'];
        }
        $whereStatement = 'COALESCE(('.implode('), 0) + COALESCE((', $checkBools).'), 0) AS searchValue';
        $this->query->selectRaw($whereStatement, $checkBoolsValues);
        if($minWordCount > 0){
            $this->query->having('searchValue', '>=', $minWordCount);
        }
        
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('get', KontaktCollectionPrepare::class);
        }
        
        $rows = $this->query->get();
        return $this->parseDbRows($rows, $this->getObjectMap());
    }
    
    private $rawData = array();
    /**
     * gets all objects from a select query
     * @return {{getName}}Collection
     */
    public function get(){
        static::trigger("get", $this);
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('get', {{getName}}CollectionPrepare::class);
        }
        $this->prepareQuery();
        $this->rawData = $this->query->get();
        
        return $this->parseDbRows($this->rawData, $this->getObjectMap());
    }
    
    public function getRawData(){
        return $this->rawData;
    }
    
    /**
     * gets all objects from a select query
     * @return {{getName}}Collection
     */
    public function getSimpleData(){
        static::trigger("get", $this);
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('get', {{getName}}CollectionPrepare::class);
        }
        $this->prepareQuery();
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        $rows = $this->query->get();
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_NUM);
        return $rows;
    }
    
    {{^getSpecializes}}
    /**
     * gets all non active results (if active if specified else its alias for get)
     * @return {{getName}}Collection
     */
    public function getRegular(){
        {{#hasActive}}
        //only select Active Elements if its not specific set otherwise
        if(!$this->activeSpecified){ //  && count($this->query->wheres) > 0
            $this->whereActive('=', 1);
        }
        {{/hasActive}}
        return $this->get();
    }
    {{/getSpecializes}}
    
    /**
     * @return int 
     */
    public function count($columns = '*'){
        
        {{#hasActive}}
        //only select Active Elements if its not specific set otherwise
        if(!$this->activeSpecified){ //  && count($this->query->wheres) > 0
            $this->whereActive('=', 1);
        }
        {{/hasActive}}
        static::trigger("get", $this);
        if($this->triggerGenericEvents){
            CollectionPrepare::trigger('get', {{getName}}CollectionPrepare::class);
        }
        static::trigger("count", $this);
        
        $this->prepareQuery();
        
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_ASSOC);
        $count = $this->query->count($columns);
        $this->query->getConnection()->setFetchMode(\PDO::FETCH_NUM);
        return $count;
    }
    
    /**
     * delete all elements with the WHERE clausel
     * @return type
     */
    public function delete(){
        if(static::trigger("delete", $this)){
            {{#hasSpecializes}}
            $this->mysqlTable = "{{getName}}_sub";
            $this->tableName = "{{getName}}_sub";
            {{/hasSpecializes}}
            
            $this->prepareQuery({{#hasSpecializes}}false{{/hasSpecializes}});
            
            return $this->query->delete();
        }
    }
    
    
    public function update($array){
        if(static::trigger("update", $this, $array) !== false){
            $this->prepareQuery();
            return $this->query->update($array);
        }
    }
    
    public function insert($array){
        if(static::trigger("insert", $this, $array) !== false){
            $this->prepareQuery();
            return $this->query->insert($array);
        }
    }
    
    public function insertGetId($array){
        if(static::trigger("insert", $this, $array) !== false){
            $this->prepareQuery();
            return $this->query->insertGetId($array);
        }
    }

    /**
     * parse all return rows from sql
     */
    private function parseDbRows($rows, $map){
        $objsArray = array();
        $objs = {{getName}}Collection::create();
        $hasPrimaryKeyIndex = count($this->primaryKeyIndex) > 0;
        //build assoc Array with all data
        foreach($rows as $row){
            
            if(!$hasPrimaryKeyIndex){
                $objsArray[] = $this->parseDbRow($row, $map, array());                
            }else{
                //generate unique identifier with primary keys
                $primaryFields = array();
                foreach($this->primaryKeyIndex as $keyIndex){
                    $primaryFields[] = $row[$keyIndex];                
                }
                $primaryField = implode(".", $primaryFields);

                //extend object if allready exists
                $initObj = (isset($objsArray[$primaryField]))?$objsArray[$primaryField]:array();
                $objsArray[$primaryField] = $this->parseDbRow($row, $map, $initObj);
            }
        }
        
        //convert array to real objects (subobjects are generated in constructors)
        foreach($objsArray as $dataArray){
            $objs->add({{getName}}::createFromArray($dataArray));
        }
        
        {{getName}}Collection::trigger('afterInit', $objs);
        
        return $objs;
    }
    
    
    /**
     * iterates through all indexes fields by a given map and returns the new index,
     * without processing the data
     * @param type $index starting index
     * @param type $map map for the processing
     * @return type returns new index after processing
     */
    private function calculateIndex($index, $map){
        foreach($map as $m){
            //check if association
            if(isset($m['map'])){
                //check if cardinality is multi
                $index = $this->calculateIndex($index, $m['map']);
            }
            else{
                $index++;
            }
        }
        return $index;
    }

    /**
     * parse one row into Object and extend existing ones
     * index is present through iterations
     */
    private function parseDbRow($row, $map, $objsData = array(), &$index = 0){
        foreach($map as $m){
            //check if association
            if(isset($m['map'])){
                //check if cardinality is multi
                if($m['multi']){
                    if(!isset($objsData[$m['field']])) $objsData[$m['field']] = array();
                    //get primaryKey ob subobject
                    if(count($m['primaryKeyIndex']) != 0){
                        $ids = array();
                        foreach($m['primaryKeyIndex'] as $keyIndex){
                            $ids[] = $row[$index+$keyIndex];
                        }
//                        $id = $row[$index+$m['primaryKeyIndex']];
                        $id = implode('.', $ids);
//                        
                        if(empty($id)){
                            //only calculate new index if the current data for the map is empty
                            $index = $this->calculateIndex($index, $m['map']);
                        }else{
                            //extend object if allready exists
                            $initObj = (isset($objsData[$m['field']][$id]))?$objsData[$m['field']][$id]:array();
                            //add/extend object with id
                            $objsData[$m['field']][$id] = $this->parseDbRow($row, $m['map'], $initObj, $index);
                        }
                    }else{
                        $objsData[$m['field']][] = $this->parseDbRow($row, $m['map'], array(), $index);
                    }
                }else{
                    $preIndex = $index;
                    $currentObjectIsNull = false;
                    foreach($m['primaryKeyIndex'] as $idIndex){
                        if(!$row[$idIndex+$preIndex]) $currentObjectIsNull = true;                        
                    }
                    
                    if($currentObjectIsNull){
                        $index = $this->calculateIndex($index, $m['map']);
                    }else{
                        $initSubData = isset($objsData[$m['field']]) && is_array($objsData[$m['field']]) ? $objsData[$m['field']] : array();
                        $objsData[$m['field']] = $this->parseDbRow($row, $m['map'], $initSubData, $index);
//                        foreach($m['primaryKeyIndex'] as $idIndex){
//                            if(!$row[$idIndex+$preIndex]) $objsData[$m['field']] = null;                        
//                        }
                    }
//                    if(!$row[$m['primaryKeyIndex']+$preIndex]) $objsData[$m['field']] = null;
                }
            }
            else{
                $objsData[$m['field']] = $row[$index];
                $index++;
            }
        }
        return $objsData;
    }
    
    private $prepared = false;
    private $preparedAttributes = [];

    /**
     * create joins depending on associations
     * @return {{../getName}}CollectionPrepare
     */
    public function prepareJoins({{#getSpecializes}}$prepareParentsJoins = true{{/getSpecializes}}){
        
        {{#getSpecializes}}
        if($prepareParentsJoins){
            parent::prepareJoins();
        }
        {{/getSpecializes}}
        {{#getPrimaryKeyAttributes}}
        if(!$this->prepared){
            if(!$this->getQuery()->distinct && (!is_array($this->query->columns) || !in_array('{{getName}}', $this->query->columns))){
                if(is_array($this->query->columns) && !in_array('{{getName}}', $this->query->columns)){
                    $this->select{{getNameUCFirst}}();
                }
            }elseif(!is_array($this->query->columns)){
                $this->selectAll();
            }
        }
        {{/getPrimaryKeyAttributes}}
        
        {{#getAssociationEnds}}
        {{^isCardinalityZero}}
        if(isSet($this->{{getNameLCFirst}}) && !in_array('{{getNameLCFirst}}', $this->preparedAttributes)){
            if($this->{{getNameLCFirst}}->hasSelects() && !$this->hasSelects()){
                $this->selectId();
            }
            $this->preparedAttributes[] = '{{getNameLCFirst}}';
            $realTable = $this->{{getNameLCFirst}}->getTableName();
            $tableName = $this->{{getNameLCFirst}}->getTableNameAlias();
            {{#getParent}}{{#isAssociationClass}}
            //Assoc    {{../../../getName}}
            {{^../../isAssociationClass}}
            $this->query->join('{{getName}} as {{getName}}', '{{getName}}.{{getClass.getNameLCFirst}}', '=', $this->getTableNameAlias().'.{{../../../getPrimaryKeyAttribute.getName}}', 'LEFT');
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', '{{getName}}.{{getConnectionClass.getNameLCFirst}}', 'LEFT');
            {{/../../isAssociationClass}}
            {{#../../isAssociationClass}}
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', $this->getTableNameAlias().'.{{getConnectionClass.getNameLCFirst}}', 'LEFT');
            {{/../../isAssociationClass}}
            {{/isAssociationClass}}{{/getParent}}
            {{^getParent.isAssociationClass}}
            {{#isCardinalitySingle}}
            //Single {{../../getName}}
             $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', $this->getTableNameAlias().'.{{getName}}', 'LEFT');
            {{/isCardinalitySingle}}
            {{^isCardinalitySingle}}
            //Multi
            {{^getParent.isRelationNM}}
            //no n-m relation
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getOppositeAssociationEnd.getName}}', '=', $this->getTableNameAlias().'.{{getPrimaryKeyAttribute.getName}}', 'LEFT');
            {{/getParent.isRelationNM}}
            {{#getParent.isRelationNM}}
            //is n-m relation
            $this->query->join('{{getParent.getName}} as {{getParent.getName}}', '{{getParent.getName}}.{{getClass.getNameLCFirst}}', '=', $this->getTableNameAlias().'.{{getPrimaryKeyAttribute.getName}}', 'LEFT');
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', '{{getParent.getName}}.{{getConnectionClass.getNameLCFirst}}', 'LEFT');
            {{/getParent.isRelationNM}}
            {{/isCardinalitySingle}}
            {{/getParent.isAssociationClass}}
            $this->{{getNameLCFirst}}->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->{{getNameLCFirst}}->getQuery(),$tableName);
            $this->query->mergeQuery($this->{{getNameLCFirst}}->getQuery(), $this->{{getNameLCFirst}}->getConnectionLogic());

            if(!is_null($this->{{getNameLCFirst}}->getQuery()->columns)){
                $this->objectMap[] = array(
                    'class' => '{{getConnectionClass.getName}}',
                    'field' => '{{getNameLCFirst}}',
                    'multi' => {{#isCardinalitySingle}}false{{/isCardinalitySingle}}{{^isCardinalitySingle}}true{{/isCardinalitySingle}},
                    'primaryKeyIndex' => $this->{{getNameLCFirst}}->getPrimaryKeyIndex(),
                    'map' => $this->{{getNameLCFirst}}->getObjectMap()
                );
            }
            $this->joins['{{getNameLCFirst}}'] = $this->{{getNameLCFirst}};
        }

        {{#getParent.isAssociationClass}}
        {{^isAssociationClass}}
        if(isSet($this->{{getParent.getNameLCFirst}}) && !in_array('{{getParent.getNameLCFirst}}', $this->preparedAttributes)){
            if($this->{{getParent.getNameLCFirst}}->hasSelects() && !$this->hasSelects()){
                $this->selectId();
            }
            $this->preparedAttributes[] = '{{getParent.getNameLCFirst}}';
            $realTableName = $this->{{getParent.getNameLCFirst}}->getTableName();
            $tableName = $this->{{getParent.getNameLCFirst}}->getTableNameAlias();
            {{#getParent}}
            //Assoc    {{../../../getName}}
            $this->query->join($realTableName.' as '.$tableName, $tableName.'.{{../getClass.getNameLCFirst}}', '=', $this->getTableNameAlias().'.{{../../../getPrimaryKeyAttribute.getName}}', 'LEFT');
//            $this->query->join('{{getConnectionClass.getName}} as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', '{{getName}}.{{getConnectionClass.getNameLCFirst}}', 'LEFT');
            {{/getParent}}

            $this->{{getParent.getNameLCFirst}}->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->{{getParent.getNameLCFirst}}->getQuery(),$tableName);
            $this->query->mergeQuery($this->{{getParent.getNameLCFirst}}->getQuery(), $this->{{getParent.getNameLCFirst}}->getConnectionLogic());

            if(!is_null($this->{{getParent.getNameLCFirst}}->getQuery()->columns)){
                $this->objectMap[] = array(
                    'class' => '{{getParent.getName}}',
                    'field' => '{{getParent.getNameLCFirst}}',
                    'multi' => {{#isCardinalitySingle}}false{{/isCardinalitySingle}}{{^isCardinalitySingle}}true{{/isCardinalitySingle}},
                    'primaryKeyIndex' => $this->{{getParent.getNameLCFirst}}->getPrimaryKeyIndex(),
                    'map' => $this->{{getParent.getNameLCFirst}}->getObjectMap()
                );
            }
            $this->joins['{{getParent.getNameLCFirst}}'] = $this->{{getParent.getNameLCFirst}};
        }
        {{/isAssociationClass}}
        {{/getParent.isAssociationClass}}
        
        {{#isAssociationClass}}
        if(isSet($this->{{getType.getNameLCFirst}}) && !in_array('{{getType.getNameLCFirst}}', $this->preparedAttributes)){
            if($this->{{getType.getNameLCFirst}}->hasSelects() && !$this->hasSelects()){
                $this->selectId();
            }
            $this->preparedAttributes[] = '{{getType.getNameLCFirst}}';
            $realTable = $this->{{getType.getNameLCFirst}}->getTableName();
            $tableName = $this->{{getType.getNameLCFirst}}->getTableNameAlias();
            {{#getParent}}{{#isAssociationClass}}
            //Assoc    {{../../../getName}}
            {{^../../isAssociationClass}}
            $this->query->join('{{getName}} as {{getName}}', '{{getName}}.{{getClass.getNameLCFirst}}', '=', $this->getTableNameAlias().'.{{../../../getPrimaryKeyAttribute.getName}}', 'LEFT');
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', '{{getName}}.{{getConnectionClass.getNameLCFirst}}', 'LEFT');
            {{/../../isAssociationClass}}
            {{#../../isAssociationClass}}
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', $this->getTableNameAlias().'.{{getConnectionClass.getNameLCFirst}}', 'LEFT');
            {{/../../isAssociationClass}}
            {{/isAssociationClass}}{{/getParent}}
            {{^getParent.isAssociationClass}}
            
            //Single {{../../getName}}
            $this->query->join($realTable.' as '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', $this->getTableNameAlias().'.{{getName}}', 'LEFT');
            
            {{/getParent.isAssociationClass}}
            $this->{{getType.getNameLCFirst}}->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->{{getType.getNameLCFirst}}->getQuery(),$tableName);
            $this->query->mergeQuery($this->{{getType.getNameLCFirst}}->getQuery(), $this->{{getType.getNameLCFirst}}->getConnectionLogic());

            if(!is_null($this->{{getType.getNameLCFirst}}->getQuery()->columns)){
                $this->objectMap[] = array(
                    'class' => '{{getConnectionClass.getName}}',
                    'field' => '{{getType.getNameLCFirst}}',
                    'multi' => false,
                    'primaryKeyIndex' => $this->{{getType.getNameLCFirst}}->getPrimaryKeyIndex(),
                    'map' => $this->{{getType.getNameLCFirst}}->getObjectMap()
                );
            }
            $this->joins['{{getType.getNameLCFirst}}'] = $this->{{getType.getNameLCFirst}};
        }
        {{/isAssociationClass}}

        {{/isCardinalityZero}}
        {{/getAssociationEnds}}

        {{#getAttributes}}
        {{#getType.isClass}}
        if(isSet($this->{{getNameLCFirst}}) && !in_array('{{getType.getNameLCFirst}}', $this->preparedAttributes)){
            if($this->{{getNameLCFirst}}->hasSelects() && !$this->hasSelects()){
                $this->selectId();
            }
            $this->preparedAttributes[] = '{{getType.getNameLCFirst}}';
            $tableName = $this->{{getName}}->getTableNameAlias();
            {{#isCardinalitySingle}}
            //Single {{../../../getName}}
            $this->query->join('{{getConnectionClass.getName}} AS '.$tableName, $tableName.'.{{getConnectionClass.getPrimaryKeyAttribute.getName}}', '=', $this->getTableNameAlias().'.{{getName}}', 'LEFT');
            {{/isCardinalitySingle}}
            {{^isCardinalitySingle}}
            //Multi
            $this->query->join('{{getConnectionClass.getName}} AS '.$tableName, $tableName.'.{{getOppositeAssociationEnd.getName}}', '=', $this->getTableNameAlias().'.{{getPrimaryKeyAttribute.getName}}', 'LEFT');
            {{/isCardinalitySingle}}

            $this->{{getName}}->prepareJoins();
            QueryBuilder::appendWhereWithTable($this->{{getName}}->getQuery(),$tableName);
            $this->query->mergeQuery($this->{{getName}}->getQuery(), $this->{{getName}}->getConnectionLogic());

            if(!is_null($this->{{getNameLCFirst}}->getQuery()->columns)){
                $this->objectMap[] = array(
                    'class' => '{{getConnectionClass.getName}}',
                    'field' => '{{getNameLCFirst}}',
                    'multi' => {{#isCardinalitySingle}}false{{/isCardinalitySingle}}{{^isCardinalitySingle}}true{{/isCardinalitySingle}},
                    'primaryKeyIndex' => $this->{{getNameLCFirst}}->getPrimaryKeyIndex(),
                    'map' => $this->{{getNameLCFirst}}->getObjectMap()
                );
            }
            $this->joins['{{getNameLCFirst}}'] = $this->{{getNameLCFirst}};
        }
        {{/getType.isClass}}
        {{/getAttributes}}
        
        {{#hasActive}}
        //only select Active Elements if its not specific set otherwise
        if(!$this->activeSpecified  && count($this->query->wheres) > 0){
//            $this->whereActive('=', 1);
        }
        {{/hasActive}}
        
        $this->prepared = true;
        return $this;
    }
    
    public function setMysqlTable($mysqlTable){
        $this->mysqlTable = $mysqlTable;
        return $this;
    }
    
    public function getMysqlTable(){
        return $this->mysqlTable;
    }
    
    public function getId(){
        return md5($this->getQuery()->toSql());
    }
    
    public function latest(){
        $this->limit(1);
        {{#getPrimaryKeyAttributes}}
        $this->orderBy('{{getName}} DESC');
        {{/getPrimaryKeyAttributes}}
        $this->prepareQuery();
        return $this->get()->first();
    }
        
    //TODO: needed for backcall, pls remove
    public function loadJsObject($hash = false, $subTailor = array()){
    }
    
    public function toJsObject($returnArray = false, $hash = true, $addObjectAndId = true, $subTailor = false){
        $wheres = $this->getWheres();
        $select = array();
        $joins = array();
        
        FCVJsObj::addClass(static::class);
        
        foreach($this->objectMap as $map){
            $selectStr = !empty($map['class']) ? $map['class'].'.' : '';
            $selectStr .= $map['field'];
            $select[] = $selectStr;            
        }
        foreach($this->joins as $index => $j){
            $joins[$index] = $j->toJsObject();
        }
        
        
        return array(
            'class' => '{{getName}}CollectionPrepare',
            'id' => $this->getId(),
//            'class' => '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}CollectionPrepare',
            'data' => array(
                'columns' => $this->columns,
                'wheres' => $this->wheres,
                'order' => $this->orderBy,
                'limit' => $this->limitStr,
                'joins' => $joins,
//                'type' => '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}CollectionPrepare'
            )
        );
    }
    
    public function addToJs($selectionString = '', $tailorArray = false){
        FCVJsObj::add($this, $selectionString, $tailorArray);
        return $this;
    }
}
