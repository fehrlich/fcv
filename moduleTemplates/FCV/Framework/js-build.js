var globalScope = (function() { return this; })();
var jsModel = {};
var requirejs = function(def, cb){
    cb();
};
var require = requirejs;
var define = requirejs = function(def, cb){
    if(typeof def == "function") def();
    else if(typeof cb == "function") cb();
    else {
        debugger;
    }
};

requirejs.config = function(){};
require.config = function(){};
    
//<editor-fold defaultstate="collapsed" desc="FCVClass">
var FCVClass = function (obj, primKey) {
    primKey = primKey || 'id';
    this.onCallbacks = {};

    if (!obj) obj = {};
    if (obj.class && obj.id) {
        if (!obj.data) {
            if (!jsModel[obj.class + '.' + obj.id]) return obj;
            return jsModel[obj.class + '.' + obj.id];
        } else {
            obj = obj.data;
        }
    }

    var id = obj[primKey] ? obj[primKey] : Math.random();
    if (id && !obj.partialLoaded) {
        jsModel[this.getClassName() + '.' + id] = this;
    }
};



FCVClass.prototype.init = function () {};

//<editor-fold defaultstate="collapsed" desc="PHP-Connector">

FCVClass.prototype.toObj = function (recursive, recursiveQueue) {
    recursive = recursive || false;

    if (recursive && !recursiveQueue) {
        console.error("toObj needs a recursiveQueeue if called recursive");
    }
    var obj = {};

    if (recursive) {
        if (this.convertetToObj == recursiveQueue) {
            return this.getId();
        }
        this.convertetToObj = recursiveQueue;
    }

    obj.class = this.getClassName();
    obj.nsClass = this.getNsClassName();

    if (this.created !== null && typeof this.created != 'undefined') {
        obj.created = (typeof this.created == 'object')
            ? this.created.val() : this.created;
    }
    if (this.lastModifiedTime !== null && typeof this.lastModifiedTime
        != 'undefined') {
        obj.lastModifiedTime = (typeof this.lastModifiedTime == 'object')
            ? this.lastModifiedTime.val() : this.lastModifiedTime;
    }
    if (this.createdBy !== null && typeof this.createdBy != 'undefined')
        obj.createdBy = this.createdBy;

    return obj;
};

FCVClass.prototype.updateFromObj = function (obj, fromUser) {
    fromUser = fromUser || false;
    if (obj['created'] != null) this.created = obj['created'];
    if (obj['createdBy'] != null) this.createdBy = obj['createdBy'];
    if (obj['lastModifiedTime'] != null) this.lastModifiedTime =
            obj['lastModifiedTime'];

    for(var field in obj){
        if(obj[field] != null && field != 'partialLoaded'){
            try{
                this.set(field, obj[field]);                
            } catch(ex){
                console.warn(ex);
            }
        }
    }
    return this;
};

/**
 * calls a non static methods on an object, the object parameters are
 * generated before the call
 * @param {type} action method name
 * @param {type} parameter method parameter
 * @param {type} recursive if the object should also transmit associations ends
 * @param {type} dataType return type
 * @param {type} cb deprecated callback
 * @returns $.Deferred()
 */
FCVClass.prototype.getPostOptions = function (recursive, recursiveQueue) {
    var objParam = this.toObjForTransfer(recursive, recursiveQueue);
    var nsClass = this.getNsClassName();
    return {
        'class': nsClass,
        'objParam': objParam,
        'param': {}
    };
};

/**
 * calls a non static methods on an object, the object parameters are
 * generated before the call
 * @param {type} action method name
 * @param {type} parameter method parameter
 * @param {type} recursive if the object should also transmit associations ends
 * @param {type} dataType return type
 * @param {type} cb deprecated callback
 * @param {type} returnClass if the return is a class-type 
 * @returns $.Deferred()
 */
FCVClass.prototype.callPHPMethod = function (action, parameter, recursive,
    dataType, cb, returnClass) {
    var nsClass = this.getNsClassName();
    var objParam = this.toObjForTransfer(recursive, recursive
        ? Math.random() : null);
    return FCVClass.callPHP(action, nsClass, false, parameter, dataType,
        cb, objParam, returnClass);
};

/**
 * calls a static or non static method in the server layer, the object parameters
 * can be specified with the overwriteObjParam parameter for non static methods
 * @param {type} action method name
 * @param {type} nsClass full class namespace
 * @param {type} static if the method is a static method
 * @param {type} parameter method parameter
 * @param {type} dataType return datatype
 * @param {type} cb deprecated callback
 * @param {type} overwriteObjParam overwrite the objects default value
 * @param {type} returnClass if the return is a class-type 
 * @returns $.Deferred()
 */
FCVClass.callPHP = function (action, nsClass, static, parameter, dataType,
    cb, overwriteObjParam, returnClass) {
    dataType = dataType || null;
    var objParam, dfd, self;

    dfd = $.Deferred();
    self = this;

    if (static) {
        objParam = 'static';
    } else {
        objParam = overwriteObjParam;
    }

    $.post('ajax.php', {
        'class': nsClass,
        'action': action,
        'objParam': objParam,
        'param': parameter
    }, function (returnObj) {
        if (returnObj.error) alert(returnObj.error);
        if (returnClass) {
            returnObj = returnClass.createFromArray(returnObj);
        }
        if (cb) cb.call(self, returnObj);
        dfd.resolve(returnObj);
    }, dataType).fail(function (r) {
        dfd.reject(r);
    });

    return dfd;
};

FCVClass.prototype.toObjForTransfer = function (recursive,
    recursiveQueue) {
    var obj = this.toObj(recursive, recursiveQueue);

    //change boolean values, so they are not interpretet as string
    for (var index in obj) {
        var val = obj[index];
        if (val === true || val === false) {
            obj[index] = val ? 1 : 0;
        }
    }

    return obj;
};

//</editor-fold>

FCVClass.extendWithCreateMethod = function (objToExt) {
    objToExt.create = function (obj) {
        obj = obj || {};
        return new objToExt(obj);
    };

    objToExt.createFromArray = function (obj) {
        obj = obj || {};
        return new objToExt(obj);
    };
};

FCVClass.extendWithEventAction = function (obj) {
    obj.onCallbacks = {};
    var self = obj;

    obj.on = function (name, cb) {
        if(!cb){
            cb = name;
            name = 'all';
        }
        if (!this.onCallbacks[name]) this.onCallbacks[name] = [];
        this.onCallbacks[name].push(cb);
        return this;
    };

    obj.trigger = function (name, arg) {

        if (this.onCallbacks[name]) {
            for (var index in this.onCallbacks[name]) {
                this.onCallbacks[name][index].call(this, arg);
            }
        }

        if (this.onCallbacks['all']) {
            for (var index in this.onCallbacks['all']) {
                this.onCallbacks['all'][index].call(this, name, arg);
            }
        }

        return this;
    };
};

//<editor-fold defaultstate="collapsed" desc="Table Extensions">


FCVClass.extendWithCRUD = function (objToExt, primKeyNames) {

    objToExt.prototype.save = function () {
        var self = this;
        var recursive = false;
        return self.callPHPMethod('save', {}, recursive, 'json', function (
            fields) {
            self.updateFromObj(fields);
            self.trigger('save');
        }, objToExt);
    };

    objToExt.prototype.saveNew = function () {
        var self = this;
        var recursive = false;
        return self.callPHPMethod('saveNew', {}, recursive, 'json',
            function (fields) {
                self.updateFromObj(fields);
                self.trigger('save');
            }, objToExt);
    };

    objToExt.prototype.complexSave = function () {
        var self = this;
        var recursive = true;

        return self.callPHPMethod('save', {}, recursive, 'json', function (
            fields) {
            self.updateFromObj(fields);
            self.trigger('save');
        }, objToExt);
    };

    objToExt.prototype.treeSave = function () {
        var self = this;
        var recursive = true;

        return self.callPHPMethod('treeSave', {}, recursive, 'json',
            function (fields) {
                self.updateFromObj(fields);
                self.trigger('save');
                self.trigger('treeSave');
            }, objToExt);
    };

    /**
     * sets an array of fields to zero and update it on the server. this method
     * also supports the dot-notation to access diffrent objects. Each updated
     * object creates a ajax request.
     * @param array fieldSelectors array of fieldselectors, including dot notation
     * @returns {Deferred}
     */
    objToExt.prototype.setNullWithDotNotation = function (fieldSelectors) {
        var nullCalls, thisNullFields, dfd, dfds;

        dfd = $.Deferred();
        dfds = [];
        nullCalls = {};
        thisNullFields = [];

        for (var selectorIndex in fieldSelectors) {
            var selector = fieldSelectors[selectorIndex];
            var dotIndex = selector.lastIndexOf('.');
            if (dotIndex > 0) {
                var getter = selector.substr(0, dotIndex);
                var field = selector.substr(dotIndex + 1);
                if (!nullCalls[getter]) {
                    nullCalls[getter] = [];
                }
                nullCalls[getter].push(field);
            } else {
                thisNullFields.push(selector);
            }
        }

        for (var objectSelector in nullCalls) {
            var nullFields = nullCalls[objectSelector];
            var object = this.get(objectSelector);

            dfds.push(object.setNull(nullFields));
        }
        if (thisNullFields.length > 0) {
            dfds.push(this.setNull(thisNullFields));
        }

        $.when(dfds).done(function () {
            dfd.resolve();
        });

        return dfd;
    };

    /**
     * set an array of fields to zero and update it on the server
     * @param array fieldNames
     * @returns {unresolved}
     */
    objToExt.prototype.setNull = function (fieldNames) {
        var nsClass = this.getNsClassName();
        var recursive = false;
        var primKeyObj = this.getPrimaryKeyObject();

        for (var index in fieldNames) {
            this[fieldNames[index]] = null;
        }

        return FCVClass.callPHP('setNull', nsClass, false, {
            'fields': fieldNames
        }, 'json', null, primKeyObj);
    };

    objToExt.prototype.delete = function () {
        var self = this;
        var nsClass = this.getNsClassName();
        var primKeyObj = this.getPrimaryKeyObject();

        return FCVClass.callPHP('delete', nsClass, false, {}, 'json',
            function (r) {
                self.trigger('delete');
            }, primKeyObj);
    };

    objToExt.load = function (id) {
        var jsModelName = objToExt.className + '.' + id;
        if (jsModel[jsModelName]) {
            return jsModel[jsModelName];
        }
        var nsClass = objToExt.nsClassName;

        return FCVClass.callPHP('load', nsClass, true, {
            'id': id
        });
    };
};
FCVClass.extendWithActive = function (objToExt) {
    objToExt.softDelete = function () {
        var primKeyObj = this.getPrimaryKeyObject();
        var self = this;
        var nsClass = this.getNsClassName();

        return FCVClass.callPHP('softDelete', nsClass, false, {}, 'json',
            function (r) {
                self.trigger('delete');
                self.trigger('softDelete');
            }, primKeyObj);
    };
};

//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Attributes">
FCVClass.prototype.get = function (selector) {
    var dotIndex = selector.indexOf('.');
    if (dotIndex > 0) {
        return this.get(selector.substr(0, dotIndex)).get(selector.substr(
            dotIndex+ 1));
    }
    var first = selector.charAt(0).toUpperCase();
    selector = first + selector.substr(1);
    return this['get' + selector]();
};

FCVClass.prototype.set = function (selector, value, fromUser) {
    fromUser = fromUser || false;
    var dotIndex = selector.indexOf('.');
    if (dotIndex > 0) {
        var newScope = this.get(selector.substr(0, dotIndex));
        if (!newScope) {
            throw ("Element {{getName}} has no attribute "
                + selector.substr(0, dotIndex)
                + " please declare it before");
        }
        return newScope.set(selector.substr(dotIndex + 1), value);
    }
    var first = selector.charAt(0).toUpperCase();
    selector = first + selector.substr(1);

    if (typeof this['set' + selector] == 'undefined') {
        throw ("Element {{getName}} has no setter for " + selector);
    }

    return this['set' + selector](value, fromUser);
};

FCVClass.extendWithTypeParser = function (objToExt) {
    objToExt.prototype.isNull = false;
    objToExt.null = function () {
        var n = new objToExt();
        n.isNull = true;
        return n;
    };
    objToExt.prototype.val = function () {
        if (this.isNull) return null;
        else return this.toString();
    };
};

FCVClass.extendWithTableFields = function (objToExt) {
    if (!objToExt.prototype.getCreated) objToExt.prototype.getCreated =
            function () {
                return this.created;
            };

    if (!objToExt.prototype.setCreated) objToExt.prototype.setCreated =
            function (created, fromUser) {
                fromUser = fromUser || false;
                if (created != null) {
                    if (typeof created == 'object'
                        && created instanceof DateTime) {
                        this.created = created;
                    } else if (fromUser && DateTime.parseFromUser) {
                        this.created = DateTime.parseFromUser(created);
                    } else {
                        this.created = DateTime.parse(created);
                    }
                }

                return this;
            };
};

FCVClass.addAttributeSimple = function (objToExt, name, defaultValue,
    isClass, getterFunction, setterFunction) {

};

FCVClass.addAttributeTypeParser = function (objToExt, name, defaultValue,
    isClass, getterFunction, setterFunction) {

};

FCVClass.addAttribute = function (objToExt, name, defaultValue, isClass,
    getterFunction, setterFunction) {
//        objToExt[name] = defaultValue;
//
//        if(!objToExt[getterFunction]) objToExt[getterFunction] = function(){
//            var val = this[name];
//            if(val && val.class && val.id) this[name] = jsModel[val.class+'.'+val.id];
//            return this[name];
//        };
//
//        if(!objToExt[setterFunction]) objToExt[setterFunction] = function({{getName}}, fromUser){
//            fromUser = fromUser || false;
//            {{^isDefaultType}}
//            if({{getName}} == null) this[name] = null;
//            else if({{getName}} == '[null]') this[name] = null;
//
//
//            {{^isCardinalityMulti}}
//            {{#isClass}}
//            else if(typeof {{getName}} == 'object' && {{getName}} instanceof {{getType.getName}}) this[name] = {{getName}};
//    //            else if(typeof {{getName}} == 'object' && {{getName}}.class && {{getName}}.id) this[name] = {{getName}};
//            else if(typeof {{getName}} == 'object') this[name] = new {{getType.getName}}({{getName}});
//            {{/isClass}}
//            {{#getType.getPrimaryKeyAttribute}}else if(objToExt > 0  || (typeof objToExt == 'string' && objToExt != '')) this.objToExt = {{../getType.getName}}.create({ {{getName}}: objToExt, partialLoaded: true});{{/getType.getPrimaryKeyAttribute}}
//            {{/isCardinalityMulti}}{{#isCardinalityMulti}}
//    //            else if({{getName}} instanceof {{getType.getName}}Collection) this[name] = {{getName}};
//            else if(Object.prototype.toString.call( {{getName}} ) === '[object Array]'){
//                this[name] = [];
//                for(var index in {{getName}}){
//                    var element = {{getName}}[index];
//                    if(typeof element == 'object' && element instanceof {{getType.getName}}) this[name].push(element);
//                    else if(typeof element == 'object') this[name].push({{getType.getName}}.create(element));
//                    {{#getType.getPrimaryKeyAttribute}}else if(element > 0 || (typeof element == 'string' && element != '')) this.objToExt.push({{../getType.getName}}.create({ {{getName}}: element, partialLoaded: true}));{{/getType.getPrimaryKeyAttribute}}
//    //                    {{#getType.hasPrimaryKeyAttribute}}else if(element > 0) this[name].push({{getType.getName}}.create({ {{getType.getPrimaryKeyAttribute.getName}}: element, partialLoaded: true}));{{/getType.hasPrimaryKeyAttribute}}
//                    else{
//                        this[name] = null;
//                        console.log("TYPEERROR: ",{{getName}});
//                    }
//                }
//            }
//
//            {{/isCardinalityMulti}}
//            else{
//                this[name] = null;
//                console.log("TYPEERROR: ",{{getName}});
//            }
//            {{/isDefaultType}}
//            {{#isDefaultType}}
//            {{#getType.usesTypeParser}}
//            if({{getName}} != null){
//                if(typeof {{getName}} == 'object' && {{getName}} instanceof {{getType.getName}}){
//                    this[name] = {{getName}};
//                }
//                else if(fromUser && {{getType.getName}}.parseFromUser){
//                    this[name] = {{getType.getName}}.parseFromUser({{getName}});
//                }else{
//                    this[name] = {{getType.getName}}.parse({{getName}});
//                }
//            }else{
//                this[name] = {{getType.getName}}.null();
//            }
//            {{/getType.usesTypeParser}}{{^getType.usesTypeParser}}
//            this[name] = {{#isBoolean}}!{{getName}} || {{getName}} == "0" || {{getName}} == "false" ? false : true{{/isBoolean}}{{^isBoolean}}{{getName}}{{/isBoolean}};
//            {{/getType.usesTypeParser}}
//            {{/isDefaultType}}

//            return this;
//        };
};

//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Templates-Extension">


FCVClass.extendWithTemplates = function (objToExt) {
    objToExt.prototype.getHtmlPHP = function (templateName) {
        return this.callPHPMethod('getHtml', {
            'template': templateName
        });
    };
    objToExt.prototype.getHtml = function (templateName, cb) {
        var tplSrc = objToExt.getTemplate(templateName);
        var template = Handlebars.compile(tplSrc);
        var html = template(this);
        if(cb){
            cb(html);
        }
        return html;
    };
};

//</editor-fold>


FCVClass.extendWithSingleton = function (objToExt) {
    objToExt.instance = null;
    objToExt.get = function () {
        if (objToExt.instance === null) objToExt.instance = new objToExt();
        return objToExt.instance;
    };
};

//<editor-fold defaultstate="collapsed" desc="Inheritance">

FCVClass.extend = function (sub, base) {
    // Avoid instantiating the base class just to setup inheritance
    // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create
    // for a polyfill
    // Also, do a recursive merge of two prototypes, so we don't overwrite 
    // the existing prototype, but still maintain the inheritance chain
    // Thanks to @ccnokes

    for (var key in base) {
        if (base.hasOwnProperty(key) && !sub[key] && key !== 'prototype'
            && key !== 'onCallbacks') {
            sub[key] = base[key];
        }
    }

    var origProto = sub.prototype;
    sub.prototype = Object.create(base.prototype);
    for (var key in origProto) {
        sub.prototype[key] = origProto[key];
    }
    // Remember the constructor property was set wrong, let's fix it
    sub.prototype.constructor = sub;
    // In ECMAScript5+ (all modern browsers), you can make the constructor property
    // non-enumerable if you define it like this instead
    Object.defineProperty(sub.prototype, 'constructor', {
        enumerable: false,
        value: sub
    });
};
//</editor-fold>

globalScope.FCVClass = FCVClass;


//</editor-fold>

{{#getAllClassesOrdered}}
{{#isClient}}

//<editor-fold defaultstate="collapsed" desc="js-class">

    {{#isPageOrFrame}}
    {{#getWorkPackage}}
    {{^getControlIsDefault}}
    function {{getName}}(obj,fromInherit){
            this.loadPage = function(){
            {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
            {{#includeCode}}{{#indent 2}}{{#singleParse}}{{&getControlContent}}{{/singleParse}}{{/indent 2}}{{/includeCode}}
            {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        };
    }
    
    {{getName}}.create = function(){
        return new {{getName}}();
    };
    
    {{/getControlIsDefault}}
    {{/getWorkPackage}}
    {{/isPageOrFrame}}
    {{^isPageOrFrame}}
    function {{getName}}(obj,fromInherit){
        fromInherit = fromInherit || false;
        obj = obj || {};
        var overwrite;
                
        {{#getClientSpecializes}}overwrite = {{getName}}.call(this, obj,true);{{/getClientSpecializes}}
        {{^getClientSpecializes}}overwrite = FCVClass.call(this, obj, {{^getPrimaryKeyAttribute}}false{{/getPrimaryKeyAttribute}}{{#getPrimaryKeyAttribute}}'{{getPrimaryKeyAttribute.getName}}'{{/getPrimaryKeyAttribute}});{{/getClientSpecializes}}
        
        if (overwrite) {
            return overwrite;
        }
        
        if(!fromInherit){
            this.updateFromObj(obj);
    {{^getWorkPackage.getControlIsDefault}}
            this.init();
    {{/getWorkPackage.getControlIsDefault}}
        }
    };
    
    {{getName}}.fcvFields = [{{#getAttributes}}'{{getName}}',{{/getAttributes}}];
    {{getName}}.primaryKeysNames = [
        {{#hasPrimaryKeyAttribute}}
        {{#getPrimaryKeyAttribute}}
        '{{getName}}'
        {{/getPrimaryKeyAttribute}}
        {{/hasPrimaryKeyAttribute}}
        {{^hasPrimaryKeyAttribute}}
        {{#isAssociationClass}}
        {{#getAssociationEnds}}
        '{{getType.getNameLCFirst}}',
        {{/getAssociationEnds}}
        {{/isAssociationClass}}
        {{/hasPrimaryKeyAttribute}}
    ];
        
    //Helper
    FCVClass.extendWithEventAction({{getName}});
    FCVClass.extendWithEventAction({{getName}}.prototype);
    {{^isSingleton}}
    FCVClass.extendWithCreateMethod({{getName}});
    {{/isSingleton}}
    {{#isSingleton}}
    FCVClass.extendWithSingleton({{getName}});
    {{/isSingleton}}
            
    {{getName}}.className = '{{getName}}';
    {{getName}}.nsClassName = '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}';
    {{getName}}.getClassName = function(){
        return '{{getName}}';
    };
    {{getName}}.prototype.getClassName = function(){
        return '{{getName}}';
    };
    {{getName}}.prototype.getNsClassName = function(){
        return {{getName}}.nsClassName;
    };
    {{#usesTypeParser}}
    FCVClass.extendWithTypeParser({{getName}});    
    {{/usesTypeParser}}
            
    {{#getClientSpecializes}}
    {{../getName}}._parent = {{getName}}.prototype;
    {{/getClientSpecializes}}
    {{^getClientSpecializes}}
    {{../getName}}._parent = FCVClass.prototype;
    {{/getClientSpecializes}}
    
    {{^getClientSpecializes}}
    FCVClass.extendWithTableFields({{getName}});
    {{/getClientSpecializes}}
        
    //Attributes Getter Setter
    {{#getAttributes}}
    {{^noJs}}
    {{../getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getName}}{{#getDefault}} = {{&.}}{{/getDefault}};

    {{../getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getterFunction}} = function(){
        var val = this.{{getName}};
        {{#isClass}}if(val && val.class && val.id) this.{{getName}} = jsModel[val.class+'.'+val.id];{{/isClass}}
//        return {{#isBoolean}}!val || val == "0" || val == "false" ? false : true {{/isBoolean}}{{^isBoolean}}this.{{getName}}{{/isBoolean}};
        return this.{{getName}};
    };

    {{../getName}}{{^isStatic}}.prototype{{/isStatic}}.{{setterFunction}} = function({{getName}}, fromUser){
        fromUser = fromUser || false;
        {{^isDefaultType}}
        if({{getName}} == null) this.{{getName}} = null;
        else if({{getName}} == '[null]') this.{{getName}} = null;


        {{^isCardinalityMulti}}
        {{#isClass}}
        else if(typeof {{getName}} == 'object' && {{getName}} instanceof {{getType.getName}}) this.{{getName}} = {{getName}};
//            else if(typeof {{getName}} == 'object' && {{getName}}.class && {{getName}}.id) this.{{getName}} = {{getName}};
        else if(typeof {{getName}} == 'object') this.{{getName}} = new {{getType.getName}}({{getName}});
        {{/isClass}}
        {{#getType.getPrimaryKeyAttribute}}else if({{../getName}} > 0  || (typeof {{../getName}} == 'string' && {{../getName}} != '')) this.{{../getName}} = {{../getType.getName}}.create({ {{getName}}: {{../getName}}, partialLoaded: true});{{/getType.getPrimaryKeyAttribute}}
        {{/isCardinalityMulti}}{{#isCardinalityMulti}}
//            else if({{getName}} instanceof {{getType.getName}}Collection) this.{{getName}} = {{getName}};
        else if(Object.prototype.toString.call( {{getName}} ) === '[object Array]'){
            this.{{getName}} = [];
            for(var index in {{getName}}){
                var element = {{getName}}[index];
                if(typeof element == 'object' && element instanceof {{getType.getName}}) this.{{getName}}.push(element);
                else if(typeof element == 'object') this.{{getName}}.push({{getType.getName}}.create(element));
                {{#getType.getPrimaryKeyAttribute}}else if(element > 0 || (typeof element == 'string' && element != '')) this.{{../getName}}.push({{../getType.getName}}.create({ {{getName}}: element, partialLoaded: true}));{{/getType.getPrimaryKeyAttribute}}
//                    {{#getType.hasPrimaryKeyAttribute}}else if(element > 0) this.{{getName}}.push({{getType.getName}}.create({ {{getType.getPrimaryKeyAttribute.getName}}: element, partialLoaded: true}));{{/getType.hasPrimaryKeyAttribute}}
                else{
                    this.{{getName}} = null;
                    console.log("TYPEERROR: ",{{getName}});
                }
            }
        }

        {{/isCardinalityMulti}}
        else{
            this.{{getName}} = null;
            console.log("TYPEERROR: ",{{getName}});
        }
        {{/isDefaultType}}
        {{#isDefaultType}}
        {{#getType.usesTypeParser}}
        if({{getName}} != null){
            if(typeof {{getName}} == 'object' && {{getName}} instanceof {{getType.getName}}){
                this.{{getName}} = {{getName}};
            }
            else if(fromUser && {{getType.getName}}.parseFromUser){
                this.{{getName}} = {{getType.getName}}.parseFromUser({{getName}});
            }else{
                this.{{getName}} = {{getType.getName}}.parse({{getName}});
            }
        }else{
            this.{{getName}} = {{getType.getName}}.null();
        }
        {{/getType.usesTypeParser}}{{^getType.usesTypeParser}}
        this.{{getName}} = {{#isBoolean}}!{{getName}} || {{getName}} == "0" || {{getName}} == "false" ? false : true{{/isBoolean}}{{^isBoolean}}{{getName}}{{/isBoolean}};
        {{/getType.usesTypeParser}}
        {{/isDefaultType}}

        return this;
    };
    {{/noJs}}
    {{/getAttributes}}

    //AssociationEnds
    {{#getAssociationEnds}}
    {{#getConnectionClass.isClient}}
    {{^isCardinalityZero}}
    {{../../getName}}.prototype.{{getName}};


    {{../../getName}}.prototype.get{{getNameUCFirst}} = function(){
        {{#isCardinalityMulti}}
        if(this.{{getName}}){
            for(var index in this.{{getName}}){
                var val = this.{{getName}}[index];
                if(val.class && val.id) this.{{getName}}[index] = jsModel[val.class+'.'+val.id];
            }
        }
        {{/isCardinalityMulti}}
        {{^isCardinalityMulti}}
        if(this.{{getName}} && this.{{getName}}.class && this.{{getName}}.id) this.{{getName}} = jsModel[this.{{getName}}.class+'.'+this.{{getName}}.id];
        {{/isCardinalityMulti}}
        return this.{{getName}};
    };

    {{../../getName}}.prototype.set{{getNameUCFirst}} = function({{getName}}){
        {{^isDefaultType}}
        if({{getName}} == null) this.{{getName}} = null;
        else if({{getName}} == '[null]') this.{{getName}} = null;

        {{^isCardinalityMulti}}
        else if(typeof {{getName}} == 'object' && {{getName}} instanceof {{getType.getName}}) this.{{getName}} = {{getName}};
        else if(typeof {{getName}} == 'object') this.{{getName}} = new {{getType.getName}}({{getName}});
//            else if({{getName}} > 0) this.{{getName}} = {{getType.getName}}.create({ {{getType.getPrimaryKeyAttribute.getName}}: {{getName}}, partialLoaded: true});
        {{#getType.getPrimaryKeyAttribute}}else if({{../../getName}} > 0  || (typeof {{../../getName}} == 'string' && {{../../getName}} != '')) this.{{../../getName}} = {{../../getType.getName}}.create({ {{getName}}: {{../../getName}}, partialLoaded: true});{{/getType.getPrimaryKeyAttribute}}
        {{/isCardinalityMulti}}{{#isCardinalityMulti}}
//            else if({{getName}} instanceof {{getType.getName}}Collection) this.{{getName}} = {{getName}};
        else if(Object.prototype.toString.call( {{getName}} ) === '[object Array]'){
            this.{{getName}} = [];
            for(var index in {{getName}}){
                var element = {{getName}}[index];
                if(typeof element == 'object') this.{{getName}}.push({{getType.getName}}.create(element));
                else if(typeof element == 'object' && element instanceof {{getType.getName}}) this.{{getName}}.push(element);
                {{#getType.getPrimaryKeyAttribute}}else if(element > 0 || (typeof element == 'string' && element != '')) this.{{../../getName}}.push({{../../getType.getName}}.create({ {{getName}}: element, partialLoaded: true}));{{/getType.getPrimaryKeyAttribute}}
//                    {{#getType.hasPrimaryKeyAttribute}}else if(element > 0) this.{{getName}}.push({{getType.getName}}.create({ {{getType.getPrimaryKeyAttribute.getName}}: element, partialLoaded: true}));{{/getType.hasPrimaryKeyAttribute}}
                else{
                    this.{{getName}} = null;
                    console.log("TYPEERROR: ",{{getName}});
                }
            }
        }

        {{/isCardinalityMulti}}
        else{
            this.{{getName}} = null;
            console.log("TYPEERROR: ",{{getName}});
        }
        {{/isDefaultType}}
        {{#isDefaultType}}this.{{getName}} = {{getName}};{{/isDefaultType}}
        {{^getOppositeAssociationEnd.isCardinalityZero}}

        {{#getOppositeAssociationEnd.isCardinalitySingle}}
        {{#isCardinalityMulti}}
        for(var index in {{getNameLCFirst}}){
            if(typeof {{getNameLCFirst}}[index] == 'object' && {{getNameLCFirst}}[index] instanceof {{getType.getName}} && {{getNameLCFirst}}[index].get{{getOppositeAssociationEnd.getNameUCFirst}}().getId() != this.getId()) {{getNameLCFirst}}[index].set{{getOppositeAssociationEnd.getNameUCFirst}}(this);
        }
        {{/isCardinalityMulti}}
        {{^isCardinalityMulti}}
        if(typeof {{getNameLCFirst}} == 'object' && {{getNameLCFirst}} instanceof {{getType.getName}} && {{getNameLCFirst}}.get{{getOppositeAssociationEnd.getNameUCFirst}}() && {{getNameLCFirst}}.get{{getOppositeAssociationEnd.getNameUCFirst}}().getId() != this.getId()) {{getNameLCFirst}}.set{{getOppositeAssociationEnd.getNameUCFirst}}(this);
        {{/isCardinalityMulti}}
        {{/getOppositeAssociationEnd.isCardinalitySingle}}
        {{/getOppositeAssociationEnd.isCardinalityZero}}

        return this;
    };


    {{#isCardinalityMulti}}
    {{../../../getName}}.prototype.add{{getNameUCFirst}} = function({{getName}}){
        if(!this.{{getName}}) this.{{getName}} = [];
        this.{{getName}}.push({{getName}});
        return this;
    };
    {{/isCardinalityMulti}}

    {{#isAssociationClass}}
    //AssocClass
    {{../../../getName}}.prototype.{{getType.getNameLCFirst}};

    {{../../../getName}}.prototype.get{{getType.getName}} = function(){
        if(this.{{getType.getNameLCFirst}} && this.{{getType.getNameLCFirst}}.class && this.{{getType.getNameLCFirst}}.id) this.{{getType.getNameLCFirst}} = jsModel[this.{{getType.getNameLCFirst}}.class+'.'+this.{{getType.getNameLCFirst}}.id];
        return this.{{getType.getNameLCFirst}};
    };

    {{../../../getName}}.prototype.set{{getType.getName}} = function({{getType.getNameLCFirst}}){

        if({{getType.getNameLCFirst}} == null) this.{{getType.getNameLCFirst}} = null;

        else if(typeof {{getType.getNameLCFirst}} == 'object' && {{getType.getNameLCFirst}} instanceof {{getType.getName}}) this.{{getType.getNameLCFirst}} = {{getType.getNameLCFirst}};
        else if(typeof {{getType.getNameLCFirst}} == 'object') this.{{getType.getNameLCFirst}} = new {{getType.getName}}({{getType.getNameLCFirst}});
        {{#getType.getPrimaryKeyAttribute}}else if({{../../getType.getNameLCFirst}} > 0) this.{{../../getType.getNameLCFirst}} = {{../../getType.getName}}.create({ {{getName}}: {{../../getType.getNameLCFirst}}, partialLoaded: true});{{/getType.getPrimaryKeyAttribute}}
        else{
            this.{{getType.getNameLCFirst}} = null;
            console.log("ERROR: ",{{getType.getNameLCFirst}});
        }

        //{{^getOppositeAssociationEnd.isCardinalityZero}}if({{getType.getNameLCFirst}}.get{{getOppositeAssociationEnd.getNameUCFirst}}() != this.getId()) {{getType.getNameLCFirst}}.set{{getOppositeAssociationEnd.getNameUCFirst}}(this);{{/getOppositeAssociationEnd.isCardinalityZero}}

        return this;
    };


    {{/isAssociationClass}}

    {{#getParent.isAssociationClass}}
    {{^isAssociationClass}}

    //end belongs to AssocClass
    {{../../../getName}}.prototype.{{getParent.getNameLCFirst}};

    {{../../../getName}}.prototype.set{{getParent.getName}} = function({{getParent.getNameLCFirst}}){
        if({{getParent.getNameLCFirst}} == null) this.{{getParent.getNameLCFirst}} = null;
        {{^isCardinalitySingle}}
        else if(typeof {{getParent.getNameLCFirst}} == 'object' && {{getParent.getNameLCFirst}} instanceof Object){
            this.{{getParent.getNameLCFirst}} = [];
            for(var index in {{getParent.getNameLCFirst}}){
                var val = {{getParent.getNameLCFirst}}[index];
                if(!(typeof val == 'object' && val instanceof {{getParent.getName}})) val = new {{getParent.getName}}(val);
                this.{{getParent.getNameLCFirst}}.push(val);
            }
        }
        {{/isCardinalitySingle}}
        {{#isCardinalitySingle}}
        else if(typeof {{getParent.getNameLCFirst}} == 'object' &&  {{getParent.getNameLCFirst}} instanceof {{getParent.getName}}) this.{{getParent.getNameLCFirst}} = {{getParent.getNameLCFirst}};
        else if(typeof {{getParent.getNameLCFirst}} == 'object') this.{{getParent.getNameLCFirst}} = new {{getParent.getName}}({{getParent.getNameLCFirst}});
        {{/isCardinalitySingle}}
        else{
            this.{{getParent.getNameLCFirst}} = null;
            console.log("ERROR: ",{{getParent.getNameLCFirst}});
        }

        {{#isCardinalityMulti}}
        for(var index in {{getParent.getNameLCFirst}}){
            if(typeof {{getParent.getNameLCFirst}}[index]== 'object' && {{getParent.getNameLCFirst}}[index] instanceof {{getParent.getName}} && (!{{getParent.getNameLCFirst}}[index].get{{../../../../getName}}() || {{getParent.getNameLCFirst}}[index].get{{../../../../getName}}().getId() != this.getId())) {{getParent.getNameLCFirst}}[index].set{{../../../../getName}}(this);
        }
        {{/isCardinalityMulti}}
        {{^isCardinalityMulti}}
        if(typeof {{getParent.getNameLCFirst}}== 'object' &&  {{getParent.getNameLCFirst}} instanceof {{getParent.getName}} && (!{{getParent.getNameLCFirst}}.get{{../../../getName}}() || {{getParent.getNameLCFirst}}.get{{../../../getName}}().getId() != this.getId())) {{getParent.getNameLCFirst}}.set{{../../../getName}}(this);
        {{/isCardinalityMulti}}

        return this;
    };

    {{../../../getName}}.prototype.get{{getParent.getName}} = function(){
        {{#isCardinalityMulti}}
        if(this.{{getParent.getNameLCFirst}}){
            for(var index in this.{{getParent.getNameLCFirst}}){
                var val = this.{{getParent.getNameLCFirst}}[index];
                if(val.class && val.id) this.{{getParent.getNameLCFirst}}[index] = jsModel[val.class+'.'+val.id];
            }
        }
        {{/isCardinalityMulti}}
        {{^isCardinalityMulti}}
        if(this.{{getParent.getNameLCFirst}} && this.{{getParent.getNameLCFirst}}.class && this.{{getParent.getNameLCFirst}}.id) this.{{getParent.getNameLCFirst}} = jsModel[this.{{getParent.getNameLCFirst}}.class+'.'+this.{{getParent.getNameLCFirst}}.id];
        {{/isCardinalityMulti}}
        return this.{{getParent.getNameLCFirst}};
    };

    {{/isAssociationClass}}
    {{/getParent.isAssociationClass}}
    {{/isCardinalityZero}}
    {{/getConnectionClass.isClient}}
    {{/getAssociationEnds}}

    //PrimaryAttribute
    {{^getPrimaryKeyAttribute}}
     {{#isAssociationClass}}

    {{../getName}}.prototype.getId = function(){
        var ids = [];
        {{#getAssociationEnds}}
        ids.push(this.{{getType.getNameLCFirst}});
        {{/getAssociationEnds}}
        return ids;
    };
    {{../getName}}.prototype.setId =  function(ids){
        if(typeof ids == "string") ids = ids.split('_');
        {{#getAssociationEnds}}
        this.{{getType.getNameLCFirst}} = ids.shift();
        {{/getAssociationEnds}}
        return this;
    };
     {{/isAssociationClass}}
    {{/getPrimaryKeyAttribute}}

    {{#getPrimaryKeyAttribute}}
    {{../getName}}.prototype.getId = function(){
        return this.{{getName}};
    };
    {{../getName}}.prototype.setId =  function(id){
        this.{{getName}} = id;
        return this;
    };

    {{../getName}}.getPrimaryKeyName = function(){
        return "{{getName}}";
    };
    {{/getPrimaryKeyAttribute}}

    {{getName}}.hasPrimaryKey = function(){
        return {{#getPrimaryKeyAttribute}}true{{/getPrimaryKeyAttribute}}{{^getPrimaryKeyAttribute}}false{{/getPrimaryKeyAttribute}};
    };

    
    
    {{getName}}.prototype.convertetToObj = false;
    {{getName}}.prototype.toObj = function(recursive, recursiveQueue){
        recursive = recursive || false;
        
        //TODO: Cardinality Multi + Class ids
        var obj = {{getName}}._parent.toObj.call(this, recursive, recursiveQueue);
                
        //From Attributes
        {{#getAttributes}}
        {{^noJs}}
        {{^isStatic}}
        {{#isDefaultType}}
        {{#getType.usesTypeParser}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = this.get{{getNameUCFirst}}().val();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';        
        {{/getType.usesTypeParser}}{{^getType.usesTypeParser}} 
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = this.get{{getNameUCFirst}}();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/getType.usesTypeParser}} 
        {{/isDefaultType}}    
        {{^isDefaultType}}          
        {{#isClass}}
        {{#getType.hasPrimaryKeyAttribute}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = (recursive)?this.get{{getNameUCFirst}}().toObj(true,recursiveQueue):this.get{{getNameUCFirst}}().getId();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}{{^getType.hasPrimaryKeyAttribute}}
        if(this.{{getName}} !== null && recursive) obj.{{getName}} = this.get{{getNameUCFirst}}().toObj(true,recursiveQueue);   
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';                 
        {{/getType.hasPrimaryKeyAttribute}}
        {{/isClass}}{{^isClass}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = (typeof this.{{getName}} === 'object')?null:this.get{{getNameUCFirst}}();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/isClass}}
        {{/isDefaultType}}
        {{/isStatic}}
        {{/noJs}}
        {{/getAttributes}}

        //From AssociationsEnds
        {{#getAssociationEnds}}
        {{#getConnectionClass.isClient}}
        {{^isCardinalityZero}}

        {{^isCardinalityMulti}}
        //{{isCardinalityMulti}}
        {{#getType.hasPrimaryKeyAttribute}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = (recursive)? this.get{{getNameUCFirst}}().toObj(true, recursiveQueue):this.get{{getNameUCFirst}}().getId();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}{{^getType.hasPrimaryKeyAttribute}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined' && recursive) obj.{{getName}} = this.get{{getNameUCFirst}}().toObj(true, recursiveQueue);
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}

        {{^isAssociationClass}}
        {{#getParent.isAssociationClass}}
        //From AssocClassConnections
        {{#getType.hasPrimaryKeyAttribute}}
        if(this.{{getNameLCFirst}} !== null && typeof this.{{getNameLCFirst}} != 'undefined') obj.{{getNameLCFirst}} = (recursive)?this.get{{getNameUCFirst}}().toObj(true, recursiveQueue):this.get{{getNameUCFirst}}().getId();
//        else if(this.{{getNameLCFirst}} == null) obj.{{getNameLCFirst}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}{{^getType.hasPrimaryKeyAttribute}}
        if(this.{{getNameLCFirst}} !== null && typeof this.{{getNameLCFirst}} != 'undefined' && recursive) obj.{{getNameLCFirst}} = this.get{{getNameUCFirst}}().toObj(true, recursiveQueue);
//        else if(this.{{getNameLCFirst}} == null) obj.{{getNameLCFirst}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}
        {{/getParent.isAssociationClass}}
        {{/isAssociationClass}} 
        {{/isCardinalityMulti}}

        {{#isAssociationClass}}
        //From AssocClass
        {{#getType.hasPrimaryKeyAttribute}}
        if(this.{{getConnectionClass.getNameLCFirst}} !== null && typeof this.{{getConnectionClass.getNameLCFirst}} != 'undefined') obj.{{getConnectionClass.getNameLCFirst}} = (recursive)?this.get{{getConnectionClass.getNameUCFirst}}().toObj(true, recursiveQueue):this.{{getConnectionClass.getNameLCFirst}}.getId();
//        else if(this.{{getConnectionClass.getNameLCFirst}} == null) obj.{{getConnectionClass.getNameLCFirst}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}{{^getType.hasPrimaryKeyAttribute}}
        if(this.{{getConnectionClass.getNameLCFirst}} !== null && typeof this.{{getConnectionClass.getNameLCFirst}} != 'undefined' && recursive) obj.{{getConnectionClass.getNameLCFirst}} = this.get{{getConnectionClass.getNameUCFirst}}().toObj(true, recursiveQueue);
//        else if(this.{{getConnectionClass.getNameLCFirst}} == null) obj.{{getConnectionClass.getNameLCFirst}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}
        {{/isAssociationClass}}
        {{/isCardinalityZero}}
        {{/getConnectionClass.isClient}}
        {{/getAssociationEnds}}

        return obj;
    };

    //Userdefined Operation default php mapping
    {{#getOperations}}
    {{#isClient}}
    /**
     * 
    {{#getParameters}}
      * @param {{getType.getName}} {{getName}}
    {{/getParameters}}
      * @return {{getReturnParameter.getName}}
     */
    {{getParent.getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getName}}PHP = function({{#getParameters}}{{^first}}, {{/first}}{{getName}}{{/getParameters}}){
        
        {{#getParameters}}{{#getDefault}}{{getName}} = typeof {{getName}} === 'undefined' ? {{&.}} : {{getName}};{{/getDefault}}{{/getParameters}}
        
        var param = {
            {{#getParameters}}
            '{{getName}}': JsModel.transferParameter({{getName}}),
            {{/getParameters}}
        };
        {{#isStatic}}
        return FCVClass.callPHP('{{getName}}', '{{#pathToNamespace 2}}{{getParent.getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getParent.getName}}', 1, param{{#getReturnParameter.isClass}}, 'json'{{/getReturnParameter.isClass}});
        {{/isStatic}}
        {{^isStatic}}
        return this.callPHPMethod('{{getName}}', param, false{{#getReturnParameter.isClass}}, 'json'{{/getReturnParameter.isClass}});
        {{/isStatic}}
    };

    {{/isClient}}
    {{/getOperations}}

    //CRUD
    {{#isTable}}
    {{getName}}.prototype.getPrimaryKeyObject = function(){
        return {
            {{#hasPrimaryKeyAttribute}}
            {{#getPrimaryKeyAttribute}}
            {{getName}}: this.get{{getNameUCFirst}}()
            {{/getPrimaryKeyAttribute}}
            {{/hasPrimaryKeyAttribute}}
            {{^hasPrimaryKeyAttribute}}
            {{#isAssociationClass}}
            {{#getAssociationEnds}}
            {{getType.getNameLCFirst}}: this.get{{getType.getNameUCFirst}}().getId(),
            {{/getAssociationEnds}}
            {{/isAssociationClass}}
            {{/hasPrimaryKeyAttribute}}
        }
    };
    FCVClass.extendWithCRUD({{getName}}, {{getName}}.primaryKeysNames);
    
    {{#hasActive}}
    FCVClass.extendWithActive({{getName}}.prototype);
    {{/hasActive}}
    {{/isTable}}

    //Views and Templates
    FCVClass.extendWithTemplates({{getName}});
        
    {{getName}}.getTemplate = function(templateName){
        if(typeof templateName !== "string") templateName = "default";
        var availableTemplates = {
            {{#uniqueLines}}
            {{#getWorkPackage.getViews}}
            '{{getName}}': '{{../getName}}.{{getName}}',
            {{/getWorkPackage.getViews}}
            {{/uniqueLines}}
        };
        if(!availableTemplates[templateName]){
            {{#getClientSpecializes}}
            return {{getName}}.getTemplate(templateName);
            {{/getClientSpecializes}}{{^getClientSpecializes}}
            throw Error("template '"+templateName+"' doesnt exists");
            {{/getClientSpecializes}}
        }
        templateName = templateName || 'default';
        
        {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        var templateSrcs = {
            {{#getWorkPackage}}
            {{#list getViews}}
            {{^first}},{{/first}}'{{../getName}}.{{value.getName}}': '{{#value}}{{#jsMultilineString}}{{#includeTemplate 1}}{{&getHtmlContent}}{{/includeTemplate 1}}{{/jsMultilineString}}{{/value}}' //{{value.getId}}
            {{/list getViews}}
            {{/getWorkPackage}}
        };
        var tplSrc = templateSrcs[availableTemplates[templateName]];
        return tplSrc;
        {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        {{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        
        var templateFile = "FCVAPs{{getWorkPackage.getPath}}/"+availableTemplates[templateName]+".html";
       
        var template = $.ajax({
            'type': "GET",
            'url': templateFile,
            'async': false
        }).responseText;
        return template;
        {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    };
    
    {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    
    {{^getWorkPackage.getControlIsDefault}}
    {{getName}}.prototype.init = function(){
        {{#includeCode}}{{#indent 2}}{{#getWorkPackage}}{{#singleParse}}{{&getControlContent}}{{/singleParse}}{{/getWorkPackage}}{{/indent 2}}{{/includeCode}}
    };
    {{/getWorkPackage.getControlIsDefault}}
    
    //userMethods
    {{#getOperations}}
    {{^isInterface}}
    {{#isClient}}
    {{^getWorkPackage.getControlIsDefault}}
    /**
      *
    {{#getParameters}}
      * @param {{getType.getName}} {{getName}}
    {{/getParameters}}
      * @return {{getReturnParameter.getType.getName}}
      */
    {{getParent.getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getName}} = function({{#getParameters}}{{^first}}, {{/first}}{{getName}}{{/getParameters}}){
        {{#getParameters}}{{#getDefault}}{{getName}} = typeof {{getName}} === 'undefined' ? {{&.}} : {{getName}};{{/getDefault}}{{/getParameters}}
        {{#includeCode}}{{#indent 2}}{{#getWorkPackage}}{{#singleParse}}{{&getControlContent}}{{/singleParse}}{{/getWorkPackage}}{{/indent 2}}{{/includeCode}}
    };
    {{/getWorkPackage.getControlIsDefault}}
    
    {{/isClient}}
    {{/isInterface}}
    {{/getOperations}}
    
    {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    {{#getClientSpecializes}}
    FCVClass.extend({{../getName}}, {{getName}});
    {{/getClientSpecializes}}
    {{^getClientSpecializes}}
    FCVClass.extend({{getName}}, FCVClass);
    {{/getClientSpecializes}}
    {{/isPageOrFrame}}

// </editor-fold>

{{^isInterface}}
{{^isEnum}}
{{#isTable}}
{{^isPageOrFrame}}

//<editor-fold defaultstate="collapsed" desc="js-colPClass">
    var {{getName}}CollectionPrepare = function(obj){    
        {{#getClientSpecializes table}}{{getName}}CollectionPrepare.call(this, obj);{{/getClientSpecializes table}}
        {{^getClientSpecializes table}}FCVCollectionPrepare.call(this, obj);{{/getClientSpecializes table}}

        var self = this; 
        {{#getAttributes}}
        {{#checkClientColP where}}
        self.where{{getNameUCFirst}} = function(operation, value){
            this.where('{{getName}}', operation, value);
            return this;
        };
        
        self.orWhere{{getNameUCFirst}} = function(operation, value){
            this.orWhere('{{getName}}', operation, value);
            return this;
        };
        {{/checkClientColP where}}
        {{#checkClientColP select}}
        self.select{{getNameUCFirst}} = function(){
            this.columns.push('{{getName}}');
            return this;
        };
        
        {{/checkClientColP select}}
        {{#isClass}}
        {{#checkClientColP set}}
        self.set{{getNameUCFirst}} = function(collectionsPrepare, logic){
            logic = logic || "and";
            this.joins['{{getName}}'] = collectionsPrepare;
            collectionsPrepare.setConnectionLogic(logic);
            return this;
        };
        
        {{/checkClientColP set}}
        {{/isClass}}
        {{/getAttributes}}
        {{#getAssociationEnds}}
        {{^isCardinalityZero}}
        {{#checkClientColP select}}
        self.select{{getNameUCFirst}} = function(){
            this.columns.push('{{getName}}');
            return this;
        };
        
        {{/checkClientColP select}}
        {{#checkClientColP set}}
        self.{{setAssociationFunction}} = function(collectionsPrepare, logic){
            logic = logic || "and";
            this.joins['{{getName}}'] = collectionsPrepare;
            collectionsPrepare.setConnectionLogic(logic);
            return this;
        };
        
        {{/checkClientColP set}}
        {{#isCardinalitySingle}}
        {{#checkClientColP where}}
        self.where{{getNameUCFirst}} = function(operation, value){
            this.where('{{getName}}', operation, value);
            return this;
        };
        
        self.orWhere{{getNameUCFirst}} = function(operation, value){
            this.orWhere('{{getName}}', operation, value);
            return this;
        };
        
        {{/checkClientColP where}}
        {{/isCardinalitySingle}}
        {{^isCardinalitySingle}}
        {{#isAssociationClass}}
        //assocClass 
    //    self.where{{getNameUCFirst}} = function(operation, value){
    //        this.where('{{getName}}', operation, value);
    //        return this;
    //    };
    //    self.orWhere{{getNameUCFirst}} = function(operation, value){
    //        this.orWhere('{{getName}}', operation, value);
    //        return this;
    //    };
        {{/isAssociationClass}}
        {{/isCardinalitySingle}}
        {{#getParent.isAssociationClass}}
        {{^isAssociationClass}}
        {{#checkClientColP set}}
        self.set{{getParent.getName}} = function(collectionsPrepare, logic){
            logic = logic || "and";
            this.joins['{{getParent.getName}}'] = collectionsPrepare;
            collectionsPrepare.setConnectionLogic(logic);
            return this;
        };
        {{/checkClientColP set}}
        {{/isAssociationClass}}
        {{/getParent.isAssociationClass}}

        {{#isAssociationClass}}
        {{#getOppositeAssociationEnd.checkClientColP where}}
        self.where{{getClass.getName}} = function(operation, value){
            this.where('{{getClass.getNameLCFirst}}', operation, value);
            return this;
        };
        self.orWhere{{getClass.getName}} = function(operation, value){
            this.orWhere('{{getClass.getNameLCFirst}}', operation, value);
            return this;
        };
        
        {{/getOppositeAssociationEnd.checkClientColP where}}
        {{#getOppositeAssociationEnd.checkClientColP select}}
        self.select{{getClass.getName}} = function(operation, value){
            this.where('{{getClass.getNameLCFirst}}', operation, value);
            return this;
        };        
        {{/getOppositeAssociationEnd.checkClientColP select}}

        {{#getOppositeAssociationEnd.checkClientColP set}}
        self.set{{getClass.getName}} = function(collectionsPrepare, logic){
            logic = logic || "and";
            this.joins['{{getClass.getName}}'] = collectionsPrepare;
            collectionsPrepare.setConnectionLogic(logic);
            return this;
        };
        self.get{{getNameUCFirst}} = function(){
            if(this.{{getName}} && this.{{getName}}.class && this.{{getName}}.id) this.{{getName}} = jsModel[this.{{getName}}.class+'.'+this.{{getName}}.id];
            return this.{{getName}};
        };
        {{/getOppositeAssociationEnd.checkClientColP set}}
        {{/isAssociationClass}}
        {{/isCardinalityZero}}
        {{/getAssociationEnds}}
    
    };

    {{getName}}CollectionPrepare.prototype.getClassName = function(){
        return '{{getName}}CollectionPrepare';
    };

    {{getName}}CollectionPrepare.prototype.getNsClassName = function(){
        return '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}CollectionPrepare';
    };

    {{getName}}CollectionPrepare.prototype.getObjectClassName = function(){
        return '{{getName}}';
    };

    {{getName}}CollectionPrepare.create = function(){
        return new {{getName}}CollectionPrepare();
    };

    {{#getClientSpecializes table}}
    FCVClass.extend({{../getName}}CollectionPrepare, {{getName}}CollectionPrepare);
    {{/getClientSpecializes table}}

    {{^getClientSpecializes table}}
    FCVClass.extend({{../getName}}CollectionPrepare, FCVCollectionPrepare);
    {{/getClientSpecializes table}}
//</editor-fold>

{{/isPageOrFrame}}
{{/isTable}}
{{/isEnum}}
{{/isInterface}}

{{/isClient}}
{{/getAllClassesOrdered}}