<?php
namespace fcv\view;

use fcv\Routing;

class Page{
{{#getWorkPackage.getWorkPackagesByType Page}}
    const {{#getUmlElement}}{{getNameUppercase}} = '{{getName}}'{{/getUmlElement}};
{{/getWorkPackage.getWorkPackagesByType Page}}
    
    static function create(){
        return new Page();
    }
    private $name = '';
    
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    static function getCurrentPage(){
        return Routing::getObj()->getPageInstance();
    }
}