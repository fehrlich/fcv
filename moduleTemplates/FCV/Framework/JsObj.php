<?php

namespace fcv;

class FCVJsObj {

    static $jsObj = array();
    static $jsObjReq = array();
    static $jsObjSel = array();
    
    static $jsObjIdList = array();

    static function addRequire($req) {
        FCVJsObj::$jsObjReq[substr($req, 1)] = FCVJsObj::toJSNameSpace($req);
    }

    static function add(&$val, $selectionString = '') {
        if(isset(FCVJsObj::$jsObjIdList[get_class($val).'.'.$val->getId()])) return;
        FCVJsObj::$jsObjIdList[get_class($val).'.'.$val->getId()] = true;
        FCVJsObj::$jsObj[] = $val;
        FCVJsObj::$jsObjSel[] = $selectionString;
    }
    
    static function toJSNameSpace($objectName){
        $spl = explode('/', $objectName);
        $objectName = $spl[count($spl)-1];
        $spl = explode('\\', $objectName);
        $objectName = $spl[count($spl)-1];
        return $spl[count($spl)-1];
    }
    
//    static function load

    static function get() { 
        $loadedElements = array();
        
//        foreach (FCVJsObj::$jsObj as $i => $el) {
        for ($i = 0; $i < count(FCVJsObj::$jsObj); $i++) {
            $el = FCVJsObj::$jsObj[$i];
            $id = $el->getId();
            if (!empty($id) && !is_null($id)) {
                $jsEl = $el->toJSObject(true);
                
                $jsEl['selector'] = FCVJsObj::$jsObjSel[$i];
                if(isset($jsEl['data'])) $loadedElements[get_class($el) . '.' . $id] = $jsEl; 
            }
        }
//        d($loadedElements);
//        d("Loaded ".count(FCVJsObj::$jsObj)." JS Objects");
        $jsInit = "";
//        $jsInit .= "var jsModel = {};\n";
//        $jsInit .= "requirejs(['" . implode("','", array_keys(FCVJsObj::$jsObjReq)) . "'], function(".implode(',', FCVJsObj::$jsObjReq)."){\n\t";

        foreach ($loadedElements as $name => $jsObj) {
            $name = FCVJsObj::toJSNameSpace($name);
            $jsInit .= "\tjsModel['" . $name . "'] = new " . $jsObj['class'] . " (" . json_encode($jsObj['data']) . ");\n";
            $jsInit .= (!empty($jsObj['selector'])) ? "\tjsModel['" . $jsObj['selector'] . "'] = jsModel['" . $name . "'];" : "";
            
        }

        $jsInit .= "for(var id in jsModel){
            if(jsModel[id].class && jsModel[id].id) jsModel[id] = jsModel[jsModel[id].class+'.'+jsModel[id].id];
        }\n";
//        $jsInit .= "});\n";

        return $jsInit;
    }

}
