<?php
namespace fcv;

use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

use fcv\view\View;
use fcv\view\Frame;

//use FCVNavigation\Page;
{{#getWorkPackage.getWorkPackagesByType Page}}
use impro\FCVNavigation\Page\{{&getUmlElement.getName}};
{{/getWorkPackage.getWorkPackagesByType Page}}
//use FCVNavigation\Frame;
{{#getWorkPackage.getWorkPackagesByType Frame}}
use impro\FCVNavigation\Frame\{{&getUmlElement.getName}};
{{/getWorkPackage.getWorkPackagesByType Frame}}

class Routing extends RouteCollection{
    /**
     *
     * @var Routing
     */
    private static $handle;
    private $pageMap;
    private $frameMap;
    private $currentView = null;
    private $onNotFound = null;
    private $requestURI = '';


    private function __construct($uri = false) {
        $reqUri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
        $this->requestURI = $uri ? $uri : $reqUri;
    }

    public function setPageMap($pageMap) {
        $this->pageMap = $pageMap;
        return $this;
    }

    public function setFrameMap($frame) {
        $this->frameMap = $frame;
        return $this;
    }


    /**
     *
     * @return Routing
     */
    public static function getObj(){
        if(!isset(Routing::$handle)){
            Routing::$handle = new Routing();
        }

        return Routing::$handle;
    }

    /**
     * @return Routing
     */
    public function addDefaultRouting(){
        $firstPage = true;
        foreach($this->pageMap as $page => $vars){
            $defaultVars = array(
                'page' => $page
            );
            
            $path = ''.$page;
            $firstDefaultVars = array();
            $defaultPath = $path;
            
            foreach($vars as $varName => $possibleFrames){
                
                if(is_array($possibleFrames)){
                    $path .= '/{'.$varName.'}';
                    $firstDefaultVars[$varName] = current($possibleFrames);
                }else{
                    $defaultVars[$varName] = $possibleFrames;
                }
            }
            
            $moreThenOneFramePossible = count($firstDefaultVars) > 0;
            
            if($firstPage){
                $defPath = substr($path,strlen($page)+1);
                $this->addRoute($defPath, $defaultVars);
                if($moreThenOneFramePossible){
                    $defPath = substr($defaultPath,strlen($page)+1);
                    $this->addRoute($defPath, array_merge($defaultVars, $firstDefaultVars),$page.'Default');
                }
            }else{
                if($moreThenOneFramePossible){
                    $this->addRoute($defaultPath, array_merge($defaultVars, $firstDefaultVars));
                }
                $this->addRoute($path, $defaultVars);
            }
            $firstPage = false;
        }

        return $this;
    }

    public function addRoute($path, $objArray, $name = false){
        if(!$name) $name = $objArray['page'];
        $route = new Route($path, $objArray);
        $this->add($name, $route);

        return $this;
    }
    
    public function replace($name, $replaceArray){
        $route = parent::get($name);
        $defaults = $route->getDefaults();;
        
        foreach($replaceArray as $key => $value){
            $defaults[$key] = $value;
        }
        $route->setDefaults($defaults);
        $this->currentView = null;
        return $this;
    }
    
    public function add($name, Route $route) {
        parent::add($name, $route);
        return $this;
    }
    
    /**
     * 
     * @return View
     */
    public function getCurrentView(){
        
        if(!is_null($this->currentView)) return $this->currentView;
        $context = new RequestContext();
        $context->fromRequest(Request::createFromGlobals());
        $matcher = new UrlMatcher($this, $context);
        try {
            $parameters = $matcher->matchRequest(Request::createFromGlobals());
        } catch (ResourceNotFoundException $exc) {
            header('HTTP/1.0 404 Not Found');
            
            if($this->onNotFound){
                $cb = $this->onNotFound;
                $cb();
                
            }
            exit();
        }
        
        
        $view = View::create()
            ->setPage(isset($parameters['page']) ? $parameters['page'] : '');
        
        foreach($parameters as $varname => $value){
            if($varname != '_route' && $varname != 'page'){
                $view->addFrame($varname, $value);
            }
        }
        
        $this->currentView = $view;
        
        return $view;
    }
    
    public function throw404(){
        header('HTTP/1.0 404 Not Found');

        if($this->onNotFound){
            $cb = $this->onNotFound;
            $cb();

        }
        exit();
    }
    
    /**
     * 
     * @return View
     */
    function getView($url){
        
        $context = new RequestContext();
        $request = Request::create($url);
        $context->fromRequest($request);
        $matcher = new UrlMatcher($this, $context);
        try {
            $parameters = $matcher->matchRequest($request);
        } catch (ResourceNotFoundException $exc) {
            $this->throw404();
        }
        
        $view = View::create()
            ->setPage(isset($parameters['page']) ? $parameters['page'] : '');
        
        foreach($parameters as $varname => $value){
            if($varname != '_route' && $varname != 'page'){
                $view->addFrame($varname, $value);
            }
        }
                
        return $view;        
    }

    
    public function getPageInstance($url = false){
        $view =  $url ? $this->getView($url) : $this->getCurrentView();
        return $view->getPageInstance();
    }

    public function getUrl($page, $frameOptions){

        $context = new RequestContext($this->requestURI);
        $generator = new UrlGenerator($this, $context);
        $url = $generator->generate($page, $frameOptions);

        return $url;
    }

    public function getUrlView($page, $frameOptions){
        $view = new View();
        $view->setPage($page);
        foreach($frameOptions as $name => $frame){
            $view->addFrame($name, $frame);
        }
        return $view;
        $context = new RequestContext($_SERVER['REQUEST_URI']);
        $generator = new UrlGenerator($this, $context);
        $url = $generator->generate($page, $frameOptions);
    }

    function onNotFound($cb) {
        return $this->onNotFound = $cb;
    }
    
    static function getRoutingFor($uri){
        return new Routing($uri);
    }
}