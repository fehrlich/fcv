<?php
namespace fcv;

use DebugBar\StandardDebugBar;
use fcv\database\DB;
use fcv\traits\Singleton;
/**
 * Description of Debug
 *
 * @author XX
 */
class Debug {
    use Singleton;
    
    private $enabled = true;
    private $throwErrors = true;
    private $debugBar = null;
    private $entries = array();
    private $lastTime = false;
    
    /**
     * 
     * @return Debug
     */
    static function get(){
        return self::getObj();
    }
    
    public function init(){
//        $this->debugBar = new StandardDebugBar();
//        $this->debugBar->addCollector(new \DebugBar\DataCollector\PDO\PDOCollector(DB::getObj()));
    }
    
    public function getThrowErrors() {
        return $this->throwErrors;
    }

    public function setThrowErrors($throwErrors) {
        $this->throwErrors = $throwErrors;
    }
    
    public function startMeassure(){

    }
    public function getDebugBar() {
        return $this->debugBar;
    }

    public function setDebugBar($debugBar) {
        $this->debugBar = $debugBar;
    }

    public function startMeasure($name, $desc){
//        d(1);
        $this->debugBar['time']->startMeasure($name, $desc);
    }
    public function stopMeasure($name){
        $this->debugBar['time']->stopMeasure($name);
    }
    public function info($info){
        $this->debugBar["messages"]->addMessage($info);
    }

    public function dump(){
        d($this->entries);
        $jsRender = $this->debugBar->getJavascriptRenderer();;
//        echo $jsRender->renderHead();
        echo $jsRender->render();
    }
}