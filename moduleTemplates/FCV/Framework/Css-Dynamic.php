<?php
header('Content-Type: text/css');
//get all css from views

{{#getAllClasses}}
{{#getWorkPackage}}
{{#getViews}}
include("../FCVAPs{{../getPath}}/{{../getName}}.{{getName}}.css");
{{/getViews}}
{{/getWorkPackage}}
{{/getAllClasses}}
{{#getAllAssociationClasses}}
{{#getWorkPackage}}
{{#getViews}}
include("../FCVAPs{{../getPath}}/{{../getName}}.{{getName}}.css");
{{/getViews}}
{{/getWorkPackage}}
{{/getAllAssociationClasses}}
{{#getWorkPackage.getWorkPackagesByType Frame}}
{{#getViews}}
include("../FCVAPs{{../getPath}}/{{../getName}}.{{getName}}.css");
{{/getViews}}
{{/getWorkPackage.getWorkPackagesByType Frame}}
{{#getWorkPackage.getWorkPackagesByType Page}}
{{#getViews}}
include("../FCVAPs{{../getPath}}/{{../getName}}.{{getName}}.css");
{{/getViews}}
{{/getWorkPackage.getWorkPackagesByType Page}}