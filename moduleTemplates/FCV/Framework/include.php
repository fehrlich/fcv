<?php

function getCss(){
    $css = '';
    {{#getAllClasses}}
    {{#getWorkPackage}}
    {{#hasViews}}
    {{#getViews}}
    $css .= file_get_contents("FCVAPs{{getPath}}/{{../getName}}.{{getName}}.css");
    {{/getViews}}
    {{/hasViews}}
    {{/getWorkPackage}}
    {{/getAllClasses}}
    {{#getAllAssociationClasses}}
    {{#getWorkPackage}}
    {{#hasViews}}
    {{#getViews}}
    $css .= file_get_contents("FCVAPs{{getPath}}/{{../getName}}.{{getName}}.css");
    {{/getViews}}
    {{/hasViews}}
    {{/getWorkPackage}}
    {{/getAllAssociationClasses}}
    
    {{#getWorkPackage.getWorkPackagesByType Page}}
    {{#getViews}}
    $css .= file_get_contents("FCVAPs{{getPath}}/{{../getName}}.{{getName}}.css");
    {{/getViews}}
    {{/getWorkPackage.getWorkPackagesByType Page}}
    {{#getWorkPackage.getWorkPackagesByType Frame}}
    {{#getViews}}
    $css .= file_get_contents("FCVAPs{{getPath}}/{{../getName}}.{{getName}}.css");
    {{/getViews}}
    {{/getWorkPackage.getWorkPackagesByType Frame}}
    return $css;
}

function getJSScripts(){
    $scripts = array();
    {{#getAllClasses}}
    {{#getWorkPackage}}
    $scripts[] = array('path' => "js/classes{{getPath}}.js");
    {{/getWorkPackage}}
    {{/getAllClasses}}
    {{#getAllAssociationClasses}}
    {{#getWorkPackage}}
    $scripts[] = array('path' => "js/classes{{getPath}}.js");
    {{/getWorkPackage}}
    {{/getAllAssociationClasses}}
    
    {{#getWorkPackage.getWorkPackagesByType Page}}
        $scripts[] = array('path' => "js/classes{{getPath}}.js"); //Routing::getObj()->getDefaultPath().
    {{/getWorkPackage.getWorkPackagesByType Page}}
    {{#getWorkPackage.getWorkPackagesByType Frame}}
        $scripts[] = array('path' => "js/classes{{getPath}}.js");
    {{/getWorkPackage.getWorkPackagesByType Frame}}
    return $scripts;
}