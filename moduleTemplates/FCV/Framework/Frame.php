<?php
namespace fcv\view;

use fcv\Routing;

class Frame{
{{#getWorkPackage.getWorkPackagesByType Frame}}
    const {{#getUmlElement}}{{getNameUppercase}} = '{{getName}}'{{/getUmlElement}};
{{/getWorkPackage.getWorkPackagesByType Frame}}
   
    static $loaded = array();
    
    static public function create(){
        
        return new Frame();
    }
    
    public function getVarName() {
        return $this->varName;
    }

    public function setVarName($varName) {
        $this->varName = $varName;
        return $this;
    }

    public function getFrameName() {
        return $this->frameName;
    }

    public function setFrameName($frameName) {
        $this->frameName = $frameName;
        Frame::load($frameName);
        return $this;
    }
    
    static function isLoaded($frameName){
        return isset(Frame::$loaded[$frameName]) && Frame::$loaded[$frameName];
}
    
    static function load($frameName){
        Frame::$loaded[$frameName] = true;
        $view = Routing::getObj()->getCurrentView()
            ->addFrame($frameName, $frameName);
    }
}