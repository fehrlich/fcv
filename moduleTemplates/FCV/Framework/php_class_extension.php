//FCV Attributes
static $fcvFields = array({{#list getAttributes}}{{^first}},{{/first}}'{{value.getName}}'{{/list getAttributes}});
static $fcvDev = {{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}true{{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}{{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}false{{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}};
protected static $fcvSpecializes = {{^getSpecializes}}false{{/getSpecializes}}{{#getSpecializes}}true{{/getSpecializes}};
protected static $fcvControlClassName = '{{getName}}';