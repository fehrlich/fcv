<?php

//check 1-1 Connection for inconsistency data

{{#uniqueLines}}
{{#getAllAssociationClassesAndClasses}}
{{#isTable}}
{{#getAssociationEnds}}
{{#isCardinalitySingle}}
{{^isCardinalityZero}}
{{#getOppositeAssociationEnd}}
{{#isCardinalitySingle}}
{{#getClass}}
{{#isTable}}
use {{#substr 1}}{{getConnectionClass.getNamePath}}CollectionPrepare{{/substr 1}};
use {{#substr 1}}{{getOppositeAssociationEnd.getConnectionClass.getNamePath}}CollectionPrepare{{/substr 1}};
{{/isTable}}
{{/getClass}}
{{/isCardinalitySingle}}
{{/getOppositeAssociationEnd}}
{{/isCardinalityZero}}
{{/isCardinalitySingle}}
{{/getAssociationEnds}}
{{/isTable}}
{{/getAllAssociationClassesAndClasses}}
{{/uniqueLines}}

function fcv_getInconsistencyErrors($debug = false){
    $errors = [];
    {{#getAllClasses}}
    {{#isTable}}
    {{#getAssociationEnds}}
    {{#isCardinalitySingle}}
    {{^isCardinalityZero}}
    {{#getOppositeAssociationEnd}}
    {{^getParent.isAssociationClass}}
    {{#isCardinalitySingle}}
    {{^isCardinalityZero}}
    {{#getClass}}
    {{#isTable}}
    $count = {{getName}}CollectionPrepare::create()
        ->set{{getOppositeAssociationEnd.getOppositeAssociationEnd.getNameUCFirst}}(
            {{getConnectionClass.getName}}CollectionPrepare::create()
        )
        ->whereRaw('{{getClass.getName}}.{{getClass.getPrimaryKeyAttribute.getName}} != {{getClass.getName}}{{getConnectionClass.getName}}{{getConnectionClass.getName}}.{{getOppositeAssociationEnd.getName}}')
        ->count()
    ;
    if($debug){
        $color = $count == 0 ? 'green' : 'red';
        echo 'Check {{getName}}/{{getConnectionClass.getName}} for inconsistent data: <span style="color: '.$color.'">'.$count.'</span><br />';
        echo 'SQL: '.{{getName}}CollectionPrepare::create()
            ->set{{getOppositeAssociationEnd.getOppositeAssociationEnd.getNameUCFirst}}(
                {{getConnectionClass.getName}}CollectionPrepare::create()
            )
            ->whereRaw('{{getClass.getName}}.{{getClass.getPrimaryKeyAttribute.getName}} != {{getClass.getName}}{{getConnectionClass.getName}}{{getConnectionClass.getName}}.{{getOppositeAssociationEnd.getName}}')
            ->toSql()
        ;
        echo '<br>';
    }
    if($count > 0){
        $errors['{{getName}}/{{getConnectionClass.getName}}'] = $count;
    }
    {{/isTable}}
    {{/getClass}}
    {{/isCardinalityZero}}
    {{/isCardinalitySingle}}
    {{/getParent.isAssociationClass}}
    {{/getOppositeAssociationEnd}}
    {{/isCardinalityZero}}
    {{/isCardinalitySingle}}
    {{/getAssociationEnds}}
    {{/isTable}}
    {{/getAllClasses}}
    return $errors;
}