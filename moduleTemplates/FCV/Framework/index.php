<?php

use fcv\App;
use fcv\Config;
use fcv\libs\Debug;
use fcv\FCVJsObj;
use fcv\Routing;
use Illuminate\Support\ClassLoader;

ob_start();
require_once("autoload.php");

 {{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
define('C3CODECOVERAGEERRORLOGFILE', '/c3tmp/c3_error.log');
//define('C3_CODECOVERAGE_MEDIATE_STORAGE', '1');
include('c3.php');
{{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}

//load js/css glue for development
require_once('include.php');
require_once('js/init.js.php');


App::get()
    ->load();

$site = Routing::getObj()->getPageInstance();
if(is_string($site)){
    $html = $site;
}else{
    {{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    $site->addTemplateVar('head', '<link rel="stylesheet" type="text/css" href="'.Config::get()->getDefaultPath().'/css/dyn.css.php">');
    {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    $site->addTemplateVar('head', '<link rel="stylesheet" type="text/css" href="'.Config::get()->getDefaultPath().'/css/static.css">');
    $site->addTemplateVar('foot', '<script type="text/javascript" src="'.Config::get()->getDefaultPath().'/js/init.js"></script>');
    {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    //$site->addTemplateVar('jsScripts', getJSScripts());
    App::get()
        ->setCurrentSite($site);

    $site->loadContent();
    $html = $site->getHtml();
}

{{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
$jsModel = FCVJsObj::get();

foreach(FCVJsObj::$jsClasses as $class => $c){
    FCVJsObj::$jsClasses[basename(str_replace('\\', '/', $class))] = true;
}

$html = str_replace('{!{&initJs}!}', jsBuild($jsModel, FCVJsObj::$jsClasses), $html);
{{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
{{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
$html = str_replace('{!{&initJs}!}', jsBuild(FCVJsObj::get()), $html);
{{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
  
//Error handling (if output allready send out)
$debugOutput = ob_get_contents ();

if($debugOutput !== false && $debugOutput != ''){
        if(strpos($html, '[ERRORS]')){
            $html = str_replace('[ERRORS]',$debugOutput,$html);
            ob_clean();
        }
}else $html = str_replace('[ERRORS]', '',$html);


echo $html;
App::get()->trigger('finished');