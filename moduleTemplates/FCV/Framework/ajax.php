<?php

use fcv\App;
use fcv\classTraits\CollectionPrepare;
use fcv\FCVJsObj;

include("autoload.php");

if(isset($_POST['class']) && isset($_POST['action'])){
    $class = $_POST['class'];
    $action = $_POST['action'];
    $param = (isset($_POST['param']))?$_POST['param']:array();
    $objParam = (isset($_POST['objParam']))?$_POST['objParam']:array();

    CollectionPrepare::convertJsInputData($param, true);
    CollectionPrepare::convertJsInputData($objParam, false);

    $app = App::get()
            ->setAjax(true)
            ->load();

    if($action == 'log'){
        systemLog($_POST['msg'], $_POST['type'], 'jsErrors');
    }
    elseif($action == 'get'){
        /**
         * @var CollectionPrepare $colPrepareObject
         */
        $colPrepareObject = CollectionPrepare::createObjectPrepare($objParam);
        $col = $colPrepareObject->getRegular();
        $tailorArray = $colPrepareObject->getTailorArray();
        App::get()->trigger('finished');
        echo json_encode($col->toJSObject(false,mt_rand(0, 100000), false, $tailorArray));
    }else{

        if($objParam == 'static'){
            $ret = call_user_func_array($class.'::'.$action, $param);
        }else{
            $ret = call_user_func_array(array($class::createFromArray($objParam), $action), $param);        
        }

        $jsModel = FCVJsObj::getJsModel();
        if(count($jsModel) > 0){
            sendSplitHeader('addToJsModel', json_encode($jsModel));        
        }
        App::get()->trigger('finished');
        
        
        if(is_bool($ret)){
            echo json_encode($ret);            
        }
        elseif(is_string($ret)){
            echo $ret;
        }elseif(is_array ($ret)){
            header('Content-Type: application/json');
            echo json_encode($ret);
        }elseif(is_object ($ret)){
            header('Content-Type: application/json');
            $jsObj = $ret->toJSObject(false,mt_rand(0, 100000), false, false, false, true);
            echo json_encode($jsObj);
        }
    }
}