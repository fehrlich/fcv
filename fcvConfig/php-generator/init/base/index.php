<?php

use fcv\App;
use fcv\Config;
use fcv\libs\Debug;
use fcv\FCVJsObj;
use fcv\Routing;
use Illuminate\Support\ClassLoader;

ob_start();
require_once("autoload.php");

define('C3CODECOVERAGEERRORLOGFILE', '/c3tmp/c3_error.log');
//define('C3_CODECOVERAGE_MEDIATE_STORAGE', '1');
include('c3.php');

//load js/css glue for development
require_once('js/init.js.php');


App::get()
    ->load();

$site = Routing::getObj()->getPageInstance();
if(is_string($site)){
    $html = $site;
}else{
    $site->addTemplateVar('head', '<link rel="stylesheet" type="text/css" href="'.Config::get()->getDefaultPath().'/css/dyn.css.php">');
    //$site->addTemplateVar('jsScripts', getJSScripts());
    App::get()
        ->setCurrentSite($site);

    $site->loadContent();
    $html = $site->getHtml();
}

$html = str_replace('{!{&initJs}!}', jsBuild(FCVJsObj::get()), $html);
  
//Error handling (if output allready send out)
$debugOutput = ob_get_contents ();

if($debugOutput !== false && $debugOutput != ''){
        if(strpos($html, '[ERRORS]')){
            $html = str_replace('[ERRORS]',$debugOutput,$html);
            ob_clean();
        }
}else $html = str_replace('[ERRORS]', '',$html);


echo $html;
App::get()->trigger('finished');