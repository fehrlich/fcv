<?php

namespace {{#pathToNamespace}}{{getWorkPackage.getPath}}{{/pathToNamespace}};

class {{getName}}Generator{
    static function createRandom($withLabels = null){
        $obj = {{getName}}::create();
        
        {{#getAttributes}}
        $obj->set('{{getName}}', static::generate{{getNameUCFirst}}($withLabels));
        {{/getAttributes}}
        
        return $obj;
    }
    
    static function getGeneratorBuilder($field){
        $colP = {{getName}}CollectionPrepare::create()
            ->setMysqlTable('{{getName}}Generator');
        
        return $colP
    }
    
    {{#getAttributes}}
    
    static function generate{{getNameUCFirst}}($withLabels = null){
        static::get
    }
    
    {{/getAttributes}}
}
