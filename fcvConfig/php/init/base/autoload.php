<?php

use Symfony\Component\ClassLoader\Psr4ClassLoader;


//Autoloader

//autoload composer
include('vendor/autoload.php');

//autoload classes
$loader = new Psr4ClassLoader();
$loader->addPrefix('impro', __DIR__.'/php/classes/impro');
$loader->addPrefix('fcv', __DIR__.'/php/libs/fcv');
$loader->register();

//load custom mustache.php
//include("php/libs/mustache.php/src/Mustache/Autoloader.php");
include("php/libs/Mustache_2.3.1/Mustache/Autoloader.php");
Mustache_Autoloader::register();