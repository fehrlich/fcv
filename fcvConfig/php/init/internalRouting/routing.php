<?php
/*
 * This file is used to run manual url requests through the app
 */
use fcv\App;
use fcv\Routing;
include("autoload.php");

App::get()
    ->setAjax(false)
    ->load();

if(isset($_GET['url'])){
    $url = $_GET['url'];
    $routing = Routing::getObj();
    $site = $routing->getPageInstance($url);
    
    
    $site->loadContent();
    echo $site->getHtml();
}