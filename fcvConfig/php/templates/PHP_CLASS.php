<?php
namespace {{#pathToNamespace}}{{getWorkPackage.getPath}}{{/pathToNamespace}};

// <editor-fold defaultstate="collapsed" desc="uses">
//default uses 
{{#uniqueLines}}
use Exception;
use fcv\exceptions\EmptyChainException;
use fcv\exceptions\AttributeDoesNotExist;
use fcv\exceptions\ObjectDoesNotExist;
use fcv\database\DB;
use fcv\view\TemplateEngine;
use fcv\libs\Debug;
use fcv\FCVJsObj;
use fcv\traits\EventAction;
use fcv\classTraits\ViewConnector;
use fcv\classTraits\ControlConnector;
{{#isTable}}
use fcv\classTraits\TableConnector;
{{#hasHistory}}
use fcv\classTraits\TableHistory;
{{/hasHistory}}
{{/isTable}}

//use From AssociationEnds
{{#getAssociationEnds}}
use {{#substr 1}}{{getConnectionClass.getNamePath}}{{/substr 1}};
{{#getConnectionClass.getFirstTableClass.isTable}}
use {{#substr 1}}{{getConnectionClass.getFirstTableClass.getNamePath}}{{/substr 1}}CollectionPrepare;
{{/getConnectionClass.getFirstTableClass.isTable}}
{{^getOppositeAssociationEnd.isCardinalityZero}}
use {{#substr 1}}{{getOppositeAssociationEnd.getClass.getNamePath}}{{/substr 1}};
{{/getOppositeAssociationEnd.isCardinalityZero}}

{{#getParent}}
{{#isAssociationClass}}
//  use From AssociationsClasses
use {{#substr 1}}{{getNamePath}}{{/substr 1}};
use {{#substr 1}}{{getNamePath}}Collection{{/substr 1}};
{{#isTable}}
use {{#substr 1}}{{getNamePath}}CollectionPrepare{{/substr 1}};
{{/isTable}}
{{/isAssociationClass}}
{{/getParent}}
{{^isCardinalitySingle}}
use {{#substr 1}}{{getConnectionClass.getNamePath}}{{/substr 1}}Collection;
{{/isCardinalitySingle}}
{{/getAssociationEnds}}
{{#getAttributes}}
{{#getType.isClass}}
use {{#substr 1}}{{getType.getNamePath}}{{/substr 1}};
{{^isCardinalitySingle}}
use {{#substr 1}}{{getConnectionClass.getNamePath}}Collection{{/substr 1}}; 
{{/isCardinalitySingle}}
{{/getType.isClass}}
{{#getType.usesTypeParser}}
use {{#substr 1}}{{getType.getNamePath}}{{/substr 1}};
{{^isCardinalitySingle}}
use {{#substr 1}}{{getConnectionClass.getNamePath}}Collection{{/substr 1}};
{{/isCardinalitySingle}}
{{/getType.usesTypeParser}}
{{/getAttributes}}

//uses from operation parameters
{{#getOperations}}
{{#getParameters}}
{{#getType.isClass}}
use {{#substr 1}}{{getType.getNamePath}}{{/substr 1}};
{{/getType.isClass}}
{{/getParameters}}
{{/getOperations}}

//use From Workpackages
{{#addUses}}
    

{{getId}} 

{{/addUses}}

//use For Implementation
{{#getImplements}}
use {{#substr 1}}{{getNamePath}}{{/substr 1}};
{{/getImplements}}

//use From Specializations
{{#getSpecializes}}
use {{#substr 1}}{{getNamePath}}{{/substr 1}};
{{/getSpecializes}} 
{{#isSingleton}}
use fcv\traits\Singleton;
{{/isSingleton}}
{{/uniqueLines}}

// </editor-fold>

/**
 * {{getDocumentation}}
 */
{{#isInterface}}//Realizes:{{#getRealizes}} {{getName}}{{/getRealizes}}{{/isInterface}}
{{#isInterface}}interface{{/isInterface}}{{^isInterface}}class{{/isInterface}} {{getName}}
        {{#isEnum}}{{^specializeClasses}} extends \Garoevans\PhpEnum\Enum{{/specializeClasses}}{{/isEnum}}
        {{#getSpecializes}} extends {{getName}}{{/getSpecializes}} 
        {{#hasStereoType typeparser}} extends \fcv\TypeParser{{/hasStereoType typeparser}}
        {{#implementsClasses}}implements{{/implementsClasses}}{{#getImplements}}
            {{getName}}
        {{/getImplements}}{
    
    {{^isInterface}}use EventAction;{{/isInterface}}
    {{#isSingleton}}
    use Singleton;
    {{/isSingleton}}
    {{^isInterface}}
    {{^isEnum}}
    use ViewConnector;
    {{#isTable}}
    use TableConnector;
    {{#hasHistory}}
    use TableHistory;
    {{/hasHistory}}
    {{/isTable}}    
    {{/isEnum}}
    use ControlConnector;
    {{/isInterface}}
    {{PositionAttributes}}
    {{PositionMethods}}
    {{#isEnum}}
    const __default = '';
    
    static function listValues(){
        return array({{#list getLiterals}}{{^first}},{{/first}}'{{value.getName}}'{{/list getLiterals}});
    }
    static function getIndexOf($element){
        $array = static::listValues();
        return array_search($element, $array);
    }
    {{/isEnum}}
    {{^isInterface}}
    {{/isInterface}}
}