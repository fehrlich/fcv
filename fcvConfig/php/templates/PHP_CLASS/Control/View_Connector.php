
// <editor-fold defaultstate="collapsed" desc="View-Helper">
{{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
{{#getWorkPackage}}
{{#list getViews}}
static $TEMPLATE_{{value.getName}} = '{{#value}}{{#includeTemplate 2}}{{&getHtmlContent}}{{/includeTemplate 2}}{{/value}}'; //{{value.getId}}
{{/list getViews}}
{{/getWorkPackage}}
{{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}


protected function loadDynamicTemplate($templateName){
    {{#getSpecializes}}
    parent::loadDynamicTemplate($templateName);
    {{/getSpecializes}} 
    switch($templateName){
        {{#getWorkPackage}}
        {{#list getViews}}
        {{#value.getPhpContent}}
        case '{{value.getName}}':
            {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
            {{#includeCode}}{{#indent 2}}{{&value.getPhpContent}}{{/indent 2}}{{/includeCode}}
            {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
            {{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
            include('FCVAPs{{getWorkPackage.getPath}}/{{../getName}}.' . $templateName . '.php');
            {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
            break;
        {{/value.getPhpContent}}
        {{/list getViews}}
        {{/getWorkPackage}}
    }
    return $this;
}

protected function getAvailableTemplates() {
    $ret = [
        {{#getWorkPackage.getViews}}
        '{{getName}}' => 'FCVAPs{{getWorkPackage.getPath}}/{{../getName}}.{{getName}}',
        {{/getWorkPackage.getViews}}
    ];
    {{#getSpecializes}}
    $ret += array_merge($ret, parent::getAvailableTemplates());
    {{/getSpecializes}} 
    return $ret;    
}
// </editor-fold>