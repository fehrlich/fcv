
public function addToJs($selectionString = '', $tailorArray = false, $overwrite = true){
    FCVJsObj::add($this, $selectionString, $tailorArray, $overwrite);
    return $this;
}

protected $visitedByToArray = false;

public function loadJsObject($hash, $tailorArray){
    
    if($this->visitedByToArray  != $hash){
        foreach($tailorArray as $field => $tailorValue){
            if(is_string($tailorValue) && is_int($field)){
                $field = $tailorValue;
            }
            if(is_array($tailorValue)){
                $value = $this->has($field) ? $this->get($field) : null;
                if(is_object($value)){
                    $subTailor = is_array($tailorValue) ? $tailorValue : [];
                    $value = $value->loadJsObject($hash, $subTailor);
                }
            }
        }
    }else{
        return false;
    }
    
    FCVJsObj::addClass(static::class);
    FCVJsObj::addRequire('{{getWorkPackage.getPath}}');
    FCVJsObj::add($this, '', $tailorArray);
    
    return true;
}

{{^getSpecializes}} 

protected $fcvJSVariables = [];

public function addJSVariable($name, $value = null){
    if(is_null($value) && !is_string($name)){
        $this->fcvJSVariables = array_merge($this->fcvJSVariables, $name($this));
    }else{
        if(!is_string($value)) $value = $value($this);
        $this->fcvJSVariables[$name] = $value;
    }
}

{{/getSpecializes}}

public function toArray(){
    return $this->toJSObject(true, 'kopie', false, false, false, true);
}

public function toJSObject($returnArray = false, $hash = true, $addObjectAndId = true, $tailorArray = false, $tempId = false, $simple = false){

    $id = $tempId && !$this->getId() ? $tempId : $this->getId();
    if($this->visitedByToArray  == $hash && !$returnArray){
            
            if(!empty($id)){
                return array(
                    'class' => '{{getName}}',
                    'id' => $id
                );
            }else{
                throw new \Exception("{{getName}} hat keine ID");
            }
    }
    
    $retArray = array();
    
    if(is_array($tailorArray)){
        {{#getPrimaryKeyAttribute}}$retArray['{{getName}}'] = $this->getId();{{/getPrimaryKeyAttribute}}
        {{^getPrimaryKeyAttribute}}$retArray['id'] = $id;{{/getPrimaryKeyAttribute}}
        foreach($tailorArray as $field => $tailorValue){
            if(is_string($tailorValue) && is_int($field)){
                $field = $tailorValue;
            }
            if(!is_string($tailorValue) && is_callable($tailorValue)){
                $value = $tailorValue($this, $hash);
            }else{
                $value = $this->has($field) ? $this->get($field) : null;
                $subTailor = is_array($tailorValue) ? $tailorValue : false;
                if(is_object($value)){
                    if($value instanceof \fcv\TypeParser){
                        FCVJsObj::addClass(get_class($value));
                        $value = $value->toString();
                    }else{
                        $value = $value->toJSObject(false, $hash, $addObjectAndId, $subTailor);
                    }
                }
            }
            /* Needed if simple also should return metavars like created/createdBy
            if($field == 'createdBy'){
                if(!is_object($this->createdBy)){
                    $value = $this->createdBy;
                }else{
                    $value = $this->createdBy->toString();
                }
            }*/
            
            $fieldChain = explode('.', $field);
            $chain = &$retArray;
            foreach($fieldChain as $subField){
                if(!isset($chain[$subField])) $chain[$subField] = array();
                $chain = &$chain[$subField];
            }
            $chain = $value;
        }
    }else{
    
        {{#getSpecializes}} 
        $retArray = parent::toJSObject($returnArray, $hash, $addObjectAndId);
        if($returnArray) $retArray = $retArray['data'];
        {{/getSpecializes}}
        $this->visitedByToArray = $hash;
        {{^getPrimaryKeyAttribute}}$retArray['id'] = $id;{{/getPrimaryKeyAttribute}}
        {{#getAttributes}}
        if(isSet($this->{{getName}}) && !is_null($this->{{getName}})){
            {{#getType.usesTypeParser}}
            FCVJsObj::addClass({{getType.getName}}::class);
            {{/getType.usesTypeParser}}
            {{^getType.usesTypeParser}}
            {{#isClass}}
            if(!is_object($this->{{getName}})){
                FCVJsObj::addClass({{getType.getName}}::class);
            }
            {{/isClass}}
            {{/getType.usesTypeParser}}
            $val = $this->{{getName}};
            if(!is_object($val)){
                $retArray['{{getName}}'] = $val;
            }else{
                $retArray['{{getName}}'] = ({{#isClass}}$simple ? $this->{{getName}}->getId() : $this->{{getName}}->toJSObject(false, $hash, $addObjectAndId){{/isClass}}{{^isClass}}$this->{{getName}}{{#getType.usesTypeParser}}->toString(){{/getType.usesTypeParser}}{{/isClass}});
            }
        }
        {{/getAttributes}}

        {{#getAssociationEnds}}
        {{^isCardinalityZero}}
        if(isSet($this->{{getName}}) && !is_null($this->{{getName}})){
            if(!is_object($this->{{getName}})){
                FCVJsObj::addClass({{getType.getName}}::class);
                $val = $this->{{getName}};
            }else{
                {{#getType.usesTypeParser}}
                FCVJsObj::addClass({{getType.getName}}::class);
                {{/getType.usesTypeParser}}
                {{^isCardinalitySingle}}
                $val = array();
                foreach($this->{{getName}} as $o){
                    $val[] = {{#isClass}}$simple ? $o->getId() : $o->toJSObject(false, $hash, $addObjectAndId){{/isClass}}{{^isClass}}$this->{{getName}}{{#getType.usesTypeParser}}->toString(){{/getType.usesTypeParser}}{{/isClass}};
                }            
                {{/isCardinalitySingle}}
                {{#isCardinalitySingle}}
                $val = {{#isClass}}$simple ? $this->{{getName}}->getId() : $this->{{getName}}->toJSObject(false, $hash, $addObjectAndId){{/isClass}}{{^isClass}}$this->{{getName}}{{#getType.usesTypeParser}}->toString(){{/getType.usesTypeParser}}{{/isClass}};
                {{/isCardinalitySingle}}
            }
            $retArray['{{getName}}'] = $val;
        }
        {{/isCardinalityZero}}
        {{#getParent.isAssociationClass}}
        {{^isAssociationClass}}
        if(isSet($this->{{getParent.getNameLCFirst}}) && !is_null($this->{{getParent.getNameLCFirst}})){
            if(!is_object($this->{{getParent.getNameLCFirst}})) $val = $this->{{getParent.getNameLCFirst}};
            else{
                $val = $simple ? $this->{{getParent.getNameLCFirst}}->getId() : $this->{{getParent.getNameLCFirst}}->toJSObject(false, $hash, $addObjectAndId);
            }
            $retArray['{{getParent.getNameLCFirst}}'] = $val;
        }
        {{/isAssociationClass}}
        {{/getParent.isAssociationClass}}

        {{#isAssociationClass}}
        //is AssocClass
        if(isSet($this->{{getType.getNameLCFirst}}) && !is_null($this->{{getType.getNameLCFirst}})){
            if(!is_object($this->{{getType.getNameLCFirst}})) $val = $this->{{getType.getNameLCFirst}};
            else{
                $val = $simple ? $this->{{getType.getNameLCFirst}}->getId() : $this->{{getType.getNameLCFirst}}->toJSObject(false, $hash, $addObjectAndId);
            }
            $retArray['{{getType.getNameLCFirst}}'] = $val;
        }
        {{/isAssociationClass}}

        {{/getAssociationEnds}}
        
        {{^getSpecializes}}
        if(isSet($this->createdBy) && !is_null($this->createdBy)){
            if(!is_object($this->createdBy)){
                $val = $this->createdBy;
            }else{
                $val = $this->createdBy->toString();
            }
            $retArray['createdBy'] = $val;
        }
        
        if(isSet($this->created) && !is_null($this->created)){
            if(!is_object($this->created)){
                $val = $this->created;
            }else{
                $val = $this->created->toString();
            }
            $retArray['created'] = $val;
        }
        
        if(isSet($this->lastModified) && !is_null($this->lastModified)){
            if(!is_object($this->lastModified)){
                $val = $this->lastModified;
            }else{
                $val = $this->lastModified->toString();
            }
            
            $retArray['lastModified'] = $val;
        }
        {{/getSpecializes}}
        
    }
    
    foreach($this->fcvJSVariables as $jsVarName => $jsVarValue){
        $retArray[$jsVarName] = $jsVarValue;
    }
    
    FCVJsObj::addClass(static::class);
    if(!$addObjectAndId) return $retArray;
    
    return array(
        'class' => '{{getName}}',
        'id' => $id,
        'data' => ($returnArray)?$retArray:null
    );
}