
{{^getSpecializes}}
protected $lastModified = null;
protected $created = null;
protected $createdBy = null;
{{#hasActive}}
protected $active = true;
{{/hasActive}}
{{/getSpecializes}}

public function __construct(){
    
    {{#getSpecializes}}
    parent::__construct();
    {{/getSpecializes}}
    {{#hasConstructor}}
    {{#getWorkPackage}}
    {{#getProject.getCurrentBuild.getBuildWorkpackages}}
    include("FCVAPs{{getPath}}/{{getName}}.php");
    {{/getProject.getCurrentBuild.getBuildWorkpackages}}
    {{^getProject.getCurrentBuild.getBuildWorkpackages}}
     //CodeInclude
    {{#includeCode}}{{#indent 2}}{{#singleParse}}{{&getFunctionContent}}{{/singleParse}}{{/indent 2}}{{/includeCode}}
    {{/getProject.getCurrentBuild.getBuildWorkpackages}}
    {{/getWorkPackage}}
    {{/hasConstructor}}
}

// <editor-fold defaultstate="collapsed" desc="factory methods and helper">
 
/**
 * creates an Instance of {{getName}}
 * @return {{getName}}
 */ 
public static function create(){
    $ret = new {{getName}}();
    return $ret;
}



/**
 * creates an Instance of {{getName}} from an array. class datatypes also need 
 * to be an array
 * @return {{getName}}
 */ 
public static function createFromArray($data, $prevObject = null){
    if(is_null($prevObject) ||  empty($prevObject)){
        $prevObject = {{getName}}::create();
    }else{
        if(!($prevObject instanceof static)){
            $prevObject = static::load($prevObject);
        }
    }
    
    $prevObject->loadFromArray($data);    
    return $prevObject;
}

/**
 * update current object based on an array [fieldname : mixed], the value of an
 * attribute can also be an array, which creates an object based on the attribute
 * this methods also calls parents classes if they existt
 * @return {{getName}}
 */
public function loadFromArray($data){
    {{#getSpecializes}}
    parent::loadFromArray($data);
    {{/getSpecializes}}
    $this->loadFromArrayPartial($data);
    $this->trigger('loadFromArray', $data);
    return $this;
}

/**
 * update current object based on an array [fieldname : mixed], the value of an
 * attribute can also be an array, which creates an object based on the attribute
 * this method only updates owned attributes, not attributes from more generic classes 
 * @return {{getName}}
 */
private function loadFromArrayPartial($data){
    foreach($data as $fieldName => $value){
        switch((string) $fieldName){
            {{#getAttributes}}
            {{^isStatic}}
            case '{{getName}}':
                {{^isDefaultType}}
                if(is_array($value)){
                    $this->{{getName}} = {{getType.getName}}::createFromArray($value, $this->{{getName}});
                    {{#getType.isTable}}
                    if({{getType.getName}}::isIdObject($value)){
                        $this->{{getName}} = $this->{{getName}}->getId();
                    }
                    {{/getType.isTable}}
                {{/isDefaultType}}
                {{^isDefaultType}}}else{{/isDefaultType}}if(!is_null($value)){
                    $this->{{getName}} = {{#getType.usesTypeParser}}{{getType.getName}}::parse($value{{#getType.hasPartialParse}}, $this->{{getName}}{{/getType.hasPartialParse}}){{/getType.usesTypeParser}}{{^getType.usesTypeParser}}$value{{/getType.usesTypeParser}};
                }
                break;
            {{/isStatic}}
            {{/getAttributes}}
            //associationEnds
            {{#getAssociationEnds}}
            {{^isCardinalityZero}}
            {{#isAssociationClass}}
            //For AssociationClasses {{getName}}
            case '{{getConnectionClass.getNameLCFirst}}':
                $this->{{getConnectionClass.getNameLCFirst}} = (is_array($value))?{{getConnectionClass.getName}}::createFromArray($value, $this->{{getConnectionClass.getNameLCFirst}}):$value;
                {{#getConnectionClass.isTable}}
                if({{getConnectionClass.getName}}::isIdObject($value)){
                    $this->{{getConnectionClass.getNameLCFirst}} = $this->{{getConnectionClass.getNameLCFirst}}->getId();
                }
                {{/getConnectionClass.isTable}}
                {{#isCardinalitySingle}}
                if($this->{{getConnectionClass.getNameLCFirst}}  instanceof {{getConnectionClass.getName}}){
                    $this->{{getConnectionClass.getNameLCFirst}}->set{{getParent.getNameUCFirst}}($this);
                }
                {{/isCardinalitySingle}}
                break;
            
            {{/isAssociationClass}}
            //Regular AssociationEnd {{getParent.getName}} ({{getParent.debugType}})
            {{^isAssociationClass}}
            {{#getParent.isAssociationClass}}
            //AssociationEnd is from AssociationClass
            case '{{getNameLCFirst}}':
                    {{#isCardinalitySingle}}
                    $this->{{getNameLCFirst}} = (is_array($value))?{{getConnectionClass.getName}}::createFromArray($value, $this->{{getNameLCFirst}}):$value;
                    //{{getConnectionClass.getName}}: {{getConnectionClass.isTableOrParent}}
                    {{#getConnectionClass.isTableOrParent}}
                    if({{getConnectionClass.getName}}::isIdObject($value)){
                        $this->{{getNameLCFirst}} = $this->{{getNameLCFirst}}->getId();
                    }
                    {{/getConnectionClass.isTableOrParent}}
                    {{/isCardinalitySingle}}
                    {{^isCardinalitySingle}}
                    $newVal = new {{getConnectionClass.getName}}Collection();
                foreach($value as $el){
                    $newVal->add({{getConnectionClass.getName}}::createFromArray($el)); //does not reload data if allready exists
                    }
                $this->{{getNameLCFirst}} = $newVal;
                    {{/isCardinalitySingle}}

                    {{#getOppositeAssociationEnd.isCardinalitySingle}}
                    if($this->{{getNameLCFirst}} instanceof {{getConnectionClass.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}}){
                        $this->{{getNameLCFirst}}->set{{getOppositeAssociationEnd.getNameUCFirst}}($this);
                    }
                    {{/getOppositeAssociationEnd.isCardinalitySingle}}
                break;
            {{/getParent.isAssociationClass}}

            {{#getParent.isAssociationClass}}
            {{^isAssociationClass}}

            //value from connected AssociationClass
            case '{{getParent.getNameLCFirst}}':
                if({{#isCardinalitySingle}}!is_array($value) || {{/isCardinalitySingle}}!is_object(current($value))){
                    {{#isCardinalitySingle}}
                    $this->{{getParent.getNameLCFirst}} = {{getParent.getNameUCFirst}}::createFromArray($value, $this->{{getParent.getNameLCFirst}});
                    {{/isCardinalitySingle}}
                    {{^isCardinalitySingle}}
                    $newVal = new {{getParent.getName}}Collection();
                    $onlyIds = true;
                    foreach($value as $el){
                        $elObj = {{getParent.getName}}::createFromArray($el);
                        $onlyIds = $onlyIds && is_array($el) && {{getParent.getName}}::isIdObject($el);
                        $newVal->add($elObj); //does not reload data if allready exists
                    }
                    $this->{{getParent.getNameLCFirst}} = $onlyIds ? null : $newVal;
                    {{/isCardinalitySingle}}

                }else{
                    $this->{{getParent.getNameLCFirst}} = $value;

                    {{#getParent.isTable}}
                    if({{getParent.getName}}::isIdObject($value)){
                        $this->{{getParent.getNameLCFirst}} = $this->{{getParent.getNameLCFirst}}->getId();
                    }
                    {{/getParent.isTable}}
                }
                if($this->{{getParent.getNameLCFirst}} instanceof {{getParent.getNameUCFirst}}){
                    $this->{{getParent.getNameLCFirst}}->set{{getOppositeAssociationEnd.getType.getNameUCFirst}}($this);
                }
                break;
            {{/isAssociationClass}}
            {{/getParent.isAssociationClass}}
            case '{{getNameLCFirst}}':
                    {{#isCardinalitySingle}}
                    $this->{{getNameLCFirst}} = (is_array($value))?{{getConnectionClass.getName}}::createFromArray($value, $this->{{getNameLCFirst}}):$value;

                    {{#getConnectionClass.isTable}}
                    if({{getConnectionClass.getName}}::isIdObject($value)){
                        $this->{{getNameLCFirst}} = $this->{{getNameLCFirst}}->getId();
                    }
                    {{/getConnectionClass.isTable}}
                    {{/isCardinalitySingle}}
                    {{^isCardinalitySingle}}
                    $newVal = new {{getConnectionClass.getName}}Collection();
                foreach($value as $el){
                    $newVal->add({{getConnectionClass.getName}}::createFromArray($el)); //does not reload data if allready exists
                    }
                $this->{{getNameLCFirst}} = $newVal;
                    {{/isCardinalitySingle}}

                    {{#getOppositeAssociationEnd.isCardinalitySingle}}
                    {{^getOppositeAssociationEnd.isCardinalityZero}}
                    if($this->{{getNameLCFirst}} instanceof {{getConnectionClass.getName}}{{^isCardinalitySingle}}Collection{{/isCardinalitySingle}}){
                        $this->{{getNameLCFirst}}->set{{getOppositeAssociationEnd.getNameUCFirst}}($this);
                    }
                    {{/getOppositeAssociationEnd.isCardinalityZero}}
                    {{/getOppositeAssociationEnd.isCardinalitySingle}}
                break;
            {{/isAssociationClass}}
            {{/isCardinalityZero}}
            {{/getAssociationEnds}}

            {{^getSpecializes}}
            //Helper
            case 'lastModified':
                $this->setLastModified($value);
                break;
            case 'created':
                $this->setCreated($value);
                break;
            case 'createdBy':
                $this->setCreatedBy($value);
                break;
            {{#hasActive}}
            case 'active':
                $value ? $this->activate() : $this->deactivate();
                break;
            {{/hasActive}}
            {{/getSpecializes}}
        }
    }
    return $this;
}

// </editor-fold>

{{#isPageOrFrame}}
/**
 * loads the content from this frame/page
 */
function loadContent(){
    {{#getWorkPackage}}
    {{#getProject.getCurrentBuild.getBuildWorkpackages}}
    include("FCVAPs{{getPath}}/{{getName}}.php");
    {{/getProject.getCurrentBuild.getBuildWorkpackages}}
    {{^getProject.getCurrentBuild.getBuildWorkpackages}}
     //CodeInclude getFunctionContent
    //{{#includeCode}}{{#indent 2}}{{#singleParse}}{{&getFunctionContent}}{{/singleParse}}{{/indent 2}}{{/includeCode}}
    {{/getProject.getCurrentBuild.getBuildWorkpackages}}
    {{/getWorkPackage}}
}

{{/isPageOrFrame}}
{{#isTable}}
{{#hasPrimaryKeyAttributes}}
/**
 * return true if the given array only has primary keys as keys in it
 */
public static function isIdObject($data){
//    if(!is_array($data) && !is_null($data)) return true;
    $is = (is_array($data) && count($data) == {{countMultiplePrimaryKeyAttributes}});
    {{#getPrimaryKeyAttributes}}
    $is = $is && isset($data['{{getName}}']);
    {{/getPrimaryKeyAttributes}}
    return $is;
}

{{/hasPrimaryKeyAttributes}}
{{/isTable}}
{{^getSpecializes}}
public function hasLastModifiedTime(){
    return !is_null($this->lastModifiedTime);
}

public function hasCreated(){
    return !is_null($this->created);
}

public function hasCreatedBy(){
    return !is_null($this->createdBy);
}

public function getLastModifiedTime(){
    return $this->lastModifiedTime;
}

public function setLastModifiedTime($time){
    $this->lastModifiedTime = $time;
    return $this;
}

public function getCreatedBy(){
    return \fcv\database\DB::getCreatedByText($this->createdBy);
}

public function setCreatedBy($time){
    $this->createdBy = $time;
    return $this;
}

{{/getSpecializes}}
public function hasPrimaryKey(){
    return {{#hasPrimaryKeyAttributes}}true{{/hasPrimaryKeyAttributes}}{{^hasPrimaryKeyAttributes}}false{{/hasPrimaryKeyAttributes}};
}

{{#hasPrimaryKeyAttributes}}
{{#hasMultiplePrimaryKeyAttributes}}
public function {{getIdFunctionName}}(){
    $ret = array();
    {{#getPrimaryKeyAttributes}}
    $ret[] = (is_object($this->{{getNameLCFirst}}))?$this->{{getNameLCFirst}}->getId():$this->{{getNameLCFirst}};
    {{/getPrimaryKeyAttributes}}
    return implode('_', $ret);
}
public function {{setIdFunctionName}}($id){
    {{#getPrimaryKeyAttributes}}
    //$this->{{getNameLCFirst}} = array_pop($id); //TODO: fix
    {{/getPrimaryKeyAttributes}}
    return $this;
}
public function getPrimaryKeyName(){
    return "{{getName}}";
}
{{/hasMultiplePrimaryKeyAttributes}}
{{^hasMultiplePrimaryKeyAttributes}}
{{#getPrimaryKeyAttribute}}
public function {{getIdFunctionName}}(){
    if(is_object($this->{{getName}})){
        return $this->{{getName}}->getId();
    }
    if(!empty($this->{{getName}})){
        return $this->{{getName}};
    }
    return null;
}
public function {{setIdFunctionName}}($id){
    $oldId = $this->getId();
    $this->{{getName}} = $id;
    $this->trigger('idUpdated', $this, $oldId);
    return $this;
}
public function getPrimaryKeyName(){
    return "{{getName}}";
}
{{/getPrimaryKeyAttribute}}
{{/hasMultiplePrimaryKeyAttributes}}
{{/hasPrimaryKeyAttributes}}

{{^hasPrimaryKeyAttributes}}
public function {{getIdFunctionName}}(){
    return 'undefined';
}
{{/hasPrimaryKeyAttributes}}

{{#isDefaultType}}
public function __toString(){
    return $this->toString();
}

{{/isDefaultType}}

{{#usesTypeParser}}
private $isNull = false;

/**
 * @return string representation of current value
 */
public function __toString(){
    return $this->toString();
}

/**
 * @return boolean set null-state of current object
 */
public function setNull($null){
    $this->isNull = $null;
    return $this;
}

/**
 * @return boolean if current object represents null value
 */
public function isNull(){
    return $this->isNull;
}

/**
 * return string value of current type or null if value is set to null
 * @return string
 */
public function val(){
    if($this->isNull) return null;
    return $this->toString();
}

/**
 * creates a null-object of this type
 * @return {{getName}} null-object
 */
public static function null(){
    return {{getName}}::create()->setNull(true);
}
{{/usesTypeParser}}