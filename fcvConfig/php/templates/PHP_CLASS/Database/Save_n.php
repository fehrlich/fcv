private $treeSaved = false;
/**
 * Saves the Object Data and every child Object
 * @return {{getName}} this Object
 */
public function treeSave(){
    {{#getSpecializes}}
    parent::treeSave();
    {{/getSpecializes}}
    if($this->treeSaved) return;
    $this->treeSaved = true;
    $this->save();
    {{#getAttributes}}
    {{#isClass}}
    if($this->{{getName}} && is_object($this->{{getName}})) $this->{{getName}}->treeSave();
    {{/isClass}}
    {{/getAttributes}}
    {{#getAssociationEnds}}
    if($this->{{getName}} && is_object($this->{{getName}})) $this->{{getName}}->treeSave();
    {{/getAssociationEnds}}

    return $this;
}
/**
 * Saves the Object Data
 * @return {{getName}} this Object
 */
public function {{getSaveOperation}}(){
    $insertObj = array();
    {{#getSpecializes}}
    $id = array('i', parent::save()->getId());
    $insertObj['{{getPrimaryKeyAttribute.getName}}'] = $id;
    {{/getSpecializes}}
    //New {{getName}}
    $mysqli = mys::getObj();

    {{#hasHistory}}
    $sql = 'INSERT INTO `{{getName}}History` SELECT * FROM `{{getName}}` WHERE {{getPrimaryKeyAttribute.getName}} = '.$this->getId().';';
    $mysqli->query($sql);
    echo $mysqli->error;
    {{/hasHistory}}
    {{#getAttributes}}
        {{^hasStereoType noDBField}}
        {{^isClass}}if(isSet($this->{{getName}})) $insertObj['{{getName}}'] = $this->{{getName}}{{#getType.usesTypeParser}}->toString(){{/getType.usesTypeParser}}{{#isClass}}->getId(){{/isClass}};{{/isClass}}
        {{#isClass}}if(isSet($this->{{getName}})){
            $insertObj['{{getName}}'] =  (!is_object($this->{{getName}}))?$this->{{getName}}:$this->{{getName}}->getId();
        }{{/isClass}}
        {{/hasStereoType noDBField}}
    {{/getAttributes}} 

    {{#isAssociationClass}}
     {{#getAssociationEnds}} 
        {{^hasStereoType noDBField}}
    if($this->{{getConnectionClass.getNameLCFirst}}){
        if(is_numeric($this->{{getConnectionClass.getNameLCFirst}})) $insertObj['{{getConnectionClass.getNameLCFirst}}'] = $this->{{getConnectionClass.getNameLCFirst}};
        else $insertObj['{{getConnectionClass.getNameLCFirst}}'] = $this->{{getConnectionClass.getNameLCFirst}}->getId();
    }
        {{/hasStereoType noDBField}}
    {{/getAssociationEnds}}
    {{/isAssociationClass}}
    {{^isAssociationClass}}
    {{#getAssociationEnds}} 
    {{#getParent.isAssociationClass}}
//        if($this->{{getNameLCFirst}}){
//            if(is_numeric($this->{{getNameLCFirst}})) $insertObj['{{getNameLCFirst}}'] = $this->{{getNameLCFirst}};
//            else $insertObj['{{getNameLCFirst}}'] = $this->{{getNameLCFirst}}->getId();
//        }
    {{/getParent.isAssociationClass}}
    {{^getParent.isAssociationClass}}
    {{#isCardinalitySingle}}
        if($this->{{getName}}){
            if(!is_object($this->{{getName}})) $insertObj['{{getName}}'] = $this->{{getName}};
            else $insertObj['{{getName}}'] = $this->{{getName}}->getId();
        }
    {{/isCardinalitySingle}}
    {{/getParent.isAssociationClass}}
    {{/getAssociationEnds}}
    {{/isAssociationClass}}


//        if($this->getId() > 0){
//            $sql = "UPDATE `{{getName}}{{#getSpecializes}}_sub{{/getSpecializes}}`
//                    SET ".implode("','", array_map(function($d) use ($mysqli){
//                    return $key." = '".$mysqli->real_escape_string($key, $d)."'";
//                }, $insertObj))." WHERE id = '".$this->getId()."'";
//        }
    $sql = "INSERT INTO `{{getName}}{{#getSpecializes}}_sub{{/getSpecializes}}` 
            (`".implode('`,`', array_keys($insertObj))."`{{^getSpecializes}}, `lastModified`, `created`{{/getSpecializes}})
        VALUES
            ('".implode("','", array_map(function($d) use ($mysqli){
                if(is_object($d[1])) $d[1] = serialize ($d[1]);
                return $mysqli->real_escape_string($d[1]);
            }, $insertObj))."'{{^getSpecializes}}, NOW(), NOW(){{/getSpecializes}}) ON DUPLICATE KEY UPDATE ".implode(",", array_map(function($d,$key) use ($mysqli){
                if(is_object($d[1])) $d[1] = serialize ($d[1]);
                return "`".$key."` = '".$mysqli->real_escape_string($d[1])."'";
            }, $insertObj, array_keys($insertObj)))."{{^getSpecializes}}, `lastModified` = NOW(){{/getSpecializes}}{{#hasHistory}}, `version` = version+1{{/hasHistory}}";
//        $prepared = $mysqli->prepare("INSERT INTO `{{getName}}{{#getSpecializes}}_sub{{/getSpecializes}}` 
//                (`".implode('`,`', array_keys($insertObj))."`)
//            VALUES
//                ('".implode("','", array_fill(0, count($insertObj), '?'))."')
//            ");
//        if(!$prepared) echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
//        foreach($insertObj as $v){
//            $b = $prepared->bind_param('s', $v);
//            if(!$b) echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//        }
    $mysqli->query($sql);
    $error = $mysqli->error;
    if($error){
        echo 'Mysql Error: '.$mysqli->error.'<br>';
        echo 'beim speichern des eines Objektes der Klasse "{{getName}}"<br >';
        echo 'Objekt: <br >';
        echo 'SQL:'.$sql;
        d($insertObj);
        d($this);
        exit();
    }
    if($this->hasPrimaryKey() && !$this->getId()) $this->setId($mysqli->insert_id);
    return $this;
}

public static function getFieldValues($fieldName, $filterObj = null, $filterFieldName = ''){
    $mysqli = mys::getObj();
    $fieldName = $mysqli->real_escape_string($fieldName);
    $rows = array();

    //GET TYPE
    $where = '1';
    if(!is_null($filterObj)){
        if(isset({{getName}}::$filterFieldName)){
            $where = '`'.$filterFieldName.'` IN 
                (SELECT DISTINCT `'.$filterObj->getPrimaryKeyName().'` as `'.$fieldName.'` FROM `'.get_class($filterObj).'` WHERE '.$filterObj->toSearchSqlString().')';
        }else{
            $where = '`{{getPrimaryKeyAttribute.getName}}` IN 
                (SELECT DISTINCT `'.$filterFieldName.'` as `{{getPrimaryKeyAttribute.getName}}` FROM `'.get_class($filterObj).'` WHERE '.$filterObj->toSearchSqlString().')';
        }
    }

    $sql = "SELECT DISTINCT `".$fieldName."` FROM `{{getName}}` WHERE ".$where;
    $result = $mysqli->query($sql);
    echo mysql_error();
    while($row = $result->fetch_assoc()){
        $rows[] = $row[$fieldName];
    }
    return $rows;
}

public function toSearchSqlString(){
    $obj = $this->toArray(true);
    if(count($obj) == 0) $searchWhere = array(1);
    else{
        $searchWhere = array();
        $objKeys = array_keys($obj);
        {{#getAttributes}}
        if(in_array('{{getName}}', $objKeys) && !empty($obj['{{getName}}'])) $searchWhere[] = "`{{getName}}` LIKE '%".$obj['{{getName}}']."%'";
        {{/getAttributes}}
        if(count($searchWhere == 0)) $searchWhere = array(1);
    }
    return implode(',', $searchWhere);
}

/**
 * Loads {{getName}} object from Id
 * @return {{getName}} object
 * @throws Exception
 */
public static function load($id){
    //New {{getName}}
    $mysqli = mys::getObj();

    $sql = "SELECT * FROM `{{getName}}` WHERE `{{getPrimaryKeyAttribute.getName}}` = '".$id."'";
    $result = $mysqli->query($sql);
    $row = $result->fetch_assoc();
    if($result->num_rows == 0){
        throw new Exception('Object {{getName}} with Id '.$id.' not found.');
    }
    return new {{getName}}($row);
}
