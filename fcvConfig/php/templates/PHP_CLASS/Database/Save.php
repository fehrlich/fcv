
// <editor-fold defaultstate="collapsed" desc="persistent layer information">
    
static $fcvHasHistory = {{#hasHistory}}true{{/hasHistory}}{{^hasHistory}}false{{/hasHistory}};
{{#hasHistory}}static $fcvTable = '{{getName}}';{{/hasHistory}}
{{#hasHistory}}static $fcvHistoryTable = '{{getName}}History';{{/hasHistory}}
static $fcvHasActive = {{#hasActive}}true{{/hasActive}}{{^hasActive}}false{{/hasActive}};
static $fcvPrimaryKeyAttributes = [
    {{#getPrimaryKeyAttributes}}
    '{{getNameLCFirst}}',
    {{/getPrimaryKeyAttributes}}
];

protected static $fcvDbFields = [
    {{^getSpecializes}}
    {{#isAssociationClass}}
    {{^hasPrimaryKeyAttribute}}
    {{#getAssociationEnds}}
    '{{getType.getNameLCFirst}}',
    {{/getAssociationEnds}}
    {{/hasPrimaryKeyAttribute}}
    {{#hasPrimaryKeyAttribute}}
    '{{getPrimaryKeyAttribute.getName}}',
    {{/hasPrimaryKeyAttribute}}
    {{/isAssociationClass}}
    {{^isAssociationClass}}
    '{{getPrimaryKeyAttribute.getName}}',
    {{/isAssociationClass}}
    {{/getSpecializes}}
    
    {{#getAttributes}}
    {{^isStatic}}
    {{^hasStereoType noDBField}}
    '{{getName}}',
    {{/hasStereoType noDBField}}
    {{/isStatic}}
    {{/getAttributes}} 
    {{#isAssociationClass}}
    {{#getAssociationEnds}} 
    {{^isStatic}}
    {{^isCardinalityZero}}
    {{^hasStereoType noDBField}}
    '{{getConnectionClass.getNameLCFirst}}',
    {{/hasStereoType noDBField}}
    {{/isCardinalityZero}}
    {{/isStatic}}
    {{/getAssociationEnds}}
    {{/isAssociationClass}}
    {{^isAssociationClass}}
    {{#getAssociationEnds}} 
    {{^isStatic}}
    {{^isCardinalityZero}}
    {{^getParent.isAssociationClass}}
    {{#isCardinalitySingle}}
    '{{getName}}',
    {{/isCardinalitySingle}}
    {{/getParent.isAssociationClass}}
    {{/isCardinalityZero}}
    {{/isStatic}}
    {{/getAssociationEnds}}
    {{/isAssociationClass}}
];

// <editor-fold defaultstate="collapsed" desc="save-NM-Relations">
{{#getAssociationEnds}}
{{#getParent.isRelationNM}}
{{#getOppositeAssociationEnd.getClass.isTable}}
/**
 * saves the relational "{{getNameUCFirst}}" Table for this object
 * @return static
 */
public function save{{getNameUCFirst}}(){
    if($this->{{getName}} && is_object($this->{{getName}})){
        $q = new \fcv\database\QueryBuilder();
        $q->from('{{getParent.getName}}');
        $q->where('{{getOppositeAssociationEnd.getConnectionClass.getNameLCFirst}}', '=', $this->getId());
        $q->delete();

        $q = new \fcv\database\QueryBuilder();
        $q->from('{{getParent.getName}}');
        foreach($this->{{getName}} as ${{getName}}){
            $q->insert(array(
                '{{getConnectionClass.getNameLCFirst}}' => ${{getName}}->getId(),
                '{{getOppositeAssociationEnd.getConnectionClass.getNameLCFirst}}' => $this->getId()
            ));
        }
    }
    return $this;
}
{{/getOppositeAssociationEnd.getClass.isTable}}
{{/getParent.isRelationNM}}
{{/getAssociationEnds}}
// </editor-fold>
    
/**
 * returns a CollectionPrepare-Instance for {{getName}}
 * @return {{getName}}CollectionPrepare
 */
public static function getCollectionPrepare($id = null){

    {{#hasMultiplePrimaryKeyAttributes}}
    $id = is_string($id) ? explode('_', $id) : $id;
    {{/hasMultiplePrimaryKeyAttributes}}
    
    $col = {{getName}}CollectionPrepare::create();
    if(!is_null($id)){
        {{^hasPrimaryKeyAttribute}}
        {{#isAssociationClass}}
        $index = 0;
        {{#getAssociationEnds}}
        $col->where('{{getType.getNameLCFirst}}', '=', $id[$index++]);
        {{/getAssociationEnds}}
        {{/isAssociationClass}}
        {{/hasPrimaryKeyAttribute}}

        {{#hasPrimaryKeyAttribute}}
        {{#getPrimaryKeyAttributes}}
        $col->where('{{getName}}', '=', $id);
        {{/getPrimaryKeyAttributes}}
        {{/hasPrimaryKeyAttribute}}
    }
    return $col;
}

// <editor-fold defaultstate="collapsed" desc="deprecated, used for import, can be deletet after">

/**
 * Only used for the import because its possible to create specific id's with this function
 * can be deleted after last import
 * @deprecated
 * @return {{getName}} this Object
 */
public function {{getSaveOperation}}OldAndImport($useHistory = true, $updateCreated = false){
    $this->trigger('beforeSave');
    $insertObj = array();
    {{#getSpecializes}}
    $insertObj['{{getPrimaryKeyAttribute.getName}}'] = parent::saveOldAndImport()->getId();
    {{/getSpecializes}}
    {{^getSpecializes}}
    {{#isAssociationClass}}
    {{#getAssociationEnds}}
    $insertObj['{{getType.getNameLCFirst}}'] = ((is_object($this->{{getType.getNameLCFirst}}))?$this->{{getType.getNameLCFirst}}->getId():$this->{{getType.getNameLCFirst}});
    if(is_null($insertObj['{{getType.getNameLCFirst}}'])) $insertObj['{{getType.getNameLCFirst}}'] = 0;
    {{/getAssociationEnds}}
    {{/isAssociationClass}}{{^isAssociationClass}}
    $insertObj['{{getPrimaryKeyAttribute.getName}}'] = $this->getId()?$this->getId():'';
    {{/isAssociationClass}}
    {{/getSpecializes}}
    //New {{getName}}
    $mysql = DB::getObj();
    
    {{^getSpecializes}}
    $created = ($this->created)?"'".$this->created->format('Y-m-d H:i:s')."'":'NOW()';
    $lastModified = ($this->lastModified)?"'".$this->lastModified->format('Y-m-d H:i:s')."'":'NOW()';
    $createdBy = ($this->createdBy && !is_array($this->createdBy))?$this->createdBy:"NULL";
    {{#hasActive}}
    $insertObj['active'] = $this->active;
    {{/hasActive}}
    {{/getSpecializes}}
    
    {{#hasHistory}}
    if($this->getId() && $useHistory){        
        $sql = 'INSERT INTO `{{getName}}History` '
            . 'SELECT *,COALESCE(('
            . 'SELECT version FROM `{{getName}}History` WHERE {{getPrimaryKeyAttribute.getName}} = '.$this->getId().' ORDER BY version DESC  LIMIT 1'
            . '),0)+1 as version '
            . 'FROM `{{getName}}` WHERE {{getPrimaryKeyAttribute.getName}} = '.$this->getId().';';
        $mysql->query($sql);
    }
    {{/hasHistory}}
    {{#getAttributes}}
    {{^isStatic}}
        {{^hasStereoType noDBField}}
        {{^isClass}}if(isSet($this->{{getName}})) $insertObj['{{getName}}'] = $this->{{getName}}{{#getType.usesTypeParser}}->val(){{/getType.usesTypeParser}}{{#isClass}}->getId(){{/isClass}};{{/isClass}}
        {{#isClass}}if(isSet($this->{{getName}})){
            $insertObj['{{getName}}'] =  (!is_object($this->{{getName}}))?$this->{{getName}}:$this->{{getName}}->getId();
        }{{/isClass}}
        {{/hasStereoType noDBField}}
    {{/isStatic}}
    {{/getAttributes}} 

    {{#isAssociationClass}}
     {{#getAssociationEnds}} 
     {{^isStatic}}
        {{^isCardinalityZero}}
        {{^hasStereoType noDBField}}
    if($this->{{getConnectionClass.getNameLCFirst}}){
        if(is_numeric($this->{{getConnectionClass.getNameLCFirst}})) $insertObj['{{getConnectionClass.getNameLCFirst}}'] = $this->{{getConnectionClass.getNameLCFirst}};
        else $insertObj['{{getConnectionClass.getNameLCFirst}}'] = $this->{{getConnectionClass.getNameLCFirst}}->getId();
    }
        {{/hasStereoType noDBField}}
        {{/isCardinalityZero}}
     {{/isStatic}}
    {{/getAssociationEnds}}
    {{/isAssociationClass}}
    {{^isAssociationClass}}
    {{#getAssociationEnds}} 
    {{^isStatic}}
    {{^isCardinalityZero}}
    {{#getParent.isAssociationClass}}
//        if($this->{{getNameLCFirst}}){
//            if(is_numeric($this->{{getNameLCFirst}})) $insertObj['{{getNameLCFirst}}'] = $this->{{getNameLCFirst}};
//            else $insertObj['{{getNameLCFirst}}'] = $this->{{getNameLCFirst}}->getId();
//        }
    {{/getParent.isAssociationClass}}
    {{^getParent.isAssociationClass}}
    {{#isCardinalitySingle}}
        if($this->{{getName}}){
            if(!is_object($this->{{getName}})) $insertObj['{{getName}}'] = $this->{{getName}};
            else $insertObj['{{getName}}'] = $this->{{getName}}->getId();
        }
    {{/isCardinalitySingle}}
    {{/getParent.isAssociationClass}}
    {{/isCardinalityZero}}
    {{/isStatic}}
    {{/getAssociationEnds}}
    {{/isAssociationClass}}
    
    $fields = array_keys($insertObj);
    $prepareValues = array();
    $sql = "INSERT INTO `{{getName}}{{#getSpecializes}}_sub{{/getSpecializes}}` (`" . implode('`,`', $fields) . "`{{^getSpecializes}},`lastModified`,`created`,`createdBy`{{/getSpecializes}}) VALUES (" .
            implode(",", array_map(function($key) {
                        return ":" . $key;
                    }, $fields)) . "{{^getSpecializes}}, ".$lastModified.", ".$created.", ".$createdBy."{{/getSpecializes}})";
    $sql .= " ON DUPLICATE KEY UPDATE " .
            implode(",", array_map(function($key) {
                        return "`" . $key . "` = :" . $key . "";
                    }, $fields)) {{^getSpecializes}}. ", `lastModified` = ".$lastModified.(($updateCreated && $this->created)?", `created` = ".$created:'').(($createdBy)?", `createdBy` = ".$createdBy:''){{/getSpecializes}};
      

    $sth = $mysql->prepare($sql);

    foreach ($insertObj as $id => $obj) {
        $prepareValues[':' . $id] = $obj;
    }
    $this->trigger("save", $insertObj);
    
    $sth->execute($prepareValues);
    
    if ($this->hasPrimaryKey() && !$this->getId()){
            $this->setId($mysql->lastInsertId());
    }
    $this->trigger("afterSave", $insertObj);
    return $this;
}

// </editor-fold>
// </editor-fold>