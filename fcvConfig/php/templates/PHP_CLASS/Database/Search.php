{{#ignoreThis}}
public function getWhatStringFromLoadObject($overWriteName = false, $parent = ''){

    $ret = array(
        'class' => '{{getName}}',
        'what' => array($parent.'{{getName}}.{{getPrimaryKeyAttribute.getName}}'),
        'joins' => array(),
        'values' => array((($parent!='')?$parent.'.':'').'*'),
        'map' => array()
    );
    //$addTableAs = ($parent)?'AS '.$parent.'
    $tableName = $parent;
    $tableName .= ($overWriteName)?$overWriteName:'{{getName}}';
    $setParent = $parent.(($overWriteName)?$overWriteName:'{{getName}}');
    //$setParent = $tableName;
    {{#getSpecializes}}
    $ret = parent::getWhatStringFromLoadObject('{{../getName}}', $parent);
    $ret['class'] = '';
    array_shift($ret['what']);
    array_unshift($ret['what'],'{{../getName}}.{{getPrimaryKeyAttribute.getName}}');
    {{/getSpecializes}}



    {{#getAttributes}}
    if(isSet($this->{{getName}}) && isset($this->loadedAttributes['{{getName}}'])){
        {{^isClass}}
        $ret['what'][{{#isPrimaryKey}} 0 {{/isPrimaryKey}}] = $tableName.'.{{getName}}';
        $ret['values'][{{#isPrimaryKey}} 0 {{/isPrimaryKey}}] = $this->{{getName}};
        {{/isClass}}
        {{#isClass}}
        {{^isCardinalitySingle}}
        $c = $this->{{getName}}->current();
        {{/isCardinalitySingle}}
        {{#isCardinalitySingle}}
        $c = $this->{{getName}};
        {{/isCardinalitySingle}}
        $x = $c->getWhatStringFromLoadObject(false, $setParent);
        $ret['what'] = array_merge($ret['what'], $x['what']);
        $ret['values'] = array_merge($ret['values'], $x['values']);
        $ret['joins'][] = 'LEFT JOIN {{getType.getName}} AS '.$setParent.'{{getType.getName}} ON '.$setParent.'{{getType.getName}}.{{getType.getPrimaryKeyAttribute.getName}} = '.$parent.''.get_class($this).'.{{getName}}';
        $ret['joins'] = array_merge($ret['joins'], $x['joins']);

        $ret['map']['{{getType.getName}}'] = array(
            'fieldName' => '{{../getName}}',
            'mysqlTableName' => $setParent.'{{getType.getName}}',
            'multi' => {{^isCardinalitySingle}}true{{/isCardinalitySingle}}{{#isCardinalitySingle}}false{{/isCardinalitySingle}},
            'map' => $x['map']
        );
        {{/isClass}}

    }
    {{/getAttributes}} 
    {{#getAssociationEnds}}
    if(isSet($this->{{getName}}) && isset($this->loadedAttributes['{{getName}}'])){
//            if(isSet($this->{{getName}})) $what[] = {{getName}};  
        {{^isCardinalitySingle}}
        $c = $this->{{getName}}->current();
        {{/isCardinalitySingle}}
        {{#isCardinalitySingle}}
        $c = $this->{{getName}};
        {{/isCardinalitySingle}}
        $x = $c->getWhatStringFromLoadObject(false, $setParent);
        $ret['what'] = array_merge($ret['what'], $x['what']);
        $ret['values'] = array_merge($ret['values'], $x['values']);
        {{#getParent}}
        {{#isAssociationClass}}
        $ret['joins'][] = 'LEFT JOIN {{getName}} AS '.$setParent.'{{getName}} ON '.$setParent.'{{getName}}.{{getClass.getNameLCFirst}} = '.$parent.get_class($this).'.'.$this->getPrimaryKeyName().'';
        $ret['joins'][] = 'LEFT JOIN {{getConnectionClass.getName}}  AS '.$setParent.'{{getConnectionClass.getName}}  ON '.$setParent.'{{getConnectionClass.getName}}.{{getConnectionClass.getPrimaryKeyAttribute.getName}} = '.$setParent.'{{getName}}.{{getConnectionClass.getNameLCFirst}}';
        /*$ret['map']['{{getNameLCFirst}}'] = array(
            'fieldName' => '{{getName}}',
            'mysqlTableName' => $setParent.'{{getName}}',
            'multi' => false,
            'map' => $x['map']
        );*/
        {{/isAssociationClass}}
        {{/getParent}}
        {{^getParent.isAssociationClass}}
        $ret['joins'][] = 'LEFT JOIN {{getConnectionClass.getName}}  AS '.$setParent.'{{getConnectionClass.getName}} ON '.$setParent.'{{getConnectionClass.getName}}.{{getOppositeAssociationEnd.getName}} = '.$tableName.'.'.$this->getPrimaryKeyName().'';
        {{/getParent.isAssociationClass}}
        $ret['joins'] = array_merge($ret['joins'], $x['joins']);
        $ret['map']['{{getConnectionClass.getName}}'] = array(
            'fieldName' => '{{getName}}',
            'mysqlTableName' => $setParent.'{{getConnectionClass.getName}}',
            'multi' => {{^isCardinalitySingle}}true{{/isCardinalitySingle}}{{#isCardinalitySingle}}false{{/isCardinalitySingle}},
            'map' => $x['map']
        );
    }
    {{/getAssociationEnds}}

//        new dBug($ret);
    return $ret;
    return implode(',', $what);
}

/**
 * @param {{getName}} $obj searchItem
 * @param int $limit
 * @param string $orderBy
 * @param {{getName}} $loadObject
 * @return {{getName}}Collection
 */
public static function search($obj = null, $limit = '', $orderBy = '', $loadObject = false){
    $mysqli = mys::getObj();
    $joins = array();
    $searchWhere = array();
    if(!is_null($obj)){
        $whatStr = $obj->getWhatStringFromLoadObject();
        foreach($whatStr['values'] as $index => $val){
            if($val != '*'){
                $searchWhere[] = ''.$whatStr['what'][$index].' LIKE "%'.$val.'%"';
            }
        }
        $joins = $whatStr['joins'];
        $obj = $obj->toArray(true);
    }
    /*if(count($obj) == 0) $searchWhere = array(1);
    else{
        $searchWhere = array();
        $objKeys = array_keys($obj);
        {{#getAttributes}}
        if(in_array('{{getName}}', $objKeys) && !empty($obj['{{getName}}'])) $searchWhere[] = "`{{getName}}` LIKE '%".$obj['{{getName}}']."%'";
        {{/getAttributes}}
    }*/
    $limit = ($limit == '')?'':' LIMIT '.$limit;
    $orderBy = ($orderBy == '')?'':' ORDER BY '.$orderBy;
    $whatJoins = ($loadObject)?$loadObject->getWhatStringFromLoadObject():array(
        'joins' => array(),
        'what' => array('{{getName}}.*'),
        'map' => array()
    );

    $whatJoins['joins'] = array_unique(array_merge($joins,$whatJoins['joins']));
//        new dBug($whatJoins);
    $what = implode(', ', $whatJoins['what']);
    $sql = 'SELECT '.$what.' FROM `{{getName}}`
        '.implode("\n", $whatJoins['joins']).'
        WHERE '.((empty($searchWhere))?'1':implode(' AND ', $searchWhere)).$limit.$orderBy;
//        echo $sql.'!';
    $result = $mysqli->query($sql);
    echo $mysqli->error;
    $return = new {{getName}}Collection();
    $spl = array();
    $whatJoins['what'][0] = '{{getPrimaryKeyAttribute.getName}}';
    foreach($whatJoins['what'] as $w){
        if(strpos($w, '.')){
            $path = explode('.', $w);
            if(!isset($spl[$path[0]])) $spl[$path[0]] = array();
//                new dBug(array_pop($path));
            $spl[$path[0]][] = array_pop($path);
        }
    }

    $objs = array();
    while($row = $result->fetch_assoc()){

        $row = {{getName}}::parseMapRows($row,$whatJoins,$spl, $objs,$row);
        if(!isSet($objs[$row['{{getPrimaryKeyAttribute.getName}}']])) $objs[$row['{{getPrimaryKeyAttribute.getName}}']] = $row;
    }
    foreach($objs as $obj){
        $return->add(new {{getName}}($obj));
    }
    return $return;
}

/**
 * map Mysql Rows to nested Array for initializising Objects
 */
private static function parseMapRows($row,$whatJoins,$spl, &$objs,$orgRow){
    if(isSet($whatJoins['map'])){
        foreach($whatJoins['map'] as $className => $map){
            $row[$map['fieldName']] = array();
            if(isSet($spl[$map['mysqlTableName']])){
                $newSubObj = array();
                foreach($spl[$map['mysqlTableName']] as $key){
                    $newSubObj[$key] = $orgRow[$key];
                }
                $newSubObj = {{getName}}::parseMapRows($newSubObj,$map,$spl, $objs,$row);
                foreach($spl[$map['mysqlTableName']] as $key){                        
                    if($map['multi']){
                        if(isSet($objs[$orgRow['{{getPrimaryKeyAttribute.getName}}']])){
                            if(isSet($objs[$orgRow['{{getPrimaryKeyAttribute.getName}}']][$key])){
                                $objs[$orgRow['{{getPrimaryKeyAttribute.getName}}']][$map['fieldName']][] = $newSubObj;
                            }else{
                                $objs[$orgRow['{{getPrimaryKeyAttribute.getName}}']][$map['fieldName']] = array($newSubObj);
                            }
                        }else{

                            $row[$map['fieldName']] = array($newSubObj);
                        }
                    }else $row[$map['fieldName']] = $newSubObj;

                }
            }

        }
    }
    return $row;
}
{{/ignoreThis}}