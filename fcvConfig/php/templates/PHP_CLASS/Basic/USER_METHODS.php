
{{#isClass}}
{{#getOperations}}
/**
  * {{&getDocumentation}}
{{#getParameters}}
  * @param {{getType.getName}} ${{getName}} {{&getDocumentation}}
{{/getParameters}}
  * @return {{#getReturnParameter}}{{#getType}}{{getType.getName}}{{/getType}}{{#getClass}}{{getClass.getName}}{{/getClass}} {{&getDocumentation}}{{/getReturnParameter}}
  */
public {{#isAbstract}}abstract {{/isAbstract}}{{#isStatic}} static{{/isStatic}} function {{getName}}({{#getParameters}}{{^first}}, {{/first}}{{^isDefaultType}}{{getType.getName}}{{/isDefaultType}} ${{getName}} {{#hasDefault}} = {{&getDefault}}{{/hasDefault}}{{/getParameters}}){{#isInterface}};{{/isInterface}}{{^isInterface}}{
    {{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    return include("FCVAPs{{#getWorkPackage}}{{getPath}}/{{getName}}.php{{/getWorkPackage}}");
    {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    {{#includeCode}}{{#indent 2}}{{#getWorkPackage}}{{#singleParse}}{{&getFunctionContent}}{{/singleParse}}{{/getWorkPackage}}{{/indent 2}}{{/includeCode}}
    {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}

}

{{/isInterface}}
{{/getOperations}}
{{/isClass}}