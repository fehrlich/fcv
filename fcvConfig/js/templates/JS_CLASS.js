define(['require'{{^getClientSpecializes}}, 'fcv/FCVClass'{{/getClientSpecializes}}{{#getClientSpecializes}},'{{#substr 1}}{{getWorkPackage.getPath}}{{/substr 1}}'{{/getClientSpecializes}}], //{{#multi}},'js/classes{{req.getWorkPackage.getPath}}Collection.js'{{/multi}}
function (require{{^getClientSpecializes}}, FFCVClass{{/getClientSpecializes}}{{#getClientSpecializes}},{{getName}}F{{/getClientSpecializes}}) { //{{#multi}},{{req.getName}}CollectionF{{/multi}}
    "use strict";
    {{#isPageOrFrame}}
    {{#getWorkPackage}}
    function {{getName}}(obj,fromInherit){}
    
    {{getName}}.prototype.loadPage = function(){
        {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        {{#includeCode}}{{#indent 2}}{{#singleParse}}{{&getControlContent}}{{/singleParse}}{{/indent 2}}{{/includeCode}}
        {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    };
    
    {{getName}}.create = function(){
        return new {{getName}}();
    };
    {{/getWorkPackage}}
    {{/isPageOrFrame}}
    {{^isPageOrFrame}}
    function {{getName}}(obj,fromInherit){
        fromInherit = fromInherit || false;
        obj = obj || {};
        var overwrite;
                
        {{#getClientSpecializes}}overwrite = {{getName}}.call(this, obj,true);{{/getClientSpecializes}}
        {{^getClientSpecializes}}overwrite = FCVClass.call(this, obj, {{^getPrimaryKeyAttribute}}false{{/getPrimaryKeyAttribute}}{{#getPrimaryKeyAttribute}}'{{getPrimaryKeyAttribute.getName}}'{{/getPrimaryKeyAttribute}});{{/getClientSpecializes}}
        
        if (overwrite) {
            return overwrite;
        }
        
        if(!fromInherit){
            this.updateFromObj(obj);
            this.init();
        }
    };
    
    {{getName}}.fcvFields = [{{#getAttributes}}'{{getName}}',{{/getAttributes}}];
    {{getName}}.primaryKeysNames = [
        {{#hasPrimaryKeyAttribute}}
        {{#getPrimaryKeyAttribute}}
        '{{getName}}'
        {{/getPrimaryKeyAttribute}}
        {{/hasPrimaryKeyAttribute}}
        {{^hasPrimaryKeyAttribute}}
        {{#isAssociationClass}}
        {{#getAssociationEnds}}
        '{{getType.getNameLCFirst}}',
        {{/getAssociationEnds}}
        {{/isAssociationClass}}
        {{/hasPrimaryKeyAttribute}}
    ];
        
    //Helper
    FCVClass.extendWithEventAction({{getName}});
    FCVClass.extendWithEventAction({{getName}}.prototype);
    {{^isSingleton}}
    FCVClass.extendWithCreateMethod({{getName}});
    {{/isSingleton}}
    {{#isSingleton}}
    FCVClass.extendWithSingleton({{getName}});
    {{/isSingleton}}
            
    {{getName}}.className = '{{getName}}';
    {{getName}}.nsClassName = '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}';
    {{getName}}.getClassName = function(){
        return '{{getName}}';
    };
    {{getName}}.prototype.getClassName = function(){
        return '{{getName}}';
    };
    {{getName}}.prototype.getNsClassName = function(){
        return {{getName}}.nsClassName;
    };
    {{#usesTypeParser}}
    FCVClass.extendWithTypeParser({{getName}});    
    {{/usesTypeParser}}
            
    {{#getClientSpecializes}}
    {{../getName}}._parent = {{getName}}.prototype;
    {{/getClientSpecializes}}
    {{^getClientSpecializes}}
    {{getName}}._parent = FCVClass.prototype;
    {{/getClientSpecializes}}
    
    {{^getClientSpecializes}}
    FCVClass.extendWithTableFields({{getName}});
    {{/getClientSpecializes}}
        
    //Attributes Getter Setter
    {{#getAttributes}}
    {{^noJs}}
    {{../getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getName}}{{#getDefault}} = {{&.}}{{/getDefault}};

    {{../getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getterFunction}} = function(){
        var val = this.{{getName}};
        {{#isClass}}if(val && val.class && val.id) this.{{getName}} = jsModel[val.class+'.'+val.id];{{/isClass}}
//        return {{#isBoolean}}!val || val == "0" || val == "false" ? false : true {{/isBoolean}}{{^isBoolean}}this.{{getName}}{{/isBoolean}};
        return this.{{getName}};
    };

    {{../getName}}{{^isStatic}}.prototype{{/isStatic}}.{{setterFunction}} = function({{getName}}, fromUser){
        fromUser = fromUser || false;
        {{^isDefaultType}}
        if({{getName}} == null) this.{{getName}} = null;
        else if({{getName}} == '[null]') this.{{getName}} = null;


        {{^isCardinalityMulti}}
        {{#isClass}}
        else if(typeof {{getName}} == 'object' && {{getName}} instanceof {{getType.getName}}) this.{{getName}} = {{getName}};
//            else if(typeof {{getName}} == 'object' && {{getName}}.class && {{getName}}.id) this.{{getName}} = {{getName}};
        else if(typeof {{getName}} == 'object') this.{{getName}} = new {{getType.getName}}({{getName}});
        {{/isClass}}
        {{#getType.getPrimaryKeyAttribute}}else if({{../getName}} > 0  || (typeof {{../getName}} == 'string' && {{../getName}} != '')) this.{{../getName}} = {{../getType.getName}}.create({ {{getName}}: {{../getName}}, partialLoaded: true});{{/getType.getPrimaryKeyAttribute}}
        {{/isCardinalityMulti}}{{#isCardinalityMulti}}
//            else if({{getName}} instanceof {{getType.getName}}Collection) this.{{getName}} = {{getName}};
        else if(Object.prototype.toString.call( {{getName}} ) === '[object Array]'){
            this.{{getName}} = [];
            for(var index in {{getName}}){
                var element = {{getName}}[index];
                if(typeof element == 'object' && element instanceof {{getType.getName}}) this.{{getName}}.push(element);
                else if(typeof element == 'object') this.{{getName}}.push({{getType.getName}}.create(element));
                {{#getType.getPrimaryKeyAttribute}}else if(element > 0 || (typeof element == 'string' && element != '')) this.{{../getName}}.push({{../getType.getName}}.create({ {{getName}}: element, partialLoaded: true}));{{/getType.getPrimaryKeyAttribute}}
//                    {{#getType.hasPrimaryKeyAttribute}}else if(element > 0) this.{{getName}}.push({{getType.getName}}.create({ {{getType.getPrimaryKeyAttribute.getName}}: element, partialLoaded: true}));{{/getType.hasPrimaryKeyAttribute}}
                else{
                    this.{{getName}} = null;
                    console.log("TYPEERROR: ",{{getName}});
                }
            }
        }

        {{/isCardinalityMulti}}
        else{
            this.{{getName}} = null;
            console.log("TYPEERROR: ",{{getName}});
        }
        {{/isDefaultType}}
        {{#isDefaultType}}
        {{#getType.usesTypeParser}}
        if({{getName}} != null){
            if(typeof {{getName}} == 'object' && {{getName}} instanceof {{getType.getName}}){
                this.{{getName}} = {{getName}};
            }
            else if(fromUser && {{getType.getName}}.parseFromUser){
                this.{{getName}} = {{getType.getName}}.parseFromUser({{getName}});
            }else{
                this.{{getName}} = {{getType.getName}}.parse({{getName}});
            }
        }else{
            this.{{getName}} = {{getType.getName}}.null();
        }
        {{/getType.usesTypeParser}}{{^getType.usesTypeParser}}
        this.{{getName}} = {{#isBoolean}}!{{getName}} || {{getName}} == "0" || {{getName}} == "false" ? false : true{{/isBoolean}}{{^isBoolean}}{{getName}}{{/isBoolean}};
        {{/getType.usesTypeParser}}
        {{/isDefaultType}}

        return this;
    };
    {{/noJs}}
    {{/getAttributes}}

    //AssociationEnds
    {{#getAssociationEnds}}
    {{#getConnectionClass.isClient}}
    {{^isCardinalityZero}}
    {{../../getName}}.prototype.{{getName}};


    {{../../getName}}.prototype.get{{getNameUCFirst}} = function(){
        {{#isCardinalityMulti}}
        if(this.{{getName}}){
            for(var index in this.{{getName}}){
                var val = this.{{getName}}[index];
                if(val.class && val.id) this.{{getName}}[index] = jsModel[val.class+'.'+val.id];
            }
        }
        {{/isCardinalityMulti}}
        {{^isCardinalityMulti}}
        if(this.{{getName}} && this.{{getName}}.class && this.{{getName}}.id) this.{{getName}} = jsModel[this.{{getName}}.class+'.'+this.{{getName}}.id];
        {{/isCardinalityMulti}}
        return this.{{getName}};
    };

    {{../../getName}}.prototype.set{{getNameUCFirst}} = function({{getName}}){
        {{^isDefaultType}}
        if({{getName}} == null) this.{{getName}} = null;
        else if({{getName}} == '[null]') this.{{getName}} = null;

        {{^isCardinalityMulti}}
        else if(typeof {{getName}} == 'object' && {{getName}} instanceof {{getType.getName}}) this.{{getName}} = {{getName}};
        else if(typeof {{getName}} == 'object') this.{{getName}} = new {{getType.getName}}({{getName}});
//            else if({{getName}} > 0) this.{{getName}} = {{getType.getName}}.create({ {{getType.getPrimaryKeyAttribute.getName}}: {{getName}}, partialLoaded: true});
        {{#getType.getPrimaryKeyAttribute}}else if({{../../getName}} > 0  || (typeof {{../../getName}} == 'string' && {{../../getName}} != '')) this.{{../../getName}} = {{../../getType.getName}}.create({ {{getName}}: {{../../getName}}, partialLoaded: true});{{/getType.getPrimaryKeyAttribute}}
        {{/isCardinalityMulti}}{{#isCardinalityMulti}}
//            else if({{getName}} instanceof {{getType.getName}}Collection) this.{{getName}} = {{getName}};
        else if(Object.prototype.toString.call( {{getName}} ) === '[object Array]'){
            this.{{getName}} = [];
            for(var index in {{getName}}){
                var element = {{getName}}[index];
                if(typeof element == 'object') this.{{getName}}.push({{getType.getName}}.create(element));
                else if(typeof element == 'object' && element instanceof {{getType.getName}}) this.{{getName}}.push(element);
                {{#getType.getPrimaryKeyAttribute}}else if(element > 0 || (typeof element == 'string' && element != '')) this.{{../../getName}}.push({{../../getType.getName}}.create({ {{getName}}: element, partialLoaded: true}));{{/getType.getPrimaryKeyAttribute}}
//                    {{#getType.hasPrimaryKeyAttribute}}else if(element > 0) this.{{getName}}.push({{getType.getName}}.create({ {{getType.getPrimaryKeyAttribute.getName}}: element, partialLoaded: true}));{{/getType.hasPrimaryKeyAttribute}}
                else{
                    this.{{getName}} = null;
                    console.log("TYPEERROR: ",{{getName}});
                }
            }
        }

        {{/isCardinalityMulti}}
        else{
            this.{{getName}} = null;
            console.log("TYPEERROR: ",{{getName}});
        }
        {{/isDefaultType}}
        {{#isDefaultType}}this.{{getName}} = {{getName}};{{/isDefaultType}}
        {{^getOppositeAssociationEnd.isCardinalityZero}}

        {{#getOppositeAssociationEnd.isCardinalitySingle}}
        {{#isCardinalityMulti}}
        for(var index in {{getNameLCFirst}}){
            if(typeof {{getNameLCFirst}}[index] == 'object' && {{getNameLCFirst}}[index] instanceof {{getType.getName}} && {{getNameLCFirst}}[index].get{{getOppositeAssociationEnd.getNameUCFirst}}().getId() != this.getId()) {{getNameLCFirst}}[index].set{{getOppositeAssociationEnd.getNameUCFirst}}(this);
        }
        {{/isCardinalityMulti}}
        {{^isCardinalityMulti}}
        if(typeof {{getNameLCFirst}} == 'object' && {{getNameLCFirst}} instanceof {{getType.getName}} && {{getNameLCFirst}}.get{{getOppositeAssociationEnd.getNameUCFirst}}() && {{getNameLCFirst}}.get{{getOppositeAssociationEnd.getNameUCFirst}}().getId() != this.getId()) {{getNameLCFirst}}.set{{getOppositeAssociationEnd.getNameUCFirst}}(this);
        {{/isCardinalityMulti}}
        {{/getOppositeAssociationEnd.isCardinalitySingle}}
        {{/getOppositeAssociationEnd.isCardinalityZero}}

        return this;
    };


    {{#isCardinalityMulti}}
    {{../../../getName}}.prototype.add{{getNameUCFirst}} = function({{getName}}){
        if(!this.{{getName}}) this.{{getName}} = [];
        this.{{getName}}.push({{getName}});
        return this;
    };
    {{/isCardinalityMulti}}

    {{#isAssociationClass}}
    //AssocClass
    {{../../../getName}}.prototype.{{getType.getNameLCFirst}};

    {{../../../getName}}.prototype.get{{getType.getName}} = function(){
        if(this.{{getType.getNameLCFirst}} && this.{{getType.getNameLCFirst}}.class && this.{{getType.getNameLCFirst}}.id) this.{{getType.getNameLCFirst}} = jsModel[this.{{getType.getNameLCFirst}}.class+'.'+this.{{getType.getNameLCFirst}}.id];
        return this.{{getType.getNameLCFirst}};
    };

    {{../../../getName}}.prototype.set{{getType.getName}} = function({{getType.getNameLCFirst}}){

        if({{getType.getNameLCFirst}} == null) this.{{getType.getNameLCFirst}} = null;

        else if(typeof {{getType.getNameLCFirst}} == 'object' && {{getType.getNameLCFirst}} instanceof {{getType.getName}}) this.{{getType.getNameLCFirst}} = {{getType.getNameLCFirst}};
        else if(typeof {{getType.getNameLCFirst}} == 'object') this.{{getType.getNameLCFirst}} = new {{getType.getName}}({{getType.getNameLCFirst}});
        {{#getType.getPrimaryKeyAttribute}}else if({{../../getType.getNameLCFirst}} > 0) this.{{../../getType.getNameLCFirst}} = {{../../getType.getName}}.create({ {{getName}}: {{../../getType.getNameLCFirst}}, partialLoaded: true});{{/getType.getPrimaryKeyAttribute}}
        else{
            this.{{getType.getNameLCFirst}} = null;
            console.log("ERROR: ",{{getType.getNameLCFirst}});
        }

        //{{^getOppositeAssociationEnd.isCardinalityZero}}if({{getType.getNameLCFirst}}.get{{getOppositeAssociationEnd.getNameUCFirst}}() != this.getId()) {{getType.getNameLCFirst}}.set{{getOppositeAssociationEnd.getNameUCFirst}}(this);{{/getOppositeAssociationEnd.isCardinalityZero}}

        return this;
    };


    {{/isAssociationClass}}

    {{#getParent.isAssociationClass}}
    {{^isAssociationClass}}

    //end belongs to AssocClass
    {{../../../getName}}.prototype.{{getParent.getNameLCFirst}};

    {{../../../getName}}.prototype.set{{getParent.getName}} = function({{getParent.getNameLCFirst}}){
        if({{getParent.getNameLCFirst}} == null) this.{{getParent.getNameLCFirst}} = null;
        {{^isCardinalitySingle}}
        else if(typeof {{getParent.getNameLCFirst}} == 'object' && {{getParent.getNameLCFirst}} instanceof Object){
            this.{{getParent.getNameLCFirst}} = [];
            for(var index in {{getParent.getNameLCFirst}}){
                var val = {{getParent.getNameLCFirst}}[index];
                if(!(typeof val == 'object' && val instanceof {{getParent.getName}})) val = new {{getParent.getName}}(val);
                this.{{getParent.getNameLCFirst}}.push(val);
            }
        }
        {{/isCardinalitySingle}}
        {{#isCardinalitySingle}}
        else if(typeof {{getParent.getNameLCFirst}} == 'object' &&  {{getParent.getNameLCFirst}} instanceof {{getParent.getName}}) this.{{getParent.getNameLCFirst}} = {{getParent.getNameLCFirst}};
        else if(typeof {{getParent.getNameLCFirst}} == 'object') this.{{getParent.getNameLCFirst}} = new {{getParent.getName}}({{getParent.getNameLCFirst}});
        {{/isCardinalitySingle}}
        else{
            this.{{getParent.getNameLCFirst}} = null;
            console.log("ERROR: ",{{getParent.getNameLCFirst}});
        }

        {{#isCardinalityMulti}}
        for(var index in {{getParent.getNameLCFirst}}){
            if(typeof {{getParent.getNameLCFirst}}[index]== 'object' && {{getParent.getNameLCFirst}}[index] instanceof {{getParent.getName}} && (!{{getParent.getNameLCFirst}}[index].get{{../../../../getName}}() || {{getParent.getNameLCFirst}}[index].get{{../../../../getName}}().getId() != this.getId())) {{getParent.getNameLCFirst}}[index].set{{../../../../getName}}(this);
        }
        {{/isCardinalityMulti}}
        {{^isCardinalityMulti}}
        if(typeof {{getParent.getNameLCFirst}}== 'object' &&  {{getParent.getNameLCFirst}} instanceof {{getParent.getName}} && (!{{getParent.getNameLCFirst}}.get{{../../../getName}}() || {{getParent.getNameLCFirst}}.get{{../../../getName}}().getId() != this.getId())) {{getParent.getNameLCFirst}}.set{{../../../getName}}(this);
        {{/isCardinalityMulti}}

        return this;
    };

    {{../../../getName}}.prototype.get{{getParent.getName}} = function(){
        {{#isCardinalityMulti}}
        if(this.{{getParent.getNameLCFirst}}){
            for(var index in this.{{getParent.getNameLCFirst}}){
                var val = this.{{getParent.getNameLCFirst}}[index];
                if(val.class && val.id) this.{{getParent.getNameLCFirst}}[index] = jsModel[val.class+'.'+val.id];
            }
        }
        {{/isCardinalityMulti}}
        {{^isCardinalityMulti}}
        if(this.{{getParent.getNameLCFirst}} && this.{{getParent.getNameLCFirst}}.class && this.{{getParent.getNameLCFirst}}.id) this.{{getParent.getNameLCFirst}} = jsModel[this.{{getParent.getNameLCFirst}}.class+'.'+this.{{getParent.getNameLCFirst}}.id];
        {{/isCardinalityMulti}}
        return this.{{getParent.getNameLCFirst}};
    };

    {{/isAssociationClass}}
    {{/getParent.isAssociationClass}}
    {{/isCardinalityZero}}
    {{/getConnectionClass.isClient}}
    {{/getAssociationEnds}}

    //PrimaryAttribute
    {{^getPrimaryKeyAttribute}}
     {{#isAssociationClass}}

    {{../getName}}.prototype.getId = function(){
        var ids = [];
        {{#getAssociationEnds}}
        ids.push(this.{{getType.getNameLCFirst}});
        {{/getAssociationEnds}}
        return ids;
    };
    {{../getName}}.prototype.setId =  function(ids){
        if(typeof ids == "string") ids = ids.split('_');
        {{#getAssociationEnds}}
        this.{{getType.getNameLCFirst}} = ids.shift();
        {{/getAssociationEnds}}
        return this;
    };
     {{/isAssociationClass}}
    {{/getPrimaryKeyAttribute}}

    {{#getPrimaryKeyAttribute}}
    {{../getName}}.prototype.getId = function(){
        return this.{{getName}};
    };
    {{../getName}}.prototype.setId =  function(id){
        this.{{getName}} = id;
        return this;
    };

    {{../getName}}.getPrimaryKeyName = function(){
        return "{{getName}}";
    };
    {{/getPrimaryKeyAttribute}}

    {{getName}}.hasPrimaryKey = function(){
        return {{#getPrimaryKeyAttribute}}true{{/getPrimaryKeyAttribute}}{{^getPrimaryKeyAttribute}}false{{/getPrimaryKeyAttribute}};
    };

    
    
    {{getName}}.prototype.convertetToObj = false;
    {{getName}}.prototype.toObj = function(recursive, recursiveQueue){
        recursive = recursive || false;
        
        //TODO: Cardinality Multi + Class ids
        var obj = {{getName}}._parent.toObj.call(this, recursive, recursiveQueue);
                
        //From Attributes
        {{#getAttributes}}
        {{^noJs}}
        {{^isStatic}}
        {{#isDefaultType}}
        {{#getType.usesTypeParser}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = this.get{{getNameUCFirst}}().val();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';        
        {{/getType.usesTypeParser}}{{^getType.usesTypeParser}} 
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = this.get{{getNameUCFirst}}();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/getType.usesTypeParser}} 
        {{/isDefaultType}}    
        {{^isDefaultType}}          
        {{#isClass}}
        {{#getType.hasPrimaryKeyAttribute}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = (recursive)?this.get{{getNameUCFirst}}().toObj(true,recursiveQueue):this.get{{getNameUCFirst}}().getId();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}{{^getType.hasPrimaryKeyAttribute}}
        if(this.{{getName}} !== null && recursive) obj.{{getName}} = this.get{{getNameUCFirst}}().toObj(true,recursiveQueue);   
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';                 
        {{/getType.hasPrimaryKeyAttribute}}
        {{/isClass}}{{^isClass}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = (typeof this.{{getName}} === 'object')?null:this.get{{getNameUCFirst}}();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/isClass}}
        {{/isDefaultType}}
        {{/isStatic}}
        {{/noJs}}
        {{/getAttributes}}

        //From AssociationsEnds
        {{#getAssociationEnds}}
        {{#getConnectionClass.isClient}}
        {{^isCardinalityZero}}

        {{^isCardinalityMulti}}
        //{{isCardinalityMulti}}
        {{#getType.hasPrimaryKeyAttribute}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined') obj.{{getName}} = (recursive)? this.get{{getNameUCFirst}}().toObj(true, recursiveQueue):this.get{{getNameUCFirst}}().getId();
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}{{^getType.hasPrimaryKeyAttribute}}
        if(this.{{getName}} !== null && typeof this.{{getName}} != 'undefined' && recursive) obj.{{getName}} = this.get{{getNameUCFirst}}().toObj(true, recursiveQueue);
//        else if(this.{{getName}} == null) obj.{{getName}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}

        {{^isAssociationClass}}
        {{#getParent.isAssociationClass}}
        //From AssocClassConnections
        {{#getType.hasPrimaryKeyAttribute}}
        if(this.{{getNameLCFirst}} !== null && typeof this.{{getNameLCFirst}} != 'undefined') obj.{{getNameLCFirst}} = (recursive)?this.get{{getNameUCFirst}}().toObj(true, recursiveQueue):this.get{{getNameUCFirst}}().getId();
//        else if(this.{{getNameLCFirst}} == null) obj.{{getNameLCFirst}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}{{^getType.hasPrimaryKeyAttribute}}
        if(this.{{getNameLCFirst}} !== null && typeof this.{{getNameLCFirst}} != 'undefined' && recursive) obj.{{getNameLCFirst}} = this.get{{getNameUCFirst}}().toObj(true, recursiveQueue);
//        else if(this.{{getNameLCFirst}} == null) obj.{{getNameLCFirst}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}
        {{/getParent.isAssociationClass}}
        {{/isAssociationClass}} 
        {{/isCardinalityMulti}}

        {{#isAssociationClass}}
        //From AssocClass
        {{#getType.hasPrimaryKeyAttribute}}
        if(this.{{getConnectionClass.getNameLCFirst}} !== null && typeof this.{{getConnectionClass.getNameLCFirst}} != 'undefined') obj.{{getConnectionClass.getNameLCFirst}} = (recursive)?this.get{{getConnectionClass.getNameUCFirst}}().toObj(true, recursiveQueue):this.{{getConnectionClass.getNameLCFirst}}.getId();
//        else if(this.{{getConnectionClass.getNameLCFirst}} == null) obj.{{getConnectionClass.getNameLCFirst}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}{{^getType.hasPrimaryKeyAttribute}}
        if(this.{{getConnectionClass.getNameLCFirst}} !== null && typeof this.{{getConnectionClass.getNameLCFirst}} != 'undefined' && recursive) obj.{{getConnectionClass.getNameLCFirst}} = this.get{{getConnectionClass.getNameUCFirst}}().toObj(true, recursiveQueue);
//        else if(this.{{getConnectionClass.getNameLCFirst}} == null) obj.{{getConnectionClass.getNameLCFirst}} = 'null';
        {{/getType.hasPrimaryKeyAttribute}}
        {{/isAssociationClass}}
        {{/isCardinalityZero}}
        {{/getConnectionClass.isClient}}
        {{/getAssociationEnds}}

        return obj;
    };

    //Userdefined Operation default php mapping
    {{#getOperations}}
    {{#isClient}}
    {{#canCallFromClient}}
    /**
     * 
    {{#getParameters}}
      * @param {{getType.getName}} {{getName}}
    {{/getParameters}}
      * @return {{getReturnParameter.getName}}
     */
    {{getParent.getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getName}}PHP = function({{#getParameters}}{{^first}}, {{/first}}{{getName}}{{/getParameters}}){
        
        {{#getParameters}}{{#getDefault}}{{getName}} = typeof {{getName}} === 'undefined' ? {{&.}} : {{getName}};{{/getDefault}}{{/getParameters}}
        
        var param = {
            {{#getParameters}}
            '{{getName}}': JsModel.transferParameter({{getName}}),
            {{/getParameters}}
        };
        {{#isStatic}}
        return FCVClass.callPHP('{{getName}}', '{{#pathToNamespace 2}}{{getParent.getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getParent.getName}}', 1, param{{#getReturnParameter.isClass}}, 'json'{{/getReturnParameter.isClass}});
        {{/isStatic}}
        {{^isStatic}}
        return this.callPHPMethod('{{getName}}', param, false{{#getReturnParameter.isClass}}, 'json'{{/getReturnParameter.isClass}});
        {{/isStatic}}
    };
    {{/canCallFromClient}}
    {{/isClient}}
    {{/getOperations}}

    //CRUD
    {{#isTable}}
    {{getName}}.prototype.getPrimaryKeyObject = function(){
        return {
            {{#hasPrimaryKeyAttribute}}
            {{#getPrimaryKeyAttribute}}
            {{getName}}: this.get{{getNameUCFirst}}()
            {{/getPrimaryKeyAttribute}}
            {{/hasPrimaryKeyAttribute}}
            {{^hasPrimaryKeyAttribute}}
            {{#isAssociationClass}}
            {{#getAssociationEnds}}
            {{getType.getNameLCFirst}}: this.get{{getType.getNameUCFirst}}().getId(),
            {{/getAssociationEnds}}
            {{/isAssociationClass}}
            {{/hasPrimaryKeyAttribute}}
        }
    };
    FCVClass.extendWithCRUD({{getName}}, {{getName}}.primaryKeysNames);
    
    {{#hasActive}}
    FCVClass.extendWithActive({{getName}}.prototype);
    {{/hasActive}}
    {{/isTable}}

    //Views and Templates
    FCVClass.extendWithTemplates({{getName}});
        
    {{getName}}.getTemplate = function(templateName){
        if(typeof templateName !== "string") templateName = "default";
        var availableTemplates = {
            {{#uniqueLines}}
            {{#getWorkPackage.getViews}}
            '{{getName}}': '{{../getName}}.{{getName}}',
            {{/getWorkPackage.getViews}}
            {{/uniqueLines}}
        };
        if(!availableTemplates[templateName]){
            {{#getClientSpecializes}}
            return {{getName}}.getTemplate(templateName);
            {{/getClientSpecializes}}{{^getClientSpecializes}}
            throw Error("template '"+templateName+"' doesnt exists");
            {{/getClientSpecializes}}
        }
        templateName = templateName || 'default';
        
        {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        var templateSrcs = {
            {{#getWorkPackage}}
            {{#list getViews}}
            {{^first}},{{/first}}'{{../getName}}.{{value.getName}}': '{{#value}}{{#jsMultilineString}}{{#includeTemplate 1}}{{&getHtmlContent}}{{/includeTemplate 1}}{{/jsMultilineString}}{{/value}}' //{{value.getId}}
            {{/list getViews}}
            {{/getWorkPackage}}
        };
        var tplSrc = templateSrcs[availableTemplates[templateName]];
        return tplSrc;
        {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        {{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
        
        var templateFile = "FCVAPs{{getWorkPackage.getPath}}/"+availableTemplates[templateName]+".html";
       
        var template = $.ajax({
            'type': "GET",
            'url': templateFile,
            'async': false
        }).responseText;
        return template;
        {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    };
    
    {{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    
    
    {{getName}}.prototype.init = function(){
        {{#includeCode}}{{#indent 2}}{{#getWorkPackage}}{{#singleParse}}{{&getControlContent}}{{/singleParse}}{{/getWorkPackage}}{{/indent 2}}{{/includeCode}}
    }
    
    //userMethods
    {{#getOperations}}
    {{^isInterface}}
    {{#isClient}}
    /**
      *
    {{#getParameters}}
      * @param {{getType.getName}} {{getName}}
    {{/getParameters}}
      * @return {{getReturnParameter.getType.getName}}
      */
    {{getParent.getName}}{{^isStatic}}.prototype{{/isStatic}}.{{getName}} = function({{#getParameters}}{{^first}}, {{/first}}{{getName}}{{/getParameters}}){
        {{#getParameters}}{{#getDefault}}{{getName}} = typeof {{getName}} === 'undefined' ? {{&.}} : {{getName}};{{/getDefault}}{{/getParameters}}
        {{#includeCode}}{{#indent 2}}{{#getWorkPackage}}{{#singleParse}}{{&getControlContent}}{{/singleParse}}{{/getWorkPackage}}{{/indent 2}}{{/includeCode}}
    };
    
    {{/isClient}}
    {{/isInterface}}
    {{/getOperations}}
    
    {{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
    {{#getClientSpecializes}}
    FCVClass.extend({{../getName}}, {{getName}});
    {{/getClientSpecializes}}
    {{^getClientSpecializes}}
    FCVClass.extend({{getName}}, FCVClass);
    {{/getClientSpecializes}}
    {{/isPageOrFrame}}

    globalScope.{{getName}} = {{getName}};
    JsModel.getObj().loadedClass('{{getName}}');
    
    return {{getName}};
});
