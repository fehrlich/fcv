<?php
//header('Content-Type: text/javascript');

use fcv\view\Frame;
use fcv\Routing;

function jsBuild($initJs = '', $neededClassesForJsModel = false){
{{#getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
$js = '
requirejs.config({
    baseUrl: "js/classes",
    waitSeconds: 15
});
';

//$neededClassesPrefix = 'js/classes/';
$neededClassesPrefix = '';

$nestedJs = '';
$classCount = 0;
$classesNeededToLoaded = array();
$nestedJs .= 'JsModel.getObj().trigger("baseStructureLoaded");'."\n";
$neededClasses = array(
    'require' => 'require'
);

{{#getAllAssociationClassesAndClasses}}
{{#isClient}}

if(!$neededClassesForJsModel || isset($neededClassesForJsModel['{{getName}}']) || isset($neededClassesForJsModel['{{getName}}CollectionPrepare'])){
    $classCount++;
    $classesNeededToLoaded['{{getName}}'] = true;
    $neededClasses['{{getName}}'] = $neededClassesPrefix."{{#substr 1}}{{getWorkPackage.getPath}}{{/substr 1}}";
    {{#isTable}}
    if(!$neededClassesForJsModel || isset($neededClassesForJsModel['{{getName}}CollectionPrepare'])){
        $neededClasses['{{getName}}CollectionPrepare'] = $neededClassesPrefix."{{#substr 1}}{{getWorkPackage.getPath}}CollectionPrepare{{/substr 1}}";
    }
    {{/isTable}}

    $contentMain = file_get_contents('FCVAPs{{getWorkPackage.getPath}}/{{getName}}.js');
    $nestedJs .= '
    {{getName}}.prototype.init = function(){
        '.$contentMain."\n".'
    };'."\n";

    {{#getOperations}}
    {{#isClient}}
    $contentOperation = '';

    $neededClasses['{{../../getName}}'] = $neededClassesPrefix."{{#substr 1}}{{../../getWorkPackage.getPath}}{{/substr 1}}";

    $contentOperation .= file_get_contents('FCVAPs{{getWorkPackage.getPath}}/{{getName}}.js');
    if($contentOperation != ''){
        $nestedJs .= '{{getParent.getName}}.{{^isStatic}}prototype.{{/isStatic}}{{getName}} = function ({{#getParameters}}{{^first}}, {{/first}} {{getName}}{{/getParameters}}) {'."\n";
        $nestedJs .= "{{#addDoubleSlashes}}\t{{#getParameters}}{{#getDefault}}{{getName}} = typeof {{getName}} === 'undefined' ? {{&.}} : {{getName}};{{/getDefault}}{{/getParameters}}\n{{/addDoubleSlashes}}";
        $nestedJs .= "\t".$contentOperation."\n";
        $nestedJs .= "};\n";
    }
    {{/isClient}}                             
    {{/getOperations}}

    {{#getClientSpecializes}}
        $nestedJs .= "FCVClass.extend({{../getName}}, {{getName}});";
    {{/getClientSpecializes}}
}
{{/isClient}}
{{/getAllAssociationClassesAndClasses}}

$pageAndFrames = '';
$framesArray = [];
{{#getWorkPackage.getWorkPackagesByType Page}}
$content = file_get_contents('FCVAPs{{getPath}}/{{getName}}.js');
if($content != ''){
    $neededClasses['{{getName}}'] = $neededClassesPrefix."{{#substr 1}}{{getPath}}{{/substr 1}}";
        $pageAndFrames .= "\n".$content."\n";
//        $pageAndFrames .= "JsModel.loadPage('{{getName}}');";
        $framesArray[] = "{{getName}}";
    $classCount++;
    $classesNeededToLoaded['{{getName}}'] = true;
}
{{/getWorkPackage.getWorkPackagesByType Page}}
{{#getWorkPackage.getWorkPackagesByType Frame}}
    
if(Frame::isLoaded('{{getName}}')){
    $content = file_get_contents('FCVAPs{{getPath}}/{{getName}}.js');
    if($content != ''){
        $neededClasses['{{getName}}'] = $neededClassesPrefix."{{#substr 1}}{{getPath}}{{/substr 1}}";
        $pageAndFrames .= "\n".$content."\n";
        $classCount++;
        $classesNeededToLoaded['{{getName}}'] = true;
    }
}
{{/getWorkPackage.getWorkPackagesByType Frame}}

$pageAndFrames .= 'JsModel.loadPage(["'.implode('","', $framesArray).'"]);';
$initJs .= 'JsModel.getObj().trigger("modelLoaded");';

$pageAndFrames .= 'JsModel.getObj().trigger("pageLoaded");';
//'.implode(',',  array_keys($neededClasses)).'
$js .= '
    var globalScope = (function() { return this; })();
    var jsModel = {};
    JsModel.getObj().setLoadingNeeded('.count($classesNeededToLoaded).');
    requirejs(["'.implode('","',$neededClasses).'"], function(){
        '.$nestedJs.'
        '.$initJs.'
        '.$pageAndFrames.'
});';

    return $js;
{{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}

{{^getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
$js = '';

$view = Routing::getObj()->getCurrentView();
$page = $view->getPage();
$frames = $view->getFrames();

$pageAndFrames = array_merge([$page], $frames);

$loadPF = 'JsModel.loadPage(["'.implode('","', $pageAndFrames).'"]);';

$js .= '
    console.time("loadModel");
    '.$initJs.'
    console.timeEnd("loadModel");
    JsModel.getObj().trigger("baseStructureLoaded");
    JsModel.getObj().trigger("modelLoaded");
    console.time("loadFrame");
    '.$loadPF.'
    console.timeEnd("loadFrame");
    JsModel.getObj().trigger("pageLoaded");
';

    return $js;
{{/getWorkPackage.getProject.getCurrentBuild.getBuildWorkpackages}}
}