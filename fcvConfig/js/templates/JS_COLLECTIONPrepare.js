define(['require','{{#substr 1}}{{getWorkPackage.getPath}}{{/substr 1}}'{{#getClientSpecializes table}},'{{#substr 1}}{{getWorkPackage.getPath}}CollectionPrepare{{/substr 1}}'{{/getClientSpecializes table}}], //{{#multi}},'js/classes/{{req.getWorkPackage.getPath}}Collection.js'{{/multi}}
function (require) { //,{{getName}}{{#getClientSpecializes table}},{{getName}}CollectionPrepare{{/getClientSpecializes table}} //{{#multi}},{{req.getName}}Collection{{/multi}}

    var {{getName}}CollectionPrepare = function(obj){    
        {{#getClientSpecializes table}}{{getName}}CollectionPrepare.call(this, obj);{{/getClientSpecializes table}}
        {{^getClientSpecializes table}}FCVCollectionPrepare.call(this, obj);{{/getClientSpecializes table}}

        var self = this; 
        {{#getAttributes}}
        {{#checkClientColP where}}
        self.where{{getNameUCFirst}} = function(operation, value){
            this.where('{{getName}}', operation, value);
            return this;
        }
        self.orWhere{{getNameUCFirst}} = function(operation, value){
            this.orWhere('{{getName}}', operation, value);
            return this;
        }
        {{/checkClientColP where}}
        {{#checkClientColP select}}
        self.select{{getNameUCFirst}} = function(){
            this.columns.push('{{getName}}');
            return this;
        }
        {{/checkClientColP select}}
        {{#isClass}}
        {{#checkClientColP set}}
        self.set{{getNameUCFirst}} = function(collectionsPrepare, logic){
            logic = logic || "and";
            this.joins['{{getName}}'] = collectionsPrepare;
            collectionsPrepare.setConnectionLogic(logic);
            return this;
        }
        {{/checkClientColP set}}
        {{/isClass}}
        {{/getAttributes}}
        {{#getAssociationEnds}}
        {{^isCardinalityZero}}
        {{#checkClientColP select}}
        self.select{{getNameUCFirst}} = function(){
            this.columns.push('{{getName}}');
            return this;
        }
        {{/checkClientColP select}}
        {{#checkClientColP set}}
        self.{{setAssociationFunction}} = function(collectionsPrepare, logic){
            logic = logic || "and";
            this.joins['{{getName}}'] = collectionsPrepare;
            collectionsPrepare.setConnectionLogic(logic);
            return this;
        }
        {{/checkClientColP set}}
        {{#isCardinalitySingle}}
        {{#checkClientColP where}}
        self.where{{getNameUCFirst}} = function(operation, value){
            this.where('{{getName}}', operation, value);
            return this;
        }
        self.orWhere{{getNameUCFirst}} = function(operation, value){
            this.orWhere('{{getName}}', operation, value);
            return this;
        }
        {{/checkClientColP where}}
        {{/isCardinalitySingle}}
        {{^isCardinalitySingle}}
        {{#isAssociationClass}}
        //assocClass 
    //    self.where{{getNameUCFirst}} = function(operation, value){
    //        this.where('{{getName}}', operation, value);
    //        return this;
    //    }
    //    self.orWhere{{getNameUCFirst}} = function(operation, value){
    //        this.orWhere('{{getName}}', operation, value);
    //        return this;
    //    }
        {{/isAssociationClass}}
        {{/isCardinalitySingle}}
        {{#getParent.isAssociationClass}}
        {{^isAssociationClass}}
        {{#checkClientColP set}}
        self.set{{getParent.getName}} = function(collectionsPrepare, logic){
            logic = logic || "and";
            this.joins['{{getParent.getName}}'] = collectionsPrepare;
            collectionsPrepare.setConnectionLogic(logic);
            return this;
        }
        {{/checkClientColP set}}
        {{/isAssociationClass}}
        {{/getParent.isAssociationClass}}

        {{#isAssociationClass}}
        {{#getOppositeAssociationEnd.checkClientColP where}}
        self.where{{getClass.getName}} = function(operation, value){
            this.where('{{getClass.getNameLCFirst}}', operation, value);
            return this;
        };
        self.orWhere{{getClass.getName}} = function(operation, value){
            this.orWhere('{{getClass.getNameLCFirst}}', operation, value);
            return this;
        };
        {{/getOppositeAssociationEnd.checkClientColP where}}
        {{#getOppositeAssociationEnd.checkClientColP select}}
        self.select{{getClass.getName}} = function(operation, value){
            this.where('{{getClass.getNameLCFirst}}', operation, value);
            return this;
        }
        {{/getOppositeAssociationEnd.checkClientColP select}}

        {{#getOppositeAssociationEnd.checkClientColP set}}
        self.set{{getClass.getName}} = function(collectionsPrepare, logic){
            logic = logic || "and";
            this.joins['{{getClass.getName}}'] = collectionsPrepare;
            collectionsPrepare.setConnectionLogic(logic);
            return this;
        }
        {{/getOppositeAssociationEnd.checkClientColP set}}
        {{/isAssociationClass}}
        {{/isCardinalityZero}}
        {{/getAssociationEnds}}
    
    };

    {{getName}}CollectionPrepare.prototype.getClassName = function(){
        return '{{getName}}CollectionPrepare';
    };

    {{getName}}CollectionPrepare.prototype.getNsClassName = function(){
        return '{{#pathToNamespace 2}}{{getWorkPackage.getPath}}{{/pathToNamespace 2}}\\{{getName}}CollectionPrepare';
    };

    {{getName}}CollectionPrepare.prototype.getObjectClassName = function(){
        return '{{getName}}';
    };

    {{getName}}CollectionPrepare.create = function(){
        return new {{getName}}CollectionPrepare();
    };

    {{#getClientSpecializes table}}
    FCVClass.extend({{../getName}}CollectionPrepare, {{getName}}CollectionPrepare);
    {{/getClientSpecializes table}}

    {{^getClientSpecializes table}}
    FCVClass.extend({{../getName}}CollectionPrepare, FCVCollectionPrepare);
    {{/getClientSpecializes table}}
        
    globalScope.{{getName}}CollectionPrepare = {{getName}}CollectionPrepare;

    return {{getName}}CollectionPrepare;        
});