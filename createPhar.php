<?php

//php.ini set phar.readonly = 0
ini_set('phar.readonly',0);

// create with alias "project.phar"
$phar = new Phar('dist/fcv.phar', 0, 'fcv.phar');
// add all files in the project
$phar->buildFromDirectory(dirname(__FILE__));
$phar->setStub($phar->createDefaultStub('cli.php', 'index.php'));