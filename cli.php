<?php

use fcv\fcvPackage\Build;
use fcv\fcvPackage\CodeTemplate;
use fcv\fcvPackage\CodeTemplateCollection;
use fcv\fcvPackage\ModulPosition;
use fcv\fcvPackage\ModulPositionCollection;
use fcv\fcvPackage\Project;
use fcv\libs\Debug;
use Symfony\Component\Yaml\Yaml;

ini_set('memory_limit','4096M');

ob_start();

include('vendor/autoload.php');

Debug::getObj()->setDebugPoint('Start');

include("php/libs/Mustache_2.3.1/Mustache/Autoloader.php");
Mustache_Autoloader::register();
//Libaries


include('php/functions.php');
$jsHtml = '';

ini_set('memory_limit', '3096M');
include('php/libs/MetaModels/Parser/xmiParser.php');
include('php/libs/MetaModels/Parser/xmiParserArgo.php');

include('php/libs/MetaModels/UML/UMLModel.php');
include('php/libs/MetaModels/Mysql/MysqlModel.php');
//include('php/libs/MetaModels/FCV/FCVModel.php');
include('php/libs/MetaModels/UMLMerge/UMLMerge.php');
//php cli.php -u uml/test.uml -b tmp -f FCVAPs build

$options = getopt("u:p:b:f:c:d:", [
    "umlpath:",
    "project-name:",
    "build-path:",
    "fcvaps-path:",
    "config:",
    "defaultContentPath:",
    "first-build",
    "dontbuild-wp",
    "build-production",
    "help",
]);

if (isset($options["help"])) {
    // Print help message and exit
    echo "Usage: script.php [options]\n";
    echo "Options:\n";
    echo "  -u, --umlpath [path]       path to the argo-uml file (required)\n";
    echo "  -p, --project-name [name]  name of the project (required)\n";
    echo "  -b, --build-path [path]    path where the files should be generated (default: '.')\n";
    echo "  -f, --fcvaps-path [path]   path where the FCVAPs should be read (default: 'FCVAPs')\n";
    echo "  -c, --config [path]        path to the config file (default: 'fcvConfig/web.yaml')\n";
    echo "  -d, --defaultContentPath   path to default templates for the different types (default: 'fcvConfig/defaultContent')\n";
    echo "      --first-build          build initial files (default: false)\n";
    echo "      --dontbuild-wp         build workpackages folder and content (default: true)\n";
    echo "      --build-production     build project for production (default: true)\n";
    echo "      --help                 display this help message\n";
    exit(0);
}

// Set default values for options that have them
$options += [
    "build-path" => ".",
    "fcvaps-path" => "FCVAPs",
    "config" => "fcvConfig/web.yaml",
    "defaultContentPath" => "fcvConfig/defaultContent",
];

foreach ([
    "u" => "umlpath", 
    "p" => "project-name",
    "b" => "build-path",
    "f" => "fcvaps-path",
    "c" => "config",
    "d" => "defaultContentPath",
    ] as $shortOption => $longOption) {

    if (isset($options[$shortOption])) {
        $options[$longOption] = $options[$shortOption];
    } elseif (isset($options[$longOption])) {
        $options[$longOption] = $options[$longOption];
    }
}


$cmd = $argv[(count($argv)-1)];

echo "Running cmd: $cmd";
error_reporting(E_ALL);
// Validate required options
if (!isset($options['umlpath']) || !isset($options['fcvaps-path'])) {
    fwrite(STDERR, "Error: --umlpath (-u) and --project-name (-p) are required options.\n");
    echo("Error: --umlpath (-u) and --project-name (-p) are required options.\n");
    exit(1);
}
if ($cmd != "build") {
    fwrite(STDERR, "Error: the build command needs to be passed: php fcv.phar -p ... -u ... build\n");
    echo("Error: the build command needs to be passed: php fcv.phar -p ... -u ... build\n");
    exit(1);
}

set_include_path(get_include_path() . PATH_SEPARATOR . dirname($options['fcvaps-path']));

$umlPath = $options['umlpath'];
$fcvApsPath = $options['fcvaps-path'];
$configFilePath = $options['config'];
$projectName = $options['project-name'];
$defaultContentPath = $options['defaultContentPath'];
$buildPath = $options['build-path'];
$buildworkpackages = isset($options['dontbuild-wp']);
$firstBuild = !isset($options['first-build']);
$buildProduction = isset($options['build-production']);


$basePath = dirname($configFilePath);

$configFileContent = file_get_contents($configFilePath);
$config = Yaml::parse($configFileContent);

foreach($config as $field => $value){
    switch($field){
        case 'includeConfig':
            $value = !is_array($value) ? [$value] : $value;
            foreach($value as $includePaths){
                $configFilePath = $basePath.'/'.$includePaths;
                $configFileContent = file_get_contents($configFilePath);
                $configInclude = Yaml::parse($configFileContent);
                
                if($configInclude['templates'] && is_array($configInclude['templates'])){
                    foreach($configInclude['templates'] as $index => $tplArray){
                        $tplArray['basePath'] = dirname($configFilePath);
                        $configInclude['templates'][$index] = $tplArray;
                    }
                }
                $config = array_merge_recursive($config, $configInclude);
            }
            break;
        case 'security':
            $value = !is_array($value) ? [$value] : $value;
            if(isset($value['colPWhitelist']) && isset($value['colPWhitelist']['includeFile']) && isset($value['colPWhitelist']['variableName'])){
                $includeFile = $value['colPWhitelist']['includeFile'];
                $varName = $value['colPWhitelist']['variableName'];
                UMLProperties::$colPWhitList = $includeFile;
                UMLProperties::$colPWhitListVar = $varName;
            }
            break;
    }
}


$project = Project::create()
    ->setName($projectName)
    ->setUmlPath($umlPath)
;
Build::$defaultContentPath = $defaultContentPath;

$codeTemplates = CodeTemplateCollection::create();
$updatePositions = [];

$getPositionFromNames = function($names){
    $ret = ModulPositionCollection::create();
    foreach($names as $n){
        $ret->add(ModulPosition::create()->setName($n));
    }
    return $ret;
};

if($config['templates'] && is_array($config['templates'])){
    foreach($config['templates'] as $name => $tpl){
        $position = isset($tpl['position']) ? $getPositionFromNames([$tpl['position']])->current()
                    : null;
        $positions = isset($tpl['positions']) ? $getPositionFromNames($tpl['positions']) : null;
        
        $currentBasePath = isset($tpl['basePath']) ? $tpl['basePath'] : $basePath;
        $codeTpl = 
            CodeTemplate::create()
            ->setName($name)
            ->setType(isset($tpl['type']) ? $tpl['type'] : null)
            ->setTemplate(file_get_contents($currentBasePath.'/'.$tpl['template']))
            ->setRequiereUmlProperties(isset($tpl['require']) ? $tpl['require'] : null)
            ->setPath(isset($tpl['buildTo']) ? $tpl['buildTo'] : null)
            ->setUpdateTemplateFromFile(false)
            ->setPosition($position);
        
        if($positions){
            $codeTpl->setPositions($positions);
        }
        
        if(!$position){
            $codeTemplates->add($codeTpl);
        } else {
            if(!isset($updatePositions[$tpl['position']])){
                $updatePositions[$tpl['position']] = CodeTemplateCollection::create();
            }
            $updatePositions[$tpl['position']]->add($codeTpl);
        }
    }
}

foreach($codeTemplates as $codeTemplate){
    if($codeTemplate->hasPositions()){
        $positionNames = $codeTemplate->getPositions()->map(function($p){
            return $p->getName();
        });
        foreach($updatePositions as $position => $childTemplates){
            if(in_array($position, $positionNames)){
                $childTemplates->each(function($el) use(&$codeTemplate){
                    $codeTemplate->addCodeModul($el);
                });
            }
        }
    }
}

switch($cmd){
    case 'build':
        echo "Building project: $projectName production mode: " . $buildProduction;
        $build = Build::create()
        ->setBuildPath($buildPath)
        ->setFirstBuild($firstBuild)
        ->setProject($project)
        ->setBuildWorkpackages(!$buildProduction)
        ->setWorkPackagePath($fcvApsPath ? $fcvApsPath : null );
        $project->setCurrentBuild($build);
        
        $project->update($codeTemplates, $buildworkpackages);
}