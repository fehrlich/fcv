<?php
class XMIElement{
    public static $settingTpl = array(
        'parseTo' => '',
        'reverseLink' => '',
        'parse' => false,
        'isArray' => false
    );
    /**
     * @var string Description
     */
    public $name;
    function getname(){ return $name; }
    /**
     * @var string Description
     */
    public $id;
    function getid(){ return $id; }
    /**
     * @var string Description
     */
    public $type;
    function gettype(){ return $type; }
    /**
     * @var string Description
     */
    public $comment;
    function getcomment(){ return $comment; }
    /**
     * @var array Description
     */
    public $childs;
    function getchilds(){ return $childs; }
    /**
     * @var array Description
     */
    public $attributes;
    function getattributes(){ return $attributes; }
    /**
     * @var array Description
     */
    public $generalization;
    function getgeneralization(){ return $generalization; }
    /**
     * @var string Description
     */
    public $extends;
    function getextends(){ return $extends; }
    /**
     * @var array Description
     */
    public $memberEnds;
    function getmemberEnds(){ return $memberEnds; }
    /**
     * @var string Description
     */
    public $ownedEnd;
    function getownedEnd(){ return $ownedEnd; }
    /**
     * @var string Description
     */
    public $association;
    function getassociation(){ return $association; }
    /**
     * @var string Description
     */
    public $aggregation;
    function getaggregation(){ return $aggregation; }
    /**
     * @var string Description
     */
    public $visibility;
    function getvisibility(){ return $visibility; }
    /**
     * @var string Description
     */
    public $dataType;
    function getdataType(){ return $dataType; }
    /**
     * @var string Description
     */
    public $lowerValue;
    function getlowerValue(){ return $lowerValue; }
    /**
     * @var string Description
     */
    public $upperValue;
    function getupperValue(){ return $upperValue; }
    /**
     * @var string Description
     */
    public $value;
    function getvalue(){ return $value; }
    /**
     * @var array Description
     */
    public $constraints;
    function getconstraints(){ return $constraints; }
    /**
     * @var string Description
     */
    public $constrain;
    function getconstrain(){ return $constrain; }
    /**
     * @var string Description
     */
    public $specification;
    function getspecification(){ return $specification; }
    /**
     * @var string Description
     */
    public $language;
    function getlanguage(){ return $language; }
    /**
     * @var string Description
     */
    public $bodyValue;
    function getbodyValue(){ return $bodyValue; }
    /**
     * @var string Description
     */
    public $refLink;
    function getrefLink(){ return $refLink; }
}