<?php
/**
 * {{&getComment}}
 */
class {{getName}} {{#getExtends}}extends {{.}}{{/getExtends}}{
    {{#getPackagedElements}}
    private ${{getName}} = array();
    
    function get{{getName}}(){
        return ${{getName}};
    }
    {{/getPackagedElements}}
}