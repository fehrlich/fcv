<?php
{{!>Array
(
    [name] => s
    [type] => s
    [comment] => s
    [childs] => a
    [attributes] => a
    [generalization] => a
    [extends] => s
    [memberEnds] => a
    [ownedEnd] => a
    [association] => s
    [aggregation] => s
    [visibility] => s
    [dataType] => s
    [lowerValue] => s
    [upperValue] => s
    [constraints] => a
    [constrain] => s
    [specification] => s
    [language] => s
    [bodyValue] => s
)}}
/**
 * {{&comment}} Type {{type}}
 */
class {{name}} 
    {{#generalization}}extends {{extends.name}}{{/generalization}}
    {{^generalization}}{{#type}}extends {{.}}{{/type}}{{/generalization}}    
    {

    //Attributes
    {{#attributes}}
    /**
     * @var {{type}} {{./comment}}
     */
    private ${{name}} = array();
    
    function get{{name}}(){return $this->{{name}};}
    {{/attributes}}
    
    //Childs
    {{#childs}}
    /**
     * @var {{type}} {{./comment}}
     */
    private ${{name}} = array();
    
    function get{{name}}(){return $this->{{name}};}
    {{/childs}}
    
    //Associations
    {{#ownedEnd}}
        /**
     * @var {{type}}[] {{./comment}}
     */
    private ${{name}} = array();
    
    function get{{name}}(){return $this->{{name}};}
    {{/ownedEnd}}
    
    //Associations2
    {{#association}}
        /**
     * @var {{type}}[] {{./comment}}
     */
    private ${{name}} = array();
    
    function get{{name}}(){return $this->{{name}};}
    {{/association}}
}