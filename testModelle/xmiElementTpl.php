<?php
class XMIElement{
    public static $settingTpl = array(
        'parseTo' => '',
        'reverseLink' => '',
        'parse' => false,
        'isArray' => false
    );
    {{#fields}}
    /**
     * @var {{#isArray}}array{{/isArray}}{{^isArray}}string{{/isArray}} Description
     */
    public ${{parseTo}};
    function get{{parseTo}}(){ return ${{parseTo}}; }
    {{/fields}}
}