<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of settings2Model
 *
 * @author XX
 */
class Settings2Model {
    private $settings;
    private $settingsArray;
    private $vars;
    function __construct($settings) {
        $this->settings = $settings;
    }
    function parse(){
        $i = 0;
        foreach($this->settings as $path => $parseEl){
//            d($parseEl);
            if(!is_array($parseEl)){
                $parseEl = array('parseTo' => $parseEl);
            }
            $parseEl['path'] = $path;
            if(!isset($vars[$parseEl['parseTo']])){
                $this->settingsArray[$i] = array_merge(MetaModelParser::$settingTpl, $parseEl);
                $vars[$parseEl['parseTo']] = true;
                $i++;
            }
        }
    }
    function build(){
        $tpl = new TemplateEngine();
        $classContent = $tpl->render(file_get_contents('testModelle/xmiElementTpl.php'), array('fields' => $this->settingsArray));
        file_put_contents('testModelle/XMIElement.php', $classContent);
    }
}
