<?php

use fcv\fcvPackage\Build;
if(isset($_GET['build']) && $_GET['build'] > 0){
    $b = Build::load($_GET['build']);
    $b->getBuildWorkpackages = true;
    return $b;
}else{
    throw Exception('No Build selected');
}