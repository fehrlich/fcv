<?php

use fcv\libs\Debug;
use fcv\fcvPackage\Project;
use fcv\fcvPackage\WorkPackage;
use fcv\fcvPackage\WorkPackageCollection;
/* @var $this Project*/
/* @var $workPackage WorkPackage*/

$root = ($workPackage === false);
if(!$workPackage){
    $workPackage = WorkPackage::create()
                ->setId($this->getId())
                ->setType('Project')
                ->setUmlElement($this->getUmlModel())
                ->setProject($this)
                ->setChilds(WorkPackageCollection::create());
    $this->getUmlModel()->setWorkPackage($workPackage);
    Debug::getObj()->startMeasure('buildPackage', 'Building Packages From UML');
}
$umlPackage = $workPackage->getUmlElement();

$classes = array_merge($umlPackage->getClasses(), $umlPackage->getAssociationClasses());
foreach($classes as $class){
    if(!$class->hasStereoType("Connector")){
        $wp = WorkPackage::create()
                ->setId($class->getId())
                ->setType('Class')
                ->setUmlElement($class)
                    ->setProject($this)
                ->setChilds(WorkPackageCollection::create());
        $class->setWorkPackage($wp);
        $workPackage->addworkPackage($wp);

        foreach($class->getOperations() as $operation){
            $w = WorkPackage::create()
                    ->setId($operation->getId())
                    ->setType('Method')
                    ->setUmlElement($operation)
                    ->setProject($this)
                    ->setChilds(WorkPackageCollection::create());;
            $wp->addworkPackage($w);
            $operation->setWorkPackage($w);
        }
    }
}

foreach($workPackage->getUmlElement()->getPackages() as $umlPackage){
    $package = WorkPackage::create()
                ->setId($umlPackage->getId())
                ->setType('Package')
                ->setUmlElement($umlPackage)
                    ->setProject($this)
                    ->setChilds(WorkPackageCollection::create());
    $workPackage->addworkPackage($package);
    $umlPackage->setWorkPackage($workPackage);
    $this->buildWorkPackages($package);
}

if($root){

    //Activity Graphs
    $pagesAndFrames = array();
    $activityGraphs = $this->getUmlModel()->getActivityGraphs();
    foreach($activityGraphs as $graph){
        $graph->iterate(function($umlElement) use (&$pagesAndFrames){
            if($umlElement->hasStereoType('page') || $umlElement->hasStereoType('frame')) $pagesAndFrames[] = &$umlElement;
        });
    }

    foreach($pagesAndFrames as $umlElement){
       $w = WorkPackage::create()
                    ->setId($umlElement->getId())
                    ->setUmlElement($umlElement)
//                    ->setType('Class')
                    ->setType($umlElement->hasStereoType('page')?'Page':'Frame')
                    ->setProject($this)
               ->setChilds(WorkPackageCollection::create());;
//       $debug = $umlElement->getId() == '-64--88-0-100--6ac4e88f:140a0214d01:-8000:0000000000000C7D';
//        if($umlElement->getId() == '-64--88-0-100--6ac4e88f:140a0214d01:-8000:0000000000000C7D'){
////            dd($w);
////            dd($umlElement);
//            $debug = true;
//        }
        $umlElement->setWorkPackage($w);
//        if($debug) d($w);
        $workPackage->addWorkPackage($w);
//        if($debug) dd($workPackage);
    }


    $this->addWorkPackage($workPackage);
//    dd($workPackage);
    Debug::getObj()->stopMeasure('buildPackage', 'Building Packages');
    Debug::getObj()->startMeasure('savePackages', 'save Packages');
    $workPackage->treeSave();
    Debug::getObj()->stopMeasure('savePackages');
    Debug::getObj()->startMeasure('buildPackageFS', 'build Packages from FileSystem');
    $workPackage->build();
    Debug::getObj()->stopMeasure('buildPackageFS');
//    echo 'WUT';
    Debug::getObj()->startMeasure('buildModules', 'build Modules');
    $workPackage->buildModules();
    Debug::getObj()->stopMeasure('buildModules');
}