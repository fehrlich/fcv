<?php
/* @var $this CodeModul*/

if($this->hasExtends()){
//    $this->getExtends(); // = CodeModul::load($this->extends);
    $extends = $this->getExtends()->toArray(TRUE);
    foreach($extends as $field => $val){
        if(empty($this->$field) || is_null($this->$field)) $this->$field = $val;
    }
}