<?php
/* @var $this CodeTemplate*/
$path = 'moduleTemplates'.$this->getAbsolutePath();
try {
    $filepath = $path . '/' . $this->getName() . '.' . $this->getLanguage()->getName();
} catch (Exception $exc) {
    d($this);
    d(1);
    exit();
}

//Todo: check if needed !? only on creation ?
//Debug::getObj()->info('Check '.$this->getName());
if(!is_dir($path)) mkdir ($path,0777, true);
if(!file_exists($filepath)) touch($filepath);

//Todo: Check file modified date before file_get_content
$content = file_get_contents($filepath);
$this->setUpdated($this->getTemplate() != $content);
$this->setUpdated(true);
$this->template = $content;

if($this->getUpdated()){
//    Debug::getObj()->info('CodeTemplate '.$this->getName().' updated!');
    $this->save();
}
return $this;