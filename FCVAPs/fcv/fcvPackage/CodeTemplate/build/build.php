<?php

use fcv\libs\Debug;
use fcv\fcvPackage\CodeTemplate;
use fcv\fcvPackage\CodeTemplateCollectionPrepare;
use fcv\fcvPackage\ModulPosition;
use fcv\fcvPackage\ModulPositionCollectionPrepare;
use fcv\libs\TemplateEngine;

/* @var $this CodeTemplate */
//Debug::getObj()->info('Build ' . $this->getName());
if ($this->requiereUmlProperties != '') {
    $spl = explode(',', $this->requiereUmlProperties);
    foreach ($spl as $prop) {
        if (substr($prop, 0, 1) == '^') {
            $negate = true;
            $prop = substr($prop, 1);
        }
        else
            $negate = false;
        $umlElement = $this->getWorkPackage()->getUmlElement();
        if (!method_exists($umlElement, $prop) || ($this->getWorkPackage()->getUmlElement()->$prop() == $negate)) {
//            Debug::getObj()->info('Skipped ' . $this->getName() . ' does not meet reuqirements ' . $prop . ' == ' . $negate);
            return '';
        }
    }
}

$this->loadTemplateFromFile();

if (!$this->getUpdated() && $this->path != '' && !$this->getWorkPackage()->getUmlElement()->hasChanged() && !$this->getWorkPackage()->getUmlElement()->isNew()) {
    return '';
}

Debug::getObj()->info('BUILD CodeTpl ' . $this->getName() . ' ('.($this->getUpdated()?'U':'').($this->path == ''?'S':'').') for WP '. $this->getWorkPackage()->getName().' ('.($this->getWorkPackage()->getUmlElement()->isNew()?'N':'').($this->getWorkPackage()->getUmlElement()->hasChanged()?'C':'').')');

$content = $this->getTemplate();

$tplEngine = new TemplateEngine();
if ($this->hasPositions()) {
    $positions = $this->getPositions();

    $debug = false;
    foreach ($positions as $pos) {
        $subTemplates = CodeTemplateCollectionPrepare::create()
                ->setPosition(
                    ModulPositionCollectionPrepare::create()
                        ->wherePositionId('=', $pos->getId())
//                        ->selectAll()
//                        ->set
                )
                ->selectAll()->get();
//        if($this->getId() == 2){
//            $q = CodeTemplateCollectionPrepare::create()
//                ->setPosition(
//                ModulPositionCollectionPrepare::create()
//                        ->wherePositionId('=', $pos->getId())
//                        ->selectAll()
////                        ->set
//                )->selectAll();
//            d($q->prepareQuery()->getQuery()->toSql());
//            d($subTemplates);
//            exit();
//        }
//        $subTemplates = CodeTemplate::search(
//                        CodeTemplate::create(array(), true)
//                                ->setPosition(
//                                        ModulPosition::create(array(), TRUE)
//                                        ->setPositionId($pos->getId())
//                                )
//                        , '', ''
//                        , null
////                CodeTemplate::create(array(), true)
////                    ->setName(1)
////                    ->setPath(1)
////                    ->setParent(1)
//                        , $debug
//        );
//        
        $subContent = '';
        foreach ($subTemplates as $subTpl) {
            $subTpl->setWorkPackage($this->getWorkPackage());
            $subTplC = $subTpl->build();

            $subContent .= $subTplC;
        }
        $space = str_pad("", $pos->getIndent() * 4);
        $subContent = str_replace(array("\r\n", "\n"), array("\n", "\n" . $space), $subContent);
        $content = str_replace('{{Position' . $pos->getName() . '}}', $subContent, $content);
    }
}
//if($this->getId() == 7){
//    d($this); 
//   d($this->getWorkPackage()->getUmlElement());
//   echo '<textarea>'.$content.'</textarea>';
//   echo '<textarea>'.$tplEngine->render($content, $this->getWorkPackage()->getUmlElement()).'</textarea>';
//    exit();
//}
$content = $tplEngine->render($content, $this->getWorkPackage()->getUmlElement());

if ($this->path != '') {
    $path = $this->getWorkPackage()->getProject()->getBuildPath() . '/' . $this->getPath();
    $path = $tplEngine->render($path, $this->getWorkPackage());
    $dir = dirname($path);
    if (!is_dir($dir))
        mkdir($dir, 0777, true);
    file_put_contents($path, $content);
    return '';
}elseif ($this->hasPosition()) {
    return $content;
} else {
    echo 'Error: CodeTemplate "' . $this->getName() . '" has no Position or Path';
}
