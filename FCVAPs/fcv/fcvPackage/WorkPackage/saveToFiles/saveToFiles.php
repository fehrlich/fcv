<?php
/* @var $this WorkPackage*/

$apPath = $this->getProject()->getCurrentBuild()->getBuildPath().'/FCVAPs';
$path = $apPath.$this->getPath();

if(!is_dir($path)) mkdir ($path,0777, true);
file_put_contents($path.'/'.$this->getName().'.php', $this->functionContent);
file_put_contents($path.'/'.$this->getName().'.js', $this->controlContent);

$updateViews = ViewsCollection::create();
if($this->hasViews()){
    foreach($this->getViews() as $view){
        if(!empty($view->getHtmlContent()) || !empty($view->getCssContent()) || $view->forceWrite){
            file_put_contents($path.'/'.$this->getName().'.'.$view->getName().'.html', $view->getHtmlContent());
            file_put_contents($path.'/'.$this->getName().'.'.$view->getName().'.css', $view->getCssContent());
            $updateViews->add($view);
        }
        if(!empty($view->getPhpContent())){
            file_put_contents($path.'/'.$this->getName().'.'.$view->getName().'.php', $view->getPhpContent());
        }
    }
}
$this->setViews($updateViews);

if($this->hasChilds()){
    $this->getChilds()->saveToFiles();
}