<?php

use fcv\fcvPackage\Views;
use fcv\fcvPackage\WorkPackage;
use fcv\libs\TemplateEngine;
/* @var $this WorkPackage*/

if($this->getType() == 'Attribute') return;
if($this->getType() == 'Package') return;
if($this->getType() == 'Model') return;

$apPath = $this->getProject()->getBuildPath().'/FCVAPs';
$path = $apPath.$this->getPath();

//Todo: Outsourcen

$defaultFunctionTpl = file_get_contents('moduleTemplates/defaultContent/'.strtolower($this->getType()).'.php');
$defaultControlTpl  = file_get_contents('moduleTemplates/defaultContent/'.strtolower($this->getType()).'.js');

$tplEngine = new TemplateEngine();

$defaultFunction = $tplEngine->render($defaultFunctionTpl, $this);
$defaultControl = $tplEngine->render($defaultControlTpl, $this);

$functionContent = @file_get_contents($path.'/'.$this->getName().'.php');
$controlContent =  @file_get_contents($path.'/'.$this->getName().'.js');
$update = false;

$functionContentExists = $functionContent !== false;
$controlContentExists = $functionContent !== false;

$functionContent = $functionContentExists ? $functionContent : $this->functionContent;
$controlContent = $functionContentExists ? $controlContent : $this->controlContent;

if($this->functionIsDefault) $this->setFunctionIsDefault(!$functionContentExists || $functionContent == $defaultFunction);
$this->functionContent = ($this->functionIsDefault)?$defaultFunction:$functionContent;

if($this->controlIsDefault) $this->setControlIsDefault(!$controlContentExists || $controlContent == $defaultControl);
$this->controlContent = ($this->controlIsDefault)?$defaultControl:$controlContent;

$viewContentFields = array();

foreach(Views::$fcvFields as $field){
    if(substr($field, -7) == 'Content'){
        $viewContentFields[] = substr($field, 0, -7);
    }
}

if(!$this->hasViews() && $this->getType() == 'Frame'){
    $this->addViews(
        Views::create()
            ->setName('default')
            ->setHtmlContent('')
            ->setCssContent('')
            ->setPhpContent('')
    );
}else{
    $views = $this->getViews();

    foreach($views as $v){
        foreach(Views::$fcvFields as $field){
            if(substr($field, -7) == 'Content'){
                $type = substr($field, 0, -7);
                $viewConent = @file_get_contents($path.'/'.$this->getName().'.'.$v->getName().'.'.$type);

                $v->set($field, $viewConent);
                $v->save();
            }
        }
    }
}

//load new views

$files = glob($path."/".$this->getName().'.*.'.$viewContentFields[0]);
if($files){
    foreach ($files as $filename) {
        $viewNameAndExt = substr(basename($filename),  strlen($this->getName())+1);
        $spl = explode('.', $viewNameAndExt);
        $viewName = $spl[0];
        $exists = false;
        if($this->hasViews()){
            foreach($this->getViews() as $v){
                if($v->getName() == $viewName) $exists = true;
            }
        }
        if(!$exists){
            echo 'NewView: '.$viewName.'<br>';
            $newView = Views::create()
                ->setName($viewName);

            foreach($viewContentFields as $viewField){
                $f = $path.'/'.$this->getName ().'.'.$viewName.'.'.$viewField;
                $vContent = @file_get_contents($f);
                $newView->set($viewField.'Content', $vContent);
            }
            $this->addViews($newView);
        }
    }
}

$this->save();