<?php
/* @var $this WorkPackage*/

$this->loadContentFromSource();
$this->save();
if($this->hasViews()){
    $this->getViews()->save();
}
if($this->hasChilds()){
    $this->getChilds()->build();
}
//if($this->getUmlElement()->isNew()){ //$this->getProject()->getCurrentBuild()->getBuildWorkpackages() && 
    $this->saveToFiles();
//}
//echo 'Type:'.$this->getName().':'.$this->getType().'<br>';

