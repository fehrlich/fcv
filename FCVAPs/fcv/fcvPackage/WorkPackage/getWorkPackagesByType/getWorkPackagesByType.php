<?php
/* @var $this WorkPackage*/
$arr = array();
if($this->getType() == $type){
    $arr[] = $this;
}
if($this->hasChilds()){
    foreach($this->getChilds() as $child){
        $arr = array_merge($arr, $child->getWorkPackagesByType($type));
    }
}
return $arr;