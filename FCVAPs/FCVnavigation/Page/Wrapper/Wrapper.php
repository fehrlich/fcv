<?php
/* @var $this Wrapper*/
ini_set('memory_limit', '512M');
include('php/libs/MetaModels/Parser/xmiParser.php');
include('php/libs/MetaModels/Parser/xmiParserArgo.php');

include('php/libs/MetaModels/UML/UMLModel.php');
include('php/libs/MetaModels/Mysql/MysqlModel.php');
//include('php/libs/MetaModels/FCV/FCVModel.php');
include('php/libs/MetaModels/UMLMerge/UMLMerge.php');

if(isSet($_POST['addLib']))     Library::create($_POST)->save();
if(isSet($_POST['addLang']))    CodeLanguage::create($_POST)->save();
if(isSet($_POST['addProj']))    Project::create($_POST)->save();
if(isSet($_GET['action']) && $_GET['action'] == 'update' && $_GET['build']) Project::load ($_GET['id'])->update (Build::load($_GET['build']));



$tpl = new TemplateEngine();

$this->projects = Project::search();
$this->libs = Library::search();
$this->languages = CodeLanguage::search();
$this->Builds = Build::search();
//languages
if(isSet($_GET['createContent']) && $_GET['createContent'] == '2'){
//    mys::getObj()->query('SET foreign_key_checks=0');
//    mys::getObj()->query('TRUNCATE TABLE WorkPackage');
//    mys::getObj()->query('SET foreign_key_checks=1');
    $p = Project::load(1);
//    dd($p);
//    $startTime = microtime(true);
    $p->update();
//    echo 'BuildTime: '.((microtime(true)-$startTime)).'s<br>';
//    exit();
//    $p->build(Build::create()->setName('dev'));
}
if(isSet($_GET['createContent']) && $_GET['createContent'] == '3'){
//    mys::getObj()->query('SET foreign_key_checks=0');
//    mys::getObj()->query('TRUNCATE TABLE WorkPackage');
//    mys::getObj()->query('SET foreign_key_checks=1');
//    $p = Project::load(2);
    $p = Project::create()
        ->setName('test')
        ->setUmlPath('https://franz:Wodili25@svn.21cms.de/svn/uml/test.uml')
        ->setMysqlHost('localhost')
        ->setMysqlDb('uml_test')
        ->setMysqlUser('kassen')
        ->setMysqlPassword('mobenovupatiyovuci63')
        ->setBuildPath('projects/test')
        ->setId(2)->save();
    $p->update();
//    $p->build(Build::create()->setName('dev'));
}
if(isSet($_GET['createContent']) && $_GET['createContent'] == '4'){
//    mys::getObj()->query('SET foreign_key_checks=0');
//    mys::getObj()->query('TRUNCATE TABLE WorkPackage');
//    mys::getObj()->query('SET foreign_key_checks=1');
    $p = Project::load(4);
//    dd($p);
    $p->update();
//    $p->build(Build::create()->setName('dev'));
}
if(isSet($_GET['createContent']) && $_GET['createContent'] == '1'){
    $php = CodeLanguage::create()->setName('php')->setId(1)->save();
    $js = CodeLanguage::create()->setName('js')->setId(2)->save();
    $html = CodeLanguage::create()->setName('html')->setId(3)->save();
    $css = CodeLanguage::create()->setName('css')->setId(4)->save();
    $mysqlLang = CodeLanguage::create()->setName('sql')->setId(5)->save();
    $startPosition = ModulPosition::create()->setName('ProjectStart')->setId(1)->save();
    $mysql = mys::getObj();
    $mysql->query('SET foreign_key_checks=0;');
    $mysql->query('TRUNCATE CodeTemplate_sub');
    $mysql->query('TRUNCATE ModulPosition');
    $mysql->query('TRUNCATE CodeLanguage');
    $mysql->query('TRUNCATE CodeModul');
    $mysql->query('SET foreign_key_checks=1');
    $apPath = 'FCVAPs/';
    $classPath = 'php/classes/';
    $jsClassPath = 'js/classes/';
    $defaultPath = "{{path}}/{{name}}.{{language}}";
    
    $p = Project::create()
        ->setName('impro')
        ->setUmlPath('https://franz:Wodili25@svn.21cms.de/svn/uml/impro_uml.zargo.uml')
        ->setMysqlHost('localhost')
        ->setMysqlDb('improFromUML')
        ->setMysqlUser('kassen')
        ->setMysqlPassword('mobenovupatiyovuci63')
        ->setBuildPath('projects/impro')
        ->setId(1);
    
    ModulPosition::create()->setName('Attributes')->setIndent(1)->setId(2);
    ModulPosition::create()->setName('Methods')->setIndent(1)->setId(2);
    
    $x = CodeModul::create()
            ->setName('FCV')
            ->addModulPosition($startPosition)
//            ->addCodeModul(
//                    CodeModul::create()
//                        ->setName('workPackages')
//                        ->addCodeModul(
//                            CodeTemplate::create()
//                                ->setLanguage($php)
//                                ->setName('wp_CLASSphp')
//                                ->setPath($apPath.$defaultPath)
//                        )
//                        ->addCodeModul(
//                            CodeTemplate::create()
//                                ->setLanguage($php)
//                                ->setName('wp_CLASSjs')
//                                ->setPath($apPath.$defaultPath)
//                        )
//            )
            ->addCodeModul(
                    CodeTemplate::create()
                        ->setLanguage($php)
                        ->setApplyTo('Class')
                        ->setPath($classPath.'{{getPath}}.php')
                        ->setName('PHP_CLASS')
                        ->setPosition($startPosition)
                        ->addModulPosition(ModulPosition::create()->setName('Attributes')->setIndent(1)->setId(2))
                        ->addModulPosition(ModulPosition::create()->setName('Methods')->setIndent(1)->setId(3))
                        ->addCodeModul(
                            CodeModul::create()
                                ->setName('Basic')
                                ->addCodeModul(
                                    CodeTemplate::create()
                                        ->setLanguage($php)
                                        ->setPosition(3)
                                        ->setName('USER_METHODS')
                                )
                                ->addCodeModul(
                                    CodeTemplate::create()
                                        ->setLanguage($php)
                                        ->setPosition(2)
                                        ->setName('USER_ATTRIBUTES')
                                )
                        )
                        ->addCodeModul(
                            CodeModul::create()
                                ->setName('Control')
                                ->addCodeModul(

                                    CodeTemplate::create()
                                        ->setLanguage($php)
                                        ->setPosition(3)
                                        ->setRequiereUmlProperties('^isInterface,^isEnum')
                                        ->setName('Construct')
                                )
                                ->addCodeModul(
                                    CodeTemplate::create()
                                        ->setLanguage($php)
                                        ->setPosition(3)
                                        ->setRequiereUmlProperties('^isInterface,^isEnum')
                                        ->setName('Getter_Setter_Adder_Haser')
                                )
                                ->addCodeModul(
                                    CodeTemplate::create()
                                        ->setLanguage($php)
                                        ->setPosition(3)
                                        ->setRequiereUmlProperties('^isInterface,^isEnum')
                                        ->setName('Control_Connector')
                                )
                                ->addCodeModul(
                                    CodeTemplate::create()
                                        ->setLanguage($php)
                                        ->setPosition(3)
                                        ->setRequiereUmlProperties('^isInterface,^isEnum')
                                        ->setName('View_Connector')
                                )
                        )
                              
                        ->addCodeModul(
                            CodeModul::create()
                                ->setName('Database')
                                ->addCodeModul(
                                    CodeTemplate::create()
                                        ->setLanguage($php)
                                        ->setPosition(3)
                                        ->setRequiereUmlProperties('^isInterface,^isEnum,isTable')
                                        ->setName('Save')
//                                        ->addCodeModul(
//                                            CodeTemplate::create()
//                                                ->setLanguage($php)
//                                                ->setPosition(3)
//                                                ->setName('History')
//                                        )
                                )
                                ->addCodeModul(
                                    CodeTemplate::create()
                                        ->setLanguage($php)
                                        ->setPosition(3)
                                        ->setRequiereUmlProperties('^isInterface,^isEnum,isTable')
                                        ->setName('Search')
                                )
                        )
            )
            ->addCodeModul(
                    CodeTemplate::create()
                        ->setLanguage($php)
                        ->setName('PHP_COLLECTION')
                        ->setApplyTo('Class')
                        ->setRequiereUmlProperties('^isInterface,^isEnum')
                        ->setPath($classPath.'{{getPath}}Collection.php')
            )
            ->addCodeModul(
                    CodeTemplate::create()
                        ->setLanguage($php)
                        ->setName('PHP_COLLECTIONPrepare')
                        ->setApplyTo('Class')
                        ->setRequiereUmlProperties('^isInterface,^isEnum,isTable')
                        ->setPath($classPath.'{{getPath}}CollectionPrepare.php')
            )
            ->addCodeModul(
                    CodeTemplate::create()
                        ->setLanguage($js)
                        ->setName('JS_COLLECTIONPrepare')
                        ->setApplyTo('Class')
                        ->setRequiereUmlProperties('^isInterface,^isEnum,isTable')
                        ->setPath($classPath.'{{getPath}}CollectionPrepare.php')
            )
//            ->addCodeModul(
//                    CodeTemplate::create()
//                        ->setLanguage($php)
//                        ->setExtend(2)
//                        ->setApplyTo('Frame')
////                        ->setPath($classPath.'{{getPath}}.php')
//                        ->addModulPosition(ModulPosition::create()->setName('Attributes')->setIndent(1)->setId(4))
//                        ->addModulPosition(ModulPosition::create()->setName('Methods')->setIndent(1)->setId(5))
////                        ->setPath($classPath.'{{getPath}}.php')
////                                ->addCodeModul(
////
////                                    CodeTemplate::create()
////                                        ->setLanguage($php)
////                                        ->setPosition(3)
////                                        ->setName('Construct')
////                                )
//            )
//            ->addCodeModul(
//                    CodeTemplate::create()
//                        ->setLanguage($php)
//                        ->setExtend(2)
//                        ->setApplyTo('Page')
//                        ->addModulPosition(ModulPosition::create()->setName('Attributes')->setIndent(1)->setId(6))
//                        ->addModulPosition(ModulPosition::create()->setName('Methods')->setIndent(1)->setId(7))
////                        ->setPath($classPath.'{{getPath}}.php')
////                        ->setName('PHP_CLASS')
////                        ->copyModul('')
////                        ->addModulPosition(ModulPosition::create()->setId(2))
////                        ->addModulPosition(ModulPosition::create()->setId(3))
////                                ->addCodeModul(
////
////                                    CodeTemplate::create()
////                                        ->setLanguage($php)
////                                        ->setPosition(3)
////                                        ->setName('Construct')
////                                )
//            )
            ->addCodeModul(
                    CodeTemplate::create()
                        ->setLanguage($js)
                        ->setName('JS_CLASS')
                        ->setApplyTo('Class')
                        ->setPath($jsClassPath.'{{getPath}}.js')
            )
            ->addCodeModul(
                CodeModul::create()
                    ->setName('Framework')
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setPosition(ModulPosition::create()->setId(2))
                            ->setRequiereUmlProperties('^isInterface,^isEnum')
                            ->setName('php_class_extension')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setApplyTo('Project')
                            ->setName('index')                        
                            ->setPath('index.php')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setApplyTo('Project')
                            ->setName('ajax')                        
                            ->setPath('ajax.php')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
//                            ->setRequiereUmlProperties($requiereUmlProperties)
                            ->setApplyTo('Project')
                            ->setName('include')                        
                            ->setPath('include.php')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setApplyTo('Project')
                            ->setName('Page')                        
                            ->setPath('/php/libs/fcv/Page.php')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setApplyTo('Project')
                            ->setName('Routing')                        
                            ->setPath('/php/libs/fcv/Routing.php')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setApplyTo('Project')
                            ->setName('Frame')                        
                            ->setPath('/php/libs/fcv/Frame.php')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setApplyTo('Project')
                            ->setName('Debug')                        
                            ->setPath('/php/libs/fcv/Debug.php')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setApplyTo('Project')
                            ->setName('JsObj')                        
                            ->setPath('/php/libs/fcv/JsObj.php')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setApplyTo('Project')
                            ->setName('SplEnum')                        
                            ->setPath('/php/libs/fcv/SplEnum.php')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setApplyTo('Project')
                            ->setName('TemplateEngine')                        
                            ->setPath('/php/libs/fcv/TemplateEngine.php')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setApplyTo('Project')
                            ->setName('Css-Dynamic')                        
                            ->setPath('css/dyn.css.php')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($css)
                            ->setApplyTo('Project')
                            ->setName('Css-Static')               
                            ->setPath('css/static.css')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setApplyTo('Project')
                            ->setName('JS-Init-Dyn')               
                            ->setPath('js/init.js.php')
                    )
            )
            ->addCodeModul(
                CodeModul::create()
                    ->setName('Database')
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($mysqlLang)
                            ->setApplyTo('Project')
                            ->setName('install')                        
                            ->setPath('/mysql/install.sql')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($mysqlLang)
                            ->setApplyTo('Project')
                            ->setName('update')                        
                            ->setPath('/mysql/update.{{getProject.getVersion}}.sql')
                    )
                    ->addCodeModul(
                        CodeTemplate::create()
                            ->setLanguage($php)
                            ->setApplyTo('Project')
                            ->setName('mys')                        
                            ->setPath('/php/libs/fcv/mys.php')
                    )
            )
//            ->addCodeModul(
//                    CodeModul::create()
//                        ->setName('NodeJs')
//                        ->addCodeModul(
//                            CodeTemplate::create()
//                                ->setLanguage($js)
//                                ->setApplyTo('ProjectRevision')
//                                ->setName('main')                        
//                                ->setPath('/main.js')
//                        )
//                        ->addCodeModul(
//                            CodeTemplate::create()
//                                ->setLanguage($js)
//                                ->setName('JS_CLASS')
//                                ->setApplyTo('Class')
//                                ->setPath('/classes/{{getPath}}.js')
//                        )
//                        ->addCodeModul(
//                            CodeTemplate::create()
//                                ->setLanguage($js)
//                                ->setName('JS_CLASS_METHODS')
//                                ->setApplyTo('Class')
//                                ->setPath('/classes/{{getPath}}.m2.js')
//                        )
//                )
            ->treeSave();
    $p->addCodeModul($x);
    $p->setActiveModules(CodeModul::search(
            CodeModul::create(array(), true)
                ->setName('PHP_COLLECTIONPrepare')
        ));
    $p->save();
//    $x->getCode
//    $x->getCodeTemplates()->
//    foreach($x->getCodeTemplates() as $codeTpl){
//        $codeTpl->getCodeTemplates()->loadTemplateFromFile();
//    }
    foreach(CodeTemplate::search() as $codeTpl){
//        new dBug($codeTpl->toArray(1));
        $codeTpl->loadTemplateFromFile();
    }
}