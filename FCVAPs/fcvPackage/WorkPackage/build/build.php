<?php
/* @var $this WorkPackage*/

$this->loadContentFromSource();
$this->save();
$this->getViews()->save();

if($this->hasChilds()){
    $this->getChilds()->build();
}
if($this->getProject()->getCurrentBuild()->getBuildWorkpackages() && $this->getUmlElement()->isNew()){
    $this->saveToFiles();    
}

