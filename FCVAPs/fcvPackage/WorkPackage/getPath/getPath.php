<?php
/* @var $this WorkPackage*/

if($this->type == 'Frame' || $this->type == "Page") return '/'.$this->getProject()->getName().'/FCVNavigation/'.$this->getType ().'/'.$this->getName();
if (!$this->hasParent()) {
    return '/';
}
if (!$this->hasParent() || $this->getParent()->getType() == 'Project') {
    return '/'.$this->getName();
}

$path = array();
$parent = $this->getParent();
//new dBug($parent);
$path[] = $parent->getName();
while($parent->hasParent() && $parent->getParent()->getType() != 'Project'){
    $parent = $parent->getParent();
//    d($parent);
    $path[] = $parent->getName();
}
$path[] = $this->getProject()->getName();
//d($this);
//dd('/'.implode('/',array_reverse($path)).'/'.$this->getName());
return '/'.implode('/',array_reverse($path)).'/'.$this->getName();