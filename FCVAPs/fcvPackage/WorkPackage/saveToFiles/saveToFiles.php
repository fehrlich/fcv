<?php
/* @var $this WorkPackage*/

$apPath = $this->getProject()->fcvgetBuildPath().'/FCVAPs';
$path = $apPath.$this->getPath();

if(!is_dir($path)) mkdir ($path,0777, true);
file_put_contents($path.'/'.$this->getName().'.php', $this->functionContent);
file_put_contents($path.'/'.$this->getName().'.js', $this->controlContent);

foreach($this->getViews() as $view){
    file_put_contents($path.'/'.$this->getName().'.'.$view->getName().'.html', $view->getHtmlContent());    
    file_put_contents($path.'/'.$this->getName().'.'.$view->getName().'.css', $view->getCssContent());    
}

if($this->hasChilds()){
    $this->getChilds()->saveToFiles();
}