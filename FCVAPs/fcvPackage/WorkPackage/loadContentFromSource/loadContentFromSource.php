<?php
/* @var $this WorkPackage*/

if($this->getType() == 'Attribute') return;
$apPath = $this->getProject()->getCurrentBuild()->getBuildPath().'/FCVAPs';
$path = $apPath.$this->getPath();
if(substr($path, 0, 1) == '/') $path = substr ($path, 1);

//Todo: Outsourcen
$type = strtolower($this->getType());
if(!isset(WorkPackage::$defaultContents[$type.'.php'])){
    WorkPackage::$defaultContents[$type.'.php'] = trim(file_get_contents('moduleTemplates/defaultContent/'.strtolower($this->getType()).'.php'));
}
if(!isset(WorkPackage::$defaultContents[$type.'.js'])){
    WorkPackage::$defaultContents[$type.'.js'] = trim(file_get_contents('moduleTemplates/defaultContent/'.strtolower($this->getType()).'.js'));
}
$defaultFunctionTpl = WorkPackage::$defaultContents[$type.'.php'];
$defaultControlTpl  = WorkPackage::$defaultContents[$type.'.js'];

$tplEngine = new TemplateEngine();

$defaultFunction = $tplEngine->render($defaultFunctionTpl, $this);
$defaultControl = $tplEngine->render($defaultControlTpl, $this);

$functionContent = @file_get_contents($path.'/'.$this->getName().'.php');
$controlContent =  @file_get_contents($path.'/'.$this->getName().'.js');
$update = false;

$functionContentExists = $functionContent !== false;
$controlContentExists = $functionContent !== false;

$functionContent = $functionContentExists ? trim($functionContent) : $this->functionContent;
$controlContent = $functionContentExists ? trim($controlContent) : $this->controlContent;

if($this->functionIsDefault) $this->setFunctionIsDefault($functionContent == $defaultFunction || in_array(trim(stripCommentsPHP($functionContent)), ['', '<?php']));
$this->functionContent = ($this->functionIsDefault)?$defaultFunction:$functionContent;

if($this->controlIsDefault) $this->setControlIsDefault($controlContent == $defaultControl || in_array(trim(stripCommentsJS($controlContent, true)), ['']));
$this->controlContent = ($this->controlIsDefault)?$defaultControl:$controlContent;

$viewContentFields = array();

foreach(Views::$fcvFields as $field){
    if(substr($field, -7) == 'Content'){
        $viewContentFields[] = substr($field, 0, -7);
    }
}

if(!$this->hasViews()){
    $this->addViews(
        Views::create()
            ->setName('default')
            ->setHtmlContent('')
            ->setCssContent('') 
    );
}else{
    $views = $this->getViews();

    foreach($views as $v){
        foreach(Views::$fcvFields as $field){
            if(substr($field, -7) == 'Content'){
                $type = substr($field, 0, -7);
                $viewConent = @file_get_contents($path.'/'.$this->getName().'.'.$v->getName().'.'.$type);

                $v->set($field, $viewConent);
                $v->save();
            }
        }
    }
}

//load new views

$files = glob($path."/".$this->getName().'.*.'.$viewContentFields[0]);
if($files){
    foreach ($files as $filename) {
        $viewNameAndExt = substr(basename($filename),  strlen($this->getName())+1);
        $spl = explode('.', $viewNameAndExt);
        $viewName = $spl[0];
        $exists = false;
        foreach($this->getViews() as $v){
            if($v->getName() == $viewName) $exists = true;
        }
        if(!$exists){
            echo 'NewView: '.$viewName.'<br>';
            $newView = Views::create()
                ->setName($viewName);

            foreach($viewContentFields as $viewField){
                $f = $path.'/'.$this->getName ().'.'.$viewName.'.'.$viewField;
                $vContent = @file_get_contents($f);
                $newView->set($viewField.'Content', $vContent);
            }
            $this->addViews($newView);
        }
    }
}
//d($this);

$this->save();