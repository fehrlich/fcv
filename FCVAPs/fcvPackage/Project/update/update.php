<?php
/* @var $this Project*/
/* @var $build Build*/

$this->setCurrentBuild($build);
Debug::getObj()->startMeasure('update', 'Update Project '.$this->getId());

Debug::getObj()->startMeasure('getPathData', 'Get Raw Data from Path '.$this->getUmlPath());
$xmlData = file_get_contents($this->getUmlPath());
Debug::getObj()->stopMeasure('getPathData');

Debug::getObj()->startMeasure('parseUml', 'parse UML');
$umlModelOld = $this->getUmlModel();
if(!is_object($umlModelOld) && is_string($umlModelOld)){
    $umlModelOld = unserialize ($umlModelOld);
}
else
    $umlModelOld = new UMLModel($this->getId(), $this->getName());

$parser = new xmiParserArgo($xmlData, new UMLModel($this->getId(), $this->getName()));
$newUmlModel = $parser->parse();

Debug::getObj()->stopMeasure('parseUml');
Debug::getObj()->setDebugPoint('Merge with old SVN ...');
Debug::getObj()->startMeasure('mergeUml', 'merg UML with old one');
$umlMerge = new UMLMerge($umlModelOld, $newUmlModel);
Debug::getObj()->stopMeasure('mergeUml');
$umlMerge->merge();
    
//if($newUmlModel->hasChanged()) $revision++;

Debug::getObj()->startMeasure('parseSQL', 'parse SQL');
Debug::getObj()->setDebugPoint('Parse Mysql Model from UML ...');
$mysqlModel = new MysqlModel($newUmlModel);
$mysqlModel->parse();
Debug::getObj()->stopMeasure('parseSQL');

//d($newUmlModel);

//var_dump($newUmlModel);
$this->setUmlModel($newUmlModel);
$this->setMysqlModel($mysqlModel);

Debug::getObj()->startMeasure('projBuild', 'Build WorkPackages');
Debug::getObj()->setDebugPoint('Build WorkPackages ...');
$this->buildWorkPackages(false);
Debug::getObj()->stopMeasure('projBuild');

Debug::getObj()->setDebugPoint('Update Done');

Debug::getObj()->stopMeasure('update');
//d($res);
//krumo($this);
//d($this);
//dump_r($this,false,true, 10,0);

//$this->getWorkPackages()->build();
//Build
//$this->build();
////Cleanup
return $this;