<?php
/* @var $this CodeModul*/

if(!$this->hasParent()) return '/';
$path = array();
$parent = $this->getParent();
$path[] = $parent->getName();
while($parent->hasParent()){
    $parent = $parent->getParent();
    $path[] = $parent->getName(); 
}
 
return '/'.implode('/',array_reverse($path));